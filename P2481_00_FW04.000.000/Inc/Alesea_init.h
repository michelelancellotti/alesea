#define BOOT_VERSION_ADDRESS		0x08000240
#define HW_VERSION_ADDRESS		0x08000248

extern uint8_t DimensioneVariabiliCriticheArray;


void InitVersioni(void);
void InitCollaudo(void);
void InitDati(void);
void InitVariabiliCritiche(void);
