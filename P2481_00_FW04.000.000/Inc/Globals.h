#include <string.h>
#include <stdint.h>


#ifndef _GLOBALS_H
#define _GLOBALS_H

#include "Versioni_HW.h"




//define di test
/*
#define GPS_ON
//#define FREE_FALL_ON
#define DEBUG_ON
//#define DEBUG_NO_TX
//#define CALIBRAZIONE_GYRO_ON
//#define ENB_MODEM_DEBUG

#define PERIODO_COLLAUDO		10	        //secondi
#define PERIODO_PRE_ACTIVE	        20//300	//secondi
#define PERIODO_ACTIVE			172800//86400      //3600*24	//secondi
#define PERIODO_4TX_AL_GIORNO	        21600	//secondi
#define NUMERO_TX_ATTIVAZIONE_4TX	1
#define NUMERO_TX_ATTIVAZIONE	        NUMERO_TX_ATTIVAZIONE_4TX//40
#define NUMERO_TRASMISSIONI_COLLAUDO	1
#define MAX_RETRY_SUB	                2

#define SPIN_SCH_INTERVAL_FIRST          60
#define SPIN_SCH_INTERVAL_SECOND         30
#define MOVE_SCH_INTERVAL_DEFAULT        43200//7200
#define MOVE_SCH_INTERVAL_MIN            600//7200
#define MOVE_SCH_INTERVAL_MAX            43200//7200
#define TEMPERATURE_SCH_INTERVAL         3600

#define TIMEOUT_SUPERVISOR	        1200	//secondi

#define VERSIONE_FW			4
#define SOTTO_VERSIONE_FW		22

#define SOGLIA_MOV_DEFAULT              SOGLIA_0_18g  
#define SOGLIA_SHOCK_DEFAULT            SOGLIA_3_5g   
#define SOGLIA_MOV_MAX                  SOGLIA_2g
#define SOGLIA_MOV_MIN                  SOGLIA_0_18g         
#define SOGLIA_SHOCK_MAX                SOGLIA_4g
#define SOGLIA_SHOCK_MIN                SOGLIA_2g
*/


//define di produzione

#define GPS_ON
#define FREE_FALL_ON
//#define DEBUG_ON
//#define CALIBRAZIONE_GYRO_ON
//#define ENB_MODEM_DEBUG

#define PERIODO_COLLAUDO		10	//secondi
#define PERIODO_PRE_ACTIVE	        300	//secondi
#define PERIODO_ACTIVE			172800//86400   //3600*24	//secondi
#define PERIODO_4TX_AL_GIORNO	        21600	//secondi
#define NUMERO_TX_ATTIVAZIONE_4TX	5
#define NUMERO_TX_ATTIVAZIONE	        NUMERO_TX_ATTIVAZIONE_4TX//40
#define NUMERO_TRASMISSIONI_COLLAUDO	3
#define MAX_RETRY_SUB	                1    

#define SPIN_SCH_INTERVAL_FIRST                 3600
#define SPIN_SCH_INTERVAL_SECOND                1200
#define MOVE_SCH_INTERVAL_DEFAULT        43200//7200
#define MOVE_SCH_INTERVAL_MIN            600//7200
#define MOVE_SCH_INTERVAL_MAX            43200//7200
#define TEMPERATURE_SCH_INTERVAL                3600

#define TIMEOUT_SUPERVISOR	        1200	//secondi

#define VERSIONE_FW			5//4
#define SOTTO_VERSIONE_FW		50//23


#define SOGLIA_MOV_DEFAULT              SOGLIA_0_18g  
#define SOGLIA_SHOCK_DEFAULT            SOGLIA_3_5g   
#define SOGLIA_MOV_MAX                  SOGLIA_2g
#define SOGLIA_MOV_MIN                  SOGLIA_0_18g         
#define SOGLIA_SHOCK_MAX                SOGLIA_4g
#define SOGLIA_SHOCK_MIN                SOGLIA_2g





#define PROFILO_TEMPERATURA_ON  0xAA
#define PROFILO_TEMPERATURA_OFF  0x55

#define ROTAZIONE_ORIZZ_OFF  0X00
#define ROTAZIONE_ORIZZ_ON  0X01

#define HZ_RTC	                        10

typedef enum
{
  ALESEA_ACTIVE				= 1,
  ALESEA_READY				= 2,
  ALESEA_PRE_COLLAUDO	                = 3,
  ALESEA_COLLAUDO			= 4,
} AleseaStato;

extern uint8_t WwdgCounter;


extern uint8_t FlagAttivazione;
extern uint8_t FlagDisattivazione;
extern uint8_t FlagFactoryReset;
extern uint8_t FlagSpinReset;
extern uint8_t FlagShockReset;
extern uint8_t FlagFreeFallReset;
extern uint8_t FlagBatteryReset;
extern uint8_t FlagDiagnosticaReset;
extern uint8_t FlagUpgradeFw;
extern uint8_t FlagInstantMessage;
extern uint8_t FlagSincronizzazione;
extern uint8_t FlagMicroReset;
extern uint8_t FlagGpsAssistito;
extern uint8_t FlagNoRxMQTT;	//Flag per il bypass della fase di ricezione MQTT: usato quando viene dato cmd da app e viene forzato risveglio RTC
extern uint8_t flagCalibrazioneMems;
extern uint8_t FlagForceGpsOn;
extern uint8_t FlagSetGnssSatelliti;
extern uint32_t SchIntervalCounter;
extern uint32_t TempoDiRisveglioDef;
////////////////////////////////////////////////////////////////////////
/////////////////////////SET PROSSIMO RISVEGLIO/////////////////////////
////////////////////////////////////////////////////////////////////////
void ImpostaRisveglio(uint32_t period);

#endif
