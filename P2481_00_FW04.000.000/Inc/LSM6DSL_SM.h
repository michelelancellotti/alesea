

#define NUMERO_CAMPIONI_ORIENTAMENTO	1
#define SOGLIA_ORIENTAMENTO		300
#define FLANGIA_PERPENDICOLARE_TERRA	1
#define FLANGIA_A_TERRA			0
#define	FLANGIA_NO_RESULT               2
#define MEMS_CALIB_MAX_VALUE            4       //dps
#define N_CAMPIONI_CALIBRAZIONE_V_ANG   500
#define N_CAMPIONI_V_ANG                100
#define N_CAMPIONI_ACC                  25      //25 => max 60 rpm              //100 => max 15 rpm
#define QUADRANTE_ISTERESI              100
#define ARRAY_TEMPERATURE_DIM           30
#define SOGLIA_SUP_RPS_CNG_MODE         0.5      //giri al secondo = RPM/60
#define SOGLIA_INF_RPS_CNG_MODE         0.3       //giri al secondo = RPM/60

#define GYRO    1
#define QUADRANTI   0



typedef struct
{
  float sign;
  float val;
  float sum;
  float mean;
}mg_t;

typedef enum
{
  MEMS_INIT_CONTA_GIRI					= 0,
  MEMS_CONTA_GIRI					= 1, 
  MEMS_SALVA_GIRI					= 2,
  MEMS_GO_TO_STOP_MODE					= 3,
} AleseaMacchinaStatiMems;

typedef enum
{
  MEMS_INIT_CALIBRAZIONE				= 0,
  MEMS_PAUSA					        = 1, 
  MEMS_CALIBRAZIONE					= 2,
} AleseaMacchinaStatiCalibrazioneMems;

typedef enum
{
  MOVE_INIT				                = 0,
  MOVE_RUN					        = 1, 
  MOVE_STOP				        	= 2,
} AleseaMacchinaStatiMovimento;



extern AleseaMacchinaStatiMems StatoMEMS;
extern AleseaMacchinaStatiCalibrazioneMems StatoCalibrazioneMEMS;
extern AleseaMacchinaStatiMovimento StatoMovimento;
extern uint8_t FlagWakeupDaMEMS;
extern uint8_t FlagRefreshRotation;
extern uint8_t FlagWakeUpDaShock;
extern uint8_t FlagWakeUpDaMovimento;
extern uint8_t FlagWakeUpDaFreeFall;
extern uint8_t FlagModemOnInSpinSession;
extern uint8_t FlagWakeupPerTemp;
extern uint8_t PrimoAvvioMEMS;
extern uint8_t FlagTemperaturaAcquisita;
extern uint8_t FlagSpinSessionStart;
extern uint8_t FlagMovementSessionStart;
extern float NumeroGiriFloatFase;
extern uint16_t SpinTimeCounter;
extern uint16_t SpinStartStopCounterAppoggio;

extern uint8_t FlagShockSessionStart;
extern uint8_t FlagModemOnInShockSession;
extern uint8_t FlagModemOnInMovementSession;

extern uint8_t SpinTimeCounterEnable;
extern float CalibrazioneMems;

extern uint8_t DebugSession;
extern uint8_t SogliaAccelerometro;

extern uint8_t ArrayTemperaturaIndex;

uint8_t GetOrientamento(void);
void MacchinaStatiMEMS(void);
void MacchinaStatiShock(void);
void MacchinaStatiFreeFall(void);
void MacchinaStatiMovimento(void);
uint8_t GetTemperature(void);
void MacchinaStatiCalibrazioneMEMS(void);
void MacchinaStatiSpinSession(void);
void SogliaAccelerometroSet(uint8_t);