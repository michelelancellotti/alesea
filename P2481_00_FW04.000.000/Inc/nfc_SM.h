#include "nfc.h"
#include "ndef.h"



#define TIMEOUT_PRESENZA_NFC				50 //base 100ms -> 5 sec

extern uint8_t TimerPresenzaNFC;

typedef enum
{
  NFC_START						= 0,	//mantenere lo stato START = 0 in modo che la variabile sia inizializzata allo stato corretto!
  NFC_READ						= 1,
  NFC_DECODE						= 2, 
  NFC_CHECK						= 3,
  NFC_END						= 4,
  NFC_WAIT						= 5,
  NFC_WRITE                                             = 6,
} AleseaMacchinaStatiNFC;
extern AleseaMacchinaStatiNFC StatoNFC, StatoFuturoNFC;

extern uint8_t FlagWakeupDaNFC;

void MacchinaStatiNFC(void);