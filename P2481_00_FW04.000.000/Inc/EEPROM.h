#include <string.h>
#include <stdint.h>




#define CHIAVE_COLLAUDO	                                0xBA

#define EEPROM_ADD_START  				0x08080000


#define EEPROM_ADD_CHIAVE  				0x08080000
#define EEPROM_ADD_VERSION  				0x08080001
#define EEPROM_ADD_SUBVERSION  				0x08080002
#define EEPROM_ADD_COD_ERRORE  				0x08080003
#define EEPROM_ADD_N_RETRY  				0x08080004
#define EEPROM_ADD_CHECKSUM  				0x08080005		//4B




#define EEPROM_CHIAVE_COLLAUDO1	  		        0x08080009		//1 byte


#define EEPROM_INDICE_RECORD				0x0808000A		//1 byte
#define EEPROM_TX_TIME1	  				0x0808000B		//4 byte
#define EEPROM_VERSIONE_HW				0x0808000F		//1 byte
#define EEPROM_CONSUMO_TOT				0x08080010		//4 byte
#define EEPROM_NUMERO_GIRI_TOT_CW			0x08080014		//4 byte
#define EEPROM_ICCID					0x08080018		//20 byte
#define EEPROM_STATO_ALESEA1				0x0808002C		//1 byte
#define EEPROM_INDICE_TRASMISSIONE		        0x0808002D		//4 byte
#define EEPROM_NUMERO_GIRI_TOT_ACW		        0x08080031		//4 byte
#define EEPROM_FLAG_TEMPERATURA_ACQ		        0x08080035		//1 byte
#define EEPROM_CONTA_SHOCK				0x08080036		//4 byte
#define EEPROM_PERIODO_ACTIVE1				0x0808003A		//4 byte
#define EEPROM_PREACTIVE_COUNT				0x0808003E		//1 byte
#define EEPROM_CONTA_FREE_FALL                          0x0808003F		//1 byte

#define EEPROM_ADD_DATI_1  				0x08080040		//5 X 256 BYTE  -> 0x08080540



#define EEPROM_MICRO_ACTIVITY				0x08080600		//4 byte
#define EEPROM_MODEM_ACTIVITY				0x08080604		//4 byte
#define EEPROM_RESET_EEPROM				0x08080608		//4 byte
#define EEPROM_CONTA_STATO_IN				0x0808060C		//1 byte
#define EEPROM_CONTA_STATO_OUT				0x0808060D		//1 byte
#define EEPROM_CONTA_FAIL_MODEM_ON		        0x0808060E		//1 byte
#define EEPROM_TIPO_MODEM		                0x0808060F		//1 byte

//liberi
#define EEPROM_CHIAVE_COLLAUDO2	  		        0x08080700		//1 byte
//#define 	  					0x08080701		//1 byte
//#define 	  					0x08080702		//1 byte
//#define 	  					0x08080703		//1 byte
#define EEPROM_CHIAVE_COLLAUDO3	  		        0x08080704		//1 byte
//#define 	  					0x08080705		//1 byte
//#define 	  					0x08080706		//1 byte
//#define 	  					0x08080707		//1 byte
#define EEPROM_STATO_ALESEA2				0x08080708		//1 byte
//#define 	  					0x08080709		//1 byte
//#define 	  					0x0808070A		//1 byte
//#define 	  					0x0808070B		//1 byte
#define EEPROM_STATO_ALESEA3				0x0808070C		//1 byte
//#define 	  					0x0808070D		//1 byte
//#define 	  					0x0808070E		//1 byte
//#define 	  					0x0808070F 		//1 byte

#define EEPROM_CONTA_EVENTI_CRC				0x08080710		//1 byte
//#define 	  					0x08080711		//1 byte
//#define 	  					0x08080712		//1 byte
//#define 	  					0x08080713 		//1 byte

#define EEPROM_CONTA_EVENTI_RAM				0x08080714		//1 byte
//#define 	  					0x08080715		//1 byte
//#define 	  					0x08080716		//1 byte
//#define 	  					0x08080717 		//1 byte

#define EEPROM_STATO_GPS_ASSISTITO		        0x08080718		//1 byte
#define EEPROM_STATO_GPS_TIMEOUT			0x08080719              //1 byte
//#define 						0x0808071A              //1 byte
//#define 						0x0808071B              //1 byte

#define EEPROM_PERIODO_ACTIVE2				0x0808071C		//4 byte

#define EEPROM_PERIODO_ACTIVE3				0x08080720		//4 byte

#define EEPROM_TX_TIME2					0x08080724		//4 byte

#define EEPROM_TX_TIME3					0x08080728		//4 byte

//diagnostica
#define EEPROM_DIAGNOSTICA_1				0x0808072C		//1 byte
#define EEPROM_DIAGNOSTICA_2				0x0808072D		//1 byte
#define EEPROM_DIAGNOSTICA_3				0x0808072E		//1 byte
#define EEPROM_DIAGNOSTICA_4				0x0808072F		//1 byte
#define EEPROM_DIAGNOSTICA_5				0x08080730		//1 byte
#define EEPROM_DIAGNOSTICA_6				0x08080731		//1 byte
#define EEPROM_DIAGNOSTICA_7				0x08080732		//1 byte
#define EEPROM_DIAGNOSTICA_8				0x08080733		//1 byte
//                                                      
#define EEPROM_CONTA_UNP				0x08080734		//1 byte
#define EEPROM_SOGLIA_SHOCK                             0x08080735		//1 byte
#define EEPROM_SOGLIA_MOV                               0x08080736		//1 byte
#define EEPROM_FILTRO_QUADRANTE_FERMO                   0x08080737              //4 byte
#define EEPROM_GPS_PRO                                  0x0808073B              //1 byte
#define EEPROM_GPS_PRO_TIME                             0x0808073C              //1 byte
#define EEPROM_GPS_FREQ_FORCING                         0x0808073D              //1 byte
#define EEPROM_PROFILO_TEMPERATURA_ON                   0x0808073E              //1 byte
#define EEPROM_ROTAZIONE_ORIZZ                          0x0808073F              //1 byte
#define EEPROM_MOVE_SCH_INTERVAL                        0x08080740              //1 byte






#define EEPROM_MEMS_CALIBRATION				0x080807E0		//4 byte
//...


#define EEPROM_CRC					0x080807F0		//4 byte

#define CONTA_RESET					0x080807F4


#define EEPROM_ADD_PASSO				256
#define NUM_MAX_RECORD	  				5

#define UPD_KEY_PRESENTE				0xAA

#define EEPROM_ADD_END	  				0x080807FF








uint32_t EEPROM_WriteByte(uint32_t Indirizzo, uint8_t Dato);
uint8_t EEPROM_ReadByte(uint32_t Posizione);
void EEPROM_WriteWord(uint32_t Indirizzo, int32_t Dato);
int32_t EEPROM_ReadWord(uint32_t Posizione);
void EEPROM_Clear(void);


	
