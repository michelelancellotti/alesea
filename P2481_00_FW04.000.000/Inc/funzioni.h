#define TIMEOUT_ATTIVAZIONE_READY	10
#define TIMEOUT_ATTIVAZIONE_PRECOLLAUDO	10


#define LED_1_ON		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
#define LED_2_ON		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
#define LED_3_ON		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_SET);

#define LED_1_OFF		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
#define LED_2_OFF		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);
#define LED_3_OFF		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_6, GPIO_PIN_RESET);

#define SWITCH_OFF	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);

extern uint32_t CRC_calcolato, CRC_letto;
extern uint32_t Supervisor;


void AttivaAlesea(void);
void ResettaDiagnostica(void);
void ResetParametriFabbrica(void);
void ResettaConsumo(void);
void ResettaContagiri(void);
void ResettaContaShock(void);
uint32_t CalcolaCRC(void);
void Spegnimento(void);
void EnterStopMode(void);
void VerificaStatoCollaudo(void);
void DisattivaAlesea(void);
void InitVersioni(void);
void InitCollaudo(void);
void GestioneStopMode(void);
void GestioneTimeoutAttivazione(void);
void GestioneDiagnostica(void);
void ResettaContaFreeFall(void);
void AggiornaModemType(void);