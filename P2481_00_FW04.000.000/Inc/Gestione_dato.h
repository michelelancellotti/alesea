#include <string.h>
#include <stdint.h>
#include "LSM6DSL.h"
#include "Globals.h"




#define LIMITE_SCHEDULING_INTERVAL	31536000	//1 anno espresso in secondi
#define	JSON_MAX_BUFFER_DATA_TX	        800
#define JSON_MAX_BUFFER_DATA_RX	        64

#define GPS_TIMEOUT_MAX			20		//minuti
#define GPS_TIMEOUT_DEFAULT	        5		//minuti
#define GPS_PRO_STATE_DEFAULT           0
#define GPS_PRO_TIME_DEFAULT            5
#define GPS_PRO_TIME_MIN                1
#define GPS_PRO_TIME_MAX                20
#define GPS_FREQ_FORCING_DEFAULT        20//99
#define GPS_FREQ_FORCING_MIN            1
#define GPS_FREQ_FORCING_MAX            99

#define PESO_SCHEDULE   1//(uint8_t)0b00000001      				
#define PESO_SPIN       2//(uint8_t)0b00000010    				
#define PESO_MOVE       4//(uint8_t)0b00000100   				
#define PESO_SHOCK      8//(uint8_t)0b00001000				


extern uint8_t DatoJson[];
extern uint16_t DatoJsonDim;

extern uint8_t DatoJsonRx[];
extern uint16_t DatoJsonRxDim;


extern uint8_t VersioneFW;
extern uint8_t SottoversioneFW;

extern uint8_t VersioneFWRX;
extern uint8_t SottoversioneFWRX;

extern uint8_t VersioneHW;
extern uint8_t SottoversioneHW;


typedef enum
{
  MODEM_SET_PERIODO				= '1',
  MODEM_FACTORY_RESET 				= '2',
  MODEM_SPIN_RESET				= '3',
  MODEM_DEACTIVATION				= '4',
  MODEM_BATTERY_RESET 				= '5',
  MODEM_UPGRADE_FW 				= '6',
  //MODEM_INSTANT_MESSAGE			= '7',
  MODEM_MICRO_RESET  				= '8',
  MODEM_SHOCK_RESET  				= '9',
  MODEM_GPS_SETUP		  		= 'A',
  MODEM_DIAGNOSTICA_RESET			= 'B',
  MODEM_SET_SOGLIA_MOV      			= 'C',
  MODEM_SET_SOGLIA_SHOCK    			= 'D',
  //PERIODO START UP - VERSIONE X
  //PERIODO WARM UP - VERSIONE X
  MODEM_SET_GNSS_SATELLITI                      = 'G',
  MODEM_FREE_FALL_RESET                         = 'H',
  MODEM_SET_TIMEOUT_SPIN                        = 'I',
  MODEM_GPS_PRO_SETUP                           = 'J',
  MODEM_GPS_FREQ_FORCING                        = 'L',
  MODEM_PROFILO_TEMPERATURA_SETUP               = 'M',
  MODEM_ROTAZIONI_ORIZZ_ON                      = 'N',
  MODEM_SET_PERIODO_MOVIMENTO                   = 'O',
}MessageTypes;


typedef struct 
{
  uint32_t IndiceTrasmissione;		//d0
  uint16_t TempoConnessioneGps; 	//t1
  uint8_t SpinSession;                  //ss
  int32_t  NumeroGiriCw;		//s0
  int32_t  NumeroGiriACw;		//s1
  uint16_t SpinStartStopCounter;        //s2
  float  SpinFreq;                      //s3
  uint8_t ICCID[20 + 1];		//id
  uint16_t SizeOfICCID;
  uint8_t BootVersion[8+1];		//bw
  uint8_t VersioneFwTx[5+1];		//sw
  uint8_t VersioneHwTx[5+1];		//hw
  uint32_t SchedulingInterval;		//p0		//espresso in secondi
  uint8_t TempoRegGsm;			//t2
  uint8_t Rssi;				//d1
  uint8_t data_ora[17+1];		//dt
  uint16_t SizeOfDataOra;		//dt
  uint32_t CaricaResidua;		//b0
  float gps_latitudine;			//c0/c2
  float gps_longitudine;		//c0/c2 
  uint16_t	MMC;			//c1
  uint16_t	MNC;			//c1		
  uint32_t	CID;			//c1	
  uint16_t	LAC_TAC;		//c1
  uint8_t TrasmissionChannel[10];
  uint8_t Orientamento;			//p1
  uint8_t InputVariation;		//in
  uint8_t	Retrasmission;		//rt
  int8_t 	Temperatura;		//t0
  int8_t 	TemperaturaMax;		//tmax
  int8_t 	TemperaturaMin;		//tmin
  uint8_t ProfiloTemperaturaOn;          //pt
  uint32_t ContaShock;			//sk
  uint8_t ShockSession;                 //sks
  uint8_t ContaFreeFall;                //ff        
  uint8_t MovementSession;              //ms   
  uint8_t SogliaMovimento;              //tm
  uint8_t SogliaShock;                  //ts        
  uint16_t TempoModemOn;		//t3
  AleseaStato StatoAlesea;		//st	
  uint16_t GpsRegTemp;			//ag
  uint8_t GpsAssistito;			//as
  uint8_t GpsTimeout;			//gt
  uint16_t RegRete;			//rr
  uint8_t LastError;			//rr	
  uint8_t TxInterruptSource;            //txs
  uint32_t FiltroMovimento;             //fm
  uint8_t GpsPro;                       //gp
  uint8_t GpsProTime;                   //gpt   //espresso in secondi
  float gpsPro_latitudine;              //c0
  float gpsPro_longitudine;             //c0     
  uint8_t GpsFreqForcing;               //gf
  uint8_t GpsForcingCounter;            //gfc
  uint8_t TipoModem;                    //mt
  uint8_t GpsSatelliti;
  uint8_t RotazioneOrizzontaleOn;       //ro
  uint32_t MoveSchInterval;             //msi

}StrutturaDati;
//166 byte

//typedef struct struttura_dati StrutturaDati;
extern StrutturaDati Dati;
extern StrutturaDati DatiLetti;
extern StrutturaDati *puntatore;


typedef struct
{
  uint8_t       DatoPresente;
  uint16_t	MMC;			
  uint16_t	MNC;				
  uint32_t	CID;			
  uint16_t	LAC_TAC;
  int16_t       Rssi;
  uint8_t       TrasmissionChannel[10];
}StrutturaCella;

extern StrutturaCella CellaServente;
extern StrutturaCella CellaVicina1;
extern StrutturaCella CellaVicina2;
extern StrutturaCella CellaVicina3;

  

typedef struct 
{
  uint8_t Byte1;
  uint8_t Byte2;
  uint8_t Byte3;
  uint8_t Byte4;
  uint8_t Byte5;
  uint8_t Byte6;
  uint8_t Byte7;
  uint8_t Byte8;
  
  uint8_t ContaEventiCRC;			//cc
  uint8_t ContaReset;				//cr
  uint8_t ContaEventiStatoRam;			//cc
  uint32_t MicroActivityCounter;		//mc
  uint32_t ModemActivityCounter;		//mo
  uint32_t CounterEepromReset;			//re
  uint8_t ContaStatoIn;				//si
  uint8_t ContaStatoOut;			//so
  uint8_t CountErroreModemOn;			//mf
  uint8_t CountUnpError;			//cu
}StrutturaDiagnostica;

extern StrutturaDiagnostica Diagnostica;


extern float TotGiriAcc, TotGiriGyr;

void SalvataggioDati(uint8_t Indirizzo);
void GeneraStringa(void);
void GeneraStringaLetta(void);
void CancellaEEPROM(void);
uint8_t SalvataggioStrutturaDati(uint8_t indice);
void LetturaStrutturaDati(uint8_t indice);


void SetIndiceTrasmissione(uint8_t dato);
void SetTempoConnessioneGps(uint8_t dato);
void SetICCID(uint8_t* dato);
void SetVersione_fw(uint8_t* dato);
void SetSchedulingInterval(uint8_t dato);
void SetVersioneHw(uint8_t* dato);
void SetTempoRegGsm(uint8_t dato);
void SetRssi(uint8_t dato);
void SetTempoAcqGsm(uint8_t dato);
void data_ora(uint8_t* dato);
void Setgps_latitudine(float dato);
void Setgps_longitudine(float dato);
void gsm_latitudine(float dato);
void gsm_longitudine(float dato);
void SetMCC(int8_t dato);
void SetMNC(int8_t dato);
void SetLAC(int8_t dato);
void SetCID(int8_t dato);
void GestioneDatoJsonRx(void);
void PulisciDatoJsonRx(void);
uint32_t DecodificaStringa(const uint8_t*  Stringa, uint8_t dim);




	
