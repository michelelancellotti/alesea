#include <string.h>
#include <stdint.h>

extern float ConsumoFase[];	
extern uint32_t DurataFase[];		//espresso in s

extern uint8_t FlagConsumoGPSOn;
extern uint8_t FlagConsumoGSMOn;
extern uint8_t FlagConsumoNFCOn;
extern uint8_t FlagConsumoMEMSOn;


extern uint32_t Consumo_uA_h_tot;

#define FASE_STOP_MODE				0
#define FASE_MEMS							1
#define FASE_NFC							2
#define FASE_GPS							3
#define FASE_GSM							4
#define	FASE_UC_IDLE					                5


#define CONSUMO_STOP_MODE				0.0521		//mA
#define CONSUMO_MEMS						3.25
#define CONSUMO_NFC							1.3
#define CONSUMO_GPS							87.0    //BG95 => 87.0mA             //BG96 => 55.0mA    
#define CONSUMO_GSM							30.0    //BG95 => 30mA               //BG96 => 23.0mA
#define	CONSUMO_UC_IDLE					0.0//5.0

#define NUMERO_FASI							5

#define CAPACITA_BATTERIA				2600//  3600	//mA*h          3600->2600 per usare capacit� realmente sfruttabile 12/11/2021  FW 4.18
#define SECONDI_ORA							3600

uint32_t GetConsumo(void);
void InitConsumiFasi(void);
void IncrementoTempiConsumi(uint32_t stop_mode_time);
void ResettaConsumo(void);
uint32_t CalcolaCaricaResidua(uint32_t consumo);
