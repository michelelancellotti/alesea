#include "LSM6DSL_driver.h"
#include "mTypes.h"

#define TX_BUF_DIM              1000
#define TEMPO_GIROSCOPIO_ON	500				        //500 = 5 secondi
#define FILTRO_QUADRANTE_FERMO_DEFAULT  150                             //secondi a quadrante //60 -> 150 in data 12/11/2021 per misurae vel < 0.1 rpm  (150 x 4 = 600 secondi per 1 giro)    FW 4.18
#define FILTRO_ROTAZ_ORIZZ_FERMO_DEFAULT  60                            //secondi a quadrante //(60 x 4 = 240 secondi per 1 giro)    
#define FILTRO_QUADRANTE_FERMO_MIN  30                                  //0.5 rpm
#define FILTRO_QUADRANTE_FERMO_MAX  200
#define SOGLIA_ANGOLO_MAX	900
#define SOGLIA_ANGOLO_MIN	100


#define SOGLIA_0_18g     3                      //0.18g     
#define SOGLIA_0_5g      8                      
#define SOGLIA_1g	16      		//0.15g //FS*N/64
#define SOGLIA_1_5g     24
#define SOGLIA_2g       32
#define SOGLIA_2_5g     40
#define SOGLIA_3g       48
#define SOGLIA_3_5g     56
#define SOGLIA_4g       64            

#define FALL_DUR_STOP_MODE       1//3   //1/12.5Hz =>   80ms
#define FALL_DUR_RUN_MODE       30      //307412Hz =>   73ms

#define FILTRO_FAILURE_I2C              20

extern uint32_t LSM6DSL_interrupt_timer;
extern float NumeroGiriGyr;
extern float acceleration_mg[3];
extern float angular_rate_mdps[3];

extern uint8_t flag_lsm6dsl_ignore_int;
extern uint8_t timer_flag_lsm6dsl_ignore_int;

void InitLSM6DSL(void);
float LSM6DSL_cycle(void);
float LSM6DSL_Temperatura(void);
void LSM6DSL_cycle_acc(void);
void LSM6DSL_cycle_gyr(void);
void LSM6DSL_GYRO_OFF(void);
void LSM6DSL_GYRO_ON(void);
void SetInterruptAngolo(void);
void LSM6DSL_Gyro_Start(void);
void TestTilt(void);
void ResettaNumeroGiri(void);
float LSM6DSL_Orientamento(void);
void SetInterruptShock(void);
void InitLSM6DSL_test(void);
void SetAccStopFreq(void);
void SetAccRunFreq(void);
void SogliaAccelerometroSet(uint8_t val);
void SetInterruptFreeFall(uint8_t val);
uint8_t FreeFallSourceRead(void);
uint8_t WakeUpSourceRead(void);
//extern void InitBufferShocks(void);


