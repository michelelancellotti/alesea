#ifndef __NDEF_NDEF_H__
#define __NDEF_NDEF_H__

#define NO_ID 0

#define NFC_ATTIVAZIONE						'0'
#define NFC_SET_PERIODO						'1'
#define NFC_FACTORY_RESET					'2'
#define NFC_SPIN_RESET						'3'
#define NFC_DISATTIVAZIONE				'4'
//#define NFC_BATTERY_RESET 				'5'
#define NFC_UPGRADE_FW						'6'
#define NFC_INSTANT_MESSAGE				'7'
//#define NFC_MICRO_RESET  					'8'
//#define NFC_SHOCK_RESET  					'9'
//#define NFC_GPS_ASS		  					'A'


	
	#include <stdbool.h>
#include <stdint.h>


extern uint8_t BufferNdefTxShortOffset;
extern struct ndef_payload P1,P2,P3,P4;
extern struct ndef_record R1,R2,R3,R4;
extern struct ndef_payload L1,L2,L3,L4;
extern uint8_t BufferNdefTx[150];
extern uint8_t BufferNdefTxShort[16];
extern uint8_t BufferNdefTxIndex;
extern uint8_t BufferNdefTxStop;



uint8_t LetturaCampoStato(void);
void NfcRead(void);
uint8_t VerificaCodiceNFC(void);
void NdefDecode(void);
void NdefCompile(uint8_t stato);
void GestioneComandiNdef(void);
void ClearL(void);

#endif
