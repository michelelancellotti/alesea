
#include "main.h"
#include "nfc.h"
#include "stm32l0xx_hal.h"


uint8_t 		BufferLetturaNFC[DIMENSIONE_BUFFER_LETTURA_NFC];
uint8_t 		BufferAppoggioNFC[DIMENSIONE_BLOCCO_SCRITTURA_NFC];


uint8_t 	PresenzaCampo, TempPresenzaCampo, PresenzaCampoOld;
uint32_t 	FiltroAssenzaCampo, FiltroPresenzaCampo;
uint32_t 	NFCTimeout = 100;


/*Scrive un buffer di 16 byte in un blocco*/
void ScriviDatoNFC(uint8_t bufferTx[], uint8_t destinazione)
{
	for(uint8_t i = 0; i< sizeof(BufferAppoggioNFC); i++)
	{
		BufferAppoggioNFC[i] = 0;
	}
	
	BufferAppoggioNFC[0] = destinazione;
	for(uint8_t i = 1; i < sizeof(BufferAppoggioNFC); i++)
	{
		BufferAppoggioNFC[i] = bufferTx[i-1];
	}
	HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c1, NFCAddressWR, (uint8_t*)&BufferAppoggioNFC[0], DIMENSIONE_BLOCCO_SCRITTURA_NFC, NFCTimeout);
}

/*Legge i 16 byte di un blocco e li mette nel buffer passato*/
void LeggiBloccoNFC(uint8_t blocco, uint8_t* bufferRX)
{
	uint32_t NFCSizeTX = 1;
	uint32_t NFCSizeRX = 16;
	uint8_t 	NFCBlock[2];
	NFCBlock[0] = blocco;
	NFCBlock[1] = 0x00;
	HAL_Delay(10);
	HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c1, NFCAddressRR, (uint8_t*)&NFCBlock[0], NFCSizeTX, NFCTimeout);
	HAL_I2C_Master_Receive( (I2C_HandleTypeDef *)&hi2c1, NFCAddressWR, (uint8_t*)&bufferRX[0], NFCSizeRX, NFCTimeout);
}

/*Lettura del singlo byte contenuto nel blocco passato e posizione all'interno del blocco*/
uint8_t LeggiByteNFC(uint8_t blocco, uint8_t posizione)
{
	uint8_t byte_letto;
	uint32_t NFCSizeTX = 2;
	uint32_t NFCSizeRX = 1;
	uint8_t 	NFCBlock[2];
	NFCBlock[0] = blocco;
	NFCBlock[1] = posizione;
	HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c1, NFCAddressRR, (uint8_t*)&NFCBlock[0], NFCSizeTX, NFCTimeout);
	HAL_I2C_Master_Receive( (I2C_HandleTypeDef *)&hi2c1, NFCAddressWR, (uint8_t*)&byte_letto, NFCSizeRX, NFCTimeout);
	return byte_letto;
}

/*Lettura del singlo byte contenuto nel blocco passato e posizione all'interno del blocco*/
void ScriviByteNFC(uint8_t blocco, uint8_t posizione, uint8_t* dato_wr)
{
	uint32_t NFCSizeTX = 2;
	uint8_t 	NFCBlock[2];
	NFCBlock[0] = blocco;
	NFCBlock[1] = posizione;
	HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c1, NFCAddressRR, (uint8_t*)&NFCBlock[0], NFCSizeTX, NFCTimeout);
}


/*Lettura della presenza del campo dello smartphone*/
uint8_t LeggiPresenzaCampo(void)
{
	PresenzaCampo = LeggiByteNFC(BLOCCO_REGISTRI, POSIZIONE_PRESENZA_CAMPO);
	/*TempPresenzaCampo = LeggiByteNFC(BLOCCO_REGISTRI, POSIZIONE_PRESENZA_CAMPO);
	
	if(TempPresenzaCampo == 0)
	{
		if(FiltroAssenzaCampo < TEMPO_ASSENZA_CAMPO)
		{
			FiltroAssenzaCampo++;
			if(FiltroAssenzaCampo >= TEMPO_ASSENZA_CAMPO)
			{
				PresenzaCampo = 0;
			}
		}
		FiltroPresenzaCampo = 0;
	}
	else if(TempPresenzaCampo == 1)
	{	
		if(FiltroPresenzaCampo < TEMPO_PRESENZA_CAMPO)
		{
			FiltroPresenzaCampo++;
			if(FiltroPresenzaCampo >= TEMPO_PRESENZA_CAMPO)
			{
				PresenzaCampo = 1;
			}
		}
		FiltroAssenzaCampo = 0;
	}*/
	return PresenzaCampo;
}

void PulisciBufferLetturaNFC(void)
{
	for(uint16_t i = 0; i < DIMENSIONE_BUFFER_LETTURA_NFC; i++)
	{
		BufferLetturaNFC[i] = 0;
	}
}


