
#include "EEPROM.h"
#include "Gestione_dato.h"
#include "Gestione_batteria.h"
#include "stm32l0xx_hal.h"
#include "LSM6DSL.h"
#include "Globals.h"
#include "modem.h"
#include "wwdg.h"
#include <stdio.h>


float TotGiriAcc, TotGiriGyr;

const unsigned char msgType[] = "\"msgType\":";
const unsigned char msgPeriodo[] = "\"periodo\":";
const unsigned char msgVersione[] = "\"versione\":";
const unsigned char msgSottoversione[] = "\"sottoversione\":";
const unsigned char msgMinuto[] = "\"minute\":";
const unsigned char msgGpsAss[] = "\"agps\":";
const unsigned char msgGpsTime[] = "\"tgps\":";
const unsigned char msgSogliaAcc[] = "\"soglia\":";
const unsigned char msgGnssSatelliti[] = "\"satelliti\":";
const unsigned char msgTimeoutSpin[] = "\"timeout\":";
const unsigned char msgGpsPro[] = "\"gpro\":";
const unsigned char msgGpsProTime[] = "\"tpro\":";
const unsigned char msgGpsFreqForcing[] = "\"gforce\":";
const unsigned char msgProfiloTemp[] = "\"tempon\":";
const unsigned char msgRotazOrizz[] = "\"roton\":";



StrutturaDati Dati;
StrutturaDati DatiLetti;
StrutturaDiagnostica Diagnostica;

StrutturaCella CellaServente, CellaVicina1, CellaVicina2, CellaVicina3;

uint8_t VersioneFW;
uint8_t SottoversioneFW;

uint8_t VersioneFWRX;
uint8_t SottoversioneFWRX;

uint8_t VersioneHW;
uint8_t SottoversioneHW;

uint8_t DatoJson[JSON_MAX_BUFFER_DATA_TX];
uint16_t DatoJsonDim;

uint8_t DatoJsonRx[JSON_MAX_BUFFER_DATA_RX];
uint16_t DatoJsonRxDim;



uint8_t ContenutoPuntatore;
uint8_t ContenutoEEPROM;
///////////////////////////////////////////////////
////////Scrittura della struttura in EEPROM////////	
///////////////////////////////////////////////////
uint8_t SalvataggioStrutturaDati(uint8_t indice)
{
  uint32_t add;
  uint32_t *punt;
  uint8_t *p_appoggio;
  punt = &Dati.IndiceTrasmissione;
  uint8_t result = 1;
  
  add = EEPROM_ADD_DATI_1 + indice*EEPROM_ADD_PASSO;	//Calcolo l'indirizzo in cui salvare
  
  p_appoggio = (uint8_t *)punt;
  
  for(int i = 0; i < sizeof(Dati); i++)
  { 
    EEPROM_WriteByte( add, *p_appoggio );
    
    
    
    ContenutoPuntatore = *p_appoggio;	//debug
    ContenutoEEPROM = EEPROM_ReadByte(add);	//debug
    
    //rileggo il dato salvato e ne verifico la coerenza
    if(*p_appoggio != EEPROM_ReadByte(add))
    {
      result = 0;
    }
    
    add++;
    (p_appoggio)++;
    
#ifndef DEBUG_ON
    WwdgCounter = (WWDG->CR & 0x7F);
    if((WwdgCounter < 127) && (WwdgCounter > 70))
    {
      HAL_WWDG_Refresh(&hwwdg);
    }
#endif
  }
  return result;	//return 1 se ok, 0 se anomalia EEPROM
}

///////////////////////////////////////////////////
//Lettura dell'ultima struttura salvata in EEPROM//	
///////////////////////////////////////////////////
void LetturaStrutturaDati(uint8_t indice)
{
  uint32_t add;
  uint8_t *p_appoggio;
  p_appoggio = (uint8_t *)&DatiLetti.IndiceTrasmissione;
  add = EEPROM_ADD_DATI_1 + indice*EEPROM_ADD_PASSO;	//ricavo l'indirizzo di lettura dopo aver letto l'indice pacchhetto in EEPROM
  
  for(int i = 0; i < sizeof(Dati); i++)
  {
    *(p_appoggio++) = EEPROM_ReadByte(add++);
  }
}


//////////////////////////////////////////////////
//////////////Costruziione file JSON//////////////
//////////////////////////////////////////////////
void GeneraStringa(void)		
{
  uint16_t indice = 0;	
  indice = sprintf((char*)DatoJson + indice,"\x7B\"id\"\x3A\"%s\"", /*ICCID_appoggio*/Dati.ICCID);	//per rimozione carattere 0x14 in coda a ICCID
  indice += sprintf((char*)DatoJson + indice,",\"dt\"\x3A\"20%s\"", Dati.data_ora);
  indice += sprintf((char*)DatoJson + indice,",\"bw\"\x3A\"%s\"", Dati.BootVersion);
  indice += sprintf((char*)DatoJson + indice,",\"sw\"\x3A\"%s\"", Dati.VersioneFwTx);
  indice += sprintf((char*)DatoJson + indice,",\"hw\"\x3A\"%s\"", Dati.VersioneHwTx);
  
   if(Dati.TipoModem == MOD_BG95)       indice += sprintf((char*)DatoJson + indice,",\"mt\"\x3A\"%s\"", "BG_95");
    else if(Dati.TipoModem == MOD_BG96)      indice += sprintf((char*)DatoJson + indice,",\"mt\"\x3A\"%s\"", "BG_96");
      else indice += sprintf((char*)DatoJson + indice,",\"mt\"\x3A\"%s\"", "N/D"); 
  
  
  
  indice += sprintf((char*)DatoJson + indice,",\"in\"\x3A\"%c\"", Dati.InputVariation);
  indice += sprintf((char*)DatoJson + indice,",\"cr\"\x3A%u", Diagnostica.ContaReset);
  indice += sprintf((char*)DatoJson + indice,",\"b0\"\x3A%u", Dati.CaricaResidua);
  if( (Dati.gps_latitudine != 0) || (Dati.gps_longitudine != 0))
  {
    if( (Dati.gpsPro_latitudine != 0) || (Dati.gpsPro_longitudine != 0))
    {
      indice += sprintf((char*)DatoJson + indice,",\"c0\"\x3A[%f,%f]", Dati.gpsPro_latitudine, Dati.gpsPro_longitudine);
      indice += sprintf((char*)DatoJson + indice,",\"cp\"\x3A[%f,%f]", Dati.gps_latitudine, Dati.gps_longitudine);
    }
    else
    {
      indice += sprintf((char*)DatoJson + indice,",\"c0\"\x3A[%f,%f]", Dati.gps_latitudine, Dati.gps_longitudine);
    }
  }
  else
  {

    indice += sprintf((char*)DatoJson + indice,",\"c1\"\x3A[%u,%u,%u,%u]", Dati.MMC, Dati.MNC, Dati.CID, Dati.LAC_TAC);
    indice += sprintf((char*)DatoJson + indice,",\"db\"\x3A%d", CellaServente.Rssi);  //,"db" :xxx

    indice += sprintf((char*)DatoJson + indice,",\"cn\"\x3A["); //,"cn":[
    if(CellaVicina1.DatoPresente)
    {
       indice += sprintf((char*)DatoJson + indice,"\x7B\"an\"\x3A[%u,%u,%u,%u]", CellaVicina1.MMC, CellaVicina1.MNC, CellaVicina1.CID, CellaVicina1.LAC_TAC);  //{"an": [xxx,xx,xxxxx,xxxxx]
       indice += sprintf((char*)DatoJson + indice,",\"db\"\x3A%d", CellaVicina1.Rssi);  //,"db" :xxx
       indice += sprintf((char*)DatoJson + indice,",\"tc\"\x3A\"%s\"\x7D", CellaVicina1.TrasmissionChannel);        //,"tc" : "GSM"}
    }
    if(CellaVicina2.DatoPresente)
    {
       indice += sprintf((char*)DatoJson + indice,",\x7B\"an\"\x3A[%u,%u,%u,%u]", CellaVicina2.MMC, CellaVicina2.MNC, CellaVicina2.CID, CellaVicina2.LAC_TAC);  //,{"an": [xxx,xx,xxxxx,xxxxx]
       indice += sprintf((char*)DatoJson + indice,",\"db\"\x3A%d", CellaVicina2.Rssi);  //,"db" :xxx
       indice += sprintf((char*)DatoJson + indice,",\"tc\"\x3A\"%s\"\x7D", CellaVicina2.TrasmissionChannel);        //,"tc" : "GSM"}
    }
    if(CellaVicina3.DatoPresente)
    {
       indice += sprintf((char*)DatoJson + indice,",\x7B\"an\"\x3A[%u,%u,%u,%u]", CellaVicina3.MMC, CellaVicina3.MNC, CellaVicina3.CID, CellaVicina3.LAC_TAC);  //,{"an": [xxx,xx,xxxxx,xxxxx]
       indice += sprintf((char*)DatoJson + indice,",\"db\"\x3A%d", CellaVicina3.Rssi);  //,"db" :xxx
       indice += sprintf((char*)DatoJson + indice,",\"tc\"\x3A\"%s\"\x7D", CellaVicina3.TrasmissionChannel);        //,"tc" : "GSM"}
    } 
    indice += sprintf((char*)DatoJson + indice,"]");            //]
  }
  
  
  indice += sprintf((char*)DatoJson + indice,",\"d0\"\x3A%u", Dati.IndiceTrasmissione);
  indice += sprintf((char*)DatoJson + indice,",\"d1\"\x3A%u", Dati.Rssi);
  indice += sprintf((char*)DatoJson + indice,",\"p0\"\x3A%u", (Dati.SchedulingInterval)/60);	//lo comunico espresso in minuti
  indice += sprintf((char*)DatoJson + indice,",\"p1\"\x3A%u", Dati.Orientamento);
  indice += sprintf((char*)DatoJson + indice,",\"rt\"\x3A%u", Dati.Retrasmission);
  
  indice += sprintf((char*)DatoJson + indice,",\"txs\"\x3A%u", Dati.TxInterruptSource);
  
  indice += sprintf((char*)DatoJson + indice,",\"or\"\x3A%d", Dati.RotazioneOrizzontaleOn);
  indice += sprintf((char*)DatoJson + indice,",\"ss\"\x3A%d", Dati.SpinSession);
  
  indice += sprintf((char*)DatoJson + indice,",\"s0\"\x3A%d", Dati.NumeroGiriCw);
  indice += sprintf((char*)DatoJson + indice,",\"s1\"\x3A%d", Dati.NumeroGiriACw);
  if(Dati.SpinSession != 0)
  {
    indice += sprintf((char*)DatoJson + indice,",\"sg\"\x3A\"%f\"", TotGiriGyr);
    indice += sprintf((char*)DatoJson + indice,",\"sa\"\x3A\"%f\"", TotGiriAcc);
  }
  indice += sprintf((char*)DatoJson + indice,",\"s2\"\x3A%d", Dati.SpinStartStopCounter);  
  indice += sprintf((char*)DatoJson + indice,",\"s3\"\x3A\"%.2f\"", Dati.SpinFreq); 
  
  
  indice += sprintf((char*)DatoJson + indice,",\"sks\"\x3A%d", Dati.ShockSession);
  
  indice += sprintf((char*)DatoJson + indice,",\"sk\"\x3A%u", 0/*Dati.ContaShock*/);
  indice += sprintf((char*)DatoJson + indice,",\"sk1\"\x3A%u", Dati.ContaShock);
  
  indice += sprintf((char*)DatoJson + indice,",\"ts\"\x3A%d", Dati.SogliaShock);
  
  indice += sprintf((char*)DatoJson + indice,",\"ms\"\x3A%d", Dati.MovementSession); 
  indice += sprintf((char*)DatoJson + indice,",\"tm\"\x3A%d", Dati.SogliaMovimento);
  indice += sprintf((char*)DatoJson + indice,",\"msi\"\x3A%d", Dati.MoveSchInterval);  
  
  indice += sprintf((char*)DatoJson + indice,",\"fm\"\x3A%d", Dati.FiltroMovimento);
  indice += sprintf((char*)DatoJson + indice,",\"gp\"\x3A%d", Dati.GpsPro);
  indice += sprintf((char*)DatoJson + indice,",\"gpt\"\x3A%d", Dati.GpsProTime);
  indice += sprintf((char*)DatoJson + indice,",\"gt\"\x3A%d", Dati.GpsTimeout);
  indice += sprintf((char*)DatoJson + indice,",\"gf\"\x3A%d", Dati.GpsFreqForcing);
  indice += sprintf((char*)DatoJson + indice,",\"gfc\"\x3A%d", Dati.GpsForcingCounter);
  
  indice += sprintf((char*)DatoJson + indice,",\"ff\"\x3A%u", Dati.ContaFreeFall);
  
  indice += sprintf((char*)DatoJson + indice,",\"tx\"\x3A\"%s\"", Dati.TrasmissionChannel);
  indice += sprintf((char*)DatoJson + indice,",\"t0\"\x3A%d", Dati.Temperatura);
  if(Dati.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON)
  {
    indice += sprintf((char*)DatoJson + indice,",\"tmax\"\x3A%d", Dati.TemperaturaMax);
    indice += sprintf((char*)DatoJson + indice,",\"tmin\"\x3A%d", Dati.TemperaturaMin);
    //indice += sprintf((char*)DatoJson + indice,",\"pt\"\x3A%u", 1);
  }
  else
  {
    //indice += sprintf((char*)DatoJson + indice,",\"pt\"\x3A%u", 0);
  }
  
  //indice += sprintf((char*)DatoJson + indice,",\"as\"\x3A%u", Dati.GpsAssistito);
  indice += sprintf((char*)DatoJson + indice,",\"t1\"\x3A%u", Dati.TempoConnessioneGps);
  indice += sprintf((char*)DatoJson + indice,",\"t2\"\x3A%u", Dati.TempoRegGsm);
  indice += sprintf((char*)DatoJson + indice,",\"t3\"\x3A%u", Dati.TempoModemOn);
  indice += sprintf((char*)DatoJson + indice,",\"cc\"\x3A%u", Diagnostica.ContaEventiCRC);
  indice += sprintf((char*)DatoJson + indice,",\"cs\"\x3A%u", Diagnostica.ContaEventiStatoRam);
  indice += sprintf((char*)DatoJson + indice,",\"mc\"\x3A%u", Diagnostica.MicroActivityCounter/60);	//stampo in minuti
  indice += sprintf((char*)DatoJson + indice,",\"mo\"\x3A%u", Diagnostica.ModemActivityCounter/60);	//stampo in minuti
  indice += sprintf((char*)DatoJson + indice,",\"re\"\x3A%u", Diagnostica.CounterEepromReset);
  indice += sprintf((char*)DatoJson + indice,",\"si\"\x3A%u", Diagnostica.ContaStatoIn);
  indice += sprintf((char*)DatoJson + indice,",\"so\"\x3A%u", Diagnostica.ContaStatoOut);
  indice += sprintf((char*)DatoJson + indice,",\"mf\"\x3A%u", Diagnostica.CountErroreModemOn );
  indice += sprintf((char*)DatoJson + indice,",\"cu\"\x3A%u", Diagnostica.CountUnpError);
  indice += sprintf((char*)DatoJson + indice,",\"dg\"\x3A\"%02x", Diagnostica.Byte1);
  indice += sprintf((char*)DatoJson + indice,"%02x", Diagnostica.Byte2);
  indice += sprintf((char*)DatoJson + indice,"%02x", Diagnostica.Byte3);
  indice += sprintf((char*)DatoJson + indice,"%02x", Diagnostica.Byte4);
  indice += sprintf((char*)DatoJson + indice,"%02x", Diagnostica.Byte5);
  indice += sprintf((char*)DatoJson + indice,"%02x", Diagnostica.Byte6);
  indice += sprintf((char*)DatoJson + indice,"%02x\"", Diagnostica.Byte7);
  
  indice += sprintf((char*)DatoJson + indice,",\"ag\"\x3A\"%x\"", Dati.GpsRegTemp);
  if(Dati.LastError == 0x00)
  {
    indice += sprintf((char*)DatoJson + indice,",\"rr\"\x3A\"%X\"", Dati.RegRete);
  }
  else
  {
    indice += sprintf((char*)DatoJson + indice,",\"rr\"\x3A\"%X,%X\"", Dati.RegRete, Dati.LastError);
  }
  
  indice += sprintf((char*)DatoJson + indice,",\"st\"\x3A%u\x7D", Dati.StatoAlesea);
  
  
  Dati.InputVariation = '0';	//pulisco il campo in che conteneva il MsgType ricevuto
  
  DatoJsonDim = indice;
}


//////////////////////////////////////////////////
//////////////Costruziione file JSON//////////////
//////////////////////////////////////////////////
void GeneraStringaLetta(void)		
{
  uint16_t indice = 0;
  
  indice = sprintf((char*)DatoJson + indice,"\x7B\"id\"\x3A\"%s\"", /*ICCID_appoggio*/DatiLetti.ICCID);
  indice += sprintf((char*)DatoJson + indice,",\"dt\"\x3A\"20%s\"", DatiLetti.data_ora);
  indice += sprintf((char*)DatoJson + indice,",\"bw\"\x3A\"%s\"", DatiLetti.BootVersion);
  indice += sprintf((char*)DatoJson + indice,",\"sw\"\x3A\"%s\"", DatiLetti.VersioneFwTx);
  indice += sprintf((char*)DatoJson + indice,",\"hw\"\x3A\"%s\"", DatiLetti.VersioneHwTx);
  
   if(DatiLetti.TipoModem == MOD_BG95)       indice += sprintf((char*)DatoJson + indice,",\"mt\"\x3A\"%s\"", "BG_95");
    else if(DatiLetti.TipoModem == MOD_BG96)      indice += sprintf((char*)DatoJson + indice,",\"mt\"\x3A\"%s\"", "BG_96");
      else indice += sprintf((char*)DatoJson + indice,",\"mt\"\x3A\"%s\"", "N/D"); 
    
    
  indice += sprintf((char*)DatoJson + indice,",\"in\"\x3A\"%c\"", DatiLetti.InputVariation);
  //indice += sprintf((char*)DatoJson + indice,",\"cr\"\x3A%u", DatiLetti.ContaReset);
  indice += sprintf((char*)DatoJson + indice,",\"b0\"\x3A%u", DatiLetti.CaricaResidua);
  //indice += sprintf((char*)DatoJson + indice,",\"b1\"\x3A%u", Consumo_uA_h_tot);////////////////////rimuovere
  if( (DatiLetti.gps_latitudine != 0) || (DatiLetti.gps_longitudine != 0))
  {
    if( (DatiLetti.gpsPro_latitudine != 0) || (DatiLetti.gpsPro_longitudine != 0))
    {
      indice += sprintf((char*)DatoJson + indice,",\"c0\"\x3A[%f,%f]", DatiLetti.gpsPro_latitudine, DatiLetti.gpsPro_longitudine);
      indice += sprintf((char*)DatoJson + indice,",\"cp\"\x3A[%f,%f]", DatiLetti.gps_latitudine, DatiLetti.gps_longitudine);
    }
    else
    {
      indice += sprintf((char*)DatoJson + indice,",\"c0\"\x3A[%f,%f]", DatiLetti.gps_latitudine, DatiLetti.gps_longitudine);
    }
  }
  else
  {
    indice += sprintf((char*)DatoJson + indice,",\"c1\"\x3A[%u,%u,%u,%u]", DatiLetti.MMC, DatiLetti.MNC, DatiLetti.CID, DatiLetti.LAC_TAC);
  }
  indice += sprintf((char*)DatoJson + indice,",\"d0\"\x3A%u", DatiLetti.IndiceTrasmissione);
  indice += sprintf((char*)DatoJson + indice,",\"d1\"\x3A%u", DatiLetti.Rssi);
  indice += sprintf((char*)DatoJson + indice,",\"p0\"\x3A%u", (DatiLetti.SchedulingInterval)/60);	//lo comunico espresso in minuti
  indice += sprintf((char*)DatoJson + indice,",\"p1\"\x3A%u", DatiLetti.Orientamento);
  indice += sprintf((char*)DatoJson + indice,",\"rt\"\x3A%u", DatiLetti.Retrasmission);
  
  indice += sprintf((char*)DatoJson + indice,",\"txs\"\x3A%u", DatiLetti.TxInterruptSource);
  
  indice += sprintf((char*)DatoJson + indice,",\"ss\"\x3A%d", DatiLetti.SpinSession);
  indice += sprintf((char*)DatoJson + indice,",\"or\"\x3A%d", DatiLetti.RotazioneOrizzontaleOn);
  indice += sprintf((char*)DatoJson + indice,",\"s0\"\x3A%d", DatiLetti.NumeroGiriCw);
  indice += sprintf((char*)DatoJson + indice,",\"s1\"\x3A%d", DatiLetti.NumeroGiriACw);
  indice += sprintf((char*)DatoJson + indice,",\"s2\"\x3A%d", DatiLetti.SpinStartStopCounter);  
  indice += sprintf((char*)DatoJson + indice,",\"s3\"\x3A\"%.2f\"", DatiLetti.SpinFreq); 
  indice += sprintf((char*)DatoJson + indice,",\"sks\"\x3A%d", DatiLetti.ShockSession);
  
  indice += sprintf((char*)DatoJson + indice,",\"sk\"\x3A%u", 0/*DatiLetti.ContaShock*/);
  indice += sprintf((char*)DatoJson + indice,",\"sk1\"\x3A%u", DatiLetti.ContaShock);
  
  indice += sprintf((char*)DatoJson + indice,",\"ts\"\x3A%d", DatiLetti.SogliaShock);
  indice += sprintf((char*)DatoJson + indice,",\"ms\"\x3A%d", DatiLetti.MovementSession); 
  indice += sprintf((char*)DatoJson + indice,",\"tm\"\x3A%d", DatiLetti.SogliaMovimento);
  indice += sprintf((char*)DatoJson + indice,",\"msi\"\x3A%d", DatiLetti.MoveSchInterval); 
  
  indice += sprintf((char*)DatoJson + indice,",\"fm\"\x3A%d", DatiLetti.FiltroMovimento);
  indice += sprintf((char*)DatoJson + indice,",\"gp\"\x3A%d", DatiLetti.GpsPro);
  indice += sprintf((char*)DatoJson + indice,",\"gpt\"\x3A%d",DatiLetti.GpsProTime);
  indice += sprintf((char*)DatoJson + indice,",\"gt\"\x3A%d",DatiLetti.GpsTimeout);
  indice += sprintf((char*)DatoJson + indice,",\"gf\"\x3A%d", DatiLetti.GpsFreqForcing);
  indice += sprintf((char*)DatoJson + indice,",\"gfc\"\x3A%d", DatiLetti.GpsForcingCounter);
  
  indice += sprintf((char*)DatoJson + indice,",\"ff\"\x3A%u", DatiLetti.ContaFreeFall);
  
  indice += sprintf((char*)DatoJson + indice,",\"tx\"\x3A\"%s\"", DatiLetti.TrasmissionChannel);	
  indice += sprintf((char*)DatoJson + indice,",\"t0\"\x3A%d", DatiLetti.Temperatura);
  if(Dati.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON)
  {
    indice += sprintf((char*)DatoJson + indice,",\"tmax\"\x3A%d", DatiLetti.TemperaturaMax);
    indice += sprintf((char*)DatoJson + indice,",\"tmin\"\x3A%d", DatiLetti.TemperaturaMin);
  }  
  //indice += sprintf((char*)DatoJson + indice,",\"as\"\x3A%u", DatiLetti.GpsAssistito);
  indice += sprintf((char*)DatoJson + indice,",\"t1\"\x3A%u", DatiLetti.TempoConnessioneGps);
  indice += sprintf((char*)DatoJson + indice,",\"t2\"\x3A%u", DatiLetti.TempoRegGsm);
  indice += sprintf((char*)DatoJson + indice,",\"t3\"\x3A%u", DatiLetti.TempoModemOn);
  //	indice += sprintf((char*)DatoJson + indice,",\"cc\"\x3A%u", DatiLetti.ContaEventiCRC);
  //	indice += sprintf((char*)DatoJson + indice,",\"cs\"\x3A%u", DatiLetti.ContaEventiStatoRam);
  //	indice += sprintf((char*)DatoJson + indice,",\"mc\"\x3A%u", DatiLetti.MicroActivityCounter/60);	//stampo in minuti
  //	indice += sprintf((char*)DatoJson + indice,",\"mo\"\x3A%u", DatiLetti.ModemActivityCounter/60);	//stampo in minuti
  //	indice += sprintf((char*)DatoJson + indice,",\"re\"\x3A%u", DatiLetti.CounterEepromReset);
  //	indice += sprintf((char*)DatoJson + indice,",\"si\"\x3A%u", DatiLetti.ContaStatoIn);
  //	indice += sprintf((char*)DatoJson + indice,",\"so\"\x3A%u", DatiLetti.ContaStatoOut);
  //	indice += sprintf((char*)DatoJson + indice,",\"mf\"\x3A%u", DatiLetti.CountErroreModemOn );
  indice += sprintf((char*)DatoJson + indice,",\"ag\"\x3A\"%x\"", DatiLetti.GpsRegTemp);
  if(DatiLetti.LastError == 0x00)
  {
    indice += sprintf((char*)DatoJson + indice,",\"rr\"\x3A\"%X\"", DatiLetti.RegRete);
  }
  else
  {
    indice += sprintf((char*)DatoJson + indice,",\"rr\"\x3A\"%X,%X\"", DatiLetti.RegRete, DatiLetti.LastError);
  }
  indice += sprintf((char*)DatoJson + indice,",\"st\"\x3A%u\x7D", DatiLetti.StatoAlesea);
  DatiLetti.InputVariation = 0;	//pulisco il campo in che conteneva il MsgType ricevuto
  DatoJsonDim = indice;
}


MessageTypes MessageType;
//////////////////////////////////////////////////
/////////Gestione dei comandi da portale//////////
//////////////////////////////////////////////////
void GestioneDatoJsonRx(void)
{
  uint32_t indice;
  uint32_t  Minuto;
  
  indice = DecodificaStringa((const uint8_t *)&msgType, sizeof(msgType)-1);
  MessageType = (MessageTypes)(DatoJsonRx[++indice]);
  switch(MessageType)
  {
  case MODEM_SET_PERIODO:
    Dati.InputVariation = MessageType;
    indice = DecodificaStringa((const uint8_t *)&msgMinuto, sizeof(msgMinuto)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Minuto = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }	
    Dati.SchedulingInterval = Minuto*60;	
    if(Dati.SchedulingInterval > LIMITE_SCHEDULING_INTERVAL)	
    {
      Dati.SchedulingInterval = LIMITE_SCHEDULING_INTERVAL;
    }
    EEPROM_WriteWord( EEPROM_TX_TIME1, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord( EEPROM_TX_TIME2, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord( EEPROM_TX_TIME3, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE1, (int32_t)Dati.SchedulingInterval);
    EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE2, (int32_t)Dati.SchedulingInterval);
    EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE3, (int32_t)Dati.SchedulingInterval);
    break;
    
  case MODEM_FACTORY_RESET:
    Dati.InputVariation = MessageType;
    FlagFactoryReset = 1;
    break;
    
  case MODEM_MICRO_RESET:
    Dati.InputVariation = MessageType;
    FlagMicroReset = 1;
    break;
    
  case MODEM_SPIN_RESET:
    Dati.InputVariation = MessageType;
    FlagSpinReset = 1;
    break;
    
  case MODEM_SHOCK_RESET:
    Dati.InputVariation = MessageType;
    FlagShockReset = 1;
    break;
   
  case MODEM_FREE_FALL_RESET:
    Dati.InputVariation = MessageType;
    FlagFreeFallReset = 1;
    break;
    
  case MODEM_DIAGNOSTICA_RESET:
    Dati.InputVariation = MessageType;
    FlagDiagnosticaReset = 1;
    break;
    
  case MODEM_BATTERY_RESET:
    Dati.InputVariation = MessageType;
    FlagBatteryReset = 1;
    break;
    
  case MODEM_GPS_SETUP:
    Dati.InputVariation = MessageType;
    
    indice = DecodificaStringa((const uint8_t *)&msgGpsAss, sizeof(msgGpsAss)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x31) )		//se � un numero tra 0 e 1
    {
      Dati.GpsAssistito = DatoJsonRx[indice] - 0x30;
      EEPROM_WriteByte(EEPROM_STATO_GPS_ASSISTITO, Dati.GpsAssistito);
    }	
    
    indice = DecodificaStringa((const uint8_t *)&msgGpsTime, sizeof(msgGpsTime)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Dati.GpsTimeout = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Dati.GpsTimeout = (Dati.GpsTimeout*10) + (DatoJsonRx[indice] - 0x30);
    }
    
    if( (Dati.GpsTimeout > GPS_TIMEOUT_MAX) || (Dati.GpsTimeout < GPS_TIMEOUT_DEFAULT) ) 	Dati.GpsTimeout = GPS_TIMEOUT_DEFAULT;
    EEPROM_WriteByte(EEPROM_STATO_GPS_TIMEOUT, Dati.GpsTimeout);
    break;
    
  case MODEM_GPS_PRO_SETUP:
    Dati.InputVariation = MessageType;
    
    indice = DecodificaStringa((const uint8_t *)&msgGpsPro, sizeof(msgGpsPro)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x31) )		//se � un numero tra 0 e 1
    {
      Dati.GpsPro = DatoJsonRx[indice] - 0x30;
      EEPROM_WriteByte(EEPROM_GPS_PRO, Dati.GpsPro);
    }	
    
    indice = DecodificaStringa((const uint8_t *)&msgGpsProTime, sizeof(msgGpsProTime)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Dati.GpsProTime = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Dati.GpsProTime = (Dati.GpsProTime*10) + (DatoJsonRx[indice] - 0x30);
    }
    
    if( (Dati.GpsProTime > GPS_PRO_TIME_MAX) || (Dati.GpsProTime < GPS_PRO_TIME_MIN) ) 	Dati.GpsProTime = GPS_PRO_TIME_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_PRO_TIME, Dati.GpsProTime);
    
    
    break;
    
  case MODEM_PROFILO_TEMPERATURA_SETUP:
    Dati.InputVariation = MessageType;
    
    indice = DecodificaStringa((const uint8_t *)&msgProfiloTemp, sizeof(msgProfiloTemp)-1);
    indice++;
    if( DatoJsonRx[indice] == 0x30 )		//se � un numero tra 0 e 1
    {
      Dati.ProfiloTemperaturaOn = PROFILO_TEMPERATURA_OFF;
      EEPROM_WriteByte(EEPROM_PROFILO_TEMPERATURA_ON, Dati.ProfiloTemperaturaOn);
    }
    else if( DatoJsonRx[indice] == 0x31 )
    {
      Dati.ProfiloTemperaturaOn = PROFILO_TEMPERATURA_ON;
      EEPROM_WriteByte(EEPROM_PROFILO_TEMPERATURA_ON, Dati.ProfiloTemperaturaOn);
    }
    
    
    break;
    
  case MODEM_ROTAZIONI_ORIZZ_ON:
    Dati.InputVariation = MessageType;
    
    indice = DecodificaStringa((const uint8_t *)&msgRotazOrizz, sizeof(msgRotazOrizz)-1);
    indice++;
    if( DatoJsonRx[indice] == 0x30 )		//se � un numero tra 0 e 1
    {
      Dati.RotazioneOrizzontaleOn = ROTAZIONE_ORIZZ_OFF;
      EEPROM_WriteByte(EEPROM_ROTAZIONE_ORIZZ, Dati.RotazioneOrizzontaleOn);
    }
    else if( DatoJsonRx[indice] == 0x31 )
    {
      Dati.RotazioneOrizzontaleOn = ROTAZIONE_ORIZZ_ON;
      EEPROM_WriteByte(EEPROM_ROTAZIONE_ORIZZ, Dati.RotazioneOrizzontaleOn);
    }
    
    
    break;  
    
  case MODEM_GPS_FREQ_FORCING:
    Dati.InputVariation = MessageType;	
    
    indice = DecodificaStringa((const uint8_t *)&msgGpsFreqForcing, sizeof(msgGpsFreqForcing)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Dati.GpsFreqForcing = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Dati.GpsFreqForcing = (Dati.GpsFreqForcing*10) + (DatoJsonRx[indice] - 0x30);
    }
    
    if( (Dati.GpsFreqForcing > GPS_FREQ_FORCING_MAX) || (Dati.GpsFreqForcing < GPS_FREQ_FORCING_MIN) ) 	Dati.GpsFreqForcing = GPS_FREQ_FORCING_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_FREQ_FORCING, Dati.GpsFreqForcing);
    
    break;
    
  case MODEM_DEACTIVATION:
    Dati.InputVariation = MessageType;
    FlagDisattivazione = 1;
    break;
    
  case MODEM_UPGRADE_FW:
    Dati.InputVariation = MessageType;
    indice = DecodificaStringa((const uint8_t *)&msgVersione, sizeof(msgVersione)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      VersioneFWRX = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      VersioneFWRX = (VersioneFWRX*10) + (DatoJsonRx[indice] - 0x30);
    }
    
    indice = DecodificaStringa((const uint8_t *)&msgSottoversione, sizeof(msgSottoversione)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      SottoversioneFWRX = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      SottoversioneFWRX = (SottoversioneFWRX*10) + (DatoJsonRx[indice] - 0x30);
    }
    
    if( (VersioneFWRX > VersioneFW) || ((VersioneFWRX == VersioneFW) && (SottoversioneFWRX > SottoversioneFW)) )		
    {
      FlagUpgradeFw = 1;
      
    }		
    else	
    {
      FlagUpgradeFw = 0;
    }
    break;
    
    case MODEM_SET_SOGLIA_MOV:
    Dati.InputVariation = MessageType;
   
    indice = DecodificaStringa((const uint8_t *)&msgSogliaAcc, sizeof(msgSogliaAcc)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Dati.SogliaMovimento = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Dati.SogliaMovimento = (Dati.SogliaMovimento*10) + (DatoJsonRx[indice] - 0x30);
    }
    
    if( (Dati.SogliaMovimento > SOGLIA_MOV_MAX) || (Dati.SogliaMovimento < SOGLIA_MOV_MIN) ) 	Dati.SogliaMovimento = SOGLIA_MOV_DEFAULT;
    EEPROM_WriteByte(EEPROM_SOGLIA_MOV, Dati.SogliaMovimento);
   
    break;
    
    case MODEM_SET_PERIODO_MOVIMENTO:
    Dati.InputVariation = MessageType;
    indice = DecodificaStringa((const uint8_t *)&msgMinuto, sizeof(msgMinuto)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Minuto = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Minuto = (Minuto*10) + (DatoJsonRx[indice] - 0x30);
    }	
    Dati.MoveSchInterval = Minuto*60;	
    if( (Dati.MoveSchInterval < MOVE_SCH_INTERVAL_MIN) || (Dati.MoveSchInterval > MOVE_SCH_INTERVAL_MAX) )	
    {
      Dati.MoveSchInterval = MOVE_SCH_INTERVAL_DEFAULT;
    }
    EEPROM_WriteWord( EEPROM_MOVE_SCH_INTERVAL, (int32_t)Dati.MoveSchInterval );

    break;
    
    case MODEM_SET_SOGLIA_SHOCK:
    Dati.InputVariation = MessageType;
   
    indice = DecodificaStringa((const uint8_t *)&msgSogliaAcc, sizeof(msgSogliaAcc)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Dati.SogliaShock = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Dati.SogliaShock = (Dati.SogliaShock*10) + (DatoJsonRx[indice] - 0x30);
    }
    
    if( (Dati.SogliaShock > SOGLIA_SHOCK_MAX) || (Dati.SogliaShock < SOGLIA_SHOCK_MIN) ) 	Dati.SogliaShock = SOGLIA_SHOCK_DEFAULT;
    EEPROM_WriteByte(EEPROM_SOGLIA_SHOCK, Dati.SogliaShock);
   
    break;
    
    case MODEM_SET_GNSS_SATELLITI:
    Dati.InputVariation = MessageType;
    indice = DecodificaStringa((const uint8_t *)&msgGnssSatelliti, sizeof(msgGnssSatelliti)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Dati.GpsSatelliti = DatoJsonRx[indice] - 0x30;
    }
    FlagSetGnssSatelliti = 1;
    break;
    
    case MODEM_SET_TIMEOUT_SPIN:
    Dati.InputVariation = MessageType;
   
    indice = DecodificaStringa((const uint8_t *)&msgTimeoutSpin, sizeof(msgTimeoutSpin)-1);
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero...
    {
      Dati.FiltroMovimento = DatoJsonRx[indice] - 0x30;
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Dati.FiltroMovimento = (Dati.FiltroMovimento*10) + (DatoJsonRx[indice] - 0x30);
    }
    indice++;
    if( (DatoJsonRx[indice] >= 0x30) && (DatoJsonRx[indice] <= 0x39) )		//se � un numero....
    {
      Dati.FiltroMovimento = (Dati.FiltroMovimento*10) + (DatoJsonRx[indice] - 0x30);
    }
    
    if( (Dati.FiltroMovimento > FILTRO_QUADRANTE_FERMO_MAX) || (Dati.FiltroMovimento < FILTRO_QUADRANTE_FERMO_MIN) ) 	Dati.FiltroMovimento = FILTRO_QUADRANTE_FERMO_DEFAULT;
    EEPROM_WriteWord(EEPROM_FILTRO_QUADRANTE_FERMO, Dati.FiltroMovimento);
   
    break;
    
    
    
  default:
    Dati.InputVariation = 'x';
    break;
  }
}


//////////////////////////////////////////////////
///////////////Pulizia buffer JSON//////////////
//////////////////////////////////////////////////
void PulisciDatoJsonRx(void)
{
  for(uint32_t i = 0 ; i < JSON_MAX_BUFFER_DATA_RX; i++)
  {
    DatoJsonRx[i] = 0;
  }
}


///////////////////////////////////////////////////////////////////////
//////ricerca parola chiave all'interno del buffer di ricezione////////
///////////////////////////////////////////////////////////////////////
uint32_t DecodificaStringa(const uint8_t*  Stringa, uint8_t dim)
{	
  //scansione intero buffer per ricerca primo carattere
  uint32_t Result = 0;
  
  for( uint32_t k = 0; k < JSON_MAX_BUFFER_DATA_RX; k++)
  {
    if(DatoJsonRx[k] == *Stringa)
    {
      //scansione delle posizioni successive per ricerca corrispondenza intera stringa
      for(uint32_t t = 0; t <= dim; t++)
      {
        if(DatoJsonRx[(k+t)] != *(Stringa + t))
        {
          break;
        }				
        else if((t+1) == dim)
        {
          Result = (k+t);	//trovata corrispondenza su tutta la stringa
        }
      }
    }
  }
  return Result;
}







