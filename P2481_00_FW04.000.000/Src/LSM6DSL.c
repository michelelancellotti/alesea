#include "main.h"
#include "Gestione_dato.h"
#include "i2c.h"
#include "EEPROM.h"

/* USER CODE BEGIN PM */
static axis3bit16_t data_raw_acceleration;
static axis3bit16_t data_raw_angular_rate;
static axis1bit16_t data_raw_temperature;
float acceleration_mg[3];
float angular_rate_mdps[3];
static uint8_t whoamI, rst;
float NumeroGiriGyr;
uint8_t count_init_LSM;

uint32_t LSM6DSL_interrupt_timer;
uint32_t lsm6dsl_angular_fail_count;
uint32_t lsm6dsl_acceleration_fail_count;
uint32_t lsm6dsl_temperature_fail_count;
uint8_t FlagDebugGyro, FlagDebugAcc, FlagDebugTemp;

uint8_t flag_lsm6dsl_ignore_int;
uint8_t timer_flag_lsm6dsl_ignore_int;

uint8_t ff_duration;
lsm6dsl_ff_ths_t ff_soglia;

static int32_t platform_write(void *handle, uint8_t Reg, uint8_t *Bufp,
                              uint16_t len)
{
  HAL_StatusTypeDef result;
  if (handle == &hi2c1)
  {
    result = HAL_I2C_Mem_Write(handle, LSM6DSL_I2C_ADD_L, Reg,
                               I2C_MEMADD_SIZE_8BIT, Bufp, len, 100/*1000*/);
  }
  return result;
}

static int32_t platform_read(void *handle, uint8_t Reg, uint8_t *Bufp,
                             uint16_t len)
{
  HAL_StatusTypeDef result;
  if (handle == &hi2c1)
  {
    result = HAL_I2C_Mem_Read(handle, LSM6DSL_I2C_ADD_L, Reg,
                              I2C_MEMADD_SIZE_8BIT, Bufp, len, 100/*1000*/);
  } 
  return result;
}

/*
 *  Function to print messages
 */
void tx_com( uint8_t *tx_buffer, uint16_t len )
{
  #ifdef NUCLEO_STM32F411RE  
  HAL_UART_Transmit( &huart2, tx_buffer, len, 1000 );
  #endif
  #ifdef MKI109V2  
  CDC_Transmit_FS( tx_buffer, len );
  #endif
}


  
void InitLSM6DSL(void)
{
  /*
  *  Initialize mems driver interface
  */
  
  /*
  *  Check device ID
  */
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  
  whoamI = 0;
  
  do {
    lsm6dsl_device_id_get(&dev_ctx, &whoamI);
    count_init_LSM++;
    HAL_Delay(100);
  } while (whoamI != LSM6DSL_ID);
  
  //lsm6dsl_device_id_get(&dev_ctx, &whoamI);
  //if ( whoamI != LSM6DSL_ID )
  //  while(1); /*manage here device not found */
  /*
  *  Restore default configuration
  */
  lsm6dsl_reset_set(&dev_ctx, PROPERTY_ENABLE);
  do {
    lsm6dsl_reset_get(&dev_ctx, &rst);
  } while (rst);
  /*
  *  Enable Block Data Update
  */
  lsm6dsl_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);
  
  //Abilitate pull-up interne su I2C MASTER (risolve il problema consumo anomalo dovuto ai pin I2C flottanti)
  lsm6dsl_sh_pin_mode_set(&dev_ctx,LSM6DSL_INTERNAL_PULL_UP);
  //
  
  //enable low power
  lsm6dsl_xl_power_mode_set(&dev_ctx, LSM6DSL_XL_NORMAL);
  
  
  /*
  * Set Output Data Rate
  */
  
  
  lsm6dsl_xl_data_rate_set(&dev_ctx, LSM6DSL_XL_ODR_12Hz5);
  lsm6dsl_gy_data_rate_set(&dev_ctx, LSM6DSL_GY_ODR_OFF);
  /*
  * Set full scale
  */ 
  
  lsm6dsl_xl_full_scale_set(&dev_ctx, LSM6DSL_4g);
  lsm6dsl_gy_full_scale_set(&dev_ctx, LSM6DSL_2000dps);
  
  //lsm6dsl_xl_hp_path_internal_set(&dev_ctx, LSM6DSL_USE_HPF);
  /*
  * Apply high-pass digital filter on Wake-Up function
  * Duration time is set to zero so Wake-Up interrupt signal
  * is generated for each X,Y,Z filtered data exceeding the
  * configured threshold
  */
  
  /*
  * Configure filtering chain(No aux interface)
  */  
  /* Accelerometer - analog filter */
  lsm6dsl_xl_filter_analog_set(&dev_ctx, LSM6DSL_XL_ANA_BW_1k5Hz/*LSM6DSL_XL_ANA_BW_400Hz*/);
  
  /* Accelerometer - LPF1 path ( LPF2 not used )*/
  //lsm6dsl_xl_lp1_bandwidth_set(&dev_ctx, LSM6DSL_XL_LP1_ODR_DIV_4);
  
  /* Accelerometer - LPF1 + LPF2 path */   
  //lsm6dsl_xl_lp2_bandwidth_set(&dev_ctx, LSM6DSL_XL_LOW_NOISE_LP_ODR_DIV_400);
  
  /* Accelerometer - High Pass / Slope path */
  //lsm6dsl_xl_reference_mode_set(&dev_ctx, PROPERTY_DISABLE);
  //lsm6dsl_xl_hp_bandwidth_set(&dev_ctx, LSM6DSL_XL_HP_ODR_DIV_100);
  
  /* Gyroscope - filtering chain */
  lsm6dsl_gy_band_pass_set(&dev_ctx, LSM6DSL_HP_DISABLE_LP1_LIGHT/*LSM6DSL_HP_260mHz_LP1_STRONG*/);
  
  /*
  * Read samples in polling mode (no int)
  */
}


void SetAccRunFreq(void)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_xl_data_rate_set(&dev_ctx, LSM6DSL_XL_ODR_416Hz); 
  
  flag_lsm6dsl_ignore_int = 1;
  timer_flag_lsm6dsl_ignore_int = 10;
}

void SetAccStopFreq(void)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_xl_data_rate_set(&dev_ctx, LSM6DSL_XL_ODR_12Hz5);
  
  flag_lsm6dsl_ignore_int = 1;
  timer_flag_lsm6dsl_ignore_int = 10;
  
}

void SetInterruptAngolo(void)
{
  lsm6dsl_ctx_t dev_ctx;
  lsm6dsl_int1_route_t int_1_reg;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  //angolo
  lsm6dsl_6d_threshold_set(&dev_ctx, LSM6DSL_DEG_50);
  lsm6dsl_6d_feed_data_set(&dev_ctx, LSM6DSL_LPF2_FEED);
  lsm6dsl_pin_int1_route_get(&dev_ctx, &int_1_reg);
  int_1_reg.int1_6d = PROPERTY_ENABLE;
  lsm6dsl_pin_int1_route_set(&dev_ctx, int_1_reg);
  
  flag_lsm6dsl_ignore_int = 1;
  timer_flag_lsm6dsl_ignore_int = 10;
}

void SetInterruptShock(void)
{
  lsm6dsl_ctx_t dev_ctx;
  lsm6dsl_int2_route_t int_2_reg;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_wkup_dur_set(&dev_ctx, 0);
  /*
  * Set Wake-Up threshold: 1 LSb corresponds to FS_XL/2^6
  */
  lsm6dsl_wkup_threshold_set(&dev_ctx, 64);		//0.15g //FS*N/64
  /*
  * Enable interrupt generation on Wake-Up INT1 pin
  */
  lsm6dsl_pin_int2_route_get(&dev_ctx, &int_2_reg);
  int_2_reg.int2_wu = PROPERTY_ENABLE;
  lsm6dsl_pin_int2_route_set(&dev_ctx, int_2_reg);
  
  flag_lsm6dsl_ignore_int = 1;
  timer_flag_lsm6dsl_ignore_int = 10;
}




float LSM6DSL_cycle(void)
{		
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	  
  
  lsm6dsl_reg_t reg;
  lsm6dsl_status_reg_get(&dev_ctx, &reg.status_reg);
  
  if (reg.status_reg.gda)
  {
    lsm6dsl_angular_rate_raw_get(&dev_ctx, data_raw_angular_rate.u8bit);
    angular_rate_mdps[2] = LSM6DSL_FROM_FS_2000dps_TO_mdps(data_raw_angular_rate.i16bit[2]);
  } 
  else
  {
    lsm6dsl_angular_fail_count++;
    if(lsm6dsl_angular_fail_count > FILTRO_FAILURE_I2C)
    {
      MX_I2C1_Init();
      lsm6dsl_angular_fail_count = 0;
      FlagDebugGyro++;
    }
  }
  
  if (reg.status_reg.xlda)
  {
    lsm6dsl_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
    acceleration_mg[0] = LSM6DSL_FROM_FS_4g_TO_mg( data_raw_acceleration.i16bit[0]);
    acceleration_mg[1] = LSM6DSL_FROM_FS_4g_TO_mg( data_raw_acceleration.i16bit[1]);
  } 
  else
  {
    lsm6dsl_acceleration_fail_count++;
    if(lsm6dsl_acceleration_fail_count > FILTRO_FAILURE_I2C)
    {
      MX_I2C1_Init();
      lsm6dsl_acceleration_fail_count = 0;
      FlagDebugAcc++;
    }
  }
  
  
  ///////conteggio numero giri basato su giroscopio///////
  //NumeroGiriGyr = (angular_rate_mdps[2]/3600);	//base 100ms
  NumeroGiriGyr = (angular_rate_mdps[2]);///36000);	//base 10ms
  ////////////////////////////////////////////////////////
  
  
  return NumeroGiriGyr;	
}

float LSM6DSL_Orientamento(void)
{
  float result;
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	  
  
  lsm6dsl_reg_t reg;
  lsm6dsl_status_reg_get(&dev_ctx, &reg.status_reg);
  
  if (reg.status_reg.xlda)
  {   
    lsm6dsl_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
    //acceleration_mg[0] = LSM6DSL_FROM_FS_2g_TO_mg( data_raw_acceleration.i16bit[0]);
    //acceleration_mg[1] = LSM6DSL_FROM_FS_2g_TO_mg( data_raw_acceleration.i16bit[1]);
    result = LSM6DSL_FROM_FS_4g_TO_mg( data_raw_acceleration.i16bit[2]);
  }
  else
  {
    lsm6dsl_acceleration_fail_count++;
    if(lsm6dsl_acceleration_fail_count > FILTRO_FAILURE_I2C)
    {
      MX_I2C1_Init();
      lsm6dsl_acceleration_fail_count = 0;
      FlagDebugAcc++;
    }
  }
  return result;
}

float LSM6DSL_Temperatura(void)
{
  float temperature_degC;
  
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	  
  
  lsm6dsl_reg_t reg;
  lsm6dsl_status_reg_get(&dev_ctx, &reg.status_reg);
  
  if (reg.status_reg.tda)
  {   
    lsm6dsl_temperature_raw_get(&dev_ctx, data_raw_temperature.u8bit);
    temperature_degC = LSM6DSL_FROM_LSB_TO_degC( data_raw_temperature.i16bit );
    lsm6dsl_temperature_fail_count = 0;
  }
  else
  {
    lsm6dsl_temperature_fail_count++;
    if(lsm6dsl_temperature_fail_count > FILTRO_FAILURE_I2C)
    {
      MX_I2C1_Init();
      lsm6dsl_temperature_fail_count = 0;
      FlagDebugTemp++;
    }
    temperature_degC = 0xFF;
  }
  return temperature_degC;
}


void LSM6DSL_GYRO_OFF(void)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	
  lsm6dsl_gy_data_rate_set(&dev_ctx, LSM6DSL_GY_ODR_OFF);
  lsm6dsl_gy_power_mode_set(&dev_ctx, LSM6DSL_GY_NORMAL);	
  
  angular_rate_mdps[0] = 0;
  angular_rate_mdps[1] = 0;
  angular_rate_mdps[2] = 0;
}

void LSM6DSL_GYRO_ON(void)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	
  
  lsm6dsl_gy_data_rate_set(&dev_ctx, LSM6DSL_GY_ODR_1k66Hz/*LSM6DSL_GY_ODR_12Hz5*/);
  lsm6dsl_gy_full_scale_set(&dev_ctx, LSM6DSL_2000dps);
  lsm6dsl_gy_power_mode_set(&dev_ctx, LSM6DSL_GY_HIGH_PERFORMANCE);	
  
}


void SogliaAccelerometroSet(uint8_t val)
{
  lsm6dsl_ctx_t dev_ctx;
  lsm6dsl_int2_route_t int_2_reg;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_wkup_dur_set(&dev_ctx, 0);
  /*
  * Set Wake-Up threshold: 1 LSb corresponds to FS_XL/2^6
  */
  lsm6dsl_wkup_threshold_set(&dev_ctx, val);
  
  /*
  * Enable interrupt generation on Wake-Up INT1 pin
  */
  lsm6dsl_pin_int2_route_get(&dev_ctx, &int_2_reg);
  int_2_reg.int2_wu = PROPERTY_ENABLE;
  lsm6dsl_pin_int2_route_set(&dev_ctx, int_2_reg); 
  
  flag_lsm6dsl_ignore_int = 1;
  timer_flag_lsm6dsl_ignore_int = 10;
}



void SetInterruptFreeFall(uint8_t val)
{
  lsm6dsl_ctx_t dev_ctx;
  lsm6dsl_int2_route_t int_2_reg;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;

  /*
  * Enable interrupt generation on Free fall INT1 pin
  */
  lsm6dsl_pin_int2_route_get(&dev_ctx, &int_2_reg);
  int_2_reg.int2_ff = PROPERTY_ENABLE;
  lsm6dsl_pin_int2_route_set(&dev_ctx, int_2_reg);
  
  ff_duration = val;
  ff_soglia = LSM6DSL_FF_TSH_156mg;
  lsm6dsl_ff_threshold_set(&dev_ctx, ff_soglia);
  lsm6dsl_ff_dur_set(&dev_ctx, ff_duration);
  
    flag_lsm6dsl_ignore_int = 1;
    timer_flag_lsm6dsl_ignore_int = 10;
}



lsm6dsl_all_sources_t lsm6dsl_source;
uint8_t FreeFallSourceRead(void)
{
  lsm6dsl_ctx_t lsm6dsl_mov;
  //lsm6dsl_int2_route_t int_2_reg;
  
  lsm6dsl_mov.write_reg = platform_write;
  lsm6dsl_mov.read_reg = platform_read;
  lsm6dsl_mov.handle = &hi2c1;
  
  lsm6dsl_all_sources_get(&lsm6dsl_mov, &lsm6dsl_source);
  return lsm6dsl_source.reg.wake_up_src.ff_ia;
}

uint8_t WakeUpSourceRead(void)
{
  lsm6dsl_ctx_t lsm6dsl_mov;
  //lsm6dsl_int2_route_t int_2_reg;
  
  lsm6dsl_mov.write_reg = platform_write;
  lsm6dsl_mov.read_reg = platform_read;
  lsm6dsl_mov.handle = &hi2c1;
  
  lsm6dsl_all_sources_get(&lsm6dsl_mov, &lsm6dsl_source);
  return lsm6dsl_source.reg.wake_up_src.wu_ia;
}



