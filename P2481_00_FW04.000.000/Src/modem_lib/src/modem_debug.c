/**
******************************************************************************
* File          	: modem_debug.c
* Versione libreria	: 0.01
* Descrizione        	: Debug libreria Modem.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "modem_debug.h"
#include "modem.h"
#include "main.h"
#include "Globals.h"


#ifdef ENB_MODEM_DEBUG
char debug_msg[32];
uint8_t cmd_debug_trigger;
uint8_t dbg_gnss[4];



/* Funzioni locali. */


void modem_debug_trigger(void)
{
  //static uint8_t first_time = 1;
  static uint16_t msg_num;
  int16_t cnt;
  
  /*if(first_time == 1)
  {
    cmd_debug_trigger = 2;
    first_time = 0;
  }*/
  
  if(cmd_debug_trigger == 1)
  {
    /* Reset buffer debug */
    memset(dbg_modem, 0x00, sizeof(dbg_modem));
    dbg_modem_idx = 0;
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 2)
  {
    /* Test modem ON */
    modem_Trigger(TRG_MODEM_ON, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 3)
  {
    /* Test modem OFF */
    //HAL_GPIO_WritePin(GPS_AMPLI_GPIO_Port, GPS_AMPLI_Pin, GPIO_PIN_RESET);	//spegnimento ampli
    modem_Trigger(TRG_MODEM_OFF, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 4)
  {
    /* Test apertura contesto PDP */
    modem_Trigger(TRG_SYNC_RETE, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 5)
  {
    /* Test connessione MQTT */
    modem_Trigger(TRG_CONNETTI_MQTT, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 6)
  {    
    /* Test PUBLISH MQTT */
    cnt = snprintf(debug_msg, sizeof(debug_msg), "TEST MSG %d", ++msg_num);
    if(cnt > 0)
      modem_Trigger(TRG_INVIA_MQTT, (uint8_t*)debug_msg, cnt);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 7)
  {
    /* Test SUBSCRIBE MQTT */
    modem_Trigger(TRG_RICEVI_MQTT, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 8)
  {
    /* Test chiusura connessione */
    modem_Trigger(TRG_CHIUDI_RETE, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 9)
  {
    /* Test GNSS */
    //HAL_GPIO_WritePin(GPS_AMPLI_GPIO_Port, GPS_AMPLI_Pin, GPIO_PIN_SET);	//accensione ampli
    modem_Trigger(TRG_GNSS_ON, dbg_gnss, sizeof(dbg_gnss));
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 10)
  {
    /* Test impostazioni GP */
    modem_Trigger(TRG_GP_SETTINGS, dbg_gnss, 1);
    cmd_debug_trigger = 0;
  }
}
#endif
