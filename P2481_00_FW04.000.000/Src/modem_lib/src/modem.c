/**
******************************************************************************
* File          	: modem.c
* Versione libreria	: 0.01
* Descrizione        	: Supporto Modem (livello applicativo).
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

//### INCLUDE applicazione ###
#include "main.h"
#include "usart.h"
#include <stdlib.h>
#include "modem_utils.h"
#include "Globals.h"
//############################

#include "modem.h"
#include "bg_96.h"


//Handle modem
Mod_t ModemType = MOD_BG95;
void (*modem_Automa)(const uint32_t PERIODO);
void (*modem_ProcessaByte)(uint8_t data);

/* Risorse allocate */
GPTIMER modem_Timer_TO;                         //Timer



//### Risorse applicazione ###
Topic_t CurrentTopic = RELEASE_T;
char* TOPIC_LIST[TOPIC_NUM] = {
  "CMPTC/",
  "CMPTC/",
  "CMPTC/",
};
uint8_t MQTT_TOPIC_OUT[MOD_MAX_TOPIC];
uint8_t MQTT_TOPIC_IN[MOD_MAX_TOPIC];

const uint8_t MODEM_GNSS_TIMEOUT[MODEM_GNSS_TO_NUM] = {         /* T[n]=5s*MODEM_GNSS_TIMEOUT[n] */
  36,   //00 (180s)
  48,   //01 (240s)
  60,	//02 (300s)
  72,	//03 (360s)
  84,	//04 (420s)
  96,	//05 (480s)
  108,	//06 (540s)
  120,	//07 (600s)
  132,	//08 (660s)
  144,	//09 (720s)
  156,	//10 (780s)
  168,	//11 (840s)
  180,	//12 (900s)
  192,	//13 (960s)
  204,	//14 (1020s)
  216,	//15 (1080s)

};

/* Funzioni locali. */
void Modem_Reset_Pin(uint8_t ON);
void Modem_Power_Pin(uint8_t ON);
uint8_t Modem_Read_Status_Pin(void);
void crea_Topic_MQTT(uint8_t topic_IN, uint8_t* HEADER, uint8_t* TAILER);
void dummy_fun_arg0(const uint32_t src);
void dummy_fun_arg1(uint8_t src);


/* Inizializza libreria modem: assegnazione driver seriale e reset GPIO + allocazione risorse. */
ErrorCode_t modem_Init(Mod_t modello, uint8_t versione_HW, uint8_t sottoversione_HW)
{
  ErrorCode_t err = M_NO_ERROR;  

  modem_Automa = dummy_fun_arg0;		
  modem_ProcessaByte = dummy_fun_arg1;
  
  if(modello!=MOD_BG95 && modello!=MOD_BG96)
  {
    if(versione_HW==VERSIONE_HW_DEFAULT_BG96 && sottoversione_HW==SOTTO_VERSIONE_HW_DEFAULT_BG96)
      ModemType = MOD_BG96;
    else
    {
      err = MODEL_INIT_ERROR;
      ModemType = MOD_BG95;   //default	
    }
  }
  else
    ModemType = modello;
    
  //Assegnazione risorse HW 
  mod_Write_seriale = Write_SCI1;
  mod_Drive_power = Modem_Power_Pin;
  mod_Drive_reset = Modem_Reset_Pin;
  mod_Read_status = Modem_Read_Status_Pin;
  //Init parametri salvati
  set_Topic_MQTT(TEST_T);				//TODO: init a valore diverso (applicazione) ?

  //Assegnazione macchina a stati	
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96: 
      modem_Automa = bg96_Automa;
      modem_ProcessaByte = bg96_ProcessaByte;			
      init_bg96(versione_HW, sottoversione_HW);
      break;
    default: err=MODEL_INIT_ERROR; break;
  }	
  
  return err; 
}

void dummy_fun_arg0(const uint32_t src){}
void dummy_fun_arg1(uint8_t src){}
	

/* Pilotaggio pin reset modem. */
void Modem_Reset_Pin(uint8_t ON)
{
  if(ON) HAL_GPIO_WritePin(MODEM_RST_PORT, MODEM_RST_PIN, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(MODEM_RST_PORT, MODEM_RST_PIN, GPIO_PIN_RESET);
}

/* Pilotaggio pin Power modem. */
void Modem_Power_Pin(uint8_t ON)
{
  if(ON) HAL_GPIO_WritePin(MODEM_PWR_PORT, MODEM_PWR_PIN, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(MODEM_PWR_PORT, MODEM_PWR_PIN, GPIO_PIN_RESET);
}

/* Lettura pin Status. */
uint8_t Modem_Read_Status_Pin(void)
{
  if(HAL_GPIO_ReadPin(STATUS_MODEM_GPIO_Port, STATUS_MODEM_Pin) == GPIO_PIN_RESET) return 1;
  else return 0;
}



/* GET versione FW modem.
*	
*@param: resp: buffer risultato
*@param: resp_size: dimensione risultato (19B)
*/	
ModemAppli_t modem_GET_InfoFW(uint8_t* resp, uint16_t* resp_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *resp_size = sizeof(bg96_FW_info);
      memcpy(resp, bg96_FW_info, sizeof(bg96_FW_info));
      
      result = MA_OK;
      break;
    
    default: break;
  }
  
  return result;
}

/* GET tipologia modem [0=BG95,1=BG96].
*	
*@param: resp: risultato (1B)
*/	
ModemAppli_t modem_GET_Type(uint8_t* resp)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *resp = ModemType;
      
      if(bg96_reg.BIT.TYPE_DETECT == 1)
        result = MA_OK;         //Tipologia modulo rilevata
      else
        result = MA_ERROR;      //Tipologia modulo non rilevata (fallback a BG95)
      break;
      
    default: break;
  }
  
  return result;
}
	
/* GET ICCID SIM.
*	
*@param: resp: buffer risultato
*@param: resp_size: dimensione risultato (20B)
*/	
ModemAppli_t modem_GET_ICCID(uint8_t* resp, uint16_t* resp_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *resp_size = sizeof(bg96_ICCID);
      memcpy(resp, bg96_ICCID, sizeof(bg96_ICCID));
      
      if(bg96_reg.BIT.SIM_RILEVATA == 1)
        result = MA_OK;				//SIM rilevata (ritorno valore corrente)
      else
        result = MA_ERROR;			//SIM non rilevata (ritorno ultimo valore disponibile)	
      break;
      
    default: break;
  }
  
  return result;
}

/* GET RSSI rete cellulare.
*	
*@param: resp: risultato (1B)
*/	
ModemAppli_t modem_GET_RSSI(uint8_t* resp)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *resp = bg96_RSSI;
      
      if(bg96_reg.BIT.STATO_REG_RETE == 1)
        result = MA_OK;				//Registrazione rete COMPLETATA
      else
        result = MA_ERROR;			//Registrazione rete  FALLITA (dato non significativo)	
      break;
      
    default: break;
  }
  
  return result;
}

/* GET tempo registrazione rete cellulare [s].
*	
*@param: resp: risultato (1 B)	
*/	
ModemAppli_t modem_GET_TempoRegistrazioneRete(uint8_t* resp, uint16_t* reg_status, uint8_t* last_error)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint8_t res_app;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      res_app = (uint8_t)(bg96_tempo_Registrazione_rete/((uint32_t)1000));
      *resp = res_app;
      *reg_status = bg96_reg.WORD;
      *last_error = bg96_last_error;
      
      if(bg96_reg.BIT.STATO_REG_RETE == 1)
        result = MA_OK;				//Registrazione rete COMPLETATA
      else
        result = MA_ERROR;			//Registrazione rete  FALLITA
      break;
      
    default: break;
  }
  
  return result;
}

/* GET data/ora UTC [yy/mm/dd hh:mm:ss].
*	
*@param: resp: buffer risultato
*@param: resp_size: dimensione risultato (17B)	
*/	
ModemAppli_t modem_GET_DataOra(uint8_t* resp, uint16_t* resp_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      bg96_DataOra_UTC[8] = ' ';		//Sostituisco ',' con ' ' (formattazione)
      *resp_size = sizeof(bg96_DataOra_UTC);
      memcpy(resp, bg96_DataOra_UTC, sizeof(bg96_DataOra_UTC));
      
      if(bg96_reg.BIT.NTP_SYNC == 1)
        result = MA_OK;				//Sincronizzazione NTP COMPLETATA (ritorno valore corrente)
      else
        result = MA_ERROR;			//Sincronizzazione NTP FALLITA (ritorno ultimo valore disponibile)	
      break;
      
    default: break;
  }
  
  return result;
}

/* GET info cella (localizzazione cellulare).
*
*@param: idx_cella: indice cella (0:SERVING-CELL, >0:NEIGHBOUR-CELL)
*@param: tecnologia: risultato (stringa tecnologia corrente, array 10B)
*@param: MCC: risultato codice paese (2B)
*@param: MNC: risultato, codice operatore (2B)
*@param: cell_ID: risultato, ID cella agganciata (4B)
*@param: LAC_TAC: risultato, codice d'area, LAC(GSM) o TAC(LTE)	(2B)
*@param: rx_lev: risultato, potenza segnale RX [dBm] (2B)
*
*@return: 
*MA_OK=indice cella richiesto valido
*MA_ERROR=indice cella richiesto non valido o superiore alle celle rilevate
*MA_INIT_ERROR=modem non gestito
*/	
ModemAppli_t modem_GET_InfoCella(uint8_t idx_cella, uint8_t* tecnologia, uint16_t* MCC, uint16_t* MNC, uint32_t* cell_ID, uint16_t* LAC_TAC, int16_t* rx_lev)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint8_t i, idx=0;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(idx_cella<bg96_num_celle && idx_cella<MOD_MAX_CELL_NUM)
      {
        for(i=0; i<sizeof(bg96_info_cella[idx_cella].tecnologia); i++)
        {
          if(bg96_info_cella[idx_cella].tecnologia[i] != '"')			//Filtro carattere '"'
            tecnologia[idx++] = bg96_info_cella[idx_cella].tecnologia[i];
        }
        *MCC = (uint16_t)(strtoul(bg96_info_cella[idx_cella].MCC, NULL, 10));
        *MNC = (uint16_t)(strtoul(bg96_info_cella[idx_cella].MNC, NULL, 10));
        *cell_ID = (uint32_t)(strtoul(bg96_info_cella[idx_cella].cell_ID, NULL, 16));
        *LAC_TAC = (uint16_t)(strtoul(bg96_info_cella[idx_cella].LAC_TAC, NULL, 16));
        *rx_lev = bg96_info_cella[idx_cella].rx_lev;
        
        result = MA_OK;         //Indice cella richiesto valido
      }
      else
        result = MA_ERROR;      //Indice cella richiesto non valido o superiore alle celle rilevate
      break;
      
    default: break;
  }
  
  return result;
}

/* GET info GNSS (localizzazione GNSS).
*
*@param: latitudine: risultato  (4B)
*@param: longitudine: risultato (4B)
*/
ModemAppli_t modem_GET_InfoGNSS(float* latitudine, float* longitudine)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *latitudine = bg96_latitudine_f;
      *longitudine = bg96_longitudine_f;
      
      if(bg96_reg.BIT.LOC_GNSS == 1)					
        result = MA_OK;				//Localizzazione COMPLETATA (ritorno valore corrente)
      else
        result = MA_ERROR;			//Localizzazione FALLITA (ritorno ultimo valore disponibile)
      break;
      
    default: break;
  }
  
  return result;
}

/* GET info GNSS PRO (localizzazione GNSS precisione aumentata).
*
*@param: latitudine: risultato  (4B)
*@param: longitudine: risultato (4B)
*/
ModemAppli_t modem_GET_InfoGNSS_PRO(float* latitudine, float* longitudine)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *latitudine = bg96_latitudine_f_PRO;
      *longitudine = bg96_longitudine_f_PRO;
      
      if(bg96_gnss_reg.BIT.LOC_GNSS_PRO == 1)					
        result = MA_OK;				//Localizzazione PRO COMPLETATA (ritorno valore corrente)
      else
        result = MA_ERROR;			//Localizzazione PRO FALLITA (ritorno ultimo valore disponibile)
      break;
      
    default: break;
  }
  
  return result;
}

/* GET tempo aggancio GNSS [s].
*	
*@param: resp: risultato (2B)
*@param: status_reg: risultato (2B)
*/
ModemAppli_t modem_GET_TempoGNSS(uint16_t* resp, uint16_t* status_reg)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint16_t res_app;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      res_app = (uint16_t)(bg96_tempo_GNSS/((uint32_t)1000));
      *resp = res_app;
      *status_reg = bg96_gnss_reg.WORD;
      
      if(bg96_reg.BIT.LOC_GNSS == 1)					
        result = MA_OK;				//Localizzazione COMPLETATA (ritorno valore corrente)
      else
        result = MA_ERROR;			//Localizzazione FALLITA (ritorno ultimo valore disponibile)	
      break;
      
    default: break;
  }
  
  return result;
}

/* GET messaggio MQTT ricevuto (SUBSCRIBE).
*	
*@param: resp: buffer risultato
*@param: resp_size: dimensione risultato (N bytes, max MOD_MAX_BUFFER_DATA_RX bytes)	
*/	
ModemAppli_t modem_GET_MsgMQTT(uint8_t* resp, uint16_t* resp_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(bg96_reg.BIT.MQTT_MSG_RX == 1)
      {
        //Messaggio MQTT ricevuto
        *resp_size = bg96_MQTT_RX_size;
        memcpy(resp, mod_buff_data_RX, bg96_MQTT_RX_size);
        result = MA_OK;
      }
      else
      {
        *resp_size = 0;
        result = MA_ERROR;		//Messaggio MQTT non ricevuto o non disponibile
      }
      break;
      
    default: break;
  }
  
  return result;
}


//#################### Applicazione ####################

/* Imposta TOPIC MQTT (da lista TOPIC). */
void set_Topic_MQTT(Topic_t topic)
{
  if(topic < TOPIC_NUM)
  {
    CurrentTopic = topic;		
  }
}

void crea_Topic_MQTT(uint8_t topic_IN, uint8_t* HEADER, uint8_t* TAILER)
{
  uint16_t offset=0, size;
  uint8_t* topic_ptr;	
  
  if(topic_IN) topic_ptr=MQTT_TOPIC_IN;
  else topic_ptr=MQTT_TOPIC_OUT;
  
  memset(topic_ptr, 0x00, MOD_MAX_TOPIC); 
  offset += concatenaStringhe(topic_ptr, offset, HEADER, MOD_MAX_TOPIC);
  if((offset+MOD_ICCID_MAX_SIZE) <= MOD_MAX_TOPIC)
  {
    modem_GET_ICCID(&topic_ptr[offset], &size); 
    offset += size;
  }
  offset += concatenaStringhe(topic_ptr, offset, TAILER, MOD_MAX_TOPIC);
}


/* Trigger azioni modem. */
ModemAppli_t modem_Trigger(ModemTrigger_t trigger, uint8_t* data, uint16_t data_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(trigger==TRG_MODEM_ON) { set_bg96_status(BG96_PWR_ON_S); result=MA_TRIG_OK; }			//Trigger asincrono
      else if(trigger==TRG_MODEM_OFF) { set_bg96_status(BG96_PWR_OFF_S); result=MA_TRIG_OK; }		//Trigger asincrono
      else if(trigger==TRG_MODEM_ON_RESET) { set_bg96_status(BG96_PWR_ON_RESET_S); result=MA_TRIG_OK; } //Trigger asincrono
      else if(get_bg96_status() == BG96_IDLE_S)
      {
        //Trigger sincroni
        switch(trigger)
        {
          case TRG_GNSS_ON:
            bg96_gnss_reg.WORD &= 0xE0FF;         //Reset bit opzioni impostabili
            if(data)      //Check 0 parametro : esiste						
            {
              //Modalit� GNSS (B1:b0)
              if(data[0]==GNSS_ASS) bg96_gnss_reg.BIT.ASS_OPT=1;					
              else bg96_gnss_reg.BIT.ASS_OPT=0;
              //Timeout GNSS
              if(data[1]>=MODEM_GNSS_TO_NUM) bg96_timeout_GNSS=MODEM_GNSS_TIMEOUT[0];			//Check 1 parametro : entro i limiti
              else bg96_timeout_GNSS=MODEM_GNSS_TIMEOUT[data[1]];
              //Precisione aumentata (B1:b1)
              if(data[2]==GNSS_PRO_PREC) 
              {
                bg96_gnss_reg.BIT.PRO_OPT=1;
                //Timeout GNSS PRO
                if(data[3]<MODEM_GNSS_PRO_TO_MIN || data[3]>MODEM_GNSS_PRO_TO_MAX) bg96_timeout_GNSS_PRO=MODEM_GNSS_PRO_TO_MIN*MODEM_GNSS_PRO_TO_CONV;     //Check 2 parametro : entro i limiti
                else bg96_timeout_GNSS_PRO = data[3]*MODEM_GNSS_PRO_TO_CONV;
              }
              else bg96_gnss_reg.BIT.PRO_OPT=0;
            }
            /* --- Rimosso supporto per GPS assistito (forzo GPS standard) ---
            if(bg96_gnss_reg.BIT.ASS_OPT) set_bg96_status(BG96_GNSS_ASSISTANCE_S);
            else set_bg96_status(BG96_GNSS_STANDARD_S);*/
            bg96_gnss_reg.BIT.ASS_OPT = 0;
            set_bg96_status(BG96_GNSS_ON_S);						
            result = MA_TRIG_OK;
            break;
            
          case TRG_SYNC_RETE:
            set_bg96_status(BG96_PDP_ON_S);
            result = MA_TRIG_OK;
            break;
            
          case TRG_CONNETTI_MQTT:
            set_bg96_status(BG96_MQTT_CONNECT_S);
            result = MA_TRIG_OK;
            break;
            
          case TRG_INVIA_MQTT:
            if(data && data_size>0)
            {
              if(data_size > sizeof(mod_buff_data_TX))
              {
                data_size = sizeof(mod_buff_data_TX); 
              }
              crea_Topic_MQTT(1, (uint8_t*)TOPIC_LIST[CurrentTopic], (uint8_t*)"/IN");
              crea_Topic_MQTT(0, (uint8_t*)TOPIC_LIST[CurrentTopic], (uint8_t*)"/OUT");
              
              memset(mod_buff_data_TX, 0x00, sizeof(mod_buff_data_TX));
              memcpy(mod_buff_data_TX, data, data_size);
              set_bg96_status(BG96_MQTT_PUBLISH_S);
              result = MA_TRIG_OK;
            }
            break;
            
          case TRG_RICEVI_MQTT:
            crea_Topic_MQTT(1, (uint8_t*)TOPIC_LIST[CurrentTopic], (uint8_t*)"/IN");
            crea_Topic_MQTT(0, (uint8_t*)TOPIC_LIST[CurrentTopic], (uint8_t*)"/OUT");	
            
            set_bg96_status(BG96_MQTT_SUBSCRIBE_S);						
            result = MA_TRIG_OK;
            break;
            
          case TRG_FW_UPDATE:
            if(data)
            {
              if(data[0]<10 && data[1]<100)		//Ammessi solo: versione (1 cifra) e sottoversione (2 cifre)
              {								
                bg96_FTP_version = data[0];
                bg96_FTP_subversion = data[1];
                set_bg96_status(BG96_FTP_S);
                result = MA_TRIG_OK;
              }
            }
            break;
            
          case TRG_CHIUDI_RETE:
            set_bg96_status(BG96_CLOSE_CONN_S);
            result = MA_TRIG_OK;
            break;
            
          case TRG_GP_SETTINGS:
            if(!data){ bg96_gnss_constellation=BG95_GNSS_CONST_DFLT; }                                  //Check 0 parametro : esiste
            else
            {
              //Costellazione GNSS (eseguito solo su BG95)
              if((BG95_GNSS_Const_t)data[0]<GPS_GLONASS || (BG95_GNSS_Const_t)data[0]>MCC_BASED)        //Check 1 parametro : entro i limiti 
                bg96_gnss_constellation = BG95_GNSS_CONST_DFLT;          
              else 
                bg96_gnss_constellation = (BG95_GNSS_Const_t)data[0];
            }
            set_bg96_status(BG96_GP_SETTINGS_S);
            result = MA_TRIG_OK;
            break;
            
          default: break; 
          }
        }
        else
          result = MA_BUSY;				//BUSY (modulo impegnato)
      break;
      
    default: break;	
  }
  
  return result;
}

/* GET stato operazione richiesta a modem. */
ModemAppli_t modem_GET_Status(ModemTrigger_t operazione)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint8_t status;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      status = get_bg96_status();
      if(status==BG96_PWR_OFF_S || status==BG96_STDBY_S)
      {
        if(bg96_PWR==PWR_OFF) result=MA_OK;		//--- Spegnimento completato ---
        else result=MA_BUSY;				//--- Spegnimento in corso ---
      }
      else if(status != BG96_IDLE_S) 
        result = MA_BUSY;				//--- Operazione in corso --- 
      else 
      {
        //Modulo in stato IDLE
        if(bg96_reg.BIT.PWR_ON_FAILURE)
          result = MA_POWER_ON_FAIL;			//--- Failure accensione modem ---
        else if(bg96_reg.BIT.GENERIC_ERROR)
          result = MA_UNP_ERROR;			//--- Operazione completata: errore inatteso ---
        else
        {
          //--- Operazione completata: OK o errore gestito ---
          result = MA_OK;
          switch(operazione)
          {
            case TRG_MODEM_ON:
            case TRG_MODEM_ON_RESET:
              if(!bg96_reg.BIT.SIM_RILEVATA) result=MA_ERROR;			//SIM non rilevata
              break;
            case TRG_GNSS_ON:
              if(!bg96_reg.BIT.LOC_GNSS) result=MA_ERROR;			//Localizzazione GNSS fallita
              break;
            case TRG_SYNC_RETE:
              if(bg96_reg.BIT.PDP_ERROR || bg96_reg.BIT.STATO_REG_RETE==0) 
                result=MA_ERROR;						//Errore apertura contesto PDP o registrazione rete
              break;
            case TRG_CONNETTI_MQTT:
              if(bg96_reg.BIT.MQTT_ERROR_CONN || bg96_reg.BIT.STATO_REG_RETE==0) 
                result=MA_NETWORK_ERROR;					//Errore connessione MQTT o registrazione rete
              break;
            case TRG_INVIA_MQTT:
              if(bg96_reg.BIT.MQTT_ERROR_PUB) result=MA_NETWORK_ERROR;		//Publish MQTT fallita
              break;
            case TRG_RICEVI_MQTT:
              if(!bg96_reg.BIT.MQTT_MSG_RX) result=MA_ERROR;			//Subscribe MQTT fallita: errore o messaggio non presente
              break;
            case TRG_FW_UPDATE:
              if(bg96_reg.BIT.FTP_ERROR) result=MA_ERROR;			//Connessione FTP fallita
              break;
            case TRG_MODEM_OFF:
              result = MA_ERROR;						//Non � stato triggerato lo spegnimento	
              break;
            default: break;
          }
        }
      }				
      break;
      
    default: break;
  }
  
  return result; 
}

/* Gestione LED segnalazione Modem (ogni 100ms):
*/
void led_segnalazione_Modem(void)
{
  uint8_t STATO_PIN;
  static uint16_t led_cnt = 0;
  BG96_State_t stato_modem;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      //### STATUS (riportato su LED3) ###
      STATO_PIN = !HAL_GPIO_ReadPin(STATUS_MODEM_GPIO_Port, STATUS_MODEM_Pin);
      HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, (GPIO_PinState)STATO_PIN);
      //### NETLIGTH (riportato su LED2) ###			
      STATO_PIN = !HAL_GPIO_ReadPin(NETLIGHT_GPIO_Port, NETLIGHT_Pin);
      HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, (GPIO_PinState)STATO_PIN);
      //### DEBUG ###
      stato_modem = get_bg96_status();
      switch(stato_modem)
      {
        case BG96_STDBY_S:
        case BG96_IDLE_S:																																				
          if(bg96_reg.BIT.GENERIC_ERROR)
            HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);		//Segnalazione ERRORE (LED SPENTO)
          else
          {
            if(++led_cnt >= 5)
            {
              led_cnt = 0;
              HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);				//Segnalazione IDLE/STDBY (LED LAMPEGGIO)
            }
          }
          break;
        
        default:
          HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);			//Segnalazione altri stati (LED ACCESO)
          break;							
      }
      break;
    
    default: break;
  }
}
