/**
******************************************************************************
* File          	: modem_utils.c
* Versione libreria	: 0.01
* Descrizione        	: Funzioni di appoggio libreria.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


#include "modem_utils.h"


Field_t searchField_res[MAX_SEARCH_FIELD_RES];



/* Cerca una stringa all'interno di un buffer per tutta la lunghezza:
* trovata l'occorrenza restituisce la posizione del primo carattere dopo l'occorrenza.
*
*@param buffer, buffer di ricerca
*@param string, stringa (marker) da ricercare nel buffer
*@param start, offset da applicare al buffer 
*@param end, indice finale del buffer in cui ricercare il marker
*@param offset, posizione primo carattere dopo l'occorrenza (N.B. relativa a 'start')
*@param caseSensistive, se TRUE valuta il case dei caratteri
*@param checkSpace, se TRUE valuta gli spazi nella stringa da ricercare
*
*@return: TRUE se trova l'occorrenza, FALSE altrimenti
*/
uint8_t searchString(uint8_t* buffer, uint8_t* string, uint16_t start, uint16_t end, uint16_t* offset, uint8_t caseSensitive, uint8_t checkSpace)
{
  uint16_t i, ci, markerLenght, bufferSize;
  uint8_t firstMarker, FirstMarker;
  uint8_t result = 0;
  uint8_t* src;
  
  ci = 0;
  while(checkSpace==0 && string[ci]==' ' && string[ci]!=0) ci++;  //salta il carattere spazio nella stringa da ricercare
  firstMarker = string[ci];  
  
  //Determina la lunghezza del buffer
  if(end > start)
    bufferSize = end - start;
  else
    bufferSize = 0;  
  
  if(caseSensitive == 0)
    FirstMarker = caseReverse(firstMarker);     //No case sensitive
  else 
    FirstMarker = firstMarker;  		//Case sensitive
  
  for(i=ci; i<0xFFFF; i++)
    if(string[i]==0) break;
  
  markerLenght = i;				//Lunghezza del marker da cercare
  
  
  *offset = 0;
  src = buffer + start;                         //Puntatore ad inizio buffer di ricerca  
  
  if(markerLenght > bufferSize) 
    return(0);
  
  for(i=0; i<bufferSize; i++)
  {
    if(checkSpace==0 && src[i]==' ') continue;  //salta il carattere spazio  nella stringa
    
    if(src[i]==firstMarker || src[i]==FirstMarker)  //rilevamento primo carattere
    {
      result = 1;
      for(i=i+1,ci=1 ;ci<markerLenght; ci++,i++)
      {
        if(checkSpace==0 &&  string[ci]==' '){i--; continue;}       //salta il carattere spazio nella stringa
        if(checkSpace==0 &&  src[i]==' '){ci--; continue;}          //salta il carattere spazio nella stringa
        
        if(string[ci] != src[i])
        {
          //Verifica del case sensitive
          if(caseSensitive==0 && caseReverse(string[ci])==src[i]) continue;
          
          result = 0;
          break;
        };		
      }
      if(result) break;  //Trovata occorrenza
    }
  }	
  *offset = i;
  return(result);	
}


/* Cerca campi all'interno di una stringa. 
*
*@param buffer, buffer di ricerca
*@param field_limit, carattere limitatore di campo
*@param start, offset da applicare al buffer 
*@param end, indice finale del buffer in cui ricercare il marker
*
*@return: numero di campi trovati
*/
uint8_t searchField(uint8_t* buffer, uint8_t field_limit, uint16_t start, uint16_t end)
{
  uint16_t i,sub_i, bufferSize;  
  uint8_t result = 0;
  uint8_t* src;  
  
  //Determina la lunghezza del buffer
  if(end > start)
    bufferSize = end - start;
  else
    bufferSize = 0;      
  
  memset(&searchField_res, 0x00, sizeof(searchField_res));
  src = buffer + start;                         		//Puntatore ad inizio buffer di ricerca
  sub_i = 0;
  
  for(i=0; i<bufferSize && result<MAX_SEARCH_FIELD_RES; i++)
  {
    if(src[i] == field_limit)
    {
      //Trovato delimitatore fine campo
      searchField_res[result].idx = start+i;			//Salvo posizione ASSOLUTA delimitatore corrente
      sub_i = 0;						//Reset sotto-indice campo
      result++;			     
    }
    else
    {
      //Popolo i campi risultato
      if(sub_i<MAX_FIELD_SIZE)
        searchField_res[result].field[sub_i++] = src[i];
    }
  }	
  return(result);
}


/* Restituisce il carattere con case opposto (se esiste). */
uint8_t caseReverse(uint8_t car)
{
  if(car>='A' && car<='Z') {car+=32; return(car);}
  if(car>='a' && car<='z') car-=32; 
  return(car);  
}


/* Converte stringa in ingresso in numero (espresso su byte). */
unsigned char convertStrToByte(unsigned char* str, uint16_t start, uint16_t end)
{
  unsigned char i,k,pow;
  unsigned char number[3+1];
  int value = 0;	
  uint16_t bufferSize;
  
  memset(number, 0x00, sizeof(number));
  //Determina la lunghezza del buffer
  if(end > start)
    bufferSize = end - start;
  else
    bufferSize = 0;
  
  //Parsing della stringa in ingresso
  for(i=0,k=0; i<bufferSize; i++)
  {
    if(str[i]>='0' && str[i]<='9')
    {
      number[k] = str[i]; 
      k++;
    }
    else if(k>0)
      break;
    
    if(str[i]==0) break;	//Fine stringa rilevato (EOF)
    if(k>=3) break;		//Dimensione massima raggiunta
  }
  number[k] = 0;
  
  //Conversione del byte da stringa a numerico
  if(k > 0)
  {
    pow = 1;
    
    for(i=k; i>0; i--)
    {
      value += (number[i-1]-0x30)*pow;
      pow *= 10;
    }
    if(value>255) value=255;  //Saturazione				
  }
  return((unsigned char)value);
}
/* Converte stringa in ingresso in numero (espresso su uint16_t). */
uint16_t convertStrToInt(unsigned char* str, uint16_t start, uint16_t end)
{
  unsigned char i,k;
  unsigned char number[5+1];
  uint32_t value=0, pow;
  uint16_t bufferSize;
  
  memset(number, 0x00, sizeof(number));
  //Determina la lunghezza del buffer
  if(end > start)
    bufferSize = end - start;
  else
    bufferSize = 0;
  
  //Parsing della stringa in ingresso
  
  for(i=0,k=0; i<bufferSize; i++)
  {
    if(str[i]>='0' && str[i]<='9')
    {
      number[k] = str[i]; 
      k++;
    }
    else if(k>0)
      break;
    
    if(str[i]==0) break;	//Fine stringa rilevato (EOF)
    if(k>=5) break;		//Dimensione massima raggiunta
  }
  number[k] = 0;
  
  //Conversione del byte da stringa a numerico
  if(k > 0)
  {
    pow = 1;
    
    for(i=k; i>0; i--)
    {
      value += (number[i-1]-0x30)*pow;
      pow *= 10;
    }
    if(value>65535) value=65535;  //Saturazione				
  }
  return((uint16_t)value);
}


/* Esegue concatenazione della stringa IN_2 sulla stringa IN_1. 
* La stringa IN_2 si ritiene terminata in corrispondenza del terminatore di stringa.
*
*@param: in_1 (stringa 1)
*@param: offset_in_1 (offset start concatenazione su stringa 1)
*@param: in_2 (stringa 2)
*@param: max_length_in_1 (limite lunghezza stringa 1)
*@return: lunghezza stringa IN_2 concatenata  
*/
uint16_t concatenaStringhe(uint8_t* in_1, uint16_t offset_in_1, uint8_t* in_2, uint16_t max_length_in_1)
{
  uint16_t index_cmd;
  
  for(index_cmd=0; (offset_in_1+index_cmd)<max_length_in_1; index_cmd++)
  {
    if(in_2[index_cmd] == 0x00)
      break;
    else
      in_1[offset_in_1+index_cmd] = in_2[index_cmd];
  }
  
  return index_cmd;
}


/* Cerca corrispondenza tra stream di byte in ingresso e PATTERN (di lunghezza nota). */
uint8_t trovaPattern(Pattern_t* pattern, uint8_t data)
{	
  if(data == pattern->stringa[pattern->idx])
  {
    //Match trovato	  
    (pattern->idx)++;
    
    if(pattern->idx >= pattern->size)
    {
      pattern->idx = 0;
      return 1;
    }
  }
  else
    pattern->idx = 0;
  
  return 0;	
}

void resetPattern(Pattern_t* pattern)
{
  pattern->idx = 0;
}


/* Controlla se la stringa in ingresso � formata da caratteri alfanumerici + '_'. */
uint8_t isAlfaNumerico(uint8_t* src, uint16_t src_size)
{
  uint8_t res = 0;
  uint16_t i;
  
  for(i=0; i<src_size; i++)
  {
    if(	(src[i]>='0' && src[i]<='9') ||
       (src[i]>='A' && src[i]<='Z') ||
         (src[i]>='a' && src[i]<='z') ||
           (src[i]=='_'))
      res = 1;
    else
    {
      res = 0;
      break;
    }
  }
  
  return res;
}
