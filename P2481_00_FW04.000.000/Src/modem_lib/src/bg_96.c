/**
******************************************************************************
* File          	: bg_96.c
* Versione libreria	: 0.01
* Descrizione       	: Supporto modem Quectel BG96.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "bg_96.h"
#include "modem_utils.h"
#include <stdio.h>
#include <stdlib.h>
#include "Versioni_HW.h"


const BG96_PWR_Action_t ACCENSIONE = {.tempo_drive=BG96_ON_PWRKEY_LOW_TIME, .next_status=BG96_WAIT_READY_S};
const BG96_PWR_Action_t SPEGNIMENTO = {.tempo_drive=BG96_OFF_PWRKEY_LOW_TIME, .next_status=BG96_STDBY_S};

const BG96_Cell_Idx CELL_FIELD_IDX[4] = {                               //Tabella indici risultati per info celle
  {.tech=1, .MCC=2, .MNC=3, .LAC_TAC=4, .cell_ID=5, .rx_lev=9},         //GSM - serving
  {.tech=1, .MCC=3, .MNC=4, .LAC_TAC=11, .cell_ID=5, .rx_lev=14},       //LTE - serving
  {.tech=0, .MCC=1, .MNC=2, .LAC_TAC=3, .cell_ID=4, .rx_lev=7},         //GSM - neighbour  
  {.tech=0, .MCC=2, .MNC=3, .LAC_TAC=10, .cell_ID=4, .rx_lev=5},        //LTE - neighbour (non significativo: parsing comando per celle neighbour
                                                                        //in tecnologie diverse da GSM NON E' SUPPORTATO)
};


GPTIMER bg96_pwrTimer, bg96_resetTimer, bg96_guardTimer, bg96_waitReadyTimer;                   //Timer
GPTIMER bg96_NopTimer, bg96_offDelayTimer;
BG96_State_t bg96_status, bg96_status_prev;
BG96_GNSS_State_t bg96_gnss_status;
static Mod_t modello = MOD_BG96;
uint32_t bg96_reset_cnt = 0;									//Contatore RESET
uint8_t bg96_pwr_off_failure = 0;
uint32_t bg96_err_cnt = 0;									//Contatore errori
uint32_t bg96_rat_cnt = 0;                                                                      //Contatore impostazioni RAT
BG96_PWR_t bg96_PWR = PWR_IGNOTO;								//Stato accensione modem
const BG96_PWR_Action_t* bg96_PWR_action;                                                       //Azione richiesta con pilotaggio PWRKEY
uint16_t bg96_cmd_MASK  = 0;
static uint8_t fifo_index = 0;
uint16_t bg96_wait_ready_failure;
IN_t bg96_GPIO_STATUS;

uint8_t bg96_ready = 0;
BG96_Reg_t bg96_reg;
BG96_GNSS_Reg_t bg96_gnss_reg;
uint8_t bg96_last_error;


//TEST FW_BG95 @ 24/05/2022 = BG95M3LAR02A04 - APPVER: 01.002.01.002
//TEST FW_BG95 @ 20/04/2022 = BG95M3LAR02A04 - APPVER: 01.001.01.001

/* ---------- DATA SET ---------- */
char bg96_FW_info[BG96_FW_INFO_SIZE];								//Stringa versione FW AT (main version)
char bg96_FW_info_sub[BG96_FW_INFO_SUB_SIZE];                                                   //Stringa versione FW AT (sub version)
char bg96_ICCID[MOD_ICCID_MAX_SIZE];								//ICCID SIM
uint8_t bg96_RSSI = 99;										//RSSI rete cellulare
char bg96_latitudine[BG96_LAT_SIZE], bg96_longitudine[BG96_LONG_SIZE];				//Latitudine + Longitudine (stringa)
float bg96_latitudine_f, bg96_longitudine_f;							//Latitudine + Longitudine (float)
float bg96_latitudine_f_PRO, bg96_longitudine_f_PRO;						//Latitudine + Longitudine (float): precisione aumentata
char bg96_DataOra_UTC[BG96_DATE_TIME_SIZE];							//Data + Ora UTC (stringa)
uint32_t bg96_tempo_GNSS = 0;									//Tempo connessione GNSS [ms]
uint32_t bg96_tempo_Registrazione_rete = 0;							//Tempo registrazione rete cellulare [ms]
Mod_InfoCellaStr_t bg96_info_cella[MOD_MAX_CELL_NUM];						//Info celle serving+neighbour (stringa)
uint8_t bg96_num_celle;                                                                         //Numero celle serving+neighbour
uint8_t bg96_stato_Registrazione_rete = IGNOTO;							//Stato registrazione rete cellulare
uint32_t bg96_MQTT_RX_size = 0;									//Dimensione messaggio MQTT ricevuto
uint32_t bg96_tempo_HTTP = 0;									//Tempo GET HTTP (download file GNSS assistito)
uint8_t bg96_timeout_GNSS = BG96_GNSS_TIMEOUT_DFLT;						//Timeout GNSS (T=5s*bg96_timeout_GNSS)
uint8_t bg96_timeout_GNSS_PRO = BG96_GNSS_PRO_TIMEOUT_DFLT;					//Timeout GNSS PRO (T=5s*bg96_timeout_GNSS_PRO)
uint8_t bg96_FTP_version, bg96_FTP_subversion;							//Numero versione + sottoversione file FTP
uint8_t bg96_gnss_constellation = GPS_GLONASS;                                                  //Tipologia costellazione (BG95-only)
BG96_RAT_Scan_Mode_t bg96_RAT_scan_mode = RAT_SM_NULL;                                          //RAT scan mode
BG96_RAT_LTE_Mode_t bg96_RAT_LTE_mode = RAT_LTE_NULL;                                           //RAT LTE mode
uint8_t mod_versione_HW, mod_sottoversione_HW;
/* ------------------------------ */
uint8_t bg96_check_COM=0, bg96_lettura_GNSS_PRO=0, bg96_RTC_error=0, bg96_PDP_pending_cnt=0;
uint8_t bg96_MQTT_buffer[128], bg96_HTTP_FTP_buffer[64], bg96_GNSS_buffer[64], bg96_GpSettings_buffer[32];
uint8_t bg96_tentativi_Registrazione_rete=0, bg96_tentativi_GNSS_PRO=0;
uint16_t bg96_MQTT_msgId_publish, bg96_MQTT_msgId_subscribe;
char bg96_MQTT_result = 0;									//Codice errore transazione MQTT (ASCII)
uint16_t bg96_FTP_result[2], bg96_FTP_fSize;							//Codici errore transazione FTP
uint16_t bg96_HTTP_result[3], bg96_HTTP_fSize;							//Codici errore transazione HTTP
uint16_t bg96_GNSS_valInt;									//Periodo validità file GNSS assistito [s]
char bg96_GNSS_valDate[BG96_DATE_XTRA_SIZE];							//Data START validità file GNSS assistito


static uint8_t AT_OK_STR[] = "OK";
static uint8_t AT_ERROR_STR[] = "ERROR";

#define FIFO_INFO_SIZE             	8
ErrorCode_t bg96_GetInfo(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_GetInfo[FIFO_INFO_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT",        			/*.at_resp=*/(uint8_t*)"OK",			        /*.at_cmd_type=*/BG96_CMD_AT,				/*.at_cmd_maxRetry=*/10},
  {/*.at_cmd=*/(uint8_t*)"ATI",        			/*.at_resp=*/(uint8_t*)"Revision: ",		        /*.at_cmd_type=*/BG96_CMD_FW_INFO,			/*.at_cmd_maxRetry=*/3},
  {/*.at_cmd=*/(uint8_t*)"AT+QCCID",			/*.at_resp=*/(uint8_t*)"+QCCID: ",     	                /*.at_cmd_type=*/BG96_CMD_ICCID, 			/*.at_cmd_maxRetry=*/10},
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSXTRA?",		/*.at_resp=*/(uint8_t*)"+QGPSXTRA: ",                   /*.at_cmd_type=*/BG96_CMD_QUERY_GNSS_ASS,               /*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSCFG=\"gnssconfig\"",   /*.at_resp=*/(uint8_t*)"+QGPSCFG: \"gnssconfig\",",     /*.at_cmd_type=*/BG96_CMD_QUERY_GNSS_CONST,	        /*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QAPPVER",        		/*.at_resp=*/(uint8_t*)"+QAPPVER: ",		        /*.at_cmd_type=*/BG96_CMD_FW_INFO_SUB,			/*.at_cmd_maxRetry=*/1},  
  {/*.at_cmd=*/(uint8_t*)"AT+QCFG=\"nwscanmode\"",      /*.at_resp=*/(uint8_t*)"+QCFG: \"nwscanmode\",",	/*.at_cmd_type=*/BG96_CMD_QUERY_RAT_SCAN_MODE,		/*.at_cmd_maxRetry=*/1},  
  {/*.at_cmd=*/(uint8_t*)"AT+QCFG=\"iotopmode\"",       /*.at_resp=*/(uint8_t*)"+QCFG: \"iotopmode\",",		/*.at_cmd_type=*/BG96_CMD_QUERY_RAT_LTE_MODE,		/*.at_cmd_maxRetry=*/1},
};


#define FIFO_CONFIG_RAT_SIZE            3
ErrorCode_t bg96_ConfigRAT(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_ConfigRAT[FIFO_CONFIG_RAT_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+CFUN=0",        		                /*.at_resp=*/(uint8_t*)"OK",            /*.at_cmd_type=*/BG96_CMD_AIRPLANE_MODE,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QCFG=\"nwscanmode\","SET_RAT_SM_AUTO,      /*.at_resp=*/(uint8_t*)"OK",		/*.at_cmd_type=*/BG96_CMD_RAT_SCAN_MODE,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QCFG=\"iotopmode\","SET_RAT_LTE_CATM_ONLY, /*.at_resp=*/(uint8_t*)"OK",		/*.at_cmd_type=*/BG96_CMD_RAT_LTE_MODE,		        /*.at_cmd_maxRetry=*/1},  
};


#define FIFO_NETWORK_REG_SIZE           3               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_NetworkReg(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_NetworkReg[FIFO_NETWORK_REG_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+CFUN?",        	/*.at_resp=*/(uint8_t*)"+CFUN: 1",		/*.at_cmd_type=*/BG96_CMD_QUERY_PHONE_MODE,	/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+CFUN=1",        	/*.at_resp=*/(uint8_t*)"OK",		        /*.at_cmd_type=*/BG96_CMD_FULL_FUNC_MODE,	/*.at_cmd_maxRetry=*/2},
  {/*.at_cmd=*/(uint8_t*)"AT+CREG?",            /*.at_resp=*/(uint8_t*)"+CREG: ",               /*.at_cmd_type=*/BG96_CMD_REG_RETE,             /*.at_cmd_maxRetry=*/1},  
};


#define GNSS_ASS_FNAME			"\"UFS:xtra2.bin\""
#define FIFO_GNSS_ASSISTANCE_SIZE       8		//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_GNSS_Assistance(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_GNSS_Assistance[FIFO_GNSS_ASSISTANCE_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSXTRA?",				/*.at_resp=*/(uint8_t*)"+QGPSXTRA: ",   		/*.at_cmd_type=*/BG96_CMD_QUERY_GNSS_ASS,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSXTRA=1",				/*.at_resp=*/(uint8_t*)"OK",				/*.at_cmd_type=*/BG96_CMD_GNSS_ASS_ENB,			/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSXTRADATA?",				/*.at_resp=*/(uint8_t*)"+QGPSXTRADATA: ",               /*.at_cmd_type=*/BG96_CMD_QUERY_GNSS_ASS_FILE,          /*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSDEL=3",				/*.at_resp=*/(uint8_t*)"OK",   				/*.at_cmd_type=*/BG96_CMD_CANCELLA_ASS,			/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+CCLK?",					/*.at_resp=*/(uint8_t*)"+CCLK: \"",			/*.at_cmd_type=*/BG96_CMD_RTC_DATE_TIME,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSXTRATIME=0,\"",			/*.at_resp=*/(uint8_t*)"OK",   				/*.at_cmd_type=*/BG96_CMD_GNSS_ASS_TIME,		/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSXTRADATA="GNSS_ASS_FNAME,              /*.at_resp=*/(uint8_t*)"OK",				/*.at_cmd_type=*/BG96_CMD_GNSS_ASS_FILE,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT",        					/*.at_resp=*/(uint8_t*)"OK",				/*.at_cmd_type=*/BG96_CMD_AT,				/*.at_cmd_maxRetry=*/1},  
};
#define FIFO_GNSS_STANDARD_SIZE     	4		//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_GNSS_Standard(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_GNSS_Standard[FIFO_GNSS_STANDARD_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSXTRA?",		/*.at_resp=*/(uint8_t*)"+QGPSXTRA: ",		/*.at_cmd_type=*/BG96_CMD_QUERY_GNSS_ASS,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSDEL=3",		/*.at_resp=*/(uint8_t*)"OK",   			/*.at_cmd_type=*/BG96_CMD_CANCELLA_ASS,			/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSXTRA=0",               /*.at_resp=*/(uint8_t*)"OK",			/*.at_cmd_type=*/BG96_CMD_GNSS_ASS_ENB,			/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT",        			/*.at_resp=*/(uint8_t*)"OK",			/*.at_cmd_type=*/BG96_CMD_AT,				/*.at_cmd_maxRetry=*/1},  
};

#define FIFO_GNSS_CFG_SIZE          	3               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_GNSS_Config(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_GNSS_Config[FIFO_GNSS_CFG_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+QGPS=1,1",                 /*.at_resp=*/(uint8_t*)"OK",            /*.at_cmd_type=*/BG96_CMD_GNSS_ON,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QGPS=1,255,,,",            /*.at_resp=*/(uint8_t*)"OK",            /*.at_cmd_type=*/BG96_CMD_GNSS_ON,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT",        			/*.at_resp=*/(uint8_t*)"OK",		/*.at_cmd_type=*/BG96_CMD_AT,			/*.at_cmd_maxRetry=*/1},
};


#define PDP_ID				"1"
#define NTP_SERV_ADDR			"\"193.204.114.232\""	//I.N.RI.M NTP server
#define FIFO_PDP_SIZE             	6			//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_PDP_Config(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_PDP_Config[FIFO_PDP_SIZE] = {	
  {/*.at_cmd=*/(uint8_t*)"AT+CCLK?",            				/*.at_resp=*/(uint8_t*)"+CCLK: \"",				/*.at_cmd_type=*/BG96_CMD_RTC_DATE_TIME,                /*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QICSGP="PDP_ID",1,"APN_ADDR",\"\",\"\",0",         /*.at_resp=*/(uint8_t*)"OK",					/*.at_cmd_type=*/BG96_CMD_CFG_PDP,			/*.at_cmd_maxRetry=*/1},  	
  {/*.at_cmd=*/(uint8_t*)"AT+QIACT?",   					/*.at_resp=*/(uint8_t*)"+QIACT: "PDP_ID",1",    		/*.at_cmd_type=*/BG96_CMD_QUERY_PDP, 			/*.at_cmd_maxRetry=*/1},		
  {/*.at_cmd=*/(uint8_t*)"AT+QIACT="PDP_ID,   					/*.at_resp=*/(uint8_t*)"OK",    				/*.at_cmd_type=*/BG96_CMD_PDP_ON, 			/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QNTP="PDP_ID","NTP_SERV_ADDR,			/*.at_resp=*/(uint8_t*)"+QNTP: 0",				/*.at_cmd_type=*/BG96_CMD_NTP,				/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+CCLK?",						/*.at_resp=*/(uint8_t*)"+CCLK: \"",				/*.at_cmd_type=*/BG96_CMD_RTC_DATE_TIME,		/*.at_cmd_maxRetry=*/1},
};

#define NOT_RETAIN                      "0"
#define RETAIN				"1"
#define MQTT_ID				"0"
#define MQTT_VSN			"4"/*"3"*/	
				//Versione MQTT (3=MQTT v3.1, 4=MQTT v3.1.1)

/*
#define MQTT_SERVER_ADDR		"\"20.71.212.16\"" 	//Indirizzo server MQTT
                                        //"\"40.119.157.149\""
#define MQTT_SERVER_PORT		"61613"						//Porta server MQTT
#define MQTT_USER_NAME			"\"\""					//User-name server MQTT
#define MQTT_PASSWORD			"\"\""					//Password server MQTT
*/

#define MQTT_SERVER_ADDR		 "\"alesea-lb.westeurope.cloudapp.azure.com\""	//Indirizzo server MQTT
                                        //"\"40.119.157.149\""
#define MQTT_SERVER_PORT		"61613"						//Porta server MQTT
#define MQTT_USER_NAME			"\"zetes\""					//User-name server MQTT
#define MQTT_PASSWORD			"\"ZqUiD897\""					//Password server MQTT


#define MQTT_QoS_PUBLISH                1                                               //QoS Publish
#define MQTT_QoS_SUBSCRIBE              1                                               //QoS Subscribe
#if ((MQTT_QoS_PUBLISH<0) || (MQTT_QoS_PUBLISH>2))
#error "PUBLISH QoS non valido!"
#endif
#if ((MQTT_QoS_SUBSCRIBE<0) || (MQTT_QoS_SUBSCRIBE>2))
#error "SUBSCRIBE QoS non valido!"
#endif
#define FIFO_MQTT_CONN_SIZE             10		//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_MQTT_Connect(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_MQTT_Connect[FIFO_MQTT_CONN_SIZE] = {	
  {/*.at_cmd=*/(uint8_t*)"AT+CCLK?",								/*.at_resp=*/(uint8_t*)"+CCLK: \"",			        /*.at_cmd_type=*/BG96_CMD_RTC_DATE_TIME,                /*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QIACT?",   							/*.at_resp=*/(uint8_t*)"+QIACT: "PDP_ID",1",            	/*.at_cmd_type=*/BG96_CMD_QUERY_PDP, 			/*.at_cmd_maxRetry=*/1},			
  {/*.at_cmd=*/(uint8_t*)"AT+QIACT="PDP_ID,   							/*.at_resp=*/(uint8_t*)"OK",    			        /*.at_cmd_type=*/BG96_CMD_PDP_ON, 			/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+CSQ",								/*.at_resp=*/(uint8_t*)"+CSQ: ",			        /*.at_cmd_type=*/BG96_CMD_RSSI,				/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QENG=\"servingcell\"",						/*.at_resp=*/(uint8_t*)"+QENG: \"servingcell\",",	        /*.at_cmd_type=*/BG96_CMD_INFO_CELLA,			/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QENG=\"neighbourcell\"",				                /*.at_resp=*/(uint8_t*)"+QENG: \"neighbourcell\",",             /*.at_cmd_type=*/BG96_CMD_INFO_CELLA_VICINA,		/*.at_cmd_maxRetry=*/1},	    
  {/*.at_cmd=*/(uint8_t*)"AT+QMTCFG=\"version\","MQTT_ID","MQTT_VSN,				/*.at_resp=*/(uint8_t*)"OK",    				/*.at_cmd_type=*/BG96_CMD_MQTT_CFG_VSN, 		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QMTCFG=\"recv/mode\","MQTT_ID",0,1",				/*.at_resp=*/(uint8_t*)"OK",    				/*.at_cmd_type=*/BG96_CMD_MQTT_CFG_RCV, 		/*.at_cmd_maxRetry=*/1},  
  {/*.at_cmd=*/(uint8_t*)"AT+QMTOPEN="MQTT_ID","MQTT_SERVER_ADDR","MQTT_SERVER_PORT,            /*.at_resp=*/(uint8_t*)"+QMTOPEN: ",    			/*.at_cmd_type=*/BG96_CMD_MQTT_OPEN, 			/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QMTCONN="MQTT_ID",\"",  						/*.at_resp=*/(uint8_t*)"+QMTCONN: ",				/*.at_cmd_type=*/BG96_CMD_MQTT_CONN,			/*.at_cmd_maxRetry=*/1},
};

#define FIFO_MQTT_PUB_SIZE            2
ErrorCode_t bg96_MQTT_Publish(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_MQTT_Publish[FIFO_MQTT_PUB_SIZE] = {	  
  {/*.at_cmd=*/(uint8_t*)"AT+QMTPUB="MQTT_ID",",                /*.at_resp=*/(uint8_t*)"> ",            /*.at_cmd_type=*/BG96_CMD_MQTT_PUB_TOPIC, 		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"",   				        /*.at_resp=*/(uint8_t*)"+QMTPUB: ",    	/*.at_cmd_type=*/BG96_CMD_MQTT_PUB_PAYLOAD,             /*.at_cmd_maxRetry=*/1},
};

#define FIFO_MQTT_SUB_SIZE            3/*4*/
ErrorCode_t bg96_MQTT_Subscribe(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_MQTT_Subscribe[FIFO_MQTT_SUB_SIZE] = {	
//  {/*.at_cmd=*/(uint8_t*)"AT+QMTUNS="MQTT_ID",",			/*.at_resp=*/(uint8_t*)"+QMTUNS: ",		/*.at_cmd_type=*/BG96_CMD_MQTT_UNSUBSCRIBE,             /*.at_cmd_maxRetry=*/1},		
  {/*.at_cmd=*/(uint8_t*)"AT+QMTSUB="MQTT_ID",",			/*.at_resp=*/(uint8_t*)"+QMTRECV: ",            /*.at_cmd_type=*/BG96_CMD_MQTT_SUBSCRIBE,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QMTPUB="MQTT_ID",0,0,"RETAIN",",           /*.at_resp=*/(uint8_t*)"> ",    		/*.at_cmd_type=*/BG96_CMD_MQTT_PUB_TOPIC, 		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"",   						/*.at_resp=*/(uint8_t*)"+QMTPUB: ",    	        /*.at_cmd_type=*/BG96_CMD_MQTT_PUB_PAYLOAD, 	        /*.at_cmd_maxRetry=*/1},
};

#define FTP_SERVER_ADDR                 "\"ftp.computec.biz\""		//Indirizzo server FTP (max 200 B)
#define FTP_USER_NAME			"\"computec\""			//User-name server FTP (max 255 B)
#define FTP_PASSWORD			"\"!Alesea@2019\""		//Password server FTP (max 255 B)
#define FTP_DIRECTORY			"\"./P2481_Alesea\""		//Directory download file aggiornamento su FTP (max 255 B)
#define FTP_FILE			"hP2481_Alesea"			//Nome file aggiornamento su FTP (max 255 B)
#define LOCAL_UFS_FILE			"\"P2481_Alesea.bin\""		//Nome locale file aggiornamento (UFS modulo)
#define FIFO_FW_UP_SIZE            	8			        //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_FW_Update(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_FW_Update[FIFO_FW_UP_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+QFTPCFG=\"account\","FTP_USER_NAME","FTP_PASSWORD,         /*.at_resp=*/(uint8_t*)"OK",				/*.at_cmd_type=*/BG96_CMD_FTP_CFG_ACCOUNT,              /*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QFDEL="LOCAL_UFS_FILE,   					/*.at_resp=*/(uint8_t*)"OK",    			/*.at_cmd_type=*/BG96_CMD_CANCELLA_FILE, 		/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QIACT?",   						/*.at_resp=*/(uint8_t*)"+QIACT: "PDP_ID",1",            /*.at_cmd_type=*/BG96_CMD_QUERY_PDP, 			/*.at_cmd_maxRetry=*/1},			
  {/*.at_cmd=*/(uint8_t*)"AT+QIACT="PDP_ID,   						/*.at_resp=*/(uint8_t*)"OK",    			/*.at_cmd_type=*/BG96_CMD_PDP_ON, 			/*.at_cmd_maxRetry=*/1},		
  {/*.at_cmd=*/(uint8_t*)"AT+QFTPOPEN="FTP_SERVER_ADDR,					/*.at_resp=*/(uint8_t*)"+QFTPOPEN: ",			/*.at_cmd_type=*/BG96_CMD_FTP_OPEN,			/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QFTPCWD="FTP_DIRECTORY,					/*.at_resp=*/(uint8_t*)"+QFTPCWD: ",			/*.at_cmd_type=*/BG96_CMD_FTP_SET_DIR,			/*.at_cmd_maxRetry=*/1},		
  {/*.at_cmd=*/(uint8_t*)"AT+QFTPGET=",							/*.at_resp=*/(uint8_t*)"+QFTPGET: ",			/*.at_cmd_type=*/BG96_CMD_FTP_GET_FILE,			/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QFTPCLOSE",						/*.at_resp=*/(uint8_t*)"+QFTPCLOSE: ",			/*.at_cmd_type=*/BG96_CMD_FTP_CLOSE,			/*.at_cmd_maxRetry=*/1},
};

#define HTTP_URL_ADDR					"http://xtrapath4.izatcloud.net/xtra2.bin"				//URL file GNSS assistito
#define FIFO_HTTP_CFG_SIZE          	7		//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_HTTP_Config(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_HTTP_Config[FIFO_HTTP_CFG_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+QIACT?",   				/*.at_resp=*/(uint8_t*)"+QIACT: "PDP_ID",1",            /*.at_cmd_type=*/BG96_CMD_QUERY_PDP, 		/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QIACT="PDP_ID,   				/*.at_resp=*/(uint8_t*)"OK",    			/*.at_cmd_type=*/BG96_CMD_PDP_ON, 		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QFDEL="GNSS_ASS_FNAME,   			/*.at_resp=*/(uint8_t*)"OK",    			/*.at_cmd_type=*/BG96_CMD_CANCELLA_FILE, 	/*.at_cmd_maxRetry=*/1},	
  {/*.at_cmd=*/(uint8_t*)"AT+QHTTPURL=",     				/*.at_resp=*/(uint8_t*)"CONNECT",			/*.at_cmd_type=*/BG96_CMD_HTTP_CFG_URL,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)HTTP_URL_ADDR,     				/*.at_resp=*/(uint8_t*)"OK",				/*.at_cmd_type=*/BG96_CMD_HTTP_URL,		/*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QHTTPGET",     				/*.at_resp=*/(uint8_t*)"+QHTTPGET: ",			/*.at_cmd_type=*/BG96_CMD_HTTP_GET_HEADER,      /*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QHTTPREADFILE="GNSS_ASS_FNAME,             /*.at_resp=*/(uint8_t*)"+QHTTPREADFILE: ",		/*.at_cmd_type=*/BG96_CMD_HTTP_GET_FILE,	/*.at_cmd_maxRetry=*/1},
};

#define FIFO_CLOSE_CONN_SIZE            2
ErrorCode_t bg96_CloseConn(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_CloseConn[FIFO_CLOSE_CONN_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+QMTDISC="MQTT_ID,          /*.at_resp=*/(uint8_t*)"+QMTDISC: "MQTT_ID",0",         /*.at_cmd_type=*/BG96_CMD_MQTT_DISCONN,         /*.at_cmd_maxRetry=*/1},
  {/*.at_cmd=*/(uint8_t*)"AT+QIDEACT="PDP_ID,   	/*.at_resp=*/(uint8_t*)"OK",    			/*.at_cmd_type=*/BG96_CMD_PDP_OFF, 		/*.at_cmd_maxRetry=*/1},
};

#define FIFO_GP_SETTINGS_CFG_SIZE       2               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
ErrorCode_t bg96_GpSettings_Config(uint8_t fifo_index, uint16_t* offsetResp);
Mod_Fifo_Entry_t fifo_GpSettings_Config[FIFO_GP_SETTINGS_CFG_SIZE] = {
  {/*.at_cmd=*/(uint8_t*)"AT+QGPSCFG=\"gnssconfig\",",  /*.at_resp=*/(uint8_t*)"OK",            /*.at_cmd_type=*/BG96_CMD_GNSS_CONST,	        /*.at_cmd_maxRetry=*/3},
  {/*.at_cmd=*/(uint8_t*)"AT",        			/*.at_resp=*/(uint8_t*)"OK",		/*.at_cmd_type=*/BG96_CMD_AT,			/*.at_cmd_maxRetry=*/1},  
};


/* ############################################################ 
* Local functions
* ############################################################
*/
void init_ctx_bg96(void);
ErrorCode_t bg96_GNSS_Location(uint16_t* offsetResp, uint8_t enh_prec);
ErrorCode_t bg96_GNSS_OFF(uint16_t* offsetResp);
uint8_t bg96_bypass_cmd(uint8_t cmd_index, uint16_t cmd_mask);
uint8_t bg96_is_GNSS_file_valid(void);
uint8_t bg96_is_RTC_sync(void);
void bg96_parse_location(float* latitudine_f, float* longitudine_f);
Mod_t bg96_detect_modulo(void);
void bg96_parse_info_cella(uint8_t idx_cella);




void init_bg96(uint8_t versione_HW, uint8_t sottoversione_HW)
{
  mod_versione_HW = versione_HW;
  mod_sottoversione_HW = sottoversione_HW;
  
  GPTIMER_init(&bg96_pwrTimer, BG96_ON_PWRKEY_LOW_TIME);
  GPTIMER_init(&bg96_resetTimer, BG95_RESET_WAIT_TIME);
  GPTIMER_init(&bg96_guardTimer, BG96_PWR_OFF_TIMEOUT);
  GPTIMER_init(&bg96_waitReadyTimer, BG96_WAIT_READY_TIMEOUT);
  GPTIMER_init(&bg96_NopTimer, BG96_NOP_TIMEOUT);
  GPTIMER_init(&bg96_offDelayTimer, BG96_OFF_DELAY_TIME);	
  
  init_ctx_bg96();
  
  bg96_GPIO_STATUS.stato = mod_Read_status();		//Assegno valore iniziale pin
  
  //Init macchina a stati
  set_bg96_status(BG96_STDBY_S);
  bg96_status_prev = BG96_STDBY_S;
  bg96_PWR = PWR_IGNOTO;
  bg96_PWR_action = &ACCENSIONE;
  bg96_gnss_status = BG96_GNSS_IDLE_S;
}

/* Init contesto "sessione" corrente. */
void init_ctx_bg96(void)
{
  bg96_err_cnt = 0;
  bg96_last_error = 0x00;
  bg96_cmd_MASK = 0;
  bg96_check_COM = 0;
  
  bg96_reg.WORD = 0;
  bg96_gnss_reg.WORD &= 0xFF00;         //Reset solo B0 (Diagnostica)
  
  mod_Init_AT();                        //Reset gestione RX/TX comandi AT (caso di reset asincroni del modulo)
}

uint8_t is_bg96_ready(void)
{
  return bg96_ready;	
}


/* Riconosce tipologia di modem (Quectel), in base a versione FW. */
Mod_t bg96_detect_modulo(void)
{
  if(memcmp(bg96_FW_info, "BG96", 4) == 0)
  {
    bg96_reg.BIT.TYPE_DETECT = 1;
    return MOD_BG96;
  }
  else if(memcmp(bg96_FW_info, "BG95", 4) == 0)
  {
    bg96_reg.BIT.TYPE_DETECT = 1;
    return MOD_BG95;
  }
  else
  {
    bg96_reg.BIT.TYPE_DETECT = 0;
    if(mod_versione_HW==VERSIONE_HW_DEFAULT_BG96 && mod_sottoversione_HW==SOTTO_VERSIONE_HW_DEFAULT_BG96)
      return MOD_BG96;  //errore detect (fallback a BG96)
    else
      return MOD_BG95;  //errore detect (fallback a BG95)
  }    
}


/* Parsing comando AT localizzazione. */
void bg96_parse_location(float* latitudine_f, float* longitudine_f)
{  				  						
  memset(bg96_latitudine, 0x00, sizeof(bg96_latitudine)); 
  *latitudine_f = 0.0;
  memset(bg96_longitudine, 0x00, sizeof(bg96_longitudine)); 
  *longitudine_f = 0.0;	
  memcpy(bg96_latitudine, searchField_res[1].field, sizeof(bg96_latitudine));		//Parse latitudine
  *latitudine_f = atof(bg96_latitudine);
  memcpy(bg96_longitudine, searchField_res[2].field, sizeof(bg96_longitudine));         //Parse longitudine
  *longitudine_f = atof(bg96_longitudine);
}


/* Ritorna 1 se il file di assistenza è ancora valido, 0 altrimenti. */
uint8_t bg96_is_GNSS_file_valid(void)
{
  //#define BYPASS_VALIDITA
#define GIORNI_VALIDITA			1				//TODO: utilizzare 'bg96_GNSS_valInt' per il controllo ? 
  
  uint8_t dd=0, dd_ass=0;
  int8_t delta_dd = 0;
  
#ifdef BYPASS_VALIDITA
  return 0;
#else
  if(memcmp(&bg96_DataOra_UTC[0],&bg96_GNSS_valDate[2],2) != 0) return 0;	//Anni diversi
  if(memcmp(&bg96_DataOra_UTC[3],&bg96_GNSS_valDate[5],2) != 0) return 0;	//Mesi diversi
  
  dd = (convertStrToByte((unsigned char*)&bg96_DataOra_UTC[6], 6, 8))%32;	//Giorno corrente (decimale, [0,31])
  dd_ass = (convertStrToByte((unsigned char*)&bg96_GNSS_valDate[8], 8, 10))%32;	//Giorno file assistenza (decimale, [0,31])
  
  if(dd==0 || dd_ass==0) return 0;                                              //Errore parsing
  
  if(dd >= dd_ass) delta_dd = dd-dd_ass;
  else delta_dd = (dd+31)-dd_ass;
  if(delta_dd >= GIORNI_VALIDITA) return 0;
  else return 1;
#endif
}

/* Ritorna 1 se l'RTC interno al modem è stato sincronizzato (almeno una volta), 0 altrimenti. */
uint8_t bg96_is_RTC_sync(void)
{
  uint8_t sync = 0;
  
  if(bg96_DataOra_UTC[0]=='8' && bg96_DataOra_UTC[1]=='0') sync=0;		//Anno pari a default RTC
  else if(bg96_DataOra_UTC[0]==0 && bg96_DataOra_UTC[1]==0) sync=0;		//Anno non disponibile (e.g. dopo POR uC)
  else sync=1;
  
  if(sync) bg96_reg.BIT.RTC_SYNC=1;
  else bg96_reg.BIT.RTC_SYNC=0;
  
  return sync;
}


/* Parsing comando AT info cella. */
void bg96_parse_info_cella(uint8_t idx_cella)
{
  const BG96_Cell_Idx* lista_res_idx;         //Puntatore a lista da cui recuperare gli indici (posizionali) dei campi
  uint8_t offset_lista = 0;
  
  if(idx_cella >= MOD_MAX_CELL_NUM)
    return;
  
  if(idx_cella == MOD_SERVING_CELL) offset_lista=0;     //GSM - serving
  else offset_lista=2;                                  //GSM - neighbour
  
  lista_res_idx = &CELL_FIELD_IDX[offset_lista];
  memset(&bg96_info_cella[idx_cella], 0x00, sizeof(bg96_info_cella[idx_cella]));						
  memcpy(bg96_info_cella[idx_cella].tecnologia, searchField_res[lista_res_idx->tech].field, sizeof(bg96_info_cella[idx_cella].tecnologia));
  if(memcmp(&bg96_info_cella[idx_cella].tecnologia[1], "GSM", 3) == 0)
  {
    //--- GSM ---
    memcpy(bg96_info_cella[idx_cella].MCC, searchField_res[lista_res_idx->MCC].field, sizeof(bg96_info_cella[idx_cella].MCC));
    memcpy(bg96_info_cella[idx_cella].MNC, searchField_res[lista_res_idx->MNC].field, sizeof(bg96_info_cella[idx_cella].MNC));
    memcpy(bg96_info_cella[idx_cella].LAC_TAC, searchField_res[lista_res_idx->LAC_TAC].field, sizeof(bg96_info_cella[idx_cella].LAC_TAC));
    memcpy(bg96_info_cella[idx_cella].cell_ID, searchField_res[lista_res_idx->cell_ID].field, sizeof(bg96_info_cella[idx_cella].cell_ID));
    bg96_info_cella[idx_cella].rx_lev = (int16_t)(strtol((char const*)searchField_res[lista_res_idx->rx_lev].field, NULL, 10));
  }
  else
  {
    //--- LTE ---
    offset_lista++;     //Sposto puntatore su liste LTE
    lista_res_idx = &CELL_FIELD_IDX[offset_lista];
    //Nota: parsing comando per celle neighbour in tecnologie diverse da GSM NON E' SUPPORTATO
    memcpy(bg96_info_cella[idx_cella].MCC, searchField_res[lista_res_idx->MCC].field, sizeof(bg96_info_cella[idx_cella].MCC));
    memcpy(bg96_info_cella[idx_cella].MNC, searchField_res[lista_res_idx->MNC].field, sizeof(bg96_info_cella[idx_cella].MNC));
    memcpy(bg96_info_cella[idx_cella].LAC_TAC, searchField_res[lista_res_idx->LAC_TAC].field, sizeof(bg96_info_cella[idx_cella].LAC_TAC));
    memcpy(bg96_info_cella[idx_cella].cell_ID, searchField_res[lista_res_idx->cell_ID].field, sizeof(bg96_info_cella[idx_cella].cell_ID));
    bg96_info_cella[idx_cella].rx_lev = (int16_t)(strtol((char const*)searchField_res[lista_res_idx->rx_lev].field, NULL, 10));
  }
}


/* Determina se il comando indicizzato è da saltare, in base alla maschera. */
uint8_t bg96_bypass_cmd(uint8_t cmd_index, uint16_t cmd_mask)
{
  if(((1<<(cmd_index)) & cmd_mask) == 0)
    return 0;		//comando da eseguire
  else
    return 1;		//comando da saltare
}


void set_bg96_status(BG96_State_t new_status)
{
  switch(new_status)
  {
    case BG96_PWR_ON_S:
    case BG96_RESET_S:
    case BG96_PWR_ON_RESET_S:
      init_ctx_bg96(); break;							//init contesto "sessione" corrente
    case BG96_PWR_OFF_S: {
      GPTIMER_stop(&bg96_offDelayTimer);
      GPTIMER_setTics(&bg96_offDelayTimer, BG96_OFF_DELAY_TIME);
      GPTIMER_reTrigger(&bg96_offDelayTimer);
    } break;
    case BG96_WAIT_READY_S: bg96_reg.BIT.PWR_ON_FAILURE=0; break;
    case BG96_ERR_S: bg96_status_prev=bg96_status; break;			//snapshot stato che ha generato errore
    case BG96_INFO_S: { bg96_reg.BIT.SIM_RILEVATA=0; bg96_reg.BIT.TYPE_DETECT=0; } break;
    case BG96_CONFIG_RAT_S: { bg96_reg.BIT.RAT_SET=1; bg96_rat_cnt++; } break;
    case BG96_NETWORK_REG_S: { 
      bg96_tentativi_Registrazione_rete=0; bg96_stato_Registrazione_rete=IGNOTO;
      bg96_tempo_Registrazione_rete=0; bg96_reg.BIT.STATO_REG_RETE=0; bg96_cmd_MASK=0;
    } break;
    case BG96_PDP_ON_S: { 
      bg96_reg.BIT.PDP_ERROR=0; bg96_reg.BIT.NTP_SYNC=0; 
      bg96_cmd_MASK=0;
    } break;
    case BG96_GNSS_ON_S: { bg96_reg.BIT.LOC_GNSS=0; bg96_gnss_reg.BIT.LOC_GNSS_PRO=0; } break;
    case BG96_GNSS_LOCATION_PRO_S: { bg96_tentativi_GNSS_PRO=0; bg96_lettura_GNSS_PRO=0; } break;
    case BG96_MQTT_PUBLISH_S: 
#if (MQTT_QoS_PUBLISH == 0)
      bg96_MQTT_msgId_publish = 0;
#else
      bg96_MQTT_msgId_publish++;                                        //Incremento Msg-Id (PUBLISH)
      if(bg96_MQTT_msgId_publish==0) bg96_MQTT_msgId_publish++;         //Msg-Id=0 non ammesso con QoS>0
#endif
      bg96_reg.BIT.MQTT_ERROR_PUB = 0; 
      break;
    case BG96_MQTT_SUBSCRIBE_S:
#if (MQTT_QoS_SUBSCRIBE == 0)
      bg96_MQTT_msgId_subscribe = 1;                                    //Msg-Id=0 non ammesso
#else
      bg96_MQTT_msgId_subscribe++;                                      //Incremento Msg-Id (SUBSCRIBE)
      if(bg96_MQTT_msgId_subscribe==0) bg96_MQTT_msgId_subscribe++;     //Msg-Id=0 non ammesso
#endif
      bg96_reg.BIT.MQTT_MSG_RX = 0; 
      break;
    case BG96_FTP_S: { bg96_FTP_fSize=0; bg96_reg.BIT.FTP_ERROR=0; bg96_cmd_MASK=0; } break;
    case BG96_MQTT_CONNECT_S: { bg96_cmd_MASK=0; bg96_reg.BIT.MQTT_ERROR_CONN=0; bg96_num_celle=0; } break;
    case BG96_HTTP_S: { bg96_cmd_MASK=0; bg96_tempo_HTTP=0; bg96_HTTP_fSize=0; bg96_reg.BIT.HTTP_ERROR=0; } break;
    case BG96_GNSS_ASSISTANCE_S:
    case BG96_GNSS_STANDARD_S: {
      bg96_cmd_MASK=0; bg96_gnss_reg.BIT.GENERIC_ERROR=0; bg96_reg.BIT.LOC_GNSS=0;
    } break;	
    case BG96_GP_SETTINGS_S: bg96_cmd_MASK=0; break;
    default: break;
  }
  if(new_status!=BG96_IDLE_S) bg96_reg.BIT.GENERIC_ERROR=0; 
  bg96_status = new_status;
  fifo_index = 0;
}
BG96_State_t get_bg96_status(void)
{
  return bg96_status;	
}


/* Macchina a stati BG96. */

void bg96_Automa(const uint32_t PERIODO)
{
#define FILTRO_INGRESSI		5		/* 50ms di filtro chiamando la funzione ogni 10ms */
  ErrorCode_t err;
  uint16_t offset=0, offset_cell_start=0, offset_cell_end=0;
  uint32_t offset_msg_RX = 0;
  
  
  bg96_GPIO_STATUS.pin = mod_Read_status();
  /* Filtro ingresso STATUS: integrale. */
  if(bg96_GPIO_STATUS.pin==0) (bg96_GPIO_STATUS.contatore)--;
  else (bg96_GPIO_STATUS.contatore)++;
  /* Saturazione filtro */
  if(bg96_GPIO_STATUS.contatore <= 0)
  {
    bg96_GPIO_STATUS.contatore = 0;
    bg96_GPIO_STATUS.stato = 0;
  }
  if(bg96_GPIO_STATUS.contatore >= FILTRO_INGRESSI)
  {
    bg96_GPIO_STATUS.contatore = FILTRO_INGRESSI;
    bg96_GPIO_STATUS.stato = 1;
  }
  
  
  switch(bg96_status)
  {
    case BG96_PWR_ON_RESET_S:
      //### Accensione modulo con Reset ###
      bg96_pwr_off_failure = 0;
      GPTIMER_stop(&bg96_guardTimer);		//Reset condizioni di guardia su richiesta esplicita di PWR-ON
      GPTIMER_stop(&bg96_NopTimer);		
      fifo_index = 0;
      set_bg96_status(BG96_RESET_S);
      break;
      
    case BG96_PWR_ON_S:
      //### Accensione modulo ###
      bg96_pwr_off_failure = 0;
      GPTIMER_stop(&bg96_guardTimer);		//Reset condizioni di guardia su richiesta esplicita di PWR-ON
      GPTIMER_stop(&bg96_NopTimer);		
      fifo_index = 0;
      bg96_PWR_action = &ACCENSIONE;
      if(bg96_GPIO_STATUS.stato == 0)
      {
        //Modem spento: accendi
        bg96_ready = 0;
        set_bg96_status(BG96_DRIVE_PWRKEY_S);
      }
      else
      {
        //Modem già acceso (UART già READY)
        set_bg96_status(BG96_RESET_S);          //Reset modem sincrono con reset uC
      }
      break;
      
    case BG96_DRIVE_PWRKEY_S:
      //### Pilotaggio pin PWRKEY (sia per accensione che per spegnimento) ###
      if(!bg96_pwrTimer.enable)
      {
        mod_Drive_power(1);			//PWR attivo
        GPTIMER_stop(&bg96_pwrTimer);
        GPTIMER_setTics(&bg96_pwrTimer, bg96_PWR_action->tempo_drive);
        GPTIMER_reTrigger(&bg96_pwrTimer);      //Start wait time
      }
      else if(GPTIMER_isTimedOut(&bg96_pwrTimer))
      {
        GPTIMER_stop(&bg96_pwrTimer);
        mod_Drive_power(0);                     //Rilascio PWR
        set_bg96_status(bg96_PWR_action->next_status);
      }
      break;
      
    case BG96_WAIT_READY_S:
      //----- Check tempo guardia per failure power-on -----
      if(!bg96_waitReadyTimer.enable)
      {
        GPTIMER_stop(&bg96_waitReadyTimer);
        GPTIMER_setTics(&bg96_waitReadyTimer, BG96_WAIT_READY_TIMEOUT);
        GPTIMER_reTrigger(&bg96_waitReadyTimer);	                            //Start guard time
      }
      else if(GPTIMER_isTimedOut(&bg96_waitReadyTimer))
      {
        GPTIMER_stop(&bg96_waitReadyTimer);					
        if(bg96_wait_ready_failure < BG96_RETRY_PWR_ON)
        {
          bg96_wait_ready_failure++;
          set_bg96_status(BG96_RESET_S);
        }
        else
        {
          bg96_wait_ready_failure = 0;
          bg96_reg.BIT.PWR_ON_FAILURE = 1;
          set_bg96_status(BG96_IDLE_S);
          return;
        }
      }
      //----------------------------------------------------
      if(bg96_GPIO_STATUS.stato == 1)
      {
        bg96_wait_ready_failure = 0;
        GPTIMER_stop(&bg96_waitReadyTimer);
        bg96_reg.BIT.PWR_ON_FAILURE = 0;
        bg96_PWR = PWR_ON;
        bg96_ready = 1;
        if(bg96_pwr_off_failure>0) {set_bg96_status(BG96_PWR_OFF_S);}           //Retry spegnimento
        else 
        {
          GPTIMER_stop(&bg96_NopTimer);
          GPTIMER_setTics(&bg96_NopTimer, BG96_NOP_TIMEOUT);
          GPTIMER_reTrigger(&bg96_NopTimer);
          set_bg96_status(BG96_INFO_S);
        }
      }
      else
        bg96_ready = 0;
      break;  
      
    case BG96_IDLE_S:
      fifo_index = 0;
      bg96_cmd_MASK = 0;
      break;
    
    case BG96_PWR_OFF_S:
      //### Spegnimento modulo ###
      if(!GPTIMER_isTimedOut(&bg96_offDelayTimer) && bg96_offDelayTimer.enable)
        return;
      
      GPTIMER_stop(&bg96_offDelayTimer);
      /* Ritardo prima di pilotare PWR OFF concluso:
      *  inserito su gestione BG95 altrimenti pilotando PWR OFF troppo ravvicinato a RESET, PWR_OFF non viene interpretato.
      */
      bg96_PWR_action = &SPEGNIMENTO;
      if(bg96_GPIO_STATUS.stato == 1)
      {
        //Modem acceso: spegni
        set_bg96_status(BG96_DRIVE_PWRKEY_S);
      }
      else
        set_bg96_status(BG96_STDBY_S);		        //Modem già spento
      break;
      
    case BG96_STDBY_S:
      //### Stato di appoggio (POR uC host o dopo spegnimento modulo) ###
      if(bg96_GPIO_STATUS.stato == 0)					
      {
        if(bg96_gnss_status == BG96_GNSS_WAIT_RESTART_S)
        {
          set_bg96_status(BG96_PWR_ON_S);					//Riavvio modulo
          bg96_gnss_status = BG96_GNSS_RESTART_S;
        }
        else
        {
          bg96_PWR=PWR_OFF; bg96_pwr_off_failure=0; GPTIMER_stop(&bg96_guardTimer); 
          bg96_gnss_status=BG96_GNSS_IDLE_S; bg96_RTC_error=0;
        }
      }
      else
      {
        //Check tempo guardia per failure power-off
        if(!bg96_guardTimer.enable)
        {
          GPTIMER_stop(&bg96_guardTimer);
          GPTIMER_setTics(&bg96_guardTimer, BG96_PWR_OFF_TIMEOUT);
          GPTIMER_reTrigger(&bg96_guardTimer);	                                //Start guard time
        }
        else if(GPTIMER_isTimedOut(&bg96_guardTimer))
        {
          GPTIMER_stop(&bg96_guardTimer);
          if(bg96_pwr_off_failure < BG96_RETRY_PWR_OFF)
          {
            bg96_pwr_off_failure++; 
            set_bg96_status(BG96_RESET_S);
          }
          else {bg96_PWR=PWR_OFF; bg96_gnss_status=BG96_GNSS_IDLE_S; bg96_RTC_error=0;}
        }
      }
      break;
      
    case BG96_RESET_S:
      //### Reset modem (a seguito di PWR-OFF failure) ###
      if(!bg96_resetTimer.enable)
      {
        bg96_reset_cnt++;
        mod_Drive_reset(1);                     //RST attivo
        GPTIMER_stop(&bg96_resetTimer);
        //Tempo differenziato tra BG95 e BG96 (per rispettare specifiche di BG96)
        if(ModemType == MOD_BG96)
          GPTIMER_setTics(&bg96_resetTimer, BG96_RESET_WAIT_TIME);
        else
          GPTIMER_setTics(&bg96_resetTimer, BG95_RESET_WAIT_TIME);
        GPTIMER_reTrigger(&bg96_resetTimer);    //Start wait time
      }
      else if(GPTIMER_isTimedOut(&bg96_resetTimer))
      {					
        GPTIMER_stop(&bg96_resetTimer);
        mod_Drive_reset(0);			//Rilascio RST
        set_bg96_status(BG96_WAIT_READY_S);
      }
      break;			     
    
    case BG96_INFO_S:				
      //### Info statiche modulo: versione FW, ICCID SIM, ENB GNSS assistito, costellazione GNSS, info RAT. ###
      if(!GPTIMER_isTimedOut(&bg96_NopTimer) && bg96_NopTimer.enable)
        return;
      
      GPTIMER_stop(&bg96_NopTimer);					
      //Attesa invio primo comando AT conclusa					
      err = bg96_GetInfo(fifo_index, &offset);
      if(err == M_OK_AT_RESP)
      {					
        switch(fifo_GetInfo[fifo_index].at_cmd_type)
        {
          case BG96_CMD_AT:
            bg96_check_COM++;
            break;
            
          case BG96_CMD_FW_INFO:
            {
              memset(bg96_FW_info, 0x00, sizeof(bg96_FW_info));
              memcpy(bg96_FW_info, &mod_buff_Risposta[offset], sizeof(bg96_FW_info));
              ModemType = bg96_detect_modulo();
            } break;
            
          case BG96_CMD_FW_INFO_SUB:
            {
              memset(bg96_FW_info_sub, 0x00, sizeof(bg96_FW_info_sub));
              memcpy(bg96_FW_info_sub, &mod_buff_Risposta[offset], sizeof(bg96_FW_info_sub));
            } break;
            
          case BG96_CMD_ICCID:
            {							
              memset(bg96_ICCID, 0x00, sizeof(bg96_ICCID));
              memcpy(bg96_ICCID, &mod_buff_Risposta[offset], sizeof(bg96_ICCID));
              bg96_reg.BIT.SIM_RILEVATA = 1;
            } break;
            
          case BG96_CMD_QUERY_GNSS_ASS:
            {
              if(mod_buff_Risposta[offset] == '1') bg96_gnss_reg.BIT.ASS_ENB=1;
              else bg96_gnss_reg.BIT.ASS_ENB=0;
            } break;
            
          case BG96_CMD_QUERY_GNSS_CONST:
            {
              bg96_gnss_constellation = convertStrToByte(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));
              bg96_gnss_reg.BYTE.B1 &= 0x1F;                                    //Reset bit costellazione GNSS (B1:[b5,b7])
              bg96_gnss_reg.BYTE.B1 |= ((bg96_gnss_constellation&0x07)<<5);     //Costellazione GNSS (B1:[b5,b7])
            } break;
            
          case BG96_CMD_QUERY_RAT_SCAN_MODE:
            bg96_RAT_scan_mode = (BG96_RAT_Scan_Mode_t)(mod_buff_Risposta[offset]);
            break;
            
          case BG96_CMD_QUERY_RAT_LTE_MODE:
            bg96_RAT_LTE_mode = (BG96_RAT_LTE_Mode_t)(mod_buff_Risposta[offset]);
            break;
            
          default: break;
        }
        
        if(mod_Inc_Fifo(&fifo_index, FIFO_INFO_SIZE))
        {
          if(bg96_RAT_scan_mode==RAT_SM_AUTO && bg96_RAT_LTE_mode==RAT_LTE_CATM_ONLY)
            set_bg96_status(BG96_NETWORK_REG_S);
          else
            set_bg96_status(BG96_CONFIG_RAT_S);
        }
      }
      else if(err == M_NO_AT_RESP)
        set_bg96_status(BG96_ERR_S);
      break;
      
    case BG96_CONFIG_RAT_S:
      //### Configurazione Radio Access Technology (RAT) ###
      err = bg96_ConfigRAT(fifo_index, &offset);
      if(err==M_OK_AT_RESP || err==M_NO_AT_RESP)        
      {
        if(mod_Inc_Fifo(&fifo_index, FIFO_CONFIG_RAT_SIZE))
          set_bg96_status(BG96_NETWORK_REG_S);
      }
      //N.B.
      //Impedisco che questo stato generi errori: i comandi vengono eseguiti tutti in ogni caso
      //Questo gestisce in automatico la condizione in cui non si riceve risposta ad AT+CFUN=0 (accade a volte)
      break;
    
    case BG96_NETWORK_REG_S:
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_NETWORK_REG_SIZE);       //Salta comando corrente
      else
      {
        //Esegui comando corrente
        bg96_tempo_Registrazione_rete += PERIODO;
        //### Info registrazione rete: stato registrazione ###
        err = bg96_NetworkReg(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          switch(fifo_NetworkReg[fifo_index].at_cmd_type)
          {
            case BG96_CMD_REG_RETE:
              bg96_tentativi_Registrazione_rete++;
              searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
              bg96_stato_Registrazione_rete = searchField_res[1].field[0];
              if(bg96_stato_Registrazione_rete==REGISTRATO_HOME || bg96_stato_Registrazione_rete==REGISTRATO_ROAMING)							
                bg96_reg.BIT.STATO_REG_RETE = 1;        //Registrazione completata (passo al prossimo comando in coda)
              else
              {
                //Registrazione non completata
                bg96_reg.BIT.STATO_REG_RETE = 0;
                if(bg96_tentativi_Registrazione_rete < BG96_MAX_RETRY_REG_RETE) return;         //Resto nello stato corrente (retry comando corrente)
              }
              break;
              
            case BG96_CMD_QUERY_PHONE_MODE: bg96_cmd_MASK=0x0002; break;        //Full functionality già attiva, salta Attivazione[1]
            
            default: break;
          }					
          
          if(mod_Inc_Fifo(&fifo_index, FIFO_NETWORK_REG_SIZE))
          {
            if(bg96_gnss_status == BG96_GNSS_RESTART_S)
            {
              //Riavvio modulo completato: riprendo configurazione
              if(bg96_gnss_reg.BIT.ASS_OPT) { bg96_gnss_status=BG96_GNSS_CHECK_ASS_FILE_S; set_bg96_status(BG96_GNSS_ASSISTANCE_S); }
              else { bg96_gnss_status=BG96_GNSS_START_S; set_bg96_status(BG96_GNSS_STANDARD_S); }
            }
            else set_bg96_status(BG96_IDLE_S);
          }
        }
        else if(err == M_NO_AT_RESP)
        {
          //--- Eccezioni su errore ---
          if(fifo_NetworkReg[fifo_index].at_cmd_type == BG96_CMD_FULL_FUNC_MODE)        
            mod_Inc_Fifo(&fifo_index, FIFO_NETWORK_REG_SIZE);		//Ripristino full functionality fallita: continuo con i comandi successivi
          else if(fifo_NetworkReg[fifo_index].at_cmd_type == BG96_CMD_QUERY_PHONE_MODE)
            mod_Inc_Fifo(&fifo_index, FIFO_NETWORK_REG_SIZE);           //Full functionality non attiva --> Eseguo Attivazione (e.g. comando successivo)
          else
            set_bg96_status(BG96_ERR_S);
        }
      }
      break;
    
    case BG96_GNSS_ASSISTANCE_S:
      //### Configurazione modalità GNSS assistito ###				
      switch(bg96_gnss_status)						//Selezione comandi da eseguire in base a stato GNSS
      {
        case BG96_GNSS_ENB_ASS_S: bg96_cmd_MASK=0x0000; break;		//Eseguo attivazione GNSS assistito
        case BG96_GNSS_CHECK_ASS_FILE_S: bg96_cmd_MASK=0x0003; break;	//Bypass CMD[0],[1]
        case BG96_GNSS_INJECT_ASS_S: bg96_cmd_MASK=0x007; break;	//Bypass CMD[0],[1],[2]
        case BG96_GNSS_START_S: bg96_cmd_MASK=0x007F; break;		//Bypass CMD[0],[1],[2],[3],[4],[5],[6]
        default: bg96_cmd_MASK=0x0002; break;				//Bypass CMD[1] (attivazione/disattivazione GNSS assistito)
      }
      
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_GNSS_ASSISTANCE_SIZE);		//Salta comando corrente
      else
      {
        //Esegui comando corrente
        err = bg96_GNSS_Assistance(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          switch(fifo_GNSS_Assistance[fifo_index].at_cmd_type)
          {
            case BG96_CMD_QUERY_GNSS_ASS:
              {
                if(mod_buff_Risposta[offset] == '1') bg96_gnss_reg.BIT.ASS_ENB=1;
                else bg96_gnss_reg.BIT.ASS_ENB=0;
                
                if(!bg96_gnss_reg.BIT.ASS_ENB)
                {
                  //Attivo GNSS assistito (precedentemente disattivato o mai attivato)
                  bg96_gnss_status = BG96_GNSS_ENB_ASS_S;									
                }
                else bg96_gnss_status=BG96_GNSS_CHECK_ASS_FILE_S;	//GNSS assistito già attivo: check validità file 
              } break;
              
            case BG96_CMD_GNSS_ASS_ENB: { bg96_gnss_status=BG96_GNSS_WAIT_RESTART_S; } break;
            
            case BG96_CMD_QUERY_GNSS_ASS_FILE:
              {
                memset(bg96_GNSS_valDate, 0x00, sizeof(bg96_GNSS_valDate));
                searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
                bg96_GNSS_valInt = convertStrToInt(searchField_res[0].field, 0, MAX_FIELD_SIZE);
                memcpy(bg96_GNSS_valDate, &searchField_res[1].field[1], sizeof(bg96_GNSS_valDate));
                //Check validità file assistenza
                if(bg96_is_GNSS_file_valid()) { bg96_gnss_reg.BIT.ASS_FILE_VALID=1; bg96_gnss_status=BG96_GNSS_START_S; }
                else { bg96_gnss_reg.BIT.ASS_FILE_VALID=0; bg96_gnss_reg.BIT.ASS_FILE_DW=0; bg96_gnss_status=BG96_GNSS_DW_ASS_FILE_S; }
              } break;
              
            case BG96_CMD_RTC_DATE_TIME:
              {
                memset(bg96_DataOra_UTC, 0x00, sizeof(bg96_DataOra_UTC));
                memcpy(bg96_DataOra_UTC, &mod_buff_Risposta[offset], sizeof(bg96_DataOra_UTC));
                if(!bg96_is_RTC_sync()) 
                {
                  bg96_RTC_error = 1; 						
                  bg96_gnss_status = BG96_GNSS_RTC_FAULT_S;
                }
              } break;
              
            default: break;
          }
          
          //---- Transizioni di stato intermedie ---- 
          if(bg96_gnss_status == BG96_GNSS_WAIT_RESTART_S)
          {
            set_bg96_status(BG96_PWR_OFF_S);
            return;						//Riavvio modulo
          }
          if(bg96_gnss_status == BG96_GNSS_DW_ASS_FILE_S)
          {
            set_bg96_status(BG96_HTTP_S);			//Download file
            return;
          }
          if(bg96_gnss_status == BG96_GNSS_RTC_FAULT_S)
          {
            bg96_gnss_reg.BIT.ASS_OPT = 0;
            bg96_gnss_status = BG96_GNSS_IDLE_S;
            set_bg96_status(BG96_GNSS_STANDARD_S);	        //RCT modem non sincronizzato: fallback a GNSS standard
            return;
          }
          //-----------------------------------------
          if(mod_Inc_Fifo(&fifo_index, FIFO_GNSS_ASSISTANCE_SIZE))
          {
            set_bg96_status(BG96_GNSS_ON_S);
            bg96_gnss_status = BG96_GNSS_START_S;
          }
        }
        else if(err == M_NO_AT_RESP)
          set_bg96_status(BG96_ERR_S);
      }
      break;
    case BG96_GNSS_STANDARD_S:
      //### Configurazione modalità GNSS standard ###
      switch(bg96_gnss_status)						//Selezione comandi da eseguire in base a stato GNSS
      {
        case BG96_GNSS_ENB_ASS_S: bg96_cmd_MASK=0x0000; break;          //Eseguo disattivazione GNSS assistito
        default: bg96_cmd_MASK=0x0006; break;				//Bypass CMD[1],[2]
      }
      
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_GNSS_STANDARD_SIZE);		//Salta comando corrente
      else
      {
        //Esegui comando corrente
        err = bg96_GNSS_Standard(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          switch(fifo_GNSS_Standard[fifo_index].at_cmd_type)
          {
            case BG96_CMD_QUERY_GNSS_ASS:
              {
                if(mod_buff_Risposta[offset] == '1') bg96_gnss_reg.BIT.ASS_ENB=1;
                else bg96_gnss_reg.BIT.ASS_ENB=0;
                
                if(bg96_gnss_reg.BIT.ASS_ENB) bg96_gnss_status=BG96_GNSS_ENB_ASS_S;		//Disattivo GNSS assistito (precedentemente attivato)
                else bg96_gnss_status=BG96_GNSS_START_S;					//GNSS assistito già disattivo
              } break;
              
            case BG96_CMD_GNSS_ASS_ENB: { bg96_gnss_status=BG96_GNSS_WAIT_RESTART_S; } break;
            
            default: break;
          }
          
          //---- Transizioni di stato intermedie ---- 
          if(bg96_gnss_status == BG96_GNSS_WAIT_RESTART_S)
          {
            set_bg96_status(BG96_PWR_OFF_S);
            return;										//Riavvio modulo
          }
          //-----------------------------------------
          if(mod_Inc_Fifo(&fifo_index, FIFO_GNSS_STANDARD_SIZE))
          {
            set_bg96_status(BG96_GNSS_ON_S);
            bg96_gnss_status = BG96_GNSS_START_S;
          }
        }
        else if(err == M_NO_AT_RESP)
          set_bg96_status(BG96_ERR_S);
      }
      break;
    case BG96_GNSS_ON_S:				
      //### Accensione/Configurazione GNSS ###
      if(bg96_RTC_error) {bg96_RTC_error=0; bg96_gnss_reg.BIT.RTC_ERROR=1;}
      bg96_tempo_GNSS = 0;
      
      if(ModemType == MOD_BG96)
        bg96_cmd_MASK = 0x0001;     //Bypass CMD[0]
      else
        bg96_cmd_MASK = 0x0002;     //Bypass CMD[1]
      
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_GNSS_CFG_SIZE);     //Salta comando corrente
      else
      {
        //Esegui comando corrente 
        err = bg96_GNSS_Config(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          if(fifo_GNSS_Config[fifo_index].at_cmd_type == BG96_CMD_GNSS_ON)
            bg96_reg.BIT.STATO_GNSS = 1;		//GNSS --> ON						
          
          if(mod_Inc_Fifo(&fifo_index, FIFO_GNSS_CFG_SIZE))
            set_bg96_status(BG96_GNSS_LOCATION_S);
        }
        else if(err == M_NO_AT_RESP)
        {		
          if(mod_cmdAttesa.codiceErrore == EC_GNSS_ALREADY_ACTIVE)
          {
            bg96_reg.BIT.STATO_GNSS = 1;		//GNSS --> ON		
            set_bg96_status(BG96_GNSS_LOCATION_S);
          }						
          else
            set_bg96_status(BG96_ERR_S);
        }
      }
      break;
    case BG96_GNSS_LOCATION_S:
      //### Lettura GNSS ###
      bg96_tempo_GNSS += PERIODO;		//Incremento tempo connessione GNSS
      err = bg96_GNSS_Location(&offset, 0);
      if(err == M_OK_AT_RESP)
      {
        searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
        bg96_parse_location(&bg96_latitudine_f, &bg96_longitudine_f);
        bg96_reg.BIT.LOC_GNSS = 1;
        if(bg96_gnss_reg.BIT.PRO_OPT) 
          set_bg96_status(BG96_GNSS_LOCATION_PRO_S);
        else 
          set_bg96_status(BG96_GNSS_OFF_S);
      }
      else if(err == M_NO_AT_RESP)
      {
        bg96_reg.BIT.LOC_GNSS = 0;
        set_bg96_status(BG96_ERR_S);
      }
      break;
    case BG96_GNSS_LOCATION_PRO_S:
      //### Lettura GNSS: aumento precisione ###
      bg96_tempo_GNSS += PERIODO;		//Incremento tempo connessione GNSS
      err = bg96_GNSS_Location(&offset, 1);
      if(err==M_OK_AT_RESP || err==M_NO_AT_RESP)
      {        
        if(err == M_OK_AT_RESP)
        {
          bg96_lettura_GNSS_PRO++;
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_parse_location(&bg96_latitudine_f_PRO, &bg96_longitudine_f_PRO);
        }
        if(++bg96_tentativi_GNSS_PRO < bg96_timeout_GNSS_PRO) break;      //Resto nello stato corrente (retry comando corrente)
        else
        {
          //Validazione efficacia aumento precisione (% di successo)
          if(bg96_lettura_GNSS_PRO >= (((uint16_t)bg96_tentativi_GNSS_PRO)*((uint16_t)50))/100) bg96_gnss_reg.BIT.LOC_GNSS_PRO=1;
          else bg96_gnss_reg.BIT.LOC_GNSS_PRO=0;
          set_bg96_status(BG96_GNSS_OFF_S);
        }
      }
      break;
    case BG96_GNSS_OFF_S:
      //### Spegnimento GNSS ###
      bg96_gnss_status = BG96_GNSS_IDLE_S;	//Reset
      err = bg96_GNSS_OFF(&offset);
      if(err == M_OK_AT_RESP)
      {
        bg96_reg.BIT.STATO_GNSS = 0;		//GNSS --> OFF
        set_bg96_status(BG96_IDLE_S);
      }
      else if(err == M_NO_AT_RESP)
      {		
        if(mod_cmdAttesa.codiceErrore == EC_GNSS_SESSION_NOT_ACTIVE)
        {
          bg96_reg.BIT.STATO_GNSS = 0;		//GNSS --> OFF (già spento)
          set_bg96_status(BG96_IDLE_S);
        }
        else
          set_bg96_status(BG96_ERR_S);
      }
      break;
    
    case BG96_PDP_ON_S:
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_PDP_SIZE);		//Salta comando corrente
      else
      {
        //Esegui comando corrente
        bg96_tempo_Registrazione_rete += PERIODO;
        //### Configuro e attivo contesto PDP ###
        err = bg96_PDP_Config(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          switch(fifo_PDP_Config[fifo_index].at_cmd_type)
          {
            case BG96_CMD_NTP: 
              bg96_reg.BIT.NTP_SYNC = 1; 			//Sincronizzazione NTP completata
              break;
              
            case BG96_CMD_RTC_DATE_TIME:
              {
                memset(bg96_DataOra_UTC, 0x00, sizeof(bg96_DataOra_UTC));
                memcpy(bg96_DataOra_UTC, &mod_buff_Risposta[offset], sizeof(bg96_DataOra_UTC));
                bg96_is_RTC_sync();							
              } break;            
              
            case BG96_CMD_QUERY_PDP: { bg96_cmd_MASK=0x0008; bg96_PDP_pending_cnt++; } break;			//Contesto PDP già attivo, salta Attivazione[3]
            
            default: break;
          }
          
          if(mod_Inc_Fifo(&fifo_index, FIFO_PDP_SIZE))						
            set_bg96_status(BG96_IDLE_S);						
        }
        else if(err == M_NO_AT_RESP)
        {
          //--- Eccezioni su errore ---
          if(fifo_PDP_Config[fifo_index].at_cmd_type == BG96_CMD_NTP)
          {
            bg96_reg.BIT.NTP_SYNC = 0;
            mod_Inc_Fifo(&fifo_index, FIFO_PDP_SIZE);		//Sincronizzazione NTP fallita: continuo con i comandi successivi
          }
          else if(fifo_PDP_Config[fifo_index].at_cmd_type == BG96_CMD_QUERY_PDP)
          {
            mod_Inc_Fifo(&fifo_index, FIFO_PDP_SIZE);		//Contesto PDP non attivo --> Eseguo Attivazione (e.g. comando successivo)
            bg96_PDP_pending_cnt = 0;
          }
          else
          {
            bg96_reg.BIT.PDP_ERROR = 1;
            set_bg96_status(BG96_ERR_S);
          }
        }
      }
      break;
      
    case BG96_MQTT_CONNECT_S:
      bg96_tempo_Registrazione_rete += PERIODO;
      //### Configuro e attivo connessione MQTT ###
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_MQTT_CONN_SIZE);		//Salta comando corrente
      else
      {
        //Esegui comando corrente
        err = bg96_MQTT_Connect(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          switch(fifo_MQTT_Connect[fifo_index].at_cmd_type)
          {			
            case BG96_CMD_RTC_DATE_TIME:
              {
                memset(bg96_DataOra_UTC, 0x00, sizeof(bg96_DataOra_UTC));
                memcpy(bg96_DataOra_UTC, &mod_buff_Risposta[offset], sizeof(bg96_DataOra_UTC));
                bg96_is_RTC_sync();								
              } break;
              
            case BG96_CMD_QUERY_PDP: { bg96_cmd_MASK=0x0004; bg96_PDP_pending_cnt++; } break;           //Contesto PDP già attivo, salta Attivazione[2]
            
            case BG96_CMD_MQTT_OPEN:
            case BG96_CMD_MQTT_CONN:
              {
                //Gestione codici errore MQTT
                searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
                bg96_MQTT_result =  searchField_res[1].field[0];
                if(bg96_MQTT_result!='0') { bg96_reg.BIT.MQTT_ERROR_CONN=1; set_bg96_status(BG96_ERR_S); return; }              //Errore connessione
              } break;
              
            case BG96_CMD_RSSI:
              {
                bg96_RSSI = convertStrToByte(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));								
              }	break;
              
            case BG96_CMD_INFO_CELLA:
              {                
                searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
                bg96_parse_info_cella(MOD_SERVING_CELL);
                bg96_num_celle = 1;                
              } break;
              
            case BG96_CMD_INFO_CELLA_VICINA:
              {
                offset_cell_start = 0;
                offset_cell_end = 0;
                for(bg96_num_celle=1; bg96_num_celle<MOD_MAX_CELL_NUM; )
                {
                  if(searchString(mod_buff_Risposta, "+QENG: \"neighbourcell\",", offset_cell_start, sizeof(mod_buff_Risposta), &offset_cell_end, 1, 1))
                  {
                    //Match trovato: cerco il prossimo                    
                    searchField(mod_buff_Risposta, ',', (offset_cell_start+offset_cell_end), sizeof(mod_buff_Risposta));
                    bg96_parse_info_cella(bg96_num_celle);
                    bg96_num_celle++;
                    offset_cell_start += offset_cell_end;                    
                  }
                  else//Match NON trovato: esco dal ciclo
                    break;            
                }
              } break;             
              
            default: break;
          }
          
          if(mod_Inc_Fifo(&fifo_index, FIFO_MQTT_CONN_SIZE))
            set_bg96_status(BG96_IDLE_S);													
        }
        else if(err == M_NO_AT_RESP)
        {
          //--- Eccezioni su errore ---
          if(fifo_MQTT_Connect[fifo_index].at_cmd_type == BG96_CMD_QUERY_PDP)
          {
            //Contesto PDP non attivo --> Eseguo Attivazione (e.g. comando successivo)							
            mod_Inc_Fifo(&fifo_index, FIFO_MQTT_CONN_SIZE);
            bg96_PDP_pending_cnt = 0;
          }
          else if(fifo_MQTT_Connect[fifo_index].at_cmd_type == BG96_CMD_INFO_CELLA_VICINA)
          {
            //Eccezione su risposta in caso di tecnologia diversa da GSM: passo ai comandi successivi
            //(e.g. in questo caso il comando per celle neighbour NON E' SUPPORTATO)
            mod_Inc_Fifo(&fifo_index, FIFO_MQTT_CONN_SIZE);
          }
          else { bg96_reg.BIT.MQTT_ERROR_CONN=1; set_bg96_status(BG96_ERR_S); }
        }
      }
      break;
    
    case BG96_MQTT_PUBLISH_S:
      //### Publish MQTT ###
      err = bg96_MQTT_Publish(fifo_index, &offset);
      if(err == M_OK_AT_RESP)
      {
        if(fifo_MQTT_Publish[fifo_index].at_cmd_type == BG96_CMD_MQTT_PUB_PAYLOAD)
        {
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_MQTT_result =  searchField_res[2].field[0];
          if(bg96_MQTT_result!='0') { bg96_reg.BIT.MQTT_ERROR_PUB=1; set_bg96_status(BG96_ERR_S); break; }		//Errore PUBLISH
        }
        
        if(mod_Inc_Fifo(&fifo_index, FIFO_MQTT_PUB_SIZE))          
          set_bg96_status(BG96_IDLE_S);
      }
      else if(err == M_NO_AT_RESP)
      {
        set_bg96_status(BG96_ERR_S);
        bg96_reg.BIT.MQTT_ERROR_PUB = 1;
      }
      break;
      
    case BG96_MQTT_SUBSCRIBE_S:
      //### Subscribe MQTT ###				
      err = bg96_MQTT_Subscribe(fifo_index, &offset);				
      if(err == M_OK_AT_RESP)
      {
        //Codici errore non controllati: attendo notifica
        if(fifo_MQTT_Subscribe[fifo_index].at_cmd_type == BG96_CMD_MQTT_SUBSCRIBE)
        {
          memset(mod_buff_data_RX, 0x00, sizeof(mod_buff_data_RX));
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_MQTT_RX_size = convertStrToInt(searchField_res[3].field, 0, MAX_FIELD_SIZE);             //Parse campo lunghezza payload
          offset_msg_RX = searchField_res[3].idx + 2;							//Bypass carattere '"' (inserito dal modulo per delimitare il payload)
          if( ((offset_msg_RX+bg96_MQTT_RX_size) <= sizeof(mod_buff_Risposta)) &&
             (bg96_MQTT_RX_size<=sizeof(mod_buff_data_RX)) &&							
               (offset_msg_RX<=sizeof(mod_buff_Risposta)) )
          {
            memcpy(mod_buff_data_RX, &mod_buff_Risposta[offset_msg_RX], bg96_MQTT_RX_size);		//Check OK: parse payload
            bg96_reg.BIT.MQTT_MSG_RX = 1;
          }
          else
          {
            set_bg96_status(BG96_ERR_S);								//Check fallito
            break;
          }
        }
        
        if(mod_Inc_Fifo(&fifo_index, FIFO_MQTT_SUB_SIZE))          
          set_bg96_status(BG96_IDLE_S);
      }
      else if(err == M_NO_AT_RESP)
      {
        //--- Eccezioni su errore ---
        if(fifo_MQTT_Subscribe[fifo_index].at_cmd_type == BG96_CMD_MQTT_UNSUBSCRIBE)
        {
          //Procedo comunque con la SUBSCRIBE
          mod_Inc_Fifo(&fifo_index, FIFO_MQTT_SUB_SIZE);							
        }
        else
          set_bg96_status(BG96_ERR_S);          //Messaggio non ricevuto o non presente
      }
      break;
    
    case BG96_FTP_S:
      //### Connessione server FTP (download aggiornamento FW) ###
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_FW_UP_SIZE);		//Salta comando corrente
      else
      {
        //Esegui comando corrente
        err = bg96_FW_Update(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          switch(fifo_FW_Update[fifo_index].at_cmd_type)
          {
            case BG96_CMD_FTP_OPEN:
            case BG96_CMD_FTP_SET_DIR:
            case BG96_CMD_FTP_GET_FILE:
            case BG96_CMD_FTP_CLOSE:
              {
                searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
                bg96_FTP_result[0] = convertStrToInt(searchField_res[0].field, 0, MAX_FIELD_SIZE);						
                bg96_FTP_result[1] = convertStrToInt(searchField_res[1].field, 0, MAX_FIELD_SIZE);
                if(fifo_FW_Update[fifo_index].at_cmd_type==BG96_CMD_FTP_GET_FILE) bg96_FTP_fSize=bg96_FTP_result[1];            //Dimensione file scaricato da FTP (B)
                if(bg96_FTP_result[0]!=0) { bg96_reg.BIT.FTP_ERROR=1; set_bg96_status(BG96_ERR_S); return; }			//Errore FTP
              } break;
              
            case BG96_CMD_QUERY_PDP: { bg96_cmd_MASK=0x0008; bg96_PDP_pending_cnt++; } break;           //Contesto PDP già attivo, salta Attivazione[3]
            
            default: break;
          }
          
          if(mod_Inc_Fifo(&fifo_index, FIFO_FW_UP_SIZE))          
            set_bg96_status(BG96_IDLE_S);
        }
        else if(err == M_NO_AT_RESP)
        {
          //--- Eccezioni su errore ---
          if(fifo_FW_Update[fifo_index].at_cmd_type == BG96_CMD_CANCELLA_FILE)
          {
            //File già cancellato --> Eseguo download
            mod_Inc_Fifo(&fifo_index, FIFO_FW_UP_SIZE);							
          }
          else if(fifo_FW_Update[fifo_index].at_cmd_type == BG96_CMD_QUERY_PDP)
          {
            //Contesto PDP non attivo --> Eseguo Attivazione (e.g. comando successivo)
            mod_Inc_Fifo(&fifo_index, FIFO_FW_UP_SIZE);
            bg96_PDP_pending_cnt = 0;
          }
          else { bg96_reg.BIT.FTP_ERROR=1; set_bg96_status(BG96_ERR_S); }
        }
      }
      break;
      
    case BG96_HTTP_S:
      bg96_tempo_HTTP += PERIODO;
      //### Connessione server HTTP (download file GNSS assistito) ###
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_HTTP_CFG_SIZE);		//Salta comando corrente
      else
      {
        //Esegui comando corrente
        err = bg96_HTTP_Config(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          switch(fifo_HTTP_Config[fifo_index].at_cmd_type)
          {
            case BG96_CMD_QUERY_PDP: { bg96_cmd_MASK=0x0002; bg96_PDP_pending_cnt++; } break;           //Contesto PDP già attivo, salta Attivazione[1]
            
            case BG96_CMD_HTTP_GET_HEADER:
              {
                searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
                bg96_HTTP_result[0] = convertStrToInt(searchField_res[0].field, 0, MAX_FIELD_SIZE);
                bg96_HTTP_result[1] = convertStrToInt(searchField_res[1].field, 0, MAX_FIELD_SIZE);
                bg96_HTTP_fSize = convertStrToInt(searchField_res[2].field, 0, MAX_FIELD_SIZE);		//Dimensione file HTTP da scaricare (B)
                if(bg96_HTTP_result[0]!=0 || bg96_HTTP_result[1]!=EC_HTTP_OK) { 
                  bg96_reg.BIT.HTTP_ERROR=1; set_bg96_status(BG96_ERR_S); return; }
              } break;
              
            case BG96_CMD_HTTP_GET_FILE:
              {
                bg96_HTTP_result[2] = convertStrToInt(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));
                if(bg96_HTTP_result[2] != 0) { bg96_reg.BIT.HTTP_ERROR=1; set_bg96_status(BG96_ERR_S); return; }
              } break;
              
            default: break;
          }
          
          if(mod_Inc_Fifo(&fifo_index, FIFO_HTTP_CFG_SIZE))
          {		
            bg96_gnss_reg.BIT.ASS_FILE_DW=1; bg96_gnss_status=BG96_GNSS_INJECT_ASS_S;
            set_bg96_status(BG96_GNSS_ASSISTANCE_S);            //Download file completato: riprendo configurazione
          }
        }
        else if(err == M_NO_AT_RESP)
        {
          //--- Eccezioni su errore ---
          if(fifo_HTTP_Config[fifo_index].at_cmd_type == BG96_CMD_QUERY_PDP)
          {
            //Contesto PDP non attivo (ad es. dopo prima attivazione GNSS assistito, perchè passo da restart modulo) --> Eseguo Attivazione (e.g. comando successivo)
            mod_Inc_Fifo(&fifo_index, FIFO_HTTP_CFG_SIZE);
            bg96_PDP_pending_cnt = 0;
          }
          else if(fifo_HTTP_Config[fifo_index].at_cmd_type == BG96_CMD_CANCELLA_FILE)
          {
            //File già cancellato --> Eseguo download
            mod_Inc_Fifo(&fifo_index, FIFO_HTTP_CFG_SIZE);							
          }
          else { bg96_reg.BIT.HTTP_ERROR=1; set_bg96_status(BG96_ERR_S); }
        }
      }
      break;
      
    case BG96_CLOSE_CONN_S:
      //### Chiusura connessioni ### 
      err = bg96_CloseConn(fifo_index, &offset);
      if(err == M_OK_AT_RESP)
      {
        if(mod_Inc_Fifo(&fifo_index, FIFO_CLOSE_CONN_SIZE))						
          set_bg96_status(BG96_IDLE_S);
      }
      else if(err == M_NO_AT_RESP)
      {
        //--- Eccezioni su errore ---
        if(fifo_CloseConn[fifo_index].at_cmd_type == BG96_CMD_MQTT_DISCONN)
        {
          //Chiusura connessioni fallita, proseguo comunque con disattivazione contesto PDP
          mod_Inc_Fifo(&fifo_index, FIFO_CLOSE_CONN_SIZE);													
        }
        else set_bg96_status(BG96_ERR_S);
      }
      break;
      
    case BG96_GP_SETTINGS_S:
      //### Impostazioni general-purpose modem: costellazione GNSS ###
      if(ModemType == MOD_BG96)
        bg96_cmd_MASK = 0x0001;     //Bypass CMD[0]
      
      if(bg96_bypass_cmd(fifo_index, bg96_cmd_MASK)) 
        mod_Inc_Fifo(&fifo_index, FIFO_GP_SETTINGS_CFG_SIZE);		//Salta comando corrente
      else
      {
        //Esegui comando corrente    
        err = bg96_GpSettings_Config(fifo_index, &offset);
        if(err == M_OK_AT_RESP)
        {
          if(mod_Inc_Fifo(&fifo_index, FIFO_GP_SETTINGS_CFG_SIZE))
            set_bg96_status(BG96_IDLE_S);
        }
        else if(err == M_NO_AT_RESP)
        {		      
          set_bg96_status(BG96_ERR_S);
        }
      }
      break;
    
    case BG96_ERR_S:
      //### Stato di errore ###
      bg96_err_cnt++;
      bg96_last_error = mod_cmdAttesa.lastCmd.cmdCode;			
      if(bg96_status_prev==BG96_PDP_ON_S ||
         bg96_status_prev==BG96_MQTT_CONNECT_S ||
           bg96_status_prev==BG96_MQTT_PUBLISH_S ||
             bg96_status_prev==BG96_MQTT_SUBSCRIBE_S ||
               bg96_status_prev==BG96_FTP_S ||
                 bg96_status_prev==BG96_CLOSE_CONN_S ) 
      {
        //Errore GESTITO
        set_bg96_status(BG96_IDLE_S); 
      }
      else if(bg96_status_prev==BG96_GNSS_ON_S || bg96_status_prev==BG96_GNSS_LOCATION_S)
      {
        //Errore GESTITO
        set_bg96_status(BG96_GNSS_OFF_S);
      }
      else if(bg96_status_prev==BG96_GNSS_ASSISTANCE_S || bg96_status_prev==BG96_HTTP_S)
      {
        //Errore GESTITO
        bg96_gnss_reg.BIT.GENERIC_ERROR = 1;
        set_bg96_status(BG96_IDLE_S);
      }
      else 
      { 
        //Errore IMPREVISTO
        bg96_reg.BIT.GENERIC_ERROR = 1;
        set_bg96_status(BG96_IDLE_S); 
      }
      break;
      
    default: set_bg96_status(BG96_IDLE_S); break;       //Safety  
  }	
}


/* Processa byte ricevuti su seriale. */
void bg96_ProcessaByte(uint8_t data)
{
  //### Modulo pronto a ricevere comandi ###
  //<<< Buffer ricerca risposta >>>
  if(mod_cmdAttesa.attesaRisposta && (mod_buff_Risposta_idx < sizeof(mod_buff_Risposta)))
  {      
    mod_buff_Risposta[mod_buff_Risposta_idx++] = data;      
  }
}


/* Get info statiche modulo:
*  - check COM
*  - versione FW AT
*  - ICCID SIM
*  - ENB GNSS assistito
*  - costellazione GNSS
*  - RAT scan mode
*  - RAT LTE mode
*/
ErrorCode_t bg96_GetInfo(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  
  if(fifo_GetInfo[fifo_index].at_cmd_type == BG96_CMD_AT)
  {
    mod_Push_Cmd(fifo_GetInfo[fifo_index].at_cmd, fifo_GetInfo[fifo_index].at_cmd_type, fifo_GetInfo[fifo_index].at_cmd_maxRetry, modello);
    err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_GetInfo[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
  }
  else
  {
    mod_Push_Cmd(fifo_GetInfo[fifo_index].at_cmd, fifo_GetInfo[fifo_index].at_cmd_type, fifo_GetInfo[fifo_index].at_cmd_maxRetry, modello);
    err = mod_Read_Response(BG96_300MS_TO, 1, fifo_GetInfo[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
  }
  
  return err;
}


/* Set impostazioni RAT:
*  - RAT scan mode
*  - RAT LTE mode
*/
ErrorCode_t bg96_ConfigRAT(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  
  switch(fifo_ConfigRAT[fifo_index].at_cmd_type)
  {
    case BG96_CMD_AIRPLANE_MODE:
      mod_Push_Cmd(fifo_ConfigRAT[fifo_index].at_cmd, fifo_ConfigRAT[fifo_index].at_cmd_type, fifo_ConfigRAT[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 15, fifo_ConfigRAT[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_ConfigRAT[fifo_index].at_cmd, fifo_ConfigRAT[fifo_index].at_cmd_type, fifo_ConfigRAT[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_ConfigRAT[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}


/* Get info registrazione rete:
*  - Phone mode
*  - Stato registrazione rete cellulare
*/
ErrorCode_t bg96_NetworkReg(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  
  switch(fifo_NetworkReg[fifo_index].at_cmd_type)
  {
    case BG96_CMD_REG_RETE:
      mod_Push_Cmd(fifo_NetworkReg[fifo_index].at_cmd, fifo_NetworkReg[fifo_index].at_cmd_type, fifo_NetworkReg[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_NetworkReg[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_FULL_FUNC_MODE:
      mod_Push_Cmd(fifo_NetworkReg[fifo_index].at_cmd, fifo_NetworkReg[fifo_index].at_cmd_type, fifo_NetworkReg[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 15, fifo_NetworkReg[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_NetworkReg[fifo_index].at_cmd, fifo_NetworkReg[fifo_index].at_cmd_type, fifo_NetworkReg[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_NetworkReg[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}


/* Configurazione GNSS assistito. */
ErrorCode_t bg96_GNSS_Assistance(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  uint8_t GNSS_TIME_T[] = "\",1,1,5";
  
  switch(fifo_GNSS_Assistance[fifo_index].at_cmd_type)
  {
    case BG96_CMD_GNSS_ASS_TIME:
      memset(bg96_GNSS_buffer, 0x00, sizeof(bg96_GNSS_buffer));
      offset += concatenaStringhe(bg96_GNSS_buffer, offset, fifo_GNSS_Assistance[fifo_index].at_cmd, sizeof(bg96_GNSS_buffer));				//Add CMD header
      bg96_GNSS_buffer[offset++]='2'; bg96_GNSS_buffer[offset++]='0';											//Add prefisso anno
      memcpy(&bg96_GNSS_buffer[offset], bg96_DataOra_UTC, sizeof(bg96_DataOra_UTC)); offset+=sizeof(bg96_DataOra_UTC);					//Add data/ora corrente
      offset += concatenaStringhe(bg96_GNSS_buffer, offset, GNSS_TIME_T, sizeof(bg96_GNSS_buffer));							//Add MQTT user + password
      mod_Push_Cmd(bg96_GNSS_buffer, fifo_GNSS_Assistance[fifo_index].at_cmd_type, fifo_GNSS_Assistance[fifo_index].at_cmd_maxRetry, modello);          
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_GNSS_Assistance[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_GNSS_Assistance[fifo_index].at_cmd, fifo_GNSS_Assistance[fifo_index].at_cmd_type, fifo_GNSS_Assistance[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_GNSS_Assistance[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}
/* Configurazione GNSS standard. */
ErrorCode_t bg96_GNSS_Standard(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  
  mod_Push_Cmd(fifo_GNSS_Standard[fifo_index].at_cmd, fifo_GNSS_Standard[fifo_index].at_cmd_type, fifo_GNSS_Standard[fifo_index].at_cmd_maxRetry, modello);
  err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_GNSS_Standard[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
  
  return err;
}

/* Accensione/Configurazione GNSS. */
ErrorCode_t bg96_GNSS_Config(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  
  mod_Push_Cmd(fifo_GNSS_Config[fifo_index].at_cmd, fifo_GNSS_Config[fifo_index].at_cmd_type, fifo_GNSS_Config[fifo_index].at_cmd_maxRetry, modello);
  err = mod_Read_Response(BG96_300MS_TO, 1, fifo_GNSS_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
  
  return err;
}

/* Spegnimento GNSS. */
ErrorCode_t bg96_GNSS_OFF(uint16_t* offsetResp)
{
  ErrorCode_t err;
  
  mod_Push_Cmd((uint8_t*)"AT+QGPSEND", BG96_CMD_GNSS_OFF, 1, modello);
  err = mod_Read_Response(BG96_300MS_TO, 1, AT_OK_STR, offsetResp, AT_ERROR_STR);
  
  return err;
}

/* Lettura info GNSS.
*
* @param enh_prec:'0' precisione standard,'1': precisione aumentata
*/
ErrorCode_t bg96_GNSS_Location(uint16_t* offsetResp, uint8_t enh_prec)
{
  ErrorCode_t err;
  
  if(enh_prec)
  {
    mod_Push_Cmd((uint8_t*)"AT+QGPSLOC=2", BG96_CMD_GNSS_LOC, 1, modello);
    err = mod_Read_Response(BG96_5000MS_TO, 1, (uint8_t*)"+QGPSLOC: ", offsetResp, AT_ERROR_STR);
  }
  else
  {
    mod_Push_Cmd((uint8_t*)"AT+QGPSLOC=2", BG96_CMD_GNSS_LOC, /*1*/ bg96_timeout_GNSS, modello);         //modificare qui x forzare timeout GPS
    err = mod_Read_Response(BG96_1000MS_TO, 5, (uint8_t*)"+QGPSLOC: ", offsetResp, AT_ERROR_STR);
  }
  
  return err;
}


/* Configurazione/Apertura comunicazione verso server (contesto PDP, NTP, HTTP).
*  Richiesta info rete (RSSI, MCC-MCN operatore, TrasmissionChannel rete operatore). 
*
* TODO: rendere APN configurabile ?
*
* Nota: richiesta info rete posticipata in stato BG96_PDP_ON_S, perchè se richieste nello stato BG96_NETWORK_REG_S in alcuni casi (CAT-M1) le info rete non risultavano aggiornate.
*/
ErrorCode_t bg96_PDP_Config(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  
  switch(fifo_PDP_Config[fifo_index].at_cmd_type)
  {
    case BG96_CMD_QUERY_PDP:
      mod_Push_Cmd(fifo_PDP_Config[fifo_index].at_cmd, fifo_PDP_Config[fifo_index].at_cmd_type, fifo_PDP_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_PDP_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_PDP_ON:
      mod_Push_Cmd(fifo_PDP_Config[fifo_index].at_cmd, fifo_PDP_Config[fifo_index].at_cmd_type, fifo_PDP_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 90, fifo_PDP_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_NTP:
      mod_Push_Cmd(fifo_PDP_Config[fifo_index].at_cmd, fifo_PDP_Config[fifo_index].at_cmd_type, fifo_PDP_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 30, fifo_PDP_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_PDP_Config[fifo_index].at_cmd, fifo_PDP_Config[fifo_index].at_cmd_type, fifo_PDP_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_PDP_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}


/* Configurazione/Apertura comunicazione verso server (MQTT).
*
* TODO: rendere settings MQTT configurabili ?
*/
ErrorCode_t bg96_MQTT_Connect(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  uint8_t MQTT_CONN_T[] = "\","MQTT_USER_NAME","MQTT_PASSWORD;
  
  switch(fifo_MQTT_Connect[fifo_index].at_cmd_type)
  {
    case BG96_CMD_QUERY_PDP:
      mod_Push_Cmd(fifo_MQTT_Connect[fifo_index].at_cmd, fifo_MQTT_Connect[fifo_index].at_cmd_type, fifo_MQTT_Connect[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_MQTT_Connect[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_PDP_ON:
      mod_Push_Cmd(fifo_MQTT_Connect[fifo_index].at_cmd, fifo_MQTT_Connect[fifo_index].at_cmd_type, fifo_MQTT_Connect[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 90, fifo_MQTT_Connect[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_MQTT_OPEN:
      mod_Push_Cmd(fifo_MQTT_Connect[fifo_index].at_cmd, fifo_MQTT_Connect[fifo_index].at_cmd_type, fifo_MQTT_Connect[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 75, fifo_MQTT_Connect[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_MQTT_CONN:
      memset(bg96_MQTT_buffer, 0x00, sizeof(bg96_MQTT_buffer));
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, fifo_MQTT_Connect[fifo_index].at_cmd, sizeof(bg96_MQTT_buffer));				//Add CMD header
      memcpy(&bg96_MQTT_buffer[offset], bg96_ICCID, sizeof(bg96_ICCID)); offset+=sizeof(bg96_ICCID);							//Add MQTT client ID (ICCID)
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, MQTT_CONN_T, sizeof(bg96_MQTT_buffer));							//Add MQTT user + password
      mod_Push_Cmd(bg96_MQTT_buffer, fifo_MQTT_Connect[fifo_index].at_cmd_type, fifo_MQTT_Connect[fifo_index].at_cmd_maxRetry, modello);                
      //TODO: timeout in base a setting MQTT (<pkt_timeout>) ?
      err = mod_Read_Response(BG96_1000MS_TO, 5, fifo_MQTT_Connect[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_MQTT_Connect[fifo_index].at_cmd, fifo_MQTT_Connect[fifo_index].at_cmd_type, fifo_MQTT_Connect[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_MQTT_Connect[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}


/* Publish messaggio MQTT (TOPIC OUT). */
ErrorCode_t bg96_MQTT_Publish(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  int16_t res = 0;
  
  switch(fifo_MQTT_Publish[fifo_index].at_cmd_type)
  {		
    case BG96_CMD_MQTT_PUB_TOPIC:
      memset(bg96_MQTT_buffer, 0x00, sizeof(bg96_MQTT_buffer));
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, fifo_MQTT_Publish[fifo_index].at_cmd, sizeof(bg96_MQTT_buffer));				        //Add CMD header
      res = snprintf((char*)&bg96_MQTT_buffer[offset], sizeof(bg96_MQTT_buffer)-offset, "%d,%d,%s,", bg96_MQTT_msgId_publish, MQTT_QoS_PUBLISH, NOT_RETAIN);    //Add Msg-Id + QoS + Retain flag 
      if(res>0) offset+=res;
      if(offset<sizeof(bg96_MQTT_buffer)-1) bg96_MQTT_buffer[offset++]='"';
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, MQTT_TOPIC_OUT, sizeof(bg96_MQTT_buffer));							        //Add TOPIC
      if(offset<sizeof(bg96_MQTT_buffer)-1) bg96_MQTT_buffer[offset++]='"';
      mod_Push_Cmd(bg96_MQTT_buffer, fifo_MQTT_Publish[fifo_index].at_cmd_type, fifo_MQTT_Publish[fifo_index].at_cmd_maxRetry, modello);                
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_MQTT_Publish[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_MQTT_PUB_PAYLOAD:
      mod_Push_Cmd(mod_buff_data_TX, fifo_MQTT_Publish[fifo_index].at_cmd_type, fifo_MQTT_Publish[fifo_index].at_cmd_maxRetry, MOD_CTRL_Z_TAILER);
      //TODO: timeout in base a setting MQTT (<pkt_timeout>*<retry_times>) ?
      err = mod_Read_Response(BG96_1000MS_TO, 15, fifo_MQTT_Publish[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_MQTT_Publish[fifo_index].at_cmd, fifo_MQTT_Publish[fifo_index].at_cmd_type, fifo_MQTT_Publish[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_MQTT_Publish[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}

/* Subscribe messaggio MQTT.
*  Eventuale Publish messaggio MQTT per pulizia coda comandi (TOPIC IN).
*
* TODO: rendere settings MQTT configurabili ?
*/
ErrorCode_t bg96_MQTT_Subscribe(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  int16_t res = 0;
  
  switch(fifo_MQTT_Subscribe[fifo_index].at_cmd_type)
  {
    case BG96_CMD_MQTT_UNSUBSCRIBE:
      memset(bg96_MQTT_buffer, 0x00, sizeof(bg96_MQTT_buffer));
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, fifo_MQTT_Subscribe[fifo_index].at_cmd, sizeof(bg96_MQTT_buffer));				//Add CMD header
      res = snprintf((char*)&bg96_MQTT_buffer[offset], sizeof(bg96_MQTT_buffer)-offset, "%d,", bg96_MQTT_msgId_subscribe);                              //Add Msg-Id
      if(res>0) offset+=res;
      if(offset<sizeof(bg96_MQTT_buffer)-1) bg96_MQTT_buffer[offset++]='"';
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, MQTT_TOPIC_IN, sizeof(bg96_MQTT_buffer));							//Add TOPIC
      if(offset<sizeof(bg96_MQTT_buffer)-1) bg96_MQTT_buffer[offset++]='"';				
      mod_Push_Cmd(bg96_MQTT_buffer, fifo_MQTT_Subscribe[fifo_index].at_cmd_type, fifo_MQTT_Subscribe[fifo_index].at_cmd_maxRetry, modello);            
      //TODO: timeout in base a setting MQTT (<pkt_timeout>*<retry_times>) ?
      err = mod_Read_Response(BG96_1000MS_TO, 15/*10*/, fifo_MQTT_Subscribe[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_MQTT_SUBSCRIBE:
      memset(bg96_MQTT_buffer, 0x00, sizeof(bg96_MQTT_buffer));
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, fifo_MQTT_Subscribe[fifo_index].at_cmd, sizeof(bg96_MQTT_buffer));				//Add CMD header
      res = snprintf((char*)&bg96_MQTT_buffer[offset], sizeof(bg96_MQTT_buffer)-offset, "%d,", bg96_MQTT_msgId_subscribe);                              //Add Msg-Id
      if(res>0) offset+=res;
      if(offset<sizeof(bg96_MQTT_buffer)-1) bg96_MQTT_buffer[offset++]='"';
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, MQTT_TOPIC_IN, sizeof(bg96_MQTT_buffer));							//Add TOPIC
      if(offset<sizeof(bg96_MQTT_buffer)-1) bg96_MQTT_buffer[offset++]='"';
      res = snprintf((char*)&bg96_MQTT_buffer[offset], sizeof(bg96_MQTT_buffer)-offset, ",%d", MQTT_QoS_SUBSCRIBE);                                     //Add QoS
      if(res>0) offset+=res;											
      mod_Push_Cmd(bg96_MQTT_buffer, fifo_MQTT_Subscribe[fifo_index].at_cmd_type, fifo_MQTT_Subscribe[fifo_index].at_cmd_maxRetry, modello);            
      //TODO: timeout in base a setting MQTT (<pkt_timeout>*<retry_times>) ?
      err = mod_Read_Response(BG96_1000MS_TO, 15/*10*/, fifo_MQTT_Subscribe[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_MQTT_PUB_TOPIC:		//Publish per pulire coda comandi (invia TOPIC)
      memset(bg96_MQTT_buffer, 0x00, sizeof(bg96_MQTT_buffer));
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, fifo_MQTT_Subscribe[fifo_index].at_cmd, sizeof(bg96_MQTT_buffer));				//Add CMD header
      if(offset<sizeof(bg96_MQTT_buffer)-1) bg96_MQTT_buffer[offset++]='"';
      offset += concatenaStringhe(bg96_MQTT_buffer, offset, MQTT_TOPIC_IN, sizeof(bg96_MQTT_buffer));							//Add TOPIC
      if(offset<sizeof(bg96_MQTT_buffer)-1) bg96_MQTT_buffer[offset++]='"';
      mod_Push_Cmd(bg96_MQTT_buffer, fifo_MQTT_Subscribe[fifo_index].at_cmd_type, fifo_MQTT_Subscribe[fifo_index].at_cmd_maxRetry, modello);			
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_MQTT_Subscribe[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_MQTT_PUB_PAYLOAD:		//Publish per pulire coda comandi (invia payload vuoto)
      mod_Push_Cmd(fifo_MQTT_Subscribe[fifo_index].at_cmd, fifo_MQTT_Subscribe[fifo_index].at_cmd_type, fifo_MQTT_Subscribe[fifo_index].at_cmd_maxRetry, MOD_CTRL_Z_TAILER);
      //TODO: timeout in base a setting MQTT (<pkt_timeout>*<retry_times>) ?
      err = mod_Read_Response(BG96_1000MS_TO, 15, fifo_MQTT_Subscribe[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_MQTT_Subscribe[fifo_index].at_cmd, fifo_MQTT_Subscribe[fifo_index].at_cmd_type, fifo_MQTT_Subscribe[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_MQTT_Subscribe[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}


/* Configurazione/Apertura comunicazione verso server FTP (aggiornamento FW). */
ErrorCode_t bg96_FW_Update(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  
  switch(fifo_FW_Update[fifo_index].at_cmd_type)
  {
    case BG96_CMD_CANCELLA_FILE:
      mod_Push_Cmd(fifo_FW_Update[fifo_index].at_cmd, fifo_FW_Update[fifo_index].at_cmd_type, fifo_FW_Update[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_FW_Update[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_QUERY_PDP:
      mod_Push_Cmd(fifo_FW_Update[fifo_index].at_cmd, fifo_FW_Update[fifo_index].at_cmd_type, fifo_FW_Update[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_FW_Update[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_PDP_ON:
      mod_Push_Cmd(fifo_FW_Update[fifo_index].at_cmd, fifo_FW_Update[fifo_index].at_cmd_type, fifo_FW_Update[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 90, fifo_FW_Update[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_FTP_OPEN:
    case BG96_CMD_FTP_SET_DIR:
    case BG96_CMD_FTP_CLOSE:
      mod_Push_Cmd(fifo_FW_Update[fifo_index].at_cmd, fifo_FW_Update[fifo_index].at_cmd_type, fifo_FW_Update[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 90, fifo_FW_Update[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_FTP_GET_FILE:
      memset(bg96_HTTP_FTP_buffer, 0x00, sizeof(bg96_HTTP_FTP_buffer));
      offset += concatenaStringhe(bg96_HTTP_FTP_buffer, offset, fifo_FW_Update[fifo_index].at_cmd, sizeof(bg96_HTTP_FTP_buffer));					//Add CMD header
      offset += snprintf((char*)&bg96_HTTP_FTP_buffer[offset], sizeof(bg96_HTTP_FTP_buffer)-offset, "\"%s", FTP_FILE);							//Add file name					
      offset += snprintf((char*)&bg96_HTTP_FTP_buffer[offset], sizeof(bg96_HTTP_FTP_buffer)-offset, "_%01d_%02d.bin\",", bg96_FTP_version, bg96_FTP_subversion);	//Add version + subversion
      offset += snprintf((char*)&bg96_HTTP_FTP_buffer[offset], sizeof(bg96_HTTP_FTP_buffer)-offset, "%s", LOCAL_UFS_FILE);						//Add UFS file name					
      mod_Push_Cmd(bg96_HTTP_FTP_buffer, fifo_FW_Update[fifo_index].at_cmd_type, fifo_FW_Update[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 270, fifo_FW_Update[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_FW_Update[fifo_index].at_cmd, fifo_FW_Update[fifo_index].at_cmd_type, fifo_FW_Update[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_FW_Update[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}


/* Configurazione/Richiesta GET HTTP (download file GNSS assistito). */
ErrorCode_t bg96_HTTP_Config(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  
  switch(fifo_HTTP_Config[fifo_index].at_cmd_type)
  {
    case BG96_CMD_PDP_ON:
      mod_Push_Cmd(fifo_HTTP_Config[fifo_index].at_cmd, fifo_HTTP_Config[fifo_index].at_cmd_type, fifo_HTTP_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 90, fifo_HTTP_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_HTTP_CFG_URL:
      memset(bg96_HTTP_FTP_buffer, 0x00, sizeof(bg96_HTTP_FTP_buffer));
      offset += concatenaStringhe(bg96_HTTP_FTP_buffer, offset, fifo_HTTP_Config[fifo_index].at_cmd, sizeof(bg96_HTTP_FTP_buffer));             //Add CMD header			
      offset += snprintf((char*)&bg96_HTTP_FTP_buffer[offset], sizeof(bg96_HTTP_FTP_buffer)-offset, "%d", (sizeof(HTTP_URL_ADDR)-1));		
      mod_Push_Cmd(bg96_HTTP_FTP_buffer, fifo_HTTP_Config[fifo_index].at_cmd_type, fifo_HTTP_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_HTTP_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    case BG96_CMD_HTTP_URL:
    case BG96_CMD_HTTP_GET_HEADER:
    case BG96_CMD_HTTP_GET_FILE:
      mod_Push_Cmd(fifo_HTTP_Config[fifo_index].at_cmd, fifo_HTTP_Config[fifo_index].at_cmd_type, fifo_HTTP_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 60, fifo_HTTP_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_HTTP_Config[fifo_index].at_cmd, fifo_HTTP_Config[fifo_index].at_cmd_type, fifo_HTTP_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 1, fifo_HTTP_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}


/* Chiusura connessioni/contesto PDP. */
ErrorCode_t bg96_CloseConn(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  
  switch(fifo_CloseConn[fifo_index].at_cmd_type)
  {		
    case BG96_CMD_PDP_OFF:
      mod_Push_Cmd(fifo_CloseConn[fifo_index].at_cmd, fifo_CloseConn[fifo_index].at_cmd_type, fifo_CloseConn[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 30, fifo_CloseConn[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_CloseConn[fifo_index].at_cmd, fifo_CloseConn[fifo_index].at_cmd_type, fifo_CloseConn[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_1000MS_TO, 10, fifo_CloseConn[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}


/* Configurazione impostazioni general-purpose:
*  - costellazione GNSS
*/
ErrorCode_t bg96_GpSettings_Config(uint8_t fifo_index, uint16_t* offsetResp)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  
  switch(fifo_GpSettings_Config[fifo_index].at_cmd_type)
  {
    case BG96_CMD_GNSS_CONST:
      memset(bg96_GpSettings_buffer, 0x00, sizeof(bg96_GpSettings_buffer));
      offset += concatenaStringhe(bg96_GpSettings_buffer, offset, fifo_GpSettings_Config[fifo_index].at_cmd, sizeof(bg96_GpSettings_buffer));   //Add CMD header
      offset += snprintf((char*)&bg96_GpSettings_buffer[offset], sizeof(bg96_GpSettings_buffer)-offset, "%d", bg96_gnss_constellation);	        //Add tipo costellazione
      mod_Push_Cmd(bg96_GpSettings_buffer, fifo_GpSettings_Config[fifo_index].at_cmd_type, fifo_GpSettings_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_GpSettings_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
      
    default:
      mod_Push_Cmd(fifo_GpSettings_Config[fifo_index].at_cmd, fifo_GpSettings_Config[fifo_index].at_cmd_type, fifo_GpSettings_Config[fifo_index].at_cmd_maxRetry, modello);
      err = mod_Read_Response(BG96_300MS_TO, 1, fifo_GpSettings_Config[fifo_index].at_resp, offsetResp, AT_ERROR_STR);
      break;
  }
  
  return err;
}
