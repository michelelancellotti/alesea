/**
******************************************************************************
* File          	: modem_utils.h
* Versione libreria	: 0.01
* Descrizione        	: Funzioni di appoggio libreria.
******************************************************************************
*
* COPYRIGHT(c) 2017 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MODEM_UTILS_H
#define __MODEM_UTILS_H
#ifdef __cplusplus
extern "C" {
#endif
  
  /* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include "mTypes.h"
  
  /* Defines */
#define	MAX_SEARCH_FIELD_RES		16              //Numero MAX risultati di 'searchField'
#define MAX_FIELD_SIZE			16		//[B], dimensione MAX campo (risultato di 'searchField')
  
#define IS_BIT_SET(REG,POS)     	((REG & (1<<(POS)))>0?TRUE:FALSE)
#define SET_REG_BIT(REG,POS)    	((REG) |= (1<<(POS)))
#define CLR_REG_BIT(REG,POS)    	((REG) &= ~((1)<<(POS)))	 
  
  
/* Types & global variables */

typedef struct {
  uint8_t idx;
  const uint8_t* stringa;
  uint8_t size;
} Pattern_t;

typedef struct {
  uint16_t idx;
  uint8_t field[MAX_FIELD_SIZE];
} Field_t;


extern Field_t searchField_res[MAX_SEARCH_FIELD_RES];
  
/* Global functions */
extern uint8_t searchString(uint8_t* buffer, uint8_t* string, uint16_t start, uint16_t end, uint16_t* offset, uint8_t caseSensitive, uint8_t checkSpace);
extern uint8_t searchField(uint8_t* buffer, uint8_t field_limit, uint16_t start, uint16_t end);
extern uint8_t caseReverse(uint8_t car);
extern unsigned char convertStrToByte(unsigned char* str, uint16_t start, uint16_t end);
extern uint16_t convertStrToInt(unsigned char* str, uint16_t start, uint16_t end);
extern uint16_t concatenaStringhe(uint8_t* in_1, uint16_t offset_in_1, uint8_t* in_2, uint16_t max_length_in_2);
extern uint8_t trovaPattern(Pattern_t* pattern, uint8_t data);
extern void resetPattern(Pattern_t* pattern);
extern uint8_t isAlfaNumerico(uint8_t* src, uint16_t src_size);
  
  
  
#endif	/* End __MODEM_UTILS_H */ 
  