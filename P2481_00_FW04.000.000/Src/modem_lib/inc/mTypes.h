/**
******************************************************************************
* File          	: mTypes.h
* Versione libreria	: 0.01
* Descrizione        	: Codici errore API.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#ifndef _M_TYPES_H
#define _M_TYPES_H

/* Includes */
#include <string.h>
#include <stdint.h>


/* Define & Typedef */

#define MODEM_LIB_DEBUG


typedef enum {
  M_NO_ERROR 		= 0x00,
  QUEUE_EMPTY		= 0x01,         /* Coda comandi svuotata. */
  LIB_INIT_ERROR 	= 0xFF,         /* Errore inizializzazione libreria. */
  MODEL_INIT_ERROR	= 0xFE,         /* Errore inizializzazione modello modulo. */
  M_CMD_PARAM_ERROR	= 0xFD,         /* Errore scrittura comando: parametri errati. */
  QUEUE_CMD_ERROR	= 0xFC,         /* Impossibile aggiungere ulteriori comandi in coda: coda satura o errore parametri. */	
  GENERIC_PARAM_ERROR	= 0xFB,         /* Errore set parametro. */
  GENERIC_BUSY_STATE	= 0xFA,         /* Generico stato busy. */
  M_HW_ERROR            = 0xF9,
  M_WAIT_AT_RESP        = 0xF8,         /* Attesa risposta comando AT. */
  M_NO_AT_RESP          = 0xF7,         /* Risposta comando AT non ricevuta o errata. */
  M_OK_AT_RESP          = 0xF6,         /* Risposta comando AT ricevuta. */
  M_TO_AT_END           = 0xF5,         /* Timeout comando AT trascorso. */
  M_NO_AT_RESP_RETRY	= 0xF4,         /* Risposta comando AT non ricevuta o errata: riprovo a inviare comando. */
} ErrorCode_t;


#define round(x) ((x)>=0?(long)((x)+0.5):(long)((x)-0.5))

#define IS_BIT_SET(REG,POS)     ((REG & (1<<(POS)))>0?TRUE:FALSE)
#define SET_REG_BIT(REG,POS)    ((REG) |= (1<<(POS)))
#define CLR_REG_BIT(REG,POS)    ((REG) &= ~((1)<<(POS)))




#endif	/* End _M_TYPES_H */
