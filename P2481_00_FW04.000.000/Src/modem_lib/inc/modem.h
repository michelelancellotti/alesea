/**
******************************************************************************
* File          	: modem.h
* Versione libreria	: 0.01
* Descrizione        	: Supporto Modem (livello applicativo).
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#ifndef _MODEM_H
#define _MODEM_H


/* Includes */
#include "mTypes.h"
#include "modem_common.h"


/* Define & Typedef */

//########## Define & Typedef Applicazione ##########

#define MODEM_PWR_PORT		POWERKEY_MODEM_GPIO_Port
#define MODEM_PWR_PIN		POWERKEY_MODEM_Pin
#define MODEM_RST_PORT		RESET_MODEM_GPIO_Port
#define MODEM_RST_PIN		RESET_MODEM_Pin

#define MODEM_GNSS_TO_NUM	16
#if (MODEM_GNSS_TO_NUM>16)
#error "Parametro 0 fuori range (registro)."
#endif

#define MODEM_GNSS_PRO_TO_CONV  12      //=60/5: fattore conversione da [min] a tick da 5s (=periodo invio comando localizzazione)
#define MODEM_GNSS_PRO_TO_MIN   1
#define MODEM_GNSS_PRO_TO_MAX   21      //MODEM_GNSS_PRO_TO_MAX*MODEM_GNSS_PRO_TO_CONV<256: timeout GNSS PRO rappresentato su 1B
#if ((MODEM_GNSS_PRO_TO_MIN==0) || (MODEM_GNSS_PRO_TO_MAX*MODEM_GNSS_PRO_TO_CONV>255))
#error "Parametro 1 fuori range (registro)."
#endif


typedef enum {
  TRG_MODEM_ON 		= 0,
  TRG_GNSS_ON		= 1,			
  TRG_SYNC_RETE		= 2,
  TRG_CONNETTI_MQTT     = 3,
  TRG_INVIA_MQTT	= 4,
  TRG_RICEVI_MQTT	= 5,
  TRG_FW_UPDATE		= 6,
  TRG_CHIUDI_RETE	= 7,
  TRG_MODEM_OFF		= 8,
  TRG_GP_SETTINGS       = 9,
  TRG_MODEM_ON_RESET    = 10,
} ModemTrigger_t;

typedef enum {
  MA_BUSY 		= 0,	//Automa modem impegnato (operazione in corso) 
  MA_OK 		= 1,	//Operazione completata: nessun errore
  MA_ERROR 		= 2,	//Operazione completata: errore gestito
  MA_UNP_ERROR 		= 3,	//Errore imprevisto automa modem
  MA_INIT_ERROR		= 4,	//Errore inizializzazione libreria modem
  MA_TRIG_OK		= 5,	//Trigger operazione OK
  MA_NETWORK_ERROR	= 6,	//Errore connessione rete
  MA_POWER_ON_FAIL	= 7,	//Errore accensione modem
} ModemAppli_t;

typedef enum {
  RELEASE_T = 0x00,
  DEBUG_T,
  TEST_T,
  TOPIC_NUM
} Topic_t;


/* Funzioni globali */
extern ErrorCode_t modem_Init(Mod_t modello, uint8_t versione_HW, uint8_t sottoversione_HW);
extern void (*modem_Automa)(const uint32_t PERIODO);
extern void (*modem_ProcessaByte)(uint8_t data);

extern ModemAppli_t modem_GET_InfoFW(uint8_t* resp, uint16_t* resp_size);
extern ModemAppli_t modem_GET_Type(uint8_t* resp);
extern ModemAppli_t modem_GET_ICCID(uint8_t* resp, uint16_t* resp_size);
extern ModemAppli_t modem_GET_RSSI(uint8_t* resp);
extern ModemAppli_t modem_GET_TempoRegistrazioneRete(uint8_t* resp, uint16_t* reg_status, uint8_t* last_error);
extern ModemAppli_t modem_GET_DataOra(uint8_t* resp, uint16_t* resp_size);
extern ModemAppli_t modem_GET_InfoCella(uint8_t idx_cella, uint8_t* tecnologia, uint16_t* MCC, uint16_t* MNC, uint32_t* cell_ID, uint16_t* LAC_TAC, int16_t* rx_lev);
extern ModemAppli_t modem_GET_InfoGNSS(float* latitudine, float* longitudine);
extern ModemAppli_t modem_GET_InfoGNSS_PRO(float* latitudine, float* longitudine);
extern ModemAppli_t modem_GET_TempoGNSS(uint16_t* resp, uint16_t* status_reg);
extern ModemAppli_t modem_GET_MsgMQTT(uint8_t* resp, uint16_t* resp_size);

extern void led_segnalazione_Modem(void);
extern ModemAppli_t modem_Trigger(ModemTrigger_t trigger, uint8_t* data, uint16_t data_size);
extern ModemAppli_t modem_GET_Status(ModemTrigger_t operazione);
extern void set_Topic_MQTT(Topic_t topic);


#endif	/* End _MODEM_H */
