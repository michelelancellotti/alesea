#include "Gestione_batteria.h"
#include "EEPROM.h"
#include "Globals.h"
#include <string.h>
#include <stdint.h>

uint32_t Consumo_uA_h_tot;

float ConsumoFase[NUMERO_FASI];	
uint32_t DurataFase[NUMERO_FASI];		//espresso in s

uint8_t FlagConsumoGPSOn;
uint8_t FlagConsumoGSMOn;
uint8_t FlagConsumoNFCOn;
uint8_t FlagConsumoMEMSOn;



/* Inizializza i consumi per le diverse fasi */
void InitConsumiFasi(void)
{
  ConsumoFase[FASE_STOP_MODE] 			= 	CONSUMO_STOP_MODE;
  ConsumoFase[FASE_MEMS] 						= 	CONSUMO_MEMS;
  ConsumoFase[FASE_NFC] 						= 	CONSUMO_NFC;
  ConsumoFase[FASE_GPS] 						= 	CONSUMO_GPS;
  ConsumoFase[FASE_GSM] 						= 	CONSUMO_GSM;
  //ConsumoFase[FASE_UC_IDLE] 				= 	CONSUMO_UC_IDLE;
}



/* Calcola il consumo in uAh */
uint32_t GetConsumo()		//dopo modifica algoritmo non utilizzo pi� il periodo passato come argomento
{																		
  float SommaPesata_mA_s;
  float Appoggio_mA_ms;
  float Consumo_mA_h;
  uint32_t Consumo_uA_h;
  
  SommaPesata_mA_s = 0;
  Appoggio_mA_ms = 0;
  
  
  for(char i = 0; i < NUMERO_FASI; i++)
  {
    Appoggio_mA_ms = (float)(ConsumoFase[i]*(float)DurataFase[i]);			//mA*ms
    SommaPesata_mA_s += Appoggio_mA_ms/1000;														//mA*s
    DurataFase[i] = 0;
  }
  Consumo_mA_h = (float)(SommaPesata_mA_s/(float)(SECONDI_ORA));				//mA*h		
  Consumo_uA_h = (uint32_t)(Consumo_mA_h*1000.0);												//uA*h
  
  return Consumo_uA_h;
}


/* Calcola la carica residua della batteria */
uint32_t CalcolaCaricaResidua(uint32_t consumo)
{
  uint32_t CaricaResidua;
  float Consumo_mA_h_tot;
  
  Consumo_mA_h_tot = ((float)consumo)/1000.0;
  
  if((uint32_t)Consumo_mA_h_tot <= CAPACITA_BATTERIA)
  {
    CaricaResidua = 100 - (uint32_t)((Consumo_mA_h_tot/CAPACITA_BATTERIA)*100);					//% carica residua
  }
  else
  {
    CaricaResidua = 0;
  }
  return CaricaResidua;
}



/* Incrementa i tempi di attivit� per fase */
void IncrementoTempiConsumi(uint32_t stop_mode_time)
{
  if(FlagConsumoNFCOn)
  {
    DurataFase[FASE_NFC]++;
  }
  if(FlagConsumoMEMSOn)
  {
    DurataFase[FASE_MEMS]++;
  }
  if(FlagConsumoGPSOn)
  {
    DurataFase[FASE_GPS]++;
  }
  if(FlagConsumoGSMOn)
  {
    DurataFase[FASE_GSM]++;
  }
  //DurataFase[FASE_UC_IDLE]++;
  
  DurataFase[FASE_STOP_MODE] = stop_mode_time*1000;
}


