/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "crc.h"
#include "i2c.h"
#include "lptim.h"
#include "rtc.h"
#include "usart.h"
#include "wwdg.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "LSM6DSL.h"
#include "LSM6DSL_driver.h"
#include "Gestione_dato.h"
#include "modem_SM.h"   
#include "EEPROM.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nfc.h"
#include "crc.h"
#include "Gestione_batteria.h"
#include "Alesea_ciclo_vita.h"
#include "Globals.h"
#include "funzioni.h"
#include "LSM6DSL_SM.h"
#include "Alesea_init.h"
    
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/******************************************************************************
		V A R I A B L E S
******************************************************************************/

//Seriale verso modem

#define handle_UART_modem                       huart1
#define UART_MODEM                              USART1


DbgSeriale_t dbg_Seriale_Modem;
SerialBuffer Seriale1;              		//Struttura ricezione seriale 1 per comunicazione verso modem

void clean_context(void);

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
 
  clean_context();
	
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  //MX_LPTIM1_Init();
  MX_RTC_Init();
  MX_USART1_UART_Init();
  #ifndef DEBUG_ON
    MX_WWDG_Init();
  #endif
  MX_CRC_Init();
  /* USER CODE BEGIN 2 */
	
	
  HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_SET);	//accendo il tag
  InitLSM6DSL();
  SetInterruptAngolo();
#ifdef FREE_FALL_ON
  SetInterruptFreeFall(FALL_DUR_STOP_MODE); 
#endif
  
  InitVersioni();
  InitCollaudo();
  InitDati();
  InitConsumiFasi();
  
  modem_Init((Mod_t)Dati.TipoModem, VersioneHW, SottoversioneHW);       //Init libreria modem
  Init_SCI1();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */  

  ///////////////////////////////////////////
  ImpostaRisveglio(Dati.SchedulingInterval);
  ///////////////////////////////////////////
  
  StatoGPS_GSM = GET_TEMPERATURA;
  
  
  //segnalazione accensione
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
  HAL_Delay(500);
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
  //
  
  FlagWakeupDaRTC = 1; //forzo una trasmissione dopo un reset         
  FlagForceGpsOn = 1; 
	
  while (1)
  {		
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		
    #ifndef DEBUG_ON
        WwdgCounter = (WWDG->CR & 0x7F);
        if((WwdgCounter < 127) && (WwdgCounter > 70))
        {
          HAL_WWDG_Refresh(&hwwdg);
        }
    #endif
        
    
    BaseDeiTempi();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV16;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_LPTIM1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.LptimClockSelection = RCC_LPTIM1CLKSOURCE_PCLK;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void clean_context(void)
{
  /* Disable DMA IT */
  DMA1_Channel2->CCR &= ~(DMA_IT_TC | DMA_IT_HT | DMA_IT_TE);
  DMA1_Channel3->CCR &= ~(DMA_IT_TC | DMA_IT_HT | DMA_IT_TE);
  /* Disable the channel */
  DMA1_Channel2->CCR &=  ~DMA_CCR_EN;
  DMA1_Channel3->CCR &=  ~DMA_CCR_EN;
  
  if (HAL_IS_BIT_SET(USART1->CR3, USART_CR3_DMAR))
  {
    CLEAR_BIT(USART1->CR3, USART_CR3_DMAR);
  }
}


//-----------------------------------------------------------------------------
// Function Name: Init_SCI1
// Description  : funzione di inizializzazione variabili SCI1 (UART verso modem)
//-----------------------------------------------------------------------------
void Init_SCI1(void)
{
  Seriale1.tx_cnt = Seriale1.tx_end = Seriale1.tx_st = 0;
  Seriale1.rx_cnt = Seriale1.rx_end = Seriale1.rx_st = 0;
  Seriale1.tx_avail = 1;
  Seriale1.rx_avail = 0;

  //### IRQ ###
  //Armo interrupt RX
  if(HAL_UART_Receive_IT(&handle_UART_modem, Seriale1.rx_buffer, 1) != HAL_OK)
    dbg_Seriale_Modem.hal_err_count_RX++;
}

//-----------------------------------------------------------------------------
// Function Name: Write_SCI1
// Description  : funzione di scrittura su SCI1 (UART verso modem)
//-----------------------------------------------------------------------------
void Write_SCI1(uint8_t *str, uint16_t cnt)//funzione di scrittura su buffer circolare per periferica DEVICE e inizio tx se libero
{
  if((Seriale1.tx_avail==0) || (cnt>sizeof(Seriale1.tx_buffer)))
    return;
  
  memcpy(Seriale1.tx_buffer, str, cnt);
  Seriale1.tx_cnt = cnt;
  
  Seriale1.tx_avail = 0;
  if(HAL_UART_Transmit_IT(&handle_UART_modem, Seriale1.tx_buffer, Seriale1.tx_cnt) != HAL_OK)
    dbg_Seriale_Modem.hal_err_count_TX++;
}

//-----------------------------------------------------------------------------
// Function Name: HAL_UART_TxCpltCallback
// Description  : funzione di gestione interrupt SCIx in trasmissione
//-----------------------------------------------------------------------------
void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart)
{
  if(huart->Instance == UART_MODEM)
  {
    /* Seriale modem */
    
    Seriale1.tx_avail = 1;
  }
}

//-----------------------------------------------------------------------------
// Function Name: HAL_UART_RxCpltCallback
// Description  : funzione di gestione interrupt SCIx in ricezione
//-----------------------------------------------------------------------------
void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
  uint8_t temp_RX_Modem;

  if(huart->Instance == UART_MODEM)
  {
    /* Seriale modem */
    
    temp_RX_Modem = Seriale1.rx_buffer[0];
    
    modem_ProcessaByte(temp_RX_Modem);
    
    Seriale1.rx_cnt++;
#ifdef MODEM_LIB_DEBUG
    dbg_modem[dbg_modem_idx++] = temp_RX_Modem;
    if (dbg_modem_idx >= sizeof(dbg_modem))
      dbg_modem_idx = 0;
#endif
    
    if(HAL_UART_Receive_IT(&handle_UART_modem, Seriale1.rx_buffer, 1) != HAL_OK)
      dbg_Seriale_Modem.hal_err_count_RX++;
  }
}

//-----------------------------------------------------------------------------
// Function Name: HAL_UART_ErrorCallback
// Description  : funzione generica di gestione errori UART
//-----------------------------------------------------------------------------
void HAL_UART_ErrorCallback(UART_HandleTypeDef* huart)
{
  if(huart->Instance == UART_MODEM)
  {
    /* Seriale modem */
    
    dbg_Seriale_Modem.err_code = huart->ErrorCode;
    dbg_Seriale_Modem.err_count++;		
    
    if ((HAL_IS_BIT_SET(huart->Instance->CR3, USART_CR3_DMAR)) ||
        ((dbg_Seriale_Modem.err_code & HAL_UART_ERROR_ORE) != 0U))
    {
      dbg_Seriale_Modem.ORE_error++;            
      if(HAL_UART_Receive_IT(&handle_UART_modem, Seriale1.rx_buffer, 1) != HAL_OK)
        dbg_Seriale_Modem.hal_err_count_ERR++;
    }			
  }
}

void HAL_WWDG_EarlyWakeupCallback(WWDG_HandleTypeDef *hwwdg)
{
  //HAL_WWDG_Refresh(hwwdg);
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
