#include "Alesea_ciclo_vita.h"
#include "EEPROM.h"
#include "nfc_SM.h"
#include "modem_SM.h"
#include "modem_debug.h"
#include "bg_96.h"
#include "stm32l0xx_hal.h"
#include "Gestione_dato.h"
#include "LSM6DSL_SM.h"
#include "Gestione_batteria.h"
#include "funzioni.h"
#include "wwdg.h"
#include "Globals.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>
uint32_t Count1ms, Count1msOld, CountDiff;
uint8_t Count10ms, Count100ms, Count1000ms;
uint8_t flag1ms, flag10ms, flag100ms, flag1000ms, flag10000ms;

uint8_t FlagPresenzaNFC;
uint8_t FlagWakeupPrecollaudo;
uint8_t InitShock;
uint8_t InitMovimento;

uint8_t FreeFallInterrupt;
uint8_t WakeUpInterrupt;

void HAL_SYSTICK_Callback(void)
{
  ml_SystemTick++;			//Usato per soft-timer libreria modem
  
  Count1ms++;
  flag1ms = 1;
  if(Count1ms > 9)
  {
    Count10ms++;
    Count1ms = 0;
    flag10ms = 1;	
  }		
  if(Count10ms > 9)
  {
    Count100ms++;
    Count10ms = 0;
    flag100ms = 1;
  }	
  if(Count100ms > 9)
  {
    Count100ms = 0;
    flag1000ms = 1;
    Count1000ms++;
  }
  if(Count1000ms > 9)
  {
    flag10000ms = 1;
    Count1000ms = 0;
  }
}


///////////////////////////////////////////////////////////////////////////
//////////////////////////// BASE DEI TEMPI ///////////////////////////////
///////////////////////////////////////////////////////////////////////////
uint8_t CountErrorTemp;
void BaseDeiTempi(void)
{
  HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_SET);	//Alimento TAG
  
  ///////1ms///////
  if(flag1ms)
  {
    flag1ms = 0;
    IncrementoTempiConsumi(Dati.SchedulingInterval);		//Funzione che incrementa il cronometro delle diverse fasi
  }
  ///////10ms///////
  if(flag10ms)
  {
    flag10ms = 0;
    modem_Automa(10);
    
    if(FlagWakeupPerTemp)
    {
      if(GetTemperature())
      {
        FlagWakeupPerTemp = 0;
        CountErrorTemp = 0;
      }
      else
      {
        CountErrorTemp++;
        if(CountErrorTemp >= 10)
        {
          FlagWakeupPerTemp = 0;
          CountErrorTemp = 0;
        }
      }
      if( (Dati.MovementSession == 0) && (Dati.ShockSession == 0) && (Dati.SpinSession == 0) )  //maschero il risveglio per temperatura se ho altri eventi in corso per non avere risvegli anticipati rispetto a quello impostato per evento
        ImpostaRisveglio((uint32_t)TEMPERATURE_SCH_INTERVAL);
    }
    /*if(PrimoAvvioMEMS < 9 ) //se � il primo avvio pulisco il flag che parte alto in modo da non attendere scadere timeout
    {
      FlagWakeupDaMEMS = 0;		
      PrimoAvvioMEMS++;
    }
    else*/ if(FlagWakeupDaMEMS)		
    {
      MacchinaStatiMEMS();
      FlagConsumoMEMSOn = 1;
      Supervisor = 0;	//tengo a zero il supervisore per non farlo intervenire durante lunghi srotolamenti
    }
    else
    {
      FlagConsumoMEMSOn = 0;
    }
    
    #ifdef CALIBRAZIONE_GYRO_ON
      if(flagCalibrazioneMems)
      {
        MacchinaStatiCalibrazioneMEMS();
      }
    #else
      flagCalibrazioneMems = 0;  
    #endif
    
  }
  ///////100ms///////
  if(flag100ms)
  {
    flag100ms = 0;
    
    if(timer_flag_lsm6dsl_ignore_int > 0) 
    {
      timer_flag_lsm6dsl_ignore_int--;
      if(timer_flag_lsm6dsl_ignore_int == 0)    flag_lsm6dsl_ignore_int = 0;
    }
    
    FlagPresenzaNFC = !HAL_GPIO_ReadPin(FD_GPIO_Port, FD_Pin);	//Presenza TAG
    if(FlagPresenzaNFC)
    {
      FlagWakeupDaNFC = 1;
    }

    if(FlagWakeUpDaMovimento && InitMovimento)
    {
      MacchinaStatiMovimento();
      FlagConsumoMEMSOn = 1;
    }
    else
    {
      FlagConsumoMEMSOn = 0;
      FlagWakeUpDaMovimento = 0;
      InitMovimento = 1;
    }
    
    if(FlagWakeUpDaFreeFall)
    {
      MacchinaStatiFreeFall();
      FlagWakeUpDaFreeFall = 0;
    }
    
    //Sveglia da MEMS per shock -> Chiamata alla macchina a stati Shock
    if( FlagWakeUpDaShock && InitShock )		
    {
      MacchinaStatiShock();
      FlagConsumoMEMSOn = 1;
    }
    else
    {
      FlagConsumoMEMSOn = 0;
      InitShock = 1;
      FlagWakeUpDaShock = 0;
    }
    
    //Sveglia da NFC -> Chiamata alla macchina a stati NFC
    if( FlagWakeupDaNFC == 1 )	
    {	
      MacchinaStatiNFC();
      FlagConsumoNFCOn = 1;
    }
    else
    {
      FlagConsumoNFCOn = 0;
      TimerPresenzaNFC = 0;
    } 
    GestioneFlagComandi();
    
#ifdef DEBUG_ON
    led_segnalazione_Modem();
    /*
    if(DebugSession) 
    {
      HAL_GPIO_TogglePin(LED3_GPIO_Port, LED3_Pin);
      DebugSession = 0;
    }
    */
#else
    if(Dati.StatoAlesea == ALESEA_COLLAUDO)		led_segnalazione_Modem();	
#endif
    
  }
  ///////1s///////
  if(flag1000ms)
  {
    flag1000ms = 0;
    /*
    if(FreeFallInterrupt)       //DEBUG
    {
      FreeFallInterrupt = 0;
      HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
      HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET);
      HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET);
    }
    else
    {
      HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
      HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
      HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET);
    }
    */
    if(SpinTimeCounterEnable)   SpinTimeCounter++;
    MonitorActivity();
    
    if(Supervisor < ( TIMEOUT_SUPERVISOR + (Dati.GpsTimeout * 60) ) ) Supervisor++;
    else
    {
      EEPROM_WriteWord( EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter );
      EEPROM_WriteWord( EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter );
      GestioneDiagnostica();	//funzione di diagnostica
      HAL_NVIC_SystemReset();
    }
    
    //Verifico la coerenza della variabile StatoAlesea: se non assume valore valido la porto in ACTIVE
    if( (Dati.StatoAlesea != ALESEA_PRE_COLLAUDO)	&& (Dati.StatoAlesea != ALESEA_COLLAUDO) && (Dati.StatoAlesea != ALESEA_READY) && (Dati.StatoAlesea != ALESEA_ACTIVE) )
    {
      Dati.StatoAlesea = ALESEA_ACTIVE;
      if(Diagnostica.ContaEventiStatoRam < 254) Diagnostica.ContaEventiStatoRam++;
      EEPROM_WriteByte(EEPROM_CONTA_EVENTI_RAM, Diagnostica.ContaEventiStatoRam);
      //TODO: valutare se scrivere stato in EEPROM
    }
    
    
    //spegnimento su soglia di scarica batteria 
    if( (Dati.CaricaResidua > 0) && (Dati.CaricaResidua < CARICA_RESIDUA_SWITCH_OFF) )
    {
      Spegnimento(); 
    }
    //Sveglia da NFC in fase precollaudo -> start collaudo
    if(FlagWakeupPrecollaudo)
    {
      HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_SET);	//Alimento TAG
      if(FlagWakeupPrecollaudo > 1)	//solo dal secondo passaggio...per ritardare di 100ms
      {
        FlagWakeupPrecollaudo = 0;
        Dati.StatoAlesea = ALESEA_COLLAUDO;
        CRC_calcolato = CalcolaCRC();
        CRC_letto = EEPROM_ReadWord(EEPROM_CRC);
        if( CRC_calcolato != CRC_letto )	
        {
          Diagnostica.ContaEventiCRC++;
          EEPROM_WriteByte(EEPROM_CONTA_EVENTI_CRC, Diagnostica.ContaEventiCRC);
        }
        EEPROM_WriteByte( EEPROM_STATO_ALESEA1, Dati.StatoAlesea );
        EEPROM_WriteByte( EEPROM_STATO_ALESEA2, Dati.StatoAlesea );
        EEPROM_WriteByte( EEPROM_STATO_ALESEA3, Dati.StatoAlesea );
        CRC_calcolato = CalcolaCRC();
        EEPROM_WriteWord(EEPROM_CRC, CRC_calcolato);
        NdefCompile(Dati.StatoAlesea);
        FlagWakeupDaRTC = 1; //avvio tx
      }
      else if(FlagWakeupPrecollaudo == 1)	FlagWakeupPrecollaudo++;
    }
    
#ifdef ENB_MODEM_DEBUG
    modem_debug_trigger();
#else
    //Sveglia da RTC -> macchina a stati Modem: eseguo solo se il risveglio arriva da RTCe se non ha iniziato a contare giri prima di attivare modem (solo in stato ACTIVE e COLLAUDO)
    if( 
         (( FlagWakeupDaRTC && (!FlagWakeupDaMEMS || FlagGetStatusOn)) ||  FlagModemOnInSpinSession || FlagModemOnInShockSession || FlagModemOnInMovementSession) && ( (Dati.StatoAlesea == ALESEA_COLLAUDO) || (Dati.StatoAlesea == ALESEA_ACTIVE) )        
      )	
    {
      MacchinaStatiGPS_GSM();
    }
#endif
    //Gestione dello spegnimento su condizione legata allo stato del dispositivo
    GestioneTimeoutAttivazione();
    
    //Se il modem � acceso incremento la variabile dedicata che verr� poi stampata nel JSON
    if(FlagGetStatusOn)	Dati.TempoModemOn++;
    else Dati.TempoModemOn = 0;	
    
    //Gestione della condizione su cui entrare in stop mode
    GestioneStopMode();	//ultimo comando da eseguire dopo verifica flag relativi a comandi da remoto
  }	
  /////// 10 s ///////
  if(flag10000ms)
  {
    //Salvataggio periodico in EEPROM del numero giri
    if(FlagWakeupDaMEMS)	
    {
      EEPROM_WriteWord( EEPROM_NUMERO_GIRI_TOT_CW, Dati.NumeroGiriCw );
      EEPROM_WriteWord( EEPROM_NUMERO_GIRI_TOT_ACW, Dati.NumeroGiriACw );
    }
    flag10000ms = 0;
  }
}





////////////////////////////////////////////////////////////////////////////
////////////////GESTIONE DEI FLAG COMANDI DA APP E PORTALE//////////////////
////////////////////////////////////////////////////////////////////////////
void GestioneFlagComandi(void)
{
  if(FlagAttivazione)
  {
    if(Dati.StatoAlesea == ALESEA_READY)
    {
      AttivaAlesea();
      NdefCompile(Dati.StatoAlesea);
      FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
      FlagNoRxMQTT = 1; 		//bypass rx MQTT
    }
    FlagAttivazione = 0;
  }
  if(FlagBatteryReset)		
  {
    ResettaConsumo();
    //FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    //FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagBatteryReset = 0;
  }
  if(FlagSpinReset)
  {
    ResettaContagiri();
    NdefCompile(Dati.StatoAlesea);
    FlagSpinReset = 0;
  }
  if(FlagDiagnosticaReset)
  {
    FlagDiagnosticaReset = 0;
    ResettaDiagnostica();
  }
  if(FlagShockReset)
  {
    ResettaContaShock();
    NdefCompile(Dati.StatoAlesea);
    FlagShockReset = 0;
  }
  if(FlagFreeFallReset)
  {
    ResettaContaFreeFall();
    //NdefCompile(Dati.StatoAlesea);
    FlagFreeFallReset = 0;
  }
  if(FlagFactoryReset == 1)
  {
    FactoryResetPendente = 1;   //sposto la chiamata alle funzioni dedicate nella macchina a stati del modem prima che vada in stop mode
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagFactoryReset = 0;
  }
  if(FlagDisattivazione == 1)
  {
    DisattivazionePendente = 1;//sposto la chiamata alle funzioni dedicate nella macchina a stati del modem prima che vada in stop mode
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagDisattivazione = 0;
  }
  if(FlagSincronizzazione == 1)
  {
    NdefCompile(Dati.StatoAlesea);
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagSincronizzazione = 0;
  }
  if(FlagInstantMessage == 1)
  {
    NdefCompile(Dati.StatoAlesea);
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    //FlagNoRxMQTT = 1; 	//bypass rx MQTT
    FlagInstantMessage = 0;
  }
  if(FlagUpgradeFw == 1)
  {
    NdefCompile(Dati.StatoAlesea);
    AggiornamentoFwPendente = 1;
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagUpgradeFw = 0;
  }
}



////////////////////////////////////////////////////////////////
//                    Risveglio da RTC		              //																		//
////////////////////////////////////////////////////////////////
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
  //Alzo il flag solo se il dispositivo � attivo o in collaudo e se il flag non � gi� attivo
  if( !FlagWakeupDaRTC && ((Dati.StatoAlesea == ALESEA_ACTIVE) || (Dati.StatoAlesea == ALESEA_COLLAUDO)) )	
  {
    if( /*FlagSpinSessionStart*/ Dati.SpinSession == 1)   
    {
      if(!FlagWakeupDaMEMS)     //se mi sono svegliato per schedulazione da rotazione e non sto pi� girando...
      {
        FlagWakeupDaRTC = 1; 
        if(Dati.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON)        FlagWakeupPerTemp = 1;
        FlagModemOnInSpinSession = 0;        //se la sessione di spin � iniziata ma non ho attivit� sul MEMS allora avvio modem SM
        //FlagSpinSessionStart = 0;
        Dati.SpinSession = 2;
        
        //se � terminata la rotazione termino anche il movimento generato in concomitanza
        if(Dati.MovementSession == 1)
          Dati.MovementSession = 2;
        
        
        if(SpinTimeCounter > (/*FILTRO_QUADRANTE_FERMO*/Dati.FiltroMovimento*SpinStartStopCounterAppoggio) ) SpinTimeCounter = SpinTimeCounter - (/*FILTRO_QUADRANTE_FERMO*/Dati.FiltroMovimento*SpinStartStopCounterAppoggio);  //passo da base 10ms a 1s
        Dati.SpinFreq = 60*NumeroGiriFloatFase/SpinTimeCounter; 
        Dati.SpinStartStopCounter = SpinStartStopCounterAppoggio;       //numero sottosessioni di spin tra la prima (conclusa) e il risveglio da RTC schedulato a 3600 secondi dopo la prima rotazione
        SpinStartStopCounterAppoggio = 0;
        NumeroGiriFloatFase = 0;
        SpinTimeCounter = 0;    
      }
      else
      {
        
        //////////////////////////////////////////
        ImpostaRisveglio(SPIN_SCH_INTERVAL_SECOND);
        //////////////////////////////////////////
      
      }
    }
    else if(/*FlagShockSessionStart*/ (Dati.ShockSession == 1) && (FlagWakeUpDaShock == 0) )   
    {
      FlagWakeupDaRTC = 1;           
      //FlagShockSessionStart = 0;
      Dati.ShockSession = 2;
    }
    else if( (Dati.MovementSession == 1) && (FlagWakeUpDaMovimento == 0) )   //aggiunta condizione su FlagWakeUpDaMovimento per evitare ingresso su scadere RTC durante comunicazione causata da movimento 
    {
      FlagWakeupDaRTC = 1; 
      Dati.MovementSession = 2;
      //FlagMovementSessionStart = 0;
      //SogliaAccelerometro = Dati.SogliaMovimento;//SOGLIA_MOV; //se trasmetto per RTC e non per movimentazioni riporto la soglia al default per rilevare movimento
      //SogliaAccelerometroSet(SogliaAccelerometro);
    }
    else    //risveglio per schedulazione senza eventi esterni in corso
    {
      
      //FlagWakeupDaRTC = 1;  
      //01/03/2022 algoritmo temperatura
      //lettura temperatura
      if(Dati.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON)     
      {
        if(SchIntervalCounter > 1)      //se il rapporto fra la schedulazione e 1 ora per lettura temp � > 1
        {
          SchIntervalCounter--;
          FlagWakeupPerTemp = 1;
        }
        else
        {
          FlagWakeupDaRTC = 1;  
        }
      }
      else
      {
        FlagWakeupDaRTC = 1;  
      }
      //SogliaAccelerometro = Dati.SogliaMovimento;//SOGLIA_MOV; //se trasmetto per RTC e non per movimentazioni riporto la soglia al default per rilevare movimento
      //SogliaAccelerometroSet(SogliaAccelerometro);
    }
  }
}


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(GPIO_Pin);
  if(flag_lsm6dsl_ignore_int != 0)        
  {
    flag_lsm6dsl_ignore_int = 0 ;
    return;
  }
  
  
  if(GPIO_Pin == GPIO_PIN_3)	//risveglio da MEMS
  {
#ifdef FREE_FALL_ON
    FreeFallInterrupt = FreeFallSourceRead();
#endif
    WakeUpInterrupt = WakeUpSourceRead();
    
    if(WakeUpInterrupt)
    {
      if(SogliaAccelerometro == Dati.SogliaShock)    FlagWakeUpDaShock = 1;
      else if(!FlagGetStatusOn)   //eseguo solo se la macchina a stati del modem non � gi� attiva. Altrimenti rischio di avere un pacchetto con ms = 1 e senza GPS
      {
        if(Dati.MovementSession == 0)
          FlagWakeUpDaMovimento = 1;
        
        if(Dati.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON) //solo se abilitata la rotaz orizz
          FlagWakeupDaMEMS = 1;
      }
      WakeUpInterrupt = 0;
    }
    if(FreeFallInterrupt)
    {
      FlagWakeUpDaFreeFall = 1;
      FreeFallInterrupt = 0;
    }
  }
  if(GPIO_Pin == GPIO_PIN_4)	//risveglio da MEMS
  {
    FlagWakeupDaMEMS = 1;
    FlagRefreshRotation = 1;
  }
  if(GPIO_Pin == GPIO_PIN_5)	//risveglio da NFC
  {
    if(Dati.StatoAlesea == ALESEA_PRE_COLLAUDO)
    {
      FlagWakeupPrecollaudo = 1;
    }
    else
    {
      FlagWakeupDaNFC = 1;
    }
  }
  
  /* NOTE: This function Should not be modified, when the callback is needed,
  the HAL_GPIO_EXTI_Callback could be implemented in the user file
  */ 
}




