#include "EEPROM.h"
#include "nfc_SM.h"
#include "modem_SM.h"
#include "bg_96.h"
#include "stm32l0xx_hal.h"
#include "Gestione_dato.h"
#include "LSM6DSL_SM.h"
#include "crc.h"
#include "Gestione_batteria.h"
#include "funzioni.h"
#include "wwdg.h"
#include "Globals.h"
#include "i2c.h"
#include "gpio.h"
#include "lptim.h"
#include "usart.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>

uint32_t CRC_calcolato, CRC_letto;
uint8_t ContatoreAttivazione;
uint32_t Supervisor;


void AttivaAlesea(void)
{
  //Ad ogni attivazione il periodo viene portato a PERIODO_PRE_ACTIVE per un numero fisso di tx per poi passare a PERIODO_ACTIVE
  Dati.StatoAlesea = ALESEA_ACTIVE;
  
  CRC_calcolato = CalcolaCRC();
  CRC_letto = EEPROM_ReadWord(EEPROM_CRC);
  if( CRC_calcolato != CRC_letto )	
  {
    Diagnostica.ContaEventiCRC++;
    EEPROM_WriteByte(EEPROM_CONTA_EVENTI_CRC, Diagnostica.ContaEventiCRC);
  }
  EEPROM_WriteByte( EEPROM_STATO_ALESEA1, Dati.StatoAlesea );
  EEPROM_WriteByte( EEPROM_STATO_ALESEA2, Dati.StatoAlesea );
  EEPROM_WriteByte( EEPROM_STATO_ALESEA3, Dati.StatoAlesea );
  
  Dati.SchedulingInterval = PERIODO_PRE_ACTIVE;
  EEPROM_WriteWord(EEPROM_TX_TIME1, Dati.SchedulingInterval);
  EEPROM_WriteWord(EEPROM_TX_TIME2, Dati.SchedulingInterval);
  EEPROM_WriteWord(EEPROM_TX_TIME3, Dati.SchedulingInterval);
  ContaTxPreActive = 0;
  EEPROM_WriteWord(EEPROM_PREACTIVE_COUNT, ContaTxPreActive);
  Diagnostica.ContaReset = 0;
  EEPROM_WriteByte( CONTA_RESET, (uint32_t)Diagnostica.ContaReset );
  CRC_calcolato = CalcolaCRC();
  EEPROM_WriteWord(EEPROM_CRC, CRC_calcolato);
}



void ResetParametriFabbrica(void)
{
  Dati.IndiceTrasmissione = 0;	//azzero n pacchetti trasmesso
  EEPROM_WriteWord(EEPROM_INDICE_TRASMISSIONE, Dati.IndiceTrasmissione);
  
  IndiceDatoSalvato = 0;	//azzero indice di struttura dati salvati
  IndiceDatoLetto = 0;		//azzero indice di struttura dati letti
  Dati.SchedulingInterval = PERIODO_PRE_ACTIVE;		//ripristino periodo tx
  EEPROM_WriteWord( EEPROM_TX_TIME1, (int32_t)Dati.SchedulingInterval );
  EEPROM_WriteWord( EEPROM_TX_TIME2, (int32_t)Dati.SchedulingInterval );
  EEPROM_WriteWord( EEPROM_TX_TIME3, (int32_t)Dati.SchedulingInterval );
  EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE1, PERIODO_ACTIVE);
  EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE2, PERIODO_ACTIVE);
  EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE3, PERIODO_ACTIVE);
  Diagnostica.ContaReset = 0;
  EEPROM_WriteByte( CONTA_RESET, (uint32_t)Diagnostica.ContaReset );
  ContaTxPreActive = 0;
  EEPROM_WriteWord(EEPROM_PREACTIVE_COUNT, ContaTxPreActive);
  
  //todo: valutare quali altri parametri resettare
  Dati.GpsPro = GPS_PRO_STATE_DEFAULT;
  EEPROM_WriteByte(EEPROM_GPS_PRO, Dati.GpsPro);
  Dati.GpsProTime = GPS_PRO_TIME_DEFAULT;
  EEPROM_WriteByte(EEPROM_GPS_PRO_TIME, Dati.GpsProTime);
  Dati.GpsFreqForcing = GPS_FREQ_FORCING_DEFAULT;
  EEPROM_WriteByte(EEPROM_GPS_FREQ_FORCING, Dati.GpsFreqForcing);
  //  
  
  ResettaContagiri();
}


void DisattivaAlesea(void)
{
  //Il dispositivo torna a READY e perde il periodo di schedulazione attuale
  Dati.StatoAlesea = ALESEA_READY;		//cambio stato a READY
  CRC_calcolato = CalcolaCRC();
  CRC_letto = EEPROM_ReadWord(EEPROM_CRC);
  if( CRC_calcolato != CRC_letto )	
  {
    Diagnostica.ContaEventiCRC++;
    EEPROM_WriteByte(EEPROM_CONTA_EVENTI_CRC, Diagnostica.ContaEventiCRC);
  }
  EEPROM_WriteByte( EEPROM_STATO_ALESEA1, Dati.StatoAlesea );
  EEPROM_WriteByte( EEPROM_STATO_ALESEA2, Dati.StatoAlesea );
  EEPROM_WriteByte( EEPROM_STATO_ALESEA3, Dati.StatoAlesea );
  CRC_calcolato = CalcolaCRC();
  EEPROM_WriteWord(EEPROM_CRC, CRC_calcolato);
}


void ResettaConsumo(void)
{
  Consumo_uA_h_tot = 0;
  EEPROM_WriteWord(EEPROM_CONSUMO_TOT, Consumo_uA_h_tot);	
}


void ResettaContagiri(void)
{
  NumeroGiriGyr = 0;
  Dati.NumeroGiriCw = 0;
  Dati.NumeroGiriACw = 0;
  EEPROM_WriteWord(EEPROM_NUMERO_GIRI_TOT_CW, Dati.NumeroGiriCw);	
  EEPROM_WriteWord(EEPROM_NUMERO_GIRI_TOT_ACW, Dati.NumeroGiriACw);	
}


void ResettaContaShock(void)
{
  Dati.ContaShock = 0;
  EEPROM_WriteWord(EEPROM_CONTA_SHOCK, Dati.ContaShock);	
}

void ResettaContaFreeFall(void)
{
  Dati.ContaFreeFall = 0;
  EEPROM_WriteWord(EEPROM_CONTA_FREE_FALL, Dati.ContaFreeFall);
}


void ResettaDiagnostica(void)
{
  Diagnostica.Byte1 = 0;
  Diagnostica.Byte2 = 0;
  Diagnostica.Byte3 = 0;
  Diagnostica.Byte4 = 0;
  Diagnostica.Byte5 = 0;
  Diagnostica.Byte6 = 0;
  Diagnostica.Byte7 = 0;
  Diagnostica.Byte8 = 0;
  Diagnostica.ModemActivityCounter = 0;
  Diagnostica.MicroActivityCounter = 0;
  Diagnostica.ContaStatoIn = 0;
  Diagnostica.ContaStatoOut = 0;
  Diagnostica.CounterEepromReset = 0;
  Diagnostica.CountErroreModemOn = 0;
  Diagnostica.ContaReset = 0;
  Diagnostica.CountUnpError = 0;
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_1, Diagnostica.Byte1);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_2, Diagnostica.Byte2);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_3, Diagnostica.Byte3);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_4, Diagnostica.Byte4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_5, Diagnostica.Byte5);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_6, Diagnostica.Byte6);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_7, Diagnostica.Byte7);
  EEPROM_WriteByte(EEPROM_CONTA_STATO_IN,Diagnostica.ContaStatoIn);
  EEPROM_WriteByte(EEPROM_CONTA_STATO_OUT,Diagnostica.ContaStatoOut);
  EEPROM_WriteWord(EEPROM_MODEM_ACTIVITY,Diagnostica.ModemActivityCounter);
  EEPROM_WriteWord(EEPROM_MICRO_ACTIVITY,Diagnostica.MicroActivityCounter);
  EEPROM_WriteWord(EEPROM_RESET_EEPROM,Diagnostica.CounterEepromReset);
  EEPROM_WriteByte(EEPROM_CONTA_FAIL_MODEM_ON, Diagnostica.CountErroreModemOn);
  EEPROM_WriteByte(CONTA_RESET, Diagnostica.ContaReset);
  EEPROM_WriteByte(EEPROM_CONTA_UNP, Diagnostica.CountUnpError);
}


void GestioneTimeoutAttivazione(void)
{
  if( Dati.StatoAlesea == ALESEA_READY )
  {
    if(++ContatoreAttivazione >= TIMEOUT_ATTIVAZIONE_READY)
    {
      Spegnimento();
    }
  }
  else if(Dati.StatoAlesea == ALESEA_PRE_COLLAUDO)
  {
    if(++ContatoreAttivazione >= TIMEOUT_ATTIVAZIONE_PRECOLLAUDO)
    {
      Spegnimento();
    }
  }
  else
  {
    ContatoreAttivazione = 0;
  }
}


void VerificaStatoCollaudo(void)
{
  //se sono state effettuate le trasmissioni previste per il collaudo va in stato READY
  if( (Dati.StatoAlesea == ALESEA_COLLAUDO) && (/*Dati.IndiceTrasmissione*/IndiceTrasmissioneCollaudo >= NUMERO_TRASMISSIONI_COLLAUDO) )
  {
    IndiceTrasmissioneCollaudo = 0;
    
    //EEPROM_WriteWord(EEPROM_INDICE_TRASMISSIONE, Dati.IndiceTrasmissione);
    
    CRC_calcolato = CalcolaCRC();
    CRC_letto = EEPROM_ReadWord(EEPROM_CRC);
    if( CRC_calcolato != CRC_letto )
    {
      Diagnostica.ContaEventiCRC++;
      EEPROM_WriteByte(EEPROM_CONTA_EVENTI_CRC, Diagnostica.ContaEventiCRC);
    }
    EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO1, CHIAVE_COLLAUDO);
    EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO2, CHIAVE_COLLAUDO);
    EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO3, CHIAVE_COLLAUDO);
    /*	
    Dati.SchedulingInterval = PERIODO_ACTIVE;
    EEPROM_WriteWord( EEPROM_TX_TIME, (int32_t)Dati.SchedulingInterval );
    */
    EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE1, PERIODO_ACTIVE);
    EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE2, PERIODO_ACTIVE);
    EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE3, PERIODO_ACTIVE);
    
    Dati.SchedulingInterval = PERIODO_PRE_ACTIVE;
    EEPROM_WriteWord( EEPROM_TX_TIME1, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord( EEPROM_TX_TIME2, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord( EEPROM_TX_TIME3, (int32_t)Dati.SchedulingInterval );
    
    ContaTxPreActive = 0;
    EEPROM_WriteWord(EEPROM_PREACTIVE_COUNT, ContaTxPreActive);
    
    Dati.StatoAlesea = ALESEA_READY;
    EEPROM_WriteByte( EEPROM_STATO_ALESEA1, Dati.StatoAlesea );
    EEPROM_WriteByte( EEPROM_STATO_ALESEA2, Dati.StatoAlesea );
    EEPROM_WriteByte( EEPROM_STATO_ALESEA3, Dati.StatoAlesea );
    
    EEPROM_WriteWord(EEPROM_CRC, CalcolaCRC());
    
    NdefCompile(Dati.StatoAlesea);
    Spegnimento();
  }
}



uint32_t CalcolaCRC(void)
{
  for(uint8_t CRC_index = 0; CRC_index < CRC_BUFF_DIM; CRC_index++)
  {
    // 0 to 19
    if(CRC_index < (sizeof(Dati.ICCID)-1))
    {
      CRCBuff[CRC_index] = EEPROM_ReadByte(EEPROM_ICCID + CRC_index);
      continue;
    }
    
    //Last loop
    CRCBuff[CRC_index] = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO1);
    CRCBuff[++CRC_index] = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO2);
    CRCBuff[++CRC_index] = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO3);
    CRCBuff[++CRC_index] = EEPROM_ReadByte(EEPROM_STATO_ALESEA1);
    CRCBuff[++CRC_index] = EEPROM_ReadByte(EEPROM_STATO_ALESEA2);
    CRCBuff[++CRC_index] = EEPROM_ReadByte(EEPROM_STATO_ALESEA3);
    
    //2byte extra
    CRCBuff[++CRC_index] = 0x00;
    CRCBuff[++CRC_index] = 0x00;
    break;
  }
  
  return HAL_CRC_Calculate(&hcrc, (uint32_t *)&CRCBuff[0], CRC_BUFF_DIM);
}




void Spegnimento(void)
{
  EEPROM_WriteWord( EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter );
  EEPROM_WriteWord( EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter );
#ifndef DEBUG_ON
  SWITCH_OFF   
#endif
}


uint8_t StopMode;
void GestioneStopMode(void)
{
  StatusModemMonitor = !HAL_GPIO_ReadPin(STATUS_MODEM_GPIO_Port, STATUS_MODEM_Pin);
  if(!FlagWakeupPerTemp && !FlagWakeupDaRTC && !FlagWakeupDaMEMS && !FlagWakeUpDaMovimento && !FlagWakeUpDaShock && !FlagWakeupDaNFC && !StatusModemMonitor && !flagCalibrazioneMems)
  {
    if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, TempoDiRisveglioDef/HZ_RTC, RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK)
    {
      Error_Handler();
    }
    StopMode = 1;
#ifndef DEBUG_ON
    if(Dati.StatoAlesea != ALESEA_READY)	
    {
      EnterStopMode();	
    }
#else
    Supervisor = 0;		
#endif
  }
  else StopMode = 0;
}

void EnterStopMode(void)
{
  NdefCompile(Dati.StatoAlesea);
  
  EEPROM_WriteWord( EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter );
  EEPROM_WriteWord( EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter );
  
  
  HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_RESET);	//Disalimento TAG
  HAL_GPIO_WritePin(GPS_AMPLI_GPIO_Port, GPS_AMPLI_Pin, GPIO_PIN_RESET);	//Spegnimento Ampli GPS
  
  LSM6DSL_GYRO_OFF();
  
  LED_1_OFF;
  LED_2_OFF;
  LED_3_OFF;
  
  Supervisor = 0;
  //HAL_SuspendTick();  //da attivare se necessario fare debug in stop mode. (per debug in stop mode abilitare DBG_STOP in DBG_CR. Il registro si pulisce solo disalimentando)
  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
  HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);      
  //HAL_ResumeTick();  //da attivare se necessario fare debug in stop mode.
  
  SystemClock_Config();
  MX_GPIO_Init();  
  MX_I2C1_Init();
  //MX_LPTIM1_Init();
  MX_USART1_UART_Init();
}



void GestioneDiagnostica(void)
{	
  Diagnostica.Byte1 = StatoNFC;
  Diagnostica.Byte2 = StatoMEMS;
  Diagnostica.Byte3 = StatoGPS_GSM;
  Diagnostica.Byte4 = bg96_status;
  Diagnostica.Byte5 = Dati.StatoAlesea;
  Diagnostica.Byte6 = bg96_PWR;
  
  Diagnostica.Byte7 = 0;
  Diagnostica.Byte7 = (0x01 & FlagWakeupDaRTC);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeupDaMEMS) << 1);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeupDaNFC) << 2);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeUpDaShock) << 3);
  Diagnostica.Byte7 |= ((0x01 & FlagGetStatusOn) << 4);
  Diagnostica.Byte7 |= ((0x01 & StatusModemMonitor) << 5);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeupDaRTC) << 6);
  
  //Diagnostica.Byte8 = 
  
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_1, Diagnostica.Byte1);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_2, Diagnostica.Byte2);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_3, Diagnostica.Byte3);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_4, Diagnostica.Byte4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_5, Diagnostica.Byte5);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_6, Diagnostica.Byte6);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_7, Diagnostica.Byte7);
  
}



void AggiornaModemType(void)
{
   modem_GET_Type(&Dati.TipoModem);
   EEPROM_WriteByte(EEPROM_TIPO_MODEM, Dati.TipoModem);
}

