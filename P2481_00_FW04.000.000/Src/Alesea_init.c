#include "EEPROM.h"
#include "Alesea_init.h"
#include "Globals.h"
#include "Gestione_dato.h"
#include "Gestione_batteria.h"
#include "funzioni.h"
#include "modem_SM.h"
#include "ndef.h"
#include "LSM6DSL_SM.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>

const uint8_t* BootVersionAdd;
const uint8_t* HwVersionAdd;
uint8_t StatoEEPROM;


void InitVersioni(void)
{
  VersioneFW = VERSIONE_FW;	
  SottoversioneFW = SOTTO_VERSIONE_FW;
  EEPROM_WriteByte(EEPROM_ADD_VERSION,VersioneFW);
  EEPROM_WriteByte(EEPROM_ADD_SUBVERSION,SottoversioneFW);
  
  BootVersionAdd = (uint8_t*)BOOT_VERSION_ADDRESS;
  if( (BootVersionAdd[0] == 'B') && (BootVersionAdd[1] == '.') )
    memcpy(Dati.BootVersion, BootVersionAdd, sizeof(Dati.BootVersion));
  else
    sprintf((char*)Dati.BootVersion,"%s", "OLD_BOOT");
  
  HwVersionAdd = (uint8_t*)HW_VERSION_ADDRESS;
  if( (HwVersionAdd[0] == 'H') && (HwVersionAdd[1] == '.') )
  {
    VersioneHW = HwVersionAdd[2];
    SottoversioneHW = HwVersionAdd[3];
  }
  else
  {
    VersioneHW = VERSIONE_HW_DEFAULT_BG96;		
    SottoversioneHW = SOTTO_VERSIONE_HW_DEFAULT_BG96;
  }
}


void InitCollaudo(void)
{
  uint8_t chiave1, chiave2, chiave3;
  uint32_t CRC_EEPROM_read;
  
  chiave1 = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO1);
  chiave2 = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO2);
  chiave3 = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO3);
  
  Diagnostica.ContaEventiCRC = EEPROM_ReadWord(EEPROM_CONTA_EVENTI_CRC);
  
  CRC_EEPROM_read = EEPROM_ReadWord(EEPROM_CRC); 
  if( CalcolaCRC() != CRC_EEPROM_read )	
  {
    if( CRC_EEPROM_read != 0x00000000 )	Diagnostica.ContaEventiCRC++;
    EEPROM_WriteByte(EEPROM_CONTA_EVENTI_CRC, Diagnostica.ContaEventiCRC);	
  }
  if( (chiave1 != CHIAVE_COLLAUDO) && (chiave2 != CHIAVE_COLLAUDO) && (chiave3 != CHIAVE_COLLAUDO) ) 
  {
    Dati.StatoAlesea = ALESEA_PRE_COLLAUDO;
    StatoEEPROM = EEPROM_WriteByte( EEPROM_STATO_ALESEA1, Dati.StatoAlesea );
    EEPROM_WriteByte( EEPROM_STATO_ALESEA2, Dati.StatoAlesea );
    EEPROM_WriteByte( EEPROM_STATO_ALESEA3, Dati.StatoAlesea );
    
  }
  else
  {
    EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO1, CHIAVE_COLLAUDO );
    EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO2, CHIAVE_COLLAUDO );
    EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO3, CHIAVE_COLLAUDO );
  }
  CRC_calcolato = CalcolaCRC();
  EEPROM_WriteWord(EEPROM_CRC, CRC_calcolato);
}





void InitDati(void)
{
  uint32_t tx_time_temp1, tx_time_temp2, tx_time_temp3;
  uint32_t periodo_active_temp1, periodo_active_temp2, periodo_active_temp3;
  uint8_t indice;
  uint8_t stato_alesea1, stato_alesea2, stato_alesea3;
  CRC_calcolato = CalcolaCRC();
  CRC_letto = EEPROM_ReadWord(EEPROM_CRC);
  if( CRC_calcolato != CRC_letto )	
  {
    Diagnostica.ContaEventiCRC++;
    EEPROM_WriteByte(EEPROM_CONTA_EVENTI_CRC, Diagnostica.ContaEventiCRC);
  }	
  CalibrazioneMems = ((float)EEPROM_ReadWord(EEPROM_MEMS_CALIBRATION))/1000;
  if( (CalibrazioneMems < -MEMS_CALIB_MAX_VALUE) || (CalibrazioneMems > MEMS_CALIB_MAX_VALUE) )
  {
    CalibrazioneMems = 0;
  }
  
  Dati.Orientamento = FLANGIA_NO_RESULT;
    
  Diagnostica.ModemActivityCounter = EEPROM_ReadWord(EEPROM_MODEM_ACTIVITY);
  Diagnostica.MicroActivityCounter = EEPROM_ReadWord(EEPROM_MICRO_ACTIVITY);
  Diagnostica.ContaStatoIn = EEPROM_ReadByte(EEPROM_CONTA_STATO_IN);
  Diagnostica.ContaStatoOut = EEPROM_ReadByte(EEPROM_CONTA_STATO_OUT);
  Diagnostica.CounterEepromReset = EEPROM_ReadWord(EEPROM_RESET_EEPROM);
  Diagnostica.CountErroreModemOn = EEPROM_ReadByte(EEPROM_CONTA_FAIL_MODEM_ON);
  Diagnostica.ContaReset = EEPROM_ReadWord(CONTA_RESET);
  if(Diagnostica.ContaReset < 255 ) Diagnostica.ContaReset++;
  EEPROM_WriteByte( CONTA_RESET, (uint32_t)Diagnostica.ContaReset );
  Diagnostica.ContaEventiCRC = EEPROM_ReadByte(EEPROM_CONTA_EVENTI_CRC);
  for(uint8_t i = 0; i < sizeof(Dati.ICCID)-1; i++)
  {
    Dati.ICCID[i] = EEPROM_ReadByte(EEPROM_ICCID + i);
  }
  Dati.TipoModem = (Mod_t)EEPROM_ReadByte(EEPROM_TIPO_MODEM);
  
  tx_time_temp1 = EEPROM_ReadWord(EEPROM_TX_TIME1);
  tx_time_temp2 = EEPROM_ReadWord(EEPROM_TX_TIME2);
  tx_time_temp3 = EEPROM_ReadWord(EEPROM_TX_TIME3);
  if( (tx_time_temp1 == tx_time_temp2) && (tx_time_temp1 == tx_time_temp3) && (tx_time_temp1 > 0) )
  {
    Dati.SchedulingInterval = tx_time_temp1;
  }
  else
  {
    periodo_active_temp1 = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE1);
    periodo_active_temp2 = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE2);
    periodo_active_temp3 = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE3);
    if( (periodo_active_temp1 == periodo_active_temp2) && ( periodo_active_temp1 == periodo_active_temp3) && (periodo_active_temp1 > 0) ) //solo se il dato in memoria � valido lo leggo altrimenti ripristino il dato di fabbrica
    {
      Dati.SchedulingInterval = periodo_active_temp1; 	//ripristino periodo tx
    }
    else
    {
      Dati.SchedulingInterval = PERIODO_ACTIVE;
      EEPROM_WriteWord( EEPROM_PERIODO_ACTIVE1, Dati.SchedulingInterval );
      EEPROM_WriteWord( EEPROM_PERIODO_ACTIVE2, Dati.SchedulingInterval );
      EEPROM_WriteWord( EEPROM_PERIODO_ACTIVE3, Dati.SchedulingInterval );
      
    }
    EEPROM_WriteWord( EEPROM_TX_TIME1, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord( EEPROM_TX_TIME2, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord( EEPROM_TX_TIME3, (int32_t)Dati.SchedulingInterval );
  }
  
  //rotazione orizzontale
  Dati.RotazioneOrizzontaleOn = EEPROM_ReadByte(EEPROM_ROTAZIONE_ORIZZ);
  if(Dati.RotazioneOrizzontaleOn > ROTAZIONE_ORIZZ_ON) 
  {
    Dati.RotazioneOrizzontaleOn = ROTAZIONE_ORIZZ_OFF;
    EEPROM_WriteByte(EEPROM_ROTAZIONE_ORIZZ, Dati.RotazioneOrizzontaleOn);
  }
   
  //gps assistito
  Dati.GpsAssistito = EEPROM_ReadByte(EEPROM_STATO_GPS_ASSISTITO);
  Dati.GpsTimeout = EEPROM_ReadByte(EEPROM_STATO_GPS_TIMEOUT);
  if( (Dati.GpsTimeout > GPS_TIMEOUT_MAX) || (Dati.GpsTimeout < GPS_TIMEOUT_DEFAULT) ) 	
  {
    Dati.GpsTimeout = GPS_TIMEOUT_DEFAULT;
    EEPROM_WriteByte(EEPROM_STATO_GPS_TIMEOUT, Dati.GpsTimeout);
  }
  //gps pro
  Dati.GpsPro = EEPROM_ReadByte(EEPROM_GPS_PRO);
  Dati.GpsProTime = EEPROM_ReadByte(EEPROM_GPS_PRO_TIME);
  if( (Dati.GpsProTime > GPS_PRO_TIME_MAX) || (Dati.GpsProTime < GPS_PRO_TIME_MIN) ) 	
  {
    Dati.GpsProTime = GPS_PRO_TIME_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_PRO_TIME, Dati.GpsProTime);
  }
  
  //gps freq forcing
  Dati.GpsFreqForcing = EEPROM_ReadByte(EEPROM_GPS_FREQ_FORCING);
  if( (Dati.GpsFreqForcing > GPS_FREQ_FORCING_MAX) || (Dati.GpsFreqForcing < GPS_FREQ_FORCING_MIN) ) 	
  {
    Dati.GpsFreqForcing = GPS_FREQ_FORCING_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_FREQ_FORCING, Dati.GpsFreqForcing);
  }
   
  //soglia movimentazione
  Dati.SogliaMovimento = EEPROM_ReadByte(EEPROM_SOGLIA_MOV);
  if( (Dati.SogliaMovimento > SOGLIA_MOV_MAX) || (Dati.SogliaMovimento < SOGLIA_MOV_MIN) ) 	
  {
    Dati.SogliaMovimento = SOGLIA_MOV_DEFAULT;
    EEPROM_WriteByte(EEPROM_SOGLIA_MOV, Dati.SogliaMovimento);
  }
  SogliaAccelerometro = Dati.SogliaMovimento;
  SogliaAccelerometroSet(SogliaAccelerometro);
  
  //soglia shock
  Dati.SogliaShock = EEPROM_ReadByte(EEPROM_SOGLIA_SHOCK);
  if( (Dati.SogliaShock > SOGLIA_SHOCK_MAX) || (Dati.SogliaShock < SOGLIA_SHOCK_MIN) ) 	
  {
    Dati.SogliaShock = SOGLIA_SHOCK_DEFAULT;
    EEPROM_WriteByte(EEPROM_SOGLIA_SHOCK, Dati.SogliaShock);
  }
  //filtro movimento
  Dati.FiltroMovimento = EEPROM_ReadWord(EEPROM_FILTRO_QUADRANTE_FERMO);
  if( (Dati.FiltroMovimento > FILTRO_QUADRANTE_FERMO_MAX) || (Dati.FiltroMovimento < FILTRO_QUADRANTE_FERMO_MIN) ) 	
  {
    Dati.FiltroMovimento = FILTRO_QUADRANTE_FERMO_DEFAULT;
    EEPROM_WriteWord(EEPROM_FILTRO_QUADRANTE_FERMO, Dati.FiltroMovimento);
  }
  
  //schedulazione movimento
  Dati.MoveSchInterval = EEPROM_ReadWord(EEPROM_MOVE_SCH_INTERVAL);
  if( (Dati.MoveSchInterval > MOVE_SCH_INTERVAL_MAX) || (Dati.MoveSchInterval < MOVE_SCH_INTERVAL_MIN) ) 	
  {
    Dati.MoveSchInterval = MOVE_SCH_INTERVAL_DEFAULT;
    EEPROM_WriteWord(EEPROM_MOVE_SCH_INTERVAL, Dati.MoveSchInterval);
  }
  
  
  
  Dati.ProfiloTemperaturaOn = EEPROM_ReadByte(EEPROM_PROFILO_TEMPERATURA_ON);
  if( (Dati.ProfiloTemperaturaOn != PROFILO_TEMPERATURA_OFF) && (Dati.ProfiloTemperaturaOn != PROFILO_TEMPERATURA_ON) )
  {
    Dati.ProfiloTemperaturaOn = PROFILO_TEMPERATURA_ON;
    EEPROM_WriteByte(EEPROM_PROFILO_TEMPERATURA_ON, Dati.ProfiloTemperaturaOn);
  }
  
  Diagnostica.Byte1 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_1);
  Diagnostica.Byte2 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_2);
  Diagnostica.Byte3 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_3);
  Diagnostica.Byte4 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_4);
  Diagnostica.Byte5 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_5);
  Diagnostica.Byte6 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_6);
  Diagnostica.Byte7 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_7);
  Diagnostica.CountUnpError = EEPROM_ReadByte(EEPROM_CONTA_UNP);
  Consumo_uA_h_tot = EEPROM_ReadWord(EEPROM_CONSUMO_TOT);
  Dati.NumeroGiriCw = EEPROM_ReadWord(EEPROM_NUMERO_GIRI_TOT_CW);
  Dati.NumeroGiriACw = EEPROM_ReadWord(EEPROM_NUMERO_GIRI_TOT_ACW);
  Dati.ContaShock = EEPROM_ReadWord(EEPROM_CONTA_SHOCK);
  Dati.ContaFreeFall = EEPROM_ReadWord(EEPROM_CONTA_FREE_FALL);
  Dati.IndiceTrasmissione = EEPROM_ReadWord(EEPROM_INDICE_TRASMISSIONE);
  indice = sprintf((char*)Dati.VersioneFwTx,"%u", VersioneFW); 
  sprintf((char*)Dati.VersioneFwTx + indice,".%u", SottoversioneFW);
  indice = sprintf((char*)Dati.VersioneHwTx,"%u", VersioneHW); 	
  sprintf((char*)Dati.VersioneHwTx + indice,".%u", SottoversioneHW); 
  Dati.InputVariation = '0';//init del campo per evitare di avere 0x00 imprevisto in buffer dati JSON
  ContaTxPreActive = EEPROM_ReadWord(EEPROM_PREACTIVE_COUNT);
  stato_alesea1 = EEPROM_ReadByte( EEPROM_STATO_ALESEA1 );
  stato_alesea2 = EEPROM_ReadByte( EEPROM_STATO_ALESEA2 );
  stato_alesea3 = EEPROM_ReadByte( EEPROM_STATO_ALESEA3 );	
  if( (stato_alesea1 == stato_alesea2) && (stato_alesea1 == stato_alesea3) )	//se le tre letture sono concordi copio il dato in ram
  {
    Dati.StatoAlesea = (AleseaStato)stato_alesea1;
  }
  else	//se non sono concordi mi porto in ACTIVE
  {
    Dati.StatoAlesea = ALESEA_ACTIVE;
    EEPROM_WriteByte( EEPROM_STATO_ALESEA1, Dati.StatoAlesea );
    EEPROM_WriteByte( EEPROM_STATO_ALESEA2, Dati.StatoAlesea );
    EEPROM_WriteByte( EEPROM_STATO_ALESEA3, Dati.StatoAlesea );
    
    NdefCompile(Dati.StatoAlesea); 
    
  }
  if( (Dati.StatoAlesea == ALESEA_PRE_COLLAUDO) || (Dati.StatoAlesea == ALESEA_COLLAUDO) )
  {
    Dati.SchedulingInterval = PERIODO_COLLAUDO;
    EEPROM_WriteWord( EEPROM_TX_TIME1, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord( EEPROM_TX_TIME2, (int32_t)Dati.SchedulingInterval );
    EEPROM_WriteWord( EEPROM_TX_TIME3, (int32_t)Dati.SchedulingInterval );
  }
  
  CRC_calcolato = CalcolaCRC();
  EEPROM_WriteWord(EEPROM_CRC, CRC_calcolato);
  
  
}

