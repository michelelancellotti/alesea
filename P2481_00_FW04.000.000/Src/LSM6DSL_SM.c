#include "LSM6DSL.h"
#include "LSM6DSL_driver.h"
#include "LSM6DSL_SM.h"
#include "EEPROM.h"
#include "Gestione_dato.h"
#include "stm32l0xx_hal.h"

AleseaMacchinaStatiMems StatoMEMS;
AleseaMacchinaStatiCalibrazioneMems StatoCalibrazioneMEMS;
AleseaMacchinaStatiMovimento StatoMovimento;
uint8_t FlagWakeupDaMEMS;
uint8_t  FlagRefreshRotation;
uint8_t FlagWakeUpDaShock;
uint8_t FlagWakeUpDaMovimento;
uint8_t FlagWakeUpDaFreeFall;
uint8_t FlagWakeupPerTemp;
uint8_t PrimoAvvioMEMS;
float AccelerazioneZ;
float AccelerazioneMediaZ;
uint8_t ContaSampleZ;
float NumeroGiriFloat;
float NumeroGiriDefFloat;
float NumeroGiriFloatFase;
uint8_t FlagTemperaturaAcquisita;

uint8_t FlagModemOnInSpinSession;
uint8_t FlagSpinSessionStart;                                           
float SpinFreqAppoggioFase;
uint16_t SpinTimeCounter;
uint16_t SpinStartStopCounterAppoggio;

uint8_t FlagModemOnInShockSession;
uint8_t FlagModemOnInMovementSession;
uint8_t FlagShockSessionStart;   
uint8_t FlagMovementSessionStart;
uint8_t SpinTimeCounterEnable;

uint8_t MemsPausaTimer;

float mdps, mdps_sum, mdps_mean, dps_mean, rps_mean;
float CalibrazioneMems;
uint16_t VelAngolareCalibCounter;
uint16_t ContaEventiAppoggio;
uint8_t VelAngolareAppCounter; 
uint8_t SogliaAccelerometro;

int8_t ArrayTemperature[ARRAY_TEMPERATURE_DIM];
uint8_t ArrayTemperaturaIndex;

float NumeroGiriAccFloat;

mg_t mgx, mgy;
//float mgx, mgx_sum, mgx.mean;
//float mgy, mgy_sum, mgy.mean;
uint16_t AsseXAccAppCounter;
uint8_t MesureMode;

uint8_t Quadrante, QuadranteOld;

uint8_t DebugSession;
////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////MACCHINA A STATI CALIBRAZIONE ACCELEROMETRO////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

int32_t appoggio;
void MacchinaStatiCalibrazioneMEMS(void)
{
  switch(StatoCalibrazioneMEMS)
  {
  case MEMS_INIT_CALIBRAZIONE:
    LSM6DSL_GYRO_ON();
    MemsPausaTimer = 0;
    mdps_sum = 0;
    mdps = 0;
    VelAngolareCalibCounter = 0;
    StatoCalibrazioneMEMS = MEMS_PAUSA;
    break;
    
  case MEMS_PAUSA:
    MemsPausaTimer++;
    if(MemsPausaTimer > 100)    StatoCalibrazioneMEMS = MEMS_CALIBRAZIONE;
    break;
  case MEMS_CALIBRAZIONE:
    LSM6DSL_cycle();
    mdps = angular_rate_mdps[2];
    
    mdps_sum += mdps;
    VelAngolareCalibCounter++;
    if(VelAngolareCalibCounter >= N_CAMPIONI_CALIBRAZIONE_V_ANG)     //esecuzione ogni 1 secondo
    {
      VelAngolareCalibCounter = 0;
      mdps_mean = mdps_sum/N_CAMPIONI_CALIBRAZIONE_V_ANG;
      mdps_sum = 0;
      dps_mean = mdps_mean; 
      flagCalibrazioneMems = 0;
      StatoCalibrazioneMEMS = MEMS_INIT_CALIBRAZIONE;
      if( (dps_mean < -MEMS_CALIB_MAX_VALUE) || (dps_mean > MEMS_CALIB_MAX_VALUE) )
      {
        dps_mean = 0;
      }
      EEPROM_WriteWord( EEPROM_MEMS_CALIBRATION, (int32_t)(dps_mean*1000.0) );
      CalibrazioneMems = ((float)EEPROM_ReadWord(EEPROM_MEMS_CALIBRATION))/1000; 
    }
    break;
  }
}


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////MACCHINA A STATI ACCELEROMETRO////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

void MacchinaStatiMEMS(void)
{
  
  switch(StatoMEMS)
  {
  case MEMS_INIT_CONTA_GIRI:
    SetAccRunFreq();
    #ifdef FREE_FALL_ON
      SetInterruptFreeFall(FALL_DUR_RUN_MODE); 
    #endif
    LSM6DSL_GYRO_ON();
    
    if(Dati.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON)
    {
      if(GetOrientamento()!= 1) return;
    }
    if( (Dati.Orientamento == FLANGIA_A_TERRA) && ( Dati.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON ) ) //rotazione orizzontale
      LSM6DSL_interrupt_timer = FILTRO_ROTAZ_ORIZZ_FERMO_DEFAULT*100;
    else
      LSM6DSL_interrupt_timer = Dati.FiltroMovimento*100;

    
    StatoMEMS = MEMS_CONTA_GIRI;
    MesureMode = QUADRANTI;
    SpinStartStopCounterAppoggio++;
    SpinTimeCounterEnable = 1;
    
    mdps_sum = 0;
    VelAngolareAppCounter = 0;
    ContaEventiAppoggio = 0;
    if(Dati.SpinSession == 0)
    {
      TotGiriGyr = 0;
      TotGiriAcc = 0;
    }
    
    break;
    
  case MEMS_CONTA_GIRI:
    
    LSM6DSL_cycle();
   
    mdps = angular_rate_mdps[2];
    mdps_sum += mdps;
    VelAngolareAppCounter++;
    if(VelAngolareAppCounter >= N_CAMPIONI_V_ANG)     //esecuzione ogni 1 secondo
    {
      VelAngolareAppCounter = 0;
      mdps_mean = mdps_sum/N_CAMPIONI_V_ANG;
      mdps_sum = 0;
      mdps = 0;
      dps_mean = mdps_mean;    
      #ifdef CALIBRAZIONE_GYRO_ON
        dps_mean -= CalibrazioneMems;  
      #endif
      rps_mean = dps_mean/360;
      NumeroGiriFloat += rps_mean;
      
      ContaEventiAppoggio++;
    }


    mgx.val = acceleration_mg[0];
    if(mgx.val >= QUADRANTE_ISTERESI) mgx.sign = 1;
      else if(mgx.val < -QUADRANTE_ISTERESI) mgx.sign = -1;
    mgx.sum += mgx.sign;
    mgy.val = acceleration_mg[1];
     if(mgy.val >= 0) mgy.sign = 1;
      else mgy.sign = -1;
    mgy.sum += mgy.sign;
    AsseXAccAppCounter++;
    if(AsseXAccAppCounter >= N_CAMPIONI_ACC)
    {
      AsseXAccAppCounter = 0;
      mgx.mean = mgx.sum/N_CAMPIONI_ACC;
      mgx.sum = 0;
      mgx.val = 0;
      mgy.mean = mgy.sum/N_CAMPIONI_ACC;
      mgy.sum = 0;
      mgy.val = 0;
      
      if( 
           (((FlagRefreshRotation == 1) || (Quadrante != QuadranteOld)) && (Dati.Orientamento == FLANGIA_PERPENDICOLARE_TERRA) ) ||
             (( (rps_mean > 0.1) || (rps_mean < -0.1) ) && (Dati.Orientamento == FLANGIA_A_TERRA) && (Dati.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON))   
                     
          )   //riconoscimento stato movimento/fermo      
      {  
        FlagRefreshRotation = 0;
        QuadranteOld = Quadrante;

        if( (Dati.Orientamento == FLANGIA_A_TERRA) && (Dati.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON) )  //rotazione orizzontale
          LSM6DSL_interrupt_timer = FILTRO_ROTAZ_ORIZZ_FERMO_DEFAULT*100;
        else
          LSM6DSL_interrupt_timer = Dati.FiltroMovimento*100;

        
        
   
        
        ///////////////////////////////////START SESSIONE SPIN////////////////////////////////////////////
        //inizio della sessione di rotazione su condizione che non sia ancora iniziata e che sia stato compiuto almeno un giro
        if( /*!FlagSpinSessionStart*/ (Dati.SpinSession == 0) && ( (NumeroGiriDefFloat < -1) || (NumeroGiriDefFloat > 1) ) )
        {
          FlagModemOnInSpinSession = 1;    //Inizia la sessione di spin => Start macchina a stati Modem
          FlagNoRxMQTT = 1;                //inibisco la ricezione sulla coda MQTT
          Dati.SpinSession = 1;
          Dati.SpinFreq = 0;
          if(NumeroGiriDefFloat >= 1)        Dati.NumeroGiriACw++;//Aggiunto 1 giro
          else if(NumeroGiriDefFloat <= -1)	Dati.NumeroGiriCw++;//Aggiunto 1 giro
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////
      } 
      switch(Quadrante)
      {
      case 0:
        if( (mgx.mean > 0) && (mgy.mean > 0) ) Quadrante = 0;
        else if( (mgx.mean > 0) && (mgy.mean < 0) ) { Quadrante = 1; NumeroGiriAccFloat += 0.25; }
        else if( (mgx.mean < 0) && (mgy.mean < 0) ) Quadrante = 2;
        else if( (mgx.mean < 0) && (mgy.mean > 0) ) { Quadrante = 3; NumeroGiriAccFloat -= 0.25; }
        break;
        
      case 1:
        
        if( (mgx.mean > 0) && (mgy.mean > 0) ) { Quadrante = 0; NumeroGiriAccFloat -= 0.25; }
        else if( (mgx.mean > 0) && (mgy.mean < 0) ) Quadrante = 1;
        else if( (mgx.mean < 0) && (mgy.mean < 0) ) { Quadrante = 2; NumeroGiriAccFloat += 0.25; }
        else if( (mgx.mean < 0) && (mgy.mean > 0) ) Quadrante = 3;
        break;
        
      case 2:
        
        if( (mgx.mean > 0) && (mgy.mean > 0) ) Quadrante = 0;
        else if( (mgx.mean > 0) && (mgy.mean < 0) ) { Quadrante = 1; NumeroGiriAccFloat -= 0.25; }
        else if( (mgx.mean < 0) && (mgy.mean < 0) ) Quadrante = 2;
        else if( (mgx.mean < 0) && (mgy.mean > 0) ) { Quadrante = 3; NumeroGiriAccFloat += 0.25; }
        break;
        
      case 3:
        
        if( (mgx.mean > 0) && (mgy.mean > 0) ) { Quadrante = 0; NumeroGiriAccFloat += 0.25; }
        else if( (mgx.mean > 0) && (mgy.mean < 0) ) Quadrante = 1;
        else if( (mgx.mean < 0) && (mgy.mean < 0) ) { Quadrante = 2; NumeroGiriAccFloat -= 0.25; }
        else if( (mgx.mean < 0) && (mgy.mean > 0) ) Quadrante = 3;
        break;
      }
    }
    ///////////////////
   
    //test gyro
    TotGiriGyr += NumeroGiriFloat; 
    TotGiriAcc += NumeroGiriAccFloat;
    //
    
    
    //gestione soglia vel angolare per cambio tipo MeasureMode
    if( (Dati.Orientamento != FLANGIA_A_TERRA) || (Dati.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_OFF))  //rotazione standard
    {  
      if( (rps_mean > SOGLIA_SUP_RPS_CNG_MODE) || (rps_mean < -SOGLIA_SUP_RPS_CNG_MODE) )
      {
        MesureMode = GYRO;
      }
      else if( (rps_mean < SOGLIA_INF_RPS_CNG_MODE) && (rps_mean > -SOGLIA_INF_RPS_CNG_MODE) )
      {
        MesureMode = QUADRANTI;
      }
    }
    
    //
      
    if( (Dati.Orientamento == FLANGIA_A_TERRA) && (Dati.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON) )  //rotazione orizzontale
      NumeroGiriDefFloat += NumeroGiriFloat;
    else        //se flangia perpendicolare a terra
    {
      if(MesureMode == GYRO)    //se impostato uso giroscopio
      {
        NumeroGiriDefFloat += NumeroGiriFloat;
      }
      else if(MesureMode == QUADRANTI)  //se impostato uso accelerometro con monitoraggio quadranti
      {
        NumeroGiriDefFloat += NumeroGiriAccFloat;
      }
    }

    
    NumeroGiriFloat = 0;
    NumeroGiriAccFloat = 0;
    ////////////////////
    
    
    
    if(LSM6DSL_interrupt_timer > 0)
    {
      LSM6DSL_interrupt_timer--;		
    }
    else if(LSM6DSL_interrupt_timer == 0)
    {
      StatoMEMS = MEMS_SALVA_GIRI;
      SpinTimeCounterEnable = 0;
    }		
    break;
    
  case MEMS_SALVA_GIRI:	//salvo la rotazione compiuta nell'opportuna variabile. 

    //NumeroGiriDefFloat = NumeroGiriAccFloat
    if(NumeroGiriDefFloat >= 1)	//solo se ha compiuto giri memorizzo
    {
      Dati.NumeroGiriACw += (uint32_t)NumeroGiriDefFloat;
      //Dati.NumeroGiriACw++;//Aggiunto 1 giro!
    }
    else if(NumeroGiriDefFloat <= -1)	//solo se ha compiuto giri memorizzo
    {
      NumeroGiriDefFloat = -1*NumeroGiriDefFloat;
      Dati.NumeroGiriCw += (uint32_t)(NumeroGiriDefFloat);
      //Dati.NumeroGiriCw++;//Aggiunto 1 giro!
      //acquisisco la temperatura e salvo in EEPROM l'evento
    }
    
    if(Dati.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON) FlagWakeupPerTemp = 1;
    /*modificato meccanismo di temperatura su salvataggio giri
    GetTemperature();
    FlagTemperaturaAcquisita = 1;
    EEPROM_WriteByte( EEPROM_FLAG_TEMPERATURA_ACQ, FlagTemperaturaAcquisita );
    */
    NumeroGiriFloatFase += NumeroGiriDefFloat;
    
    NumeroGiriDefFloat = 0;
    NumeroGiriFloat = 0;
    NumeroGiriAccFloat = 0;
    
    EEPROM_WriteWord( EEPROM_NUMERO_GIRI_TOT_CW, Dati.NumeroGiriCw );
    EEPROM_WriteWord( EEPROM_NUMERO_GIRI_TOT_ACW, Dati.NumeroGiriACw );
    StatoMEMS = MEMS_GO_TO_STOP_MODE;
    break;
    
  case MEMS_GO_TO_STOP_MODE:
    StatoMEMS = MEMS_INIT_CONTA_GIRI;
    SetAccStopFreq();
    HAL_Delay(100);     //inserito perch� con delay non scatena interrupt
    #ifdef FREE_FALL_ON
      SetInterruptFreeFall(FALL_DUR_STOP_MODE);  
    #endif
    FlagWakeupDaMEMS = 0;	
    break;
  }
}


void MacchinaStatiMovimento(void)
{
  switch(StatoMovimento)
  {
    case MOVE_INIT:
      Dati.MovementSession = 1;
      FlagModemOnInMovementSession = 1;       
      FlagNoRxMQTT = 1;                //inibisco la ricezione sulla coda MQTT
      if(Dati.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_OFF)      //se abilitata la rotazione orizzontale escludo il cambio scala per la gestione shock
      {
        SogliaAccelerometro = Dati.SogliaShock;
        SogliaAccelerometroSet(SogliaAccelerometro);
      }
      StatoMovimento = MOVE_RUN;
      break;
      
    case MOVE_RUN:
      if(FlagModemOnInMovementSession == 0)    StatoMovimento = MOVE_STOP;
      break;
      
    case MOVE_STOP:
      FlagWakeUpDaMovimento = 0;
      StatoMovimento = MOVE_INIT;
      break;
  }
  
}



////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////MACCHINA A STATI FREE FALL/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
void MacchinaStatiFreeFall(void)
{
  Dati.ContaFreeFall++;
  EEPROM_WriteWord(EEPROM_CONTA_FREE_FALL, Dati.ContaFreeFall);
  FlagWakeUpDaFreeFall = 0;	
}


////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////MACCHINA A STATI SHOCK/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
void MacchinaStatiShock(void)
{
  Dati.ContaShock++;
  EEPROM_WriteWord(EEPROM_CONTA_SHOCK, Dati.ContaShock);
  FlagWakeUpDaShock = 0;	
  if(Dati.ShockSession == 0)  //accendo il modem solo se � il primo evento di shock della serie
  {
    FlagModemOnInShockSession = 1;    //Inizia la sessione di spin => Start macchina a stati Modem
    //FlagShockSessionStart = 1;        //Tengo traccia dell'inizio della sessione di spin
    FlagNoRxMQTT = 1;                //inibisco la ricezione sulla coda MQTT
    Dati.ShockSession = 1;
  }
}

////////////////////////////////////////////////////////////////////////
////////////////RICAVA ORIENTAMENTO DELLA BOBINA////////////////////////
////////////////////////////////////////////////////////////////////////
float debug_accel[32];
/**
uint8_t GetOrientamento(void)
{
  uint8_t result;
  float appoggio;
  for(uint8_t i = 0; i < NUMERO_CAMPIONI_ORIENTAMENTO; i++)
  {
    appoggio = LSM6DSL_Orientamento();
    debug_accel[i] = appoggio;
    if(appoggio < 0)   appoggio = -1*appoggio;
    AccelerazioneZ += appoggio;
    ContaSampleZ++;
  }
  if(ContaSampleZ == NUMERO_CAMPIONI_ORIENTAMENTO)
  {
    ContaSampleZ = 0;
    AccelerazioneMediaZ = AccelerazioneZ/NUMERO_CAMPIONI_ORIENTAMENTO;
    AccelerazioneZ = 0;	
    if(AccelerazioneMediaZ < SOGLIA_ORIENTAMENTO)      Dati.Orientamento = FLANGIA_PERPENDICOLARE_TERRA;
    else Dati.Orientamento = FLANGIA_A_TERRA;
    result = 1;
  }
  else result = 0;
  return result;
}*/


uint8_t GetOrientamento(void)
{
  uint8_t result;
  
  float appoggio;
  appoggio = LSM6DSL_Orientamento();
  debug_accel[ContaSampleZ] = appoggio;
  if(appoggio < 0)   appoggio = -1*appoggio;
  AccelerazioneZ += appoggio;
  ContaSampleZ++;
  if(ContaSampleZ == NUMERO_CAMPIONI_ORIENTAMENTO)
  {
    ContaSampleZ = 0;
    AccelerazioneMediaZ = AccelerazioneZ/NUMERO_CAMPIONI_ORIENTAMENTO;
    AccelerazioneZ = 0;	
    if(AccelerazioneMediaZ < SOGLIA_ORIENTAMENTO)      Dati.Orientamento = FLANGIA_PERPENDICOLARE_TERRA;
    else Dati.Orientamento = FLANGIA_A_TERRA;
    result = 1;
  }
  else result = 0;
  return result;
   
}

////////////////////////////////////////////////////////////////////////
/////////////////////////LETTURA DELLA TEMPERATURA//////////////////////
////////////////////////////////////////////////////////////////////////
uint8_t GetTemperature(void)
{
  float temperatura_temp;
  
  temperatura_temp = LSM6DSL_Temperatura();		// Misura della temperatura e messa in struttura
  Dati.Temperatura = (int8_t)temperatura_temp;
  if(temperatura_temp == 0xFF)
  {
    return 0;
  }

  ArrayTemperature[ArrayTemperaturaIndex] = Dati.Temperatura;
  
  if(ArrayTemperaturaIndex == 0)
  {
    Dati.TemperaturaMax = Dati.Temperatura;
    Dati.TemperaturaMin = Dati.Temperatura;
  } 
  
  if( Dati.Temperatura > Dati.TemperaturaMax )    Dati.TemperaturaMax = Dati.Temperatura;
  if( Dati.Temperatura < Dati.TemperaturaMin )    Dati.TemperaturaMin = Dati.Temperatura;
  ArrayTemperaturaIndex++;
  if(ArrayTemperaturaIndex >= ARRAY_TEMPERATURE_DIM) ArrayTemperaturaIndex = 0;
  return 1;
}

