#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ndef_lib.h"


struct ndef_record ndef_create(
        uint8_t tnf, bool is_begin, bool is_end, bool is_chunk,
        bool is_short, bool has_id,
        char* type, uint8_t type_length,
        char* id, uint8_t id_length,
        uint8_t* payload, uint32_t payload_length) {
    int i, j;
		char Fix[] = {0x02,'e','n'};
    struct ndef_record record;

    // one byte for header, one for type length, one or four for payload length,
    // zero or one for id length
    record.length = 2 + (is_short ? 1 : 4) + (has_id ? 1 : 0)
        + type_length + id_length + payload_length;


    record.type_length = type_length;
    record.id_length = has_id ? id_length : 0;
    record.payload_length = payload_length;

    record.buffer[0] = tnf;

    if (is_begin) record.buffer[0] |= 0x80;
    if (is_end)   record.buffer[0] |= 0x40;
    if (is_chunk) record.buffer[0] |= 0x20;
    if (is_short) record.buffer[0] |= 0x10;
    if (has_id)   record.buffer[0] |= 0x08;

    i = 1;
    record.buffer[i++] = type_length;
    if (is_short) {

        record.buffer[i++] = (uint8_t) payload_length;
    } else {
        for (j = 0; j < 4; ++j) {
            record.buffer[i++] = 0xFF & (payload_length >> ((3 - j) * 8));
        }
    }
    if (has_id) {
        record.buffer[i++] = id_length;
    }

    record.type_offset = i;
    record.id_offset = record.type_offset + record.type_length;
    record.payload_offset = record.id_offset + record.id_length;
    record.length = record.payload_offset + record.payload_length;

    memcpy(record.buffer + record.type_offset, type, type_length);
    if (has_id && id_length > 0) {
        memcpy(record.buffer + record.id_offset, id, id_length);
    }
		memcpy(record.buffer + record.payload_offset, Fix, sizeof(Fix));
    memcpy(record.buffer + record.payload_offset+sizeof(Fix), payload, payload_length);

    return record;

}
