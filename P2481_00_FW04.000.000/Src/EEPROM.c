#include "EEPROM.h"
#include "stm32l0xx_hal.h"
#include "Gestione_dato.h"


/* Scrive una word in EEPROM */
void EEPROM_WriteWord(uint32_t Indirizzo, int32_t Dato)
{
  HAL_FLASHEx_DATAEEPROM_Unlock();
  
  HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, Indirizzo++, (uint8_t)(Dato >> 0));
  HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, Indirizzo++, (uint8_t)(Dato >> 8));
  HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, Indirizzo++, (uint8_t)(Dato >> 16));
  HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, Indirizzo, (uint8_t)(Dato >> 24));
  
  HAL_FLASHEx_DATAEEPROM_Lock();
}


/* Scrive un byte in EEPROM */
uint32_t EEPROM_WriteByte(uint32_t Indirizzo, uint8_t Dato)
{
  uint32_t status = 0;
  if( HAL_FLASHEx_DATAEEPROM_Unlock() != HAL_OK)
  {
    status |= 1;
    if( HAL_FLASHEx_DATAEEPROM_Unlock() != HAL_OK)
    {
      status |= 2;
      if( HAL_FLASHEx_DATAEEPROM_Unlock() != HAL_OK)
      {
        status |= 4;
      }
    }
  }
  HAL_Delay(50);
  if( HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, Indirizzo, Dato) != HAL_OK ) 
  {
    status |= 8;
    if( HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, Indirizzo, Dato) != HAL_OK ) 
    {
      status |= 16;
      if( HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, Indirizzo, Dato) != HAL_OK ) 
      {
        status |= 32;
      }
    }
  }
  if( HAL_FLASHEx_DATAEEPROM_Lock() != HAL_OK)
  {
    status |= 64;
    if( HAL_FLASHEx_DATAEEPROM_Lock() != HAL_OK)
    {
      status |= 128;
      if( HAL_FLASHEx_DATAEEPROM_Lock() != HAL_OK)
      {
        status |= 256;
      }
    }
  }
  return status;
}


/* Legge una word in EEPROM */
int32_t EEPROM_ReadWord(uint32_t Posizione)
{
  uint8_t appoggio;
  int32_t DatoLettoEEPROM;
  
  appoggio = *(int8_t*)Posizione++;
  DatoLettoEEPROM = ((int32_t)appoggio) & 0x000000FF;
  
  appoggio = *(int8_t*)Posizione++;
  DatoLettoEEPROM |= ( ((int32_t)appoggio)<<8) & 0x0000FF00;
  
  appoggio = *(int8_t*)Posizione++;
  DatoLettoEEPROM |= ( ((int32_t)appoggio)<<16) & 0x00FF0000;
  
  appoggio = *(int8_t*)Posizione;
  DatoLettoEEPROM |= ( ((int32_t)appoggio)<<24) & 0xFF000000;
  
  return DatoLettoEEPROM;
}

/* Legge un byte in EEPROM */
uint8_t EEPROM_ReadByte(uint32_t Posizione)
{
  uint32_t align_offset = (Posizione%4);
  uint32_t align_posizione = Posizione - align_offset;
  uint32_t WordLettaEEPROM = *(uint32_t*)align_posizione;
  uint8_t DatoLettoEEPROM = (WordLettaEEPROM >> (8*align_offset));      //Workaround necessario perch� se l'indirizzo non � allineato a 4B non si riesce a leggere (HARD FAULT) 
  return DatoLettoEEPROM;
}





