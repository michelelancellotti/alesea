#include "main.h"
#include "Gestione_dato.h"
#include "Gestione_batteria.h"
#include "nfc_SM.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>

AleseaMacchinaStatiNFC StatoNFC, StatoFuturoNFC;
uint8_t FlagWakeupDaNFC;
uint8_t TimerPresenzaNFC;

uint32_t CountFail;
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////MACCHINA A STATI NFC////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
uint8_t TimerCambioStatoNFC;
uint8_t CampoPresente;
uint8_t NotificaEnd;
void MacchinaStatiNFC(void)
{
  //CampoPresente = !HAL_GPIO_ReadPin(FD_GPIO_Port, FD_Pin);	//Presenza TAG
  
 if( NotificaEnd )	
  {
    NotificaEnd = 0;  
    HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_RESET);        //Disalimento TAG
    StatoNFC = NFC_START;
    FlagWakeupDaNFC = 0;
    return;
  }
  
  
  switch(StatoNFC)
  {
  case NFC_WAIT:
    if(TimerCambioStatoNFC > 0)		TimerCambioStatoNFC--;
    else		StatoNFC = StatoFuturoNFC;
    break;
    
  case NFC_START:
    HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_SET);	//Alimento TAG
    Dati.CaricaResidua = CalcolaCaricaResidua(Consumo_uA_h_tot);
    StatoNFC = NFC_WAIT;
    StatoFuturoNFC = NFC_READ;
    TimerCambioStatoNFC = 10;	//ritardo prima del cambio stato (base 100ms)
    break;
    
  case NFC_READ:
    //leggo il tag e compilo il buffer
    NfcRead();
    StatoNFC = NFC_DECODE;/*NFC_WAIT;*/
    StatoFuturoNFC = NFC_DECODE;
    TimerCambioStatoNFC = 0; //ritardo prima del cambio stato (base 100ms)
    break;
    
  case NFC_DECODE:
    //decodifico il buffer e compilo le strutture
    NdefDecode();
    StatoNFC = NFC_CHECK;/*NFC_WAIT;*/
    StatoFuturoNFC = NFC_CHECK;
    TimerCambioStatoNFC = 0; //ritardo prima del cambio stato (base 100ms)
    break;
    
  case NFC_CHECK:
    //verifico il codice inviato da app e gestisco il comando.
    if(VerificaCodiceNFC())
    {
      GestioneComandiNdef();			
      ClearL();
      //StatoNFC = NFC_WAIT;
      //StatoFuturoNFC = NFC_END;
      //TimerCambioStatoNFC = 50; //ritardo prima del cambio stato (base 100ms)
    }
    StatoNFC = NFC_WRITE;/*NFC_WAIT;*/
    StatoFuturoNFC = NFC_WRITE;
    TimerCambioStatoNFC = 0;//50; //ritardo prima del cambio stato (base 100ms)
    /*
    else	//se codice errato o non presente ritorno in stato READ
    {
    StatoNFC = NFC_WAIT;
    StatoFuturoNFC = NFC_READ;
    TimerCambioStatoNFC = 0;//20; //ritardo prima del cambio stato (base 100ms)
    CountFail++;
  }
    */
    break;
    
   case NFC_WRITE:
    NdefCompile(Dati.StatoAlesea);
    StatoNFC = NFC_END;/*NFC_CHECK_WRITE;*/
    break;
     
    
  case NFC_END:
    StatoNFC = NFC_START;/*NFC_WAIT;*/
    StatoFuturoNFC = NFC_START;
    if(Dati.StatoAlesea == ALESEA_COLLAUDO)
    {  
      flagCalibrazioneMems = 1;
    }
    NotificaEnd = 1;
    break;		
  }
}

