#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ndef.h"
#include "ndef_lib.h"
#include "nfc.h"
#include "Gestione_dato.h"
#include "stm32l0xx_hal.h"
#include "EEPROM.h"
#include "Globals.h"

//NDEF
uint8_t BufferNdefTxShortOffset;
struct ndef_payload P1,P2,P3,P4;
struct ndef_record R1,R2,R3,R4;
struct ndef_payload L1,L2,L3,L4;
uint8_t BufferNdefTx[150];
uint8_t BufferNdefTxShort[16];
uint8_t BufferNdefTxIndex;
uint8_t BufferNdefTxStop;



/* Decodifica dei campi NDEF */
void NdefDecode(void)
{
	uint8_t CampoNdef = 1;
	for(uint8_t i = 0; i < DIMENSIONE_BUFFER_LETTURA_NFC-1; i++)
	{
		if( (BufferLetturaNFC[i] == 'T') && (BufferLetturaNFC[i+1] == 0x02) )
		{
			switch(CampoNdef)
			{
				case 1:
					if( (BufferLetturaNFC[i-1] - 3) > 0)  L1.length = BufferLetturaNFC[i-1] - 3;
					memcpy(&L1.buffer[0], (char *) &BufferLetturaNFC[i+4], L1.length);
					CampoNdef++;
				break;
				
				case 2:
					if( (BufferLetturaNFC[i-1] - 3) > 0)	L2.length = BufferLetturaNFC[i-1] - 3;
					memcpy(&L2.buffer[0], (char *) &BufferLetturaNFC[i+4], L2.length);
					CampoNdef++;
				break;
				
				case 3:
					if( (BufferLetturaNFC[i-1] - 3) > 0)	L3.length = BufferLetturaNFC[i-1] - 3;
					memcpy(&L3.buffer[0], (char *) &BufferLetturaNFC[i+4], L3.length);
					CampoNdef++;
				break;
				
				case 4:
					if( (BufferLetturaNFC[i-1] - 3) > 0)	L4.length = BufferLetturaNFC[i-1] - 3;
					memcpy(&L4.buffer[0], (char *) &BufferLetturaNFC[i+4], L4.length);
					CampoNdef++;
				break;
				
				default:
					
				break;
			}
		}	
	}
}

/* Gestione dei comandi NDEF */
void GestioneComandiNdef(void)
{
	uint32_t  Minuto;
	if( (L4.buffer[0] >= 0x30) && (L4.buffer[0] <= 0x39) )	Dati.InputVariation = L4.buffer[0]/* - 0x30*/;
	switch(L4.buffer[0])
	{
		case NFC_ATTIVAZIONE:
			Dati.InputVariation = L4.buffer[0];
			FlagAttivazione = 1;
		break;
		
		case NFC_DISATTIVAZIONE:
			Dati.InputVariation = L4.buffer[0];
			FlagDisattivazione = 1;
		break;
		
		case NFC_SET_PERIODO:
			Dati.InputVariation = L4.buffer[0];
			FlagSincronizzazione = 1;
			if(L4.buffer[1] == ';')
			{
				Minuto = 0;
				for(uint8_t i = 2; i < L4.length; i++)
				{
					if( (L4.buffer[i] >= 0x30) && (L4.buffer[i] <= 0x39) )		//se � un numero...
					{
						Minuto = (Minuto*10) + (L4.buffer[i] - 0x30);
					}
				}
				
				Dati.SchedulingInterval = Minuto*60;
				if(Dati.SchedulingInterval > LIMITE_SCHEDULING_INTERVAL)	
				{
					Dati.SchedulingInterval = LIMITE_SCHEDULING_INTERVAL;
				}	
				EEPROM_WriteWord( EEPROM_TX_TIME1, (int32_t)Dati.SchedulingInterval );
				EEPROM_WriteWord( EEPROM_TX_TIME2, (int32_t)Dati.SchedulingInterval );
				EEPROM_WriteWord( EEPROM_TX_TIME3, (int32_t)Dati.SchedulingInterval );
				EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE1, (int32_t)Dati.SchedulingInterval);
				EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE2, (int32_t)Dati.SchedulingInterval);
				EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE3, (int32_t)Dati.SchedulingInterval);
			}
		break;
		
		case NFC_FACTORY_RESET:
			Dati.InputVariation = L4.buffer[0];
			FlagFactoryReset = 1;
		break;
		
		case NFC_SPIN_RESET:
			Dati.InputVariation = L4.buffer[0];
			FlagSpinReset = 1;
		break;
		
		case NFC_UPGRADE_FW:
			Dati.InputVariation = L4.buffer[0];
			if( (L4.buffer[1] == ';') && (L4.buffer[3] == ';') )
			{	
				if( (L4.buffer[2] >= 0x30) && (L4.buffer[2] <= 0x39) )		//se � un numero...
				{
					VersioneFWRX = L4.buffer[2] - 0x30;
				}
				
				if( (L4.buffer[4] >= 0x30) && (L4.buffer[4] <= 0x39) )		//se � un numero...
				{
					SottoversioneFWRX = L4.buffer[4] - 0x30;
				}
				
				if( (VersioneFWRX > VersioneFW) || ((VersioneFWRX == VersioneFW) && (SottoversioneFWRX > SottoversioneFW)) )		
				{
					FlagUpgradeFw = 1;
				}		
				else	
				{
					FlagUpgradeFw = 0;
				}
			}
		break;
		
		case NFC_INSTANT_MESSAGE:
			Dati.InputVariation = L4.buffer[0];
			FlagInstantMessage = 1;
		break;
		
		default:
			
		break;
	}
}

/*Formattazione dei messaggi NDEF*/
void NdefCompile(uint8_t stato)
{
	uint8_t t;
	P1.length = 0;
	P2.length = 0;
	P3.length = 0;
	P4.length = 0;
	
	for(uint8_t y = 0; y < NDEF_BUFFER_SIZE; y++)
	{
		P1.buffer[y] = 0;
		P2.buffer[y] = 0;
		P3.buffer[y] = 0;
		P4.buffer[y] = 0;
	}
	//compilo ndef1
	P1.length = sprintf( (char*)P1.buffer,"%s", Dati.ICCID);
	R1 = ndef_create(0x01, true, false, false, true, false,"T", 1,NULL, 0, P1.buffer, P1.length+3);
				
	//compilo ndef2			
	switch(stato)
	{
		case ALESEA_ACTIVE: P2.length = sprintf( (char*)P2.buffer,"%s", "ACTIVE");	break;
		case ALESEA_READY: P2.length = sprintf( (char*)P2.buffer,"%s", "READY");	break;
		case ALESEA_COLLAUDO: P2.length = sprintf( (char*)P2.buffer,"%s", "DEBUG");	break;
		case ALESEA_PRE_COLLAUDO: P2.length = sprintf( (char*)P2.buffer,"%s", "PRECO");	break;	
	}			
	R2 = ndef_create(0x01, false, false, false, true, false, "T", 1, NULL, 0, P2.buffer, P2.length+3);
	//compilo ndef3
	P3.length = sprintf( (char*)P3.buffer + P3.length,"20%s", Dati.data_ora);	
	P3.length += sprintf( (char*)P3.buffer + P3.length,";%u", Dati.IndiceTrasmissione);
	//inserisco nel campo ndef non l'intervallo attuale ma quello che verr� utilizzato in active
	P3.length += sprintf( (char*)P3.buffer + P3.length,";%u", (uint32_t)(EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE1)/60));	
	//P3.length += sprintf( (char*)P3.buffer + P3.length,";%u", (uint32_t)(Dati.SchedulingInterval/60));	
	P3.length += sprintf( (char*)P3.buffer + P3.length,";%u", Dati.CaricaResidua);	
	P3.length += sprintf( (char*)P3.buffer + P3.length,";%u", Dati.NumeroGiriCw);	
	P3.length += sprintf( (char*)P3.buffer + P3.length,";%u", Dati.NumeroGiriACw);	
	P3.length += sprintf( (char*)P3.buffer + P3.length,";%s", Dati.VersioneFwTx);			
	R3 = ndef_create(0x01, false, false, false, true, false, "T", 1, NULL, 0, P3.buffer, P3.length+3);
	//compilo ndef4						
	P4.length = sprintf( (char*)P4.buffer,"%s", "IN");			
	R4 = ndef_create(0x01, false, true, false, true, false, "T", 1, NULL, 0, P4.buffer, P4.length+3);
	
	//Copio le strutture nel buffer NDEF TX aggiungendo le parti fisse
	BufferNdefTxIndex = 0;
	BufferNdefTx[BufferNdefTxIndex] = 0x03;
	BufferNdefTx[++BufferNdefTxIndex] = P1.length + P2.length +P3.length +P4.length + 28;	
	memcpy(&BufferNdefTx[++BufferNdefTxIndex], (char *) R1.buffer, P1.length+7);
	BufferNdefTxIndex += (P1.length+7);
	memcpy(&BufferNdefTx[BufferNdefTxIndex], (char *) &R2.buffer, P2.length+7);
	BufferNdefTxIndex += (P2.length+7);				
	memcpy(&BufferNdefTx[BufferNdefTxIndex], (char *) &R3.buffer, P3.length+7);
	BufferNdefTxIndex += (P3.length+7);								
	memcpy(&BufferNdefTx[BufferNdefTxIndex], (char *) &R4.buffer, P4.length+7);
	BufferNdefTxIndex += (P4.length+7);	
	BufferNdefTx[BufferNdefTxIndex] = 0xFE;
	BufferNdefTxStop = BufferNdefTxIndex+1;
	//Spezzo il buffer il short e scrivo su tag
	BufferNdefTxShortOffset = 0;		
	for(uint8_t i = 0; i <= ((BufferNdefTxStop/16 +1)*(16)); i++)
	{
		t = i%16;
		BufferNdefTxShort[t] = BufferNdefTx[i];
		if(t == 15)
		{
			ScriviDatoNFC(&BufferNdefTxShort[0], BLOCCO_DESTINAZIONE_PREAMBOLO + BufferNdefTxShortOffset);
			HAL_Delay(100);
			BufferNdefTxShortOffset++;
		}
	}
}	

/* Lettura NFC in buffer dedicato */
void NfcRead(void)
{
	PulisciBufferLetturaNFC();
	for(uint8_t i = 0; i < NUMERO_BLOCCHI_LETTI; i++)
	{
		LeggiBloccoNFC(BLOCCO_DESTINAZIONE_DATI_RX + i, (uint8_t*)&BufferLetturaNFC[i*DIMENSIONE_BLOCCO_NFC]);
	}
}

/* Pulizia strutture NDEF */
void ClearL(void)
{
	L1.length = 0;
	L2.length = 0;
	L3.length = 0;
	L4.length = 0;
	for(uint8_t i = 0; i < NDEF_BUFFER_SIZE; i++)
	{
		L1.buffer[i] = 0;
		L2.buffer[i] = 0;
		L3.buffer[i] = 0;
		L4.buffer[i] = 0;
	}
}

/* Verifica del codice NFC scritto da app */
uint8_t VerificaCodiceNFC(void)
{
	if( (L2.buffer[0] == Dati.ICCID[0]) && (L2.buffer[1] == Dati.ICCID[3]) && (L2.buffer[2] == Dati.ICCID[7]) 
				&& (L2.buffer[3] == Dati.ICCID[11]) && (L2.buffer[4] == Dati.ICCID[15]) && (L2.buffer[5] == Dati.ICCID[19]) )
		return 1;
			else return 0;
}
