#include <string.h>
#include <stdint.h>
#include "dma.h"
#include "i2c.h"
#include "lptim.h"
#include "rtc.h"
#include "usart.h"
#include "gpio.h"

#ifndef _ALESEA_H
#define _ALESEA_H
	
#define CARICA_RESIDUA_SWITCH_OFF	10


void BaseDeiTempi(void);
void GestioneFlagComandi(void);


#endif
