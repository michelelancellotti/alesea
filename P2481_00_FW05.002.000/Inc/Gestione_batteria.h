#include <string.h>
#include <stdint.h>

extern float ConsumoFase[];	
extern uint32_t DurataFase[];		//espresso in s

extern uint8_t FlagConsumoGPSOn;
extern uint8_t FlagConsumoGSMOn;
extern uint8_t FlagConsumoNFCOn;
extern uint8_t FlagConsumoMEMSOn;
extern uint8_t FlagConsumoWifiOn;


extern uint32_t Consumo_uA_h_tot;

#define FASE_STOP_MODE				                        0
#define FASE_MEMS							1
#define FASE_NFC							2
#define FASE_GPS							3
#define FASE_GSM							4
#define	FASE_WIFI					                5



#define CONSUMO_STOP_MODE_FW4				                0.0521		//mA
#define CONSUMO_MEMS_FW4					        3.25
#define CONSUMO_NFC_FW4							1.3
#define CONSUMO_GPS_FW4							87.0    
#define CONSUMO_GSM_FW4							30.0
#define	CONSUMO_UC_IDLE_FW4     					0.0//5.0


#define CONSUMO_STOP_MODE				                0.018		//mA
#define CONSUMO_MEMS						        4.74
#define CONSUMO_NFC							4.66
#define CONSUMO_GPS							110    
#define CONSUMO_GSM							75
#define	CONSUMO_WIFI					                105


#define NUMERO_FASI							6

#define CAPACITA_BATTERIA_CRAAU				                2600	//mA*h        
#define CAPACITA_BATTERIA_CRAGZ_4_CELLE			                9600	//mA*h  //-10% rispetto al nominale
#define CAPACITA_BATTERIA_CRAGZ_2_CELLE                                 4800    //mA*h  //-10% rispetto al nominale

#define SECONDI_ORA							3600

uint32_t GetConsumo(void);
uint32_t GetConsumoFG(void);
void InitConsumiFasi(void);
void IncrementoTempiConsumi(uint32_t stop_mode_time);
void ResettaConsumo(void);
uint32_t CalcolaCaricaResidua(uint32_t consumo);
