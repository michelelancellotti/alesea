#ifndef __FUNZIONI_H
#define __FUNZIONI_H



#define TIMEOUT_ATTIVAZIONE_READY	20//10
#define TIMEOUT_ATTIVAZIONE_PRECOLLAUDO	5


#define MAX_FW_UPGRADE_ATTEMPTS         5               //Numero MAX tentativi consecutivi di aggiornamento FW


#define SWITCH_OFF	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, GPIO_PIN_SET);

extern uint32_t CRC_calcolato, CRC_letto;
extern uint32_t Supervisor;
extern uint8_t ContatoreDiRisvegliEnable;
extern uint8_t ContatoreAttesaCollaudo;
extern uint32_t StopModeDuration;
extern uint32_t last_ut;
extern uint8_t LSM6DSLFlagReinit;
extern uint8_t LSM6DSLCounterReinit;
extern uint16_t CountI2C1Reinit, CountI2C3Reinit;

void AttivaAlesea(void);
void ResettaDiagnostica(void);
void ResettaDiagnosticaSupervisore(void);
void ResetParametriFabbrica(void);
void ResettaConsumo(void);
void ResettaContagiri(void);
void ResettaContaShock(void);
void ResettaContaFreeFall(void);
void Spegnimento(void);
void EnterStopMode(void);
void DisattivaAlesea(void);
void GestioneStopMode(void);
void GestioneTimeoutAttivazione(void);
void GestioneDiagnosticaSupervisore(void);
void IncrementoIndiciTx(void);
void SetStatoAlesea(/*StatoAlesea_t*/uint32_t);
void SetSchedulingInterval(uint32_t);
void SetInterval_START_UP(uint32_t val);
void SetInterval_WARM_UP(uint32_t val);
void SetInterval_ACTIVE(uint32_t);
void SetNumberTx_START_UP(uint32_t);
void SetNumberTx_WARM_UP(uint32_t);
void SetSogliaMovement(uint8_t val);
void SetMoveSchInterval(uint32_t val);
void SetSpinSchInterval(uint32_t val);
void SetShockSchInterval(uint32_t val);
void SetSogliaShock(uint8_t val);
void SetVariabiliCriticheDefault(void);
void SetTimeoutSpin(uint8_t val);
void MonitorActivity(void);
void RefreshSchedulingInterval(void);
uint32_t EstraiNumeroDaStringa(uint8_t *buffer, uint32_t i, uint8_t cifre);
void GestioneSupervisore(void);
void SetTxInterruptSource(void);
void SetGnssSatelliti(uint32_t val);
void AggiornaModemType(void);
void RunFactoryReset(void);
void RunMicroReset(void);
void RunSpinReset(void);
void RunShockReset(void);
void RunFreeFallReset(void);
void RunDiagnosticaReset(void);
void RunBatteryReset(void);
void RunDeactivation(void);
void SetGpsSetup(uint32_t var_32);
void SetGpsPro(uint8_t var_8);
void SetGpsProTime(uint8_t var_8);
void SetProfiloTemperatura(uint8_t var_8);
void SetRotazioniOrizzontali(uint8_t var_8);
void SetGpsFreqForcing(uint8_t var_8);
void SetUpgradeFw(uint8_t version, uint8_t subversion, uint8_t type);
void SetWiFiScanEnable(uint8_t var_8);
void SetWiFiComEnable(uint8_t var_8);
void SetNumberTxSwitchModem(uint8_t val);
void SetGpsMode(uint8_t var_8);
void SetNumberTxIot(uint8_t val);
void RefreshWD(void);
void GestioneLed();
void ResetCalendar(void);
void GetStopModeDuration(void);




#endif

