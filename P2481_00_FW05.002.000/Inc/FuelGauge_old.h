
#include "mTypes.h"

#define GET_VOLTAGE 0x08
#define GET_MEAN_CURRENT_LSB 0x14
#define GET_MEAN_CURRENT_MSB 0x15
#define GET_INSTANT_CURRENT 0x0C
#define GET_MEAN_POWER 0x22
#define ALT_MANUFACTURER_ACCESS 0X3E
#define MANUFACTURER_ACCESS     0x00

#define MAC_DATA                0x40
#define MAC_DATA_LENGHT         0x61

#define CALIBRATION_MODE        0x002D
#define MANUFACTURING_STATUS    0x0057
#define FIRMWARE_VERSION        0x0002
#define HARDWARE_VERSION        0x0003
#define MANUFACTURER_NAME       0x004C
#define DEVICE_TYPE             0x0001
#define DF_SIGN                 0x0008
#define DEVICE_NAME             0x004A
#define OPERATION_STATUS        0x0054
#define SET_DEEP_SLEEP          0x0044
#define CLEAR_DEEP_SLEEP        0x0045

#define FG_ASSENTE      0
#define FG_PRESENTE     1

extern int32_t FG_CorrenteMedia;
extern int32_t FG_CorrenteSample;
extern uint8_t FuelGauge;

//void TestFuelGauge(void);
//uint32_t FG_getVoltage(void);
int32_t FG_getStandardCommands(uint16_t add);
int32_t FG_SetManufactoringParam(uint16_t add);
//int32_t FG_GetManufacturingStatus(void);
//int32_t FG_CalibrationEnable(void);
//int32_t FG_GetManufactoringParam(uint16_t);
//uint32_t FG_getMacData(void);
//uint32_t FG_getMacDataLenght(void);
void GestioneFuelGauge(void);
void InitFuelGaugeInstant(void);