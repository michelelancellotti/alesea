/**
******************************************************************************************
* File          	: wifi_SM.h
* Descrizione        	: Gestione macchina a stati WiFi (livello applicativo).
******************************************************************************************
*
* COPYRIGHT(c) 2024 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************************
*/

#ifndef _WIFI_SM_H
#define _WIFI_SM_H


/* Includes */
#include <stdint.h>
#include "wifi.h"


/* Define & Typedef */
#define WIFI_POWER_ON_WAIT_TIME         5000    //[ms], tempo attesa dopo alimentazione modulo

#define WIFI_SM_IN_PROGRESS             0       //esecuzione automa in corso
#define WIFI_SM_ENDED                   1       //esecuzione automa conclusa
#define WIFI_SM_ENDED_ERROR             2       //esecuzione automa conclusa: errore imprevisto o errore di rete
#define WIFI_SM_ENDED_NO_COMM           3       //esecuzione automa conclusa: comunicazione WiFi non eseguita 


typedef struct {
  uint8_t Executed;
  uint8_t NumAccessPoint;  
  uint16_t StringDim;  
  uint8_t String[WIFI_FULL_AP_STRING_SIZE];
} AccessPointScan_t;

typedef struct {
  uint8_t Available;
  uint16_t StringDim;
  uint8_t String[WIFI_MAC_ADDRESS_SIZE];
} MAC_Address_t;


typedef enum {
  WIFI_OFF_S                    = 0,
  WIFI_ON_S,                    //1
  WIFI_WAIT_ON_S,               //2
  WIFI_RESET_S,                 //3
  WIFI_SCAN_S,                  //4
  WIFI_PARSE_S,                 //5
  WIFI_CONNETTI_RETE_S,         //6
  WIFI_INVIA_TCP_S,             //7
  WIFI_DISCONNETTI_RETE_S,      //8    
  WIFI_STATE_NUM
} WiFi_State_t;


/* Global variables */
extern AccessPointScan_t AccessPointScan;
extern MAC_Address_t MAC_Address;
extern uint8_t NumberTxWiFi;


/* Global functions */
extern WiFi_State_t Get_Stato_WiFi_Appli(void);
extern void Start_MacchinaStati_WiFi(void);
extern uint8_t MacchinaStati_WiFi(void);


#endif	/* End _WIFI_SM_H */
