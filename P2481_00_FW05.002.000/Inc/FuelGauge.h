
#include "mTypes.h"


#ifndef __FG_H
#define __FG_H
#ifdef __cplusplus
 extern "C" {
#endif
#include "stm32l0xx_hal.h"
#include <stdbool.h>

#define HAL_I2C_INSTANCE                    hi2c3
#define HAL_FG_TIMEOUT                 5
#define FG_DELAY                       1

#define FG_I2C_ADDRESS                 0xAA

#define FG_CONTROL_LOW                 0x00
#define FG_CONTROL_HIGH                0x01
#define FG_TEMP_LOW                    0x06
#define FG_TEMP_HIGH                   0x07
#define FG_VOLTAGE_LOW                 0x08
#define FG_VOLTAGE_HIGH                0x09
#define FG_AVG_CURRENT_LOW             0x14
#define FG_AVG_CURRENT_HIGH            0x15
#define FG_AVG_POWER_LOW               0x22
#define FG_AVG_POWER_HIGH              0x23
#define FG_INT_TEMP_LOW                0x28
#define FG_INT_TEMP_HIGH               0x29
#define FG_STATE_OF_HEALTH_LOW         0x2E
#define FG_STATE_OF_HEALTH_HIGH        0x2F
#define FG_DATA_CLASS                  0x3E
#define FG_DATA_BLOCK                  0x3F
#define FG_BLOCK_DATA_START            0x40
#define FG_BLOCK_DATA_END              0x5F
#define FG_BLOCK_DATA_CHECKSUM         0x60
#define FG_BLOCK_DATA_CONTROL          0x61
#define FG_CONTROL_STATUS              0x0000
#define FG_CONTROL_DEVICE_TYPE         0x0001
#define FG_CONTROL_FW_VERSION          0x0002
#define FG_CONTROL_PREV_MACWRITE       0x0007
#define FG_CONTROL_CHEM_ID             0x0006
#define FG_CONTROL_SET_HIBERNATE       0x0044
#define FG_CONTROL_CLEAR_HIBERNATE     0x0045


   
   
#define GET_MEAN_CURRENT_LSB 0x14
#define GET_MEAN_CURRENT_MSB 0x15

#define FG_ASSENTE      0
#define FG_PRESENTE     1

extern int32_t FG_CorrenteMedia,FG_CorrenteMedia_lib;
extern int32_t FG_CorrenteSample;
extern uint8_t FuelGauge;
typedef struct
{
    uint16_t    voltage_mV;
    int16_t     current_mA;
    double      temp_degC;
    uint16_t    soc_percent;
    uint16_t    soh_percent;
    uint16_t    designCapacity_mAh;
    uint16_t    remainingCapacity_mAh;
    uint16_t    fullChargeCapacity_mAh;

    bool        isCritical;
    bool        isLow;
    bool        isFull;
    bool        isCharging;
    bool        isDischarging;
} FG_info_t;
   
extern FG_info_t FG_info;

bool FG_init( uint16_t designCapacity_mAh, uint16_t terminateVoltage_mV, uint16_t taperCurrent_mA );
bool FG_update( FG_info_t *battery );
bool FG_readDeviceType( uint16_t *deviceType );
bool FG_readDeviceFWver( uint16_t *deviceFWver );
bool FG_readDesignCapacity_mAh( uint16_t *capacity_mAh );

bool FG_readVoltage_mV( uint16_t *voltage_mV );
bool FG_readTemp_degK( uint16_t *temp_degKbyTen );
bool FG_readAvgCurrent_mA( int16_t *avgCurrent_mA );
bool FG_readStateofCharge_percent( uint16_t *soc_percent );

bool FG_readControlReg( uint16_t *control );
bool FG_readFlagsReg( uint16_t *flags );
bool FG_readopConfig( uint16_t *opConfig );
bool FG_readRemainingCapacity_mAh( uint16_t *capacity_mAh );
bool FG_readFullChargeCapacity_mAh( uint16_t *capacity_mAh );
bool FG_readStateofHealth_percent( uint16_t *soh_percent );

bool FG_i2c_command_write( uint8_t command, uint16_t data );
bool FG_i2c_command_read( uint8_t command, uint16_t *data );

bool FG_i2c_control_write( uint16_t subcommand );
bool FG_i2c_control_read( uint16_t subcommand, uint16_t *data );
bool FG_i2c_write_data_block( uint8_t offset, uint8_t *data, uint8_t bytes );
bool FG_i2c_read_data_block( uint8_t offset, uint8_t *data, uint8_t bytes );

char *FG_execute_fs(char *pFS);




void InitFuelGaugeInstant(void);
int32_t FG_getStandardCommands(uint16_t add);
void FG_SetManufactoringParam(uint16_t add);
void FG_DataFlashAccess(void);
void FG_GetManufactoringParam(uint16_t);
void FG_GetAltManufactoringParam(uint16_t add);
uint32_t FG_getMacData(void);
void GestioneFuelGauge(void);

void FG_SetAltManufactoringParam(uint16_t add);
#ifdef __cplusplus
}
#endif
#endif /* __FG_H */
