#include "LSM6DSL_driver.h"
#include "mTypes.h"

#define TX_BUF_DIM              1000
#define TEMPO_GIROSCOPIO_ON	500				//500 = 5 secondi
#define FILTRO_QUADRANTE_FERMO_DEFAULT  150                             //secondi a quadrante //60 -> 150 in data 12/11/2021 per misurae vel < 0.1 rpm  (150 x 4 = 600 secondi per 1 giro)    FW 4.18
#define FILTRO_ROTAZ_ORIZZ_FERMO_DEFAULT  180                           //180 secondi a vel < 1 rpm   
#define FILTRO_QUADRANTE_FERMO_MIN  30                                  //0.5 rpm
#define FILTRO_QUADRANTE_FERMO_MAX  200
#define SOGLIA_ANGOLO_MAX	900
#define SOGLIA_ANGOLO_MIN	100


#define SOGLIA_0_18g     3                      //0.18g     
#define SOGLIA_0_5g      8                      
#define SOGLIA_1g	16      		//0.15g //FS*N/64
#define SOGLIA_1_5g     24
#define SOGLIA_2g       32
#define SOGLIA_2_5g     40
#define SOGLIA_3g       48
#define SOGLIA_3_5g     56
#define SOGLIA_4g       64            

#define FALL_DUR_STOP_MODE     1
#define FALL_DUR_RUN_MODE     30

#define FILTRO_FAILURE_I2C              20

#define ORIENTAMENTO_DUMMY      ((float)9999.9)

#define FINESTRA_OSSERVAZIONE_CAMBIO_SEGNO      500
#define DERIVATA_CAMBIO_SEGNO_SOGLIA        0.05


#define ACC_OFF                 0
#define ACC_LOW_POWER           1
#define ACC_ACTIVE              2
#define GYR_OFF                 3
#define GYR_ON                  4
#define ACC_FS_4g               5
#define GYR_FS_2000dps          6
#define ACC_INTERRUPT_ANGLE     7
#define ACC_SOGLIA              8
#define BDU                     9
#define PULL_UP                 10
#define ACC_FILTER              11
#define INT_1                   12
#define INT_2                   13
  


extern lsm6dsl_odr_xl_t x_data_rate;
extern lsm6dsl_odr_g_t y_data_rate;
extern lsm6dsl_fs_xl_t x_full_scale;
extern lsm6dsl_fs_g_t y_full_scale;
extern uint8_t SogliaAccelerometro;
extern lsm6dsl_g_hm_mode_t y_power_mode;
extern lsm6dsl_xl_hm_mode_t x_power_mode;


extern uint32_t LSM6DSL_interrupt_timer;
extern float NumeroGiriGyr;
extern float acceleration_mg[3];
extern float angular_rate_mdps[3];
extern float acceleration_mg_old[3];
extern float angular_rate_mdps_old[3];
extern uint8_t flag_lsm6dsl_ignore_int_1, flag_lsm6dsl_ignore_int_2;
extern uint32_t lsm6dsl_angular_fail_count;
extern uint32_t lsm6dsl_acceleration_fail_count;
extern uint32_t lsm6dsl_angular_busy_count;
extern uint32_t lsm6dsl_acceleration_busy_count;
extern uint32_t lsm6dsl_orientation_fail_count;
extern uint32_t lsm6dsl_orientation_busy_count;
extern uint32_t lsm6dsl_temperature_fail_count;
extern uint16_t lsm6dsl_fail_count_dbg;
extern uint32_t Conta_inversione_segno;
extern uint16_t CounterAngularRateBlocked;
extern uint16_t CounterAccelerationBlocked;
//extern uint8_t CountReinitSetReg[32];
extern uint16_t CountRegisterWRFailure;
extern uint16_t RegisterWRFailureMap;

void InitLSM6DSL(void);
float LSM6DSL_cycle(void);
float LSM6DSL_Temperatura(void);
void LSM6DSL_cycle_acc(void);
void LSM6DSL_cycle_gyr(void);
void LSM6DSL_GYRO_OFF(void);
void LSM6DSL_GYRO_ON(void);
void LSM6DSL_ACC_ON(void);
void SetInterruptAngolo(void);
void LSM6DSL_Gyro_Start(void);
void TestTilt(void);
void ResettaNumeroGiri(void);
float LSM6DSL_Orientamento(void);
void SetInterruptShock(void);
void InitLSM6DSL_test(void);
void SetAccFreq(lsm6dsl_odr_xl_t);
void SogliaAccelerometroSet(uint8_t val);
void SetInterruptFreeFall(uint8_t val);
uint8_t FreeFallSourceRead(void);
uint8_t WakeUpSourceRead(void);
void LSM6DSL_ACC_OFF(void);
void SetInterrupt1(void);
void SetInterrupt2(void);
void CleanFlagInterrupt(void);
void LS6DSL_CaricaSetup(void);
float My_LSM6DSL_FROM_FS_TO_mg(lsm6dsl_fs_xl_t scala, short val);
void SetAccFS(lsm6dsl_fs_xl_t fs);
void SetGyrFS(lsm6dsl_fs_g_t fs);
void SetGyrFilter(lsm6dsl_lpf1_sel_g_t val);
void SetAccFilter(lsm6dsl_bw0_xl_t val);
void SetAccPULL_UP(void);
void SetAccBDU(void);
uint8_t CheckI2CReinit(uint8_t tipo);
void SetLSM6DSL_ciclo(uint8_t tipo);
void SetLSM6DSL(uint8_t tipo);

//extern void InitBufferShocks(void);


