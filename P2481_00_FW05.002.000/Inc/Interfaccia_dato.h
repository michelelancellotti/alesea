/**
************************************************************************************************
* File          	: Interfaccia_dato.h
* Descrizione        	: Interfaccia dati tra livello applicativo e libreria Azure.
************************************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
************************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __INTERFACCIA_DATO_H
#define __INTERFACCIA_DATO_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"

  
/* Defines */

    
/* Types & global variables */
typedef enum {
  SP_ID_FW_UPDATE                       = 0,
  SP_ID_GNSS_TIMEOUT,                   //1
  SP_ID_MOVEMENT_THRESHOLD,             //2
  SP_ID_SHOCK_THRESHOLD,                //3
  SP_ID_GNSS_CONSTELLATION,             //4
  SP_ID_SPIN_FILTER,                    //5
  SP_ID_GNSS_PRO_SETTINGS,              //6
  SP_ID_FORCED_GNSS,                    //7
  SP_ID_TEMPERATURE_PROFILE_ENB,        //8
  SP_ID_STARTUP_SETTINGS,               //9
  SP_ID_WARMUP_SETTINGS,                //10
  SP_ID_SCHEDULING_INTERVAL,            //11
  SP_ID_HORIZONTAL_SPIN_ENB,            //12
  SP_ID_END_OF_MOVEMENT_INTERVAL,       //13
  SP_ID_WIFI_SCAN_ENB,                  //14
  SP_ID_GPS_MODE,                       //15
  SP_ID_SPIN_SCHEDULING_INTERVAL,       //16
  SP_ID_SHOCK_SCHEDULING_INTERVAL,      //17
  SP_ID_NUMBER_TX_IOT_CENTRAL,          //18
  SP_ID_WIFI_COM_ENB,                   //19
  SP_ID_NUMBER_TX_SWITCH_MODEM,         //20  
  SP_ID_FACTORY_RESET_TRG,              //21
  SP_ID_SPIN_RESET_TRG,                 //22
  SP_ID_DEACTIVATION_TRG,               //23
  SP_ID_BATTERY_RESET_TRG,              //24
  SP_ID_MICRO_RESET_TRG,                //25
  SP_ID_SHOCK_RESET_TRG,                //26
  SP_ID_DIAGNOSTIC_RESET_TRG,           //27
  SP_ID_FREE_FALL_RESET_TRG,            //28
  SP_ID_FW_UPDATE_UNLOCK_TRG,           //29
  SP_ID_NUM
} SetpointsId_t;
  
/* Global functions */
extern void InitInterfacciaDato(void);
  
/* Global variables */

  
#ifdef __cplusplus
}
#endif
#endif /* __INTERFACCIA_DATO_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
