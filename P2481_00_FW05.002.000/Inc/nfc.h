#include "mTypes.h"

#ifndef _NFC_H
#define _NFC_H

#define DIMENSIONE_BLOCCO_NFC 16					//dimensione in byte del blocco
#define DIMENSIONE_BUFFER_LETTURA_NFC 176 //spazio per 10 blocchi + 1
#define NUMERO_BLOCCHI_LETTI	10


#define DIMENSIONE_BLOCCO_SCRITTURA_NFC 	DIMENSIONE_BLOCCO_NFC + 1
#define DIMENSIONE_NFC_DATA	12
#define BLOCCO_DESTINAZIONE_ID	0x00
#define BLOCCO_DESTINAZIONE_DATI_RX		0x01 //0x03
#define BLOCCO_DESTINAZIONE_PREAMBOLO	0x01	//indirizzo del blocco riservato a scrittura
#define BLOCCO_REGISTRI							0xFE	//indirizzo registri di configurazione
#define POSIZIONE_PRESENZA_CAMPO		0x06	//posizione all'interno del blocco 0xFE
#define NFCAddressRR 								0xAB	//indirizzo i2c NFC + lettura registro
#define NFCAddressWR								0xAA	//indirizzo i2c NFC + scrittura registro

#define TEMPO_ASSENZA_CAMPO					2			//2 sec
#define TEMPO_PRESENZA_CAMPO				1			//1 sec

extern uint8_t 		BufferLetturaNFC[];

extern uint8_t 	CodiceApp[6];

void ScriviICCID(uint8_t dato[]);
//void MX_I2C1_Init(void);
void Error_Handler(void);
uint8_t LeggiPresenzaCampo(void);
void ScriviDatoNFC(uint8_t bufferTx[], uint8_t destinazione);
void ScriviByteNFC(uint8_t blocco, uint8_t posizione, uint8_t* dato_wr);
void LeggiBloccoNFC(uint8_t blocco, uint8_t* bufferRX);
uint8_t LetturaCodiceNFC(void);
void PulisciNFC(void);
void ScriviICCID_READY(void);
void ScriviICCID_ACTIVE(void);
void ScriviICCID_DEBUG(void);
void ScriviICCID_PRECOLLAUDO(void);
void PulisciBufferLetturaNFC(void);

#endif
