/**
************************************************************************************************
* @file Versioni_HW.h
* @brief Definizione delle codifiche relative alle versioni HW supportate.
*
* @copyright 2024 "Computec srl"
*
************************************************************************************************
*/


#ifndef _VERSIONI_HW_H
#define _VERSIONI_HW_H


/* Defines */
/**
**************************************************
* @defgroup HW_3_X HW 3.x
* @{
**************************************************
*/
/**
* Revisione HW legacy (3.x).
*/
#define VERSIONE_HW_3_X                                 3
/**
* Sotto-revisione HW base (3.x), con modem BG96.
*/
#define SOTTO_VERSIONE_HW_3_X_BASE		        0
/**
* Sotto-revisione HW (3.x) con modem BG95.
*/
#define SOTTO_VERSIONE_HW_3_X_BG95		        1
/** @} */       //end of HW_3_X


/**
**************************************************
* @defgroup HW_4_X HW 4.x
* @{
**************************************************
*/
/**
* Revisione HW per ottimizzazione performance (4.x).
* Principali variazioni rispetto alla @ref HW_3_X versione HW legacy:
* - antenne GNSS e cellulare IGNION
* - modem BG95
* - integrazione di modulo WiFi per localizzazione indoor
* - integrazione di magnetometro per rilevare rotazioni orizzontali
* - rimozione del circuito di spegnimento del dispositivo
* - aggiornamento switching
* - integrazione di power-switch per disalimentare il modem
*/
#define VERSIONE_HW_4_X                                 4
/**
* Sotto-revisione HW base (4.x).
*/
#define SOTTO_VERSIONE_HW_4_X_BASE		        0
/**
* Sotto-revisione HW (4.x) con batteria Panasonic a 2 celle (anzichè 4 celle).
*/
#define SOTTO_VERSIONE_HW_4_X_BATTERIA_2_CELLE	        1
/**
* Sotto-revisione HW (4.x) con eSIM Cobira (anzichè 1nce).
*/
#define SOTTO_VERSIONE_HW_4_X_COBIRA                    2
/** @} */       //end of HW_4_X


#endif
