/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usart_H
#define __usart_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

/* USER CODE BEGIN Private defines */

#define BRX_LEN         8
#define BTX_LEN         1152	 


typedef enum {
  SERIALE_ND            = 0x00,
  SERIALE_MODEM,
  SERIALE_WIFI,
} UART_t;

typedef struct
{
  uint8_t rx_buffer[BRX_LEN];
  uint8_t tx_buffer[BTX_LEN];

  /*volatile*/ uint16_t rx_st;          //indice di lettura del buffer di ricezione per rx
  /*volatile*/ uint16_t rx_end;         //indice finale del buffer di ricezione per rx
  uint16_t rx_cnt;
  /*volatile*/ uint8_t rx_avail;
  /*volatile*/ uint16_t tx_st;
  /*volatile*/ uint16_t tx_end;
  /*volatile*/ uint16_t tx_cnt;
  /*volatile*/ uint8_t tx_avail;
} SerialBuffer;

typedef struct {
  uint16_t err_count;
  uint32_t err_code;
  uint16_t hal_err_count_RX;
  uint16_t hal_err_count_ERR;
  uint16_t hal_err_count_TX;
  uint16_t ORE_error;
} SerialDbg_t;

typedef struct {
  SerialBuffer buffer;
  SerialDbg_t debug;
} UART_Mng_t;


/* USER CODE END Private defines */

void MX_USART1_UART_Init(void);
void MX_USART2_UART_Init(void);

/* USER CODE BEGIN Prototypes */

extern void Init_SCI(uint8_t id_seriale);
extern void Write_SCI(uint8_t id_seriale, uint8_t *str, uint16_t cnt);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
