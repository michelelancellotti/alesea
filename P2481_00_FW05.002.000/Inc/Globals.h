#ifndef _GLOBALS_H
#define _GLOBALS_H

#include <string.h>
#include <stdint.h>
#include "Versioni_HW.h"


#define CHIAVE_COLLAUDO	                0xBA
#define CHIAVE_VARIABILE_CRITICA        0xBB
#define CHIAVE_FW                       0xCC


//define di test
////////////////////////////////////////////////////////////////////////////////////////////
/*
#define GPS_ON                          //TODO: abilitare in RELEASE: abilita acquisizione GPS
//#define DEBUG_ON                        //TODO: disabilitare in RELEASE: disabilita watchdog e ingresso in STOP mode
//#define FREE_FALL_ON
//#define ENB_COMMUNICATION_DEBUG         //TODO: disabilitare in RELEASE: bypass macchina a stati comunicazione (modem+WiFi) liv. applicativo

#define PERIODO_COLLAUDO		30	//secondi
#define PERIODO_START_UP	        10//300	//secondi
#define PERIODO_WARM_UP 	        21600	//secondi
#define PERIODO_ACTIVE			3600//172800//86400   //3600*24	//secondi

#define NUMERO_TX_COLLAUDO_AZURE        2
#define NUMERO_TX_COLLAUDO_OLD_BROKER   2
#define NUMERO_TX_START_UP      	1//5 
#define NUMERO_TX_START_UP_MIN          1
#define NUMERO_TX_START_UP_MAX          100
#define NUMERO_TX_WARM_UP	        1
#define NUMERO_TX_WARM_UP_MIN           1
#define NUMERO_TX_WARM_UP_MAX           100        
#define MAX_RETRY_SUB	                1




#define SPIN_SCH_INTERVAL_SECOND        3600

#define MOVE_SCH_INTERVAL_DEFAULT       120
#define MOVE_SCH_INTERVAL_MIN           10
#define MOVE_SCH_INTERVAL_MAX           120

#define SPIN_SCH_INTERVAL_DEFAULT       120//7200
#define SPIN_SCH_INTERVAL_MIN           20//3600
#define SPIN_SCH_INTERVAL_MAX           120

#define SHOCK_SCH_INTERVAL_DEFAULT       120
#define SHOCK_SCH_INTERVAL_MIN           10
#define SHOCK_SCH_INTERVAL_MAX           120


#define ATTESA_COLLAUDO                 20

#define TEMPERATURE_SCH_INTERVAL        3600 

#define TIMEOUT_SUPERVISOR	        1200	//secondi

#define VERSIONE_FW			6
#define SOTTO_VERSIONE_FW		24

#define SOGLIA_MOV_DEFAULT              SOGLIA_0_18g  
#define SOGLIA_SHOCK_DEFAULT            SOGLIA_3_5g   
#define SOGLIA_MOV_MAX                  SOGLIA_2g
#define SOGLIA_MOV_MIN                  SOGLIA_0_18g         
#define SOGLIA_SHOCK_MAX                SOGLIA_4g
#define SOGLIA_SHOCK_MIN                SOGLIA_2g
*/
////////////////////////////////////////////////////////////////////////////////





//define di produzione
//////////////////////////////////////////////////////////////////////////////////////

#define GPS_ON                          //TODO: abilitare in RELEASE: abilita acquisizione GPS                
//#define DEBUG_ON                        //TODO: disabilitare in RELEASE: disabilita watchdog e ingresso in STOP mode
//#define FREE_FALL_ON
//#define ENB_COMMUNICATION_DEBUG         //TODO: disabilitare in RELEASE: bypass macchina a stati comunicazione (modem+WiFi) liv. applicativo

#define PERIODO_COLLAUDO		30	//secondi
#define PERIODO_START_UP	        300	//secondi
#define PERIODO_WARM_UP 	        21600	//secondi
#define PERIODO_ACTIVE			172800//86400   //3600*24	//secondi


#define NUMERO_TX_COLLAUDO_AZURE        2
#define NUMERO_TX_COLLAUDO_OLD_BROKER   2
#define NUMERO_TX_START_UP      	5 
#define NUMERO_TX_START_UP_MIN          1
#define NUMERO_TX_START_UP_MAX          100
#define NUMERO_TX_WARM_UP	        1
#define NUMERO_TX_WARM_UP_MIN           1
#define NUMERO_TX_WARM_UP_MAX           100        
#define MAX_RETRY_SUB	                1

#define SPIN_SCH_INTERVAL_SECOND        3600

#define MOVE_SCH_INTERVAL_DEFAULT       43200
#define MOVE_SCH_INTERVAL_MIN           600
#define MOVE_SCH_INTERVAL_MAX           43200

#define SPIN_SCH_INTERVAL_DEFAULT       7200
#define SPIN_SCH_INTERVAL_MIN           600
#define SPIN_SCH_INTERVAL_MAX           43200

#define SHOCK_SCH_INTERVAL_DEFAULT       7200
#define SHOCK_SCH_INTERVAL_MIN           600
#define SHOCK_SCH_INTERVAL_MAX           43200

#define TEMPERATURE_SCH_INTERVAL        3600

#define ATTESA_COLLAUDO                 5

#define TIMEOUT_SUPERVISOR	        1200	//secondi

#define VERSIONE_FW			6
#define SOTTO_VERSIONE_FW		24

#define SOGLIA_MOV_DEFAULT              SOGLIA_0_18g  
#define SOGLIA_SHOCK_DEFAULT            SOGLIA_3_5g   
#define SOGLIA_MOV_MAX                  SOGLIA_2g
#define SOGLIA_MOV_MIN                  SOGLIA_0_18g         
#define SOGLIA_SHOCK_MAX                SOGLIA_4g
#define SOGLIA_SHOCK_MIN                SOGLIA_2g

/////////////////////////////////////////////////////////////////////







#define PROFILO_TEMPERATURA_ON          0xAA
#define PROFILO_TEMPERATURA_OFF         0x55

#define ROTAZIONE_ORIZZ_OFF             0x00
#define ROTAZIONE_ORIZZ_ON              0x01

#define WIFI_ENABLE_OFF                 0x02
#define WIFI_ENABLE_ON                  0x01

#define HZ_RTC	                        10

extern uint8_t WwdgCounter;


extern uint8_t FlagAttivazione;
extern uint8_t FlagDisattivazione;
extern uint8_t FlagFactoryReset;
extern uint8_t FlagSpinReset;
extern uint8_t FlagShockReset;
extern uint8_t FlagFreeFallReset;
extern uint8_t FlagBatteryReset;
extern uint8_t FlagDiagnosticaReset;
extern uint8_t FlagUpgradeFw;
extern uint8_t FlagInstantMessage;
extern uint8_t FlagSincronizzazione;
extern uint8_t FlagMicroReset;
extern uint8_t FlagNoRxMQTT;	//Flag per il bypass della fase di ricezione MQTT: usato quando viene dato cmd da app e viene forzato risveglio RTC
extern uint8_t FlagForceGpsOn;
extern uint8_t FlagSetSetGnssSatelliti;
extern uint32_t SchIntervalCounter;
extern uint32_t TempoDiRisveglioDef;

extern uint8_t CounterTxStartUp, CounterTxWarmUp, CounterTxCollaudo;
////////////////////////////////////////////////////////////////////////
/////////////////////////SET PROSSIMO RISVEGLIO/////////////////////////
////////////////////////////////////////////////////////////////////////
void ImpostaRisveglio(uint32_t period);

#endif
