#ifndef __GESTIONE_DATO_H
#define __GESTIONE_DATO_H

#include <string.h>
#include <stdint.h>
#include "LSM6DSL.h"
#include "Globals.h"
#include "modem.h"


#define LIMITE_SCHEDULING_INTERVAL	        31536000	//1 anno espresso in secondi
#define	JSON_MAX_BUFFER_DATA_TX	                MOD_MAX_BUFFER_DATA_TX
#define JSON_MAX_BUFFER_DATA_RX	                64
  
#define GPS_TIMEOUT_MAX			        1200		//secondi
#define GPS_TIMEOUT_MIN	                        5		//secondi
#define GPS_TIMEOUT_DEFAULT	                180		//secondi
  
#define GPS_PRO_STATE_ON                        1
#define GPS_PRO_STATE_OFF                       0
#define GPS_PRO_STATE_DEFAULT                   GPS_PRO_STATE_OFF
  
#define GPS_SATELLITI_DEFAULT                   1               //GPS_GLONASS
  
#define GPS_PRO_TIME_DEFAULT                    5
#define GPS_PRO_TIME_MIN                        1
#define GPS_PRO_TIME_MAX                        20
  
#define GPS_FREQ_FORCING_DEFAULT                20    
#define GPS_FREQ_FORCING_MIN                    1
#define GPS_FREQ_FORCING_MAX                    99
  
#define ALESEA_UND                              0        
#define ALESEA_ACTIVE			        1
#define ALESEA_READY			        2
#define ALESEA_PRE_COLLAUDO	                3
#define ALESEA_COLLAUDO			        4
#define ALESEA_START_UP                         5
#define ALESEA_WARM_UP                          6 
  
#define GPS_MODE_ON                             1
#define GPS_MODE_OFF                            2
#define GPS_MODE_OFF_TILL_MOVE                  3
#define GPS_MODE_DEFAULT                        GPS_MODE_ON
  
#define NUMBER_TX_IOT_DEFAULT                   10
#define NUMBER_TX_IOT_AZURE                     100
#define NUMBER_TX_IOT_OLD_BROKER                99
#define NUMBER_TX_IOT_MAX                       NUMBER_TX_IOT_AZURE     
#define NUMBER_TX_IOT_MIN                       1

#define WIFI_NUM_TX_SWITCH_MODEM_DEFAULT        10
#define WIFI_NUM_TX_SWITCH_MODEM_MAX            100
#define WIFI_NUM_TX_SWITCH_MODEM_MIN            1



#define PESO_SCHEDULE   1       //(uint8_t)0b00000001      				
#define PESO_SPIN       2       //(uint8_t)0b00000010    				
#define PESO_MOVE       4       //(uint8_t)0b00000100   				
#define PESO_SHOCK      8       //(uint8_t)0b00001000
#define PESO_SPIN_ERROR 16      //(uint8_t)0b00010000




typedef struct
{
  uint8_t       DatoPresente;
  uint16_t	MMC;			
  uint16_t	MNC;				
  uint32_t	CID;			
  uint16_t	LAC_TAC;
  int16_t       Rssi;
  uint8_t       TrasmissionChannel[8];
}StrutturaCella;

extern StrutturaCella CellaServente;
extern StrutturaCella CellaVicina1;
extern StrutturaCella CellaVicina2;
extern StrutturaCella CellaVicina3;

extern uint8_t ServerType, CounterTxIot, NumberTxIot;

typedef enum
{
  MODEM_SET_PERIODO_ACTIVE			= '1',
  MODEM_FACTORY_RESET 				= '2',
  MODEM_SPIN_RESET				= '3',
  MODEM_DEACTIVATION				= '4',
  MODEM_BATTERY_RESET 				= '5',
  MODEM_UPGRADE_FW 				= '6',
  //MODEM_INSTANT_MESSAGE			= '7',
  MODEM_MICRO_RESET  				= '8',
  MODEM_SHOCK_RESET  				= '9',
  MODEM_GPS_SETUP		  		= 'A',
  MODEM_DIAGNOSTICA_RESET			= 'B',
  MODEM_SET_SOGLIA_MOV                          = 'C',
  MODEM_SET_SOGLIA_SHOCK                        = 'D',
  MODEM_SET_PERIODO_START_UP                    = 'E',
  MODEM_SET_PERIODO_WARM_UP                     = 'F',
  MODEM_SET_GNSS_SATELLITI                      = 'G',
  MODEM_FREE_FALL_RESET                         = 'H',
  MODEM_SET_TIMEOUT_SPIN                        = 'I', 
  MODEM_GPS_PRO_SETUP                           = 'J',
  MODEM_GPS_FREQ_FORCING                        = 'L',
  MODEM_PROFILO_TEMPERATURA_SETUP               = 'M',
  MODEM_ROTAZIONI_ORIZZ_ON                      = 'N', 
  MODEM_SET_PERIODO_MOVIMENTO                   = 'O',       
  MODEM_WIFI_SCAN_ENABLE                        = 'P',
  MODEM_GPS_MODE                                = 'Q',
  MODEM_SET_PERIODO_SPIN                        = 'R',
  MODEM_SET_PERIODO_SHOCK                       = 'S',
  MODEM_SET_NUMBER_TX_IOT                       = 'T',
  MODEM_WIFI_COM_ENABLE                         = 'U',
}MessageTypes;

typedef enum {
  PKT_SRC_MODEM         = 0,
  PKT_SRC_WIFI,         //1
}PktSource_t;

typedef enum {
  SRV_OLD_BROKER        = 0,    //disable Azure (modem only)
  SRV_IOT_CENTRAL       = 1,    //enable Azure (modem only)
  SRV_TCP_SOCKET        = 2,    //socket TCP (WiFi only)
}ServerType_t;


typedef struct
{
  uint8_t ICCID[20+1];	
  uint16_t SizeOfICCID;
}AleseaICCID_t; 

typedef struct
{
  uint32_t CaricaResidua;			
  int8_t Temperatura;	
  int8_t TemperaturaMax;		//tmax
  int8_t TemperaturaMin;		//tmin
  uint8_t ProfiloTemperaturaOn;          //pt
}AleseaMeasure_t;

typedef struct
{
  uint8_t DataOra[17+1];		
  uint16_t SizeOfDataOra;
  uint32_t UnixTime;
}AleseaDate_t; 

typedef struct
{
  uint8_t BootVersion[8+1];		
  uint8_t VersioneFwTx[5+1];		
  uint8_t VersioneHwTx[5+1];		
}SystemVersion_t; 

typedef struct
{
  uint16_t TempoConnessioneGps;
  uint16_t GpsRegTemp;
  uint32_t GpsTimeout;
  uint32_t GpsAssistito;
  uint32_t GnssSatelliti;
  uint8_t  GpsPro;
  uint8_t  GpsProTime;
  uint8_t  GpsFreqForcing;               
  uint8_t  GpsForcingCounter;   
  uint8_t  GpsMode;
}GNSS_data_t; 

typedef struct
{
  uint16_t MMC;
  uint16_t MNC;			
  uint32_t CID; 				
  uint16_t LAC; 
}NetworkCell_t; 

typedef struct
{
  uint32_t IndiceTrasmissione;
  uint8_t TrasmissionChannel[8];
  uint8_t RSSI;
  uint16_t TempoConnessione;
  uint16_t TempoModemOn;
  uint16_t TempoRegistrazione;
  uint16_t RegRete;  
  uint16_t RegAzure;
  uint8_t LastError;
  uint8_t Retrasmission;
  uint8_t InputVariation;
  uint8_t TxInterruptSource;
  uint8_t ErroreMqtt[2];
}NetworkPerformance_t; 

typedef struct
{
  uint16_t RegRete;
  uint8_t LastError;
  uint8_t RSSI;
}WiFiPerformance_t;

typedef struct
{
  float gps_latitudine;			
  float gps_longitudine;
  float gpsPro_latitudine;			
  float gpsPro_longitudine;  
}Coordinates_t;

typedef struct
{
  uint8_t Orientamento;		
}TiltEvent_t;

typedef struct
{
  uint8_t SpinSession;                  
  float NumeroGiriCw;		
  float NumeroGiriACw;		
  uint16_t SpinStartStopCounter;        
  float  SpinFreq; 
  uint8_t RotazioneOrizzontaleOn;
  uint32_t SpinSchInterval;
  uint32_t SpinMag;
}SpinEvent_t; 

typedef struct
{
  uint32_t ContaShock;			
  uint8_t ShockSession; 
  uint8_t SogliaShock;
  uint32_t ShockSchInterval;
}ShockEvent_t;

typedef struct
{		
  uint8_t MovementSession; 
  uint8_t SogliaMovimento;
  uint16_t FiltroMovimento;
  uint32_t MoveSchInterval;
}MovementEvent_t;

typedef struct
{		
  uint8_t WiFiScanEnable;
  uint8_t WiFiComEnable;
  uint8_t WiFiNumTxSwitchModem;
}WiFiParam_t;

typedef struct
{
  uint8_t ContaFreeFall;			
}FreeFallEvent_t;

typedef struct
{
  uint32_t SchedulingInterval;	       
  uint32_t SchInt_START_UP;		
  uint32_t SchInt_WARM_UP;		
  uint32_t SchInt_ACTIVE;		
  uint32_t NumberTx_START_UP;           
  uint32_t NumberTx_WARM_UP;            
}ActivationTiming_t;


typedef union {
  uint8_t BUFFER[4];
  struct {
    uint8_t subversion; //[0]
    uint8_t version;    //[1]
    uint8_t type;       //[2]
  }FIELD;
}UpgradeData_t;


typedef __packed struct 
{
  uint8_t Byte1;
  uint8_t Byte2;
  uint8_t Byte3;
  uint8_t Byte4;
  uint8_t Byte5;
  uint8_t Byte6;
  uint8_t Byte7;
  uint8_t Byte8;
  
  uint8_t ContaReset;				//cr
  uint32_t MicroActivityCounter;		//mc
  uint32_t ModemActivityCounter;		//mo
  uint8_t CounterErrorEeprom;			//re
  uint8_t ContaStatoIn;				//si
  uint8_t ContaStatoOut;			//so
  uint8_t CountErroreModemOn;			//mf
  uint8_t CountUnpError;			//cu
  uint32_t ContatoreRisvegli;                   //cw
}Diagnostica_t;


typedef struct
{
  NetworkPerformance_t NetworkPerformance;
  GNSS_data_t GNSS_data;
  SpinEvent_t SpinEvent;
  SystemVersion_t SystemVersion;
  ActivationTiming_t ActivationTiming;
  AleseaDate_t AleseaDate;
  AleseaMeasure_t AleseaMeasure;
  Coordinates_t Coordinates;
  NetworkCell_t NetworkCell;
  TiltEvent_t TiltEvent;
  ShockEvent_t ShockEvent;
  FreeFallEvent_t FreeFallEvent;
  MovementEvent_t MovementEvent;  
  uint32_t StatoAlesea;
  uint8_t TipoModem;
  WiFiPerformance_t WiFiPerformance;
}AleseaMessage_t;
//--> sizeof(AleseaMessage_t) = 236B @ 24/05/2024



typedef struct 
{
  uint32_t* var32;
  uint32_t varEepromAddr1;
  uint32_t varEepromAddr2;
  uint32_t crcEepromAddr1;
  uint32_t crcEepromAddr2;
  uint32_t defaultVal;
}VariabileCritica_t;


extern uint8_t* DatoJson;
extern uint16_t DatoJsonDim;
extern uint8_t DatoJsonRx[];
extern uint16_t DatoJsonRxDim;

extern uint8_t VersioneFW;
extern uint8_t SottoversioneFW;
extern uint8_t ModemVersioneFW[30];

extern UpgradeData_t UpgradeDataRX;
extern uint8_t UpgradeFwCount;

extern uint8_t VersioneHW;
extern uint8_t SottoversioneHW;

extern float TotGiriAcc, TotGiriGyr;


extern VariabileCritica_t StatoAleseaVc;
extern VariabileCritica_t SchIntStartUpVc;
extern VariabileCritica_t SchIntWarmUpVc;
extern VariabileCritica_t SchIntActiveVc;
extern VariabileCritica_t NumberTxStartUpVc; 
extern VariabileCritica_t NumberTxWarmUpVc;
extern VariabileCritica_t GpsTimeoutVc;
extern VariabileCritica_t GnssSatellitiVc;


extern AleseaMessage_t Dati;
extern AleseaMessage_t DatiLetti;
extern Diagnostica_t Diagnostica;

extern WiFiParam_t WiFiParam;
extern AleseaICCID_t AleseaICCID;



void SalvataggioDati(uint8_t Indirizzo);
void GeneraStringa(AleseaMessage_t* struct_dati, uint8_t retransmission, PktSource_t pkt_source);
void CancellaEEPROM(void);
uint8_t SalvataggioStrutturaDati(uint8_t indice);
void LetturaStrutturaDati(uint8_t indice);
void GestioneDatoJsonRx(void);
void PulisciDatoJsonRx(void);
uint32_t DecodificaStringa(const uint8_t*  Stringa, uint8_t dim);





#endif
	
