#ifndef __ALESEA_MEMORY_H
#define __ALESEA_MEMORY_H

#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include "Gestione_dato.h"

#define BANCO_1         1
#define BANCO_2         2
#define BANCO_1_2       3

void ReadStruct(uint8_t *pStruct, uint32_t add, uint16_t size);
void WriteStruct(uint8_t *pStruct, uint32_t add, uint16_t size);
uint8_t WriteCRCStruct(/*VariabileCritica_t**/uint8_t* pArray, uint32_t add, uint16_t size, uint32_t crc_add);

uint32_t CalcolaCRCVariabiliCritiche(void);
uint8_t SalvaVariabileCritica(VariabileCritica_t* argStruttura, uint8_t argBanco);
uint32_t CalcolaCRC(uint32_t val);
uint32_t LeggiVariabileCritica(VariabileCritica_t* argStruttura);
void GestioneAllineamentoBancoEeprom(VariabileCritica_t* argStruttura);


#endif