#ifndef __NDEF_NDEF_LIB_H__
#define __NDEF_NDEF_LIB_H__

#define NO_ID 0

#include <stdbool.h>
#include <stdint.h>

#define NDEF_BUFFER_SIZE	100
struct ndef_message {
    size_t length;
    struct ndef_record* records;
};

struct ndef_payload {
    size_t length;
    uint8_t buffer[NDEF_BUFFER_SIZE];
};

// A zero-copy data structure for storing ndef records. All fields should be
// accessed using accessors defined below.
struct ndef_record {
    uint8_t buffer[NDEF_BUFFER_SIZE];
    size_t length;

    uint8_t type_length;
    size_t type_offset;

    uint8_t id_length;
    size_t id_offset;

    uint32_t payload_length;
    size_t payload_offset;
};


struct ndef_record ndef_create(
        uint8_t tnf, bool is_begin, bool is_end, bool is_chunk,
        bool is_short, bool has_length,
        char* type, uint8_t type_length,
        char* id, uint8_t id_length,
        uint8_t* uint8_t, uint32_t payload_length);


#endif
