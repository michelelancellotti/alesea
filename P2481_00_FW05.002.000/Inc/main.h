/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "mTypes.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
extern I2C_HandleTypeDef hi2c1;
extern RTC_HandleTypeDef hrtc;
	
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void BaseDeiTempi(void);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/


//define in comune
#define SWITCH_OFF_Pin GPIO_PIN_7
#define SWITCH_OFF_GPIO_Port GPIOA
#define POWERKEY_MODEM_Pin GPIO_PIN_0
#define POWERKEY_MODEM_GPIO_Port GPIOB
#define RESET_MODEM_Pin GPIO_PIN_1
#define RESET_MODEM_GPIO_Port GPIOB
#define CPU_TX_GSM_Pin GPIO_PIN_9
#define CPU_TX_GSM_GPIO_Port GPIOA
#define CPU_RX_GSM_Pin GPIO_PIN_10
#define CPU_RX_GSM_GPIO_Port GPIOA
#define STATUS_MODEM_Pin GPIO_PIN_11
#define STATUS_MODEM_GPIO_Port GPIOA
#define NFC_Pin GPIO_PIN_15
#define NFC_GPIO_Port GPIOA
#define FD_Pin GPIO_PIN_5
#define FD_GPIO_Port GPIOB
#define FD_EXTI_IRQn EXTI4_15_IRQn
#define SCK_Pin GPIO_PIN_6
#define SCK_GPIO_Port GPIOB
#define SDA_Pin GPIO_PIN_7
#define SDA_GPIO_Port GPIOB


//define relative a HW3.x
#define DRIVER_NTC_HW3_Pin GPIO_PIN_1
#define DRIVER_NTC_HW3_GPIO_Port GPIOA
#define CPU_TX_DEBUG_HW3_Pin GPIO_PIN_2
#define CPU_TX_DEBUG_HW3_GPIO_Port GPIOA
#define CPU_RX_DEBUG_HW3_Pin GPIO_PIN_3
#define CPU_RX_DEBUG_HW3_GPIO_Port GPIOA
#define LED1_HW3_Pin GPIO_PIN_4
#define LED1_HW3_GPIO_Port GPIOA
#define LED2_HW3_Pin GPIO_PIN_5
#define LED2_HW3_GPIO_Port GPIOA
#define LED3_HW3_Pin GPIO_PIN_6
#define LED3_HW3_GPIO_Port GPIOA
#define GPS_AMPLI_HW3_Pin GPIO_PIN_12
#define GPS_AMPLI_HW3_GPIO_Port GPIOA
//#define NETLIGHT_Pin GPIO_PIN_8       //deciso di non gestire netlight su nessun hw
//#define NETLIGHT_GPIO_Port GPIOA


//define relative a HW4.x
#define LED1_HW_4_Pin GPIO_PIN_1
#define LED1_HW_4_GPIO_Port GPIOA
#define WIFI_TX_HW4_Pin GPIO_PIN_2
#define WIFI_TX_HW4_GPIO_Port GPIOA
#define WIFI_RX_HW4_Pin GPIO_PIN_3
#define WIFI_RX_HW4_GPIO_Port GPIOA
#define WIFI_EN_HW4_Pin GPIO_PIN_4
#define WIFI_EN_HW4_GPIO_Port GPIOA
#define MDM_ON_OFF_HW4_Pin GPIO_PIN_5
#define MDM_ON_OFF_HW4_GPIO_Port GPIOA
#define WIFI_ON_OFF_HW4_Pin GPIO_PIN_6
#define WIFI_ON_OFF_HW4_GPIO_Port GPIOA
#define BOOST_EN_HW4_Pin GPIO_PIN_12
#define BOOST_EN_HW4_GPIO_Port GPIOA
#define GPS_AMPLI_HW4_Pin GPIO_PIN_15
#define GPS_AMPLI_HW4_GPIO_Port GPIOC
#define HW1_HW4_Pin GPIO_PIN_14
#define HW1_HW4_GPIO_Port GPIOC
#define HW2_HW4_Pin GPIO_PIN_15
#define HW2_HW4_GPIO_Port GPIOC
#define SCK_MG_Pin GPIO_PIN_8
#define SCK_MG_GPIO_Port GPIOA
#define SDA_MG_Pin GPIO_PIN_4
#define SDA_MG_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

	
//#define ENB_BOOT_LOADER			//Bootloader abilitato (abilita rilocazione tabella vettori IRQ)

void SystemClock_Config(void);

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
