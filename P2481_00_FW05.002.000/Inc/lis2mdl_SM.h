

void MacchinaStatiMagnetometro(void);
void AlgoritmoRotazione(void);


typedef enum
{
  MAG_INIT					= 0,
  MAG_MONITOR					= 1, 
  MAG_LOW_POWER                                 = 2,
} AleseaMacchinaStatiMag;

#define FILTRO_VEL_ANGOLARE_STABILE     40*100  //ms


extern AleseaMacchinaStatiMag StatoMAG;
extern AleseaMacchinaStatiMag StatoCollaudoMAG;
extern uint8_t FlagMagnetometroOn;
extern uint8_t Lis2mdl_powerOn;
extern uint8_t indexMag;
extern int32_t   dato_mag_old;
extern uint8_t Count_derivata_diff;
extern uint32_t NumeroGiriMagnetometro, /*NumeroGiriMagnetometro_1,*/ NumeroGiriMagnetometro_2 /*, NumeroGiriMagnetometro_no_filtro*/;
extern uint8_t ID_mag;
extern uint16_t ContatoreVelAngolareStabile, /*ContatoreVelAngolareStabile_1,*/ ContatoreVelAngolareStabile_2, ContatoreAlgoritmoTest;
extern uint8_t /*IntegratoreMagnetometroOn, *//*IntegratoreMagnetometroOn_1,*/ IntegratoreMagnetometroOn_2;
extern uint8_t /*FlagPulisciParzialeMagnetometro,*//*FlagPulisciParzialeMagnetometro_1,*/ FlagPulisciParzialeMagnetometro_2;
extern uint8_t Contatore_retio_pos, Contatore_retio_neg;
uint16_t LIS2MDL_algoritmo(int32_t data_raw);
void CollaudoMagnetometro(void);
