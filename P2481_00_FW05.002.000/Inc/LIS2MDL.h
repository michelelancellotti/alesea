#include "LSM6DSL_driver.h"
#include "mTypes.h"




void InitLIS2MDL(void);
int32_t LIS2MDL_get_mean_10(void);
void LIS2MDL_low_power(void);
uint8_t LIS2MDL_get_ID(void);


