

//#define NUMERO_CAMPIONI_ORIENTAMENTO	4
#define SOGLIA_ORIENTAMENTO		500
#define FLANGIA_PERPENDICOLARE_TERRA	1
#define FLANGIA_A_TERRA			0
#define	FLANGIA_NO_RESULT               2
#define MEMS_CALIB_MAX_VALUE            4       //dps
#define N_CAMPIONI_CALIBRAZIONE_V_ANG   500
#define N_CAMPIONI_V_ANG                100
#define N_CAMPIONI_ACC                  25      //25 => max 60 rpm              //100 => max 15 rpm
#define QUADRANTE_ISTERESI              100
#define ARRAY_TEMPERATURE_DIM           30
#define SOGLIA_SUP_RPS_CNG_MODE         0.5      //giri al secondo = RPM/60
#define SOGLIA_INF_RPS_CNG_MODE         0.3       //giri al secondo = RPM/60
#define SOGLIA_VEL_ANGOLARE             3       //giri al minuto
#define MAX_TIME_SPIN_SESSION           7200    //secondi
#define MAX_SUBSESSION_NUMBER           10

#define GYRO    1
#define QUADRANTI   0


typedef struct
{
  float sign;
  float val;
  float sum;
  float mean;
}mg_t;

typedef enum
{
  MEMS_INIT_CONTA_GIRI					= 0,
  MEMS_ORIENTAMENTO                                     = 1,
  MEMS_CONTA_GIRI					= 2, 
  MEMS_SALVA_GIRI					= 3,
  MEMS_GO_TO_STOP_MODE					= 4,
} AleseaMacchinaStatiMems;

typedef enum
{
  MEMS_INIT_CALIBRAZIONE				= 0,
  MEMS_PAUSA					        = 1, 
  MEMS_CALIBRAZIONE					= 2,
} AleseaMacchinaStatiCalibrazioneMems;

typedef enum
{
  MOVE_INIT				                = 0,
  MOVE_RUN					        = 1, 
  MOVE_STOP				        	= 2,
} AleseaMacchinaStatiMovimento;



extern uint16_t ContaInterruptRotazione;
extern AleseaMacchinaStatiMems StatoMEMS;
extern AleseaMacchinaStatiMovimento StatoMovimento;
extern uint8_t FlagWakeupDaMEMS;
extern uint8_t FlagRefreshRotation;
extern uint8_t FlagWakeUpDaShock;
extern uint8_t FlagWakeUpDaMovimento;
extern uint8_t FlagWakeUpDaFreeFall;
extern uint8_t FlagCommOnInSpinSession;
extern uint8_t FlagWakeupPerTemp;
extern uint8_t FlagSpinSessionStart;
extern uint8_t FlagMovementSessionStart;
extern float NumeroGiriFloatFase;
extern uint16_t SpinTimeCounter;
extern uint16_t SubSessionDuration;
extern uint16_t SpinStartStopCounterAppoggio;
extern uint8_t SubSessionCounter;
extern uint8_t FlagShockSessionStart;
extern uint8_t FlagCommOnInShockSession;
extern uint8_t FlagCommOnInMovementSession;
extern uint8_t FlagCommOnInSpinSessionError;

extern uint8_t SpinTimeCounterEnable;

extern uint8_t DebugSession;
//extern uint8_t SogliaAccelerometro;

extern uint8_t ArrayTemperaturaIndex;
extern int32_t Magnetometro_dato_medio;
extern uint8_t TriggerOrientamento;
extern uint16_t num_campioni_validi_dbg;

uint8_t GetOrientamento(uint16_t num_campioni);
void MacchinaStatiMEMS(void);
void MacchinaStatiShock(void);
void MacchinaStatiMovimento(void);
uint8_t GetTemperature(void);
void MacchinaStatiSpinSession(void);
void SogliaAccelerometroSet(uint8_t);
void MacchinaStatiFreeFall(void);