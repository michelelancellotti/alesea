#include <string.h>
#include <stdint.h>


#define EEPROM_ADD_PASSO				256
#define NUM_MAX_RECORD	  				5
#define UPD_KEY_PRESENTE				0xAA



////////////////////////////////////////////////
//MAPPATURA EEPROM 4.X
////////////////////////////////////////////////

#define EEPROM_CHIAVE_COLLAUDO1_FW4	  		0x08080009		//1 byte
#define EEPROM_CHIAVE_COLLAUDO2_FW4  		        0x08080700		//1 byte
#define EEPROM_CHIAVE_COLLAUDO3_FW4	  		0x08080704		//1 byte
//#define EEPROM_TX_TIME1	  				0x0808000B		//4 byte
#define EEPROM_CONSUMO_TOT_FW4				0x08080010		//4 byte
#define EEPROM_NUMERO_GIRI_TOT_CW_FW4			0x08080014		//4 byte
#define EEPROM_ICCID_FW4				0x08080018		//20 byte
#define EEPROM_STATO_ALESEA1_FW4			0x0808002C		//1 byte
#define EEPROM_INDICE_TRASMISSIONE_FW4		        0x0808002D		//4 byte
#define EEPROM_NUMERO_GIRI_TOT_ACW_FW4		        0x08080031		//4 byte
#define EEPROM_CONTA_SHOCK_FW4				0x08080036		//4 byte
#define EEPROM_PERIODO_ACTIVE1_FW4			0x0808003A		//4 byte
#define EEPROM_PREACTIVE_COUNT				0x0808003E		//1 byte
#define EEPROM_CONTA_FREE_FALL_FW4                      0x0808003F		//1 byte

//#define EEPROM_ADD_DATI_1  				0x08080040		//5 X 256 BYTE  -> 0x08080540

#define EEPROM_MICRO_ACTIVITY_FW4			0x08080600		//4 byte
#define EEPROM_MODEM_ACTIVITY_FW4			0x08080604		//4 byte
#define EEPROM_RESET_EEPROM_FW4				0x08080608		//4 byte
#define EEPROM_CONTA_STATO_IN_FW4			0x0808060C		//1 byte
#define EEPROM_CONTA_STATO_OUT_FW4			0x0808060D		//1 byte
#define EEPROM_CONTA_FAIL_MODEM_ON_FW4		        0x0808060E		//1 byte
#define EEPROM_TIPO_MODEM_FW4		                0x0808060F		//1 byte

//liberi

#define EEPROM_STATO_ALESEA2_FW4			0x08080708		//1 byte	  					0x0808070B		//1 byte
#define EEPROM_STATO_ALESEA3_FW4			0x0808070C		//1 byte
#define EEPROM_CONTA_EVENTI_CRC_FW4     		0x08080710		//1 byte

#define EEPROM_STATO_GPS_ASSISTITO_FW4		        0x08080718		//1 byte
#define EEPROM_STATO_GPS_TIMEOUT_FW4			0x08080719              //1 byte

#define EEPROM_PERIODO_ACTIVE2_FW4			0x0808071C		//4 byte

#define EEPROM_PERIODO_ACTIVE3_FW4			0x08080720		//4 byte

//diagnostica
#define EEPROM_DIAGNOSTICA_1_FW4				0x0808072C		//1 byte
#define EEPROM_DIAGNOSTICA_2_FW4				0x0808072D		//1 byte
#define EEPROM_DIAGNOSTICA_3_FW4				0x0808072E		//1 byte
#define EEPROM_DIAGNOSTICA_4_FW4				0x0808072F		//1 byte
#define EEPROM_DIAGNOSTICA_5_FW4				0x08080730		//1 byte
#define EEPROM_DIAGNOSTICA_6_FW4				0x08080731		//1 byte
#define EEPROM_DIAGNOSTICA_7_FW4				0x08080732		//1 byte
#define EEPROM_DIAGNOSTICA_8_FW4				0x08080733		//1 byte
//                                                      
#define EEPROM_CONTA_UNP_FW4				0x08080734		//1 byte
#define EEPROM_SOGLIA_SHOCK_FW4                             0x08080735		//1 byte
#define EEPROM_SOGLIA_MOV_FW4                               0x08080736		//1 byte
#define EEPROM_FILTRO_QUADRANTE_FERMO_FW4                   0x08080737              //4 byte
#define EEPROM_GPS_PRO_FW4                                  0x0808073B              //1 byte
#define EEPROM_GPS_PRO_TIME_FW4                             0x0808073C              //1 byte
#define EEPROM_GPS_FREQ_FORCING_FW4                         0x0808073D              //1 byte
#define EEPROM_PROFILO_TEMPERATURA_ON_FW4                   0x0808073E              //1 byte
#define EEPROM_ROTAZIONE_ORIZZ_FW4                          0x0808073F              //1 byte
#define EEPROM_MOVE_SCH_INTERVAL_FW4                        0x08080740              //4 byte

//...


#define EEPROM_CRC					0x080807F0		//4 byte
#define EEPROM_CONTA_RESET_FW4				0x080807F4              //1 byte


#define EEPROM_ADD_PASSO				256
#define NUM_MAX_RECORD	  				5



////////////////////////////////////////////////
//MAPPATURA EEPROM 5.X
////////////////////////////////////////////////
#define EEPROM_B1_START  				0x08080000
#define EEPROM_B2_START  				0x08080c00

//INDIRIZZI BLINDATI    //256 byte
#define EEPROM_ADD_CHIAVE  				0x08080000              //1 byte
#define EEPROM_ADD_VERSION  				0x08080001              //1 byte
#define EEPROM_ADD_SUBVERSION  				0x08080002              //1 byte
#define EEPROM_ADD_COD_ERRORE  				0x08080003              //1 byte
#define EEPROM_ADD_N_RETRY  				0x08080004              //1 byte 
//#define EEPROM_ADD_CHECKSUM  				0x08080005		//il bootloader scrive la checksum => 4 byte


//




//chiavi        //256 byte
#define CHIAVE_VARIABILE_CRITICA_1                      0x08080100              //1 byte
#define CHIAVE_VARIABILE_CRITICA_2                      0x08080101              //1 byte
#define CHIAVE_VARIABILE_CRITICA_3                      0x08080102              //1 byte
#define EEPROM_CHIAVE_COLLAUDO_1	  		0x08080103		//1 byte
#define EEPROM_CHIAVE_COLLAUDO_2	  		0x08080104		//1 byte
#define EEPROM_CHIAVE_COLLAUDO_3	  		0x08080105		//1 byte
#define EEPROM_CHIAVE_FW_1	  	        	0x08080106		//1 byte
#define EEPROM_CHIAVE_FW_2	  	        	0x08080107		//1 byte
#define EEPROM_CHIAVE_FW_3	  	        	0x08080108		//1 byte



//costanti      //256 byte
#define EEPROM_TIPO_MODEM		                0x08080201		//1 byte
#define EEPROM_ICCID					0x08080208		//20 byte

//

//variabili     //256 byte
#define EEPROM_INDICE_TRASMISSIONE		        0x08080300		//4 byte
#define EEPROM_NUMERO_GIRI_TOT_CW			0x08080304		//4 byte
#define EEPROM_NUMERO_GIRI_TOT_ACW		        0x08080308		//4 byte
#define EEPROM_CONTA_SHOCK				0x0808030C		//4 byte
#define EEPROM_CONSUMO_TOT				0x08080310		//4 byte
#define EEPROM_SOGLIA_MOV	 	                0x08080315		//1 byte
#define EEPROM_SOGLIA_SHOCK                             0x08080316		//1 byte
#define EEPROM_CONTA_FREE_FALL                          0x08080318		//1 byte
#define EEPROM_FILTRO_QUADRANTE_FERMO                   0x08080319		//2 byte
#define EEPROM_GPS_PRO                                  0x0808031B		//1 byte
#define EEPROM_GPS_PRO_TIME                             0x0808031C		//1 byte
#define EEPROM_GPS_FREQ_FORCING                         0x0808031D              //1 byte
#define EEPROM_PROFILO_TEMPERATURA_ON                   0x0808031E              //1 byte
#define EEPROM_ROTAZIONE_ORIZZ                          0x0808031F              //1 byte
#define EEPROM_MOVE_SCH_INTERVAL                        0x08080320              //4 byte
#define EEPROM_WIFI_ENABLE                              0x08080324              //1 byte
#define EEPROM_GPS_MODE                                 0x08080325              //1 byte
#define EEPROM_SPIN_SCH_INTERVAL                        0x08080326              //4 byte
#define EEPROM_SHOCK_SCH_INTERVAL                       0x0808032A              //4 byte
#define EEPROM_NUMBER_TX_IOT                            0x0808032E              //1 byte
#define EEPROM_LSM6DSL_CONTA_REINIT                     0x0808032F              //1 byte




//diagnostica   //256 byte
#define EEPROM_DIAGNOSTICA_1				0x08080400		//1 byte
#define EEPROM_DIAGNOSTICA_2				0x08080401		//1 byte
#define EEPROM_DIAGNOSTICA_3				0x08080402		//1 byte
#define EEPROM_DIAGNOSTICA_4				0x08080403		//1 byte
#define EEPROM_DIAGNOSTICA_5				0x08080404		//1 byte
#define EEPROM_DIAGNOSTICA_6				0x08080405		//1 byte
#define EEPROM_DIAGNOSTICA_7				0x08080406		//1 byte
#define EEPROM_DIAGNOSTICA_8				0x08080407		//1 byte
#define EEPROM_CONTA_EVENTI_CRC				0x08080408		//1 byte
#define EEPROM_CONTA_RESET			        0x08080409              //1 byte
#define EEPROM_MICRO_ACTIVITY				0x0808040C		//4 byte
#define EEPROM_MODEM_ACTIVITY				0x08080410		//4 byte
#define EEPROM_ERROR_EEPROM				0x08080414		//1 byte
#define EEPROM_CONTA_STATO_IN				0x08080415		//1 byte
#define EEPROM_CONTA_STATO_OUT				0x08080416		//1 byte
#define EEPROM_CONTA_FAIL_MODEM_ON		        0x08080417		//1 byte
#define EEPROM_CONTA_UNP				0x08080418		//1 byte
#define EEPROM_CONTATORE_RISVEGLI			0x08080419		//4 byte
#define EEPROM_CONTATORE_LOCAL_DEVKEY                   0x0808041D              //1 byte
//


//variabili critiche    //256 byte
#define EEPROM_STATO_ALESEA				0x08080500		//1 byte
//                                                      0x08080501              //1 byte
//                                                      0x08080502              //1 byte
//                                                      0x08080503              //1 byte
#define EEPROM_CRC_STATO_ALESEA				0x08080504		//4 byte
#define EEPROM_TX_START_UP		                0x08080508		//4 byte
#define EEPROM_CRC_TX_START_UP		                0x0808050C		//4 byte
#define EEPROM_TX_WARM_UP                               0x08080510              //4 byte
#define EEPROM_CRC_TX_WARM_UP                           0x08080514              //4 byte
#define EEPROM_TX_ACTIVE                                0x08080518              //4 byte
#define EEPROM_CRC_TX_ACTIVE                            0x0808051C              //4 byte
#define EEPROM_NUMERO_TX_START_UP                       0x08080520              //4 byte
#define EEPROM_CRC_NUMERO_TX_START_UP                   0x08080524              //4 byte
#define EEPROM_NUMERO_TX_WARM_UP                        0x08080528              //4 byte
#define EEPROM_CRC_NUMERO_TX_WARM_UP                    0x0808052C              //4 byte
#define EEPROM_STATO_GPS_TIMEOUT			0x08080530              //4 byte
#define EEPROM_CRC_STATO_GPS_TIMEOUT			0x08080534              //4 byte
#define EEPROM_GNSS_SATELLITI    			0x08080538              //4 byte
#define EEPROM_CRC_GNSS_SATELLITI         		0x0808053C              //4 byte
//#define EEPROM_CHIAVE_BOOT_UPGRADE                    0x08080540              //v5.11 scrive chiave di agg. bootloader completato => 1 byte
#define EEPROM_WIFI_COM_ENABLE                          0x08080541              //1 byte
#define EEPROM_WIFI_NUM_TX_SWITCH_MODEM                 0x08080542              //1 byte
                                                             

//
		

#define EEPROM_ADD_DATI_1  				0x08080600		//5 X 256 BYTE  

//...


#define EEPROM_B1_END	  				0x08080BFF
#define EEPROM_B2_END	  				0x080817FF




#define EEPROM_ADD_PASSO				256
#define NUM_MAX_RECORD	  				5

#define UPD_KEY_PRESENTE				0xAA



//BANCO 2//
#define EEPROM2_STATO_ALESEA				0x08080C00		//1 byte
//                                                      0x08080C01              //1 byte
//                                                      0x08080C02              //1 byte
//                                                      0x08080C03              //1 byte
#define EEPROM2_CRC_STATO_ALESEA			0x08080C04		//4 byte
#define EEPROM2_TX_START_UP		                0x08080C08		//4 byte
#define EEPROM2_CRC_TX_START_UP		                0x08080C0C		//4 byte 
#define EEPROM2_TX_WARM_UP                              0x08080C10              //4 byte
#define EEPROM2_CRC_TX_WARM_UP                          0x08080C14              //4 byte
#define EEPROM2_TX_ACTIVE                               0x08080C18              //4 byte
#define EEPROM2_CRC_TX_ACTIVE                           0x08080C1C              //4 byte
#define EEPROM2_NUMERO_TX_START_UP                      0x08080C20              //4 byte
#define EEPROM2_CRC_NUMERO_TX_START_UP                  0x08080C24              //4 byte
#define EEPROM2_NUMERO_TX_WARM_UP                       0x08080C28              //4 byte
#define EEPROM2_CRC_NUMERO_TX_WARM_UP                   0x08080C2C              //4 byte
#define EEPROM2_STATO_GPS_TIMEOUT			0x08080C30             //4 byte
#define EEPROM2_CRC_STATO_GPS_TIMEOUT			0x08080C34              //4 byte
#define EEPROM2_GNSS_SATELLITI    			0x08080C38              //4 byte
#define EEPROM2_CRC_GNSS_SATELLITI      		0x08080C3C              //4 byte

//




//extern const uint32_t EEPROM_tab[100][2];
//extern uint16_t EEPROM_IndiceArray; 
extern uint16_t CounterErroreEEPROM;

void EEPROM_WriteByte(uint32_t Indirizzo, uint8_t Dato);
uint8_t EEPROM_ReadByte(uint32_t Posizione);
void EEPROM_WriteWord(uint32_t Indirizzo, int32_t Dato);
int32_t EEPROM_ReadWord(uint32_t Posizione);
void EEPROM_Clear(void);
void My_DATAEEPROM_Unlock(void);
void My_DATAEEPROM_Lock(void);
void My_DATAEEPROM_Program(uint32_t Indirizzo, uint8_t Dato);



	