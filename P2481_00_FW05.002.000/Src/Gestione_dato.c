
#include "EEPROM.h"
#include "Gestione_dato.h"
#include "Gestione_batteria.h"
#include "Alesea_memory.h"
#include "stm32l0xx_hal.h"
#include "funzioni.h"
#include "LSM6DSL.h"
#include "LSM6DSL_SM.h"
#include "lis2mdl_SM.h"
#include "Globals.h"
#include "wifi_SM.h"
#include "modem.h"
#include "wwdg.h"
#include <stdio.h>


float TotGiriAcc, TotGiriGyr;

const unsigned char msgType[] = "\"msgType\":";
const unsigned char msgPeriodo[] = "\"periodo\":";
const unsigned char msgVersione[] = "\"versione\":";
const unsigned char msgSottoversione[] = "\"sottoversione\":";
const unsigned char msgMinuto[] = "\"minute\":";
const unsigned char msgSecondo[] = "\"secondi\":";
const unsigned char msgNumberTx[] = "\"number\":";
const unsigned char msgGpsAss[] = "\"agps\":";
const unsigned char msgGpsTime[] = "\"tgps\":";
const unsigned char msgSogliaAcc[] = "\"soglia\":";
const unsigned char msgGnssSatelliti[] = "\"satelliti\":";
const unsigned char msgTimeoutSpin[] = "\"timeout\":";
const unsigned char msgGpsPro[] = "\"gpro\":";
const unsigned char msgGpsProTime[] = "\"tpro\":";
const unsigned char msgGpsFreqForcing[] = "\"gforce\":";
const unsigned char msgProfiloTemp[] = "\"tempon\":";
const unsigned char msgRotazOrizz[] = "\"roton\":";
const unsigned char msgWifiEnable[] = "\"wifi\":";
const unsigned char msgGpsMode[] = "\"msgGpsMode\":";


AleseaMessage_t Dati;
AleseaMessage_t DatiLetti;
Diagnostica_t Diagnostica;

StrutturaCella CellaServente, CellaVicina1, CellaVicina2, CellaVicina3;

VariabileCritica_t StatoAleseaVc =      {.var32 = &Dati.StatoAlesea,                          .varEepromAddr1 = EEPROM_STATO_ALESEA,             .varEepromAddr2 = EEPROM2_STATO_ALESEA,             .crcEepromAddr1 = EEPROM_CRC_STATO_ALESEA,            .crcEepromAddr2 = EEPROM2_CRC_STATO_ALESEA,             .defaultVal = ALESEA_ACTIVE};
VariabileCritica_t SchIntStartUpVc =    {.var32 = &Dati.ActivationTiming.SchInt_START_UP,     .varEepromAddr1 = EEPROM_TX_START_UP,              .varEepromAddr2 = EEPROM2_TX_START_UP,              .crcEepromAddr1 = EEPROM_CRC_TX_START_UP,             .crcEepromAddr2 = EEPROM2_CRC_TX_START_UP,              .defaultVal = PERIODO_START_UP};
VariabileCritica_t SchIntWarmUpVc =     {.var32 = &Dati.ActivationTiming.SchInt_WARM_UP,      .varEepromAddr1 = EEPROM_TX_WARM_UP,               .varEepromAddr2 = EEPROM2_TX_WARM_UP,               .crcEepromAddr1 = EEPROM_CRC_TX_WARM_UP,              .crcEepromAddr2 = EEPROM2_CRC_TX_WARM_UP,               .defaultVal = PERIODO_WARM_UP};
VariabileCritica_t SchIntActiveVc =     {.var32 = &Dati.ActivationTiming.SchInt_ACTIVE,       .varEepromAddr1 = EEPROM_TX_ACTIVE,                .varEepromAddr2 = EEPROM2_TX_ACTIVE,                .crcEepromAddr1 = EEPROM_CRC_TX_ACTIVE,               .crcEepromAddr2 = EEPROM2_CRC_TX_ACTIVE,                .defaultVal = PERIODO_ACTIVE};
VariabileCritica_t NumberTxStartUpVc =  {.var32 = &Dati.ActivationTiming.NumberTx_START_UP,   .varEepromAddr1 = EEPROM_NUMERO_TX_START_UP,       .varEepromAddr2 = EEPROM2_NUMERO_TX_START_UP,       .crcEepromAddr1 = EEPROM_CRC_NUMERO_TX_START_UP,      .crcEepromAddr2 = EEPROM2_CRC_NUMERO_TX_START_UP,       .defaultVal = NUMERO_TX_START_UP};
VariabileCritica_t NumberTxWarmUpVc =   {.var32 = &Dati.ActivationTiming.NumberTx_WARM_UP,    .varEepromAddr1 = EEPROM_NUMERO_TX_WARM_UP,        .varEepromAddr2 = EEPROM2_NUMERO_TX_WARM_UP,        .crcEepromAddr1 = EEPROM_CRC_NUMERO_TX_WARM_UP,       .crcEepromAddr2 = EEPROM2_CRC_NUMERO_TX_WARM_UP,        .defaultVal = NUMERO_TX_WARM_UP};
VariabileCritica_t GpsTimeoutVc =       {.var32 = &Dati.GNSS_data.GpsTimeout,                 .varEepromAddr1 = EEPROM_STATO_GPS_TIMEOUT,        .varEepromAddr2 = EEPROM2_STATO_GPS_TIMEOUT,        .crcEepromAddr1 = EEPROM_CRC_STATO_GPS_TIMEOUT,       .crcEepromAddr2 = EEPROM2_CRC_STATO_GPS_TIMEOUT,        .defaultVal = GPS_TIMEOUT_DEFAULT};
VariabileCritica_t GnssSatellitiVc =    {.var32 = &Dati.GNSS_data.GnssSatelliti,              .varEepromAddr1 = EEPROM_GNSS_SATELLITI,           .varEepromAddr2 = EEPROM2_GNSS_SATELLITI,           .crcEepromAddr1 = EEPROM_CRC_GNSS_SATELLITI,          .crcEepromAddr2 = EEPROM2_CRC_GNSS_SATELLITI,           .defaultVal = GPS_SATELLITI_DEFAULT};


WiFiParam_t WiFiParam;
AleseaICCID_t AleseaICCID;

uint8_t VersioneFW;
uint8_t SottoversioneFW;
uint8_t ModemVersioneFW[30];

UpgradeData_t UpgradeDataRX;
uint8_t UpgradeFwCount;

uint8_t VersioneHW;
uint8_t SottoversioneHW;

uint8_t* DatoJson;
uint16_t DatoJsonDim;

uint8_t DatoJsonRx[JSON_MAX_BUFFER_DATA_RX];
uint16_t DatoJsonRxDim;


uint8_t ServerType = SRV_IOT_CENTRAL;
uint8_t CounterTxIot = 0;
uint8_t NumberTxIot;

///////////////////////////////////////////////////
////////Scrittura della struttura in EEPROM////////	
///////////////////////////////////////////////////
uint8_t SalvataggioStrutturaDati(uint8_t indice)
{
  uint32_t add;
  uint32_t *punt;
  uint8_t *p_appoggio;
  punt = &Dati.NetworkPerformance.IndiceTrasmissione;
  uint8_t result = 1;
  
  add = EEPROM_ADD_DATI_1 + indice*EEPROM_ADD_PASSO;	//Calcolo l'indirizzo in cui salvare
  
  p_appoggio = (uint8_t *)punt;
  
  for(int i = 0; i < sizeof(Dati); i++)
  { 
    EEPROM_WriteByte( add, *p_appoggio );
    
    //rileggo il dato salvato e ne verifico la coerenza
    if(*p_appoggio != EEPROM_ReadByte(add))
    {
      result = 0;
    }
    
    add++;
    (p_appoggio)++;
 /* 
  RefreshWD();
*/
  }
  return result;	//return 1 se ok, 0 se anomalia EEPROM
}

///////////////////////////////////////////////////
//Lettura dell'ultima struttura salvata in EEPROM//	
///////////////////////////////////////////////////
void LetturaStrutturaDati(uint8_t indice)
{
  uint32_t add;
  uint8_t *p_appoggio;
  p_appoggio = (uint8_t *)&DatiLetti.NetworkPerformance.IndiceTrasmissione;
  add = EEPROM_ADD_DATI_1 + indice*EEPROM_ADD_PASSO;	//ricavo l'indirizzo di lettura dopo aver letto l'indice pacchhetto in EEPROM
  
  for(int i = 0; i < sizeof(Dati); i++)
  {
    *(p_appoggio++) = EEPROM_ReadByte(add++);
  }
}



//////////////////////////////////////////////////
//////////////Costruzione file JSON//////////////
//////////////////////////////////////////////////

void GeneraStringa(AleseaMessage_t* struct_dati, uint8_t retransmission, PktSource_t pkt_source)		
{  
  uint16_t indice = 0;	
  int32_t free_space; 
  
  mod_GET_TxDataBuffer(&DatoJson, (uint16_t*)&free_space);
  
  free_space = JSON_MAX_BUFFER_DATA_TX;  
  indice = snprintf((char*)DatoJson + indice, free_space, "\x7B\"id\"\x3A\"%s\"", AleseaICCID.ICCID);   //utilizzo SEMPRE dato in RAM (tampona eventuale mancanza di ICCID nel primo pkt del collaudo ...)

  
  //if(struct_dati->AleseaDate.SizeOfDataOra == 0)
  //{
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"ut\"\x3A%u", struct_dati->AleseaDate.UnixTime);
    
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"sd\"\x3A%u", StopModeDuration);    
   
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"lut\"\x3A%u", last_ut);
    
  //}
  //else  
  //{
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;    
    indice += snprintf((char*)DatoJson + indice, free_space,",\"dt\"\x3A\"20%s\"", struct_dati->AleseaDate.DataOra);
  //}
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ea\"\x3A%u", LSM6DSLCounterReinit); 
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ra\"\x3A%u", CountI2C1Reinit);   

  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"rm\"\x3A%u", CountI2C3Reinit);  
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"rf\"\x3A%u", CountRegisterWRFailure);

  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"df\"\x3A\"%02x\"", RegisterWRFailureMap);       
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"bw\"\x3A\"%s\"", struct_dati->SystemVersion.BootVersion);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"sw\"\x3A\"%s\"", struct_dati->SystemVersion.VersioneFwTx);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"hw\"\x3A\"%s\"", struct_dati->SystemVersion.VersioneHwTx);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"bk\"\x3A%d", ServerType);
  
  if(Dati.StatoAlesea == ALESEA_COLLAUDO) 
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"mw\"\x3A\"%s\"", ModemVersioneFW);
  }
  
  
  if(struct_dati->TipoModem == MOD_BG95)       
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"mt\"\x3A\"%s\"", "BG_95");
  }
  else if(struct_dati->TipoModem == MOD_BG96)      
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"mt\"\x3A\"%s\"", "BG_96");
  }
  else 
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"mt\"\x3A\"%s\"", "N/D");
  }
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"in\"\x3A\"%c\"", struct_dati->NetworkPerformance.InputVariation);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"b0\"\x3A%u", struct_dati->AleseaMeasure.CaricaResidua);
    
  
  switch(pkt_source)
  {
    case PKT_SRC_MODEM: {  
      if( (struct_dati->Coordinates.gps_latitudine != 0) || (struct_dati->Coordinates.gps_longitudine != 0))
      {
        if( (struct_dati->Coordinates.gpsPro_latitudine != 0) || (struct_dati->Coordinates.gpsPro_longitudine != 0))
        {
          free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
          indice += snprintf((char*)DatoJson + indice, free_space,",\"c0\"\x3A[%f,%f]", struct_dati->Coordinates.gpsPro_latitudine, struct_dati->Coordinates.gpsPro_longitudine);
          free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
          indice += snprintf((char*)DatoJson + indice, free_space,",\"cp\"\x3A[%f,%f]", struct_dati->Coordinates.gps_latitudine, struct_dati->Coordinates.gps_longitudine);
        }
        else
        {
          free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
          indice += snprintf((char*)DatoJson + indice, free_space,",\"c0\"\x3A[%f,%f]", struct_dati->Coordinates.gps_latitudine, struct_dati->Coordinates.gps_longitudine);
        }
      }
      else
      {    
        free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
        indice += snprintf((char*)DatoJson + indice, free_space,",\"c1\"\x3A[%u,%u,%u,%u]", struct_dati->NetworkCell.MMC, struct_dati->NetworkCell.MNC, struct_dati->NetworkCell.CID, struct_dati->NetworkCell.LAC);
        if(retransmission == 0)
        {
          free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
          indice += snprintf((char*)DatoJson + indice, free_space,",\"db\"\x3A%d", CellaServente.Rssi);  //,"db" :xxx
          free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
          indice += snprintf((char*)DatoJson + indice, free_space,",\"cn\"\x3A["); //,"cn":[
          
          if(CellaVicina1.DatoPresente)
          {
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,"\x7B\"an\"\x3A[%u,%u,%u,%u]", CellaVicina1.MMC, CellaVicina1.MNC, CellaVicina1.CID, CellaVicina1.LAC_TAC);  //{"an": [xxx,xx,xxxxx,xxxxx]
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,",\"db\"\x3A%d", CellaVicina1.Rssi);  //,"db" :xxx
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,",\"tc\"\x3A\"%s\"\x7D", CellaVicina1.TrasmissionChannel);        //,"tc" : "GSM"}
          }
          if(CellaVicina2.DatoPresente)
          {
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,",\x7B\"an\"\x3A[%u,%u,%u,%u]", CellaVicina2.MMC, CellaVicina2.MNC, CellaVicina2.CID, CellaVicina2.LAC_TAC);  //,{"an": [xxx,xx,xxxxx,xxxxx]
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,",\"db\"\x3A%d", CellaVicina2.Rssi);  //,"db" :xxx
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,",\"tc\"\x3A\"%s\"\x7D", CellaVicina2.TrasmissionChannel);        //,"tc" : "GSM"}
          }
          if(CellaVicina3.DatoPresente)
          {
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,",\x7B\"an\"\x3A[%u,%u,%u,%u]", CellaVicina3.MMC, CellaVicina3.MNC, CellaVicina3.CID, CellaVicina3.LAC_TAC);  //,{"an": [xxx,xx,xxxxx,xxxxx]
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,",\"db\"\x3A%d", CellaVicina3.Rssi);  //,"db" :xxx
            free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
            indice += snprintf((char*)DatoJson + indice, free_space,",\"tc\"\x3A\"%s\"\x7D", CellaVicina3.TrasmissionChannel);        //,"tc" : "GSM"}
          } 
          free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
          indice += snprintf((char*)DatoJson + indice,free_space,"]");            //]
        }
      }
      free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
      indice += snprintf((char*)DatoJson + indice, free_space,",\"tx\"\x3A\"%s\"", struct_dati->NetworkPerformance.TrasmissionChannel);
    } break;
    
    case PKT_SRC_WIFI: {
      free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
      indice += snprintf((char*)DatoJson + indice, free_space,",\"c1\"\x3A[%u,%u,%u,%u]", 0, 0, 0, 0);
      free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
      indice += snprintf((char*)DatoJson + indice, free_space,",\"cn\"\x3A%s", "[]");
      free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
      indice += snprintf((char*)DatoJson + indice, free_space,",\"tx\"\x3A\"%s\"", "WIFI");
    } break;
  }  
  
/*
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r0\"\x3A%u", CountReinitSetReg[0]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r1\"\x3A%u", CountReinitSetReg[1]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r2\"\x3A%u", CountReinitSetReg[2]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r3\"\x3A%u", CountReinitSetReg[3]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r4\"\x3A%u", CountReinitSetReg[4]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r5\"\x3A%u", CountReinitSetReg[5]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r6\"\x3A%u", CountReinitSetReg[6]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r7\"\x3A%u", CountReinitSetReg[7]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r8\"\x3A%u", CountReinitSetReg[8]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r9\"\x3A%u", CountReinitSetReg[9]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r10\"\x3A%u", CountReinitSetReg[10]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r11\"\x3A%u", CountReinitSetReg[11]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r12\"\x3A%u", CountReinitSetReg[12]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r13\"\x3A%u", CountReinitSetReg[13]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r14\"\x3A%u", CountReinitSetReg[14]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r15\"\x3A%u", CountReinitSetReg[15]);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"r16\"\x3A%u", CountReinitSetReg[16]);
*/ 
  
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"d0\"\x3A%u", struct_dati->NetworkPerformance.IndiceTrasmissione);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"d1\"\x3A%u", struct_dati->NetworkPerformance.RSSI);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"p0\"\x3A%u", (struct_dati->ActivationTiming.SchedulingInterval)/60);	//lo comunico espresso in minuti
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"is\"\x3A%u", (struct_dati->ActivationTiming.SchInt_START_UP)/60);	//lo comunico espresso in minuti
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"iw\"\x3A%u", (struct_dati->ActivationTiming.SchInt_WARM_UP)/60);	//lo comunico espresso in minuti
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ia\"\x3A%u", (struct_dati->ActivationTiming.SchInt_ACTIVE)/60);	//lo comunico espresso in minuti
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"nti\"\x3A%u", NumberTxIot);	
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;  
  indice += snprintf((char*)DatoJson + indice, free_space,",\"p1\"\x3A%u", struct_dati->TiltEvent.Orientamento);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"rt\"\x3A%u", struct_dati->NetworkPerformance.Retrasmission);
  if(VersioneHW == VERSIONE_HW_4_X)  
  {    
    if(Dati.StatoAlesea == ALESEA_COLLAUDO) 
    {
      free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
      indice += snprintf((char*)DatoJson + indice, free_space,",\"mg\"\x3A%x", ID_mag);
    }
  }
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"txs\"\x3A%u", struct_dati->NetworkPerformance.TxInterruptSource);
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ss\"\x3A%d", struct_dati->SpinEvent.SpinSession);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  
  indice += snprintf((char*)DatoJson + indice, free_space,",\"or\"\x3A%d", struct_dati->SpinEvent.RotazioneOrizzontaleOn);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
 
  indice += snprintf((char*)DatoJson + indice, free_space,",\"s0\"\x3A%d", (uint32_t)(struct_dati->SpinEvent.NumeroGiriCw + 0.5));
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"s1\"\x3A%d", (uint32_t)(struct_dati->SpinEvent.NumeroGiriACw + 0.5));
  if( (struct_dati->SpinEvent.SpinSession != 0) && (retransmission == 0))
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"sg\"\x3A\"%.2f\"", TotGiriGyr);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"sa\"\x3A\"%.2f\"", TotGiriAcc);
    
  }
  //////////////////////////////////////////////////////////////////
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"if\"\x3A%d", lsm6dsl_fail_count_dbg);
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ab\"\x3A%d", CounterAccelerationBlocked);
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"gb\"\x3A%d", CounterAngularRateBlocked);
  
  //////////////////////////////////////////////////////////////////
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ir\"\x3A%d", ContaInterruptRotazione);  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"s2\"\x3A%d", struct_dati->SpinEvent.SpinStartStopCounter);  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"s3\"\x3A\"%.2f\"", struct_dati->SpinEvent.SpinFreq); 
  if(VersioneHW == VERSIONE_HW_4_X)  
  { 
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"s4\"\x3A%d", struct_dati->SpinEvent.SpinMag);
  }
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"sks\"\x3A%d", struct_dati->ShockEvent.ShockSession);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"sk\"\x3A%u", 0/*struct_dati->ShockEvent.ContaShock*/);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"sk1\"\x3A%u", struct_dati->ShockEvent.ContaShock);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ts\"\x3A%d", struct_dati->ShockEvent.SogliaShock);
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ms\"\x3A%d", struct_dati->MovementEvent.MovementSession); 
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"tm\"\x3A%d", struct_dati->MovementEvent.SogliaMovimento);
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"msi\"\x3A%d", struct_dati->MovementEvent.MoveSchInterval/60);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ssi\"\x3A%d", struct_dati->SpinEvent.SpinSchInterval/60);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ski\"\x3A%d", struct_dati->ShockEvent.ShockSchInterval/60);
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"fm\"\x3A%d", struct_dati->MovementEvent.FiltroMovimento);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"gp\"\x3A%d", struct_dati->GNSS_data.GpsPro);
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"gt\"\x3A%d",struct_dati->GNSS_data.GpsTimeout);  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"gf\"\x3A%d", struct_dati->GNSS_data.GpsFreqForcing);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"gfc\"\x3A%d", struct_dati->GNSS_data.GpsForcingCounter);
  
#ifdef FREE_FALL_ON
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ff\"\x3A%u", struct_dati->FreeFallEvent.ContaFreeFall);
#endif
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"t0\"\x3A%d", struct_dati->AleseaMeasure.Temperatura);
  if(struct_dati->AleseaMeasure.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON)
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"tmax\"\x3A%d", struct_dati->AleseaMeasure.TemperaturaMax);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"tmin\"\x3A%d", struct_dati->AleseaMeasure.TemperaturaMin);
  }
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"t1\"\x3A%u", struct_dati->GNSS_data.TempoConnessioneGps);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"t2\"\x3A%u", struct_dati->NetworkPerformance.TempoConnessione);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"t3\"\x3A%u", struct_dati->NetworkPerformance.TempoModemOn);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"t4\"\x3A%u", struct_dati->NetworkPerformance.TempoRegistrazione);
  
 

  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"ag\"\x3A\"%x\"", struct_dati->GNSS_data.GpsRegTemp);
  if(struct_dati->NetworkPerformance.LastError == 0x00)
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"rr\"\x3A\"%X\"", struct_dati->NetworkPerformance.RegRete);
  }
  else
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"rr\"\x3A\"%X,%X\"", struct_dati->NetworkPerformance.RegRete, struct_dati->NetworkPerformance.LastError);
  }
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"st\"\x3A%u", struct_dati->StatoAlesea);
  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"fwc\"\x3A%u", UpgradeFwCount);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"az\"\x3A\"%X\"", azure_reg.WORD);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"az2\"\x3A\"%X\"", struct_dati->NetworkPerformance.RegAzure);
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,",\"emq\"\x3A\"%02X%02X\"", struct_dati->NetworkPerformance.ErroreMqtt[0], struct_dati->NetworkPerformance.ErroreMqtt[1]);  
  
  if(MAC_Address.Available)
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"wa\"\x3A\"%s\"", MAC_Address.String);
    MAC_Address.Available = 0;
  }
  indice += snprintf((char*)DatoJson + indice, free_space,",\"d2\"\x3A%u", struct_dati->WiFiPerformance.RSSI);
  if(struct_dati->WiFiPerformance.LastError == 0x00)
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"wg\"\x3A\"%X\"", struct_dati->WiFiPerformance.RegRete);
  }
  else
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"wg\"\x3A\"%X,%X\"", struct_dati->WiFiPerformance.RegRete, struct_dati->WiFiPerformance.LastError);
  }
  
  if(retransmission == 0)
  {
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"cr\"\x3A%u", Diagnostica.ContaReset);    
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"bstima\"\x3A%u", Consumo_uA_h_tot);
    if(AccessPointScan.Executed)
    {
      free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
      indice += snprintf((char*)DatoJson + indice, free_space,",\"wf\"\x3A%s", AccessPointScan.String);
      AccessPointScan.Executed = 0;
    }
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"mc\"\x3A%u", Diagnostica.MicroActivityCounter/60);	//stampo in minuti
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"mo\"\x3A%u", Diagnostica.ModemActivityCounter/60);	//stampo in minuti
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"re\"\x3A%u", Diagnostica.CounterErrorEeprom);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"si\"\x3A%u", Diagnostica.ContaStatoIn);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"so\"\x3A%u", Diagnostica.ContaStatoOut);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"mf\"\x3A%u", Diagnostica.CountErroreModemOn );
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"cu\"\x3A%u", Diagnostica.CountUnpError);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"cw\"\x3A%u", Diagnostica.ContatoreRisvegli);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"dg\"\x3A\"%02x", Diagnostica.Byte1);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,"%02x", Diagnostica.Byte2);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,"%02x", Diagnostica.Byte3);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,"%02x", Diagnostica.Byte4);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,"%02x", Diagnostica.Byte5);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,"%02x", Diagnostica.Byte6);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,"%02x", Diagnostica.Byte7);
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,"%02x\"", Diagnostica.Byte8);
    
    free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
    indice += snprintf((char*)DatoJson + indice, free_space,",\"ck\"\x3A%u", Local_Devkey_Counter);
  }  
  free_space = JSON_MAX_BUFFER_DATA_TX- indice;  if(free_space < 0) free_space = 0;
  indice += snprintf((char*)DatoJson + indice, free_space,"\x7D");
  
  //terminatore di stringa aggiunto da 'snprintf' ...

  
  DatoJsonDim = indice;
}





uint32_t  Minuto;
uint32_t Secondo;
uint32_t  NumberTx;
//////////////////////////////////////////////////
/////////Gestione dei comandi da portale//////////
//////////////////////////////////////////////////
void GestioneDatoJsonRx(void)
{
  uint32_t indice;
  uint8_t app_8, app_8_b;
  uint32_t app_32;
  
  MessageTypes MessageType;
  indice = DecodificaStringa((const uint8_t *)&msgType, sizeof(msgType)-1);
  MessageType = (MessageTypes)(DatoJsonRx[++indice]);
  Dati.NetworkPerformance.InputVariation = MessageType;
  switch(MessageType)
  {
  case MODEM_SET_PERIODO_ACTIVE:
    indice = DecodificaStringa((const uint8_t *)&msgMinuto, sizeof(msgMinuto)-1);
    Minuto = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 6);
    SetInterval_ACTIVE(Minuto*60);
    break;
    
  case MODEM_FACTORY_RESET:
    RunFactoryReset();
    break;
    
  case MODEM_MICRO_RESET:
    RunMicroReset();
    break;
    
  case MODEM_SPIN_RESET:
    RunSpinReset();
    break;
    
  case MODEM_SHOCK_RESET:
    RunShockReset();
    break;
    
  case MODEM_FREE_FALL_RESET:
    RunFreeFallReset();
    break;
    
  case MODEM_DIAGNOSTICA_RESET:
    RunDiagnosticaReset();
    break;
    
  case MODEM_BATTERY_RESET:
    RunBatteryReset();
    break;
    
  case MODEM_GPS_SETUP:
    indice = DecodificaStringa((const uint8_t *)&msgGpsTime, sizeof(msgGpsTime)-1);
    app_32 = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 3);
    SetGpsSetup(app_32);
    break;
    
  case MODEM_GPS_PRO_SETUP:
    indice = DecodificaStringa((const uint8_t *)&msgGpsPro, sizeof(msgGpsPro)-1);
    app_8 = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 1);
    SetGpsPro(app_8);
    
    indice = DecodificaStringa((const uint8_t *)&msgGpsProTime, sizeof(msgGpsProTime)-1);
    app_8 = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 2);
    SetGpsProTime(app_8);
    break;
    
  case MODEM_PROFILO_TEMPERATURA_SETUP:
    indice = DecodificaStringa((const uint8_t *)&msgProfiloTemp, sizeof(msgProfiloTemp)-1);
    app_8 = DatoJsonRx[++indice]; 
    SetProfiloTemperatura(app_8);
    break;    
    
  case MODEM_ROTAZIONI_ORIZZ_ON:
    indice = DecodificaStringa((const uint8_t *)&msgRotazOrizz, sizeof(msgRotazOrizz)-1);
    app_8 = DatoJsonRx[++indice];
    SetRotazioniOrizzontali(app_8);
    break;
    
    
  case MODEM_GPS_FREQ_FORCING:    
    indice = DecodificaStringa((const uint8_t *)&msgGpsFreqForcing, sizeof(msgGpsFreqForcing)-1);
    app_8 = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 2); 
    
    SetGpsFreqForcing(app_8);    
    break;
    
  case MODEM_GPS_MODE:    
    indice = DecodificaStringa((const uint8_t *)&msgGpsMode, sizeof(msgGpsMode)-1);
    app_8 = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 1);     
    SetGpsMode(app_8);    
    break;
    
  case MODEM_DEACTIVATION:
    RunDeactivation();
    break;
    
  case MODEM_UPGRADE_FW:
    indice = DecodificaStringa((const uint8_t *)&msgVersione, sizeof(msgVersione)-1);
    app_8 = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 2);
    indice = DecodificaStringa((const uint8_t *)&msgSottoversione, sizeof(msgSottoversione)-1);
    app_8_b = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 2);
    
    SetUpgradeFw(app_8, app_8_b, UPG_USER);    
    break;
    
  case MODEM_SET_PERIODO_START_UP:       
    indice = DecodificaStringa((const uint8_t *)&msgNumberTx, sizeof(msgNumberTx)-1);
    NumberTx = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 6);
    SetNumberTx_START_UP(NumberTx);
    
    indice = DecodificaStringa((const uint8_t *)&msgMinuto, sizeof(msgMinuto)-1);
    Minuto = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 6);
    SetInterval_START_UP(Minuto*60);
    break;
    
  case MODEM_SET_PERIODO_WARM_UP:    
    indice = DecodificaStringa((const uint8_t *)&msgNumberTx, sizeof(msgNumberTx)-1);
    NumberTx = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 6);
    SetNumberTx_WARM_UP(NumberTx);
    
    indice = DecodificaStringa((const uint8_t *)&msgMinuto, sizeof(msgMinuto)-1);
    Minuto = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 6);
    SetInterval_WARM_UP(Minuto*60);
    break;
    
  case MODEM_SET_SOGLIA_MOV:    
    indice = DecodificaStringa((const uint8_t *)&msgSogliaAcc, sizeof(msgSogliaAcc)-1);
    NumberTx = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 2);
    SetSogliaMovement(NumberTx); 
    break;
    
  case MODEM_SET_PERIODO_MOVIMENTO:
    indice = DecodificaStringa((const uint8_t *)&msgMinuto, sizeof(msgMinuto)-1);
    Minuto = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 6);
    SetMoveSchInterval(Minuto*60);
    break;
    
  case MODEM_SET_PERIODO_SPIN:
    indice = DecodificaStringa((const uint8_t *)&msgMinuto, sizeof(msgMinuto)-1);
    Minuto = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 6);
    SetSpinSchInterval(Minuto*60);
    break;
    
  case MODEM_SET_PERIODO_SHOCK:
    indice = DecodificaStringa((const uint8_t *)&msgMinuto, sizeof(msgMinuto)-1);
    Minuto = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 6);
    SetShockSchInterval(Minuto*60);
    break;
    
  case MODEM_SET_SOGLIA_SHOCK:   
    indice = DecodificaStringa((const uint8_t *)&msgSogliaAcc, sizeof(msgSogliaAcc)-1);
    NumberTx = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 2);
    SetSogliaShock(NumberTx); 
    break;   
    
  case MODEM_SET_GNSS_SATELLITI:
    indice = DecodificaStringa((const uint8_t *)&msgGnssSatelliti, sizeof(msgGnssSatelliti)-1);
    NumberTx = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 1);
    SetGnssSatelliti(NumberTx);
    break;   
    
  case MODEM_SET_TIMEOUT_SPIN:    
    indice = DecodificaStringa((const uint8_t *)&msgTimeoutSpin, sizeof(msgTimeoutSpin)-1);
    NumberTx = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 3);
    SetTimeoutSpin(NumberTx); 
    break;
        
  case MODEM_WIFI_SCAN_ENABLE:    
    indice = DecodificaStringa((const uint8_t *)&msgWifiEnable, sizeof(msgWifiEnable)-1);
    app_8 = DatoJsonRx[++indice];
    SetWiFiScanEnable(app_8);     
    break;
    
  case MODEM_SET_NUMBER_TX_IOT:
    indice = DecodificaStringa((const uint8_t *)&msgNumberTx, sizeof(msgNumberTx)-1);
    NumberTx = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 3);
    SetNumberTxIot(NumberTx);
    break;
    
  case MODEM_WIFI_COM_ENABLE:
    indice = DecodificaStringa((const uint8_t *)&msgWifiEnable, sizeof(msgWifiEnable)-1);
    app_8 = DatoJsonRx[++indice];
    SetWiFiComEnable(app_8);
    
    indice = DecodificaStringa((const uint8_t *)&msgNumberTx, sizeof(msgNumberTx)-1);
    NumberTx = EstraiNumeroDaStringa(&DatoJsonRx[0], indice, 3);
    SetNumberTxSwitchModem(NumberTx);
    break;
    
  default:
    Dati.NetworkPerformance.InputVariation = 'x';
    break;
  }
}

//////////////////////////////////////////////////
///////////////Pulizia buffer JSON//////////////
//////////////////////////////////////////////////
void PulisciDatoJsonRx(void)
{
  for(uint32_t i = 0 ; i < JSON_MAX_BUFFER_DATA_RX; i++)
  {
    DatoJsonRx[i] = 0;
  }
}


///////////////////////////////////////////////////////////////////////
//////ricerca parola chiave all'interno del buffer di ricezione////////
///////////////////////////////////////////////////////////////////////
uint32_t DecodificaStringa(const uint8_t*  Stringa, uint8_t dim)
{	
  //scansione intero buffer per ricerca primo carattere
  uint32_t Result = 0;
  
  for( uint32_t k = 0; k < JSON_MAX_BUFFER_DATA_RX; k++)
  {
    if(DatoJsonRx[k] == *Stringa)
    {
      //scansione delle posizioni successive per ricerca corrispondenza intera stringa
      for(uint32_t t = 0; t <= dim; t++)
      {
        if(DatoJsonRx[(k+t)] != *(Stringa + t))
        {
          break;
        }				
        else if((t+1) == dim)
        {
          Result = (k+t);	//trovata corrispondenza su tutta la stringa
        }
      }
    }
  }
  return Result;
}







