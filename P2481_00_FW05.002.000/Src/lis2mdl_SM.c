#include "lis2mdl_reg.h"
#include "LIS2MDL.h"
#include "lis2mdl_SM.h"
#include "Gestione_dato.h"
#include "main.h"
#include "EEPROM.h"


uint8_t indexMag;
uint8_t ID_mag;
AleseaMacchinaStatiMag StatoMAG;
AleseaMacchinaStatiMag StatoCollaudoMAG;
uint8_t Magnetometro_on;
int32_t Magnetometro_dato_medio;
uint8_t Lis2mdl_powerOn;
uint32_t NumeroGiriMagnetometro, /*NumeroGiriMagnetometro_1,*/ NumeroGiriMagnetometro_2,/* NumeroGiriMagnetometroParziale,*/ /*NumeroGiriMagnetometroParziale_1,*/ NumeroGiriMagnetometroParziale_2/*, NumeroGiriMagnetometro_no_filtro */;
uint16_t ContatoreVelAngolareStabile, /*ContatoreVelAngolareStabile_1,*/ ContatoreVelAngolareStabile_2,ContatoreAlgoritmoTest;
uint8_t /*IntegratoreMagnetometroOn,*/ /*IntegratoreMagnetometroOn_1,*/ IntegratoreMagnetometroOn_2;
uint8_t /*FlagPulisciParzialeMagnetometro, *//*FlagPulisciParzialeMagnetometro_1,*/ FlagPulisciParzialeMagnetometro_2;
uint8_t Contatore_retio_pos, Contatore_retio_neg;


void MacchinaStatiMagnetometro(void)
{
  uint32_t app;
  switch(StatoMAG)
  {
  case MAG_INIT:
    InitLIS2MDL();
    Lis2mdl_powerOn = 1;
    Magnetometro_dato_medio = 0;
    StatoMAG = MAG_MONITOR;
    break;
    
  case MAG_MONITOR:    
    if(FlagMagnetometroOn)
    {
      app = LIS2MDL_algoritmo(Magnetometro_dato_medio);
      //NumeroGiriMagnetometroParziale += app;
      //NumeroGiriMagnetometro_no_filtro += app;
      //NumeroGiriMagnetometroParziale_1 += app;
      NumeroGiriMagnetometroParziale_2 += app;
      
    }
    else
    {
      Lis2mdl_powerOn = 0;
      LIS2MDL_low_power();        
      StatoMAG = MAG_LOW_POWER;
    }
    
    
    /*
    if(IntegratoreMagnetometroOn == 1)  //se la velocit� angolare � rimasta con segno coerente fino allo scadere del filtro (40 secondi) allora copio il parziale nel def.
    {
      NumeroGiriMagnetometro += NumeroGiriMagnetometroParziale;
      NumeroGiriMagnetometroParziale = 0;
      IntegratoreMagnetometroOn = 0;
    }
*/
    /*
    if(IntegratoreMagnetometroOn_1 == 1)  //se la velocit� angolare � rimasta con segno coerente fino allo scadere del filtro (40 secondi) allora copio il parziale nel def.
    {
      NumeroGiriMagnetometro_1 += NumeroGiriMagnetometroParziale_1;
      NumeroGiriMagnetometroParziale_1 = 0;
      IntegratoreMagnetometroOn_1 = 0;
    }
    */
    if(IntegratoreMagnetometroOn_2 == 1)  //se la velocit� angolare � rimasta con segno coerente fino allo scadere del filtro (40 secondi) allora copio il parziale nel def.
    {
      NumeroGiriMagnetometro_2 += NumeroGiriMagnetometroParziale_2;
      NumeroGiriMagnetometroParziale_2 = 0;
      IntegratoreMagnetometroOn_2 = 0;
    }
    /*
    if(FlagPulisciParzialeMagnetometro) //se viene alzato il flag di pulizia a fronte di un cambio di segno della velocit� angolare azzero il parziale in modo da scartare il dato accumulato fino ad ora
    {
      NumeroGiriMagnetometroParziale = 0;
      FlagPulisciParzialeMagnetometro = 0;
    }
    */
    /*
    if(FlagPulisciParzialeMagnetometro_1) //se viene alzato il flag di pulizia a fronte di un cambio di segno della velocit� angolare azzero il parziale in modo da scartare il dato accumulato fino ad ora
    {
      NumeroGiriMagnetometroParziale_1 = 0;
      FlagPulisciParzialeMagnetometro_1 = 0;
    }
    */
    if(FlagPulisciParzialeMagnetometro_2) //se viene alzato il flag di pulizia a fronte di un cambio di segno della velocit� angolare azzero il parziale in modo da scartare il dato accumulato fino ad ora
    {
      NumeroGiriMagnetometroParziale_2 = 0;
      FlagPulisciParzialeMagnetometro_2 = 0;
    }
    
    
    
    break;  
  case MAG_LOW_POWER:
    if(FlagMagnetometroOn)
      StatoMAG = MAG_INIT;
    break;    
  }
}

void CollaudoMagnetometro(void)
{
  switch(StatoCollaudoMAG)
  {
  case MAG_INIT:
    InitLIS2MDL();
    Lis2mdl_powerOn = 1;
    StatoCollaudoMAG = MAG_MONITOR;
    break;
    
  case MAG_MONITOR:     
    ID_mag = LIS2MDL_get_ID();
    Lis2mdl_powerOn = 0;
    LIS2MDL_low_power();        
    StatoCollaudoMAG = MAG_LOW_POWER;
    break;  
  case MAG_LOW_POWER:

    break;    
  }
}


int8_t Derivata, Derivata_old;
int32_t   dato_mag_old;
uint8_t Count_derivata_diff;


uint16_t LIS2MDL_algoritmo(int32_t data_raw)
{
  
  uint8_t Numero_derivata_diff = 3;
  uint16_t divisore_mg = 1;
  uint16_t Compass = 0;
  
  if(data_raw > (dato_mag_old + divisore_mg*data_raw/100)) Derivata = 1;
  else Derivata = -1;
  dato_mag_old = data_raw;
 
  if(Derivata_old != Derivata)
  {
    Count_derivata_diff++;
    if(Count_derivata_diff > Numero_derivata_diff)
    {
      
      Count_derivata_diff = 0;
      if((Derivata_old == -1) && (Derivata == 1) )
      {
        Compass++; 
      }
      Derivata_old = Derivata;
    }   
  }
  else
  {
    Count_derivata_diff = 0;
  } 
  return Compass;
}

