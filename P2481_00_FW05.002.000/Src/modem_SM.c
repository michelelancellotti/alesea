/**
******************************************************************************************
* File          	: modem_SM.c
* Descrizione        	: Gestione macchina a stati modem (livello applicativo):
*                         - localizzazione GPS
*                         - comunicazione modem
******************************************************************************************
*
* COPYRIGHT(c) 2024 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************************
*/

#include "modem_SM.h"
#include "main.h"
#include "Gestione_dato.h"
#include "EEPROM.h"
#include "Gestione_batteria.h"
#include "Globals.h"
#include "funzioni.h"
#include "wifi.h"




uint8_t GpsData[7];
uint16_t GpsRegTemp;

uint16_t TempoAcqGpsTemp;
float gsm_latitudineTemp;						
float gsm_longitudineTemp;
uint8_t IndiceTrasmissioneCollaudo;

Modem_State_t modem_state = MODEM_END_S;
Modem_State_t next_modem_state = MODEM_END_S;
uint8_t modem_driver_active = 0;
ModemTrigger_t operazione_modem;

uint8_t FlagUpgradeSuccess;
uint8_t FlagErroreModemOn;
uint8_t FlagPrimoInvioEffettuato;
uint8_t FlagTriggerSalvataggio;
uint8_t retry_subscribe = 0;
uint8_t Count_MA_UNP_ERROR = 0;
uint8_t AggiornamentoFwPendente;

/* Variabili gestione retry */
uint8_t FlagBufferFull;
uint8_t IndiceDatoSalvato;
uint8_t IndiceDatoLetto;

uint8_t TimerModemPowerOn;
uint8_t TempoModemPausa;

uint8_t FlagInvioRam;




/* Funzioni locali */
void Refresh_NetworkData(void);
void SalvataggioStruttura(void);




Modem_State_t Get_Stato_Modem_Appli(void)
{
  return modem_state;
}


void Start_MacchinaStati_Modem(void)
{
  modem_driver_active = 0;
  next_modem_state = MODEM_START_S;
}


/* Automa modem (livello applicativo).
*
*@return:
*MODEM_SM_IN_PROGRESS (esecuzione automa in corso)
*MODEM_SM_ENDED (esecuzione automa conclusa)
*/
uint8_t MacchinaStati_Modem(void)
{
  //------ Variabili locali ------
  uint16_t dummy_u16;
  //------------------------------
  //------ Variabili static ------
  static ModemAppli_t modem_driver_status = MA_OK;
  //------------------------------
  
  
  //eseguo il controllo sullo stato solo dopo accensione Modem
  if((modem_driver_active==1) && (modem_state!=MODEM_END_S) && (modem_state<MODEM_STATE_NUM))	
  {
    modem_driver_status = modem_GET_Status(operazione_modem);
    if(modem_driver_status == MA_BUSY) 
      return MODEM_SM_IN_PROGRESS;  
    
    if(modem_driver_status == MA_UNP_ERROR) 
    {
      if(Count_MA_UNP_ERROR == 0)
      {         
        Count_MA_UNP_ERROR++;
        next_modem_state = MODEM_ON_S;
      }
      else	
      {
        if(Diagnostica.CountUnpError < 0xFF)
        {
          Diagnostica.CountUnpError++;
          EEPROM_WriteByte(EEPROM_CONTA_UNP, Diagnostica.CountUnpError);
        }        
        FlagTriggerSalvataggio = 1;
        next_modem_state = MODEM_ERROR_S;
      }
    }
    if(modem_driver_status == MA_POWER_ON_FAIL)
    {			       
      FlagErroreModemOn = 1;
      FlagTriggerSalvataggio = 1;
      next_modem_state = MODEM_ERROR_S;
    }
    if(modem_driver_status == MA_NETWORK_ERROR)	
    {      
      FlagTriggerSalvataggio = 1;       //triggero il salvataggio pkt non trasmesso a causa di errore rete
      next_modem_state = MODEM_CHIUDI_RETE_S;
    }
  }
  
  modem_state = next_modem_state;       //assegnazione prossimo stato automa (e.g. dopo eventuale completamento di un trigger)
  switch(modem_state)
  {
    //< ----------------- ACCENSIONE MODEM ----------------- >
    case MODEM_START_S:    
      ///////// Scelta tipologia broker MQTT /////////   
      if( Dati.StatoAlesea == ALESEA_COLLAUDO ) 
      {
        if(CounterTxCollaudo <= NUMERO_TX_COLLAUDO_OLD_BROKER) ServerType = SRV_OLD_BROKER;
        else ServerType = SRV_IOT_CENTRAL;
      }
      else
      { 
        if(NumberTxIot == NUMBER_TX_IOT_AZURE)
          ServerType = SRV_IOT_CENTRAL;
        else if(NumberTxIot == NUMBER_TX_IOT_OLD_BROKER)
          ServerType = SRV_OLD_BROKER;
        else
        {
          if(CounterTxIot >= NumberTxIot)     //se NumberTxIot == 0 comunico sempre su broker "vecchio"
          {
            ServerType = SRV_OLD_BROKER;
            CounterTxIot = 0;
          }
          else
          {
            ServerType = SRV_IOT_CENTRAL;
            CounterTxIot++; 
          }
        }
      }
      ////////////////////////////////////////////////

      TempoModemPausa = 0;
      TimerModemPowerOn = 0;
      
      if(VersioneHW == VERSIONE_HW_4_X)
        next_modem_state = MODEM_POWER_ON_S;      
      else /*if(VersioneHW == VERSIONE_HW_DEFAULT_BG96)*/
        next_modem_state = MODEM_ON_S;
      break;
    
    
    case MODEM_POWER_ON_S:      
      HAL_GPIO_WritePin(MDM_ON_OFF_HW4_GPIO_Port, MDM_ON_OFF_HW4_Pin, GPIO_PIN_SET);    //alimento modem
      //Attesa dopo alimentazione modulo
      TimerModemPowerOn++;
      if(TimerModemPowerOn > TEMPO_ATTESA_MODEM_POWER_ON)
      {
        TimerModemPowerOn = 0;
        next_modem_state = MODEM_ON_S;
      }
      break;
    
    
    case MODEM_ON_S:
      modem_driver_active = 1;
      operazione_modem = TRG_MODEM_ON;
      modem_Trigger(operazione_modem, NULL, NULL);
      FlagConsumoGSMOn = 1;
      next_modem_state = MODEM_RESET_S;
      break;
    //----> end ACCENSIONE MODEM
    
    
    //< ----------------- LOCALIZZAZIONE GPS ----------------- >
    case MODEM_RESET_S:
      modem_GET_Type(&Dati.TipoModem);
      if(Dati.TipoModem == MOD_BG95)
      {
        //Eseguo il reset solo su BG95 (fix problema GNSS)
        operazione_modem = TRG_MODEM_ON_RESET;
        modem_Trigger(operazione_modem, NULL, NULL);        
      }
      
#ifdef GPS_ON    
      //forzo il GPS ogni n comunicazioni con n = Dati.GpsFreqForcing      
      switch(Dati.GNSS_data.GpsMode)
      {
        case GPS_MODE_ON://gestione con gps attivo 1 volta su n o a fronte di eventi prestabiliti (o forzatura)
          Dati.GNSS_data.GpsForcingCounter++;
          if( (Dati.GNSS_data.GpsForcingCounter) >= Dati.GNSS_data.GpsFreqForcing )    
          {
            FlagForceGpsOn = 1;       
          }
          if( ((Dati.StatoAlesea == ALESEA_COLLAUDO) && (IndiceTrasmissioneCollaudo == 0)) || (Dati.MovementEvent.MovementSession != 0) || (Dati.SpinEvent.SpinSession != 0 ) || (Dati.ShockEvent.ShockSession != 0) || FlagForceGpsOn )	//abilito GPS solo su prima comunicazione del collaudo e se ho eventi di spin/shock/movimento o se � forzato
          {            
            FlagConsumoGPSOn = 1;
            FlagConsumoGSMOn = 0;
            Dati.GNSS_data.GpsForcingCounter = 0;
            next_modem_state = MODEM_GNSS_ON_S;
          }
          else
          {            
            Dati.GNSS_data.TempoConnessioneGps = 0;	//pulisco la variabile perch� non eseguendo il GPS resta sporca dal dato precedente
            next_modem_state = MODEM_SYNC_RETE_S;
          }
          break;
        
        case GPS_MODE_OFF://GPS forzato off         
          FlagConsumoGPSOn = 0;
          Dati.GNSS_data.GpsForcingCounter = 0;
          Dati.GNSS_data.TempoConnessioneGps = 0;	//pulisco la variabile perch� non eseguendo il GPS resta sporca dal dato precedente
          Dati.Coordinates.gps_latitudine = 0;   
          Dati.Coordinates.gps_longitudine = 0;
          next_modem_state = MODEM_PAUSA_S;
          break;
        
        case GPS_MODE_OFF_TILL_MOVE://GPS eseguito solo una volta a fronte di apertura o chiusura sessione di movimento     
          if( Dati.MovementEvent.MovementSession != 0 )
          {            
            FlagConsumoGPSOn = 1;
            FlagConsumoGSMOn = 0;
            next_modem_state = MODEM_GNSS_ON_S;
          }
          else
          {            
            FlagConsumoGPSOn = 0;
            Dati.GNSS_data.TempoConnessioneGps = 0;	//pulisco la variabile perch� non eseguendo il GPS resta sporca dal dato precedente
            Dati.Coordinates.gps_latitudine = 0;   
            Dati.Coordinates.gps_longitudine = 0;
            next_modem_state = MODEM_PAUSA_S;
          }
          Dati.GNSS_data.GpsForcingCounter = 0;
          break;
      }     
#else		
      next_modem_state = MODEM_SYNC_RETE_S;
#endif
      break;    
    
      
    case MODEM_GNSS_ON_S:    
      GpsData[0] = GNSS_STD;            //modalit� GPS (CLASSICO vs ASSISTITO [non gestito al momento])
      
      memcpy((uint8_t*)&GpsData[1], (uint8_t*)&Dati.GNSS_data.GpsTimeout, sizeof(Dati.GNSS_data.GpsTimeout));   //timeout GPS (GpsData[1][2])   variabile a 32 bit, i 2 byte pi� significativi verranno sovrascritti perch� inutilizzati
      
      if(Dati.GNSS_data.GpsPro == 1)    //precisione GPS (STANDARD vs PRO)
      {
        GpsData[3] = GNSS_PRO_PREC;
        GpsData[4] = Dati.GNSS_data.GpsProTime; 
      }
      else GpsData[3] = GNSS_STD_PREC;
      
      if(VersioneHW == VERSIONE_HW_4_X)
        HAL_GPIO_WritePin(GPS_AMPLI_HW4_GPIO_Port, GPS_AMPLI_HW4_Pin, GPIO_PIN_SET);	//accensione Ampli in ingresso fase GPS per HW4
      else
        HAL_GPIO_WritePin(GPS_AMPLI_HW3_GPIO_Port, GPS_AMPLI_HW3_Pin, GPIO_PIN_SET);	//accensione Ampli in ingresso fase GPS per HW3    
      
      operazione_modem = TRG_GNSS_ON;
      modem_Trigger(operazione_modem, &GpsData[0], sizeof(GpsData));      
      next_modem_state = MODEM_GET_INFO_GPS_S;
      break;
    
      
    case MODEM_GET_INFO_GPS_S:
      if(modem_GET_TempoGNSS(&TempoAcqGpsTemp, &GpsRegTemp) == MA_OK)
      {
        //tempo aggiornato: copio nella struttura
        Dati.GNSS_data.TempoConnessioneGps = TempoAcqGpsTemp;
      }
      else
      {
        Dati.GNSS_data.TempoConnessioneGps = TempoAcqGpsTemp; //passo comunque il tempo registrato anche se GPS fallito
      }
      if(modem_GET_InfoGNSS(&gsm_latitudineTemp, &gsm_longitudineTemp) == MA_OK)
      {
        //coordinate aggiornate: copio nella struttura
        Dati.Coordinates.gps_latitudine = gsm_latitudineTemp;
        Dati.Coordinates.gps_longitudine = gsm_longitudineTemp;
        FlagForceGpsOn = 0;       //commentando forzo il GPS sempre
        
        if(Dati.GNSS_data.GpsPro && (modem_GET_InfoGNSS_PRO(&gsm_latitudineTemp, &gsm_longitudineTemp) == MA_OK))   //se GPS PRO attivo e dato valido
        {
          //coordinate pro aggiornate: copio nella struttura
          Dati.Coordinates.gpsPro_latitudine = gsm_latitudineTemp;
          Dati.Coordinates.gpsPro_longitudine = gsm_longitudineTemp;
        }
        else
        {
          Dati.Coordinates.gpsPro_latitudine = 0;
          Dati.Coordinates.gpsPro_longitudine = 0;
        } 
      }
      else
      {
        Dati.Coordinates.gps_latitudine = 0; //coordinate non aggiornate: metto a zero
        Dati.Coordinates.gps_longitudine = 0;
        FlagForceGpsOn = 1;        //se le coordinate non sono valide forzo il GPS alla prossima comunicazione
      }
      Dati.GNSS_data.GpsRegTemp = GpsRegTemp;      
     
      next_modem_state = MODEM_GPS_OFF_S;//MODEM_SYNC_RETE_S;
      break;

      
    case MODEM_GPS_OFF_S:
      operazione_modem = TRG_MODEM_OFF;
      modem_Trigger(operazione_modem, NULL, NULL);
      
      if(VersioneHW == VERSIONE_HW_4_X)
        HAL_GPIO_WritePin(GPS_AMPLI_HW4_GPIO_Port, GPS_AMPLI_HW4_Pin, GPIO_PIN_RESET);	//Spegnimento Ampli in uscita fase GPS per HW4
      else
        HAL_GPIO_WritePin(GPS_AMPLI_HW3_GPIO_Port, GPS_AMPLI_HW3_Pin, GPIO_PIN_RESET);	//Spegnimento Ampli in uscita fase GPS per HW3
                  
      FlagConsumoGPSOn = 0;
      if( ((Dati.Coordinates.gps_latitudine == 0) && (Dati.Coordinates.gps_longitudine == 0)) || (Dati.StatoAlesea == ALESEA_COLLAUDO) )
      {
        FlagForceGpsOn = 1;       //forzo il GPS anche sulla successiva comunicazione
      }
      next_modem_state = MODEM_PAUSA_S;
      break;
    
      
    case MODEM_PAUSA_S:
      TempoModemPausa++;
      if(TempoModemPausa > PAUSA_GPS_GSM)
      {        
        TempoModemPausa = 0;
        next_modem_state = MODEM_GSM_ON_S;
      }     
      break;
    //----> end LOCALIZZAZIONE GPS
    
    
    //< ----------------- COMUNICAZIONE MODEM ----------------- >
    case MODEM_GSM_ON_S:
      modem_driver_active = 1;
      operazione_modem = TRG_MODEM_ON;
      modem_Trigger(operazione_modem, NULL, NULL);      
      FlagConsumoGSMOn = 1;
      next_modem_state = MODEM_SYNC_RETE_S;     
      break; 
    
    
    case MODEM_SYNC_RETE_S:
      AggiornaModemType();
      AleseaICCID.SizeOfICCID = sizeof(AleseaICCID.ICCID) - 1;
      modem_GET_ICCID(AleseaICCID.ICCID , &AleseaICCID.SizeOfICCID);
      AleseaICCID.ICCID [sizeof(AleseaICCID.ICCID) - 1] = 0;              //add terminatore stringa
      if(Dati.StatoAlesea == ALESEA_COLLAUDO) //ATTENZIONE!! spostare indirizzo STATO_ALESEA perch� ICCID ora � 21 e on 20  
      {
        for(uint8_t i=0; i<sizeof(AleseaICCID.ICCID )-1; i++)
        {
          EEPROM_WriteByte(EEPROM_ICCID + i, AleseaICCID.ICCID[i]);
        }
      }
      modem_GET_InfoFW(sizeof(ModemVersioneFW), ModemVersioneFW, &dummy_u16);
      
      operazione_modem = TRG_SYNC_RETE;
      modem_Trigger(operazione_modem, NULL, NULL);      

      if(ServerType == SRV_OLD_BROKER)
        next_modem_state = MODEM_CONFIGURA_RETE_S;
      else
        next_modem_state = MODEM_REGISTRAZIONE_DEVICE_S;
      break;	
    

    case MODEM_REGISTRAZIONE_DEVICE_S:
      if(modem_Trigger(TRG_REGISTRAZIONE_DEV, &ServerType, 1) == MA_TRIG_OK)
      {
        operazione_modem = TRG_REGISTRAZIONE_DEV;        
      }
      next_modem_state = MODEM_CONFIGURA_RETE_S;
      break;
    
    
    case MODEM_CONFIGURA_RETE_S:
      if(modem_Trigger(TRG_CONNETTI_MQTT, &ServerType, 1) == MA_TRIG_OK)
      {
        operazione_modem = TRG_CONNETTI_MQTT;
      }
      next_modem_state = MODEM_GET_PARAMETRI_GSM_S;
      break;
    
      
    case MODEM_GET_PARAMETRI_GSM_S:     
      Refresh_NetworkData();
      
      if(modem_GET_InfoCella(0, CellaServente.TrasmissionChannel, &CellaServente.MMC, &CellaServente.MNC, &CellaServente.CID, &CellaServente.LAC_TAC, &CellaServente.Rssi) == MA_OK) 
      {
        CellaServente.DatoPresente = 1; 
      }
      else 
      {
        memset((uint8_t*)&CellaServente, 0, sizeof(CellaServente));
        CellaServente.DatoPresente = 0; 
      }
      Dati.NetworkCell.MMC = CellaServente.MMC;
      Dati.NetworkCell.MNC = CellaServente.MNC;
      Dati.NetworkCell.CID = CellaServente.CID;
      Dati.NetworkCell.LAC = CellaServente.LAC_TAC;
      memcpy(Dati.NetworkPerformance.TrasmissionChannel, CellaServente.TrasmissionChannel, sizeof(Dati.NetworkPerformance.TrasmissionChannel));
      
      if(modem_GET_InfoCella(0, CellaServente.TrasmissionChannel, &CellaServente.MMC, &CellaServente.MNC, &CellaServente.CID, &CellaServente.LAC_TAC, &CellaServente.Rssi) == MA_OK) CellaServente.DatoPresente = 1;
      
      if(modem_GET_InfoCella(1, CellaVicina1.TrasmissionChannel, &CellaVicina1.MMC, &CellaVicina1.MNC, &CellaVicina1.CID, &CellaVicina1.LAC_TAC, &CellaVicina1.Rssi) == MA_OK) CellaVicina1.DatoPresente = 1;
      else CellaVicina1.DatoPresente = 0;
      
      if(modem_GET_InfoCella(2, CellaVicina2.TrasmissionChannel, &CellaVicina2.MMC, &CellaVicina2.MNC, &CellaVicina2.CID, &CellaVicina2.LAC_TAC, &CellaVicina2.Rssi) == MA_OK) CellaVicina2.DatoPresente = 1;
      else CellaVicina2.DatoPresente = 0;
      
      if(modem_GET_InfoCella(3, CellaVicina3.TrasmissionChannel, &CellaVicina3.MMC, &CellaVicina3.MNC, &CellaVicina3.CID, &CellaVicina3.LAC_TAC, &CellaVicina3.Rssi) == MA_OK) CellaVicina3.DatoPresente = 1;
      else CellaVicina3.DatoPresente = 0;
      
      
      if(FlagNoRxMQTT)	//evito di ricevere comandi dal portale se il flag � attivo (attivazione flag su specifici comandi da app)
      {        
        if(Dati.StatoAlesea != ALESEA_COLLAUDO) FlagNoRxMQTT = 0;
        next_modem_state = MODEM_GESTIONE_STRUTTURE_SALVATE_S;
      }
      else	
      {
        next_modem_state = MODEM_RICEVI_MQTT_S;
      }
      break;
    
    
    case MODEM_GESTIONE_STRUTTURE_SALVATE_S:      
      if(FlagPrimoInvioEffettuato)              //FlagPrimoInvioEffettuato = 1 => trasmissione rt andata a buon fine
      {
        IndiceDatoLetto = (IndiceDatoLetto + 1)%NUM_MAX_RECORD;
        FlagBufferFull = 0;
      }
      if( ( IndiceDatoLetto != IndiceDatoSalvato ) || ( FlagBufferFull == 1 ) )	//invio se indici diversi oppure buffer pieno
      {		
        LetturaStrutturaDati(IndiceDatoLetto);
        DatiLetti.NetworkPerformance.Retrasmission = 1;    
        
        GeneraStringa(&DatiLetti, 1, PKT_SRC_MODEM);
        FlagInvioRam = 0;
        next_modem_state = MODEM_INVIA_MQTT_S;          //compongo JSON	
      }								
      else
      {
        DatiLetti.NetworkPerformance.Retrasmission = 0;
        Dati.NetworkPerformance.RegAzure = azure_reg_ext.WORD;    //aggiorno diagnostica Azure (estesa) attuale
        GeneraStringa(&Dati, 0, PKT_SRC_MODEM);
        FlagInvioRam = 1;
        next_modem_state = MODEM_INVIA_MQTT_S;
      }
      break;
    
      
    case MODEM_INVIA_MQTT_S:
      if(modem_Trigger(TRG_INVIA_MQTT, DatoJson, DatoJsonDim) == MA_TRIG_OK)
      {
        operazione_modem = TRG_INVIA_MQTT;
        FlagPrimoInvioEffettuato = 1;        
        if(FlagInvioRam)  //non azzerato perch� superfluo (gestione sopra in GESTIONE_STRUTTURE_SALVATE)
          next_modem_state = MODEM_CHIUDI_RETE_S;
        else
          next_modem_state = MODEM_GESTIONE_STRUTTURE_SALVATE_S;
      }
      else
      {     
        next_modem_state = MODEM_CHIUDI_RETE_S;
      }
      break;
    
      
    case MODEM_RICEVI_MQTT_S:	
      if(modem_Trigger(TRG_RICEVI_MQTT, NULL, NULL) == MA_TRIG_OK)
      {
        operazione_modem = TRG_RICEVI_MQTT;
        next_modem_state = MODEM_GET_MQTT_S;
      }
      else
      {
        //Procedo con lo stato successivo:
        //con l'introduzione di DPS � possibile che il trigger TRG_RICEVI_MQTT produca un errore (servizio settato in maniera errata)
        next_modem_state = MODEM_GET_MQTT_S;
      }
      break;
    
    
    case MODEM_GET_MQTT_S:
      if(ServerType == SRV_OLD_BROKER)  
      {
        //Connessione verso i servizi legacy: eseguo gestione dato RX
        if(modem_GET_MsgMQTT(DatoJsonRx, &DatoJsonRxDim) == MA_OK)
        {
          GestioneDatoJsonRx();
          PulisciDatoJsonRx();
        }
        else
        {
          PulisciDatoJsonRx();
        }
        
        if(++retry_subscribe < MAX_RETRY_SUB)
        {
          next_modem_state = MODEM_RICEVI_MQTT_S;				
        }
        else
        {
          retry_subscribe = 0;
          next_modem_state = MODEM_GESTIONE_STRUTTURE_SALVATE_S;
        }
      }
      else
      {
        //Connessione verso i servizi Azure: gestione dato RX interno alla libreria Azure
        next_modem_state = MODEM_GESTIONE_STRUTTURE_SALVATE_S;
      }
      break;
    
    
    case MODEM_FW_UPGRADE_S:
      if(modem_Trigger(TRG_FW_UPDATE, UpgradeDataRX.BUFFER, sizeof(UpgradeDataRX.BUFFER)) == MA_TRIG_OK)
      {
        operazione_modem = TRG_FW_UPDATE;
        next_modem_state = MODEM_GET_FW_UPGRADE_S;        
      }
      else
      {
        next_modem_state = MODEM_CHIUDI_RETE_S;
      }
      break;
    
    
    case MODEM_GET_FW_UPGRADE_S:
      if(modem_GET_Status(TRG_FW_UPDATE) == MA_OK)
      {
        FlagUpgradeSuccess = 1;
      }
      else
      {
        FlagUpgradeSuccess = 0;
      }
      next_modem_state = MODEM_CHIUDI_RETE_S;//MODEM_OFF_S;
      break;
    
    
    case MODEM_CHIUDI_RETE_S:
      operazione_modem = TRG_CHIUDI_RETE;
      if(AggiornamentoFwPendente == 1) 
      {        
        AggiornamentoFwPendente = 0;
        next_modem_state = MODEM_FW_UPGRADE_S;
      }
      else
      {
        modem_Trigger(operazione_modem, NULL, NULL);      
        if(FlagSetSetGnssSatelliti) 
        {          
          FlagSetSetGnssSatelliti= 0;
          next_modem_state = MODEM_IMPOSTAZIONI_S;
        }
        else 
          next_modem_state = MODEM_OFF_S;
      }
      break;

      
    case MODEM_IMPOSTAZIONI_S:
      operazione_modem = TRG_GP_SETTINGS;
      modem_Trigger(operazione_modem, (uint8_t*)&Dati.GNSS_data.GnssSatelliti, 1);      
      next_modem_state = MODEM_OFF_S;
      break;
    
      
    case MODEM_OFF_S:
      operazione_modem = TRG_MODEM_OFF;
      modem_Trigger(operazione_modem, NULL, NULL);      
      next_modem_state = MODEM_END_S;
      break;
      
    
    case MODEM_ERROR_S:    
      modem_Trigger(TRG_MODEM_OFF, NULL, NULL);	//non sapendo se il modem stia funzionando correttamente do il comando senza alcuna verifica...sensato???
      if(FlagErroreModemOn)
      {
        FlagErroreModemOn = 0;
        if(Diagnostica.CountErroreModemOn < 0xFF) 
        {
          Diagnostica.CountErroreModemOn++;
          EEPROM_WriteByte(EEPROM_CONTA_FAIL_MODEM_ON, Diagnostica.CountErroreModemOn);
        }
      }
      next_modem_state = MODEM_END_S;    
      break;
    //----> end COMUNICAZIONE MODEM
    
    
    //< ----------------- SPEGNIMENTO MODEM ----------------- >
    case MODEM_END_S:    
      SalvataggioStruttura();
          
      if(VersioneHW == VERSIONE_HW_4_X) 
      {
        HAL_GPIO_WritePin(MDM_ON_OFF_HW4_GPIO_Port, MDM_ON_OFF_HW4_Pin, GPIO_PIN_RESET);        //disalimento il modem
        HAL_GPIO_WritePin(BOOST_EN_HW4_GPIO_Port, BOOST_EN_HW4_Pin, GPIO_PIN_RESET);            //spegnimento switching
        HAL_GPIO_WritePin(GPS_AMPLI_HW4_GPIO_Port, GPS_AMPLI_HW4_Pin, GPIO_PIN_RESET);	        //spegnimento Ampli di sicurezza HW4
      }
      else
      {
        HAL_GPIO_WritePin(GPS_AMPLI_HW3_GPIO_Port, GPS_AMPLI_HW3_Pin, GPIO_PIN_RESET);	        //spegnimento Ampli di sicurezza per HW3
      }
      
      FlagErroreModemOn = 0;   
      FlagPrimoInvioEffettuato = 0;
      modem_driver_active = 0;
      FlagConsumoGPSOn = 0;
      FlagConsumoGSMOn = 0;
      FlagInvioRam = 0;      
      Count_MA_UNP_ERROR = 0;

      next_modem_state = MODEM_END_S;
      break;
    //----> end SPEGNIMENTO MODEM

    default: next_modem_state=MODEM_END_S; break;
  }  
  
  if(modem_driver_active==0 && modem_state==MODEM_END_S)
    return MODEM_SM_ENDED;              //esecuzione automa conclusa
  else
    return MODEM_SM_IN_PROGRESS;        //esecuzione automa in corso
}


/* Sincronizzazione dati livello applicativo con dati modem (relativi alla rete). */
void Refresh_NetworkData(void)
{
  //Tempi registrazione/connessione + diagnostica modem
  modem_GET_TempoRete(&Dati.NetworkPerformance.TempoRegistrazione, &Dati.NetworkPerformance.TempoConnessione, &Dati.NetworkPerformance.RegRete, &Dati.NetworkPerformance.LastError);
  //RSSI
  modem_GET_RSSI(&Dati.NetworkPerformance.RSSI);
  if(Dati.NetworkPerformance.RSSI == 99) Dati.NetworkPerformance.RSSI = 0;  //99 = dato no valido -> viene sostituito con lo 0  12/11/2021  FW 4.18
  //Data/ora
  Dati.AleseaDate.SizeOfDataOra = 0;
  modem_GET_DataOra(Dati.AleseaDate.DataOra, &Dati.AleseaDate.SizeOfDataOra);
  //Diagnostica Azure (estesa)
  Dati.NetworkPerformance.RegAzure = azure_reg_ext.WORD;
  //Diagnostica MQTT
  modem_GET_Diagnostic(M_DNG_MQTT_ERR_RESULT, &Dati.NetworkPerformance.ErroreMqtt[0]);
  modem_GET_Diagnostic(M_DNG_MQTT_ERR_RETCODE, &Dati.NetworkPerformance.ErroreMqtt[1]);
  //Diagnostica WiFi
  wifi_GET_Diagnostic(&Dati.WiFiPerformance.RegRete, &Dati.WiFiPerformance.LastError);
}


/* Salvataggio struttura dati per ritrasmissioni. */
void SalvataggioStruttura(void)
{
  if(FlagTriggerSalvataggio)    //se triggerato salvo (il trigger avviene su errore modem)
  {
    FlagTriggerSalvataggio = 0;
    
    Refresh_NetworkData();                                      //aggiornamento dati modem prima di salvataggio (in caso di errore potrei non essere passato da GET_PARAMETRI_GSM)
    if( SalvataggioStrutturaDati(IndiceDatoSalvato) == 0 )      //salvataggio in EEPROM della struttura dati
    {
      if(Diagnostica.CounterErrorEeprom < 255)
        Diagnostica.CounterErrorEeprom++;
      EEPROM_WriteByte( EEPROM_ERROR_EEPROM, Diagnostica.CounterErrorEeprom );  
    } 
    if(FlagBufferFull == 1)   
    {
      FlagBufferFull = 0;
      IndiceDatoLetto = (IndiceDatoLetto + 1)%NUM_MAX_RECORD;           //se ero gi� in overflow e non sono riuscito a trasmettere rt il pkt � perso => incremento l'indice dato letto
    }
    IndiceDatoSalvato = (IndiceDatoSalvato + 1)%NUM_MAX_RECORD;         //incremento indice dato salvato                            
    
    if(IndiceDatoSalvato == IndiceDatoLetto)    FlagBufferFull = 1;     //verifica overflow
  }
}
