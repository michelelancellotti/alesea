#include "LSM6DSL.h"
#include "LSM6DSL_driver.h"
#include "Gestione_batteria.h"
#include "LSM6DSL_SM.h"
#include "lis2mdl_SM.h"
#include "EEPROM.h"
#include "Gestione_dato.h"
#include "communication_SM.h"
#include "stm32l0xx_hal.h"

#include "main.h"

AleseaMacchinaStatiMems StatoMEMS;
AleseaMacchinaStatiMovimento StatoMovimento;
uint8_t FlagWakeupDaMEMS;
uint8_t FlagRefreshRotation;
uint8_t FlagWakeUpDaShock;
uint8_t FlagWakeUpDaMovimento;
uint8_t FlagWakeUpDaFreeFall;
uint8_t FlagWakeupPerTemp;
float AccelerazioneZ;
float AccelerazioneMediaZ;
uint8_t ContaSampleZ;
uint16_t NumCampioniValidi = 0;
float NumeroGiriGyrFloat;
float NumeroGiriDefFloat;
float NumeroGiriFloatFase;
float TempoGiroGyrFloat;
float VelGyrMediaRpm;

uint8_t FlagCommOnInSpinSession;
uint8_t FlagSpinSessionStart;                                           
uint16_t SpinTimeCounter;
uint16_t SubSessionDuration;
uint16_t SpinStartStopCounterAppoggio;
uint8_t SubSessionCounter;

uint8_t FlagCommOnInShockSession;
uint8_t FlagCommOnInMovementSession;
uint8_t FlagCommOnInSpinSessionError;
uint8_t FlagShockSessionStart;   
uint8_t FlagMovementSessionStart;
uint8_t SpinTimeCounterEnable;
uint8_t StartSubSession;

float mdps, mdps_sum, mdps_mean, dps_mean, rps_mean;
uint8_t VelAngolareAppCounter; 


int8_t ArrayTemperature[ARRAY_TEMPERATURE_DIM];
uint8_t ArrayTemperaturaIndex;
uint8_t FlagMagnetometroOn;

float NumeroGiriAccFloat;

mg_t mgx, mgy;
//float mgx, mgx_sum, mgx.mean;
//float mgy, mgy_sum, mgy.mean;
uint16_t AsseXAccAppCounter;
uint8_t MesureMode;

uint8_t Quadrante, QuadranteOld;

uint8_t TriggerOrientamento;
uint16_t ContaInterruptRotazione;
uint8_t SpinTimeCounter_appoggio;


////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////MACCHINA A STATI ACCELEROMETRO////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
void MacchinaStatiMEMS(void)
{
  
  switch(StatoMEMS)
  {
  case MEMS_INIT_CONTA_GIRI:
    FlagConsumoMEMSOn = 1;
    SetLSM6DSL(ACC_ACTIVE);
    #ifdef FREE_FALL_ON
      SetInterruptFreeFall(FALL_DUR_RUN_MODE); 
    #endif
    SetLSM6DSL(GYR_ON);
    MesureMode = QUADRANTI;
    
    mdps_sum = 0;
    VelAngolareAppCounter = 0;
    if(Dati.SpinEvent.SpinSession == 0)
    {
      TotGiriGyr = 0;
      TotGiriAcc = 0;
      NumeroGiriMagnetometro = 0; 
      //NumeroGiriMagnetometro_no_filtro = 0;
    }
    TriggerOrientamento = 1;
    StatoMEMS = MEMS_ORIENTAMENTO;//MEMS_CONTA_GIRI;
    break;
    
  case MEMS_ORIENTAMENTO:
   
    if(TriggerOrientamento == 1)        return;
    
    if( (Dati.TiltEvent.Orientamento == FLANGIA_A_TERRA) && ( Dati.SpinEvent.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON ) ) //rotazione orizzontale
      LSM6DSL_interrupt_timer = FILTRO_ROTAZ_ORIZZ_FERMO_DEFAULT*100;
    else
      LSM6DSL_interrupt_timer = Dati.MovementEvent.FiltroMovimento*100;
    StatoMEMS = MEMS_CONTA_GIRI;
    
    break;
    
  case MEMS_CONTA_GIRI:
    LSM6DSL_cycle();
    
    mdps = angular_rate_mdps[2];
    mdps_sum += mdps;
    VelAngolareAppCounter++;
    
    if(VelAngolareAppCounter >= N_CAMPIONI_V_ANG)     //esecuzione ogni 1 secondo
    {
      VelAngolareAppCounter = 0;
      mdps_mean = mdps_sum/N_CAMPIONI_V_ANG;
      mdps_sum = 0;
      mdps = 0;
      dps_mean = mdps_mean;    
      rps_mean = dps_mean/360;
      NumeroGiriGyrFloat += rps_mean;
      
    }

    mgx.val = acceleration_mg[0];
    if(mgx.val >= QUADRANTE_ISTERESI) mgx.sign = 1;
      else if(mgx.val < -QUADRANTE_ISTERESI) mgx.sign = -1;
    mgx.sum += mgx.sign;
    mgy.val = acceleration_mg[1];
     if(mgy.val >= 0) mgy.sign = 1;
      else mgy.sign = -1;
    mgy.sum += mgy.sign;
    AsseXAccAppCounter++;
    if(AsseXAccAppCounter >= N_CAMPIONI_ACC)
    {
      AsseXAccAppCounter = 0;
      mgx.mean = mgx.sum/N_CAMPIONI_ACC;
      mgx.sum = 0;
      mgx.val = 0;
      mgy.mean = mgy.sum/N_CAMPIONI_ACC;
      mgy.sum = 0;
      mgy.val = 0;
      
      
      if( 
           (((FlagRefreshRotation == 1) || (Quadrante != QuadranteOld))  ) ||
             (( (rps_mean > 0.016) || (rps_mean < -0.016) ) && (Dati.SpinEvent.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON)) //filtro settato su 0.016 rps => 1rpm  
                     
          )   //riconoscimento stato movimento/fermo      
      {  
        FlagRefreshRotation = 0;
        
        QuadranteOld = Quadrante;

        if( (Dati.TiltEvent.Orientamento == FLANGIA_A_TERRA) && (Dati.SpinEvent.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON) )  //rotazione orizzontale
          LSM6DSL_interrupt_timer = FILTRO_ROTAZ_ORIZZ_FERMO_DEFAULT*100;
        else
          LSM6DSL_interrupt_timer = Dati.MovementEvent.FiltroMovimento*100;
        
        
        
        ///////////////////////////////////START SESSIONE SPIN////////////////////////////////////////////
        //inizio della sessione di rotazione su condizione che non sia ancora iniziata e che sia stato compiuto almeno un giro
        if( /*(Dati.SpinEvent.SpinSession == 0) && */( (NumeroGiriDefFloat < -1) || (NumeroGiriDefFloat > 1) ) )
        {
          if(Dati.SpinEvent.SpinSession == 0)
          {
            FlagCommOnInSpinSession = 1;  //inizia la sessione di spin => start macchina a stati comunicazione
            FlagNoRxMQTT = 1;             //inibisco la ricezione sulla coda MQTT
            TriggerOrientamento = 1;
            Dati.SpinEvent.SpinSession = 1;
            Dati.SpinEvent.SpinFreq = 0;
          }
          else if( (Dati.SpinEvent.SpinSession == 1) && (StartSubSession == 0) )
          {
            StartSubSession = 1;
            SpinTimeCounterEnable = 1;            //30/07/2024 spostato su effettivo inizio sessione di rotazione (1 giro compiuto)  
            SpinStartStopCounterAppoggio++;       //30/07/2024 spostato su effettivo inizio sessione di rotazione (1 giro compiuto)
            SubSessionCounter++;
          }
          //if(NumeroGiriDefFloat >= 1)        Dati.SpinEvent.NumeroGiriACw++;//Aggiunto 1 giro                 //rimossa somma +1 21/11/2023
          //else if(NumeroGiriDefFloat <= -1)	Dati.SpinEvent.NumeroGiriCw++;//Aggiunto 1 giro
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////
      } 
      switch(Quadrante)
      {
      case 0:
        if( (mgx.mean > 0) && (mgy.mean > 0) ) Quadrante = 0;
        else if( (mgx.mean > 0) && (mgy.mean < 0) ) { Quadrante = 1; NumeroGiriAccFloat += 0.25; }
        else if( (mgx.mean < 0) && (mgy.mean < 0) ) Quadrante = 2;
        else if( (mgx.mean < 0) && (mgy.mean > 0) ) { Quadrante = 3; NumeroGiriAccFloat -= 0.25; }
        break;
        
      case 1:
        
        if( (mgx.mean > 0) && (mgy.mean > 0) ) { Quadrante = 0; NumeroGiriAccFloat -= 0.25; }
        else if( (mgx.mean > 0) && (mgy.mean < 0) ) Quadrante = 1;
        else if( (mgx.mean < 0) && (mgy.mean < 0) ) { Quadrante = 2; NumeroGiriAccFloat += 0.25; }
        else if( (mgx.mean < 0) && (mgy.mean > 0) ) Quadrante = 3;
        break;
        
      case 2:
        
        if( (mgx.mean > 0) && (mgy.mean > 0) ) Quadrante = 0;
        else if( (mgx.mean > 0) && (mgy.mean < 0) ) { Quadrante = 1; NumeroGiriAccFloat -= 0.25; }
        else if( (mgx.mean < 0) && (mgy.mean < 0) ) Quadrante = 2;
        else if( (mgx.mean < 0) && (mgy.mean > 0) ) { Quadrante = 3; NumeroGiriAccFloat += 0.25; }
        break;
        
      case 3:
        
        if( (mgx.mean > 0) && (mgy.mean > 0) ) { Quadrante = 0; NumeroGiriAccFloat += 0.25; }
        else if( (mgx.mean > 0) && (mgy.mean < 0) ) Quadrante = 1;
        else if( (mgx.mean < 0) && (mgy.mean < 0) ) { Quadrante = 2; NumeroGiriAccFloat -= 0.25; }
        else if( (mgx.mean < 0) && (mgy.mean > 0) ) Quadrante = 3;
        break;
      }
    }
    ///////////////////
   
    //test gyro
    TotGiriGyr += NumeroGiriGyrFloat; 
    TotGiriAcc += NumeroGiriAccFloat;

    
    if( (Dati.TiltEvent.Orientamento == FLANGIA_A_TERRA) && (Dati.SpinEvent.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON) )  //rotazione orizzontale abilitata e flangia a terra
    {    
      NumeroGiriDefFloat += NumeroGiriGyrFloat;     
      if(VersioneHW == VERSIONE_HW_4_X) FlagMagnetometroOn = 1;
    }
    else        //se flangia perpendicolare a terra
    {
      NumeroGiriDefFloat += NumeroGiriAccFloat;
    }
    
    
    NumeroGiriGyrFloat = 0;
    NumeroGiriAccFloat = 0;
    ////////////////////
    
    
    
    if(LSM6DSL_interrupt_timer > 0)
    {
      LSM6DSL_interrupt_timer--;		
    }
    
    
    if( (SubSessionCounter > MAX_SUBSESSION_NUMBER) || (SubSessionDuration > MAX_TIME_SPIN_SESSION) )    //forzo chiusura spin session e trasmissione 
    {
      Dati.SpinEvent.SpinSession = 2;
      SubSessionCounter = 0;
      FlagCommOnInSpinSessionError = 1;
      //se � terminata la rotazione termino anche il movimento generato in concomitanza
      if(Dati.MovementEvent.MovementSession == 1)
          Dati.MovementEvent.MovementSession = 2;
      
      FlagWakeupDaRTC = 1;  //chiudo la sessione di spin => start macchina a stati comunicazione
      FlagCommOnInSpinSession = 0; //forzato a zero (viene normalmente utilizzato per dare start a tx e schedulare nuova sveglia)
      LSM6DSL_interrupt_timer = 0;
      
      //replica dell'operazione fatta nella callback per allineare le due casistiche di chiusura sessione (naturale e forzata)
      Dati.SpinEvent.SpinStartStopCounter = SpinStartStopCounterAppoggio; //numero sottosessioni di spin tra la prima (conclusa) e il risveglio da RTC schedulato a 3600 secondi dopo l'ultima rotazione
      SpinStartStopCounterAppoggio = 0;
    }

    
    if( LSM6DSL_interrupt_timer == 0 )
    {    
      StatoMEMS = MEMS_SALVA_GIRI;
      SpinTimeCounterEnable = 0;
      FlagMagnetometroOn = 0;
      StartSubSession = 0;
      SubSessionDuration = 0;
    }		
    break;
    
  case MEMS_SALVA_GIRI:	//salvo la rotazione compiuta nell'opportuna variabile. 

    //se la rotazione � orizzontale e la velocit� angolare � sopra soglia sovrascrivo con il dato magnetometro
    ///////////calcolo la velocit� media solo se la vel ist � > di 0.016 rps = 1 rpm/////////
    //discrimino sul maggiore tra il val del giroscopio e quello del magnetometro poi allineao i segni
    
    if( (VersioneHW == VERSIONE_HW_4_X) && (Dati.SpinEvent.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON) && (Dati.TiltEvent.Orientamento == FLANGIA_A_TERRA) )
    {
      //uso il magnetometro indipendentemente dal fatto che il giroscopio abbia contato un numero > di rotazioni        26/09/23
      NumeroGiriDefFloat = (float)NumeroGiriMagnetometro_2;
      Dati.SpinEvent.SpinMag += (uint32_t)NumeroGiriMagnetometro_2;
    }
    
    if(NumeroGiriDefFloat >= 1)	//solo se ha compiuto giri memorizzo
    {
      Dati.SpinEvent.NumeroGiriACw += NumeroGiriDefFloat;     
    }
    else if(NumeroGiriDefFloat <= -1)	//solo se ha compiuto giri memorizzo
    {
      NumeroGiriDefFloat = -1*NumeroGiriDefFloat;
      Dati.SpinEvent.NumeroGiriCw += NumeroGiriDefFloat;     

    }
    
    //if(Dati.AleseaMeasure.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON) FlagWakeupPerTemp = 1;
    NumeroGiriFloatFase += NumeroGiriDefFloat;
    
    NumeroGiriDefFloat = 0;
    NumeroGiriGyrFloat = 0;
    //NumeroGiriMagnetometro = 0;
    NumeroGiriMagnetometro_2 = 0;
    dato_mag_old = 0;
    Count_derivata_diff = 0;
    //Compass = 0;
    Magnetometro_dato_medio = 0;
    NumeroGiriAccFloat = 0;
    VelGyrMediaRpm = 0;
    TempoGiroGyrFloat = 0;
    
    EEPROM_WriteWord( EEPROM_NUMERO_GIRI_TOT_CW, (uint32_t)(Dati.SpinEvent.NumeroGiriCw + 0.5));
    EEPROM_WriteWord( EEPROM_NUMERO_GIRI_TOT_ACW, (uint32_t)(Dati.SpinEvent.NumeroGiriACw + 0.5));
    StatoMEMS = MEMS_GO_TO_STOP_MODE;
    break;
    
  case MEMS_GO_TO_STOP_MODE:
    StatoMEMS = MEMS_INIT_CONTA_GIRI;
    SetLSM6DSL(ACC_LOW_POWER);
    #ifdef FREE_FALL_ON
      SetInterruptFreeFall(FALL_DUR_STOP_MODE);  
    #endif
    FlagWakeupDaMEMS = 0;
    FlagConsumoMEMSOn = 0;
    break;
  }
}


void MacchinaStatiMovimento(void)
{
  switch(StatoMovimento)
  {
    case MOVE_INIT:
      Dati.MovementEvent.MovementSession = 1;
      FlagCommOnInMovementSession = 1;
      //FlagNoRxMQTT = 1;                //inibisco la ricezione sulla coda MQTT
      if(Dati.SpinEvent.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_OFF)      //se abilitata la rotazione orizzontale escludo il cambio scala per la gestione shock
      {
        SogliaAccelerometro = Dati.ShockEvent.SogliaShock;
        SetLSM6DSL(ACC_SOGLIA);//SogliaAccelerometroSet(SogliaAccelerometro);
      }
      StatoMovimento = MOVE_RUN;
      break;
      
    case MOVE_RUN:
      if(FlagCommOnInMovementSession == 0)    StatoMovimento = MOVE_STOP;
      break;
      
    case MOVE_STOP:
      FlagWakeUpDaMovimento = 0;
      StatoMovimento = MOVE_INIT;
      break;
  }
  
}


////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////MACCHINA A STATI FREE FALL/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
void MacchinaStatiFreeFall(void)
{
  Dati.FreeFallEvent.ContaFreeFall++;
  EEPROM_WriteByte(EEPROM_CONTA_FREE_FALL, Dati.FreeFallEvent.ContaFreeFall);
  FlagWakeUpDaFreeFall = 0;	
}


////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////MACCHINA A STATI SHOCK/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
void MacchinaStatiShock(void)
{
  Dati.ShockEvent.ContaShock++;
  EEPROM_WriteWord(EEPROM_CONTA_SHOCK, Dati.ShockEvent.ContaShock);
  FlagWakeUpDaShock = 0;	
  if(Dati.ShockEvent.ShockSession == 0) //avvio la comunicazione solo se � il primo evento di shock della serie
  {
    FlagCommOnInShockSession = 1;       //inizia la sessione di spin => start macchina a stati comunicazione
    //FlagShockSessionStart = 1;        //tengo traccia dell'inizio della sessione di spin
    //FlagNoRxMQTT = 1;                 //inibisco la ricezione sulla coda MQTT
    Dati.ShockEvent.ShockSession = 1;
  }
}

////////////////////////////////////////////////////////////////////////
////////////////RICAVA ORIENTAMENTO DELLA BOBINA////////////////////////
////////////////////////////////////////////////////////////////////////
uint16_t num_campioni_validi_dbg;
uint8_t GetOrientamento(uint16_t num_campioni)
{
  uint8_t result;
  float appoggio;
  // float appoggio;
  Dati.TiltEvent.Orientamento = FLANGIA_NO_RESULT; 
  appoggio = LSM6DSL_Orientamento();
  
  if(appoggio != ORIENTAMENTO_DUMMY)     //accumulo solo dati != 0       considero lo 0 non valido
  {
    if(appoggio < 0)   appoggio = -1*appoggio;
    AccelerazioneZ += appoggio;
    NumCampioniValidi++;  
  }
  ContaSampleZ++;
  if(ContaSampleZ == num_campioni)
  {
    if(NumCampioniValidi > 0)
    {
      AccelerazioneMediaZ = AccelerazioneZ/NumCampioniValidi;
      if(AccelerazioneMediaZ < SOGLIA_ORIENTAMENTO)      Dati.TiltEvent.Orientamento = FLANGIA_PERPENDICOLARE_TERRA;
      else Dati.TiltEvent.Orientamento = FLANGIA_A_TERRA;
    }
    num_campioni_validi_dbg = NumCampioniValidi;
    NumCampioniValidi = 0;
    AccelerazioneZ = 0;	
    ContaSampleZ = 0;
    result = 1;
  }
  else result = 0;
  return result;
}


////////////////////////////////////////////////////////////////////////
/////////////////////////LETTURA DELLA TEMPERATURA//////////////////////
////////////////////////////////////////////////////////////////////////

uint8_t GetTemperature(void)
{
  float temperatura_temp;
  
  temperatura_temp = LSM6DSL_Temperatura();		// Misura della temperatura e messa in struttura
 
  if(temperatura_temp == 0xFF)
  {
    return 0;
  }
  else
  {
    Dati.AleseaMeasure.Temperatura = (int8_t)temperatura_temp;
  }

  ArrayTemperature[ArrayTemperaturaIndex] = Dati.AleseaMeasure.Temperatura;
  
  if(ArrayTemperaturaIndex == 0)
  {
    Dati.AleseaMeasure.TemperaturaMax = Dati.AleseaMeasure.Temperatura;
    Dati.AleseaMeasure.TemperaturaMin = Dati.AleseaMeasure.Temperatura;
  } 
  
  if( Dati.AleseaMeasure.Temperatura > Dati.AleseaMeasure.TemperaturaMax )    Dati.AleseaMeasure.TemperaturaMax = Dati.AleseaMeasure.Temperatura;
  if( Dati.AleseaMeasure.Temperatura < Dati.AleseaMeasure.TemperaturaMin )    Dati.AleseaMeasure.TemperaturaMin = Dati.AleseaMeasure.Temperatura;
  ArrayTemperaturaIndex++;
  if(ArrayTemperaturaIndex >= ARRAY_TEMPERATURE_DIM) ArrayTemperaturaIndex = 0;
  return 1;
}

