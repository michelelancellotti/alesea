/**
******************************************************************************
* File          	: softTimer.h
* Versione libreria	: 0.02
* Descrizione        	: Soft timer general-purpose.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#ifndef _SOFTTIMER_H
#define _SOFTTIMER_H


/* Includes */  
#include <stdint.h>


/* Define & Typedef */
#define GP_MSEC_PER_TIC         1         			//TIC == 1 millisecondo
#define TIC_PER_SEC             (1000/GP_MSEC_PER_TIC)
typedef unsigned char (*TMR_IDLEFN)(void);


typedef struct general_purpose_timer_object  
{
  uint32_t lastCount;       //Ultimo conteggio salvato
  uint32_t timeOut;         //Numero tics al timeout
  uint32_t timedOut;        //Time-out status                  
  uint32_t enable;	    //Enable flag
} GPTIMER;


/* Funzioni globali */
void GPTIMER_init(GPTIMER *_this, const uint32_t tics);
void GPTIMER_setTics(GPTIMER *_this, const uint32_t tics);
void GPTIMER_reTrigger(GPTIMER *_this);
int GPTIMER_isTimedOut(GPTIMER *_this);
void GPTIMER_stop(GPTIMER *_this);
//System call-back
uint32_t sysRetrieveClock(void);

extern volatile uint32_t ml_SystemTick;		//Tic libreria

#endif /*_SOFTTIMER_H */
