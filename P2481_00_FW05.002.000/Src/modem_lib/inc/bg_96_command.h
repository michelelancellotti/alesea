/**
******************************************************************************
* File          	: bg_96_command.h
* Versione libreria	: 0.02
* Descrizione        	: Gestore invio comandi modem Quectel BG96/BG95.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BG_96_COMMAND_H
#define __BG_96_COMMAND_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "bg_96_common.h"


  
/* Defines */
#define MAX_BG96_MQTT_SUPPORT_BUFF      400     //[B], dimensione buffer supporto comandi MQTT
  
#if (MAX_BG96_MQTT_SUPPORT_BUFF<(AZ_MAX_SAS_TOKEN+AZ_MAX_USER_NAME))
#error "Parameter error ..."
#endif
  
/* Types & global variables */
typedef union {
  uint8_t file_buff[32];
  uint8_t gp_settings_buff[64];
  uint8_t GNSS_buff[64];  
  uint8_t HTTP_FTP_buff[64];
  uint8_t MQTT_buff[MAX_BG96_MQTT_SUPPORT_BUFF];
} BG96_Buffer_t;
  
/* Global functions */
extern ErrorCode_t bg96_send_command(Mod_Fifo_Entry_t* fifo, uint8_t fifo_index, uint16_t* offset_resp);
  
  
/* Global variables */
  
#ifdef __cplusplus
}
#endif
#endif /*__BG_96_COMMAND_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
