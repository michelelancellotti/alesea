/**
******************************************************************************
* File          	: modem_common.h
* Versione libreria	: 0.02
* Descrizione        	: Invio comandi AT & parsing risposte.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MODEM_COMMON_H
#define __MODEM_COMMON_H
#ifdef __cplusplus
extern "C" {
#endif
  
  
/* Includes ------------------------------------------------------------------*/
#include "at_common.h"  
#include "softTimer.h"  
#include "az_interface.h"
#include "az_client.h"
  
/* Defines */
/*--------------------------------------------------------------------------------
 * Parametri operatori supportati
 *--------------------------------------------------------------------------------
*/  
//Impostazioni variabili in base all'operatore della SIM (APN)
#define APN_ADDR_1NCE           "iot.1nce.net"          //SIM 1nce
#define APN_ADDR_COBIRA         "gigsky-02"             //SIM Cobira
  

#define MOD_MS_PER_TICKS                ((uint16_t)100)         //[ms], moltiplicatore timeout risposte a comandi AT
  
#define MOD_MAX_BUFFER_TOPIC_RX         64      //[B], dimensione buffer appoggio topic su cui viene ricevuto un messaggio
#define MOD_MAX_TOPIC_OUT		160     //[B], dimensione MAX topic publish MQTT
#define MOD_MAX_TOPIC_IN		48      //[B], dimensione MAX topic subscribe MQTT
  
#define MOD_NO_TAILER                   0xFF	//Invio comandi senza terminatore
#define MOD_CTRL_Z_TAILER		0xFE	//Invio comandi con terminatore CTRL_Z
  
#define MOD_ICCID_MAX_SIZE		20	//[B], dimensione ICCD SIM, 19 cifre o 20 cifre (operatore virtuale)
#define MOD_APN_MAX_SIZE                24      //[B], dimensione MAX APN  
#define MOD_MAX_TECH_SIZE               8       //[B], dimensione tecnologia radio  
#define MOD_MCC_MAX_SIZE		3       //[B], dimensione MCC (3 cifre)
#define MOD_MNC_MAX_SIZE		3	//[B], dimensione MNC (3 cifre)
#define MOD_CELL_ID_MAX_SIZE	        9       //[B], dimensione Cell-ID (9 cifre)
#define MOD_LAC_TAC_MAX_SIZE	        5	//[B], dimensione LAC/TAC (5 cifre)
#define MOD_SERVING_CELL                0       //Indice serving-cell
#define MOD_MAX_CELL_NUM                4       //1 serving-cell + 3 neighbour-cell 
  
#if (MOD_MAX_CELL_NUM<1)
#error "Parametro fuori range."
#endif


/*--------------------------------------------------------------------------------
 * Parametri API HTTP (download chiave Azure)
 *--------------------------------------------------------------------------------
*/
#define HTTP_API_HOSTNAME               "alesea.azure-api.net"
#define HTTP_API_URL_ADDR               "https://"HTTP_API_HOSTNAME  

/* Header API HTTP:
 * --> N.B.: ogni riga degli header HTTP DEVE ESSERE terminata con <CR><LF> e l'ultima riga DEVE ESSERE <CR><LF>
 * --> segnaposto %s � per <hostname>
 * --> segnaposto %d � per <content-length>
*/
#if (AZURE_ENVIRONMENT == AZ_DEV)  
#define HTTP_API_HEADER                 \
"POST /iotc/dev/getAleseaDeviceKey HTTP/1.1\r\n\
Ocp-Apim-Subscription-Key: eb3f9cc4feb949a48a2f6ad60495cb80\r\n\
Host: %s\r\n\
Content-Type: application/json\r\n\
Content-Length: %d\r\n\
\r\n"
#else
#define HTTP_API_HEADER                 \
"POST /iotc/v1/getAleseaDeviceKey HTTP/1.1\r\n\
Ocp-Apim-Subscription-Key: 26f21643b5e44641993da246e3ba4d10\r\n\
Host: %s\r\n\
Content-Type: application/json\r\n\
Content-Length: %d\r\n\
\r\n"
#endif
/* Body API HTTP:
 * --> N.B.: se negli header HTTP viene specificato il campo 'Content-Type' NON � necessario terminare l'ultima riga del body con <CR><LF>
 * --> segnaposto %s � per <device-id>
*/
#define HTTP_API_BODY                   "{\"device_id\":\"%s\"}"
    
/* Types & global variables */
typedef enum {
  MOD_BG95      = 0xA5,
  MOD_BG96      = 0xAA,
  MOD_NULL      = 0x01	
} Mod_t;

typedef enum {
  GNSS_STD = 0,		        //Opzione modalit� GNSS standard
  GNSS_ASS = 1,		        //Opzione modalit� GNSS assistito
  GNSS_STD_PREC = 2,            //Opzione modalit� precisione standard
  GNSS_PRO_PREC = 3,	        //Opzione modalit� precisione aumentata
  GNSS_NUM
} Opt_GNSS_t;

typedef enum {
  UPG_NULL = 0,
  UPG_USER = 1,                 //Opzione agg. FW (USER)
} Opt_Upgrade_t;


typedef struct {
  char tecnologia[MOD_MAX_TECH_SIZE+1];		//Stinga ASCII, apici inclusi (BG96: "GSM", "CAT-M", "CAT-NB")(BG95: "GSM", "eMTC", "NBIoT")
  char MCC[MOD_MCC_MAX_SIZE+1];			//3 cifre decimali
  char MNC[MOD_MNC_MAX_SIZE+1];			//2 o 3 cifre decimali (standard EUROPA vs USA)  
  char LAC_TAC[MOD_LAC_TAC_MAX_SIZE+1];		//LAC (GSM) o TAC (LTE): 16-bit (e.g. 5 cifre decimali)
  char cell_ID[MOD_CELL_ID_MAX_SIZE+1];		//28-bit per LTE (e.g. 9 cifre decimali)/ 16-bit per GSM
  int16_t rx_lev;                               //[dBm], RX_LEV (GSM) o RSSI (LTE)
} Mod_InfoCellaStr_t;


typedef struct {
  int8_t contatore;
  uint8_t stato:1;
  uint8_t pin:1;
} IN_t;


typedef struct {
  uint8_t* host_name;           //Host-name broker
  const uint16_t port;          //Porta broker  
  uint8_t* user_name;           //User-name broker
  uint8_t* password;            //Password broker
  uint8_t* client_id;           //Client-ID
} MQTT_Client_t;


typedef struct {
  uint8_t received;             //Flag ricezione: 0=messaggio MQTT non ricevuto, 1=messaggio MQTT ricevuto
  uint8_t* topic;               //Topic MQTT su cui � stato ricevuto il messaggio
  uint16_t topic_size;          //[B], dimensione topic
  uint8_t* payload;             //Payload messaggio MQTT ricevuto
  uint16_t payload_size;        //[B], dimensione payload
} MQTT_RxMsg_t;


typedef struct {
  uint8_t success;              //Flag lettura/scrittura: 0=file NON letto/scritto, 1=file letto/scritto
  uint16_t size;                //[B], dimensione file
  uint8_t* content;             //Contenuto file
} File_t;
  
/* Global functions */
extern void (*mod_Write_seriale)(uint8_t id_seriale, uint8_t *src, uint16_t size);
extern void (*mod_Drive_reset)(uint8_t ON);
extern void (*mod_Drive_power)(uint8_t ON);
extern uint8_t (*mod_Read_status)(void);
extern void mod_Init_AT(void);
extern ErrorCode_t mod_Read_Response(const uint8_t timeout_ticks, const uint8_t maxRetries, uint8_t* resp, uint16_t* respOffset, uint8_t* error);
extern void mod_Push_Cmd(uint8_t* cmd, uint8_t cmdCode, const uint16_t maxRetries, uint8_t modulo);
extern ErrorCode_t mod_Ignore_Response(void);
  
/* Global variables */
extern uint8_t mod_id_Seriale;
extern Mod_t ModemType;
#ifdef MODEM_UART_LOG
extern uint8_t dbg_modem[100];
extern uint16_t dbg_modem_idx;
#endif
extern uint8_t mod_buff_Risposta[MOD_MAX_BUFFER_RX_BOUND];
extern uint16_t mod_buff_Risposta_idx;
extern Mod_CmdAttesa_t mod_cmdAttesa;

extern Az_Service_t remote_service;
extern MQTT_Client_t* MQTT_Client;
extern uint8_t MQTT_TOPIC_OUT[MOD_MAX_TOPIC_OUT];
extern uint8_t MQTT_TOPIC_IN[MOD_MAX_TOPIC_IN];

extern uint8_t mod_buff_topic_RX[MOD_MAX_BUFFER_TOPIC_RX];
extern char mod_APN[MOD_APN_MAX_SIZE];
  
#ifdef __cplusplus
}
#endif
#endif /*__MODEM_COMMON_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
