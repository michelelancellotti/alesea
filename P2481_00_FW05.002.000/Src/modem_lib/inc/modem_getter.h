/**
************************************************************************************
* File          	: modem_getter.h
* Versione libreria	: 0.02
* Descrizione        	: Interfaccia GET parametri modem (livello applicativo).
************************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
************************************************************************************
*/

#ifndef _MODEM_GETTER_H
#define _MODEM_GETTER_H


/* Includes */
#include "mTypes.h"
#include "modem_common.h"


/* Define & Typedef */
typedef enum {
  MA_BUSY 		        = 0,	//Automa modem impegnato (operazione in corso) 
  MA_OK 		        = 1,	//Operazione completata: nessun errore
  MA_ERROR 		        = 2,	//Operazione completata: errore gestito
  MA_UNP_ERROR 		        = 3,	//Errore imprevisto automa modem
  MA_INIT_ERROR		        = 4,	//Errore inizializzazione libreria modem
  MA_TRIG_OK		        = 5,	//Trigger operazione OK
  MA_NETWORK_ERROR	        = 6,	//Errore connessione rete
  MA_POWER_ON_FAIL	        = 7,	//Errore accensione modem
} ModemAppli_t;

/* Diagnostica modem
*  N.B.: attenzione a indici enum --> gestione posizionale
*/
typedef enum {
  M_DGN_STATO_AUTOMA            = 0,    //stato automa (livello driver)
  M_DNG_PWR_STATUS,             //1     //stato logico accensione modem (livello driver)
  M_DNG_MQTT_ERR_RESULT,        //2     //risultato comando MQTT (connessione)
  M_DNG_MQTT_ERR_RETCODE,       //3     //codice errore comando MQTT (connessione)
  M_DNG_NUM                     //[4]
} ModemDiagnostic_t;


/* Funzioni globali */
extern ModemAppli_t modem_GET_Diagnostic(ModemDiagnostic_t type, 
                                         uint8_t* resp);
extern ModemAppli_t modem_GET_InfoFW(uint16_t max_resp_size, 
                                     uint8_t* resp, uint16_t* resp_size);
extern ModemAppli_t modem_GET_Type(uint8_t* resp);
extern ModemAppli_t modem_GET_ICCID(uint8_t* resp, uint16_t* resp_size);
extern ModemAppli_t modem_GET_RSSI(uint8_t* resp);
extern ModemAppli_t modem_GET_TempoRete(uint16_t* t_reg, uint16_t* t_conn, uint16_t* reg_status, uint8_t* last_error);
extern ModemAppli_t modem_GET_DataOra(uint8_t* resp, uint16_t* resp_size);
extern ModemAppli_t modem_GET_UnixTime(uint32_t* resp);
extern ModemAppli_t modem_GET_InfoCella(uint8_t idx_cella, 
                                        uint8_t* tecnologia, uint16_t* MCC, uint16_t* MNC, uint32_t* cell_ID, uint16_t* LAC_TAC, int16_t* rx_lev);
extern ModemAppli_t modem_GET_InfoGNSS(float* latitudine, float* longitudine);
extern ModemAppli_t modem_GET_InfoGNSS_PRO(float* latitudine, float* longitudine);
extern ModemAppli_t modem_GET_TempoGNSS(uint16_t* resp, uint16_t* status_reg);
extern ModemAppli_t modem_GET_MsgMQTT(uint8_t* resp, uint16_t* resp_size);


#endif	/* End _MODEM_GETTER_H */
