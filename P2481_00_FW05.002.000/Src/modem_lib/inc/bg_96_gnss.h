/**
  ******************************************************************************
  * File          	: bg_96_gnss.h
  * Versione libreria	: 0.02
  * Descrizione        	: Gestione localizzazione GNSS.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BG_96_GNSS_H
#define __BG_96_GNSS_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "bg_96_common.h"
   


/* Defines */
#define BG96_LAT_SIZE			9	//[B]
#define BG96_LONG_SIZE			10	//[B]
#define BG96_GNSS_TIMEOUT_DFLT		180	//[s]
#define BG96_GNSS_PRO_TIMEOUT_DFLT	12	//T=5s*BG96_GNSS_PRO_TIMEOUT_DFLT
   
	 
/* Types & global variables */


/* Global functions */
extern void BG96_GNSS_ON_func(const uint32_t PERIODO);
extern void BG96_GNSS_LOCATION_func(const uint32_t PERIODO);
extern void BG96_GNSS_LOCATION_PRO_func(const uint32_t PERIODO);
extern void BG96_GNSS_OFF_func(const uint32_t PERIODO);   

/* Global variables */
extern uint8_t bg96_tentativi_GNSS_PRO, bg96_lettura_GNSS_PRO;
   

#ifdef __cplusplus
}
#endif
#endif /*__BG_96_GNSS_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
