/**
******************************************************************************
* File          	: bg_96.h
* Versione libreria	: 0.02
* Descrizione        	: Supporto modulo Quectel BG96/BG95.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BG_96_H
#define __BG_96_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>
#include "softTimer.h"
#include "mTypes.h"
#include "bg_96_common.h"



  
/* Defines */
#define BG96_ON_PWRKEY_LOW_TIME    	750     //[ms], tempo attesa dopo pilotaggio PWRKEY (ON modem) [BG95-BG96 compatibile]
#define BG96_OFF_PWRKEY_LOW_TIME    	1000    //[ms], tempo attesa dopo pilotaggio PWRKEY (OFF modem) [BG95-BG96 compatibile]
#define BG95_RESET_WAIT_TIME		3000    //[ms], tempo attesa dopo RESET (BG95)
#define BG96_RESET_WAIT_TIME		300     //[ms], tempo attesa dopo RESET (BG96)
#define BG96_NOP_TIMEOUT		5000	//[ms], tempo attesa prima di dare comandi AT
#define BG96_PWR_OFF_TIMEOUT		10000	//[ms], tempo di guardia entro il quale deve avvenire PWR-OFF
#define BG96_RETRY_PWR_OFF		10
#define BG96_WAIT_READY_TIMEOUT		10000	//[ms], tempo attesa modem READY dopo PWR-ON
#define BG96_RETRY_PWR_ON		3
#define BG96_OFF_DELAY_TIME             1500    //[ms], tempo attesa prima di pilotare PWRKEY (OFF modem)

#if (BG96_OFF_DELAY_TIME<200)
#error "Parametro non valido"
#endif

  
/* Types & global variables */
typedef enum {
  PWR_IGNOTO = 0x00,    //Stato iniziale (POR uC host)
  PWR_ON,		//Modem acceso
  PWR_OFF		//Modem spento
} BG96_PWR_t;


typedef struct {
  uint16_t tempo_drive;          //[ms]
  BG96_State_t next_status;
} BG96_PWR_Action_t;
  
  
/* Global functions */
extern void init_bg96(uint8_t versione_HW, uint8_t sottoversione_HW);
extern void bg96_Automa(const uint32_t PERIODO);
extern void bg96_ProcessaByte(uint8_t data);
extern BG96_State_t get_bg96_status(void);
extern uint8_t is_bg96_ready(void);
  
  
/* Global variables */
extern uint8_t bg96_last_error;
extern BG96_State_t bg96_status;
extern BG96_PWR_t bg96_PWR;
  
#ifdef __cplusplus
}
#endif
#endif /*__BG_96_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
