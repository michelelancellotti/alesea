/**
  ******************************************************************************
  * File          	: bg_96_mqtt.h
  * Versione libreria	: 0.02
  * Descrizione        	: Gestione protocollo MQTT.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BG_96_MQTT_H
#define __BG_96_MQTT_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "bg_96_common.h"
   
/* Defines */
#define MQTT_QoS_PUBLISH                1               //QoS Publish
#define MQTT_QoS_SUBSCRIBE              1               //QoS Subscribe
#if ((MQTT_QoS_PUBLISH<0) || (MQTT_QoS_PUBLISH>2))
#error "PUBLISH QoS non valido!"
#endif
#if ((MQTT_QoS_SUBSCRIBE<0) || (MQTT_QoS_SUBSCRIBE>2))
#error "SUBSCRIBE QoS non valido!"
#endif

#define MQTT_PKT_TIMEOUT                10      //[s]
#define MQTT_RETRY_TIMES                3
#define MQTT_TOTAL_TIMEOUT              ((uint8_t)(MQTT_PKT_TIMEOUT*MQTT_RETRY_TIMES))
   
#define NOT_RETAIN                      "0"
#define RETAIN				"1"
#define MQTT_ID				"0"
#define MQTT_VSN			"4"     //Versione MQTT (3=MQTT v3.1, 4=MQTT v3.1.1)  
	 
/* Types & global variables */
typedef enum {                                  //Codici errore comando AT+QMTCONN
  CONN_ACCEPTED                 = '0',
  CONN_REFUSED_PROTOCOL_VER     = '1',
  CONN_REFUSED_IDENTIFIER       = '2',
  CONN_REFUSED_SERVER_UNAVAIL   = '3',
  CONN_REFUSED_USER_PWD         = '4',
  CONN_REFUSED_AUTHORIZATION    = '5',
} BG96_MQTT_Conn_Ec_t;

/* Global functions */
extern void BG96_MQTT_CONNECT_func(const uint32_t PERIODO);
extern void BG96_MQTT_PUBLISH_func(const uint32_t PERIODO);
extern void BG96_MQTT_SUBSCRIBE_func(const uint32_t PERIODO);
extern void BG96_CLOSE_CONN_func(const uint32_t PERIODO);

/* Global variables */
extern uint16_t bg96_MQTT_msgId_publish, bg96_MQTT_msgId_subscribe;
extern char bg96_pub_sub_at_cmd_resp[20];
   

#ifdef __cplusplus
}
#endif
#endif /*__BG_96_MQTT_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
