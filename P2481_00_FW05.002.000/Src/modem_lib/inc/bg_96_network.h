/**
  ******************************************************************************
  * File          	: bg_96_network.h
  * Versione libreria	: 0.02
  * Descrizione        	: Gestione network.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BG_96_NETWORK_H
#define __BG_96_NETWORK_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "bg_96_common.h"
   
/* Defines */
#define BG96_MAX_RETRY_REG_RETE		90	//T=BG96_MAX_RETRY_REG_RETE*t_attesa_risposta_cmd

#define PDP_ID				"1"
#define NTP_SERV_ADDR			"\"193.204.114.232\""	//I.N.RI.M NTP server      

/* Types & global variables */
typedef struct {
  uint8_t tech;
  uint8_t MCC;
  uint8_t MNC;
  uint8_t LAC_TAC;
  uint8_t cell_ID;
  uint8_t rx_lev;
} BG96_Cell_Idx; 

extern uint32_t bg96_rat_cnt;
extern uint8_t bg96_tentativi_registrazione_rete;
extern uint8_t bg96_stato_registrazione_rete;

/* Global functions */
extern void BG96_CONFIG_RAT_func(const uint32_t PERIODO);
extern void BG96_NETWORK_REG_func(const uint32_t PERIODO);
extern void BG96_PDP_ON_func(const uint32_t PERIODO);

/* Global variables */


#ifdef __cplusplus
}
#endif
#endif /*__BG_96_NETWORK_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
