/**
******************************************************************************
* File          	: at_common.h
* Versione libreria	: 0.02
* Descrizione        	: Interfaccia comune modem/modulo WiFi.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AT_COMMON_H
#define __AT_COMMON_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes */
#include "mTypes.h"
  
  
/* Defines */  
#define MOD_MAX_BUFFER_TX		1152                    //[B], dim. buffer appoggio invio comando AT
#define MOD_MAX_BUFFER_RX_BOUND		(MOD_MAX_BUFFER_TX+32)	//[B], dim. buffer appoggio ricezione risposta AT (BOUND):
                                                                //--> legato a MOD_MAX_BUFFER_TX, e.g. garantisce di vedere nella risposta anche l'echo del comando inviato
#define MOD_MAX_BUFFER_RX_UNBOUND       576                     //[B], dim. buffer appoggio ricezione risposta AT (UNBOUND):
                                                                //--> slegato da MOD_MAX_BUFFER_TX, e.g. NON garantisce di vedere nella risposta l'echo del comando inviato

#define MOD_MAX_BUFFER_DATA_TX		1152    //[B], dim. buffer appoggio dati TX         
#define MOD_MAX_BUFFER_DATA_RX	        920	//[B], dim. buffer appoggio dati RX  

#define MOD_DFLT_ERROR			0xFFFF	//Codice errore di default (se fallisce ricerca stringa errore)  
#define CR_CHAR                         0x0D	//Carriage return
#define LF_CHAR				0x0A	//Line feed
#define CTRL_Z_CHAR			0x1A	//Substitute
  
  
/* Types & global variables */			
typedef void (*State_fptr)(const uint32_t PERIODO);     //Tipo puntatore a funzione di stato
  
/* Entry coda comandi. */
typedef struct {
  uint8_t cmd[MOD_MAX_BUFFER_TX];	//Stringa comando AT
  uint16_t cmdSize;			//Dimensione stringa comando AT
  uint8_t cmdCode;			//Codice identificativo comando
  uint16_t maxRetryCmd;			//Numero MAX retry invio comando
} Mod_Cmd_Entry_t;

typedef struct {
  Mod_Cmd_Entry_t lastCmd;
  uint8_t attesaRisposta;
  uint8_t erroreRisposta;
  uint16_t codiceErrore;
  uint16_t retryCmd;	
} Mod_CmdAttesa_t;


typedef struct {
  uint8_t* at_cmd;
  uint8_t* at_resp;  
  const uint8_t AT_CMD_TYPE;
  const uint8_t AT_CMD_RETRY;
  const uint8_t AT_RESP_TIMEOUT;
  const uint8_t AT_RESP_RETRY;
} Mod_Fifo_Entry_t;
  
  
/* Global functions */
extern uint8_t mod_Inc_Fifo(uint8_t* fifo_index, const uint8_t FIFO_SIZE);
extern int8_t mod_GET_TxDataBuffer(uint8_t** h_data_tx, uint16_t* data_tx_size);
  
  
/* Global variables */
extern uint8_t mod_buff_data_TX[MOD_MAX_BUFFER_DATA_TX];
extern uint8_t mod_buff_data_RX[MOD_MAX_BUFFER_DATA_RX];

  
#ifdef __cplusplus
}
#endif
#endif /*__AT_COMMON_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
