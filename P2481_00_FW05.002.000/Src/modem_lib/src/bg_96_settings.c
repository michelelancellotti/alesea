/**
  ******************************************************************************
  * File          	: bg_96_settings.c
  * Versione libreria	: 0.02
  * Descrizione       	: Impostazioni general-purpose modem:
  *                       - Costellazione GNSS (BG95-only, operativa dopo reboot)                     
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "bg_96_settings.h"
#include "bg_96_command.h"




#define FIFO_GP_SETTINGS_CFG_SIZE          	1               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_GpSettings_Config[FIFO_GP_SETTINGS_CFG_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QGPSCFG=\"gnssconfig\",",      .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_GNSS_CONST,       .AT_CMD_RETRY=3},
};


/* ############################################################ 
 * Local functions
 * ############################################################
*/




/* -------------------- Stato: BG96_GP_SETTINGS -------------------- 
* Impostazioni general-purpose modem: costellazione GNSS.
*/
void BG96_GP_SETTINGS_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  if(ModemType == MOD_BG96)
    bg96_cmd_MASK = 0x0001;     //bypass CMD[0]
  
  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK))
    next_command = 1;           //salta comando corrente
  else
  {
    //Esegui comando corrente    
    err = bg96_send_command(fifo_GpSettings_Config, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
      next_command = 1;
    else if(err == M_NO_AT_RESP)
    {		      
      set_bg96_status(BG96_ERR_S);
      return;
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_GP_SETTINGS_CFG_SIZE))
      set_bg96_status(BG96_IDLE_S);
  }
}
