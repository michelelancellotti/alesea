/**
  ******************************************************************************
  * File          	: bg_96_ftp.c
  * Versione libreria	: 0.02
  * Descrizione       	: Gestione protocollo FTP:
  *                       - Aggiornamento FW uC
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "bg_96_ftp.h"
#include "bg_96_command.h"
#include "bg_96_network.h"




#define FIFO_FW_UP_SIZE            	8			        //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK, bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_FW_Update[FIFO_FW_UP_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QFTPCFG=\"account\","FTP_USER_NAME","FTP_PASSWORD,     .at_resp=(uint8_t*)"OK",			.AT_CMD_TYPE=BG96_CMD_FTP_CFG_ACCOUNT},
  {.at_cmd=(uint8_t*)"AT+QFDEL="LOCAL_UFS_FILE,   				.at_resp=(uint8_t*)"OK",    		        .AT_CMD_TYPE=BG96_CMD_CANCELLA_FILE, 	.AT_RESP_TIMEOUT=BG96_1000MS_TO},	
  {.at_cmd=(uint8_t*)"AT+QIACT?",   						.at_resp=(uint8_t*)"+QIACT: "PDP_ID",1",        .AT_CMD_TYPE=BG96_CMD_QUERY_PDP,        .AT_RESP_TIMEOUT=BG96_1000MS_TO},			
  {.at_cmd=(uint8_t*)"AT+QIACT="PDP_ID,   					.at_resp=(uint8_t*)"OK",    		        .AT_CMD_TYPE=BG96_CMD_PDP_ON,           .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=90},		
  {.at_cmd=(uint8_t*)"AT+QFTPOPEN="FTP_SERVER_ADDR,				.at_resp=(uint8_t*)"+QFTPOPEN: ",		.AT_CMD_TYPE=BG96_CMD_FTP_OPEN,         .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=90},	
  {.at_cmd=(uint8_t*)"AT+QFTPCWD="FTP_DIRECTORY,				.at_resp=(uint8_t*)"+QFTPCWD: ",		.AT_CMD_TYPE=BG96_CMD_FTP_SET_DIR,      .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=90},		
  {.at_cmd=(uint8_t*)"AT+QFTPGET=",						.at_resp=(uint8_t*)"+QFTPGET: ",		.AT_CMD_TYPE=BG96_CMD_FTP_GET_FILE,	.AT_RESP_TIMEOUT=BG96_2000MS_TO,        .AT_RESP_RETRY=135},	
  {.at_cmd=(uint8_t*)"AT+QFTPCLOSE",						.at_resp=(uint8_t*)"+QFTPCLOSE: ",		.AT_CMD_TYPE=BG96_CMD_FTP_CLOSE,        .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=30},
};


uint8_t bg96_FTP_version, bg96_FTP_subversion;	//Numero versione + sottoversione file FTP 
uint16_t bg96_FTP_fSize;

static uint16_t bg96_FTP_result[2];             //Codici errore transazione FTP


/* ############################################################ 
 * Local functions
 * ############################################################
*/




/* -------------------- Stato: BG96_FTP -------------------- 
* Connessione server FTP (download aggiornamento FW).
*/
void BG96_FTP_func(const uint32_t PERIODO)
{  
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK)) 
    next_command = 1;           //salta comando corrente
  else
  {
    //Esegui comando corrente
    err = bg96_send_command(fifo_FW_Update, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
    {
      switch(fifo_FW_Update[bg96_fifo_index].AT_CMD_TYPE)
      {
        case BG96_CMD_FTP_OPEN:
        case BG96_CMD_FTP_SET_DIR:
        case BG96_CMD_FTP_GET_FILE:
        case BG96_CMD_FTP_CLOSE:
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_FTP_result[0] = convertStrToLong(searchField_res[0].field, 0, MAX_FIELD_SIZE);						
          bg96_FTP_result[1] = convertStrToLong(searchField_res[1].field, 0, MAX_FIELD_SIZE);
          if(fifo_FW_Update[bg96_fifo_index].AT_CMD_TYPE==BG96_CMD_FTP_GET_FILE) bg96_FTP_fSize=bg96_FTP_result[1];        //dimensione file scaricato da FTP (B)
          if(bg96_FTP_result[0] != 0) 
          { 
            bg96_reg.BIT.FTP_ERROR = 1;       //errore FTP
            set_bg96_status(BG96_ERR_S); 
            return; 
          }		
          break;
          
        case BG96_CMD_QUERY_PDP:
          bg96_cmd_MASK = 0x0008;             //contesto PDP gi� attivo, salta attivazione (CMD[3]) 
          bg96_PDP_pending_cnt++; 
          break;         
        
        default: break;
      }
      
      next_command = 1;
    }
    else if(err == M_NO_AT_RESP)
    {
      bg96_critical_cmd_MASK = 0x0079;
      //--- Gestione eccezioni su errore ---
      //CMD[0]: impostazioni account FTP: CRITICO
      //CMD[1]: file gi� cancellato --> eseguo download
      //CMD[2]: contesto PDP non attivo --> eseguo attivazione (e.g. comando successivo)
      //CMD[3]: attivazione contesto PDP: CRITICO
      //CMD[4]: connessione server FTP: CRITICO
      //CMD[5]: apertura cartella FTP: CRITICO
      //CMD[6]: download file: CRITICO
      if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
      {
        bg96_reg.BIT.FTP_ERROR = 1; 
        set_bg96_status(BG96_ERR_S);
        return;
      }
      else
        next_command = 1;      
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_FW_UP_SIZE))          
      set_bg96_status(BG96_IDLE_S);
  }
}
