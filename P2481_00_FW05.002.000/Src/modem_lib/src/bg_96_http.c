/**
  *********************************************************************************************
  * File          	: bg_96_http.c
  * Versione libreria	: 0.02
  * Descrizione       	: Gestione protocollo HTTP:
  *                       - download chiave individuale dispositivo (Azure) tramite API
  *********************************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  *********************************************************************************************
  */

#include "bg_96_http.h"
#include "bg_96_command.h"
#include "bg_96_network.h"
#include "bg_96_tls.h"
#include "bg_96_file.h"




#define FIFO_HTTP_CFG_SIZE          	11		//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK, bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_HTTP_Config[FIFO_HTTP_CFG_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QHTTPCFG=\"contextid\","PDP_ID,        .at_resp=(uint8_t*)"OK",                        .AT_CMD_TYPE=BG96_CMD_HTTP_CFG_PDP_ID},
  {.at_cmd=(uint8_t*)"AT+QHTTPCFG=\"sslctxid\","TLS_ID,         .at_resp=(uint8_t*)"OK",                        .AT_CMD_TYPE=BG96_CMD_HTTP_CFG_TLS_ID},
  {.at_cmd=(uint8_t*)"AT+QHTTPCFG=\"responseheader\",0",        .at_resp=(uint8_t*)"OK",                        .AT_CMD_TYPE=BG96_CMD_HTTP_CFG_RESP_HEADER},
  {.at_cmd=(uint8_t*)"AT+QHTTPCFG=\"requestheader\",1",         .at_resp=(uint8_t*)"OK",                        .AT_CMD_TYPE=BG96_CMD_HTTP_CFG_REQ_HEADER,          .AT_CMD_RETRY=3},
  {.at_cmd=(uint8_t*)"AT+QIACT?",   				.at_resp=(uint8_t*)"+QIACT: "PDP_ID",1",        .AT_CMD_TYPE=BG96_CMD_QUERY_PDP,                                        .AT_RESP_TIMEOUT=BG96_1000MS_TO},
  {.at_cmd=(uint8_t*)"AT+QIACT="PDP_ID,   			.at_resp=(uint8_t*)"OK",    			.AT_CMD_TYPE=BG96_CMD_PDP_ON, 		                                .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=90},
  {.at_cmd=(uint8_t*)"AT+QHTTPURL=",     			.at_resp=(uint8_t*)"CONNECT",			.AT_CMD_TYPE=BG96_CMD_HTTP_CFG_URL,	                                .AT_RESP_TIMEOUT=BG96_1000MS_TO},
  {.at_cmd=(uint8_t*)HTTP_API_URL_ADDR,     			.at_resp=(uint8_t*)"OK",			.AT_CMD_TYPE=BG96_CMD_HTTP_URL,                                         .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=HTTP_INPUT_TIMEOUT},
  {.at_cmd=(uint8_t*)"AT+QHTTPPOST=",     			.at_resp=(uint8_t*)"CONNECT",		        .AT_CMD_TYPE=BG96_CMD_HTTP_CFG_POST_HEADER,                             .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=125},
  {.at_cmd=(uint8_t*)"",     			                .at_resp=(uint8_t*)"+QHTTPPOST: ",		.AT_CMD_TYPE=BG96_CMD_HTTP_POST_HEADER,                                 .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=HTTP_POST_TIMEOUT},
  {.at_cmd=(uint8_t*)"AT+QHTTPREADFILE="AZ_DEVICE_KEY_FNAME,    .at_resp=(uint8_t*)"+QHTTPREADFILE: ",		.AT_CMD_TYPE=BG96_CMD_HTTP_POST_BODY_FILE,                              .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=HTTP_POST_TIMEOUT},
};


uint32_t bg96_tempo_HTTP = 0;           //Tempo GET HTTP 
uint16_t bg96_HTTP_request_size;        //Dimensione richiesta HTTP (header+body) [B]

static uint16_t bg96_HTTP_result[2];    //Codici errore transazione HTTP


/* ############################################################ 
 * Local functions
 * ############################################################
*/




/* -------------------- Stato: BG96_HTTP -------------------- 
* Connessione server HTTP (download chiave individuale Azure).
*/
void BG96_HTTP_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  bg96_tempo_HTTP += PERIODO;
  
  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK)) 
    next_command = 1;           //salta comando corrente
  else
  {
    //Esegui comando corrente
    err = bg96_send_command(fifo_HTTP_Config, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
    {
      switch(fifo_HTTP_Config[bg96_fifo_index].AT_CMD_TYPE)
      {
        case BG96_CMD_HTTP_POST_HEADER:
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_HTTP_result[0] = convertStrToLong(searchField_res[0].field, 0, MAX_FIELD_SIZE);
          bg96_HTTP_result[1] = convertStrToLong(searchField_res[1].field, 0, MAX_FIELD_SIZE);          
          if(bg96_HTTP_result[0]!=0 || bg96_HTTP_result[1]!=EC_HTTP_OK) 
          { 
            bg96_reg.BIT.HTTP_ERROR = 1;        //errore HTTP
            set_bg96_status(BG96_ERR_S); 
            return; 
          }
          break;
          
        case BG96_CMD_HTTP_POST_BODY_FILE:
          bg96_HTTP_result[0] = convertStrToLong(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));
          if(bg96_HTTP_result[0] != 0) 
          { 
            bg96_reg.BIT.HTTP_ERROR = 1;        //errore HTTP 
            set_bg96_status(BG96_ERR_S); 
            return; 
          }
          break;
          
        case BG96_CMD_QUERY_PDP:
          bg96_cmd_MASK = 0x0020;       //contesto PDP gi� attivo, salta attivazione (CMD[5]) 
          bg96_PDP_pending_cnt++; 
          break;
          
        default: break;
      }
      
      next_command = 1;
    }
    else if(err == M_NO_AT_RESP)
    {
      bg96_critical_cmd_MASK = 0x07A0;
      //--- Gestione eccezioni su errore ---
      //CMD[4]: contesto PDP non attivo --> eseguo attivazione (e.g. comando successivo)
      //CMD[5]: attivazione contesto PDP: CRITICO
      //CMD[7]: input URL HTTP: CRITICO
      //CMD[8]: apertura connessione HTTP: CRITICO
      //CMD[9]: invio richiesta HTTP: CRITICO
      //CMD[10]: lettura risposta HTTP: CRITICO
      if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
      {
        bg96_reg.BIT.HTTP_ERROR = 1;
        set_bg96_status(BG96_ERR_S);
        return;
      }
      else
        next_command = 1;
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_HTTP_CFG_SIZE))
      set_bg96_status(BG96_IDLE_S);
  }
}

