/**
******************************************************************************
* File          	: bg_96.c
* Versione libreria	: 0.02
* Descrizione       	: Supporto modem Quectel BG96/BG95.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "bg_96.h"
#include "bg_96_info.h"
#include "bg_96_network.h"
#include "bg_96_gnss.h"
#include "bg_96_mqtt.h"
#include "bg_96_ftp.h"
#include "bg_96_http.h"
#include "bg_96_settings.h"
#include "bg_96_tls.h"
#include "bg_96_file.h"


const BG96_PWR_Action_t ACCENSIONE = {.tempo_drive=BG96_ON_PWRKEY_LOW_TIME, .next_status=BG96_WAIT_READY_S};
const BG96_PWR_Action_t SPEGNIMENTO = {.tempo_drive=BG96_OFF_PWRKEY_LOW_TIME, .next_status=BG96_STDBY_S};



GPTIMER bg96_NopTimer;
BG96_State_t bg96_status;

BG96_PWR_t bg96_PWR = PWR_IGNOTO;						//Stato accensione modem
uint16_t bg96_cmd_MASK=0, bg96_critical_cmd_MASK=0;
uint8_t mod_versione_HW, mod_sottoversione_HW;
uint8_t bg96_fifo_index = 0;                                                    //Indice coda comandi
BG96_Reg_t bg96_reg;
BG96_Option_Reg_t bg96_option_reg;
uint8_t bg96_last_error, bg96_last_nc_error;

uint8_t AT_ERROR_STR[] = "ERROR";

static BG96_State_t bg96_status_prev;
static GPTIMER bg96_pwrTimer, bg96_resetTimer;
static GPTIMER bg96_guardTimer, bg96_waitReadyTimer, bg96_offDelayTimer;	//Timer
static uint8_t bg96_wait_ready_failure;
static uint8_t bg96_pwr_off_failure = 0;
static IN_t bg96_GPIO_STATUS;
static uint8_t bg96_ready = 0;
static const BG96_PWR_Action_t* bg96_PWR_action;                                //Azione richiesta con pilotaggio PWRKEY
static uint16_t bg96_err_cnt = 0;						//Contatore errori
static uint16_t bg96_reset_cnt = 0;						//Contatore RESET
static uint16_t bg96_pwrkey_cnt = 0;						//Contatore pilotaggio PWRKEY
static uint16_t bg96_nc_err_cnt = 0;                                            //Contatore errori comandi NON CRITICI e/o comandi che possono andare in errore (eccezioni)




//TEST FW_BG95 @ 24/05/2022 = BG95M3LAR02A04 - APPVER: 01.002.01.002
//TEST FW_BG95 @ 20/04/2022 = BG95M3LAR02A04 - APPVER: 01.001.01.001


/* ############################################################ 
* Local functions
* ############################################################
*/
static void init_ctx_bg96(void);
static void BG96_PWR_ON_RESET_func(const uint32_t PERIODO);
static void BG96_PWR_ON_func(const uint32_t PERIODO);
static void BG96_DRIVE_PWRKEY_func(const uint32_t PERIODO);
static void BG96_WAIT_READY_func(const uint32_t PERIODO);
static void BG96_IDLE_func(const uint32_t PERIODO);
static void BG96_PWR_OFF_func(const uint32_t PERIODO);
static void BG96_STDBY_func(const uint32_t PERIODO);
static void BG96_RESET_func(const uint32_t PERIODO);
static void BG96_ERR_func(const uint32_t PERIODO);


static State_fptr bg96_status_func[BG96_STATE_NUM] = {
  BG96_PWR_ON_func,             //0x00
  BG96_WAIT_READY_func,	        //0x01
  BG96_IDLE_func,		//0x02
  BG96_PWR_OFF_func,            //0x03
  BG96_INFO_func,               //0x04  
  BG96_NETWORK_REG_func,        //0x05
  BG96_GNSS_ON_func,		//0x06
  BG96_GNSS_LOCATION_func,      //0x07
  BG96_GNSS_OFF_func,		//0x08
  BG96_PDP_ON_func,             //0x09
  BG96_MQTT_CONNECT_func,	//0x0A
  BG96_MQTT_PUBLISH_func,	//0x0B
  BG96_MQTT_SUBSCRIBE_func,	//0x0C
  BG96_STDBY_func,              //0x0D
  BG96_FTP_func,		//0x0E
  BG96_HTTP_func,               //0x0F
  BG96_ERR_func,                //0x10
  BG96_RESET_func,              //0x11
  BG96_CLOSE_CONN_func,		//0x12
  BG96_GP_SETTINGS_func,        //0x13
  BG96_GNSS_LOCATION_PRO_func,  //0x14
  BG96_CONFIG_RAT_func,         //0x15
  BG96_PWR_ON_RESET_func,       //0x16
  BG96_DRIVE_PWRKEY_func,       //0x17
  BG96_CONFIG_TLS_func,         //0x18
  BG96_FILE_READ_func,          //0x19
  BG96_FILE_WRITE_func,         //0x20
};



void init_bg96(uint8_t versione_HW, uint8_t sottoversione_HW)
{
  mod_versione_HW = versione_HW;
  mod_sottoversione_HW = sottoversione_HW;
  
  GPTIMER_init(&bg96_pwrTimer, BG96_ON_PWRKEY_LOW_TIME);
  GPTIMER_init(&bg96_resetTimer, BG95_RESET_WAIT_TIME);
  GPTIMER_init(&bg96_guardTimer, BG96_PWR_OFF_TIMEOUT);
  GPTIMER_init(&bg96_waitReadyTimer, BG96_WAIT_READY_TIMEOUT);
  GPTIMER_init(&bg96_NopTimer, BG96_NOP_TIMEOUT);
  GPTIMER_init(&bg96_offDelayTimer, BG96_OFF_DELAY_TIME);
  
  init_ctx_bg96();
  
  bg96_GPIO_STATUS.stato = mod_Read_status();		//Assegno valore iniziale pin
  
  //Init macchina a stati
  set_bg96_status(BG96_STDBY_S);
  bg96_status_prev = BG96_STDBY_S;
  bg96_PWR = PWR_IGNOTO;
  bg96_PWR_action = &ACCENSIONE;
}

/* Init contesto "sessione" corrente. */
static void init_ctx_bg96(void)
{
  bg96_err_cnt = 0;
  bg96_last_error = 0x00;
  bg96_cmd_MASK = 0;
  bg96_critical_cmd_MASK = 0;
  bg96_check_COM = 0;
  bg96_PDP_pending_cnt = 0;
  bg96_tempo_registrazione = 0; 
  bg96_tempo_connessione = 0;
  memset(bg96_DataOra_UTC, 0x00, sizeof(bg96_DataOra_UTC));
  memset((uint8_t*)&bg96_MQTT_result, 0x00, sizeof(bg96_MQTT_result));
  
  bg96_reg.WORD = 0;
  bg96_gnss_reg.WORD &= 0xFF00;         //Reset solo B0 (Diagnostica)

  mod_Init_AT();                        //Reset gestione RX/TX comandi AT (caso di reset asincroni del modulo)  
}

uint8_t is_bg96_ready(void)
{
  return bg96_ready;	
}


/* Determina se il comando indicizzato è da saltare, in base alla maschera. */
uint8_t bg96_bypass_cmd(uint8_t cmd_index, uint16_t cmd_mask)
{
  if(((1<<(cmd_index)) & cmd_mask) == 0)
    return 0;		//comando da eseguire
  else
    return 1;		//comando da saltare
}

/* Determina se il comando indicizzato è CRITICO, in base alla maschera. 
*  Rilevare un errore su un comando CRITICO genera l'uscita immediata dallo stato dell'automa.
*/
uint8_t bg96_critical_cmd(uint8_t cmd_index, uint16_t cmd_mask)
{
  if(((1<<(cmd_index)) & cmd_mask) == 0)
  {
    bg96_nc_err_cnt++;
    bg96_last_nc_error = mod_cmdAttesa.lastCmd.cmdCode;
    return 0;		//comando NON CRITICO
  }
  else
    return 1;		//comando CRITICO
}


void set_bg96_status(BG96_State_t new_status)
{
  switch(new_status)
  {
    case BG96_PWR_ON_S:
    case BG96_RESET_S:
    case BG96_PWR_ON_RESET_S: {
      init_ctx_bg96();  //init contesto "sessione" corrente
    } break;
    
    case BG96_PWR_OFF_S: {
      GPTIMER_stop(&bg96_offDelayTimer);
      GPTIMER_setTics(&bg96_offDelayTimer, BG96_OFF_DELAY_TIME);
      GPTIMER_reTrigger(&bg96_offDelayTimer);
    } break;
    
    case BG96_WAIT_READY_S: { bg96_reg.BIT.PWR_ON_FAILURE=0; } break;
    case BG96_ERR_S: { bg96_status_prev=bg96_status; } break;			//snapshot stato che ha generato errore
    case BG96_INFO_S: { bg96_reg.BIT.SIM_RILEVATA=0; bg96_reg.BIT.TYPE_DETECT=0; } break;
    case BG96_CONFIG_RAT_S: { bg96_reg.BIT.RAT_SET=1; bg96_rat_cnt++; } break;
    
    case BG96_NETWORK_REG_S: { 
      bg96_tentativi_registrazione_rete=0; bg96_stato_registrazione_rete=IGNOTO;      
      bg96_reg.BIT.STATO_REG_RETE=0; bg96_cmd_MASK=0;
      bg96_tempo_registrazione=0; bg96_tempo_connessione=0;      
    } break;
    
    case BG96_PDP_ON_S: {
      bg96_reg.BIT.PDP_ERROR=0; bg96_reg.BIT.NTP_SYNC=0;
      bg96_cmd_MASK=0;  
    } break;
    
    case BG96_GNSS_ON_S: { bg96_reg.BIT.LOC_GNSS=0; bg96_gnss_reg.BIT.LOC_GNSS_PRO=0; } break;
    case BG96_GNSS_LOCATION_PRO_S: { bg96_tentativi_GNSS_PRO=0; bg96_lettura_GNSS_PRO=0; } break;
    
    case BG96_MQTT_PUBLISH_S: {
#if (MQTT_QoS_PUBLISH == 0)
      bg96_MQTT_msgId_publish = 0;
#else
      bg96_MQTT_msgId_publish++;                                        //Incremento Msg-Id (PUBLISH)
      if(bg96_MQTT_msgId_publish==0) bg96_MQTT_msgId_publish++;         //Msg-Id=0 non ammesso con QoS>0
#endif
      bg96_reg.BIT.MQTT_ERROR_PUBLISH = 0;
      
      memset(bg96_pub_sub_at_cmd_resp, 0x00, sizeof(bg96_pub_sub_at_cmd_resp));
      if(az_background_in_progress())
        bg96_option_reg.BIT.MQTT_PUBLISH_WAIT_MSG_OPT = 1;
      else
        bg96_option_reg.BIT.MQTT_PUBLISH_WAIT_MSG_OPT = 0;
      //Assegno risposta a comando AT dinamicamente      
      if(bg96_option_reg.BIT.MQTT_PUBLISH_WAIT_MSG_OPT)
      {
        snprintf(bg96_pub_sub_at_cmd_resp, sizeof(bg96_pub_sub_at_cmd_resp), "+QMTRECV: ");
        bg96_MQTT_rx_msg.received = 0;
      }
      else
        snprintf(bg96_pub_sub_at_cmd_resp, sizeof(bg96_pub_sub_at_cmd_resp), "+QMTPUB: %s,%d,0", MQTT_ID, bg96_MQTT_msgId_publish);
    } break;
    
    case BG96_MQTT_SUBSCRIBE_S: {
#if (MQTT_QoS_SUBSCRIBE == 0)
      bg96_MQTT_msgId_subscribe = 1;                                    //Msg-Id=0 non ammesso
#else
      bg96_MQTT_msgId_subscribe++;                                      //Incremento Msg-Id (SUBSCRIBE)
      if(bg96_MQTT_msgId_subscribe==0) bg96_MQTT_msgId_subscribe++;     //Msg-Id=0 non ammesso
#endif
      bg96_reg.BIT.MQTT_ERROR_SUBSCRIBE = 0;
      bg96_cmd_MASK = 0;
      
      memset(bg96_pub_sub_at_cmd_resp, 0x00, sizeof(bg96_pub_sub_at_cmd_resp));
      if(az_background_in_progress())
        bg96_option_reg.BIT.MQTT_SUBSCRIBE_WAIT_MSG_OPT = 0;
      else
        bg96_option_reg.BIT.MQTT_SUBSCRIBE_WAIT_MSG_OPT = 1;
      //Assegno risposta a comando AT dinamicamente
      if(bg96_option_reg.BIT.MQTT_SUBSCRIBE_WAIT_MSG_OPT)
      {
        snprintf(bg96_pub_sub_at_cmd_resp, sizeof(bg96_pub_sub_at_cmd_resp), "+QMTRECV: ");
        bg96_MQTT_rx_msg.received = 0;
      }
      else
        snprintf(bg96_pub_sub_at_cmd_resp, sizeof(bg96_pub_sub_at_cmd_resp), "+QMTSUB: %s,%d,0", MQTT_ID, bg96_MQTT_msgId_subscribe);
    } break;
    
    case BG96_MQTT_CONNECT_S: { 
      bg96_cmd_MASK=0; bg96_reg.BIT.MQTT_ERROR_CONN=0; bg96_num_celle=0;
      memset((uint8_t*)&bg96_MQTT_result, 0x00, sizeof(bg96_MQTT_result));
    } break;
    
    case BG96_FTP_S: { bg96_FTP_fSize=0; bg96_reg.BIT.FTP_ERROR=0; bg96_cmd_MASK=0; } break;    
    case BG96_HTTP_S: { bg96_cmd_MASK=0; bg96_tempo_HTTP=0; bg96_reg.BIT.HTTP_ERROR=0; } break;    
    case BG96_GP_SETTINGS_S: { bg96_cmd_MASK=0; } break;
    case BG96_CONFIG_TLS_S: { bg96_cmd_MASK=0; bg96_reg.BIT.TLS_CONF_ERROR=0; } break;
    case BG96_FILE_READ_S: { bg96_file_read.success=0; bg96_reg.BIT.FILE_ERROR=0; } break;
    case BG96_FILE_WRITE_S: { bg96_file_write.success=0; bg96_reg.BIT.FILE_ERROR=0; } break;
    default: break;
  }
  if(new_status!=BG96_IDLE_S) bg96_reg.BIT.GENERIC_ERROR=0; 
  bg96_status = new_status;
  bg96_fifo_index = 0;
}
BG96_State_t get_bg96_status(void)
{
  return bg96_status;	
}


/* Macchina a stati BG96/BG95. */
void bg96_Automa(const uint32_t PERIODO)
{
  #define FILTRO_INGRESSI		5	/* 50ms di filtro chiamando la funzione ogni 10ms */
  static uint8_t wait_filtro_ingresso = 0;  
  
  bg96_GPIO_STATUS.pin = mod_Read_status();
  /* Filtro ingresso STATUS: integrale. */
  if(bg96_GPIO_STATUS.pin==0) (bg96_GPIO_STATUS.contatore)--;
  else (bg96_GPIO_STATUS.contatore)++;
  /* Saturazione filtro */
  if(bg96_GPIO_STATUS.contatore <= 0)
  {
    bg96_GPIO_STATUS.contatore = 0;
    bg96_GPIO_STATUS.stato = 0;
  }
  if(bg96_GPIO_STATUS.contatore >= FILTRO_INGRESSI)
  {
    bg96_GPIO_STATUS.contatore = FILTRO_INGRESSI;
    bg96_GPIO_STATUS.stato = 1;
  }
  /* Attesa latenza filtro ingresso STATUS prima di eseguire automa */
  if(wait_filtro_ingresso < FILTRO_INGRESSI)
  {
    wait_filtro_ingresso++;
    return;
  }
  
  /* Check limiti array punt. funz. stato */
  if(bg96_status >= BG96_STATE_NUM)
  {
    set_bg96_status(BG96_IDLE_S);
    return;
  }
  
  
  bg96_status_func[bg96_status](PERIODO);       /* Eseguo callback funzione di stato */	
}


/* -------------------- Stato: BG96_PWR_ON_RESET -------------------- 
* Accensione modulo con Reset.
*/
static void BG96_PWR_ON_RESET_func(const uint32_t PERIODO)
{
  bg96_pwr_off_failure = 0;
  GPTIMER_stop(&bg96_guardTimer);		//Reset condizioni di guardia su richiesta esplicita di PWR-ON
  GPTIMER_stop(&bg96_NopTimer);		
  bg96_fifo_index = 0;
  set_bg96_status(BG96_RESET_S);
}


/* -------------------- Stato: BG96_PWR_ON -------------------- 
* Accensione modulo.
*/
static void BG96_PWR_ON_func(const uint32_t PERIODO)
{
  bg96_pwr_off_failure = 0;
  GPTIMER_stop(&bg96_guardTimer);		//Reset condizioni di guardia su richiesta esplicita di PWR-ON
  GPTIMER_stop(&bg96_NopTimer);		
  bg96_fifo_index = 0;
  bg96_PWR_action = &ACCENSIONE;
  if(bg96_GPIO_STATUS.stato == 0)
  {
    //Modem spento: accendi
    bg96_ready = 0;
    set_bg96_status(BG96_DRIVE_PWRKEY_S);
  }
  else
  {
    //Modem già acceso (UART già READY)
    set_bg96_status(BG96_RESET_S);              //Reset modem sincrono con reset uC
  }
}


/* -------------------- Stato: BG96_DRIVE_PWRKEY -------------------- 
* Pilotaggio pin PWRKEY (sia per accensione che per spegnimento).
*/
static void BG96_DRIVE_PWRKEY_func(const uint32_t PERIODO)
{
  if(!bg96_pwrTimer.enable)
  {
    bg96_pwrkey_cnt++;
    mod_Drive_power(1);			//PWR attivo
    GPTIMER_stop(&bg96_pwrTimer);       
    GPTIMER_setTics(&bg96_pwrTimer, bg96_PWR_action->tempo_drive);
    GPTIMER_reTrigger(&bg96_pwrTimer);  //Start wait time
  }
  else if(GPTIMER_isTimedOut(&bg96_pwrTimer))
  {
    GPTIMER_stop(&bg96_pwrTimer);
    mod_Drive_power(0);                 //Rilascio PWR
    set_bg96_status(bg96_PWR_action->next_status);
  }
}


/* -------------------- Stato: BG96_WAIT_READY -------------------- 
* Attesa completamento accensione (segnalazione pin STATUS).
*/
static void BG96_WAIT_READY_func(const uint32_t PERIODO)
{
  //----- Check tempo guardia per failure power-on -----
  if(!bg96_waitReadyTimer.enable)
  {
    GPTIMER_stop(&bg96_waitReadyTimer);
    GPTIMER_setTics(&bg96_waitReadyTimer, BG96_WAIT_READY_TIMEOUT);
    GPTIMER_reTrigger(&bg96_waitReadyTimer);	                            //Start guard time
  }
  else if(GPTIMER_isTimedOut(&bg96_waitReadyTimer))
  {
    GPTIMER_stop(&bg96_waitReadyTimer);					
    if(bg96_wait_ready_failure < BG96_RETRY_PWR_ON)
    {
      bg96_wait_ready_failure++;
      set_bg96_status(BG96_RESET_S);
    }
    else
    {
      bg96_wait_ready_failure = 0;
      bg96_reg.BIT.PWR_ON_FAILURE = 1;
      set_bg96_status(BG96_IDLE_S);
      return;
    }
  }
  //----------------------------------------------------
  if(bg96_GPIO_STATUS.stato == 1)
  {
    bg96_wait_ready_failure = 0;
    GPTIMER_stop(&bg96_waitReadyTimer);
    bg96_reg.BIT.PWR_ON_FAILURE = 0;
    bg96_PWR = PWR_ON;
    bg96_ready = 1;
    if(bg96_pwr_off_failure>0) {set_bg96_status(BG96_PWR_OFF_S);}           //Retry spegnimento
    else 
    {
      GPTIMER_stop(&bg96_NopTimer);
      GPTIMER_setTics(&bg96_NopTimer, BG96_NOP_TIMEOUT);
      GPTIMER_reTrigger(&bg96_NopTimer);
      set_bg96_status(BG96_INFO_S);
    }
  }
  else
    bg96_ready = 0;
}


/* -------------------- Stato: BG96_IDLE -------------------- */
static void BG96_IDLE_func(const uint32_t PERIODO)
{
  bg96_fifo_index = 0;
  bg96_cmd_MASK = 0;
  bg96_critical_cmd_MASK = 0;
  //Gestione processi background libreria Azure
  az_process_background();
}


/* -------------------- Stato: BG96_PWR_OFF -------------------- 
* Spegnimento modulo.
*/
static void BG96_PWR_OFF_func(const uint32_t PERIODO)
{
  if(!GPTIMER_isTimedOut(&bg96_offDelayTimer) && bg96_offDelayTimer.enable)
    return;
  
  GPTIMER_stop(&bg96_offDelayTimer);
  /* Ritardo prima di pilotare PWR OFF concluso:
  *  inserito su gestione BG95 altrimenti pilotando PWR OFF troppo ravvicinato a RESET, PWR_OFF non viene interpretato.
  */
  bg96_PWR_action = &SPEGNIMENTO;
  if(bg96_GPIO_STATUS.stato == 1)
  {
    //Modem acceso: spegni
    set_bg96_status(BG96_DRIVE_PWRKEY_S);
  }
  else
    set_bg96_status(BG96_STDBY_S);              //Modem già spento
}


/* -------------------- Stato: BG96_STDBY -------------------- 
* Stato di appoggio (POR uC host o dopo spegnimento modulo).
*/
static void BG96_STDBY_func(const uint32_t PERIODO)
{
  if(bg96_GPIO_STATUS.stato == 0)					
  {
    bg96_PWR=PWR_OFF; bg96_pwr_off_failure=0; GPTIMER_stop(&bg96_guardTimer);
  }
  else
  {
    //Check tempo guardia per failure power-off
    if(!bg96_guardTimer.enable)
    {
      GPTIMER_stop(&bg96_guardTimer);
      GPTIMER_setTics(&bg96_guardTimer, BG96_PWR_OFF_TIMEOUT);
      GPTIMER_reTrigger(&bg96_guardTimer);              //Start guard time
    }
    else if(GPTIMER_isTimedOut(&bg96_guardTimer))
    {
      GPTIMER_stop(&bg96_guardTimer);
      if(bg96_pwr_off_failure < BG96_RETRY_PWR_OFF)
      {
        bg96_pwr_off_failure++; 
        set_bg96_status(BG96_RESET_S);
      }
      else 
        bg96_PWR = PWR_OFF;
    }
  }
}


/* -------------------- Stato: BG96_RESET -------------------- 
* Reset modulo (a seguito di PWR-OFF failure).
*/
static void BG96_RESET_func(const uint32_t PERIODO)
{
  if(!bg96_resetTimer.enable)
  {
    bg96_reset_cnt++;
    mod_Drive_reset(1);                         //RST attivo
    GPTIMER_stop(&bg96_resetTimer);
    //Tempo differenziato tra BG95 e BG96 (per rispettare specifiche di BG96)
    if(ModemType == MOD_BG96)
      GPTIMER_setTics(&bg96_resetTimer, BG96_RESET_WAIT_TIME);
    else
      GPTIMER_setTics(&bg96_resetTimer, BG95_RESET_WAIT_TIME);
    GPTIMER_reTrigger(&bg96_resetTimer);        //Start wait time
  }
  else if(GPTIMER_isTimedOut(&bg96_resetTimer))
  {					
    GPTIMER_stop(&bg96_resetTimer);
    mod_Drive_reset(0);				//Rilascio RST
    set_bg96_status(BG96_WAIT_READY_S);
  }
}


/* -------------------- Stato: BG96_ERR -------------------- 
* Stato di errore.
*/
static void BG96_ERR_func(const uint32_t PERIODO)
{
  bg96_err_cnt++;
  bg96_last_error = mod_cmdAttesa.lastCmd.cmdCode;			
  if(bg96_status_prev==BG96_PDP_ON_S ||
      bg96_status_prev==BG96_CONFIG_TLS_S ||
       bg96_status_prev==BG96_MQTT_CONNECT_S ||
         bg96_status_prev==BG96_MQTT_PUBLISH_S ||
           bg96_status_prev==BG96_MQTT_SUBSCRIBE_S ||
             bg96_status_prev==BG96_FTP_S ||
               bg96_status_prev==BG96_CLOSE_CONN_S ||
                 bg96_status_prev==BG96_GNSS_OFF_S ||
                   bg96_status_prev==BG96_GP_SETTINGS_S ||
                     bg96_status_prev==BG96_FILE_READ_S ||
                       bg96_status_prev==BG96_HTTP_S) 
  {
    //Errore GESTITO
    set_bg96_status(BG96_IDLE_S); 
  }
  else if(bg96_status_prev==BG96_GNSS_ON_S || bg96_status_prev==BG96_GNSS_LOCATION_S)
  {
    //Errore GESTITO
    set_bg96_status(BG96_GNSS_OFF_S);
  }
  else 
  { 
    //Errore IMPREVISTO
    bg96_reg.BIT.GENERIC_ERROR = 1;
    set_bg96_status(BG96_IDLE_S); 
  }
}



/* Processa byte ricevuti su seriale. */
void bg96_ProcessaByte(uint8_t data)
{
  //### Modulo pronto a ricevere comandi ###
  //<<< Buffer ricerca risposta >>>
  if(mod_cmdAttesa.attesaRisposta && (mod_buff_Risposta_idx < sizeof(mod_buff_Risposta)))
  {      
    mod_buff_Risposta[mod_buff_Risposta_idx++] = data;      
  }
}
