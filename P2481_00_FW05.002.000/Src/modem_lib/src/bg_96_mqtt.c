/**
  ******************************************************************************
  * File          	: bg_96_mqtt.c
  * Versione libreria	: 0.02
  * Descrizione       	: Gestione protocollo MQTT:
  *                       - Connessione
  *                       - Publish TOPIC
  *                       - Subscribe TOPIC
  *                       - Disconnessione
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "bg_96_mqtt.h"
#include "bg_96_command.h"
#include "bg_96_network.h"




#define FIFO_MQTT_CONN_SIZE             11		//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK, bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_MQTT_Connect[FIFO_MQTT_CONN_SIZE] = {	
  {.at_cmd=(uint8_t*)"AT+CCLK?",					.at_resp=(uint8_t*)"+CCLK: \"",			        .AT_CMD_TYPE=BG96_CMD_RTC_DATE_TIME},
  {.at_cmd=(uint8_t*)"AT+QIACT?",   					.at_resp=(uint8_t*)"+QIACT: "PDP_ID",1",            	.AT_CMD_TYPE=BG96_CMD_QUERY_PDP, 		.AT_RESP_TIMEOUT=BG96_1000MS_TO},			
  {.at_cmd=(uint8_t*)"AT+QIACT="PDP_ID,   				.at_resp=(uint8_t*)"OK",    			        .AT_CMD_TYPE=BG96_CMD_PDP_ON, 			.AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=90},	
  {.at_cmd=(uint8_t*)"AT+CSQ",						.at_resp=(uint8_t*)"+CSQ: ",			        .AT_CMD_TYPE=BG96_CMD_RSSI},
  {.at_cmd=(uint8_t*)"AT+QENG=\"servingcell\"",				.at_resp=(uint8_t*)"+QENG: \"servingcell\",",	        .AT_CMD_TYPE=BG96_CMD_INFO_CELLA},	
  {.at_cmd=(uint8_t*)"AT+QENG=\"neighbourcell\"",			.at_resp=(uint8_t*)"+QENG: \"neighbourcell\",",         .AT_CMD_TYPE=BG96_CMD_INFO_CELLA_VICINA},
  {.at_cmd=(uint8_t*)"AT+QMTCFG=\"timeout\","MQTT_ID",",                .at_resp=(uint8_t*)"OK",    				.AT_CMD_TYPE=BG96_CMD_MQTT_CFG_TIMEOUT},
  {.at_cmd=(uint8_t*)"AT+QMTCFG=\"version\","MQTT_ID","MQTT_VSN,        .at_resp=(uint8_t*)"OK",    				.AT_CMD_TYPE=BG96_CMD_MQTT_CFG_VSN},
  {.at_cmd=(uint8_t*)"AT+QMTCFG=\"recv/mode\","MQTT_ID",0,1",		.at_resp=(uint8_t*)"OK",    				.AT_CMD_TYPE=BG96_CMD_MQTT_CFG_RCV},
  {.at_cmd=(uint8_t*)"AT+QMTOPEN="MQTT_ID",",                           .at_resp=(uint8_t*)"+QMTOPEN: ",    			.AT_CMD_TYPE=BG96_CMD_MQTT_OPEN, 		.AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=75},
  {.at_cmd=(uint8_t*)"AT+QMTCONN="MQTT_ID",",  			        .at_resp=(uint8_t*)"+QMTCONN: ",			.AT_CMD_TYPE=BG96_CMD_MQTT_CONN,		.AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=MQTT_PKT_TIMEOUT},
};

#define FIFO_MQTT_PUB_SIZE              2
static Mod_Fifo_Entry_t fifo_MQTT_Publish[FIFO_MQTT_PUB_SIZE] = {	  
  {.at_cmd=(uint8_t*)"AT+QMTPUB="MQTT_ID",",            .at_resp=(uint8_t*)"> ",                        .AT_CMD_TYPE=BG96_CMD_MQTT_PUB_TOPIC},
  {.at_cmd=(uint8_t*)"",   				.at_resp=(uint8_t*)bg96_pub_sub_at_cmd_resp,    .AT_CMD_TYPE=BG96_CMD_MQTT_PUB_PAYLOAD,         .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=MQTT_TOTAL_TIMEOUT},
};

#define FIFO_MQTT_SUB_SIZE              3               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_MQTT_Subscribe[FIFO_MQTT_SUB_SIZE] = {		
  {.at_cmd=(uint8_t*)"AT+QMTSUB="MQTT_ID",",			.at_resp=(uint8_t*)bg96_pub_sub_at_cmd_resp,    .AT_CMD_TYPE=BG96_CMD_MQTT_SUBSCRIBE,		.AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=MQTT_TOTAL_TIMEOUT},
  {.at_cmd=(uint8_t*)"AT+QMTPUB="MQTT_ID",0,0,"RETAIN",",       .at_resp=(uint8_t*)"> ",    		        .AT_CMD_TYPE=BG96_CMD_MQTT_PUB_TOPIC_CLR},
  {.at_cmd=(uint8_t*)"",   					.at_resp=(uint8_t*)"+QMTPUB: ",    	        .AT_CMD_TYPE=BG96_CMD_MQTT_PUB_PAYLOAD_CLR,     .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=MQTT_TOTAL_TIMEOUT},
};

#define FIFO_CLOSE_CONN_SIZE            2               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_CloseConn[FIFO_CLOSE_CONN_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QMTDISC="MQTT_ID,      .at_resp=(uint8_t*)"+QMTDISC: "MQTT_ID",0",     .AT_CMD_TYPE=BG96_CMD_MQTT_DISCONN,     .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=10},
  {.at_cmd=(uint8_t*)"AT+QIDEACT="PDP_ID,   	.at_resp=(uint8_t*)"OK",    			.AT_CMD_TYPE=BG96_CMD_PDP_OFF, 		.AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=30},
};


MQTT_RxMsg_t bg96_MQTT_rx_msg;                                          //Parametri messaggio MQTT ricevuto
uint16_t bg96_MQTT_msgId_publish, bg96_MQTT_msgId_subscribe;
char bg96_pub_sub_at_cmd_resp[20];

BG96_AT_Result bg96_MQTT_result;                                        //Codice errore transazione MQTT (ASCII)



/* ############################################################ 
 * Local functions
 * ############################################################
*/
static uint8_t bg96_parse_mqtt_message(uint8_t* src, uint16_t src_offset, uint16_t src_size);




/* -------------------- Stato: BG96_MQTT_CONNECT -------------------- 
* Configurazione e apertura connessione MQTT.
* Get info cella.
*/
void BG96_MQTT_CONNECT_func(const uint32_t PERIODO)
{  
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset=0, offset_cell_start=0, offset_cell_end=0;
  
  bg96_tempo_connessione += PERIODO;

  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK))
    next_command = 1;
  else
  {
    //Esegui comando corrente
    err = bg96_send_command(fifo_MQTT_Connect, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
    {
      switch(fifo_MQTT_Connect[bg96_fifo_index].AT_CMD_TYPE)
      {			
        case BG96_CMD_RTC_DATE_TIME:
          memset(bg96_DataOra_UTC, 0x00, sizeof(bg96_DataOra_UTC));
          memcpy(bg96_DataOra_UTC, &mod_buff_Risposta[offset], sizeof(bg96_DataOra_UTC));
          break;
          
        case BG96_CMD_QUERY_PDP:        //contesto PDP gi� attivo, salta attivazione (CMD[2])
          bg96_cmd_MASK = 0x0004; 
          bg96_PDP_pending_cnt++; 
          break;
        
        case BG96_CMD_MQTT_OPEN:
          //Gestione codici errore MQTT
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_MQTT_result.result =  searchField_res[1].field[0];
          bg96_MQTT_result.err_code = 0x00;
          if(bg96_MQTT_result.result != '0')
          { 
            bg96_reg.BIT.MQTT_ERROR_CONN = 1;         //errore connessione
            set_bg96_status(BG96_ERR_S); 
            return; 
          }
          break;
        
        case BG96_CMD_MQTT_CONN:
          //Gestione codici errore MQTT
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_MQTT_result.result =  searchField_res[1].field[0];
          bg96_MQTT_result.err_code = searchField_res[2].field[0];
          if(bg96_MQTT_result.result!='0' || bg96_MQTT_result.err_code!=CONN_ACCEPTED) 
          {
            if(remote_service!=AZ_SERVICE_GP_BROKER &&                          //la connessione � verso i servizi Azure (IoT-Central o DPS) ...
               azure_reg.BIT.USE_BKP_UNIX_TIME==1 &&                            //sto utilizzando lo unix-time di backup per il calcolo del SAS token (i.e. sincronizzazione data/ora N/D) ...
                 bg96_MQTT_result.err_code==CONN_REFUSED_AUTHORIZATION)         //rilevo un errore di autorizzazione ...
            {
              //Il SAS token calcolato (unix-time backup) � scaduto e non ho ancora sincronizzazione data/ora
              //--> abilito calcolo SAS token con unix-time di emergenza
              azure_reg.BIT.USE_EMERGENCY_UNIX_TIME = 1;
            }
            
            if(remote_service==AZ_SERVICE_IOT_CENTRAL &&                        //la connessione � verso IoT-Central ...
               bg96_MQTT_result.err_code==CONN_REFUSED_SERVER_UNAVAIL)          //rilevo un errore di server non disponibile ...
            {
              az_iot_hub_failover = 1;
            }
            bg96_reg.BIT.MQTT_ERROR_CONN = 1;         //errore connessione
            set_bg96_status(BG96_ERR_S); 
            return; 
          }
          break;
          
        case BG96_CMD_RSSI:
          bg96_RSSI = convertStrToByte(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));								
          break;
          
        case BG96_CMD_INFO_CELLA:               
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_parse_info_cella(MOD_SERVING_CELL);
          bg96_num_celle = 1;                
          break;
          
        case BG96_CMD_INFO_CELLA_VICINA:
          offset_cell_start = 0;
          offset_cell_end = 0;
          for(bg96_num_celle=1; bg96_num_celle<MOD_MAX_CELL_NUM; )
          {
            if(searchString(mod_buff_Risposta, "+QENG: \"neighbourcell\",", offset_cell_start, sizeof(mod_buff_Risposta), &offset_cell_end, 1, 1))
            {
              //Match trovato: cerco il prossimo                    
              searchField(mod_buff_Risposta, ',', (offset_cell_start+offset_cell_end), sizeof(mod_buff_Risposta));
              bg96_parse_info_cella(bg96_num_celle);
              bg96_num_celle++;
              offset_cell_start += offset_cell_end;                    
            }
            else//Match NON trovato: esco dal ciclo
              break;            
          }
          break;
          
        default: break;
      }
      
      next_command = 1;													
    }
    else if(err == M_NO_AT_RESP)
    {
      bg96_critical_cmd_MASK = 0x0604;
      //--- Gestione eccezioni su errore ---
      //CMD[1]: contesto PDP non attivo --> eseguo attivazione (e.g. comando successivo)
      //CMD[2]: attivazione contesto PDP: CRITICO
      //CMD[5]: get info cella vicina --> eccezione su risposta in caso di tecnologia diversa da GSM, continuo con i comandi successivi
      //        (e.g. in questo caso il comando per celle neighbour NON E' SUPPORTATO)
      //CMD[9]: apertura connessione MQTT: CRITICO
      //CMD[10]: autenticazione client MQTT: CRITICO
      if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
      {
        bg96_reg.BIT.MQTT_ERROR_CONN = 1; 
        set_bg96_status(BG96_ERR_S);
        return;
      }
      else
        next_command = 1;      
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_MQTT_CONN_SIZE))
      set_bg96_status(BG96_IDLE_S);
  }
}


/* -------------------- Stato: BG96_MQTT_PUBLISH -------------------- 
* Publish MQTT.
*/
void BG96_MQTT_PUBLISH_func(const uint32_t PERIODO)
{  
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  bg96_tempo_connessione += PERIODO;
  
  err = bg96_send_command(fifo_MQTT_Publish, bg96_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {
    if(fifo_MQTT_Publish[bg96_fifo_index].AT_CMD_TYPE==BG96_CMD_MQTT_PUB_PAYLOAD && bg96_option_reg.BIT.MQTT_PUBLISH_WAIT_MSG_OPT)
    {
      bg96_parse_mqtt_message(mod_buff_Risposta, offset, sizeof(mod_buff_Risposta));
    }
    
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {
    set_bg96_status(BG96_ERR_S);
    bg96_reg.BIT.MQTT_ERROR_PUBLISH = 1;
    return;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_MQTT_PUB_SIZE))          
      set_bg96_status(BG96_IDLE_S);
  }
}


/* -------------------- Stato: BG96_MQTT_SUBSCRIBE -------------------- 
* Subscribe MQTT.
*/
void BG96_MQTT_SUBSCRIBE_func(const uint32_t PERIODO)
{  
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;

  bg96_tempo_connessione += PERIODO;
  
  if(!bg96_option_reg.BIT.MQTT_SUBSCRIBE_WAIT_MSG_OPT)
    bg96_cmd_MASK = 0x0006;     //salta pulizia messaggi retained (bypass CMD[1],[2])
  
  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK)) 
    next_command = 1;           //salta comando corrente
  else
  {
    err = bg96_send_command(fifo_MQTT_Subscribe, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
    {      
      if(fifo_MQTT_Subscribe[bg96_fifo_index].AT_CMD_TYPE==BG96_CMD_MQTT_SUBSCRIBE && bg96_option_reg.BIT.MQTT_SUBSCRIBE_WAIT_MSG_OPT)
      {
        bg96_parse_mqtt_message(mod_buff_Risposta, offset, sizeof(mod_buff_Risposta));
      }
      
      next_command = 1;
    }
    else if(err == M_NO_AT_RESP)
    {
      set_bg96_status(BG96_ERR_S);      //errore SUBSCRIBE o messaggio NON ricevuto
      bg96_reg.BIT.MQTT_ERROR_SUBSCRIBE = 1;
      return;
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_MQTT_SUB_SIZE))          
        set_bg96_status(BG96_IDLE_S);
  }
}


/* -------------------- Stato: BG96_CLOSE_CONN -------------------- 
* Chiusura connessione MQTT.
* Chiusura contesto PDP.
*/
void BG96_CLOSE_CONN_func(const uint32_t PERIODO)
{  
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  err = bg96_send_command(fifo_CloseConn, bg96_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
    next_command = 1;
  else if(err == M_NO_AT_RESP)
  {
    bg96_critical_cmd_MASK = 0x0002;
    //--- Gestione eccezioni su errore ---
    //CMD[0]: disconnessione MQTT fallita: proseguo comunque con disattivazione contesto PDP
    //CMD[1]: disattivazione contesto PDP: CRITICO
    if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
    {
      set_bg96_status(BG96_ERR_S);
      return;
    }
    else
      next_command = 1;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_CLOSE_CONN_SIZE))						
      set_bg96_status(BG96_IDLE_S);
  }
}

 
/* Estrapolazione messaggio MQTT ricevuto da risposta a comandi AT.
* Nota: vengono estrapolati sia il topic su cui il messaggio � stato ricevuto che il payload.
*
*@param[IN] src, sorgente dati parsing
*@param[IN] src_offset, offset in sorgente dati parsing
*@param[IN] src_size, dimensione (MAX) sorgente dati parsing
*
*return: 0=parsing fallito, 1=parsing OK
*/
static uint8_t bg96_parse_mqtt_message(uint8_t* src, uint16_t src_offset, uint16_t src_size)
{
  uint16_t idx_start, idx_end, dim=0;  
  
  //Clear topic & data buffer RX (per livello applicativo)
  memset(mod_buff_topic_RX, 0x00, sizeof(mod_buff_topic_RX));
  memset(mod_buff_data_RX, 0x00, sizeof(mod_buff_data_RX));     
  
  searchField(src, ',', src_offset, src_size);
  //Identifico topic RX
  idx_start = searchField_res[1].idx + 2;       //Bypass delimitatori ',' & '"'
  idx_end = searchField_res[2].idx - 2;         //Bypass delimitatori ',' & '"'
                                                //**NOTA**: se searchField_res[2].idx=0 --> idx_end=65534 e vengo protetto dai controlli sul range MAX interni a 'calcolaDelta'
  if(calcolaDelta(idx_start, idx_end, src_size, &dim) == M_NO_ERROR)        
  {
    bg96_MQTT_rx_msg.topic = &src[idx_start];   //Assegno handle inizio topic
    bg96_MQTT_rx_msg.topic_size = dim;          //Assegno dimensione topic
  }
  else
  {
    bg96_MQTT_rx_msg.topic = NULL;
    bg96_MQTT_rx_msg.topic_size = 0;
  }
  //Parse lunghezza payload
  bg96_MQTT_rx_msg.payload_size = convertStrToLong(searchField_res[3].field, 0, MAX_FIELD_SIZE); //Assegno dimensione payload
  //Identifico payload
  idx_start = searchField_res[3].idx + 2;                       //Bypass delimitatori ',' & '"'
  idx_end = idx_start + bg96_MQTT_rx_msg.payload_size - 1;      //Idem a **NOTA** (sopra)
  if(calcolaDelta(idx_start, idx_end, src_size, &dim) == M_NO_ERROR)
  {
    bg96_MQTT_rx_msg.payload = &src[idx_start]; //Assegno handle inizio payload
  }
  else
  {
    bg96_MQTT_rx_msg.payload = NULL;
    bg96_MQTT_rx_msg.payload_size = 0;
  }
  
  if(bg96_MQTT_rx_msg.topic_size>0 && bg96_MQTT_rx_msg.payload_size>0)
  {
    //Controlli saturazione buffer
    if(bg96_MQTT_rx_msg.topic_size > sizeof(mod_buff_topic_RX))
      bg96_MQTT_rx_msg.topic_size = sizeof(mod_buff_topic_RX);
    if(bg96_MQTT_rx_msg.payload_size > sizeof(mod_buff_data_RX)) 
      bg96_MQTT_rx_msg.payload_size = sizeof(mod_buff_data_RX);    
    //Copia topic & payload in topic & data buffer RX (per livello applicativo)
    memcpy(mod_buff_topic_RX, bg96_MQTT_rx_msg.topic, bg96_MQTT_rx_msg.topic_size);
    memcpy(mod_buff_data_RX, bg96_MQTT_rx_msg.payload, bg96_MQTT_rx_msg.payload_size);
    bg96_MQTT_rx_msg.received = 1;    
  }
  else
    bg96_MQTT_rx_msg.received = 0;
  
  return bg96_MQTT_rx_msg.received;
}
