/**
******************************************************************************
* File          	: modem_common.c
* Versione libreria	: 0.02
* Descrizione        	: Invio comandi AT & parsing risposte.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "modem_common.h"
#include "modem_utils.h"




Mod_CmdAttesa_t mod_cmdAttesa;
uint8_t mod_id_Seriale;

#ifdef MODEM_UART_LOG
uint8_t dbg_modem[100];
uint16_t dbg_modem_idx = 0;
#endif
uint8_t mod_buff_Risposta[MOD_MAX_BUFFER_RX_BOUND];
uint16_t mod_buff_Risposta_idx = 0;

uint8_t mod_buff_topic_RX[MOD_MAX_BUFFER_TOPIC_RX];
char mod_APN[MOD_APN_MAX_SIZE];                 //APN SIM

void (*mod_Write_seriale)(uint8_t id_seriale, uint8_t *src, uint16_t size);     //puntatore a driver per scrittura su seriale
void (*mod_Drive_reset)(uint8_t ON);				                //puntatore a driver pin RESET
void (*mod_Drive_power)(uint8_t ON);				                //puntatore a driver pin POWER
uint8_t (*mod_Read_status)(void);				                //puntatore a driver lettura pin STATUS

static GPTIMER mod_cmdTimer;			//timer
static uint8_t tentativo = 0;
static uint8_t mStatus = 0;


/* ############################################################ 
* Local functions
* ############################################################
*/
static ErrorCode_t mod_Wait_Response(const uint8_t timeout_ticks);
static void mod_Purge_BufferResponse(void);




/* Init gestione comandi AT. */
void mod_Init_AT(void)
{
  memset(&mod_cmdAttesa, 0x00, sizeof(mod_cmdAttesa));
  tentativo = 0;
  mStatus = 0;
}


/* Invio comando AT al modulo. 
*
*@param[IN]: cmd, comando da inviare
*@param[IN]: maxRetries, numero tentativi di invio comando
*@param[IN]: modulo
*/
void mod_Push_Cmd(uint8_t* cmd, uint8_t cmdCode, const uint16_t maxRetries, uint8_t modulo)
{
  uint16_t i, index_tailer, MAX_CMD_SIZE;
  
  if(!mod_cmdAttesa.attesaRisposta)
  {
    mod_Purge_BufferResponse();
    mod_cmdAttesa.attesaRisposta = 1;		
    
    //Push comando
    MAX_CMD_SIZE = sizeof(mod_cmdAttesa.lastCmd.cmd) - 1;       //-1 per considerare anche presenza di tailer comando
    memset(mod_cmdAttesa.lastCmd.cmd, 0x00, sizeof(mod_cmdAttesa.lastCmd.cmd));         
    for(i=0; cmd[i]!=0x00 && i<MAX_CMD_SIZE; i++)	        //push fino a terminatore stringa o raggiunta MAX dimensione buffer comando
      mod_cmdAttesa.lastCmd.cmd[i] = cmd[i];
    
    mod_cmdAttesa.lastCmd.cmdSize = i;                          //push dimensione comando
    mod_cmdAttesa.lastCmd.cmdCode = cmdCode;                    //push codice comando
    mod_cmdAttesa.lastCmd.maxRetryCmd = maxRetries;	        //push numero MAX retry invio comando
    mod_cmdAttesa.retryCmd++;
    
    //Aggiungo tailer
    index_tailer = mod_cmdAttesa.lastCmd.cmdSize;
    if(modulo == MOD_CTRL_Z_TAILER)
      mod_cmdAttesa.lastCmd.cmd[index_tailer++] = CTRL_Z_CHAR;
    else
      mod_cmdAttesa.lastCmd.cmd[index_tailer++] = CR_CHAR;    
    
    mod_Write_seriale(mod_id_Seriale, mod_cmdAttesa.lastCmd.cmd, index_tailer);         //invio comando						
  }
}

/* Reset buffer risposta. */
static void mod_Purge_BufferResponse(void)
{
  memset(mod_buff_Risposta, 0x00, sizeof(mod_buff_Risposta));
  mod_buff_Risposta_idx = 0;
}


/* Attendo timeout per lettura risposta a comando AT. */
static ErrorCode_t mod_Wait_Response(const uint8_t timeout_ticks)
{
  ErrorCode_t err = M_NO_ERROR;
  
  if(mod_cmdAttesa.attesaRisposta)
  { 
    switch(mStatus)
    {
      case 0:
        //Start timer timeout
        GPTIMER_stop(&mod_cmdTimer);
        GPTIMER_setTics(&mod_cmdTimer, MOD_MS_PER_TICKS*((uint16_t)timeout_ticks));
        GPTIMER_reTrigger(&mod_cmdTimer);        
        mStatus = 1;
        err = M_WAIT_AT_RESP;
        break;
        
      case 1: 
        //Wait
        if(GPTIMER_isTimedOut(&mod_cmdTimer))
        {
          mStatus = 0;
          err = M_TO_AT_END;
        }
        else err=M_WAIT_AT_RESP;
        break;
        
      default: break;
    }
  }
  else
    mStatus = 0;
  
  return err;
}

/* Cerco risposta attesa a comando AT. 
*
*@param[IN]: timeout_ticks, tempo atteso prima di cercare la risposta (espresso come ticks da MOD_MS_PER_TICKS ms)
*@param[IN]: maxRetries, numero tentativi ricerca risposta
*@param[IN]: resp, stringa risposta da cercare
*@param[OUT]: respOffset, indice offset dove termina la risposta
*@param[IN]: error, stringa errore da cercare in caso di mancata risposta
*
*@return: codice errore
*/
ErrorCode_t mod_Read_Response(const uint8_t timeout_ticks, const uint8_t maxRetries, uint8_t* resp, uint16_t* respOffset, uint8_t* error)
{
  ErrorCode_t err = M_NO_ERROR;
  uint16_t errorOffset = 0;  
  
  if(tentativo < maxRetries)
  {  
    err = mod_Wait_Response(timeout_ticks);
    if(err == M_TO_AT_END)
    {
      //-- Timeout concluso --
      if(searchString(mod_buff_Risposta, resp, 0, sizeof(mod_buff_Risposta),/*mod_buff_Risposta_idx,*/ respOffset, 1, 1))
      {
        //-- Risposta trovata --
        mod_cmdAttesa.attesaRisposta = 0;
        mod_cmdAttesa.erroreRisposta = 0;
        mod_cmdAttesa.codiceErrore = 0;
        mod_cmdAttesa.retryCmd = 0;
        tentativo = 0;				
        err = M_OK_AT_RESP;
      }
      else
      {
        //-- Risposta NON trovata --
        //Ricerca errore: limito la ricerca sull'ultima retry di invio comando
        if(searchString(mod_buff_Risposta, error, 0, sizeof(mod_buff_Risposta),/*mod_buff_Risposta_idx,*/ &errorOffset, 1, 1) &&
           mod_cmdAttesa.retryCmd>=mod_cmdAttesa.lastCmd.maxRetryCmd)
        {          
          mod_cmdAttesa.attesaRisposta = 0;
          mod_cmdAttesa.erroreRisposta = 1;
          mod_cmdAttesa.codiceErrore = convertStrToLong(&mod_buff_Risposta[errorOffset], errorOffset, sizeof(mod_buff_Risposta));
          mod_cmdAttesa.retryCmd = 0;
          tentativo = 0;
          err = M_NO_AT_RESP;   //errore trovato
        }
        else
          tentativo++;          //errore NON trovato
      }
    }
  }
  
  if(tentativo >= maxRetries)
  {
    //-- Risposta NON trovata @ fine ricerca risposta --
    mod_cmdAttesa.attesaRisposta = 0;
    mod_cmdAttesa.erroreRisposta = 1;
    mod_cmdAttesa.codiceErrore = MOD_DFLT_ERROR;    
    tentativo = 0;
    
    if(mod_cmdAttesa.retryCmd < mod_cmdAttesa.lastCmd.maxRetryCmd)
      err = M_NO_AT_RESP_RETRY;         //risposta NON trovata, retry invio comando
    else
    {
      mod_cmdAttesa.retryCmd = 0;
      err = M_NO_AT_RESP;		//risposta NON trovata
    }
  }
  
  return err;
}

/* Ignora risposta attesa a comando AT.
*
*@return: codice errore
*/
ErrorCode_t mod_Ignore_Response(void)
{
  ErrorCode_t err = M_NO_ERROR;
  
  mod_cmdAttesa.attesaRisposta = 0;
  mod_cmdAttesa.erroreRisposta = 0;
  mod_cmdAttesa.codiceErrore = 0;
  mod_cmdAttesa.retryCmd = 0;
  tentativo = 0;				
  err = M_OK_AT_RESP;
  
  return err;
}


/* Incrementa indice FIFO comandi. 
*
*@param[IN]: fifo_index, puntatore a indice FIFO corrente
*@param[IN]: FIFO_SIZE, dimensione FIFO
*
*@return: 1 se FIFO � terminata, 0 altrimenti
*/
uint8_t mod_Inc_Fifo(uint8_t* fifo_index, const uint8_t FIFO_SIZE)
{
  (*fifo_index)++;
  if(*fifo_index >= FIFO_SIZE)
  {
    *fifo_index = 0;
    return 1;
  }
  else
    return 0;
}
