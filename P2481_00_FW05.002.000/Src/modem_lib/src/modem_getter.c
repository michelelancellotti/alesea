/**
************************************************************************************
* File          	: modem_getter.c
* Versione libreria	: 0.02
* Descrizione        	: Interfaccia GET parametri modem (livello applicativo).
*
************************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
************************************************************************************
*/

#include "modem_getter.h"
#include "bg_96.h"


/* ############################################################ 
* Local functions
* ############################################################
*/


	

/* GET diagnostica modem (single byte).
*
*@param[IN]: type: tipologia diagnostica
*@param[OUT]: resp: diagnostica richiesta [1B]
*
*@return: 
*MA_OK (tipologia diagnostica valida)
*MA_ERROR (tipologia diagnostica non valida)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_Diagnostic(ModemDiagnostic_t type, 
                                  uint8_t* resp)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint8_t diagnostic[M_DNG_NUM];
  
  diagnostic[M_DGN_STATO_AUTOMA] = bg96_status;
  diagnostic[M_DNG_PWR_STATUS] = bg96_PWR;
  diagnostic[M_DNG_MQTT_ERR_RESULT] = bg96_MQTT_result.result;
  diagnostic[M_DNG_MQTT_ERR_RETCODE] = bg96_MQTT_result.err_code;
    
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(type < sizeof(diagnostic))
      {
        *resp = diagnostic[type];
        result = MA_OK;
      }
      else
        result = MA_ERROR;
      break;
      
    default: break;
  }
  
  return result;
}


/* GET versione FW modem.
*
*@param[IN]: max_resp_size: dimensione MAX buffer risposta	
*@param[OUT]: resp: stringa versione FW
*@param[OUT]: resp_size: dimensione stringa versione FW (MAX 29B)
*
*@return: 
*MA_OK
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_InfoFW(uint16_t max_resp_size, 
                              uint8_t* resp, uint16_t* resp_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint16_t free_space;
  int res = 0;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      free_space = max_resp_size;      
      res = snprintf((char*)resp, free_space, "%s_%s",
                     bg96_FW_info,
                     bg96_FW_info_sub);
      //terminatore di stringa aggiunto da 'snprintf' ...
      *resp_size = check_snprintf(res, &free_space);
      result = MA_OK;
      break;
    
    default: break;
  }
  
  return result;
}


/* GET tipologia modem [0=BG95,1=BG96].
*	
*@param[OUT]: resp: tipologia modem (1B)
*
*@return: 
*MA_OK (tipologia modem rilevata)
*MA_ERROR (tipologia modem non rilevata)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_Type(uint8_t* resp)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *resp = ModemType;
      
      if(bg96_reg.BIT.TYPE_DETECT == 1)
        result = MA_OK;         //tipologia modulo rilevata
      else
        result = MA_ERROR;      //tipologia modulo non rilevata (fallback a BG95)
      break;
      
    default: break;
  }
  
  return result;
}
	

/* GET ICCID SIM.
*	
*@param[OUT]: resp: ICCID
*@param[OUT]: resp_size: dimensione ICCID (20B)
*
*@return: 
*MA_OK (SIM rilevata)
*MA_ERROR (SIM non rilevata)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_ICCID(uint8_t* resp, uint16_t* resp_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *resp_size = sizeof(bg96_ICCID) - 1;              //-1 per escludere terminatore
      memcpy(resp, bg96_ICCID, sizeof(bg96_ICCID)-1);
      
      if(bg96_reg.BIT.SIM_RILEVATA == 1)
        result = MA_OK;         //SIM rilevata (ritorno valore corrente)
      else
        result = MA_ERROR;	//SIM non rilevata (ritorno ultimo valore disponibile)	
      break;
      
    default: break;
  }
  
  return result;
}


/* GET RSSI rete cellulare.
*	
*@param[OUT]: resp: RSSI rete cellulare (1B)
*
*@return: 
*MA_OK (registrazione rete completata)
*MA_ERROR (registrazione rete fallita)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_RSSI(uint8_t* resp)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *resp = bg96_RSSI;
      
      if(bg96_reg.BIT.STATO_REG_RETE == 1)
        result = MA_OK;				//registrazione rete COMPLETATA
      else
        result = MA_ERROR;			//registrazione rete  FALLITA (dato non significativo)	
      break;
      
    default: break;
  }
  
  return result;
}


/* GET tempi registrazione rete cellulare + connessione server MQTT [s].
*
*@param[OUT]: t_reg: tempo registrazione (2B)	
*@param[OUT]: t_conn: tempo connessione (2B)
*@param[OUT]: reg_status, registro di stato modem (2B)
*@param[OUT]: last_error, codice dell'ultimo errore rilevato (comandi AT) (1B)
*
*@return: 
*MA_OK (registrazione rete completata)
*MA_ERROR (registrazione rete fallita)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_TempoRete(uint16_t* t_reg, uint16_t* t_conn, uint16_t* reg_status, uint8_t* last_error)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint16_t res_app;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      res_app = (uint16_t)(bg96_tempo_registrazione/((uint32_t)1000));
      *t_reg = res_app;
      res_app = (uint16_t)(bg96_tempo_connessione/((uint32_t)1000));
      *t_conn = res_app;
      *reg_status = bg96_reg.WORD;
      *last_error = bg96_last_error;
      
      if(bg96_reg.BIT.STATO_REG_RETE == 1)
        result = MA_OK;				//registrazione rete COMPLETATA
      else
        result = MA_ERROR;			//registrazione rete  FALLITA
      break;
      
    default: break;
  }
  
  return result;
}


/* GET data/ora UTC [yy/mm/dd hh:mm:ss].
*	
*@param[OUT]: resp: stringa data/ora
*@param[OUT]: resp_size: dimensione stringa data/ora (17B o 0B se non disponibile)
*
*@return: 
*MA_OK (sincronizzazione NTP completata)
*MA_ERROR (sincronizzazione NTP fallita o data non disponibile)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_DataOra(uint8_t* resp, uint16_t* resp_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(bg96_is_RTC_sync())
      {        
        *resp_size = sizeof(bg96_DataOra_UTC);
        bg96_DataOra_UTC[8] = ' ';	//sostituisco ',' con ' ' (formattazione)
        memcpy(resp, bg96_DataOra_UTC, sizeof(bg96_DataOra_UTC));
        
        if(bg96_reg.BIT.NTP_SYNC == 1)
          result = MA_OK;		//sincronizzazione NTP COMPLETATA (ritorno valore corrente)
        else
          result = MA_ERROR;            //sincronizzazione NTP FALLITA (ritorno ultimo valore disponibile)
      }
      else
      {
        *resp_size = 0;
        resp[0] = 0x00;                 //data/ora non disponibile (add terminatore stringa)
        result = MA_ERROR;
      }
      break;
      
    default: break;
  }
  
  return result;
}


/* GET unix-time.
*	
*@param[OUT]: resp: unix-time
*
*@return: 
*MA_OK
*MA_ERROR (data non valida o non disponibile)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_UnixTime(uint32_t* resp)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  *resp = 0;
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:            
      if(bg96_is_RTC_sync())
      {
        *resp = bg96_get_unix_time();
        if(*resp == 0)
          result = MA_ERROR;    //data/ora non disponibile
        else
          result = MA_OK;
      }
      else
        result = MA_ERROR;      //data/ora non disponibile
      break;
      
    default: break;
  }
  
  return result;
}


/* GET info cella (localizzazione cellulare).
*
*@param[IN]: idx_cella: indice cella (0:SERVING-CELL, >0:NEIGHBOUR-CELL)
*@param[OUT]: tecnologia: stringa tecnologia corrente (array 8B)
*@param[OUT]: MCC: codice paese (2B)
*@param[OUT]: MNC: codice operatore (2B)
*@param[OUT]: cell_ID: ID cella agganciata (4B)
*@param[OUT]: LAC_TAC: codice d'area, LAC(GSM) o TAC(LTE) (2B)
*@param[OUT]: rx_lev: potenza segnale RX [dBm] (2B)
*
*@return: 
*MA_OK=indice cella richiesto valido
*MA_ERROR=indice cella richiesto non valido o superiore alle celle rilevate
*MA_INIT_ERROR=modem non gestito
*/	
ModemAppli_t modem_GET_InfoCella(uint8_t idx_cella, 
                                 uint8_t* tecnologia, uint16_t* MCC, uint16_t* MNC, uint32_t* cell_ID, uint16_t* LAC_TAC, int16_t* rx_lev)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint8_t i, idx=0;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(idx_cella<bg96_num_celle && idx_cella<MOD_MAX_CELL_NUM)
      {
        for(i=0; i<(sizeof(bg96_info_cella[idx_cella].tecnologia)-1); i++)
        {
          if(bg96_info_cella[idx_cella].tecnologia[i] != '"')			//filtro carattere '"'
            tecnologia[idx++] = bg96_info_cella[idx_cella].tecnologia[i];
        }
        *MCC = (uint16_t)(strtoul(bg96_info_cella[idx_cella].MCC, NULL, 10));
        *MNC = (uint16_t)(strtoul(bg96_info_cella[idx_cella].MNC, NULL, 10));
        *cell_ID = (uint32_t)(strtoul(bg96_info_cella[idx_cella].cell_ID, NULL, 16));
        *LAC_TAC = (uint16_t)(strtoul(bg96_info_cella[idx_cella].LAC_TAC, NULL, 16));
        *rx_lev = bg96_info_cella[idx_cella].rx_lev;
        
        result = MA_OK;         //indice cella richiesto valido
      }
      else
        result = MA_ERROR;      //indice cella richiesto non valido o superiore alle celle rilevate
      break;
      
    default: break;
  }
  
  return result;
}


/* GET info GNSS (localizzazione GNSS).
*
*@param[OUT]: latitudine  (4B)
*@param[OUT]: longitudine (4B)
*
*@return: 
*MA_OK (localizzazione completata)
*MA_ERROR (localizzazione fallita)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_InfoGNSS(float* latitudine, float* longitudine)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *latitudine = bg96_latitudine_f;
      *longitudine = bg96_longitudine_f;
      
      if(bg96_reg.BIT.LOC_GNSS == 1)					
        result = MA_OK;				//localizzazione COMPLETATA (ritorno valore corrente)
      else
        result = MA_ERROR;			//localizzazione FALLITA (ritorno ultimo valore disponibile)
      break;
      
    default: break;
  }
  
  return result;
}


/* GET info GNSS PRO (localizzazione GNSS precisione aumentata).
*
*@param[OUT]: latitudine  (4B)
*@param[OUT]: longitudine (4B)
*
*@return: 
*MA_OK (localizzazione PRO completata)
*MA_ERROR (localizzazione PRO fallita)
*MA_INIT_ERROR (modem non gestito)
*/
ModemAppli_t modem_GET_InfoGNSS_PRO(float* latitudine, float* longitudine)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      *latitudine = bg96_latitudine_f_PRO;
      *longitudine = bg96_longitudine_f_PRO;
      
      if(bg96_gnss_reg.BIT.LOC_GNSS_PRO == 1)					
        result = MA_OK;				//localizzazione PRO COMPLETATA (ritorno valore corrente)
      else
        result = MA_ERROR;			//localizzazione PRO FALLITA (ritorno ultimo valore disponibile)
      break;
      
    default: break;
  }
  
  return result;
}


/* GET tempo aggancio GNSS [s].
*	
*@param[OUT]: resp: tempo aggancio GNSS (2B)
*@param[OUT]: status_reg: registro di stato GNSS (2B)
*
*@return: 
*MA_OK (localizzazione completata)
*MA_ERROR (localizzazione fallita)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_TempoGNSS(uint16_t* resp, uint16_t* status_reg)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint16_t res_app;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      res_app = (uint16_t)(bg96_tempo_GNSS/((uint32_t)1000));
      *resp = res_app;
      *status_reg = bg96_gnss_reg.WORD;
      
      if(bg96_reg.BIT.LOC_GNSS == 1)					
        result = MA_OK;				//localizzazione COMPLETATA (ritorno valore corrente)
      else
        result = MA_ERROR;			//localizzazione FALLITA (ritorno ultimo valore disponibile)	
      break;
      
    default: break;
  }
  
  return result;
}


/* GET messaggio MQTT ricevuto (SUBSCRIBE).
*	
*@param[OUT]: resp: messaggio MQTT ricevuto
*@param[OUT]: resp_size: dimensione messaggio MQTT ricevuto (N bytes, max MOD_MAX_BUFFER_DATA_RX bytes)	
*
*@return: 
*MA_OK (messaggio MQTT ricevuto)
*MA_ERROR (messaggio MQTT non ricevuto o non disponibile)
*MA_INIT_ERROR (modem non gestito)
*/	
ModemAppli_t modem_GET_MsgMQTT(uint8_t* resp, uint16_t* resp_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(bg96_MQTT_rx_msg.received)
      {
        //Messaggio MQTT ricevuto
        *resp_size = bg96_MQTT_rx_msg.payload_size;
        memcpy(resp, mod_buff_data_RX, bg96_MQTT_rx_msg.payload_size);
        result = MA_OK;
      }
      else
      {
        //Messaggio MQTT non ricevuto o non disponibile
        *resp_size = 0;
        result = MA_ERROR;
      }
      break;
      
    default: break;
  }
  
  return result;
}

