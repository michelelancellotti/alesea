/**
  ******************************************************************************
  * File          	: bg_96_network.c
  * Versione libreria	: 0.02
  * Descrizione       	: Gestione network:
  *                       - Registrazione rete
  *                       - Configurazione/attivazione contesto PDP		    						
  *                       - Sincronizzazione NTP
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "bg_96_network.h"
#include "bg_96_command.h"
    



/* Tabella che mappa il numero di giorni presenti fino ad un certo mese (su un periodo di 4 anni) */      
const uint16_t DAYS_UP_TO_MONTH[4][12] = {                              //X=indice mese(i.e. indice colonna), Y=indice anno (i.e. indice riga)  
  {   0,  31,  60,  91, 121, 152, 182, 213, 244, 274, 305, 335},        //Y=0 --> anno bisestile
  { 366, 397, 425, 456, 486, 517, 547, 578, 609, 639, 670, 700},        //Y=1 --> anno bisestile+1
  { 731, 762, 790, 821, 851, 882, 912, 943, 974,1004,1035,1065},        //Y=2 --> anno bisestile+2
  {1096,1127,1155,1186,1216,1247,1277,1308,1339,1369,1400,1430},        //Y=3 --> anno bisestile+3
};    
/* Tabella indici risultati per info celle */    
const BG96_Cell_Idx CELL_FIELD_IDX[4] = {                               
  {.tech=1, .MCC=2, .MNC=3, .LAC_TAC=4, .cell_ID=5, .rx_lev=9},         //GSM - serving
  {.tech=1, .MCC=3, .MNC=4, .LAC_TAC=11, .cell_ID=5, .rx_lev=14},       //LTE - serving
  {.tech=0, .MCC=1, .MNC=2, .LAC_TAC=3, .cell_ID=4, .rx_lev=7},         //GSM - neighbour  
  {.tech=0, .MCC=2, .MNC=3, .LAC_TAC=10, .cell_ID=4, .rx_lev=5},        //LTE - neighbour (non significativo: parsing comando per celle neighbour
                                                                        //in tecnologie diverse da GSM NON E' SUPPORTATO)
};


#define FIFO_CONFIG_RAT_SIZE            4
static Mod_Fifo_Entry_t fifo_ConfigRAT[FIFO_CONFIG_RAT_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+CFUN=0",        		                .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_AIRPLANE_MODE,                            .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=15},
  {.at_cmd=(uint8_t*)"AT+QCFG=\"nwscanmode\","SET_RAT_SM_AUTO,          .at_resp=(uint8_t*)"OK",	.AT_CMD_TYPE=BG96_CMD_RAT_SCAN_MODE},   
  {.at_cmd=(uint8_t*)"AT+QCFG=\"iotopmode\","SET_RAT_LTE_CATM_ONLY,     .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_RAT_LTE_MODE},
  {.at_cmd=(uint8_t*)"AT+CFUN=1",                                       .at_resp=(uint8_t*)"OK",	.AT_CMD_TYPE=BG96_CMD_FULL_FUNC_MODE,   .AT_CMD_RETRY=2,        .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=15},
};

#define FIFO_NETWORK_REG_SIZE           4               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK, bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_NetworkReg[FIFO_NETWORK_REG_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+CFUN?",        .at_resp=(uint8_t*)"+CFUN: 1",          .AT_CMD_TYPE=BG96_CMD_QUERY_PHONE_MODE},
  {.at_cmd=(uint8_t*)"AT+CFUN=1",       .at_resp=(uint8_t*)"OK",		.AT_CMD_TYPE=BG96_CMD_FULL_FUNC_MODE,           .AT_CMD_RETRY=2,     .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=15},
  {.at_cmd=(uint8_t*)"AT+CREG?",        .at_resp=(uint8_t*)"+CREG: ",           .AT_CMD_TYPE=BG96_CMD_REG_RETE,                                      .AT_RESP_TIMEOUT=BG96_1000MS_TO},
  {.at_cmd=(uint8_t*)"AT+CSQ",          .at_resp=(uint8_t*)"+CSQ: ",		.AT_CMD_TYPE=BG96_CMD_RSSI},
};

#define FIFO_PDP_SIZE             	6		//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK, bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_PDP_Config[FIFO_PDP_SIZE] = {	
  {.at_cmd=(uint8_t*)"AT+CCLK?",            			.at_resp=(uint8_t*)"+CCLK: \"",			.AT_CMD_TYPE=BG96_CMD_RTC_DATE_TIME}, 
  {.at_cmd=(uint8_t*)"AT+QICSGP="PDP_ID",1,",                   .at_resp=(uint8_t*)"OK",			.AT_CMD_TYPE=BG96_CMD_CFG_PDP},  	
  {.at_cmd=(uint8_t*)"AT+QIACT?",   				.at_resp=(uint8_t*)"+QIACT: "PDP_ID",1",        .AT_CMD_TYPE=BG96_CMD_QUERY_PDP, 		.AT_RESP_TIMEOUT=BG96_1000MS_TO},		
  {.at_cmd=(uint8_t*)"AT+QIACT="PDP_ID,   			.at_resp=(uint8_t*)"OK",    			.AT_CMD_TYPE=BG96_CMD_PDP_ON, 			.AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=90},  
  {.at_cmd=(uint8_t*)"AT+QNTP="PDP_ID","NTP_SERV_ADDR,		.at_resp=(uint8_t*)"+QNTP: 0",			.AT_CMD_TYPE=BG96_CMD_NTP,			.AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=30},
  {.at_cmd=(uint8_t*)"AT+CCLK?",				.at_resp=(uint8_t*)"+CCLK: \"",			.AT_CMD_TYPE=BG96_CMD_RTC_DATE_TIME},
};


uint8_t bg96_stato_registrazione_rete = IGNOTO;         //Stato registrazione rete cellulare
uint8_t bg96_RSSI = 99;					//RSSI rete cellulare
uint32_t bg96_tempo_registrazione = 0;                  //Tempo registrazione rete cellulare [ms]
uint32_t bg96_tempo_connessione = 0;		        //Tempo connessione (apertura contesto PDP + connessione server MQTT) [ms]
Mod_InfoCellaStr_t bg96_info_cella[MOD_MAX_CELL_NUM];	//Info celle serving+neighbour (stringa)
uint8_t bg96_num_celle;                                 //Numero celle serving+neighbour
char bg96_DataOra_UTC[BG96_DATE_TIME_SIZE];	        //Data + Ora UTC (stringa)
uint8_t bg96_tentativi_registrazione_rete = 0;
uint8_t bg96_PDP_pending_cnt = 0;

uint32_t bg96_rat_cnt = 0;                              //Contatore impostazioni RAT
BG96_RAT_Scan_Mode_t bg96_RAT_scan_mode = RAT_SM_NULL;  //RAT scan mode
BG96_RAT_LTE_Mode_t bg96_RAT_LTE_mode = RAT_LTE_NULL;   //RAT LTE mode


/* ############################################################ 
 * Local functions
 * ############################################################
*/




/* -------------------- Stato: BG96_CONFIG_RAT -------------------- 
* Configurazione Radio Access Technology (RAT).
*/
void BG96_CONFIG_RAT_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  
  err = bg96_send_command(fifo_ConfigRAT, bg96_fifo_index, &offset);
  if(err==M_OK_AT_RESP || err==M_NO_AT_RESP)        
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_CONFIG_RAT_SIZE))
      set_bg96_status(BG96_IDLE_S);
  }
  //N.B.
  //Impedisco che questo stato generi errori: i comandi vengono eseguiti tutti in ogni caso
  //Questo gestisce in automatico la condizione in cui non si riceve risposta ad AT+CFUN=0 (accade a volte)
}


/* -------------------- Stato: BG96_NETWORK_REG -------------------- 
* Get info registrazione rete: stato registrazione.
*/
void BG96_NETWORK_REG_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  bg96_tempo_registrazione += PERIODO;
  
  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK))
    next_command = 1;           //salta comando corrente
  else
  {
    //Esegui comando corrente    
    err = bg96_send_command(fifo_NetworkReg, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
    {
      switch(fifo_NetworkReg[bg96_fifo_index].AT_CMD_TYPE)
      {
        case BG96_CMD_REG_RETE:
          searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
          bg96_stato_registrazione_rete = searchField_res[1].field[0];
          if(bg96_stato_registrazione_rete==REGISTRATO_HOME || bg96_stato_registrazione_rete==REGISTRATO_ROAMING)							
            bg96_reg.BIT.STATO_REG_RETE = 1;    //registrazione completata (passo al prossimo comando in coda)
          else            
            bg96_reg.BIT.STATO_REG_RETE = 0;    //registrazione NON completata
          break;
          
        case BG96_CMD_QUERY_PHONE_MODE: bg96_cmd_MASK=0x0002; break;        //full functionality gi� attiva, salta attivazione (CMD[1])
        
        case BG96_CMD_RSSI:
          bg96_RSSI = convertStrToByte(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));								
          break;
        
        default: break;
      }				
      
      next_command = 1;      
    }
    else if(err == M_NO_AT_RESP)
    {
      bg96_critical_cmd_MASK = 0x0000;
      //--- Gestione eccezioni su errore ---
      //CMD[0]: full functionality non attiva --> eseguo attivazione (e.g. comando successivo)
      //CMD[1]: ripristino full functionality fallita: continuo con i comandi successivi
      if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
      {
        set_bg96_status(BG96_ERR_S);
        return;
      }
      else
        next_command = 1;
    }
  }
  
  //Gestione retry comando richiesta registrazione (ignoro errori su comando richiesta registrazione)
  if(fifo_NetworkReg[bg96_fifo_index].AT_CMD_TYPE==BG96_CMD_REG_RETE && 
     (err==M_OK_AT_RESP || err==M_NO_AT_RESP))
  {
    bg96_tentativi_registrazione_rete++;
    if(bg96_reg.BIT.STATO_REG_RETE == 1) 
      next_command = 1;         //registrazione avvenuta
    else
    {
      //Registrazione NON avvenuta
      if(bg96_tentativi_registrazione_rete < BG96_MAX_RETRY_REG_RETE) 
        return;                 //continuo con i tentativi di richiesta registrazione
      else 
        next_command = 1;       //tentativi di richiesta registrazione terminati
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_NETWORK_REG_SIZE))
    {
      if(bg96_reg.BIT.STATO_REG_RETE == 1)
        set_bg96_status(BG96_PDP_ON_S);
      else
        set_bg96_status(BG96_IDLE_S);
    }
  }
}


/* -------------------- Stato: BG96_PDP_ON -------------------- 
* Configurazione e attivazione contesto PDP.
* Sincronizzazione data/ora NTP.
* Get RSSI.
*/
void BG96_PDP_ON_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  bg96_tempo_connessione += PERIODO;
  
  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK))
    next_command = 1;                   //salta comando corrente
  else
  {
    //Esegui comando corrente    
    err = bg96_send_command(fifo_PDP_Config, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
    {
      switch(fifo_PDP_Config[bg96_fifo_index].AT_CMD_TYPE)
      {
        case BG96_CMD_NTP: 
          bg96_reg.BIT.NTP_SYNC = 1;    //sincronizzazione NTP completata
          break;
          
        case BG96_CMD_RTC_DATE_TIME:
          memset(bg96_DataOra_UTC, 0x00, sizeof(bg96_DataOra_UTC));
          memcpy(bg96_DataOra_UTC, &mod_buff_Risposta[offset], sizeof(bg96_DataOra_UTC));
          break;
          
        case BG96_CMD_QUERY_PDP:
          bg96_cmd_MASK = 0x0008;       //contesto PDP gi� attivo, salta attivazione (CMD[3])
          bg96_PDP_pending_cnt++;
          break;
        
        default: break;
      }
      
      next_command = 1;						
    }
    else if(err == M_NO_AT_RESP)
    {
      bg96_critical_cmd_MASK = 0x0008;
      //--- Gestione eccezioni su errore ---
      //CMD[2]: contesto PDP non attivo --> eseguo attivazione (e.g. comando successivo)
      //CMD[3]: attivazione contesto PDP: CRITICO
      //CMD[4]: sincronizzazione NTP fallita: continuo con i comandi successivi
      if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
      {
        bg96_reg.BIT.PDP_ERROR = 1;
        set_bg96_status(BG96_ERR_S);
        return;
      }
      else
        next_command = 1;
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_PDP_SIZE))						
        set_bg96_status(BG96_IDLE_S);
  }
}


/* Ritorna 1 se l'RTC interno al modem � stato sincronizzato (almeno una volta), 0 altrimenti. */
uint8_t bg96_is_RTC_sync(void)
{
  uint8_t sync = 0;
  
  if(bg96_DataOra_UTC[0]=='8' && bg96_DataOra_UTC[1]=='0') sync=0;		//anno pari a default RTC
  else if(bg96_DataOra_UTC[0]==0 && bg96_DataOra_UTC[1]==0) sync=0;		//anno non disponibile (e.g. dopo POR uC)
  else sync=1;  
  
  return sync;
}

/* Converte la data/ora corrente in unix-time.
*
*@return unix-time, unix-time corrente oppure 0 per significare dato N/D
*/
uint32_t bg96_get_unix_time(void)
{
  #define DAYS_PER_4_YEAR       1461            //numero di giorni presenti su un quadriennio, incluso 1 anno bisestile (365*4+1)
  #define UT_2000	        946684800	//unix time @ 01/01/2000 00:00:00
  
  uint8_t dt_buffer[6];
  uint8_t offset, yy, mm, dd;
  uint32_t days_from_2000_to_now, unix_time;
  
  //Conversione da carattere a numero 
  for(uint8_t i=0; i<sizeof(dt_buffer); i++)
  {
    offset = 3*i;
    dt_buffer[i] = convertStrToByte((unsigned char*)&bg96_DataOra_UTC[offset], offset, sizeof(bg96_DataOra_UTC));
  }
  
  yy = dt_buffer[0]; 
  mm = dt_buffer[1];
  dd = dt_buffer[2];
  if(yy<80 && (mm>=1&&mm<=12) && (dd>=1&&dd<=31) && dt_buffer[3]<=23 && dt_buffer[4]<=59 && dt_buffer[5]<=59)
  {
    //--- Data valida ---
    mm--;       //rimappo [0-11]
    dd--;       //rimappo [0-30]
    //Calcolo i giorni trascorsi dal 01/01/2000 alla data corrente (calcolo a quadrienni)
    days_from_2000_to_now = (yy/4)*DAYS_PER_4_YEAR +            //giorni per numero di quadrienni (parte intera), fino al mese corrente
                            DAYS_UP_TO_MONTH[yy%4][mm] +        //giorni per numero di quadrienni (parte frazionaria), fino al mese corrente
                            dd;                                 //giorni del mese corrente
    //Calcolo epoch-time dal 01/01/2000 00:00:00
    unix_time = (((days_from_2000_to_now*24) + dt_buffer[3])*60 + dt_buffer[4])*60 + dt_buffer[5];
    //Calcolo unix-time
    unix_time += UT_2000;
  }
  else
  {
    //--- Data non valida (dovuta ad errore o mancata sincronizzazione) ---
    unix_time = 0;
  }
  return unix_time;
}


/* Parsing comando AT info cella. */
void bg96_parse_info_cella(uint8_t idx_cella)
{
  const BG96_Cell_Idx* lista_res_idx;         //puntatore a lista da cui recuperare gli indici (posizionali) dei campi
  uint8_t offset_lista = 0;
  
  if(idx_cella >= MOD_MAX_CELL_NUM)
    return;
  
  if(idx_cella == MOD_SERVING_CELL) offset_lista=0;     //GSM - serving
  else offset_lista=2;                                  //GSM - neighbour
  
  lista_res_idx = &CELL_FIELD_IDX[offset_lista];
  memset(&bg96_info_cella[idx_cella], 0x00, sizeof(bg96_info_cella[idx_cella]));						
  memcpy(bg96_info_cella[idx_cella].tecnologia, searchField_res[lista_res_idx->tech].field, sizeof(bg96_info_cella[idx_cella].tecnologia));
  if(memcmp(&bg96_info_cella[idx_cella].tecnologia[1], "GSM", 3) == 0)
  {
    //--- GSM ---
    memcpy(bg96_info_cella[idx_cella].MCC, searchField_res[lista_res_idx->MCC].field, sizeof(bg96_info_cella[idx_cella].MCC));
    memcpy(bg96_info_cella[idx_cella].MNC, searchField_res[lista_res_idx->MNC].field, sizeof(bg96_info_cella[idx_cella].MNC));
    memcpy(bg96_info_cella[idx_cella].LAC_TAC, searchField_res[lista_res_idx->LAC_TAC].field, sizeof(bg96_info_cella[idx_cella].LAC_TAC));
    memcpy(bg96_info_cella[idx_cella].cell_ID, searchField_res[lista_res_idx->cell_ID].field, sizeof(bg96_info_cella[idx_cella].cell_ID));
    bg96_info_cella[idx_cella].rx_lev = (int16_t)(strtol((char const*)searchField_res[lista_res_idx->rx_lev].field, NULL, 10));
  }
  else
  {
    //--- LTE ---
    offset_lista++;     //sposto puntatore su liste LTE
    lista_res_idx = &CELL_FIELD_IDX[offset_lista];
    //Nota: parsing comando per celle neighbour in tecnologie diverse da GSM NON E' SUPPORTATO
    memcpy(bg96_info_cella[idx_cella].MCC, searchField_res[lista_res_idx->MCC].field, sizeof(bg96_info_cella[idx_cella].MCC));
    memcpy(bg96_info_cella[idx_cella].MNC, searchField_res[lista_res_idx->MNC].field, sizeof(bg96_info_cella[idx_cella].MNC));
    memcpy(bg96_info_cella[idx_cella].LAC_TAC, searchField_res[lista_res_idx->LAC_TAC].field, sizeof(bg96_info_cella[idx_cella].LAC_TAC));
    memcpy(bg96_info_cella[idx_cella].cell_ID, searchField_res[lista_res_idx->cell_ID].field, sizeof(bg96_info_cella[idx_cella].cell_ID));
    bg96_info_cella[idx_cella].rx_lev = (int16_t)(strtol((char const*)searchField_res[lista_res_idx->rx_lev].field, NULL, 10));
  }
}
