/**
******************************************************************************
* File          	: at_common.c
* Versione libreria	: 0.02
* Descrizione        	: Interfaccia comune modem/modulo WiFi.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "at_common.h"




uint8_t mod_buff_data_TX[MOD_MAX_BUFFER_DATA_TX];       //buffer appoggio dati TX (comune a modem/modulo WiFi)
uint8_t mod_buff_data_RX[MOD_MAX_BUFFER_DATA_RX];       //buffer appoggio dati RX (comune a modem/modulo WiFi)


/* ############################################################ 
* Local functions
* ############################################################
*/




/* GET handle buffer invio dati [modem-->PUBLISH MQTT, WiFi-->write socket TCP].
*	
*@param[OUT]: h_data_tx: handle buffer invio dati (livello driver)
*@param[OUT]: data_tx_size: dimensione buffer invio dati (livello driver)
*
*@return: 
*0 (parametri input validi)
*-1 (parametri input non validi)
*/	
int8_t mod_GET_TxDataBuffer(uint8_t** h_data_tx, uint16_t* data_tx_size)
{
  int8_t result = -1;
  
  if(h_data_tx && data_tx_size)
  {
    *h_data_tx = mod_buff_data_TX;
    *data_tx_size = sizeof(mod_buff_data_TX);
    result = 0;
  }  
  
  return result;
}

