/**
  ******************************************************************************
  * File          	: bg_96_tls.c
  * Versione libreria	: 0.02
  * Descrizione       	: Configurazione parametri TLS.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2022 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "bg_96_tls.h"
#include "bg_96_mqtt.h"
#include "bg_96_command.h"




#define FIFO_TLS_SIZE             	6               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK, bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_TLS[FIFO_TLS_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QSSLCFG=\"seclevel\","TLS_ID",0",              .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_TLS_AUTH_MODE},
  {.at_cmd=(uint8_t*)"AT+QSSLCFG=\"sslversion\","TLS_ID",4",            .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_TLS_VERSION},
  {.at_cmd=(uint8_t*)"AT+QSSLCFG=\"ciphersuite\","TLS_ID",0xFFFF",      .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_TLS_CIPHER},
  {.at_cmd=(uint8_t*)"AT+QSSLCFG=\"ignorelocaltime\","TLS_ID",1",       .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_TLS_CERT_CHECK},
  {.at_cmd=(uint8_t*)"AT+QMTCFG=\"ssl\","MQTT_ID",0",                   .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_TLS_ENB},
  {.at_cmd=(uint8_t*)"AT+QMTCFG=\"ssl\","MQTT_ID",1,"TLS_ID,            .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_TLS_ENB},
};


/* ############################################################ 
 * Local functions
 * ############################################################
*/




/* -------------------- Stato: BG96_CONFIG_TLS -------------------- 
* Configurazione parametri TLS.
*/
void BG96_CONFIG_TLS_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  uint32_t unix_time;

  if(bg96_option_reg.BIT.ENABLE_TLS)
    bg96_cmd_MASK = 0x0010;     //attiva TLS (bypass disattivazione TLS, CMD[4])
  else
    bg96_cmd_MASK = 0x002F;     //salta configurazione TLS e disattiva TLS (bypass CMD[0][1][2][3][5])    

  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK))
    next_command = 1;           //salta comando corrente
  else
  {
    //Esegui comando corrente
    err = bg96_send_command(fifo_TLS, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
      next_command = 1;
    else if(err == M_NO_AT_RESP)
    {
      bg96_critical_cmd_MASK = 0x0030;
      //--- Gestione eccezioni su errore ---
      //CMD[4]: disattivazione TLS: CRITICO
      //CMD[5]: attivazione TLS: CRITICO
      if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
      {
        bg96_reg.BIT.TLS_CONF_ERROR = 1;
        set_bg96_status(BG96_ERR_S);
        return;
      }
      else
        next_command = 1;
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_TLS_SIZE))
    {
      if(bg96_option_reg.BIT.HTTP_APP_LAYER_OPT)
        set_bg96_status(BG96_HTTP_S);
      else
      {
        //---- Calcolo le credenziali solo se la connessione (MQTT) � verso i servizi Azure ----
        switch(remote_service)
        {
          case AZ_SERVICE_IOT_CENTRAL:
          case AZ_SERVICE_DPS:
            unix_time = bg96_get_unix_time();
            //(entry point libreria Azure)
            if(remote_service == AZ_SERVICE_DPS)
              az_calculate_credentials(unix_time, DPS_TOKEN);
            else
              az_calculate_credentials(unix_time, STANDARD_TOKEN);
            break;
          default: break;
        }        
        set_bg96_status(BG96_MQTT_CONNECT_S);
      }
    }
  }
}
