/**
******************************************************************************
* File          	: bg_96_command.c
* Versione libreria	: 0.02
* Descrizione       	: Gestore invio comandi modem Quectel BG96/BG95.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "bg_96_command.h"
#include "bg_96_ftp.h"
#include "bg_96_http.h"
#include "bg_96_mqtt.h"




MQTT_Client_t* MQTT_Client;

/* Buffer appoggio composizione comandi AT. */
static BG96_Buffer_t bg96_buffer;
static uint8_t* TOPIC;

/* ############################################################ 
* Local functions
* ############################################################
*/




/* Invia comando da coda e attende ricezione risposta. */
ErrorCode_t bg96_send_command(Mod_Fifo_Entry_t* fifo, uint8_t fifo_index, uint16_t* offset_resp)
{
  ErrorCode_t err;
  uint8_t AT_CMD_RETRY, AT_RESP_TIMEOUT, AT_RESP_RETRY, modulo;
  uint8_t* at_cmd;
  uint16_t offset=0, free_space=0;
  int res = 0;
  
  //Assegnazione comando (default, da coda comandi)
  at_cmd = fifo[fifo_index].at_cmd;
  
  //Assegnazione retry invio comando
  if(fifo[fifo_index].AT_CMD_RETRY == 0)
    AT_CMD_RETRY = 1;                   //default, se non assegnato
  else
    AT_CMD_RETRY = fifo[fifo_index].AT_CMD_RETRY;
  
  //Assegnazione modulo (default)
  modulo = ModemType;
  
  //Assegnazione timeout risposta
  if(fifo[fifo_index].AT_RESP_TIMEOUT == 0) 
    AT_RESP_TIMEOUT = BG96_300MS_TO;    //default, se non assegnato
  else
    AT_RESP_TIMEOUT = fifo[fifo_index].AT_RESP_TIMEOUT;
  
  //Assegnazione retry ricerca risposta
  if(fifo[fifo_index].AT_RESP_RETRY == 0)
    AT_RESP_RETRY = 1;                  //default, se non assegnato                  
  else
    AT_RESP_RETRY = fifo[fifo_index].AT_RESP_RETRY;
  
  //Add terminatore stringa all'inizio del buffer di appoggio:
  //--> pulizia veloce del buffer di appoggio e di TUTTI i suoi figli (union di buffer)
  bg96_buffer.MQTT_buff[0] = 0x00;
  
  switch(fifo[fifo_index].AT_CMD_TYPE)
  {
    case BG96_CMD_CFG_PDP:
      offset += concatenaStringhe(bg96_buffer.gp_settings_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.gp_settings_buff));         //add CMD header
      free_space = sizeof(bg96_buffer.gp_settings_buff) - offset;
      res = snprintf((char*)&bg96_buffer.gp_settings_buff[offset], free_space, "\"%s\",\"\",\"\",0", mod_APN);                                  //add APN
      offset += check_snprintf(res, &free_space);
      at_cmd = bg96_buffer.gp_settings_buff;
      break;
    
    case BG96_CMD_MQTT_CFG_TIMEOUT:
      offset += concatenaStringhe(bg96_buffer.MQTT_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.MQTT_buff));       //add CMD header
      free_space = sizeof(bg96_buffer.MQTT_buff) - offset;
      res = snprintf((char*)&bg96_buffer.MQTT_buff[offset], free_space, "%d,%d,0",
                     MQTT_PKT_TIMEOUT,                                                                                          //add 'pkt_timeout' MQTT
                     MQTT_RETRY_TIMES);                                                                                         //add 'retry_times' MQTT
      offset += check_snprintf(res, &free_space);
      at_cmd = bg96_buffer.MQTT_buff;
      break;
    
    case BG96_CMD_MQTT_OPEN:
      offset += concatenaStringhe(bg96_buffer.MQTT_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.MQTT_buff));       //add CMD header
      free_space = sizeof(bg96_buffer.MQTT_buff) - offset;
      res = snprintf((char*)&bg96_buffer.MQTT_buff[offset], free_space, "\"%s\",%d", 
                     MQTT_Client->host_name,                                                                                    //add host-name broker 
                     MQTT_Client->port);                                                                                        //add porta broker
      offset += check_snprintf(res, &free_space);
      at_cmd = bg96_buffer.MQTT_buff;
      break;
      
    case BG96_CMD_MQTT_CONN:
      offset += concatenaStringhe(bg96_buffer.MQTT_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.MQTT_buff));       //add CMD header
      free_space = sizeof(bg96_buffer.MQTT_buff) - offset;
      res = snprintf((char*)&bg96_buffer.MQTT_buff[offset], free_space, "\"%s\",\"%s\",\"%s\"",
                     MQTT_Client->client_id,                                                                                    //add client-ID (ICCID)
                     MQTT_Client->user_name,                                                                                    //add user-name broker
                     MQTT_Client->password);                                                                                    //add password broker
      offset += check_snprintf(res, &free_space);
      at_cmd = bg96_buffer.MQTT_buff;
      break;
      
    case BG96_CMD_MQTT_PUB_PAYLOAD:     //Publish standard (payload)
    case BG96_CMD_MQTT_PUB_PAYLOAD_CLR: //Publish per pulizia messaggio RETAINED (payload vuoto)
      modulo = MOD_CTRL_Z_TAILER;
      if(fifo[fifo_index].AT_CMD_TYPE == BG96_CMD_MQTT_PUB_PAYLOAD)
        at_cmd = mod_buff_data_TX;
      break;
      
    case BG96_CMD_MQTT_PUB_TOPIC:       //Publish standard (TOPIC)
    case BG96_CMD_MQTT_PUB_TOPIC_CLR:   //Publish per pulizia messaggio RETAINED (TOPIC)
      offset += concatenaStringhe(bg96_buffer.MQTT_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.MQTT_buff));       //add CMD header
      if(fifo[fifo_index].AT_CMD_TYPE == BG96_CMD_MQTT_PUB_TOPIC)
      {
        free_space = sizeof(bg96_buffer.MQTT_buff) - offset;
        res = snprintf((char*)&bg96_buffer.MQTT_buff[offset], free_space, "%d,%d,%s,",
                       bg96_MQTT_msgId_publish,                                                                                 //add msg-ID
                       MQTT_QoS_PUBLISH,                                                                                        //add QoS
                       NOT_RETAIN);                                                                                             //add retain flag
        offset += check_snprintf(res, &free_space);
        TOPIC = MQTT_TOPIC_OUT;
      }
      else
        TOPIC = MQTT_TOPIC_IN;             
      if(offset<sizeof(bg96_buffer.MQTT_buff)-1) bg96_buffer.MQTT_buff[offset++]='"';
      offset += concatenaStringhe(bg96_buffer.MQTT_buff, offset, TOPIC, sizeof(bg96_buffer.MQTT_buff));                         //add TOPIC
      if(offset<sizeof(bg96_buffer.MQTT_buff)-1) bg96_buffer.MQTT_buff[offset++]='"';
      at_cmd = bg96_buffer.MQTT_buff;      
      break;
      
    case BG96_CMD_MQTT_SUBSCRIBE:
      offset += concatenaStringhe(bg96_buffer.MQTT_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.MQTT_buff));       //add CMD header
      free_space = sizeof(bg96_buffer.MQTT_buff) - offset;
      res = snprintf((char*)&bg96_buffer.MQTT_buff[offset], free_space, "%d,\"", bg96_MQTT_msgId_subscribe);                    //add Msg-Id
      offset += check_snprintf(res, &free_space);
      offset += concatenaStringhe(bg96_buffer.MQTT_buff, offset, MQTT_TOPIC_IN, sizeof(bg96_buffer.MQTT_buff));                 //add TOPIC
      free_space = sizeof(bg96_buffer.MQTT_buff) - offset;
      res = snprintf((char*)&bg96_buffer.MQTT_buff[offset], free_space, "\",%d", MQTT_QoS_SUBSCRIBE);                           //add QoS
      offset += check_snprintf(res, &free_space);
      at_cmd = bg96_buffer.MQTT_buff;
      break;
      
    case BG96_CMD_HTTP_CFG_URL:
    case BG96_CMD_HTTP_CFG_POST_HEADER:
      offset += concatenaStringhe(bg96_buffer.HTTP_FTP_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.HTTP_FTP_buff));       //add CMD header
      free_space = sizeof(bg96_buffer.HTTP_FTP_buff) - offset;
      if(fifo[fifo_index].AT_CMD_TYPE == BG96_CMD_HTTP_CFG_URL)
        res = snprintf((char*)&bg96_buffer.HTTP_FTP_buff[offset], free_space, "%d,%d",
                       (sizeof(HTTP_API_URL_ADDR)-1),                                                                                   //add dimensione URL
                       HTTP_INPUT_TIMEOUT);                                                                                             //add timeout input URL
      else
        res = snprintf((char*)&bg96_buffer.HTTP_FTP_buff[offset], free_space, "%d,%d,%d",
                       bg96_HTTP_request_size,                                                                                          //add dimensione richiesta (header+body)
                       HTTP_INPUT_TIMEOUT,                                                                                              //add timeout input richiesta HTTP
                       HTTP_POST_TIMEOUT);                                                                                              //add timeout risposta POST HTTP                                              
      offset += check_snprintf(res, &free_space);
      at_cmd = bg96_buffer.HTTP_FTP_buff;
      break;
      
    case BG96_CMD_HTTP_POST_HEADER:
    case BG96_CMD_FILE_WRITE:
      at_cmd = mod_buff_data_TX;
      break;
      
    case BG96_CMD_FTP_GET_FILE:
      offset += concatenaStringhe(bg96_buffer.HTTP_FTP_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.HTTP_FTP_buff));       //add CMD header
      free_space = sizeof(bg96_buffer.HTTP_FTP_buff) - offset;
      res = snprintf((char*)&bg96_buffer.HTTP_FTP_buff[offset], free_space, "\"%s", FTP_FILE);		                                //add file name
      offset += check_snprintf(res, &free_space);
      res = snprintf((char*)&bg96_buffer.HTTP_FTP_buff[offset], free_space, "_%01d_%02d.bin\",",
                     bg96_FTP_version,                                                                                                  //add version
                     bg96_FTP_subversion);                                                                                              //add subversion
      offset += check_snprintf(res, &free_space);
      res = snprintf((char*)&bg96_buffer.HTTP_FTP_buff[offset], free_space, "%s", LOCAL_UFS_FILE);	                                //add UFS file name
      offset += check_snprintf(res, &free_space);
      at_cmd = bg96_buffer.HTTP_FTP_buff;
      break;
        
    case BG96_CMD_GNSS_CONST:
      offset += concatenaStringhe(bg96_buffer.gp_settings_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.gp_settings_buff)); //add CMD header
      free_space = sizeof(bg96_buffer.gp_settings_buff) - offset;
      res = snprintf((char*)&bg96_buffer.gp_settings_buff[offset], free_space, "%d", bg96_gnss_constellation);                          //add tipo costellazione
      offset += check_snprintf(res, &free_space);
      at_cmd = bg96_buffer.gp_settings_buff;
      break;
      
    case BG96_CMD_FILE_MOVE_TO_SOF:
    case BG96_CMD_FILE_READ:
    case BG96_CMD_CFG_FILE_WRITE:
    case BG96_CMD_FILE_CLOSE:
      offset += concatenaStringhe(bg96_buffer.file_buff, offset, fifo[fifo_index].at_cmd, sizeof(bg96_buffer.file_buff));       //add CMD header
      free_space = sizeof(bg96_buffer.file_buff) - offset;
      res = snprintf((char*)&bg96_buffer.file_buff[offset], free_space, "%d", bg96_file_handle);                                //add file handle
      offset += check_snprintf(res, &free_space);
      if(fifo[fifo_index].AT_CMD_TYPE == BG96_CMD_FILE_MOVE_TO_SOF)
      {
        res = snprintf((char*)&bg96_buffer.file_buff[offset], free_space, "%s", ",0,0");                                        //add posizione puntatore
        offset += check_snprintf(res, &free_space);
      }
      else if(fifo[fifo_index].AT_CMD_TYPE == BG96_CMD_FILE_READ)
      {
        res = snprintf((char*)&bg96_buffer.file_buff[offset], free_space, ",%d", bg96_file_read.size);                          //add numero byte da leggere
        offset += check_snprintf(res, &free_space); 
      }
      else if(fifo[fifo_index].AT_CMD_TYPE == BG96_CMD_CFG_FILE_WRITE)
      {
        res = snprintf((char*)&bg96_buffer.file_buff[offset], free_space, ",%d", bg96_file_write.size);                         //add numero byte da scrivere
        offset += check_snprintf(res, &free_space); 
      }
      at_cmd = bg96_buffer.file_buff;
      break;
      
    default: break;
  }
  

  //Add terminatore di stringa sul buffer di appoggio
  if(offset < sizeof(bg96_buffer))
    bg96_buffer.MQTT_buff[offset] = 0x00;
  else
    bg96_buffer.MQTT_buff[sizeof(bg96_buffer)-1] = 0x00;
  
  
  //Invia comando
  mod_Push_Cmd(at_cmd, fifo[fifo_index].AT_CMD_TYPE, AT_CMD_RETRY, modulo);
  err = mod_Read_Response(AT_RESP_TIMEOUT, AT_RESP_RETRY, fifo[fifo_index].at_resp, offset_resp, AT_ERROR_STR);
  
  return err;
}
