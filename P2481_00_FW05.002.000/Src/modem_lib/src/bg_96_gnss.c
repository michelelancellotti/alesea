/**
  ******************************************************************************
  * File          	: bg_96_gnss.c
  * Versione libreria	: 0.02
  * Descrizione       	: Gestione localizzazione GNSS.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "bg_96_gnss.h"
#include "bg_96_command.h"




#define FIFO_GNSS_CFG_SIZE          	2               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_GNSS_Config[FIFO_GNSS_CFG_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QGPS=1,1",             .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_GNSS_ON},
  {.at_cmd=(uint8_t*)"AT+QGPS=1,255,,,",        .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_GNSS_ON},
};

#define FIFO_GNSS_OFF_SIZE          	1
static Mod_Fifo_Entry_t fifo_GNSS_OFF[FIFO_GNSS_OFF_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QGPSEND",             .at_resp=(uint8_t*)"OK",        .AT_CMD_TYPE=BG96_CMD_GNSS_OFF},
};


BG96_GNSS_Reg_t bg96_gnss_reg;
float bg96_latitudine_f, bg96_longitudine_f;				        //Latitudine + Longitudine (float)
float bg96_latitudine_f_PRO, bg96_longitudine_f_PRO;			        //Latitudine + Longitudine (float): precisione aumentata
uint32_t bg96_tempo_GNSS = 0;                                                   //Tempo connessione GNSS [ms]
uint16_t bg96_timeout_GNSS = BG96_GNSS_TIMEOUT_DFLT;                            //Timeout GNSS [s]
uint8_t bg96_timeout_GNSS_PRO = BG96_GNSS_PRO_TIMEOUT_DFLT;		        //Timeout GNSS PRO (T=5s*bg96_timeout_GNSS_PRO)
uint8_t bg96_gnss_constellation = GPS_GLONASS;                                  //Tipologia costellazione (BG95-only)
uint8_t bg96_tentativi_GNSS_PRO=0, bg96_lettura_GNSS_PRO=0;

static char bg96_latitudine[BG96_LAT_SIZE], bg96_longitudine[BG96_LONG_SIZE];   //Latitudine + Longitudine (stringa)


/* ############################################################ 
 * Local functions
 * ############################################################
*/
static ErrorCode_t bg96_GNSS_Location(uint16_t* offsetResp, uint8_t enh_prec);
static void bg96_parse_location(float* latitudine_f, float* longitudine_f);




/* -------------------- Stato: BG96_GNSS_ON -------------------- 
* Accensione/Configurazione GNSS.
*/
void BG96_GNSS_ON_func(const uint32_t PERIODO)
{  
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  bg96_tempo_GNSS = 0;
  
  if(ModemType == MOD_BG96)
    bg96_cmd_MASK = 0x0001;     //bypass CMD[0]
  else
    bg96_cmd_MASK = 0x0002;     //bypass CMD[1]
  
  if(bg96_bypass_cmd(bg96_fifo_index, bg96_cmd_MASK))
    next_command = 1;           //salta comando corrente
  else
  {
    //Esegui comando corrente
    err = bg96_send_command(fifo_GNSS_Config, bg96_fifo_index, &offset);
    if(err == M_OK_AT_RESP)
    {
      if(fifo_GNSS_Config[bg96_fifo_index].AT_CMD_TYPE == BG96_CMD_GNSS_ON)
        bg96_gnss_reg.BIT.STATO_GNSS = 1;               //GNSS --> ON						
      
      next_command = 1;
    }
    else if(err == M_NO_AT_RESP)
    {
      //--- Gestione eccezioni su errore ---
      if(mod_cmdAttesa.codiceErrore == EC_GNSS_ALREADY_ACTIVE)
      {
        bg96_gnss_reg.BIT.STATO_GNSS = 1;               //GNSS --> ON		
        set_bg96_status(BG96_GNSS_LOCATION_S);
        return;
      }						
      else
      {
        set_bg96_status(BG96_ERR_S);
        return;
      }
    }
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_GNSS_CFG_SIZE))
      set_bg96_status(BG96_GNSS_LOCATION_S);
  }
}


/* -------------------- Stato: BG96_GNSS_LOCATION -------------------- 
* Lettura GNSS.
*/
void BG96_GNSS_LOCATION_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  
  bg96_tempo_GNSS += PERIODO;           //incremento tempo connessione GNSS
  
  err = bg96_GNSS_Location(&offset, 0);
  if(err == M_OK_AT_RESP)
  {				
    searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
    bg96_parse_location(&bg96_latitudine_f, &bg96_longitudine_f);
    bg96_reg.BIT.LOC_GNSS = 1;
    if(bg96_gnss_reg.BIT.PRO_OPT) 
      set_bg96_status(BG96_GNSS_LOCATION_PRO_S);
    else 
      set_bg96_status(BG96_GNSS_OFF_S);
  }
  else if(err == M_NO_AT_RESP)
  {
    bg96_reg.BIT.LOC_GNSS = 0;
    set_bg96_status(BG96_ERR_S);
  }
}


/* -------------------- Stato: BG96_GNSS_LOCATION_PRO -------------------- 
* Lettura GNSS: aumento precisione.
*/
void BG96_GNSS_LOCATION_PRO_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  
  bg96_tempo_GNSS += PERIODO;           //incremento tempo connessione GNSS
  
  err = bg96_GNSS_Location(&offset, 1);
  if(err==M_OK_AT_RESP || err==M_NO_AT_RESP)
  {        
    if(err == M_OK_AT_RESP)
    {
      bg96_lettura_GNSS_PRO++;
      searchField(mod_buff_Risposta, ',', offset, sizeof(mod_buff_Risposta));
      bg96_parse_location(&bg96_latitudine_f_PRO, &bg96_longitudine_f_PRO);
    }
    if(++bg96_tentativi_GNSS_PRO < bg96_timeout_GNSS_PRO) 
      return;      //resto nello stato corrente (retry comando corrente)
    else
    {
      //Validazione efficacia aumento precisione (% di successo)
      if(bg96_lettura_GNSS_PRO >= (((uint16_t)bg96_tentativi_GNSS_PRO)*((uint16_t)50))/100) bg96_gnss_reg.BIT.LOC_GNSS_PRO=1;
      else bg96_gnss_reg.BIT.LOC_GNSS_PRO=0;
      set_bg96_status(BG96_GNSS_OFF_S);
    }
  }
}


/* -------------------- Stato: BG96_GNSS_OFF -------------------- 
* Spegnimento GNSS.
*/
void BG96_GNSS_OFF_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint16_t offset = 0;
  
  err = bg96_send_command(fifo_GNSS_OFF, bg96_fifo_index, &offset); 
  if(err == M_OK_AT_RESP)
  {
    if(fifo_GNSS_OFF[bg96_fifo_index].AT_CMD_TYPE == BG96_CMD_GNSS_OFF)
      bg96_gnss_reg.BIT.STATO_GNSS = 0;         //GNSS --> OFF
    
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_GNSS_OFF_SIZE))
      set_bg96_status(BG96_IDLE_S);
  }
  else if(err == M_NO_AT_RESP)
  {
    //--- Gestione eccezioni su errore ---
    if(mod_cmdAttesa.codiceErrore == EC_GNSS_SESSION_NOT_ACTIVE)
    {
      bg96_gnss_reg.BIT.STATO_GNSS = 0;		//GNSS --> OFF (gi� spento)
      set_bg96_status(BG96_IDLE_S);
    }
    else
      set_bg96_status(BG96_ERR_S);
  }
}


/* Lettura info GNSS.
*
* @param enh_prec:'0' precisione standard,'1': precisione aumentata
*
* Note: in questo caso utilizzo una funzione dedicata al posto di 'bg96_send_command', per maggiore leggibilit� del codice.
*/
static ErrorCode_t bg96_GNSS_Location(uint16_t* offsetResp, uint8_t enh_prec)
{
  ErrorCode_t err;
  
  if(enh_prec)
  {
    mod_Push_Cmd((uint8_t*)"AT+QGPSLOC=2", BG96_CMD_GNSS_LOC, 1, ModemType);
    err = mod_Read_Response(BG96_5000MS_TO, 1, (uint8_t*)"+QGPSLOC: ", offsetResp, AT_ERROR_STR);
  }
  else
  {
    mod_Push_Cmd((uint8_t*)"AT+QGPSLOC=2", BG96_CMD_GNSS_LOC, bg96_timeout_GNSS, ModemType); 
    err = mod_Read_Response(BG96_1000MS_TO, 1, (uint8_t*)"+QGPSLOC: ", offsetResp, AT_ERROR_STR);
  }
  
  return err;
}


/* Parsing comando AT localizzazione. */
static void bg96_parse_location(float* latitudine_f, float* longitudine_f)
{  				  						
  memset(bg96_latitudine, 0x00, sizeof(bg96_latitudine)); 
  *latitudine_f = 0.0;
  memset(bg96_longitudine, 0x00, sizeof(bg96_longitudine)); 
  *longitudine_f = 0.0;	
  memcpy(bg96_latitudine, searchField_res[1].field, sizeof(bg96_latitudine));		//parse latitudine
  *latitudine_f = atof(bg96_latitudine);
  memcpy(bg96_longitudine, searchField_res[2].field, sizeof(bg96_longitudine));         //parse longitudine
  *longitudine_f = atof(bg96_longitudine);
}
