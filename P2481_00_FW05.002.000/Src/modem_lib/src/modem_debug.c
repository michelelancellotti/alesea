/**
******************************************************************************
* File          	: modem_debug.c
* Versione libreria	: 0.02
* Descrizione        	: Debug libreria modem.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "modem_debug.h"
#include "modem.h"
#include "main.h"
#include "Globals.h"
#include "bg_96_common.h"


#ifdef ENB_COMMUNICATION_DEBUG
extern uint8_t VersioneHW;

static char debug_msg[32] = "AT+QFDEL=\"devkey.txt\"";
static uint8_t cmd_debug_trigger;
static uint8_t dbg_gnss[5] = {
  0x00,         //MODE: STD vs ASSISTED
  0x2C,0x01,    //TIMEOUT
  0x02,         //PREC: STD vs PRO
  0x02          //TIMEOUT PRO
};
static uint8_t dbg_enb_azure = 1;
static ModemTrigger_t dbg_trigger;


/* Funzioni locali. */


/* Trigger manuali: DEBUG modem */
void modem_debug_trigger(void)
{
  static uint16_t msg_num;
  int16_t cnt;
  uint8_t param[2];  
  
  /*static uint8_t first_time = 1;
  if(first_time == 1)
  {
    cmd_debug_trigger = 2;
    first_time = 0;
  }*/
  
  static ModemAppli_t ec;
  ec = modem_GET_Status(dbg_trigger);
    
  
  if(cmd_debug_trigger == 1)
  {
    /* Reset buffer debug */
#ifdef MODEM_UART_LOG
    memset(dbg_modem, 0x00, sizeof(dbg_modem));
    dbg_modem_idx = 0;
#endif
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 2)
  {
    /* Test modem ON */
    if(VersioneHW == VERSIONE_HW_4_X)
    {
      HAL_GPIO_WritePin(BOOST_EN_HW4_GPIO_Port, BOOST_EN_HW4_Pin, GPIO_PIN_SET); //accendo switching
      HAL_Delay(2000);
      HAL_GPIO_WritePin(MDM_ON_OFF_HW4_GPIO_Port, MDM_ON_OFF_HW4_Pin, GPIO_PIN_SET); //alimento il modem
      HAL_Delay(2000);
    }
    dbg_trigger = TRG_MODEM_ON;
    modem_Trigger(dbg_trigger, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 3)
  {
    /* Test modem OFF */
    dbg_trigger = TRG_MODEM_OFF;
    modem_Trigger(dbg_trigger, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 4)
  {
    /* Test apertura contesto PDP */
    dbg_trigger = TRG_SYNC_RETE;
    modem_Trigger(dbg_trigger, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 5)
  {
    /* Test connessione MQTT */
    dbg_trigger = TRG_CONNETTI_MQTT;
    modem_Trigger(dbg_trigger, &dbg_enb_azure, 1);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 6)
  {
    /* Test PUBLISH MQTT */
    memset(debug_msg, 0x00, sizeof(debug_msg));
    cnt = snprintf(debug_msg, sizeof(debug_msg), "{\"d0\":%d,\"d1\":%d}", ++msg_num, 20);
    if(cnt > 0)
    {
      dbg_trigger = TRG_INVIA_MQTT;
      modem_Trigger(dbg_trigger, (uint8_t*)debug_msg, cnt);
    }
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 7)
  {
    /* Test SUBSCRIBE MQTT */
    dbg_trigger = TRG_RICEVI_MQTT;
    modem_Trigger(dbg_trigger, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 8)
  {
    /* Test chiusura connessione MQTT */
    dbg_trigger = TRG_CHIUDI_RETE;
    modem_Trigger(dbg_trigger, NULL, NULL);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 9)
  {
    /* Test GNSS */
    //HAL_GPIO_WritePin(GPS_AMPLI_GPIO_Port, GPS_AMPLI_Pin, GPIO_PIN_SET);        //accensione ampli
    dbg_trigger = TRG_GNSS_ON;
    modem_Trigger(dbg_trigger, dbg_gnss, sizeof(dbg_gnss));
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 10)
  {
    /* Test connessione FTP */
    param[0] = 5;
    param[1] = 2;
    dbg_trigger = TRG_FW_UPDATE;
    modem_Trigger(dbg_trigger, param, 2);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 11)
  {
    /* Test impostazioni GP */
    dbg_trigger = TRG_GP_SETTINGS;
    modem_Trigger(dbg_trigger, dbg_gnss, 1);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 12)
  {
    /* Invio comando generico */
    for(cnt=0; cnt<sizeof(debug_msg) && debug_msg[cnt]!=0x00; cnt++);      //Cerca fine stringa
    if(cnt >= sizeof(debug_msg))
      cnt = sizeof(debug_msg) - 2;      
    debug_msg[cnt++] = CR_CHAR;
    mod_Write_seriale(mod_id_Seriale, (uint8_t*)debug_msg, cnt);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 13)
  {
    /* Test registrazione dispositivo (DPS) */
    dbg_trigger = TRG_REGISTRAZIONE_DEV;
    modem_Trigger(dbg_trigger, &dbg_enb_azure, 1);
    cmd_debug_trigger = 0;
  }
  else if(cmd_debug_trigger == 14)
  {
    /* Test switching modem OFF */
    if(VersioneHW == VERSIONE_HW_4_X)
    {
      HAL_GPIO_WritePin(MDM_ON_OFF_HW4_GPIO_Port, MDM_ON_OFF_HW4_Pin, GPIO_PIN_RESET); //disalimento il modem
      HAL_Delay(2000);
      HAL_GPIO_WritePin(BOOST_EN_HW4_GPIO_Port, BOOST_EN_HW4_Pin, GPIO_PIN_RESET); //spengo switching
      HAL_Delay(2000);      
    }
    cmd_debug_trigger = 0;
  }
}
#endif
