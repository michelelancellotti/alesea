/**
******************************************************************************
* File          	: modem.c
* Versione libreria	: 0.02
* Descrizione        	: Supporto Modem (livello applicativo).
*
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

//### INCLUDE applicazione ###
#include "main.h"
#include "usart.h"
#include "EEPROM.h"
#include "Globals.h"
//############################

#include "modem.h"
#include "bg_96.h"
#include "az_client.h"
#include "az_utils.h"


//Handle modem
Mod_t ModemType = MOD_BG95;
void (*modem_Automa)(const uint32_t PERIODO);
void (*modem_ProcessaByte)(uint8_t data);


//### Risorse applicazione ###

/* Parametri client MQTT per connessione a broker legacy (general-purpose). */
static MQTT_Client_t GP_Broker = {
  .host_name =  "alesea-lb.westeurope.cloudapp.azure.com",
                //"\"40.119.157.149\""
  .port =       61613,  
  .user_name =  "zetes",
  .password =   "ZqUiD897",
  .client_id =  "ND",
};
/* Parametri client MQTT per connessione a Broker IoT Central. */
static MQTT_Client_t IoT_Central_Broker = {
  .host_name =  az_iot_hub_hostname_buff,
  .port =       8883,  
  .user_name =  az_username_buff,
  .password =   az_sas_token_buff,
  .client_id =  "ND",
};
/* Parametri client MQTT per connessione a Broker DPS (Device Provisioning System). */
static MQTT_Client_t DPS_Broker = {
  .host_name =  "global.azure-devices-provisioning.net",
  .port =       8883,  
  .user_name =  az_username_buff,
  .password =   az_sas_token_buff,
  .client_id =  "ND",
};


Az_Service_t remote_service = AZ_SERVICE_GP_BROKER;             //Tipologia di connessione ai servizi remoti
uint8_t MQTT_TOPIC_OUT[MOD_MAX_TOPIC_OUT];
uint8_t MQTT_TOPIC_IN[MOD_MAX_TOPIC_IN];
Az_Buffer_t publish_topic = {
  .buffer=MQTT_TOPIC_OUT, .offset=0, .size=sizeof(MQTT_TOPIC_OUT)
};
Az_Buffer_t subscribe_topic = {
  .buffer=MQTT_TOPIC_IN, .offset=0, .size=sizeof(MQTT_TOPIC_IN)
};

uint8_t Local_Devkey_Counter;


/* ############################################################ 
* Local functions
* ############################################################
*/
static void Modem_Reset_Pin(uint8_t ON);
static void Modem_Power_Pin(uint8_t ON);
static uint8_t Modem_Read_Status_Pin(void);
static void dummy_fun_arg0(const uint32_t src);
static void dummy_fun_arg1(uint8_t src);
static Az_Err_t modem_set_topic(uint8_t is_publish, uint8_t* device_id);
static Az_Err_t modem_int_operation(Az_Operation_t operation);
static Az_Err_t modem_GET_int_operation_status(Az_Operation_t operation);
static uint16_t modem_set_HTTP_body(uint8_t* dest, uint16_t* free_space);
ModemAppli_t modem_set_HTTP_request(void);




/* Inizializza libreria modem: assegnazione driver seriale e reset GPIO + allocazione risorse. 
*
*@param[IN]: modello: tipologia modem (MOD_BG95, MOD_BG96)
*@param[IN]: versione_HW: versione HW scheda
*@param[IN]: sottoversione_HW: sotto-versione HW scheda
*
*@return:
*M_NO_ERROR
*M_ERR_MODEL_INIT (tipologia modem non gestita)
*/
ErrorCode_t modem_Init(Mod_t modello, uint8_t versione_HW, uint8_t sottoversione_HW)
{
  ErrorCode_t err = M_NO_ERROR;  

  modem_Automa = dummy_fun_arg0;		
  modem_ProcessaByte = dummy_fun_arg1;
  
  if(modello!=MOD_BG95 && modello!=MOD_BG96)
  {
    if(versione_HW==VERSIONE_HW_3_X && sottoversione_HW==SOTTO_VERSIONE_HW_3_X_BASE)
      ModemType = MOD_BG96;
    else
    {
      err = M_ERR_MODEL_INIT;
      ModemType = MOD_BG95;   //default	
    }	
  }
  else  
    ModemType = modello;
       
  //Assegnazione risorse HW
  mod_id_Seriale = SERIALE_MODEM;
  mod_Write_seriale = Write_SCI;
  mod_Drive_power = Modem_Power_Pin;
  mod_Drive_reset = Modem_Reset_Pin;
  mod_Read_status = Modem_Read_Status_Pin; 
  
  //Assegnazione APN SIM
  if(versione_HW==VERSIONE_HW_4_X && sottoversione_HW==SOTTO_VERSIONE_HW_4_X_COBIRA)
    snprintf(mod_APN, sizeof(mod_APN), "%s", APN_ADDR_COBIRA);
  else
    snprintf(mod_APN, sizeof(mod_APN), "%s", APN_ADDR_1NCE);
  
  //Init tipologia servizi remoti  
  set_connection_service(AZ_SERVICE_IOT_CENTRAL);
  //Init libreria Azure
  Az_Buffer_t data_publish = {
    .buffer=mod_buff_data_TX, .offset=0, .size=sizeof(mod_buff_data_TX)
  };
  Az_Buffer_t received_topic = {
    .buffer=mod_buff_topic_RX, .offset=0, .size=sizeof(mod_buff_topic_RX)
  };
  Az_Buffer_t data_subscribe = {
    .buffer=mod_buff_data_RX, .offset=0, .size=sizeof(mod_buff_data_RX)
  };
  Az_Buffer_t data_devkey = {
    .buffer=mod_buff_data_RX, .offset=0, .size=sizeof(mod_buff_data_RX)
  };
  Az_Buffer_t data_calculated_devkey = {
    .buffer=mod_buff_data_TX, .offset=0, .size=sizeof(mod_buff_data_TX)
  };
  Az_Op_Callback_t cbk = {
    .exec_operation=modem_int_operation,
    .operation_status=modem_GET_int_operation_status
  };
  az_init(publish_topic, subscribe_topic, received_topic, 
          data_publish, data_subscribe, 
          data_devkey, data_calculated_devkey, cbk);

  //Assegnazione macchina a stati	
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      modem_Automa = bg96_Automa;
      modem_ProcessaByte = bg96_ProcessaByte;			
      init_bg96(versione_HW, sottoversione_HW);
      break;
    default: err=M_ERR_MODEL_INIT; break;
  }  
  
  return err; 
}

static void dummy_fun_arg0(const uint32_t src){}
static void dummy_fun_arg1(uint8_t src){}
	

/* Pilotaggio pin reset modem. */
static void Modem_Reset_Pin(uint8_t ON)
{
  if(ON) HAL_GPIO_WritePin(MODEM_RST_PORT, MODEM_RST_PIN, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(MODEM_RST_PORT, MODEM_RST_PIN, GPIO_PIN_RESET);
}

/* Pilotaggio pin Power modem. */
static void Modem_Power_Pin(uint8_t ON)
{
  if(ON) HAL_GPIO_WritePin(MODEM_PWR_PORT, MODEM_PWR_PIN, GPIO_PIN_SET);
  else HAL_GPIO_WritePin(MODEM_PWR_PORT, MODEM_PWR_PIN, GPIO_PIN_RESET);
}

/* Lettura pin Status. */
static uint8_t Modem_Read_Status_Pin(void)
{
  if(HAL_GPIO_ReadPin(STATUS_MODEM_GPIO_Port, STATUS_MODEM_Pin) == GPIO_PIN_RESET) return 1;
  else return 0;
}


//#################### Applicazione ####################


/* Trigger azioni modem: interfaccia livello applicativo. 
*
*@param[IN]: trigger: tipologia operazione richiesta a modem
*@param[IN]: data: buffer parametri
*@param[IN]: data_size: dimensione buffer parametri
*
*@return:
*MA_TRIG_OK (operazione richiesta gestita)
*MA_INIT_ERROR (tipologia modem non gestita o errore parametri input)
*MA_BUSY (modulo impegnato in altre operazioni)
*/
ModemAppli_t modem_Trigger(ModemTrigger_t trigger, uint8_t* data, uint16_t data_size)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint16_t u16=0, i;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(trigger == TRG_MODEM_ON)       //trigger asincrono 
      { 
        set_bg96_status(BG96_PWR_ON_S);
        azure_reg_ext.WORD = 0;         //reset diagnostica Azure (estesa)
        result = MA_TRIG_OK; 
      }
      else if(trigger==TRG_MODEM_OFF) { set_bg96_status(BG96_PWR_OFF_S); result=MA_TRIG_OK; }           //trigger asincrono
      else if(trigger==TRG_MODEM_ON_RESET) { set_bg96_status(BG96_PWR_ON_RESET_S); result=MA_TRIG_OK; } //trigger asincrono
      else if(get_bg96_status() == BG96_IDLE_S)
      {
        //Trigger sincroni
        switch(trigger)
        {
          case TRG_GNSS_ON:
            bg96_gnss_reg.WORD &= 0xE0FF;       //reset bit opzioni impostabili
            if(data)
            {
              //Modalit� GNSS (B1:b0)
              if(data[0]==GNSS_ASS) bg96_gnss_reg.BIT.ASS_OPT=1;					
              else bg96_gnss_reg.BIT.ASS_OPT=0;
              //Timeout GNSS
              u16 = (((uint16_t)data[2])<<8) | ((uint16_t)data[1]);
              if(u16<MODEM_GNSS_STD_TO_MIN || u16>MODEM_GNSS_STD_TO_MAX) bg96_timeout_GNSS=MODEM_GNSS_STD_TO_DFLT;
              else bg96_timeout_GNSS=u16;
              //Precisione aumentata (B1:b1)
              if(data[3] == GNSS_PRO_PREC) 
              {
                bg96_gnss_reg.BIT.PRO_OPT=1;
                //Timeout GNSS PRO
                if(data[4]<MODEM_GNSS_PRO_TO_MIN || data[4]>MODEM_GNSS_PRO_TO_MAX) bg96_timeout_GNSS_PRO=MODEM_GNSS_PRO_TO_MIN*MODEM_GNSS_PRO_TO_CONV;
                else bg96_timeout_GNSS_PRO = data[4]*MODEM_GNSS_PRO_TO_CONV;
              }
              else bg96_gnss_reg.BIT.PRO_OPT=0;
            }
            bg96_gnss_reg.BIT.ASS_OPT = 0;
            set_bg96_status(BG96_GNSS_ON_S);            
            result = MA_TRIG_OK;
            break;
            
          case TRG_SYNC_RETE:
            set_bg96_status(BG96_NETWORK_REG_S);
            result = MA_TRIG_OK;
            break;
            
          case TRG_CONNETTI_MQTT:
            if(data)
            {         
              //Check enable servizi Azure
              if(data[0] == 0) 
                set_connection_service(AZ_SERVICE_GP_BROKER);
              else
              {
                az_start_background(AZ_PROCESS_CHECK_CONN);
                set_connection_service(AZ_SERVICE_IOT_CENTRAL);
              }
              modem_int_operation(AZ_OP_MQTT_CONNECT);
              result = MA_TRIG_OK;
            }
            break;
            
          case TRG_INVIA_MQTT:           
            if(data && data_size>0 && (remote_service==AZ_SERVICE_GP_BROKER || remote_service==AZ_SERVICE_IOT_CENTRAL))
            {
              //-- Imposta topic --              
              if(remote_service == AZ_SERVICE_GP_BROKER)
                modem_set_topic(1, (uint8_t*)bg96_ICCID);
              else
                az_set_topic(AZ_EV_PUB_TELEMETRY, &publish_topic);
              //-- Imposta dati publish --
              if(data != mod_buff_data_TX)
              {
                if(data_size >= sizeof(mod_buff_data_TX))
                  data_size = sizeof(mod_buff_data_TX) - 1;     //-1 per garantire l'aggiunta del terminatore di stringa                
                memset(mod_buff_data_TX, 0x00, sizeof(mod_buff_data_TX));
                memcpy(mod_buff_data_TX, data, data_size);      //scrittura dati publish sul buffer di TX
              }
              else
              {
                //Dati publish gi� scritti sul buffer di TX (esternamente)
                for(i=0; mod_buff_data_TX[i]!=0x00 && i<sizeof(mod_buff_data_TX)-1; i++)
                {}                              //cerco eventuale presenza terminatore di stringa}                
                mod_buff_data_TX[i] = 0x00;     //aggiungo terminatore di stringa
              }
              //-- Esegue publish --
              modem_int_operation(AZ_OP_PUBLISH);
              result = MA_TRIG_OK;
            }
            break;
            
          case TRG_RICEVI_MQTT:      
            if(remote_service == AZ_SERVICE_GP_BROKER)
            {
              //-- Imposta topic --
              modem_set_topic(0, (uint8_t*)bg96_ICCID);
              //-- Esegue subscribe --
              modem_int_operation(AZ_OP_SUBSCRIBE);
              result = MA_TRIG_OK;
            }
            else if(remote_service == AZ_SERVICE_IOT_CENTRAL)
            {
              //-- Avvia processo background aggiornamento device-twins --
              az_start_background(AZ_PROCESS_DEVTWINS);
              result = MA_TRIG_OK;
            }           
            break;
            
          case TRG_FW_UPDATE:
            if(data)
            {
              if(data[0]<100 && data[1]<10)     //ammessi solo: sottoversione (2 cifre) e versione (1 cifra)
              {
                bg96_FTP_subversion = data[0];
                bg96_FTP_version = data[1];                
                set_bg96_status(BG96_FTP_S);
                result = MA_TRIG_OK;
              }
            }
            break;
            
          case TRG_CHIUDI_RETE:
            modem_int_operation(AZ_OP_MQTT_DISCONNECT);
            result = MA_TRIG_OK;
            break;
            
          case TRG_GP_SETTINGS:
            if(!data){ bg96_gnss_constellation=BG95_GNSS_CONST_DFLT; }                                  //check 0 parametro : esiste
            else
            {
              //Costellazione GNSS (eseguito solo su BG95)
              if((BG95_GNSS_Const_t)data[0]<GPS_GLONASS || (BG95_GNSS_Const_t)data[0]>MCC_BASED)        //check 1 parametro : entro i limiti 
                bg96_gnss_constellation = BG95_GNSS_CONST_DFLT;          
              else 
                bg96_gnss_constellation = (BG95_GNSS_Const_t)data[0];
            }
            set_bg96_status(BG96_GP_SETTINGS_S);
            result = MA_TRIG_OK;
            break;

          case TRG_REGISTRAZIONE_DEV:            
            if(data)
            {
              if(data[0] > 0)   //check enable servizi Azure
              {
                az_assign_device_id((uint8_t*)bg96_ICCID, sizeof(bg96_ICCID)-1);
                modem_set_HTTP_request();
                set_connection_service(AZ_SERVICE_DPS);
                //-- Avvia processo background DPS --
                az_start_background(AZ_PROCESS_DPS);
                result = MA_TRIG_OK;            
              }
            }
            break;
            
          default: break; 
        }
      }
      else
        result = MA_BUSY;       //BUSY (modulo impegnato)
      break;
      
    default: break;	
  }
  
  return result;
}


/* GET stato operazione richiesta a modem: interfaccia livello applicativo. 
*
*@param[IN]: operazione: tipologia operazione di cui conoscere lo stato
*
*@return:
*MA_OK (operazione conclusa: successo)
*MA_ERROR (operazione conclusa: errore)
*MA_NETWORK_ERROR (operazione conclusa: errore di rete)
*MA_BUSY (operazione in corso)
*MA_POWER_ON_FAIL (failure accensione modem)
*MA_UNP_ERROR (operazione conclusa: errore imprevisto)
*MA_INIT_ERROR (tipologia modem non gestita)
*/
ModemAppli_t modem_GET_Status(ModemTrigger_t operazione)
{
  ModemAppli_t result = MA_INIT_ERROR;
  uint8_t status;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      status = get_bg96_status();
      if(status==BG96_PWR_OFF_S || status==BG96_STDBY_S)
      {
        if(bg96_PWR==PWR_OFF) result=MA_OK;		//--- Spegnimento completato ---
        else result=MA_BUSY;				//--- Spegnimento in corso ---
      }
      else if(status != BG96_IDLE_S) 
        result = MA_BUSY;				//--- Operazione in corso --- 
      else 
      {
        //Modulo in stato IDLE
        if(az_background_in_progress())                 //--- Processo background Azure in corso: maschero lo stato IDLE al livello applicativo fino al completamento del processo ---
          result = MA_BUSY;
        else if(bg96_reg.BIT.PWR_ON_FAILURE)
          result = MA_POWER_ON_FAIL;			//--- Failure accensione modem ---
        else if(bg96_reg.BIT.GENERIC_ERROR)
          result = MA_UNP_ERROR;			//--- Operazione completata: errore inatteso ---
        else
        {
          //--- Operazione completata: OK o errore gestito ---
          result = MA_OK;
          switch(operazione)
          {
            case TRG_MODEM_ON:
            case TRG_MODEM_ON_RESET:
              if(!bg96_reg.BIT.SIM_RILEVATA) result=MA_ERROR;			//SIM non rilevata
              break;
            case TRG_GNSS_ON:
              if(!bg96_reg.BIT.LOC_GNSS) result=MA_ERROR;			//localizzazione GNSS fallita
              break;
            case TRG_SYNC_RETE:
              if(bg96_reg.BIT.PDP_ERROR || bg96_reg.BIT.STATO_REG_RETE==0) 
                result = MA_NETWORK_ERROR;					//errore apertura contesto PDP o registrazione rete
              break;
            case TRG_REGISTRAZIONE_DEV:
              if(az_process_error(AZ_PROCESS_DPS))                              //errore durante il processo di registrazione device
                result = MA_NETWORK_ERROR;
              break;
            case TRG_CONNETTI_MQTT:
                if(modem_GET_int_operation_status(AZ_OP_MQTT_CONNECT) != AZ_NO_ERROR)
                  result = MA_NETWORK_ERROR;					//errore connessione MQTT o registrazione rete
              break;
            case TRG_INVIA_MQTT:
              if(modem_GET_int_operation_status(AZ_OP_PUBLISH) != AZ_NO_ERROR)
                result = MA_NETWORK_ERROR;                                      //publish MQTT fallita
              break;
            case TRG_RICEVI_MQTT:
              if(modem_GET_int_operation_status(AZ_OP_SUBSCRIBE) != AZ_NO_ERROR)
                result = MA_ERROR;                                              //subscribe MQTT fallita: errore o messaggio non presente
              break;
            case TRG_FW_UPDATE:
              if(bg96_reg.BIT.FTP_ERROR) result=MA_ERROR;			//connessione FTP fallita
              break;
            case TRG_MODEM_OFF:
              result = MA_ERROR;						//non � stato triggerato lo spegnimento	              
              break;
            default: break;
          }
        }
      }				
      break;
      
    default: break;
  }
  
  return result; 
}


/* Imposta tipologia servizio remoto. */
void set_connection_service(Az_Service_t service)
{
  switch(service)
  {
    case AZ_SERVICE_IOT_CENTRAL:
      remote_service = service;
      MQTT_Client = &IoT_Central_Broker;
      break;
      
    case AZ_SERVICE_DPS:
      remote_service = service;
      MQTT_Client = &DPS_Broker;
      break;
      
    default:
      remote_service = AZ_SERVICE_GP_BROKER;
      MQTT_Client = &GP_Broker;
      break;
  }
}


/* Imposta topic PUBLISH/SUBSCRIBE MQTT (broker general-purpose, servizi legacy). */
static Az_Err_t modem_set_topic(uint8_t is_publish, uint8_t* device_id)
{
  Az_Err_t err;
  uint16_t topic_size;
  int res = 0;
  
  if(is_publish)
  {
    MQTT_TOPIC_OUT[0] = 0x00;
    topic_size = sizeof(MQTT_TOPIC_OUT);
    res = snprintf((char*)MQTT_TOPIC_OUT, topic_size, "CMPTC/%s/OUT", device_id);
  }
  else
  {
    MQTT_TOPIC_IN[0] = 0x00;
    topic_size = sizeof(MQTT_TOPIC_IN);
    res = snprintf((char*)MQTT_TOPIC_IN, topic_size, "CMPTC/%s/IN", device_id);
  }
  //terminatore di stringa aggiunto da 'snprintf' ...
  
  if(res<0 || res>=topic_size)
  {
    err = AZ_ERR_BUILD_TOPIC;
    azure_reg.BIT.ERR_BUILD_TOPIC = 1;
  }
  else
    err = AZ_NO_ERROR;
  
  return err;
}


/* Compone richiesta HTTP (header+body, API download chiave Azure).  */
ModemAppli_t modem_set_HTTP_request(void)
{
  int res;
  uint16_t content_length, free_space=0;
  
  bg96_HTTP_request_size = 0;
  mod_buff_data_TX[0] = 0x00;           //pulizia buffer
  free_space = sizeof(mod_buff_data_TX);
  //Compongo body per calcolo campo 'Content-Length' (appoggio su buffer 'mod_buff_data_TX')
  content_length = modem_set_HTTP_body(mod_buff_data_TX, &free_space);
  mod_buff_data_TX[0] = 0x00;           //ripulisco buffer (sporcato temporaneamente da body)
  free_space = sizeof(mod_buff_data_TX);
  //Compongo header
  res = snprintf((char*)mod_buff_data_TX, free_space, HTTP_API_HEADER,
                 HTTP_API_HOSTNAME,     //add hostname
                 content_length);       //add content-length
  bg96_HTTP_request_size += check_snprintf(res, &free_space);
  //Compongo body
  bg96_HTTP_request_size += modem_set_HTTP_body(&mod_buff_data_TX[bg96_HTTP_request_size], &free_space);
  
  if(bg96_HTTP_request_size > 0)
    return MA_OK;
  else  
    return MA_ERROR;
}

/* Compone body richiesta HTTP (API download chiave Azure).  */
static uint16_t modem_set_HTTP_body(uint8_t* dest, uint16_t* free_space)
{
  uint16_t body_size = 0;
  int res;
  
  res = snprintf((char*)dest, *free_space, HTTP_API_BODY, 
                 bg96_ICCID);           //add device-id
  body_size += check_snprintf(res, free_space);
  return body_size;
}


//#################### Interfaccia libreria Azure ####################


/* Trigger azioni modem: interfaccia interna. */
static Az_Err_t modem_int_operation(Az_Operation_t operation)
{
  Az_Err_t err = AZ_NO_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      if(get_bg96_status() == BG96_IDLE_S)
      {
        //Operazione sincrona
        switch(operation)
        {
          case AZ_OP_MQTT_CONNECT:
            MQTT_Client->client_id = (uint8_t*)bg96_ICCID;      //client-id MQTT uguale per tutte le tipologie di servizio
            if(remote_service == AZ_SERVICE_GP_BROKER)
              bg96_option_reg.BIT.ENABLE_TLS = 0;
            else
            {
              az_assign_device_id((uint8_t*)bg96_ICCID, sizeof(bg96_ICCID)-1);
              bg96_option_reg.BIT.ENABLE_TLS = 1;
            }
            bg96_option_reg.BIT.HTTP_APP_LAYER_OPT = 0;
            set_bg96_status(BG96_CONFIG_TLS_S);
            break;
          case AZ_OP_PUBLISH: set_bg96_status(BG96_MQTT_PUBLISH_S); break;
          case AZ_OP_SUBSCRIBE: set_bg96_status(BG96_MQTT_SUBSCRIBE_S); break;
          case AZ_OP_MQTT_DISCONNECT: set_bg96_status(BG96_CLOSE_CONN_S); break;
          case AZ_OP_READ_DEVKEY: set_bg96_status(BG96_FILE_READ_S); break;
          case AZ_OP_WRITE_DEVKEY:
            if(Local_Devkey_Counter < 255)
            {
              //Diagnostica chiave individuale dispositivo calcolata localmente
              Local_Devkey_Counter++;
              EEPROM_WriteByte(EEPROM_CONTATORE_LOCAL_DEVKEY, Local_Devkey_Counter);
            }
            bg96_file_write.content = mod_buff_data_TX;
            bg96_file_write.size = strnlen((char const*)mod_buff_data_TX, sizeof(mod_buff_data_TX));
            set_bg96_status(BG96_FILE_WRITE_S);
            break;
          case AZ_OP_HTTP_DEVKEY:
            bg96_option_reg.BIT.ENABLE_TLS = 1;
            bg96_option_reg.BIT.HTTP_APP_LAYER_OPT = 1;
            set_bg96_status(BG96_CONFIG_TLS_S);
            break;
          default: err=AZ_ERR_UNXPECTED; break;
        }
      }
      else
      {
        //BUSY (modulo impegnato)
        azure_reg.BIT.ERR_MQTT_CALLBACK = 1;
        err = AZ_ERR_MQTT_CALLBACK;        
      }
      break;
      
    default: err=AZ_ERR_UNXPECTED; break;
  }

  if(err == AZ_ERR_UNXPECTED)
     azure_reg.BIT.ERR_UNEXPECTED = 1;
  
  return err;
}


/* GET stato operazione richiesta a modem: interfaccia interna. */
static Az_Err_t modem_GET_int_operation_status(Az_Operation_t operation)
{
  Az_Err_t err = AZ_NO_ERROR;
  
  switch(ModemType)
  {
    case MOD_BG95:
    case MOD_BG96:
      switch(operation)
      {
        case AZ_OP_MQTT_CONNECT:
          if(bg96_reg.BIT.MQTT_ERROR_CONN || bg96_reg.BIT.TLS_CONF_ERROR)
            err = AZ_ERR_MQTT_OPERATION;
          break;
        case AZ_OP_PUBLISH:
          if(bg96_reg.BIT.MQTT_ERROR_PUBLISH) 
            err = AZ_ERR_MQTT_OPERATION;
          break;
        case AZ_OP_SUBSCRIBE:
          if(bg96_reg.BIT.MQTT_ERROR_SUBSCRIBE)
            err = AZ_ERR_MQTT_OPERATION;
          break;
        case AZ_OP_DATA_RECEIVED:
          if(!bg96_MQTT_rx_msg.received)
            err = AZ_ERR_MQTT_OPERATION;
          break;
        case AZ_OP_MQTT_DISCONNECT: break;
        case AZ_OP_READ_DEVKEY:
          if(!bg96_file_read.success)
            err = AZ_ERR_READ_DEVKEY;
          break;
        case AZ_OP_WRITE_DEVKEY:
          if(!bg96_file_write.success)
            err = AZ_ERR_WRITE_DEVKEY;
          break;
        case AZ_OP_HTTP_DEVKEY:
          if(bg96_reg.BIT.HTTP_ERROR)
            err = AZ_ERR_HTTP_DEVKEY;
          break;
        default: err=AZ_ERR_UNXPECTED; break;
      }
      break;
      
    default: err=AZ_ERR_UNXPECTED; break;
  }

  if(err == AZ_ERR_UNXPECTED)
     azure_reg.BIT.ERR_UNEXPECTED = 1;
  
  return err;
}
