/**
  ******************************************************************************
  * File          	: bg_96_info.c
  * Versione libreria	: 0.02
  * Descrizione       	: Get info modem:
  *                       - check COM
  *                       - ICCID SIM
  *                       - versione FW
  *                       - ENB GNSS assistito
  *                       - costellazione GNSS
  *                       - info RAT
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "bg_96_info.h"
#include "bg_96_command.h"




#define FIFO_INFO_SIZE             	8               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_GetInfo[FIFO_INFO_SIZE] = {
  {.at_cmd=(uint8_t*)"AT",        			.at_resp=(uint8_t*)"OK",			        .AT_CMD_TYPE=BG96_CMD_AT,			.AT_CMD_RETRY=10,        .AT_RESP_TIMEOUT=BG96_1000MS_TO},
  {.at_cmd=(uint8_t*)"ATI",        			.at_resp=(uint8_t*)"Revision: ",		        .AT_CMD_TYPE=BG96_CMD_FW_INFO,			.AT_CMD_RETRY=3},         
  {.at_cmd=(uint8_t*)"AT+QCCID",			.at_resp=(uint8_t*)"+QCCID: ",     	                .AT_CMD_TYPE=BG96_CMD_ICCID, 			.AT_CMD_RETRY=10},
  {.at_cmd=(uint8_t*)"AT+QGPSXTRA?",		        .at_resp=(uint8_t*)"+QGPSXTRA: ",                       .AT_CMD_TYPE=BG96_CMD_QUERY_GNSS_ASS},
  {.at_cmd=(uint8_t*)"AT+QGPSCFG=\"gnssconfig\"",       .at_resp=(uint8_t*)"+QGPSCFG: \"gnssconfig\",",         .AT_CMD_TYPE=BG96_CMD_QUERY_GNSS_CONST},
  {.at_cmd=(uint8_t*)"AT+QAPPVER",        		.at_resp=(uint8_t*)"+QAPPVER: ",		        .AT_CMD_TYPE=BG96_CMD_FW_INFO_SUB},
  {.at_cmd=(uint8_t*)"AT+QCFG=\"nwscanmode\"",          .at_resp=(uint8_t*)"+QCFG: \"nwscanmode\",",	        .AT_CMD_TYPE=BG96_CMD_QUERY_RAT_SCAN_MODE},
  {.at_cmd=(uint8_t*)"AT+QCFG=\"iotopmode\"",           .at_resp=(uint8_t*)"+QCFG: \"iotopmode\",",		.AT_CMD_TYPE=BG96_CMD_QUERY_RAT_LTE_MODE},
};


char bg96_FW_info[BG96_FW_INFO_SIZE+1];         //Stringa versione FW AT (main version)
char bg96_FW_info_sub[BG96_FW_INFO_SUB_SIZE+1]; //Stringa versione FW AT (sub version)
char bg96_ICCID[MOD_ICCID_MAX_SIZE+1];		//ICCID SIM (+1 per aggiunta terminatore stringa)

uint8_t bg96_check_COM = 0;

/* ############################################################ 
 * Local functions
 * ############################################################
*/
static Mod_t bg96_detect_modulo(void);




/* -------------------- Stato: BG96_INFO -------------------- 
* Info modem: versione FW, ICCID SIM, ENB GNSS assistito, costellazione GNSS, info RAT.
*/
void BG96_INFO_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  if(!GPTIMER_isTimedOut(&bg96_NopTimer) && bg96_NopTimer.enable)
    return;
  
  GPTIMER_stop(&bg96_NopTimer);					
  //Attesa invio primo comando AT conclusa
  err = bg96_send_command(fifo_GetInfo, bg96_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {					
    switch(fifo_GetInfo[bg96_fifo_index].AT_CMD_TYPE)
    {
      case BG96_CMD_AT:
        bg96_check_COM++;
        break;
        
      case BG96_CMD_FW_INFO:
        memset(bg96_FW_info, 0x00, sizeof(bg96_FW_info));
        memcpy(bg96_FW_info, &mod_buff_Risposta[offset], sizeof(bg96_FW_info)-1);               //-1 per garantire l'aggiunta del terminatore di stringa
        replaceInvalidChar((uint8_t*)bg96_FW_info, sizeof(bg96_FW_info), 0x00);                 //filtra \r\n
        ModemType = bg96_detect_modulo();
        break;
        
      case BG96_CMD_FW_INFO_SUB:
        memset(bg96_FW_info_sub, 0x00, sizeof(bg96_FW_info_sub));
        memcpy(bg96_FW_info_sub, &mod_buff_Risposta[offset], sizeof(bg96_FW_info_sub)-1);       //-1 per garantire l'aggiunta del terminatore di stringa
        replaceInvalidChar((uint8_t*)bg96_FW_info_sub, sizeof(bg96_FW_info_sub), 0x00);         //filtra \r\n
        break;
        
      case BG96_CMD_ICCID:
        memset(bg96_ICCID, 0x00, sizeof(bg96_ICCID));
        memcpy(bg96_ICCID, &mod_buff_Risposta[offset], sizeof(bg96_ICCID)-1);   //-1 per garantire l'aggiunta del terminatore di stringa
        bg96_reg.BIT.SIM_RILEVATA = 1;
        break;
        
      case BG96_CMD_QUERY_GNSS_ASS:
        if(mod_buff_Risposta[offset] == '1') bg96_gnss_reg.BIT.ASS_ENB=1;
        else bg96_gnss_reg.BIT.ASS_ENB=0;
        break;
        
      case BG96_CMD_QUERY_GNSS_CONST:
        bg96_gnss_constellation = convertStrToByte(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));
        bg96_gnss_reg.BYTE.B1 &= 0x1F;                                        //Reset bit costellazione GNSS (B1:[b5,b7])
        bg96_gnss_reg.BYTE.B1 |= ((bg96_gnss_constellation&0x07)<<5);         //Costellazione GNSS (B1:[b5,b7])
        break;
        
      case BG96_CMD_QUERY_RAT_SCAN_MODE:
        bg96_RAT_scan_mode = (BG96_RAT_Scan_Mode_t)(mod_buff_Risposta[offset]);
        break;
            
      case BG96_CMD_QUERY_RAT_LTE_MODE:
        bg96_RAT_LTE_mode = (BG96_RAT_LTE_Mode_t)(mod_buff_Risposta[offset]);
        break;
        
      default: break;
    }
    
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {
    bg96_critical_cmd_MASK = 0x0004;
    //--- Gestione eccezioni su errore ---
    //CMD[2]: get ICCID: CRITICO
    if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
    {
      set_bg96_status(BG96_ERR_S);
      return;
    }
    else
      next_command = 1;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_INFO_SIZE))
    {
      if(bg96_RAT_scan_mode==RAT_SM_AUTO && bg96_RAT_LTE_mode==RAT_LTE_CATM_ONLY)
        set_bg96_status(BG96_IDLE_S);
      else
        set_bg96_status(BG96_CONFIG_RAT_S);
    }
  }
}


/* Riconosce tipologia di modem (Quectel), in base a versione FW. */
static Mod_t bg96_detect_modulo(void)
{
  if(memcmp(bg96_FW_info, "BG96", 4) == 0)
  {
    bg96_reg.BIT.TYPE_DETECT = 1;
    return MOD_BG96;
  }
  else if(memcmp(bg96_FW_info, "BG95", 4) == 0)
  {
    bg96_reg.BIT.TYPE_DETECT = 1;
    return MOD_BG95;
  }
  else
  {
    bg96_reg.BIT.TYPE_DETECT = 0;
    if(mod_versione_HW==VERSIONE_HW_3_X && mod_sottoversione_HW==SOTTO_VERSIONE_HW_3_X_BASE)
      return MOD_BG96;  //errore detect (fallback a BG96)
    else
      return MOD_BG95;  //errore detect (fallback a BG95)
  }    
}
