/**
  *********************************************************************************************
  * File          	: bg_96_file.c
  * Versione libreria	: 0.02
  * Descrizione       	: Lettura file-system modem:
  *                       - file chiave individuale dispositivo (Azure)
  *********************************************************************************************
  *
  * COPYRIGHT(c) 2023 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  *********************************************************************************************
  */

#include "bg_96_file.h"
#include "bg_96_command.h"




#define FIFO_FILE_READ_SIZE             5		//N.B: ---- Attenzione a ordine comandi (utilizzo bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_FileRead[FIFO_FILE_READ_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QFLST="AZ_DEVICE_KEY_FNAME,            .at_resp=(uint8_t*)"+QFLST: "AZ_DEVICE_KEY_FNAME",",    .AT_CMD_TYPE=BG96_CMD_FILE_LIST,                .AT_CMD_RETRY=3},
  {.at_cmd=(uint8_t*)"AT+QFOPEN="AZ_DEVICE_KEY_FNAME",2",       .at_resp=(uint8_t*)"+QFOPEN: ",                         .AT_CMD_TYPE=BG96_CMD_FILE_OPEN},
  {.at_cmd=(uint8_t*)"AT+QFSEEK=",                              .at_resp=(uint8_t*)"OK",                                .AT_CMD_TYPE=BG96_CMD_FILE_MOVE_TO_SOF},        
  {.at_cmd=(uint8_t*)"AT+QFREAD=",                              .at_resp=(uint8_t*)"CONNECT",                           .AT_CMD_TYPE=BG96_CMD_FILE_READ,                                .AT_RESP_TIMEOUT=BG96_1000MS_TO},
  {.at_cmd=(uint8_t*)"AT+QFCLOSE=",                             .at_resp=(uint8_t*)"OK",                                .AT_CMD_TYPE=BG96_CMD_FILE_CLOSE}
};

#define FIFO_FILE_WRITE_SIZE            5               //N.B: ---- Attenzione a ordine comandi (utilizzo bg96_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t fifo_FileWrite[FIFO_FILE_WRITE_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+QFDEL="AZ_DEVICE_KEY_FNAME,   		.at_resp=(uint8_t*)"OK",    		.AT_CMD_TYPE=BG96_CMD_CANCELLA_FILE,            .AT_RESP_TIMEOUT=BG96_1000MS_TO},
  {.at_cmd=(uint8_t*)"AT+QFOPEN="AZ_DEVICE_KEY_FNAME",1",       .at_resp=(uint8_t*)"+QFOPEN: ",         .AT_CMD_TYPE=BG96_CMD_FILE_OPEN},
  {.at_cmd=(uint8_t*)"AT+QFWRITE=",                             .at_resp=(uint8_t*)"CONNECT",           .AT_CMD_TYPE=BG96_CMD_CFG_FILE_WRITE,           .AT_RESP_TIMEOUT=BG96_1000MS_TO},
  {.at_cmd=(uint8_t*)"",     			                .at_resp=(uint8_t*)"OK",		.AT_CMD_TYPE=BG96_CMD_FILE_WRITE,               .AT_RESP_TIMEOUT=BG96_1000MS_TO,        .AT_RESP_RETRY=5},  
  {.at_cmd=(uint8_t*)"AT+QFCLOSE=",                             .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=BG96_CMD_FILE_CLOSE}
};


File_t bg96_file_read, bg96_file_write;
uint8_t bg96_file_handle;


/* ############################################################ 
 * Local functions
 * ############################################################
*/




/* -------------------- Stato: BG96_FILE_READ -------------------- 
* Lettura file-system modem (file chiave individuale Azure).
*/
void BG96_FILE_READ_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t idx_start, offset=0;
  
  err = bg96_send_command(fifo_FileRead, bg96_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {
    switch(fifo_FileRead[bg96_fifo_index].AT_CMD_TYPE)
    {
      case BG96_CMD_FILE_LIST:
        bg96_file_read.size = convertStrToLong(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));          
        //Controllo saturazione 'bg96_file_read.size' in base a dimensione buffer lettura (garantisco presenza terminatore stringa)
        if(bg96_file_read.size > sizeof(mod_buff_data_RX)-1)
          bg96_file_read.size = sizeof(mod_buff_data_RX) - 1;           
        break;
        
      case BG96_CMD_FILE_OPEN:
        bg96_file_handle = convertStrToByte(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));
        break;
        
      case BG96_CMD_FILE_READ:
        searchField(mod_buff_Risposta, LF_CHAR, offset, sizeof(mod_buff_Risposta));
        idx_start = searchField_res[0].idx + 1;
        memcpy(mod_buff_data_RX, &mod_buff_Risposta[idx_start], bg96_file_read.size); //copio dati letti (i controlli di saturazione in BG96_CMD_FILE_LIST mi proteggono in scrittura)
        mod_buff_data_RX[bg96_file_read.size] = 0x00;                                 //aggiungo terminatore stringa
        bg96_file_read.content = mod_buff_data_RX;
        bg96_file_read.success = 1;
        break;
        
      default: break;
    }
    
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {
    bg96_critical_cmd_MASK = 0x0011;
    //--- Gestione eccezioni su errore ---
    //CMD[0]: presenza file: CRITICO
    //CDM[1][2][3]: non critici per garantire che avvenga chiusura file
    //CMD[4]: chiusura file: CRITICO
    if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
    {
      bg96_reg.BIT.FILE_ERROR = 1;
      set_bg96_status(BG96_ERR_S);
      return;
    }
    else
      next_command = 1;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_FILE_READ_SIZE))
      set_bg96_status(BG96_IDLE_S);
  }
}


/* -------------------- Stato: BG96_FILE_WRITE -------------------- 
* Scrittura file-system modem (file chiave individuale Azure).
*/
void BG96_FILE_WRITE_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  err = bg96_send_command(fifo_FileWrite, bg96_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {
    switch(fifo_FileWrite[bg96_fifo_index].AT_CMD_TYPE)
    {          
      case BG96_CMD_FILE_OPEN:
        bg96_file_handle = convertStrToByte(&mod_buff_Risposta[offset], offset, sizeof(mod_buff_Risposta));
        break;
        
      case BG96_CMD_FILE_WRITE:
        bg96_file_write.success = 1;
        break;
        
      default: break;
    }
    
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {    
    bg96_critical_cmd_MASK = 0x0010;
    //--- Gestione eccezioni su errore ---
    //CDM[0][1][2][3]: non critici per garantire che avvenga chiusura file
    //CMD[4]: chiusura file: CRITICO
    if(bg96_critical_cmd(bg96_fifo_index, bg96_critical_cmd_MASK))
    {
      bg96_reg.BIT.FILE_ERROR = 1;
      set_bg96_status(BG96_ERR_S);
      return;
    }
    else
      next_command = 1;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&bg96_fifo_index, FIFO_FILE_WRITE_SIZE))
      set_bg96_status(BG96_IDLE_S);
  }
}
