#include "Gestione_stato.h"
#include "Globals.h"
#include "Gestione_dato.h"
#include "funzioni.h"
#include "ndef.h"
#include "EEPROM.h"
#include "Globals.h"




void CheckStateTransition(void)
{
  switch(Dati.StatoAlesea)
  {
  case ALESEA_PRE_COLLAUDO:
    
    break;
    
  case ALESEA_COLLAUDO:      //su chiamata, non periodico    
    if( CounterTxCollaudo >= (NUMERO_TX_COLLAUDO_OLD_BROKER + NUMERO_TX_COLLAUDO_AZURE) )
    {
      CounterTxCollaudo = 0;
      EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO_1, CHIAVE_COLLAUDO);    EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO_2, CHIAVE_COLLAUDO);    EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO_3, CHIAVE_COLLAUDO);
      EEPROM_WriteByte(EEPROM_TIPO_MODEM, Dati.TipoModem);
      SetStatoAlesea(ALESEA_READY);       
      Spegnimento();
    }
    break;
    
  case ALESEA_START_UP:
    if(CounterTxStartUp >= Dati.ActivationTiming.NumberTx_START_UP)
    {
      //SetStatoAlesea(ALESEA_WARM_UP); //terminatoa la fase di start up passo direttamente in ACTIVE
      SetStatoAlesea(ALESEA_ACTIVE);
      CounterTxWarmUp = 0;
    }
    break;
    
  case ALESEA_WARM_UP:
    if(CounterTxWarmUp >= Dati.ActivationTiming.NumberTx_WARM_UP)
    {
      SetStatoAlesea(ALESEA_ACTIVE);
    }
    break;
    
  case ALESEA_ACTIVE:
    
    break;
    
  default:

    break;
  }
}


