/**
************************************************************************************************
* File          	: Interfaccia_dato.c
* Descrizione       	: Interfaccia dati tra livello applicativo e libreria Azure.                       
************************************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
************************************************************************************************
*/

#include "Interfaccia_dato.h"
#include "az_interface.h"
#include "Gestione_dato.h"
#include "funzioni.h"




Az_Property_t App_properties[SP_ID_NUM] =
{
  {.keys={"sw_p","v","subv"},   .schema=AZ_OBJ_U32_U32,},       //SP_ID_FW_UPDATE
  {.keys={"gt_p"},              .schema=AZ_U32,},               //SP_ID_GNSS_TIMEOUT
  {.keys={"tm_p"},              .schema=AZ_U32,},               //SP_ID_MOVEMENT_THRESHOLD
  {.keys={"ts_p"},              .schema=AZ_U32,},               //SP_ID_SHOCK_THRESHOLD
  {.keys={"gs_p"},              .schema=AZ_U32,},               //SP_ID_GNSS_CONSTELLATION
  {.keys={"fm_p"},              .schema=AZ_U32,},               //SP_ID_SPIN_FILTER
  {.keys={"gp_p","gp","gpt"},   .schema=AZ_OBJ_BOOL_U32,},      //SP_ID_GNSS_PRO_SETTINGS
  {.keys={"gf_p"},              .schema=AZ_U32,},               //SP_ID_FORCED_GNSS
  {.keys={"pt_p"},              .schema=AZ_BOOL,},              //SP_ID_TEMPERATURE_PROFILE_ENB
  {.keys={"is_p","is","ns"},    .schema=AZ_OBJ_U32_U32,},       //SP_ID_STARTUP_SETTINGS
  {.keys={"iw_p","iw","nw"},    .schema=AZ_OBJ_U32_U32,},       //SP_ID_WARMUP_SETTINGS
  {.keys={"ia_p"},              .schema=AZ_U32,},               //SP_ID_SCHEDULING_INTERVAL
  {.keys={"or_p"},              .schema=AZ_BOOL,},              //SP_ID_HORIZONTAL_SPIN_ENB
  {.keys={"msi_p"},             .schema=AZ_U32,},               //SP_ID_END_OF_MOVEMENT_INTERVAL
  {.keys={"ew_p"},              .schema=AZ_BOOL,},              //SP_ID_WIFI_SCAN_ENB
  {.keys={"gm_p"},              .schema=AZ_U32,},               //SP_ID_GPS_MODE
  {.keys={"ssi_p"},             .schema=AZ_U32,},               //SP_ID_SPIN_SCHEDULING_INTERVAL
  {.keys={"ski_p"},             .schema=AZ_U32,},               //SP_ID_SHOCK_SCHEDULING_INTERVAL
  {.keys={"nti_p"},             .schema=AZ_U32,},               //SP_ID_NUMBER_TX_IOT_CENTRAL
  {.keys={"ewc_p"},             .schema=AZ_BOOL,},              //SP_ID_WIFI_COM_ENB
  {.keys={"nts_p"},             .schema=AZ_U32,},               //SP_ID_NUMBER_TX_SWITCH_MODEM
  {.keys={"fr_p"},              .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_FACTORY_RESET_TRG:trigger
  {.keys={"sp_p"},              .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_SPIN_RESET_TRG:trigger
  {.keys={"de_p"},              .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_DEACTIVATION_TRG:trigger
  {.keys={"bs_p"},              .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_BATTERY_RESET_TRG:trigger
  {.keys={"cr_p"},              .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_MICRO_RESET_TRG:trigger
  {.keys={"sk_p"},              .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_SHOCK_RESET_TRG:trigger
  {.keys={"dg_p"},              .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_DIAGNOSTIC_RESET_TRG:trigger
  {.keys={"ff_p"},              .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_FREE_FALL_RESET_TRG:trigger
  {.keys={"swr_p"},             .schema=AZ_U32,                 .option=AZ_OPT_IS_TRIGGER},             //SP_ID_FW_UPDATE_UNLOCK_TRG:trigger
};

SetpointsId_t log_buffer[32];
uint8_t log_buffer_idx;

/* ############################################################ 
* Local functions
* ############################################################
*/
static void SyncSetpoints(void);
static void SyncInterfacciaDato(void);




/* Init interfaccia tra applicazione ed Azure device-twins. */
void InitInterfacciaDato(void)
{
  Az_DevTwins_Callback_t cbk = {
    .sync_app_data = SyncSetpoints,
    .sync_reported_props = SyncInterfacciaDato
  };
  az_init_properties(App_properties, sizeof(App_properties)/sizeof(App_properties[0]), cbk);
}


/* Sincronizzazione tra setpoints ricevuti da IoT Central e dati applicazione
*  Valori proprietÓ 'desired' --> Dati applicazione
*/
static void SyncSetpoints(void)
{
  uint8_t p;
  Az_Property_t* prop;
  
  for(p=0; p<sizeof(App_properties)/sizeof(App_properties[0]); p++)
  {
    prop = &App_properties[p];
    prop->reg.BIT.MISMATCH = 0;
    if(prop->reg.BIT.DESIRED_RX && prop->reg.BIT.REPORTED_RX)
    {
      if(memcmp(&prop->desired, &prop->reported, sizeof(Az_Property_Value_t)) != 0)
      {
        if(prop->option.BIT.OPT_IS_TRIGGER)
        {
          //Gestione proprietÓ a trigger (contatori uint16_t): ammesso solo incremento di 1
          if((prop->desired.U32 == prop->reported.U32+1) || (prop->desired.U32==0 && prop->reported.U32==0xFFFF))
          {
            prop->reported.U32 = prop->desired.U32;     //allineamento automatico valore 'reported'
            prop->reg.BIT.MISMATCH = 1;
          }
        }
        else
        {
          //Gestione proprietÓ standard
          prop->reg.BIT.MISMATCH = 1;
        }
      }
    }
    
    if(prop->reg.BIT.MISMATCH)
    {
      //--- Eseguo comando ---
      switch(p)
      {
        case SP_ID_FW_UPDATE:
          if(UpgradeFwCount <= MAX_FW_UPGRADE_ATTEMPTS)
            SetUpgradeFw((uint8_t)prop->desired.OBJ_U32_U32.U32_0, (uint8_t)prop->desired.OBJ_U32_U32.U32_1, UPG_USER);
          break;
        
        case SP_ID_GNSS_TIMEOUT:
          SetGpsSetup(prop->desired.U32);
          break;
 
        case SP_ID_MOVEMENT_THRESHOLD:
          SetSogliaMovement((uint8_t)prop->desired.U32);
          break;
          
        case SP_ID_SHOCK_THRESHOLD:
          SetSogliaShock((uint8_t)prop->desired.U32);
          break;
          
        case SP_ID_GNSS_CONSTELLATION:
          SetGnssSatelliti(prop->desired.U32);
          break;
          
        case SP_ID_SPIN_FILTER:
          SetTimeoutSpin((uint8_t)prop->desired.U32);
          break;
          
        case SP_ID_GNSS_PRO_SETTINGS:
          SetGpsPro(prop->desired.OBJ_BOOL_U32.BOOL);
          SetGpsProTime((uint8_t)prop->desired.OBJ_BOOL_U32.U32);
          break;
          
        case SP_ID_FORCED_GNSS:
          SetGpsFreqForcing((uint8_t)prop->desired.U32);
          break;
          
        case SP_ID_TEMPERATURE_PROFILE_ENB:          
          SetProfiloTemperatura(prop->desired.BOOL + (uint8_t)0x30);    //conversione in carattere per retrocompatibilitÓ
          break;
          
        case SP_ID_STARTUP_SETTINGS:
          SetInterval_START_UP(prop->desired.OBJ_U32_U32.U32_0*60);
          SetNumberTx_START_UP(prop->desired.OBJ_U32_U32.U32_1);          
          break;
          
        case SP_ID_WARMUP_SETTINGS:          
          SetInterval_WARM_UP(prop->desired.OBJ_U32_U32.U32_0*60);
          SetNumberTx_WARM_UP(prop->desired.OBJ_U32_U32.U32_1);
          break;
          
        case SP_ID_SCHEDULING_INTERVAL:
          SetInterval_ACTIVE(prop->desired.U32*60);
          break;
          
        case SP_ID_HORIZONTAL_SPIN_ENB:
          SetRotazioniOrizzontali(prop->desired.BOOL + (uint8_t)0x30);  //conversione in carattere per retrocompatibilitÓ
          break;
          
        case SP_ID_END_OF_MOVEMENT_INTERVAL:
          SetMoveSchInterval(prop->desired.U32*60);
          break;
          
        case SP_ID_WIFI_SCAN_ENB:
          SetWiFiScanEnable(prop->desired.BOOL + (uint8_t)0x30);        //conversione in carattere per retrocompatibilitÓ
          break;
          
        case SP_ID_GPS_MODE:
          SetGpsMode((uint8_t)prop->desired.U32);
          break;
          
        case SP_ID_SPIN_SCHEDULING_INTERVAL:
          SetSpinSchInterval(prop->desired.U32*60);
          break;
          
        case SP_ID_SHOCK_SCHEDULING_INTERVAL:
          SetShockSchInterval(prop->desired.U32*60);
          break;
          
        case SP_ID_NUMBER_TX_IOT_CENTRAL:
          SetNumberTxIot((uint8_t)prop->desired.U32);
          break;
          
        case SP_ID_WIFI_COM_ENB:
          SetWiFiComEnable(prop->desired.BOOL + (uint8_t)0x30);         //conversione in carattere per retrocompatibilitÓ
          break;
          
        case SP_ID_NUMBER_TX_SWITCH_MODEM:
          SetNumberTxSwitchModem((uint8_t)prop->desired.U32);
          break;
         
        case SP_ID_FACTORY_RESET_TRG:
          RunFactoryReset();
          break;
        
        case SP_ID_DIAGNOSTIC_RESET_TRG:
          RunDiagnosticaReset();
          break;
        
        case SP_ID_SPIN_RESET_TRG:
          RunSpinReset();
          break;
          
        case SP_ID_DEACTIVATION_TRG:
          RunDeactivation();
          break;
          
        case SP_ID_BATTERY_RESET_TRG:
          RunBatteryReset();
          break;
          
        case SP_ID_MICRO_RESET_TRG:
          RunMicroReset();
          break;
          
        case SP_ID_SHOCK_RESET_TRG:
          RunShockReset();
          break;
          
        case SP_ID_FREE_FALL_RESET_TRG:
          RunFreeFallReset();
          break;
          
        case SP_ID_FW_UPDATE_UNLOCK_TRG:
          UpgradeFwCount = 0;
          break;
          
        default: break;
      }
    }
  }
}


/* Sincronizzazione tra dati da notificare ad IoT Central e dati applicazione.
*  Dati applicazione --> Valori proprietÓ 'reported'
*/
static void SyncInterfacciaDato(void)
{
  uint8_t p;
  Az_Property_t* prop;
  
  for(p=0; p<sizeof(App_properties)/sizeof(App_properties[0]); p++)
  {
    prop = &App_properties[p];
    switch(p)
    {
      case SP_ID_FW_UPDATE:
        prop->reported.OBJ_U32_U32.U32_0 = VersioneFW;
        prop->reported.OBJ_U32_U32.U32_1 = SottoversioneFW;
        break;
      
      case SP_ID_GNSS_TIMEOUT:
        prop->reported.U32 = Dati.GNSS_data.GpsTimeout;
        break;
        
      case SP_ID_MOVEMENT_THRESHOLD:
        prop->reported.U32 = Dati.MovementEvent.SogliaMovimento;
        break;
        
      case SP_ID_SHOCK_THRESHOLD:
        prop->reported.U32 = Dati.ShockEvent.SogliaShock;
        break;
        
      case SP_ID_GNSS_CONSTELLATION:
        prop->reported.U32 = Dati.GNSS_data.GnssSatelliti;
        break;
        
      case SP_ID_SPIN_FILTER:
        prop->reported.U32 = Dati.MovementEvent.FiltroMovimento;
        break;
        
      case SP_ID_GNSS_PRO_SETTINGS:
        prop->reported.OBJ_BOOL_U32.BOOL = Dati.GNSS_data.GpsPro;
        prop->reported.OBJ_BOOL_U32.U32 = Dati.GNSS_data.GpsProTime;
        break;
        
      case SP_ID_FORCED_GNSS:
        prop->reported.U32 = Dati.GNSS_data.GpsFreqForcing;
        break;
        
      case SP_ID_TEMPERATURE_PROFILE_ENB:
        if(Dati.AleseaMeasure.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_OFF)  //parametro diverso da [0,1] --> NON conforme a BOOL
          prop->reported.BOOL = 0;
        else
          prop->reported.BOOL = 1;
        break;
        
      case SP_ID_STARTUP_SETTINGS:
        prop->reported.OBJ_U32_U32.U32_0 = Dati.ActivationTiming.SchInt_START_UP/60;
        prop->reported.OBJ_U32_U32.U32_1 = Dati.ActivationTiming.NumberTx_START_UP;
        break;
        
      case SP_ID_WARMUP_SETTINGS:
        prop->reported.OBJ_U32_U32.U32_0 = Dati.ActivationTiming.SchInt_WARM_UP/60;
        prop->reported.OBJ_U32_U32.U32_1 = Dati.ActivationTiming.NumberTx_WARM_UP;
        break;
        
      case SP_ID_SCHEDULING_INTERVAL:
        prop->reported.U32 = Dati.ActivationTiming.SchInt_ACTIVE/60;
        break;

      case SP_ID_HORIZONTAL_SPIN_ENB:
        prop->reported.BOOL = Dati.SpinEvent.RotazioneOrizzontaleOn;
        break;
        
      case SP_ID_END_OF_MOVEMENT_INTERVAL:
        prop->reported.U32 = Dati.MovementEvent.MoveSchInterval/60;
        break;
        
      case SP_ID_WIFI_SCAN_ENB:
        if(WiFiParam.WiFiScanEnable == WIFI_ENABLE_OFF)         //parametro diverso da [0,1] --> NON conforme a BOOL
          prop->reported.BOOL = 0;
        else
          prop->reported.BOOL = 1;
        break;
        
      case SP_ID_GPS_MODE:
        prop->reported.U32 = Dati.GNSS_data.GpsMode;
        break;
        
      case SP_ID_SPIN_SCHEDULING_INTERVAL:
        prop->reported.U32 = Dati.SpinEvent.SpinSchInterval/60;
        break;
        
      case SP_ID_SHOCK_SCHEDULING_INTERVAL:
        prop->reported.U32 = Dati.ShockEvent.ShockSchInterval/60;
        break;
        
      case SP_ID_NUMBER_TX_IOT_CENTRAL:
        prop->reported.U32 = NumberTxIot;
        break;
        
      case SP_ID_WIFI_COM_ENB:
        if(WiFiParam.WiFiComEnable == WIFI_ENABLE_OFF)          //parametro diverso da [0,1] --> NON conforme a BOOL
          prop->reported.BOOL = 0;
        else
          prop->reported.BOOL = 1;
        break;
        
      case SP_ID_NUMBER_TX_SWITCH_MODEM:
        prop->reported.U32 = WiFiParam.WiFiNumTxSwitchModem;
        break;
        
      default: break;
    }
  }
}
  
