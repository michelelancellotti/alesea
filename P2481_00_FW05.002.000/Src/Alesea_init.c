#include "wwdg.h"

#include "EEPROM.h"
#include "Alesea_init.h"
#include "Alesea_memory.h"
#include "Globals.h"
#include "Gestione_dato.h"
#include "Gestione_batteria.h"
#include "funzioni.h"
#include "modem_SM.h"
#include "ndef.h"
#include "LSM6DSL_SM.h"
#include "nfc_SM.h"
#include "Wifi_SM.h"
#include "Interfaccia_dato.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>


const uint8_t *BootVersionAdd;
const uint8_t* HwVersionAdd;

uint8_t NewFirmwareVersion = 0;



void InitVersioni(void)
{
  uint8_t old_VersioneFW, old_SottoVersioneFW;
    
  VersioneFW = VERSIONE_FW;	
  SottoversioneFW = SOTTO_VERSIONE_FW;
  
  old_VersioneFW = EEPROM_ReadByte(EEPROM_ADD_VERSION);
  old_SottoVersioneFW = EEPROM_ReadByte(EEPROM_ADD_SUBVERSION);
  if((VersioneFW!=old_VersioneFW) || ((VersioneFW==old_VersioneFW) && (SottoversioneFW!=old_SottoVersioneFW)))
     NewFirmwareVersion = 1;   //nuovo FW installato
      
  EEPROM_WriteByte(EEPROM_ADD_VERSION, VersioneFW);
  EEPROM_WriteByte(EEPROM_ADD_SUBVERSION, SottoversioneFW);
  
  BootVersionAdd = (uint8_t*)BOOT_VERSION_ADDRESS;
  if( (BootVersionAdd[0] == 'B') && (BootVersionAdd[1] == '.') )
    memcpy(Dati.SystemVersion.BootVersion, BootVersionAdd, sizeof(Dati.SystemVersion.BootVersion));
  else
    sprintf((char*)Dati.SystemVersion.BootVersion,"%s", "OLD_BOOT");
  
  HwVersionAdd = (uint8_t*)HW_VERSION_ADDRESS;
  if( (HwVersionAdd[0] == 'H') && (HwVersionAdd[1] == '.') )
  {
    VersioneHW = HwVersionAdd[2];
    SottoversioneHW = HwVersionAdd[3];
  }
  else
  {
    VersioneHW = VERSIONE_HW_3_X;		
    SottoversioneHW = SOTTO_VERSIONE_HW_3_X_BASE;
  }
}


void InitCollaudo(void)
{
  uint8_t chiave_temp_1, chiave_temp_2, chiave_temp_3;
  
  chiave_temp_1 = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO_1);
  chiave_temp_2 = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO_2);
  chiave_temp_3 = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO_3);
    
  
  if( (chiave_temp_1 != CHIAVE_COLLAUDO) && (chiave_temp_2 != CHIAVE_COLLAUDO) && (chiave_temp_3 != CHIAVE_COLLAUDO) ) 
  {
    //InitTag();
    SetStatoAlesea(ALESEA_PRE_COLLAUDO);       
  }
  else
  {
    EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO_1, CHIAVE_COLLAUDO );    EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO_2, CHIAVE_COLLAUDO );    EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO_3, CHIAVE_COLLAUDO );
  }
}



uint8_t chiave_temp_1, chiave_temp_2, chiave_temp_3;
void InitDati(void)
{
  uint8_t indice;
  
  
  Dati.TiltEvent.Orientamento = FLANGIA_NO_RESULT;        //init dell'orientamento flangia
  chiave_temp_1 = EEPROM_ReadByte(CHIAVE_VARIABILE_CRITICA_1);
  chiave_temp_2 = EEPROM_ReadByte(CHIAVE_VARIABILE_CRITICA_2);
  chiave_temp_3 = EEPROM_ReadByte(CHIAVE_VARIABILE_CRITICA_3);
  
  if( (chiave_temp_1 != CHIAVE_VARIABILE_CRITICA) && (chiave_temp_2 != CHIAVE_VARIABILE_CRITICA) && (chiave_temp_3 != CHIAVE_VARIABILE_CRITICA) )    //primo avvio: imposto tempi di TX di default per ogni fase
  {
    SetVariabiliCriticheDefault();                      
    SetStatoAlesea(/*(StatoAlesea_t)*/ALESEA_PRE_COLLAUDO); //StatoAlesea impostato a ACTIVE da SetVariabiliCriticheDefault e poi forzato a ALESEA_PRE_COLLAUDO
    
    NumberTxIot = NUMBER_TX_IOT_DEFAULT;
    EEPROM_WriteByte(EEPROM_NUMBER_TX_IOT, NumberTxIot);
  }
  for(uint8_t i=0; i<sizeof(AleseaICCID.ICCID)-1; i++)
  {
    AleseaICCID.ICCID[i] = EEPROM_ReadByte(EEPROM_ICCID + i);
  }
  Dati.TipoModem = (Mod_t)EEPROM_ReadByte(EEPROM_TIPO_MODEM);
  

  if(NewFirmwareVersion == 1)
  {
    NewFirmwareVersion = 0;
    ResettaDiagnosticaSupervisore();    //reset diagnostica supervisore ad ogni aggiornamento FW
  }
  else
  {
    Diagnostica.Byte1 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_1);
    Diagnostica.Byte2 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_2);
    Diagnostica.Byte3 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_3);
    Diagnostica.Byte4 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_4);
    Diagnostica.Byte5 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_5);
    Diagnostica.Byte6 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_6);
    Diagnostica.Byte7 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_7);
    Diagnostica.Byte8 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_8);
  }
  
  Diagnostica.MicroActivityCounter = EEPROM_ReadWord(EEPROM_MICRO_ACTIVITY);
  Diagnostica.ModemActivityCounter = EEPROM_ReadWord(EEPROM_MODEM_ACTIVITY);
  
  Diagnostica.CounterErrorEeprom = EEPROM_ReadByte(EEPROM_ERROR_EEPROM);
  Diagnostica.ContaStatoIn = EEPROM_ReadByte(EEPROM_CONTA_STATO_IN);
  Diagnostica.ContaStatoOut = EEPROM_ReadByte(EEPROM_CONTA_STATO_OUT);
  Diagnostica.CountErroreModemOn = EEPROM_ReadByte(EEPROM_CONTA_FAIL_MODEM_ON);
  Diagnostica.CountUnpError = EEPROM_ReadByte(EEPROM_CONTA_UNP);
  
  Diagnostica.ContaReset = EEPROM_ReadByte(EEPROM_CONTA_RESET);
  if(Diagnostica.ContaReset < 255 ) Diagnostica.ContaReset++;                   
  EEPROM_WriteByte( EEPROM_CONTA_RESET, (uint32_t)Diagnostica.ContaReset );     
  
  Diagnostica.ContatoreRisvegli = EEPROM_ReadWord(EEPROM_CONTATORE_RISVEGLI);
  LSM6DSLCounterReinit = EEPROM_ReadByte(EEPROM_LSM6DSL_CONTA_REINIT);
  
  Consumo_uA_h_tot = EEPROM_ReadWord(EEPROM_CONSUMO_TOT);

  Dati.SpinEvent.NumeroGiriCw = (float)EEPROM_ReadWord(EEPROM_NUMERO_GIRI_TOT_CW);
  Dati.SpinEvent.NumeroGiriACw = (float)EEPROM_ReadWord(EEPROM_NUMERO_GIRI_TOT_ACW);
  Dati.ShockEvent.ContaShock = EEPROM_ReadWord(EEPROM_CONTA_SHOCK);
  Dati.FreeFallEvent.ContaFreeFall = EEPROM_ReadByte(EEPROM_CONTA_FREE_FALL);
  
  Dati.NetworkPerformance.IndiceTrasmissione = EEPROM_ReadWord(EEPROM_INDICE_TRASMISSIONE);
  
  LeggiVariabileCritica(&SchIntStartUpVc);
  Dati.ActivationTiming.SchInt_START_UP = *SchIntStartUpVc.var32;
  
  LeggiVariabileCritica(&SchIntWarmUpVc);
  Dati.ActivationTiming.SchInt_WARM_UP = *SchIntWarmUpVc.var32;
  
  LeggiVariabileCritica(&SchIntActiveVc);
  Dati.ActivationTiming.SchInt_ACTIVE = *SchIntActiveVc.var32;
  
  LeggiVariabileCritica(&NumberTxStartUpVc); 
  Dati.ActivationTiming.NumberTx_START_UP = *NumberTxStartUpVc.var32;
  
  LeggiVariabileCritica(&NumberTxWarmUpVc);
  Dati.ActivationTiming.NumberTx_WARM_UP = *NumberTxWarmUpVc.var32;
  
  LeggiVariabileCritica(&GpsTimeoutVc); 
  Dati.GNSS_data.GpsTimeout = *GpsTimeoutVc.var32;
  
  LeggiVariabileCritica(&GnssSatellitiVc); 
  Dati.GNSS_data.GnssSatelliti = *GnssSatellitiVc.var32;
      
  LeggiVariabileCritica(&StatoAleseaVc); 
  SetStatoAlesea(*StatoAleseaVc.var32); 
  
  
  indice = sprintf((char*)Dati.SystemVersion.VersioneFwTx,"%u", VersioneFW); 
  sprintf((char*)Dati.SystemVersion.VersioneFwTx + indice,".%u", SottoversioneFW);
  indice = sprintf((char*)Dati.SystemVersion.VersioneHwTx,"%u", VersioneHW); 	
  sprintf((char*)Dati.SystemVersion.VersioneHwTx + indice,".%u", SottoversioneHW); 
  Dati.NetworkPerformance.InputVariation = '0';//init del campo per evitare di avere 0x00 imprevisto in buffer dati JSON
 
  //rotazione orizzontale
  Dati.SpinEvent.RotazioneOrizzontaleOn  = EEPROM_ReadByte(EEPROM_ROTAZIONE_ORIZZ);
  if(Dati.SpinEvent.RotazioneOrizzontaleOn > ROTAZIONE_ORIZZ_ON) 
  {
    Dati.SpinEvent.RotazioneOrizzontaleOn = ROTAZIONE_ORIZZ_OFF;
    EEPROM_WriteByte(EEPROM_ROTAZIONE_ORIZZ, Dati.SpinEvent.RotazioneOrizzontaleOn);
  }
  
  //numero di tx consecutive su iot central
  NumberTxIot = EEPROM_ReadByte(EEPROM_NUMBER_TX_IOT);
  if( (NumberTxIot < NUMBER_TX_IOT_MIN) || (NumberTxIot > NUMBER_TX_IOT_MAX) )
  {
    NumberTxIot = NUMBER_TX_IOT_DEFAULT;
    EEPROM_WriteByte(EEPROM_NUMBER_TX_IOT, NumberTxIot);
  }
  //contatore chiave dispostivo generata localmente
  Local_Devkey_Counter = EEPROM_ReadByte(EEPROM_CONTATORE_LOCAL_DEVKEY);
  
  //gps mode
  Dati.GNSS_data.GpsMode = EEPROM_ReadByte(EEPROM_GPS_MODE); 
  if( (Dati.GNSS_data.GpsMode != GPS_MODE_ON) && (Dati.GNSS_data.GpsMode != GPS_MODE_OFF) && (Dati.GNSS_data.GpsMode != GPS_MODE_OFF_TILL_MOVE))
  {
    Dati.GNSS_data.GpsMode = GPS_MODE_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_MODE, Dati.GNSS_data.GpsMode);
  }
  
  //gps pro
  Dati.GNSS_data.GpsPro = EEPROM_ReadByte(EEPROM_GPS_PRO);
  if( (Dati.GNSS_data.GpsPro != GPS_PRO_STATE_OFF) && (Dati.GNSS_data.GpsPro != GPS_PRO_STATE_ON) )
  {
    Dati.GNSS_data.GpsPro = GPS_PRO_STATE_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_PRO, Dati.GNSS_data.GpsPro);
  }
  Dati.GNSS_data.GpsProTime = EEPROM_ReadByte(EEPROM_GPS_PRO_TIME);
  if( (Dati.GNSS_data.GpsProTime > GPS_PRO_TIME_MAX) || (Dati.GNSS_data.GpsProTime < GPS_PRO_TIME_MIN) ) 	
  {
    Dati.GNSS_data.GpsProTime = GPS_PRO_TIME_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_PRO_TIME, Dati.GNSS_data.GpsProTime);
  }
  
  //gps freq forcing
  Dati.GNSS_data.GpsFreqForcing = EEPROM_ReadByte(EEPROM_GPS_FREQ_FORCING);
  if( (Dati.GNSS_data.GpsFreqForcing > GPS_FREQ_FORCING_MAX) || (Dati.GNSS_data.GpsFreqForcing < GPS_FREQ_FORCING_MIN) ) 	
  {
    Dati.GNSS_data.GpsFreqForcing = GPS_FREQ_FORCING_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_FREQ_FORCING, Dati.GNSS_data.GpsFreqForcing);
  }
  
  //soglia movimentazione
  Dati.MovementEvent.SogliaMovimento = EEPROM_ReadByte(EEPROM_SOGLIA_MOV);
  if( (Dati.MovementEvent.SogliaMovimento > SOGLIA_MOV_MAX) || (Dati.MovementEvent.SogliaMovimento < SOGLIA_MOV_MIN) ) 	
  {
    Dati.MovementEvent.SogliaMovimento = SOGLIA_MOV_DEFAULT;
    EEPROM_WriteByte(EEPROM_SOGLIA_MOV, Dati.MovementEvent.SogliaMovimento);
  }
  SogliaAccelerometro = Dati.MovementEvent.SogliaMovimento;
  SetLSM6DSL(ACC_SOGLIA);//SogliaAccelerometroSet(SogliaAccelerometro);
  
  //soglia shock
  Dati.ShockEvent.SogliaShock = EEPROM_ReadByte(EEPROM_SOGLIA_SHOCK);
  if( (Dati.ShockEvent.SogliaShock > SOGLIA_SHOCK_MAX) || (Dati.ShockEvent.SogliaShock < SOGLIA_SHOCK_MIN) ) 	
  {
    Dati.ShockEvent.SogliaShock = SOGLIA_SHOCK_DEFAULT;
    EEPROM_WriteByte(EEPROM_SOGLIA_SHOCK, Dati.ShockEvent.SogliaShock);
  }
  
  //filtro movimento
  Dati.MovementEvent.FiltroMovimento = EEPROM_ReadWord(EEPROM_FILTRO_QUADRANTE_FERMO);
  if( (Dati.MovementEvent.FiltroMovimento > FILTRO_QUADRANTE_FERMO_MAX) || (Dati.MovementEvent.FiltroMovimento < FILTRO_QUADRANTE_FERMO_MIN) ) 	
  {
    Dati.MovementEvent.FiltroMovimento = FILTRO_QUADRANTE_FERMO_DEFAULT;
    EEPROM_WriteWord(EEPROM_FILTRO_QUADRANTE_FERMO, Dati.MovementEvent.FiltroMovimento);
  }
  
  //gps timeout
  Dati.GNSS_data.GpsTimeout = EEPROM_ReadWord(EEPROM_STATO_GPS_TIMEOUT);
  if( (Dati.GNSS_data.GpsTimeout > GPS_TIMEOUT_MAX) || (Dati.GNSS_data.GpsTimeout < GPS_TIMEOUT_MIN) ) 	
  {
    Dati.GNSS_data.GpsTimeout = GPS_TIMEOUT_DEFAULT;
    EEPROM_WriteWord(EEPROM_STATO_GPS_TIMEOUT, Dati.GNSS_data.GpsTimeout);
    SalvaVariabileCritica(&GpsTimeoutVc, BANCO_1_2);
  }
    
  //schedulazione movimento
  Dati.MovementEvent.MoveSchInterval = EEPROM_ReadWord(EEPROM_MOVE_SCH_INTERVAL);
  if( (Dati.MovementEvent.MoveSchInterval > MOVE_SCH_INTERVAL_MAX) || (Dati.MovementEvent.MoveSchInterval < MOVE_SCH_INTERVAL_MIN) ) 	
  {
    Dati.MovementEvent.MoveSchInterval = MOVE_SCH_INTERVAL_DEFAULT;
    EEPROM_WriteWord(EEPROM_MOVE_SCH_INTERVAL, Dati.MovementEvent.MoveSchInterval);
  }

  //schedulazione spin
  Dati.SpinEvent.SpinSchInterval = EEPROM_ReadWord(EEPROM_SPIN_SCH_INTERVAL);
  if( (Dati.SpinEvent.SpinSchInterval > SPIN_SCH_INTERVAL_MAX) || (Dati.SpinEvent.SpinSchInterval < SPIN_SCH_INTERVAL_MIN) ) 	
  {
    Dati.SpinEvent.SpinSchInterval = SPIN_SCH_INTERVAL_DEFAULT;
    EEPROM_WriteWord(EEPROM_SPIN_SCH_INTERVAL, Dati.SpinEvent.SpinSchInterval);
  }

  //schedulazione shock
  Dati.ShockEvent.ShockSchInterval = EEPROM_ReadWord(EEPROM_SHOCK_SCH_INTERVAL);
  if( (Dati.ShockEvent.ShockSchInterval > SHOCK_SCH_INTERVAL_MAX) || (Dati.ShockEvent.ShockSchInterval < SHOCK_SCH_INTERVAL_MIN) ) 	
  {
    Dati.ShockEvent.ShockSchInterval = SHOCK_SCH_INTERVAL_DEFAULT;
    EEPROM_WriteWord(EEPROM_SHOCK_SCH_INTERVAL, Dati.ShockEvent.ShockSchInterval);
  }
  
  //profilo temperatura
  Dati.AleseaMeasure.ProfiloTemperaturaOn = EEPROM_ReadByte(EEPROM_PROFILO_TEMPERATURA_ON);
  if( (Dati.AleseaMeasure.ProfiloTemperaturaOn != PROFILO_TEMPERATURA_OFF) && (Dati.AleseaMeasure.ProfiloTemperaturaOn != PROFILO_TEMPERATURA_ON) )
  {
    Dati.AleseaMeasure.ProfiloTemperaturaOn = PROFILO_TEMPERATURA_ON;
    EEPROM_WriteByte(EEPROM_PROFILO_TEMPERATURA_ON, Dati.AleseaMeasure.ProfiloTemperaturaOn);
  }
  
  //enable WiFi (scansione)
  WiFiParam.WiFiScanEnable = EEPROM_ReadByte(EEPROM_WIFI_ENABLE);
  if( (WiFiParam.WiFiScanEnable != WIFI_ENABLE_ON) && (WiFiParam.WiFiScanEnable != WIFI_ENABLE_OFF) )
  {
    WiFiParam.WiFiScanEnable = WIFI_ENABLE_ON;
    EEPROM_WriteByte(EEPROM_WIFI_ENABLE, WiFiParam.WiFiScanEnable);
  }
  //enable WiFi (comunicazione)
  WiFiParam.WiFiComEnable = EEPROM_ReadByte(EEPROM_WIFI_COM_ENABLE);
  if( (WiFiParam.WiFiComEnable != WIFI_ENABLE_ON) && (WiFiParam.WiFiComEnable != WIFI_ENABLE_OFF) )
  {
    WiFiParam.WiFiComEnable = WIFI_ENABLE_OFF;
    EEPROM_WriteByte(EEPROM_WIFI_COM_ENABLE, WiFiParam.WiFiComEnable);
  }
  //numero di tx consecutive su WiFi
  WiFiParam.WiFiNumTxSwitchModem = EEPROM_ReadByte(EEPROM_WIFI_NUM_TX_SWITCH_MODEM);
  if( (WiFiParam.WiFiNumTxSwitchModem < WIFI_NUM_TX_SWITCH_MODEM_MIN) || (WiFiParam.WiFiNumTxSwitchModem > WIFI_NUM_TX_SWITCH_MODEM_MAX) )
  {
    WiFiParam.WiFiNumTxSwitchModem = WIFI_NUM_TX_SWITCH_MODEM_DEFAULT;
    EEPROM_WriteByte(EEPROM_WIFI_NUM_TX_SWITCH_MODEM, WiFiParam.WiFiNumTxSwitchModem);
  }
  
  
  NdefCompile(Dati.StatoAlesea);

  //interfaccia tra applicazione ed Azure device-twins
  InitInterfacciaDato();
}


void AllineamentoEEPROM(void)   
{ 
  uint32_t periodo_temp, periodo_temp1, periodo_temp2;
  uint8_t stato_temp, stato_temp1, stato_temp2;
  uint32_t eeprom_temp;
  uint8_t dato_temp;
  //uint8_t indice;
  //Versione/SottoversioneFW: inizializzato in InitDati() => ridondante farlo qui
  /*
  indice = sprintf((char*)Dati.SystemVersion.VersioneFwTx,"%u", VersioneFW); 
  sprintf((char*)Dati.SystemVersion.VersioneFwTx + indice,".%u", SottoversioneFW);
  indice = sprintf((char*)Dati.SystemVersion.VersioneHwTx,"%u", VersioneHW); 	
  sprintf((char*)Dati.SystemVersion.VersioneHwTx + indice,".%u", SottoversioneHW); 
  Dati.NetworkPerformance.InputVariation = '0';//init del campo per evitare di avere 0x00 imprevisto in buffer dati JSON
  */
  
  //Orientamento Flangia: inizializzato in InitDati() => ridondante farlo qui
  /*
  Dati.TiltEvent.Orientamento = FLANGIA_NO_RESULT;        //init dell'orientamento flangia
  */
   
  SetVariabiliCriticheDefault();     //settato a default il set delle variabili critiche                 
  
  //SchIntActiveVc: Ricarico il tempo di schedulazione da EEPROM e lo salvo nella variabile critica solo se le tre posizioni contengono lo stesso valore ed � diverso da zero


  periodo_temp = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE1_FW4);
  periodo_temp1 = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE2_FW4);
  periodo_temp2 = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE3_FW4);
  
  if( (periodo_temp == periodo_temp1 ) && ( periodo_temp == periodo_temp2 ) && (periodo_temp != 0)  )
  {
    SetInterval_ACTIVE(periodo_temp);
  }
  
 
  //StatoAlesea: caricato da EEPROM pos FW4, se non coerente forzato a ACTIVE e salvato in EEPROM pos FW5	
  stato_temp = EEPROM_ReadByte(EEPROM_STATO_ALESEA1_FW4);
  stato_temp1 = EEPROM_ReadByte( EEPROM_STATO_ALESEA2_FW4 );
  stato_temp2 = EEPROM_ReadByte( EEPROM_STATO_ALESEA3_FW4 );
  if( (stato_temp == stato_temp1) && (stato_temp == stato_temp2) )	//se le tre letture sono concordi copio il dato in ram
  {
    SetStatoAlesea((uint32_t)stato_temp);
  }
  else	//se non sono concordi mi porto in ACTIVE
  {
    SetStatoAlesea(ALESEA_ACTIVE);
    EEPROM_WriteByte( EEPROM_STATO_ALESEA, Dati.StatoAlesea );  
  }
  dato_temp = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO1_FW4);
  EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO_1, dato_temp);
  dato_temp = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO2_FW4);
  EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO_2, dato_temp);
  dato_temp = EEPROM_ReadByte(EEPROM_CHIAVE_COLLAUDO3_FW4);
  EEPROM_WriteByte(EEPROM_CHIAVE_COLLAUDO_3, dato_temp);
  
  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////

  //ICCID: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  for(uint8_t i=0; i<sizeof(AleseaICCID.ICCID)-1; i++)
  {
    AleseaICCID.ICCID[i] = EEPROM_ReadByte(EEPROM_ICCID_FW4+i);
    EEPROM_WriteByte(EEPROM_ICCID + i, AleseaICCID.ICCID[i]);
  }

  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////
  
  //IndiceTrasmissione: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Dati.NetworkPerformance.IndiceTrasmissione = EEPROM_ReadWord(EEPROM_INDICE_TRASMISSIONE_FW4);
  EEPROM_WriteWord(EEPROM_INDICE_TRASMISSIONE, Dati.NetworkPerformance.IndiceTrasmissione);
  
  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////
  //Tipo modem: viene aggiornato in GESTIONE STRUTTURA => ridondante ricaricarlo da EEPROM FW4
  Dati.TipoModem = (Mod_t)EEPROM_ReadByte(EEPROM_TIPO_MODEM_FW4);
  EEPROM_WriteByte(EEPROM_TIPO_MODEM, Dati.TipoModem);
  
  //Consumo_uA_h_tot: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Consumo_uA_h_tot = EEPROM_ReadWord(EEPROM_CONSUMO_TOT_FW4);
  EEPROM_WriteWord(EEPROM_CONSUMO_TOT, (int32_t)Consumo_uA_h_tot);

  
  //Counter spin/shock/freefall: caricati da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Dati.SpinEvent.NumeroGiriCw = (float)EEPROM_ReadWord(EEPROM_NUMERO_GIRI_TOT_CW_FW4);
  EEPROM_WriteWord(EEPROM_NUMERO_GIRI_TOT_CW, (uint32_t)Dati.SpinEvent.NumeroGiriCw);
  Dati.SpinEvent.NumeroGiriACw = (float)EEPROM_ReadWord(EEPROM_NUMERO_GIRI_TOT_ACW_FW4);
  EEPROM_WriteWord(EEPROM_NUMERO_GIRI_TOT_ACW, (uint32_t)Dati.SpinEvent.NumeroGiriACw);
  Dati.ShockEvent.ContaShock = EEPROM_ReadWord(EEPROM_CONTA_SHOCK_FW4);
  EEPROM_WriteWord(EEPROM_CONTA_SHOCK, Dati.ShockEvent.ContaShock);
  Dati.FreeFallEvent.ContaFreeFall = EEPROM_ReadWord(EEPROM_CONTA_FREE_FALL_FW4);
  EEPROM_WriteByte(EEPROM_CONTA_FREE_FALL, Dati.FreeFallEvent.ContaFreeFall);

  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////
  
  //RotazioneOrizzontaleOn: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Dati.SpinEvent.RotazioneOrizzontaleOn  = EEPROM_ReadByte(EEPROM_ROTAZIONE_ORIZZ_FW4);
  if(Dati.SpinEvent.RotazioneOrizzontaleOn > ROTAZIONE_ORIZZ_ON) Dati.SpinEvent.RotazioneOrizzontaleOn = ROTAZIONE_ORIZZ_OFF;
  EEPROM_WriteByte(EEPROM_ROTAZIONE_ORIZZ, Dati.SpinEvent.RotazioneOrizzontaleOn);
  //soglia movimentazione: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Dati.MovementEvent.SogliaMovimento = EEPROM_ReadByte(EEPROM_SOGLIA_MOV_FW4);
  if( (Dati.MovementEvent.SogliaMovimento > SOGLIA_MOV_MAX) || (Dati.MovementEvent.SogliaMovimento < SOGLIA_MOV_MIN) ) 	
  {
    Dati.MovementEvent.SogliaMovimento = SOGLIA_MOV_DEFAULT;
  }
  EEPROM_WriteByte(EEPROM_SOGLIA_MOV, Dati.MovementEvent.SogliaMovimento);
  SogliaAccelerometro = Dati.MovementEvent.SogliaMovimento;
  SetLSM6DSL(ACC_SOGLIA);//SogliaAccelerometroSet(SogliaAccelerometro);

  
  //Schedulazione movimento
  Dati.MovementEvent.MoveSchInterval = EEPROM_ReadWord(EEPROM_MOVE_SCH_INTERVAL_FW4);
  if( (Dati.MovementEvent.MoveSchInterval > MOVE_SCH_INTERVAL_MAX) || (Dati.MovementEvent.MoveSchInterval < MOVE_SCH_INTERVAL_MIN) ) 	
  {
    Dati.MovementEvent.MoveSchInterval = MOVE_SCH_INTERVAL_DEFAULT;
    EEPROM_WriteWord(EEPROM_MOVE_SCH_INTERVAL, Dati.MovementEvent.MoveSchInterval);
  }

  //soglia shock: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Dati.ShockEvent.SogliaShock = EEPROM_ReadByte(EEPROM_SOGLIA_SHOCK_FW4);
  if( (Dati.ShockEvent.SogliaShock > SOGLIA_SHOCK_MAX) || (Dati.ShockEvent.SogliaShock < SOGLIA_SHOCK_MIN) ) 	
  {
    Dati.ShockEvent.SogliaShock = SOGLIA_SHOCK_DEFAULT;
  }
  EEPROM_WriteByte(EEPROM_SOGLIA_SHOCK, Dati.ShockEvent.SogliaShock);
  
  //filtro movimento: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Dati.MovementEvent.FiltroMovimento = EEPROM_ReadWord(EEPROM_FILTRO_QUADRANTE_FERMO_FW4);
  if( (Dati.MovementEvent.FiltroMovimento > FILTRO_QUADRANTE_FERMO_MAX) || (Dati.MovementEvent.FiltroMovimento < FILTRO_QUADRANTE_FERMO_MIN) ) 	
  {
    Dati.MovementEvent.FiltroMovimento = FILTRO_QUADRANTE_FERMO_DEFAULT;
  }
  EEPROM_WriteWord(EEPROM_FILTRO_QUADRANTE_FERMO, Dati.MovementEvent.FiltroMovimento); 
  
  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////
  
  //gps timeout => forzo al default
  //Dati.GNSS_data.GpsTimeout = EEPROM_ReadByte(EEPROM_STATO_GPS_TIMEOUT_FW4);
  //if( (60*Dati.GNSS_data.GpsTimeout > GPS_TIMEOUT_MAX) || (60*Dati.GNSS_data.GpsTimeout < GPS_TIMEOUT_MIN) ) 	//il dato in EEPROM era in minuti => ora la gestione � in secondi
  //{
  Dati.GNSS_data.GpsTimeout = GPS_TIMEOUT_DEFAULT;
  EEPROM_WriteByte(EEPROM_STATO_GPS_TIMEOUT, Dati.GNSS_data.GpsTimeout);
  //}
  //else
  //{
  //  Dati.GNSS_data.GpsTimeout = 60*Dati.GNSS_data.GpsTimeout; //converto in secondi
  //}
  SalvaVariabileCritica(&GpsTimeoutVc, BANCO_1_2);
  
  //gps pro: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Dati.GNSS_data.GpsPro = EEPROM_ReadByte(EEPROM_GPS_PRO_FW4);
  if( (Dati.GNSS_data.GpsPro != GPS_PRO_STATE_OFF) && (Dati.GNSS_data.GpsPro != GPS_PRO_STATE_ON) )
  {
    Dati.GNSS_data.GpsPro = GPS_PRO_STATE_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_PRO, Dati.GNSS_data.GpsPro);
  }
  Dati.GNSS_data.GpsProTime = EEPROM_ReadByte(EEPROM_GPS_PRO_TIME_FW4);
  if( (Dati.GNSS_data.GpsProTime > GPS_PRO_TIME_MAX) || (Dati.GNSS_data.GpsProTime < GPS_PRO_TIME_MIN) ) 	
  {
    Dati.GNSS_data.GpsProTime = GPS_PRO_TIME_DEFAULT;
  }
  EEPROM_WriteByte(EEPROM_GPS_PRO_TIME, Dati.GNSS_data.GpsProTime);
  EEPROM_WriteByte(EEPROM_GPS_PRO, Dati.GNSS_data.GpsPro);
  //gps freq forcing: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5 
  Dati.GNSS_data.GpsFreqForcing = EEPROM_ReadByte(EEPROM_GPS_FREQ_FORCING_FW4);
  if( (Dati.GNSS_data.GpsFreqForcing > GPS_FREQ_FORCING_MAX) || (Dati.GNSS_data.GpsFreqForcing < GPS_FREQ_FORCING_MIN) ) 	
  {
    Dati.GNSS_data.GpsFreqForcing = GPS_FREQ_FORCING_DEFAULT;
    EEPROM_WriteByte(EEPROM_GPS_FREQ_FORCING, Dati.GNSS_data.GpsFreqForcing);
  }
  
  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////
  
  //ProfiloTemperaturaOn: caricato da EEPROM pos FW4 e salvato in EEPROM pos FW5
  Dati.AleseaMeasure.ProfiloTemperaturaOn = EEPROM_ReadByte(EEPROM_PROFILO_TEMPERATURA_ON_FW4);
  if( (Dati.AleseaMeasure.ProfiloTemperaturaOn != PROFILO_TEMPERATURA_OFF) && (Dati.AleseaMeasure.ProfiloTemperaturaOn != PROFILO_TEMPERATURA_ON) )
  {
    Dati.AleseaMeasure.ProfiloTemperaturaOn = PROFILO_TEMPERATURA_ON;
  }
  EEPROM_WriteByte(EEPROM_PROFILO_TEMPERATURA_ON, Dati.AleseaMeasure.ProfiloTemperaturaOn);
  
  
  //Diagnostica ------ GESTIRE SALVATAGGIO  
  Diagnostica.ContaReset = EEPROM_ReadByte(EEPROM_CONTA_RESET_FW4);   
  EEPROM_WriteByte(EEPROM_CONTA_RESET, Diagnostica.ContaReset);
  
  Diagnostica.MicroActivityCounter = EEPROM_ReadWord(EEPROM_MICRO_ACTIVITY_FW4);
  EEPROM_WriteWord(EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter);
  
  Diagnostica.ModemActivityCounter = EEPROM_ReadWord(EEPROM_MODEM_ACTIVITY_FW4);
  EEPROM_WriteWord(EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter);
  
  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////
  
  eeprom_temp = EEPROM_ReadWord(EEPROM_RESET_EEPROM_FW4);
  if(eeprom_temp >= 255)        Diagnostica.CounterErrorEeprom = 255;
    else Diagnostica.CounterErrorEeprom = eeprom_temp;
  EEPROM_WriteByte(EEPROM_ERROR_EEPROM, Diagnostica.CounterErrorEeprom);
    
  Diagnostica.ContaStatoIn = EEPROM_ReadByte(EEPROM_CONTA_STATO_IN_FW4);
  EEPROM_WriteByte(EEPROM_CONTA_STATO_IN, Diagnostica.ContaStatoIn);
  
  Diagnostica.ContaStatoOut = EEPROM_ReadByte(EEPROM_CONTA_STATO_OUT_FW4);
  EEPROM_WriteByte(EEPROM_CONTA_STATO_OUT, Diagnostica.ContaStatoOut); 
    
  Diagnostica.CountErroreModemOn = EEPROM_ReadByte(EEPROM_CONTA_FAIL_MODEM_ON_FW4);
  EEPROM_WriteByte(EEPROM_CONTA_FAIL_MODEM_ON, Diagnostica.CountErroreModemOn);
    
  Diagnostica.CountUnpError = EEPROM_ReadByte(EEPROM_CONTA_UNP_FW4);
  EEPROM_WriteByte(EEPROM_CONTA_UNP, Diagnostica.CountUnpError);
   
  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////
  
  Diagnostica.Byte1 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_1_FW4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_1, Diagnostica.Byte1);
  Diagnostica.Byte2 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_2_FW4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_2, Diagnostica.Byte2);
  Diagnostica.Byte3 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_3_FW4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_3, Diagnostica.Byte3);
  Diagnostica.Byte4 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_4_FW4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_4, Diagnostica.Byte4);
  Diagnostica.Byte5 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_5_FW4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_5, Diagnostica.Byte5);
  Diagnostica.Byte6 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_6_FW4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_6, Diagnostica.Byte6);
  Diagnostica.Byte7 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_7_FW4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_7, Diagnostica.Byte7);
  Diagnostica.Byte8 = EEPROM_ReadByte(EEPROM_DIAGNOSTICA_8_FW4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_8, Diagnostica.Byte8);
  
  
  //Allineamento contatori
  Diagnostica.ContatoreRisvegli = 0;
  EEPROM_WriteWord(EEPROM_CONTATORE_RISVEGLI, (uint32_t)Diagnostica.ContatoreRisvegli);
  
  Local_Devkey_Counter = 0;
  EEPROM_WriteByte(EEPROM_CONTATORE_LOCAL_DEVKEY, Local_Devkey_Counter);
  
  LSM6DSLCounterReinit = 0;
  EEPROM_WriteByte(EEPROM_LSM6DSL_CONTA_REINIT, LSM6DSLCounterReinit);
  //
  
  //////////////////////////////////////////////
  //RefreshWD();
  //////////////////////////////////////////////
  
  //EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO_1, CHIAVE_COLLAUDO );    EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO_2, CHIAVE_COLLAUDO );    EEPROM_WriteByte( EEPROM_CHIAVE_COLLAUDO_3, CHIAVE_COLLAUDO );    //riscrivo le chiavi di collaudo che certamente � stato eseguito

  EEPROM_WriteByte( EEPROM_CHIAVE_FW_1, CHIAVE_FW );    EEPROM_WriteByte( EEPROM_CHIAVE_FW_2, CHIAVE_FW );    EEPROM_WriteByte( EEPROM_CHIAVE_FW_3, CHIAVE_FW );
  
}


/*
void Check_EEPROM_vs_NFC(void)
{
  uint16_t val[3];
  NFC_Read_var(&val[0]);
  
  if(val[0] > Dati.NetworkPerformance.IndiceTrasmissione)  
    Dati.NetworkPerformance.IndiceTrasmissione = val[0];
  if(val[1] > Dati.SpinEvent.NumeroGiriCw)  
    Dati.SpinEvent.NumeroGiriCw = val[1];
  if(val[2] > Dati.SpinEvent.NumeroGiriACw)  
    Dati.SpinEvent.NumeroGiriACw = val[2];  
}
*/
