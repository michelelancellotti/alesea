#include "modem.h"
#include "GPS_GSM_SM.h"
#include "nfc_SM.h"
#include "stm32l0xx_hal.h"
#include "Gestione_dato.h"
#include "LSM6DSL_SM.h"
#include "EEPROM.h"
#include "crc.h"
#include "bg_96.h"
#include "Gestione_batteria.h"
#include "wwdg.h"
#include "Globals.h"



uint8_t StatusModemMonitor;
uint8_t GpsData[2];
uint8_t UpgradeData[2];
uint16_t GpsRegTemp;

uint16_t TempoAcqGpsTemp;
float gsm_latitudineTemp;						
float gsm_longitudineTemp;
uint8_t IndiceTrasmissioneCollaudo;

AleseaMacchinaStatiModem StatoGPS_GSM;
ModemTrigger_t	OperazioneModem;
uint8_t FlagWakeupDaRTC;
uint8_t FlagUpgradeSuccess;
uint8_t FlagGetStatusOn;
uint8_t FlagErroreModemOn;
uint8_t FlagErroreRete = 0;
uint8_t FlagErroreTest = 0;
uint8_t FlagPrimoInvioEffettuato;
uint8_t retry_subscribe = 0;
uint8_t Count_MA_UNP_ERROR = 0;
ModemAppli_t AppliError;
uint8_t ContaTxPreActive;
uint8_t ContaTxPreActive;
uint8_t FactoryResetPendente;
uint8_t DisattivazionePendente;
uint8_t AggiornamentoFwPendente;

/*VAriabili gestione retray */
uint8_t FlagBufferFull;
uint8_t IndiceDatoSalvato;
uint8_t IndiceDatoLetto;


void MacchinaStatiGPS_GSM(void)
{	
  uint32_t periodo_active_temp1, periodo_active_temp2, periodo_active_temp3;
  //eseguo il controllo sullo stato solo dopo accensione Modem
  if(FlagGetStatusOn)	
  {
    if(modem_GET_Status(OperazioneModem) == MA_BUSY)	return;
    if(modem_GET_Status(OperazioneModem) == MA_UNP_ERROR ) 
    {
      if(Count_MA_UNP_ERROR == 0)
      {
        StatoGPS_GSM = MODEM_ON; 
        Count_MA_UNP_ERROR++;
      }
      else	
      {
        if(Diagnostica.CountUnpError < 0xFF)
        {
          Diagnostica.CountUnpError++;
          EEPROM_WriteByte(EEPROM_CONTA_UNP, Diagnostica.CountUnpError);
        }
        StatoGPS_GSM = MODEM_ERROR;
      }
    }
    if(modem_GET_Status(OperazioneModem) == MA_POWER_ON_FAIL)
    {			
      StatoGPS_GSM = MODEM_ERROR; 
      FlagErroreModemOn = 1;
    }
    if(modem_GET_Status(OperazioneModem) == MA_NETWORK_ERROR)	FlagErroreRete = 1;
  }
  switch(StatoGPS_GSM)
  {
  case GET_TEMPERATURA:
    Diagnostica.ContaStatoIn++;
    EEPROM_WriteByte( EEPROM_CONTA_STATO_IN, Diagnostica.ContaStatoIn );
    //acquisisco la temperatura solo se non � stata acquisita durante una rotazione
    FlagTemperaturaAcquisita = EEPROM_ReadByte(EEPROM_FLAG_TEMPERATURA_ACQ);
    if(FlagTemperaturaAcquisita == 0)	//temperatura non acquisita
    {
      GetTemperature();
    }
    else
    {
      FlagTemperaturaAcquisita = 0;
      EEPROM_WriteByte( EEPROM_FLAG_TEMPERATURA_ACQ, FlagTemperaturaAcquisita );
    }
    StatoGPS_GSM = GET_ORIENTAMENTO;
    break;
    
  case GET_ORIENTAMENTO:
    if(GetOrientamento() == 1)
      StatoGPS_GSM = MODEM_ON;
    break;
    
  case MODEM_ON:
    if(modem_Trigger(TRG_MODEM_ON, NULL, NULL) == MA_TRIG_OK)
    {
      FlagPrimoInvioEffettuato = 0;		//nessun invio in corso
#ifdef GPS_ON
      if( (Dati.StatoAlesea == ALESEA_COLLAUDO) && (IndiceTrasmissioneCollaudo/*Dati.IndiceTrasmissione*/ > 0) )	//disabilito il GPS sulle connessioni di collaudo tranne la prima
      {
        StatoGPS_GSM = SYNC_RETE;
        Dati.TempoConnessioneGps = 0;	//pulisco la variabile perch� non eseguendo il GPS resta sporca dal dato preceddente
      }
      else
      {
        StatoGPS_GSM = GNSS_ON;
      }
#else		
      StatoGPS_GSM = SYNC_RETE;
#endif
      if(Dati.StatoAlesea == ALESEA_COLLAUDO)	set_Topic_MQTT(DEBUG_T);
      else set_Topic_MQTT(TEST_T);
      
      FlagGetStatusOn = 1;
    }
    break;
    
  case GNSS_ON:
    /*if(Dati.GpsAssistito == 1)	GpsData[0] = GNSS_ASS;	//GPS assistito
    else*/ GpsData[0] = GNSS_STD;	//GPS standard
    
    GpsData[1] = Dati.GpsTimeout - GPS_DELTA_CONVERSIONE;
    FlagConsumoGPSOn = 1;
    HAL_GPIO_WritePin(GPS_AMPLI_GPIO_Port, GPS_AMPLI_Pin, GPIO_PIN_SET);	//Accensione Ampli in ingresso fase GPS
    if(modem_Trigger(TRG_GNSS_ON, &GpsData[0], 2) == MA_TRIG_OK)
    {	
      OperazioneModem = TRG_GNSS_ON;
      StatoGPS_GSM = GET_INFO_GPS;
    }
    break;
    
  case GET_INFO_GPS:
    if(modem_GET_TempoGNSS(&TempoAcqGpsTemp, &GpsRegTemp) == MA_OK)
    {
      //tempo aggiornato: copio nella struttura
      Dati.TempoConnessioneGps = TempoAcqGpsTemp;
    }
    else
    {
      Dati.TempoConnessioneGps = TempoAcqGpsTemp; //passo comunque il tempo registrato anche se GPS fallito
    }
    if(modem_GET_InfoGNSS(&gsm_latitudineTemp, &gsm_longitudineTemp) == MA_OK)
    {
      //coordinate aggiornate: copio nella struttura
      Dati.gps_latitudine = gsm_latitudineTemp;
      Dati.gps_longitudine = gsm_longitudineTemp;
    }
    else
    {
      Dati.gps_latitudine = 0; //coordinate non aggiornate: metto a zero
      Dati.gps_longitudine = 0;
    }
    Dati.GpsRegTemp = GpsRegTemp;
    HAL_GPIO_WritePin(GPS_AMPLI_GPIO_Port, GPS_AMPLI_Pin, GPIO_PIN_RESET);	//Spegnimento Ampli GPS
    FlagConsumoGPSOn = 0;
    StatoGPS_GSM = SYNC_RETE;
    break;
    
  case SYNC_RETE:
    if(modem_Trigger(TRG_SYNC_RETE, NULL, NULL) == MA_TRIG_OK)
    {	
      OperazioneModem = TRG_SYNC_RETE;
      StatoGPS_GSM = CONFIGURA_RETE;	
      FlagConsumoGSMOn = 1;
    }
    break;	
    
  case CONFIGURA_RETE:
    if(modem_Trigger(TRG_CONNETTI_MQTT, NULL, NULL) == MA_TRIG_OK)
    {
      OperazioneModem = TRG_CONNETTI_MQTT;
      StatoGPS_GSM = GET_PARAMETRI_GSM;
    }
    break;
    
  case GET_PARAMETRI_GSM:
    Dati.SizeOfICCID = sizeof(Dati.ICCID) - 1;
    modem_GET_ICCID(Dati.ICCID, &Dati.SizeOfICCID);
    Dati.ICCID[sizeof(Dati.ICCID) - 1] = 0;
    if(Dati.StatoAlesea == ALESEA_COLLAUDO) //ATTENZIONE!! spostare indirizzo STATO_ALESEA perch� ICCID ora � 21 e on 20  //DEBUG !!!!!!!!!!!!!
    {
      for(uint8_t i = 0; i< sizeof(Dati.ICCID) -1; i++)
      {
        EEPROM_WriteByte(EEPROM_ICCID + i, Dati.ICCID[i]);
      }
    }
    modem_GET_TempoRegistrazioneRete(&Dati.TempoRegGsm, &Dati.RegRete, &Dati.LastError);
    modem_GET_RSSI(&Dati.Rssi);
    modem_GET_InfoCella(Dati.TrasmissionChannel, &Dati.MMC, &Dati.MNC, &Dati.CID, &Dati.LAC_TAC);
    Dati.SizeOfDataOra = sizeof(Dati.data_ora);
    modem_GET_DataOra(Dati.data_ora, &Dati.SizeOfDataOra);
    
    StatoGPS_GSM = INTEGRA_CORRENTE;
    break;
    
  case INTEGRA_CORRENTE:
    Consumo_uA_h_tot += GetConsumo();	
    EEPROM_WriteWord(EEPROM_CONSUMO_TOT, (int32_t)Consumo_uA_h_tot);
    Dati.CaricaResidua = CalcolaCaricaResidua(Consumo_uA_h_tot);
    if(FlagNoRxMQTT)	//evito di ricevere comandi dal portale se il flag � attivo (attivazione flag su specifici comandi da app)
    {
      StatoGPS_GSM = GESTIONE_STRUTTURA;
      FlagNoRxMQTT = 0;
    }
    else	
    {
      StatoGPS_GSM = RICEVI_MQTT;
    }
    break;
    
  case GESTIONE_STRUTTURA:
    Dati.IndiceTrasmissione++;
    if(Dati.StatoAlesea == ALESEA_COLLAUDO) IndiceTrasmissioneCollaudo++;
    EEPROM_WriteWord(EEPROM_INDICE_TRASMISSIONE, Dati.IndiceTrasmissione);	//incremento l'indice di trasmissione
    
    if(Dati.StatoAlesea == ALESEA_ACTIVE)
    {
      if(ContaTxPreActive < NUMERO_TX_ATTIVAZIONE) 	
      {
        ContaTxPreActive++;
        EEPROM_WriteWord(EEPROM_PREACTIVE_COUNT, ContaTxPreActive);
        //aggiunto cambio di schedulazione dopo un numero dato di trasmissioni
        
        
        if(ContaTxPreActive == NUMERO_TX_ATTIVAZIONE_4TX)
        {
          Dati.SchedulingInterval = PERIODO_4TX_AL_GIORNO;
          EEPROM_WriteWord(EEPROM_TX_TIME1, Dati.SchedulingInterval);
          EEPROM_WriteWord(EEPROM_TX_TIME2, Dati.SchedulingInterval);
          EEPROM_WriteWord(EEPROM_TX_TIME3, Dati.SchedulingInterval);
        }
        
        
        if(ContaTxPreActive == NUMERO_TX_ATTIVAZIONE)
        {
          periodo_active_temp1 = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE1);
          periodo_active_temp2 = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE2);
          periodo_active_temp3 = EEPROM_ReadWord(EEPROM_PERIODO_ACTIVE3);
          if( (periodo_active_temp1 == periodo_active_temp2) && ( periodo_active_temp1 == periodo_active_temp3) && (periodo_active_temp1 > 0) ) //solo se il dato in memoria � valido lo leggo altrimenti ripristino il dato di fabbrica
          {
            Dati.SchedulingInterval = periodo_active_temp1; 	//ripristino periodo tx
            
          }
          else	//aggiorno EEPROM con il dato attuale
          {
            Dati.SchedulingInterval = PERIODO_ACTIVE;
            EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE1, Dati.SchedulingInterval/*(int32_t)Dati.SchedulingInterval*/ );
            EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE2, Dati.SchedulingInterval/*(int32_t)Dati.SchedulingInterval*/ );
            EEPROM_WriteWord(EEPROM_PERIODO_ACTIVE3, Dati.SchedulingInterval/*(int32_t)Dati.SchedulingInterval*/ );
          }
          EEPROM_WriteWord( EEPROM_TX_TIME1, (int32_t)Dati.SchedulingInterval );
          EEPROM_WriteWord( EEPROM_TX_TIME2, (int32_t)Dati.SchedulingInterval );
          EEPROM_WriteWord( EEPROM_TX_TIME3, (int32_t)Dati.SchedulingInterval );
        }
      }
    }
    if( SalvataggioStrutturaDati(IndiceDatoSalvato) == 0 )	//Salvataggio in EEPROM della struttura dati
    {
      FlagMicroReset = 1;	//se la routine di scrittura in EEPROM ha restituito notifica di errore alzo il flag di rest del micro
    }
    
    if( (IndiceDatoSalvato == IndiceDatoLetto) && (FlagBufferFull  == 1))	//se tx uguale a rx e buffer pieno => tx ha raggiunto rx e sovrascriver� un dato presente
    {
      IndiceDatoLetto = (IndiceDatoLetto + 1)%NUM_MAX_RECORD;	
    }
    IndiceDatoSalvato = (IndiceDatoSalvato + 1)%NUM_MAX_RECORD;
    
    if(IndiceDatoLetto == IndiceDatoSalvato)
    {
      FlagBufferFull = 1; //buffer pieno
    }
    //Dati.InputVariation = '0';	//pulisco il campo in che conteneva il MsgType ricevuto
    
    StatoGPS_GSM = GESTIONE_STRUTTURE_SALVATE;
    break;
    
  case GESTIONE_STRUTTURE_SALVATE:
    if(FlagErroreRete == 1)
    {
      FlagErroreRete = 0;
      StatoGPS_GSM = CHIUDI_RETE;//MODEM_ERROR;
      return;
    }	
    if(FlagPrimoInvioEffettuato)	//Se ho gi� effettuato un invio...
    {
      IndiceDatoLetto = (IndiceDatoLetto + 1)%NUM_MAX_RECORD;
      FlagBufferFull  = 0;
      FlagPrimoInvioEffettuato = 0;	
    }	
    if( ( IndiceDatoLetto != IndiceDatoSalvato ) || ( FlagBufferFull == 1 ) )	//invio se indici diversi oppure buffer pieno
    {		
      LetturaStrutturaDati(IndiceDatoLetto);
      if( ((IndiceDatoLetto + 1)%NUM_MAX_RECORD) !=  IndiceDatoSalvato)	//se ci sono pacchetti salvati e non trasmessi invio dopo aver letto da EEPROM
      {
        DatiLetti.Retrasmission = 1;
        GeneraStringaLetta();	
      }
      else //se � la prima trasmissione (dato corrente) invio il dato in ram
      {
        DatiLetti.Retrasmission = 0;
        GeneraStringa();	
        Dati.InputVariation = '0';	//pulisco il campo in che conteneva il MsgType eventualmente ricevuto
      }
      //GeneraStringaLetta();	
      StatoGPS_GSM = INVIA_MQTT;				//Compongo JSON		
    }								
    else
    {
      StatoGPS_GSM = FW_UPGRADE;
    }
    break;
    
  case INVIA_MQTT:
    if(modem_Trigger(TRG_INVIA_MQTT, DatoJson, DatoJsonDim) == MA_TRIG_OK)
    {
      FlagPrimoInvioEffettuato = 1;
      OperazioneModem = TRG_INVIA_MQTT;
      StatoGPS_GSM = GESTIONE_STRUTTURE_SALVATE;
    }
    break;
    
  case RICEVI_MQTT:	
    if(modem_Trigger(TRG_RICEVI_MQTT, NULL, NULL) == MA_TRIG_OK)
    {
      OperazioneModem = TRG_RICEVI_MQTT;
      StatoGPS_GSM = GET_MQTT;
    }
    
    break;
    
  case GET_MQTT:
    if(modem_GET_MsgMQTT(DatoJsonRx, &DatoJsonRxDim) == MA_OK)
    {
      GestioneDatoJsonRx();
      PulisciDatoJsonRx();
    }
    else
    {
      PulisciDatoJsonRx();
    }
    
    if(++retry_subscribe < MAX_RETRY_SUB)
    {
      StatoGPS_GSM = RICEVI_MQTT;				
    }
    else
    {
      retry_subscribe = 0;
      StatoGPS_GSM = GESTIONE_STRUTTURA;
    }
    
    break;
    
  case FW_UPGRADE:
    if(AggiornamentoFwPendente == 1) 
    {
      UpgradeData[0] = VersioneFWRX;
      UpgradeData[1] = SottoversioneFWRX;
      
      if(modem_Trigger(TRG_FW_UPDATE, &UpgradeData[0], 2)==MA_TRIG_OK) 
      {
        StatoGPS_GSM = GET_FW_UPGRADE;
        OperazioneModem = TRG_FW_UPDATE;
        AggiornamentoFwPendente = 0;
      }
    }
    else
    {
      StatoGPS_GSM = CHIUDI_RETE;//MODEM_OFF;
    }
    break;
    
  case GET_FW_UPGRADE:
    if(modem_GET_Status(TRG_FW_UPDATE) == MA_OK)
    {
      FlagUpgradeSuccess = 1;
    }
    else
    {
      FlagUpgradeSuccess = 0;
    }
    StatoGPS_GSM = CHIUDI_RETE;//MODEM_OFF;
    break;
    
    
  case CHIUDI_RETE:
    if(modem_Trigger(TRG_CHIUDI_RETE, NULL, NULL) == MA_TRIG_OK)
    {
      OperazioneModem = TRG_CHIUDI_RETE;
      StatoGPS_GSM = MODEM_OFF;
    }
    
    break;
    
  case MODEM_OFF:
    if(modem_Trigger(TRG_MODEM_OFF, NULL, NULL) == MA_TRIG_OK)
    {
      HAL_GPIO_WritePin(GPS_AMPLI_GPIO_Port, GPS_AMPLI_Pin, GPIO_PIN_RESET);	//Spegnimento Ampli GPS
      OperazioneModem = TRG_MODEM_OFF;
      StatoGPS_GSM = GO_TO_STOP_MODE;
    }
    
    break;
    
  case GO_TO_STOP_MODE:		
    //Scrittura chiave aggiornamento presente + reset uC
    Diagnostica.ContaStatoOut++;
    EEPROM_WriteByte( EEPROM_CONTA_STATO_OUT, Diagnostica.ContaStatoOut );
    if(FlagUpgradeSuccess)
    {
      EEPROM_WriteByte(EEPROM_ADD_CHIAVE, UPD_KEY_PRESENTE);
      EEPROM_WriteWord( EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter );
      EEPROM_WriteWord( EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter );
      HAL_NVIC_SystemReset();
      FlagUpgradeSuccess = 0;
    }
    //se ricevuto comando di reset da remoto oppure rilevato errore in EEPROM, eseguo reset
    if(FlagMicroReset)
    {
      FlagMicroReset = 0;
      EEPROM_WriteWord( EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter );
      EEPROM_WriteWord( EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter );
      
      
      Diagnostica.CounterEepromReset++;
      EEPROM_WriteWord( EEPROM_RESET_EEPROM, Diagnostica.CounterEepromReset );
      
      HAL_NVIC_SystemReset();
    }
    
    //Imposto futuro risveglio
    ImpostaRisveglio(Dati.SchedulingInterval);
    
    if(FactoryResetPendente)
    {	
      ResetParametriFabbrica();
      ResettaDiagnostica();
      DisattivaAlesea();
      NdefCompile(Dati.StatoAlesea);
      FactoryResetPendente = 0;
      Spegnimento();
    }
    if(DisattivazionePendente)
    {
      DisattivaAlesea();
      NdefCompile(Dati.StatoAlesea);
      DisattivazionePendente = 0;
      Spegnimento();
    }
    //**//verifico flag di risveglio dopodich� vado in stop mode
    
    FlagWakeupDaRTC = 0;
    FlagConsumoGSMOn = 0;
    FlagGetStatusOn = 0;
    Count_MA_UNP_ERROR = 0;
    StatoGPS_GSM = GET_TEMPERATURA;
    VerificaStatoCollaudo();
    //**//	
    
    break;
    
  case MODEM_ERROR:	
    
    modem_Trigger(TRG_MODEM_OFF, NULL, NULL);	//non sapendo se il modem stia funzionando correttamente do il comando senza alcuna verifica...sensato???
    HAL_GPIO_WritePin(GPS_AMPLI_GPIO_Port, GPS_AMPLI_Pin, GPIO_PIN_RESET);	//Spegnimento Ampli GPS
    FlagPrimoInvioEffettuato = 0;
    if(FlagErroreModemOn)
    {
      FlagErroreModemOn = 0;
      if(Diagnostica.CountErroreModemOn < 0xFF) 
      {
        Diagnostica.CountErroreModemOn++;
        EEPROM_WriteByte(EEPROM_CONTA_FAIL_MODEM_ON, Diagnostica.CountErroreModemOn);
      }
    }
    StatoGPS_GSM = GO_TO_STOP_MODE;
    
    break;
    
  default:
    
    break;
  }
}


void MonitorActivity(void)
{
  StatusModemMonitor = !HAL_GPIO_ReadPin(STATUS_MODEM_GPIO_Port, STATUS_MODEM_Pin);
  if(StatusModemMonitor)
  {
    Diagnostica.ModemActivityCounter++;
  }
  Diagnostica.MicroActivityCounter++;
}
