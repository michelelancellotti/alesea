#include "ndef.h"
#include "main.h"
#include "Alesea_ciclo_vita.h"
#include "nfc_SM.h"
#include "modem_SM.h"
#include "stm32l0xx_hal.h"
#include "Gestione_dato.h"
#include "LSM6DSL_SM.h"
#include "EEPROM.h"
#include "crc.h"
#include "Gestione_batteria.h"
#include "wwdg.h"
#include "Globals.h"

uint8_t WwdgCounter;


//Flag relativi a comandi da remoto
uint8_t FlagDisattivazione;
uint8_t FlagAttivazione;
uint8_t FlagInstantMessage;
uint8_t FlagFactoryReset;
uint8_t FlagSpinReset;
uint8_t FlagDiagnosticaReset;
uint8_t FlagShockReset;
uint8_t FlagFreeFallReset;
uint8_t FlagBatteryReset;
uint8_t FlagUpgradeFw;
uint8_t FlagSincronizzazione;
uint8_t FlagMicroReset;
uint8_t FlagNoRxMQTT;	//Flag per il bypass della fase di ricezione MQTT: usato quando viene dato cmd da app e viene forzato risveglio RTC
uint8_t FlagForceGpsOn;
uint8_t FlagSetSetGnssSatelliti;
uint32_t SchIntervalCounter;
uint32_t TempoDiRisveglioDef;


uint8_t CounterTxStartUp, CounterTxWarmUp, CounterTxCollaudo;
////////////////////////////////////////////////////////////////////////
/////////////////////////SET PROSSIMO RISVEGLIO/////////////////////////
////////////////////////////////////////////////////////////////////////
void ImpostaRisveglio(uint32_t period)
{
  TempoDiRisveglioDef = period;
}
