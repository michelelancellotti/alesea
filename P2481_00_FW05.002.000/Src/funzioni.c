#include "EEPROM.h"
#include "nfc_SM.h"
#include "communication_SM.h"
#include "modem_SM.h"
#include "wifi_SM.h"
#include "stm32l0xx_hal.h"
#include "LSM6DSL_SM.h"
#include "LSM6DSL.h"
#include "LIS2MDL.h"
#include "crc.h"
#include "Gestione_batteria.h"
#include "Gestione_dato.h"
#include "Alesea_memory.h"
#include "funzioni.h"
#include "wwdg.h"
#include "Globals.h"
#include "funzioni.h"
#include "i2c.h"
#include "gpio.h"
#include "lptim.h"
#include "usart.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>

uint32_t CRC_calcolato, CRC_letto;
uint8_t ContatoreAttivazione;
uint8_t StatoEEPROM;
uint32_t Supervisor;
uint8_t StatusModemMonitor;
uint8_t ContatoreDiRisvegliEnable;
uint8_t ContatoreAttesaCollaudo;

RTC_TimeTypeDef rtc_time_set, rtc_time_get;
RTC_DateTypeDef rtc_date_set, rtc_date_get;
uint32_t StopModeDuration;

uint8_t LSM6DSLFlagReinit;
uint8_t LSM6DSLCounterReinit;
uint16_t CountI2C1Reinit, CountI2C3Reinit;

void AttivaAlesea(void)
{
  //Ad ogni attivazione il periodo viene portato a PERIODO_START_UP per un numero fisso di tx per poi passare a PERIODO_ACTIVE
  SetStatoAlesea(ALESEA_START_UP);     //Dati.StatoAlesea = ALESEA_ACTIVE;
  FlagForceGpsOn = 1;  //forzo attivazione GPS per la prima comunicazione
  IndiceDatoSalvato = 0;	//azzero indice di struttura dati salvati
  IndiceDatoLetto = 0;		//azzero indice di struttura dati letti
  Dati.GNSS_data.GpsForcingCounter = 0;
  Diagnostica.ContaReset = 0;
  EEPROM_WriteByte( EEPROM_CONTA_RESET, (uint32_t)Diagnostica.ContaReset );
}


void ResetParametriFabbrica(void)
{
  Dati.NetworkPerformance.IndiceTrasmissione = 0;	//azzero n pacchetti trasmesso
  EEPROM_WriteWord(EEPROM_INDICE_TRASMISSIONE, Dati.NetworkPerformance.IndiceTrasmissione);
  
  Diagnostica.ContaReset = 0;
  EEPROM_WriteByte( EEPROM_CONTA_RESET, (uint32_t)Diagnostica.ContaReset );

  
  //todo: valutare quali altri parametri resettare
  Dati.GNSS_data.GpsPro = GPS_PRO_STATE_DEFAULT;
  EEPROM_WriteByte(EEPROM_GPS_PRO, Dati.GNSS_data.GpsPro);
  Dati.GNSS_data.GpsProTime = GPS_PRO_TIME_DEFAULT;
  EEPROM_WriteByte(EEPROM_GPS_PRO_TIME, Dati.GNSS_data.GpsProTime);
  Dati.GNSS_data.GpsFreqForcing = GPS_FREQ_FORCING_DEFAULT;
  EEPROM_WriteByte(EEPROM_GPS_FREQ_FORCING, Dati.GNSS_data.GpsFreqForcing);  
  //
  ResettaContagiri();
  SetVariabiliCriticheDefault();
  SetStatoAlesea(ALESEA_READY); 
}


void DisattivaAlesea(void)
{
  //Il dispositivo torna a READY e perde il periodo di schedulazione attuale
  SetStatoAlesea(ALESEA_READY); //Dati.StatoAlesea = ALESEA_READY;		//cambio stato a READY
  //CheckCRC();
}


void ResettaConsumo(void)
{
  Consumo_uA_h_tot = 0;
  EEPROM_WriteWord(EEPROM_CONSUMO_TOT, Consumo_uA_h_tot);	
}


void ResettaContagiri(void)
{
  NumeroGiriGyr = 0;
  Dati.SpinEvent.NumeroGiriCw = 0;
  Dati.SpinEvent.NumeroGiriACw = 0;
  EEPROM_WriteWord(EEPROM_NUMERO_GIRI_TOT_CW, (uint32_t)Dati.SpinEvent.NumeroGiriCw);	
  EEPROM_WriteWord(EEPROM_NUMERO_GIRI_TOT_ACW, (uint32_t)Dati.SpinEvent.NumeroGiriACw);	
}


void ResettaContaShock(void)
{
  Dati.ShockEvent.ContaShock = 0;
  EEPROM_WriteWord(EEPROM_CONTA_SHOCK, Dati.ShockEvent.ContaShock);	
}

void ResettaContaFreeFall(void)
{
  Dati.FreeFallEvent.ContaFreeFall = 0;
  EEPROM_WriteByte(EEPROM_CONTA_FREE_FALL, Dati.FreeFallEvent.ContaFreeFall);
}


void IncrementoIndiciTx(void)
{
  Dati.NetworkPerformance.IndiceTrasmissione++;
  EEPROM_WriteWord(EEPROM_INDICE_TRASMISSIONE, Dati.NetworkPerformance.IndiceTrasmissione);	
  
  if(Dati.StatoAlesea == ALESEA_COLLAUDO) CounterTxCollaudo++;
  else if(Dati.StatoAlesea == ALESEA_START_UP) CounterTxStartUp++;
  else if(Dati.StatoAlesea == ALESEA_WARM_UP) CounterTxWarmUp++;
}

void ResettaDiagnostica(void)
{  
  Diagnostica.ModemActivityCounter = 0;
  Diagnostica.MicroActivityCounter = 0;
  Diagnostica.ContaStatoIn = 0;
  Diagnostica.ContaStatoOut = 0;
  Diagnostica.CounterErrorEeprom = 0;
  Diagnostica.CountErroreModemOn = 0;
  Diagnostica.ContaReset = 0;
  Diagnostica.CountUnpError = 0;
  lsm6dsl_fail_count_dbg = 0;
 
  ResettaDiagnosticaSupervisore();
  
  EEPROM_WriteByte(EEPROM_CONTA_STATO_IN, Diagnostica.ContaStatoIn);
  EEPROM_WriteByte(EEPROM_CONTA_STATO_OUT, Diagnostica.ContaStatoOut);
  EEPROM_WriteWord(EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter);
  EEPROM_WriteWord(EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter);
  EEPROM_WriteByte(EEPROM_ERROR_EEPROM, Diagnostica.CounterErrorEeprom);
  EEPROM_WriteByte(EEPROM_CONTA_FAIL_MODEM_ON, Diagnostica.CountErroreModemOn);
  EEPROM_WriteByte(EEPROM_CONTA_RESET, Diagnostica.ContaReset);
  EEPROM_WriteByte(EEPROM_CONTA_UNP, Diagnostica.CountUnpError);
  
  LSM6DSLCounterReinit = 0;
  EEPROM_WriteByte(EEPROM_LSM6DSL_CONTA_REINIT, LSM6DSLCounterReinit);
}

void ResettaDiagnosticaSupervisore(void)
{
  Diagnostica.Byte1 = 0;
  Diagnostica.Byte2 = 0;
  Diagnostica.Byte3 = 0;
  Diagnostica.Byte4 = 0;
  Diagnostica.Byte5 = 0;
  Diagnostica.Byte6 = 0;
  Diagnostica.Byte7 = 0;
  Diagnostica.Byte8 = 0;
  
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_1, Diagnostica.Byte1);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_2, Diagnostica.Byte2);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_3, Diagnostica.Byte3);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_4, Diagnostica.Byte4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_5, Diagnostica.Byte5);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_6, Diagnostica.Byte6);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_7, Diagnostica.Byte7);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_8, Diagnostica.Byte8);
}


void GestioneTimeoutAttivazione(void)
{
  if( Dati.StatoAlesea == ALESEA_READY )
  {
    if(++ContatoreAttivazione >= TIMEOUT_ATTIVAZIONE_READY)
    {
      ContatoreAttivazione = 0;
      if(VersioneHW == VERSIONE_HW_3_X)
        Spegnimento();
      else
      {
        SetLSM6DSL(ACC_OFF);//spengo accelerometro
#ifndef DEBUG_ON
        EnterStopMode();      
#endif
      }
    }
  }
  else if(Dati.StatoAlesea == ALESEA_PRE_COLLAUDO)
  {
    if(++ContatoreAttivazione >= TIMEOUT_ATTIVAZIONE_PRECOLLAUDO)
    {
      ContatoreAttivazione = 0;
      if(VersioneHW == VERSIONE_HW_3_X)
        Spegnimento();
      else
      {
        SetLSM6DSL(ACC_OFF);//spengo accelerometro
#ifndef DEBUG_ON
        EnterStopMode();      
#endif
      }
    }
  }
  else
  {
    ContatoreAttivazione = 0;
  }
}



void Spegnimento(void)
{
  if(VersioneHW == VERSIONE_HW_3_X)
  {  
    EEPROM_WriteWord( EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter );
    EEPROM_WriteWord( EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter );
    #ifndef DEBUG_ON 
      SWITCH_OFF 
    #endif
  }
}



void GestioneStopMode(void)
{
  uint8_t StatusComunicazione = Get_Stato_Comunicazione();
  if(!FlagWakeupPerTemp && !FlagWakeupDaRTC && !FlagWakeupDaMEMS && !FlagWakeUpDaMovimento && !FlagWakeUpDaShock && !FlagWakeupDaNFC && (StatusComunicazione == COMM_IDLE_S) && (ContatoreAttesaCollaudo == 0) )
  {
    
    //gestione dell'integrale del consumo
    Consumo_uA_h_tot += GetConsumo();	
    EEPROM_WriteWord(EEPROM_CONSUMO_TOT, (int32_t)Consumo_uA_h_tot);
    Dati.AleseaMeasure.CaricaResidua = CalcolaCaricaResidua(Consumo_uA_h_tot);
    
    
    if( (Dati.StatoAlesea != ALESEA_READY) && (Dati.StatoAlesea != ALESEA_PRE_COLLAUDO))	
    {
#ifndef DEBUG_ON 
      EnterStopMode();
#else  
      if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, TempoDiRisveglioDef/HZ_RTC, RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK)
      {
        Error_Handler();
      } 	
#endif 
    }  
  }
}


void EnterStopMode(void)
{
  NdefCompile(Dati.StatoAlesea);
  
  EEPROM_WriteWord( EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter );
  EEPROM_WriteWord( EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter );
  
  HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_RESET);	//Disalimento TAG
  if(VersioneHW == VERSIONE_HW_3_X)
      HAL_GPIO_WritePin(GPS_AMPLI_HW3_GPIO_Port, GPS_AMPLI_HW3_Pin, GPIO_PIN_RESET);	//Spegnimento Ampli in ingresso fase GPS per HW3
  
  if(VersioneHW == VERSIONE_HW_4_X)
  {
    LIS2MDL_low_power();        
    //spegnimenti superflui, mantenuti per sicurezza
    HAL_GPIO_WritePin(MDM_ON_OFF_HW4_GPIO_Port, MDM_ON_OFF_HW4_Pin, GPIO_PIN_RESET);    //disalimento il modem
    HAL_GPIO_WritePin(BOOST_EN_HW4_GPIO_Port, BOOST_EN_HW4_Pin, GPIO_PIN_RESET);        //spengo lo switching 
    HAL_GPIO_WritePin(WIFI_EN_HW4_GPIO_Port, WIFI_EN_HW4_Pin, GPIO_PIN_RESET);          //EN wifi forzato basso
    HAL_GPIO_WritePin(WIFI_ON_OFF_HW4_GPIO_Port, WIFI_ON_OFF_HW4_Pin, GPIO_PIN_RESET);  //switch wifi forzato spento
    HAL_GPIO_WritePin(GPS_AMPLI_HW4_GPIO_Port, GPS_AMPLI_HW4_Pin, GPIO_PIN_RESET);	//spegnimento Ampli GPS    
  }
  HAL_GPIO_WritePin(LED1_HW_4_GPIO_Port, LED1_HW_4_Pin, GPIO_PIN_RESET);                //led di segnalazione OFF (indipendentemente dalla versione HW: su HW vecchio il pin era floating)
  
  SetLSM6DSL(GYR_OFF);
  
  
  Supervisor = 0;
  //HAL_SuspendTick();  //da attivare se necessario fare debug in stop mode. (per debug in stop mode abilitare DBG_STOP in DBG_CR. Il registro si pulisce solo disalimentando)
  ContatoreDiRisvegliEnable = 1;

 //Imposto il tempo di risveglio
  if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, TempoDiRisveglioDef/HZ_RTC, RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK)
  {
    Error_Handler();
  }
  /////////////gestione reset accelerometro//////////////
  if(LSM6DSLFlagReinit == 1)
  {
    LSM6DSLFlagReinit = 0;
    if(LSM6DSLCounterReinit < 255) 
    {
      LSM6DSLCounterReinit++;
      EEPROM_WriteByte(EEPROM_LSM6DSL_CONTA_REINIT, LSM6DSLCounterReinit);
    }
    
    /* Rimosso init accelerometro (22/02/2024):
        - era stato aggiunto per problematica su controllo giri (possibili cause: blocco I2C o blocco accelerometro)
        - se I2C va in errore, l'esecuzione della funzione � dilatata nel tempo e causa intervento WWDG
        - se WWDG interviene in stato COLLAUDO, il collaudo si interrompe
        - se WWDG interviene in stato ACTIVE, il pezzo resetta e comunica di nuovo
    */
    /*
    InitLSM6DSL();
    
    for(uint8_t i = 0; i < 10; i++)
    {
      RefreshWD();  
      HAL_Delay(500);
    }
    
    RefreshWD();
    CleanFlagInterrupt();
    */
  }
  ///////////////////////////////////////////////////////
  
  /////////////gestione deinit I2C (vedi errata STM32L0 par. 2.12.2//////////////
  HAL_I2C_DeInit(&hi2c1);
  if(VersioneHW == VERSIONE_HW_4_X)
    HAL_I2C_DeInit(&hi2c3);
  ///////////////////////////////////////////////////////////////////////////////
  
  
  
  /////////////ingresso in STOP mode//////////////
  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);                                          
  HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);         

  //HAL_ResumeTick();  //da attivare se necessario fare debug in stop mode.
  ////////////////////////////////////////////////
  
  SystemClock_Config();
  //Stabilizzazione clock (1s)
  for(uint8_t i = 0; i < 10; i++)
  {
    RefreshWD();  
    HAL_Delay(100);
  }
  
  HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);       //disattivazione del risveglio temporizzato (in precedenza, non disattivando si aveva un risveglio ciclico non desiderato)
  
  if(VersioneHW == VERSIONE_HW_4_X)
  {
    MX_GPIO_HW4_Init(); 
    MX_I2C3_Init();
  }
  else /*if(VersioneHW == VERSIONE_HW_DEFAULT_BG96)*/
  {
    MX_GPIO_HW3_Init(); 
  }

  MX_I2C1_Init();
  MX_USART1_UART_Init();
  
  SetLSM6DSL(ACC_LOW_POWER);//LSM6DSL_ACC_ON();        
  //fino a v6.5 era presente SetInterruptAngolo()
}

/* Calcola la durata della fase di STOP trascorsa ed aggiorna lo unix-time. */
uint32_t last_ut = 0;
void GetStopModeDuration(void)
{
  
  
  if(Dati.StatoAlesea != ALESEA_PRE_COLLAUDO && Dati.StatoAlesea != ALESEA_READY)
  {
    //Calcolo la durata della fase di STOP appena trascorsa
    HAL_RTC_GetTime(&hrtc, &rtc_time_get, RTC_FORMAT_BIN); 
    HAL_RTC_GetDate(&hrtc, &rtc_date_get, RTC_FORMAT_BIN); 
    StopModeDuration = (rtc_time_get.Seconds + (60*rtc_time_get.Minutes) + (3600*rtc_time_get.Hours))*10;
    
    if(modem_GET_UnixTime(&last_ut) == MA_OK)
    {
      //La comunicazione fatta prima di andare in STOP aveva una data valida:
      //--> aggiorno lo unix-time (sincronizzazione per evitare derive)
      Dati.AleseaDate.UnixTime = last_ut + StopModeDuration;
    }
    else if(Dati.AleseaDate.UnixTime != 0)
    {
      //La comunicazione fatta prima di andare in STOP NON aveva una data valida:
      //--> incremento lo unix-time precedente
      Dati.AleseaDate.UnixTime += StopModeDuration;
    }
    //else ... unix-time nullo (dopo POR)
  }
}

  
void ResetCalendar(void)
{
  rtc_time_set.Hours = 0;
  rtc_time_set.Minutes = 0;
  rtc_time_set.Seconds = 0;
  HAL_RTC_SetTime(&hrtc, &rtc_time_set, RTC_FORMAT_BIN);
  rtc_date_set.Date = 1;
  rtc_date_set.Month = 1;
  rtc_date_set.WeekDay = 1;
  rtc_date_set.Year = 1;
  HAL_RTC_SetDate(&hrtc, &rtc_date_set, RTC_FORMAT_BIN);
}

void GestioneSupervisore(void)
{
  if(Supervisor < ( TIMEOUT_SUPERVISOR + (Dati.GNSS_data.GpsTimeout) ) ) Supervisor++;
  else
  {
    //Intervento supervisore
    EEPROM_WriteWord( EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter );
    EEPROM_WriteWord( EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter );
    GestioneDiagnosticaSupervisore();	//salvataggio snapshot diagnostica
    HAL_NVIC_SystemReset();
  }
}


/* Salvataggio snapshot delle cause che rimandano l'ingresso del uC in STOP mode. */
void GestioneDiagnosticaSupervisore(void)
{
  uint8_t val_0, val_1;
  
  Diagnostica.Byte1 = ((StatoNFC&0x0F)<<4) | (StatoMEMS&0x0F);
  
  val_0 = Get_Stato_Comunicazione();
  Diagnostica.Byte2 = ((Dati.StatoAlesea&0x0F)<<4) | (val_0&0x0F);
  
  Diagnostica.Byte3 = Get_Stato_Modem_Appli();
  
  modem_GET_Diagnostic(M_DGN_STATO_AUTOMA, &val_0);  
  modem_GET_Diagnostic(M_DNG_PWR_STATUS, &val_1);
  Diagnostica.Byte4 = ((val_1&0x03)<<6) | (val_0&0x3F);
  
  val_0 = Get_Stato_WiFi_Appli();
  wifi_GET_ExtDiagnostic(W_DGN_STATO_AUTOMA, &val_1);
  Diagnostica.Byte5 = ((val_1&0x0F)<<4) | (val_0&0x0F);
  
  if(Diagnostica.Byte6 < 255)
    Diagnostica.Byte6++;        //contatore interventi supervisore
  
  Diagnostica.Byte7 = 0;
  Diagnostica.Byte7 = (0x01 & FlagWakeupDaRTC);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeupDaMEMS) << 1);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeupDaNFC) << 2);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeUpDaShock) << 3);
  Diagnostica.Byte7 |= ((0x01 & ON_communication) << 4);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeupPerTemp) << 5);
  Diagnostica.Byte7 |= ((0x01 & FlagWakeUpDaMovimento) << 6);
  
  Diagnostica.Byte8 = ContatoreAttesaCollaudo;
  
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_1, Diagnostica.Byte1);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_2, Diagnostica.Byte2);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_3, Diagnostica.Byte3);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_4, Diagnostica.Byte4);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_5, Diagnostica.Byte5);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_6, Diagnostica.Byte6);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_7, Diagnostica.Byte7);
  EEPROM_WriteByte(EEPROM_DIAGNOSTICA_8, Diagnostica.Byte8);
}




void RunFactoryReset(void)
{
  FlagFactoryReset = 1;
}

void RunMicroReset(void)
{
  FlagMicroReset = 1;
}

void RunSpinReset(void)
{
  FlagSpinReset = 1;
}

void RunShockReset(void)
{
  FlagShockReset = 1;
}

void RunFreeFallReset(void)
{
  FlagFreeFallReset = 1;
}

void RunDiagnosticaReset(void)
{
  FlagDiagnosticaReset = 1;
}

void RunBatteryReset(void)
{
  FlagBatteryReset = 1;
}
  
void RunDeactivation(void)
{
  FlagDisattivazione = 1;
}
  
void SetGpsSetup(uint32_t var_32)
{
  if( (var_32 >= GPS_TIMEOUT_MIN) && (var_32 <= GPS_TIMEOUT_MAX) ) 	
  {
    Dati.GNSS_data.GpsTimeout = var_32;
    EEPROM_WriteByte(EEPROM_STATO_GPS_TIMEOUT, Dati.GNSS_data.GpsTimeout);
    SalvaVariabileCritica(&GpsTimeoutVc, BANCO_1_2);
  }
}

void SetGpsPro(uint8_t var_8)
{
  if( (var_8 == 0) || (var_8 == 1) )
  {
    Dati.GNSS_data.GpsPro = var_8;
    EEPROM_WriteByte(EEPROM_GPS_PRO, Dati.GNSS_data.GpsPro);
  }
}

void SetGpsProTime(uint8_t var_8)
{
  if( (var_8 <= GPS_PRO_TIME_MAX) && (var_8 >= GPS_PRO_TIME_MIN) ) 	
  {
    Dati.GNSS_data.GpsProTime = var_8;
    EEPROM_WriteByte(EEPROM_GPS_PRO_TIME, Dati.GNSS_data.GpsProTime);
  }
}

void SetProfiloTemperatura(uint8_t var_8)
{
  if( var_8 == 0x30 )		//se � un numero tra 0 e 1
  {
    Dati.AleseaMeasure.ProfiloTemperaturaOn = PROFILO_TEMPERATURA_OFF;
    EEPROM_WriteByte(EEPROM_PROFILO_TEMPERATURA_ON, Dati.AleseaMeasure.ProfiloTemperaturaOn);
  }
  else if( var_8 == 0x31 )
  {
    Dati.AleseaMeasure.ProfiloTemperaturaOn = PROFILO_TEMPERATURA_ON;
    EEPROM_WriteByte(EEPROM_PROFILO_TEMPERATURA_ON, Dati.AleseaMeasure.ProfiloTemperaturaOn);
  }
}

void SetWiFiScanEnable(uint8_t var_8)
{
  if( var_8 == 0x30 )		//se � un numero tra 0 e 1
  {
    WiFiParam.WiFiScanEnable = WIFI_ENABLE_OFF;
    EEPROM_WriteByte(EEPROM_WIFI_ENABLE, WiFiParam.WiFiScanEnable);
  }
  else if( var_8 == 0x31 )
  {
    WiFiParam.WiFiScanEnable = WIFI_ENABLE_ON;
    EEPROM_WriteByte(EEPROM_WIFI_ENABLE, WiFiParam.WiFiScanEnable);
  }
}

void SetWiFiComEnable(uint8_t var_8)
{
  if( var_8 == 0x30 )		//se � un numero tra 0 e 1
  {
    WiFiParam.WiFiComEnable = WIFI_ENABLE_OFF;
    EEPROM_WriteByte(EEPROM_WIFI_COM_ENABLE, WiFiParam.WiFiComEnable);
  }
  else if( var_8 == 0x31 )
  {
    WiFiParam.WiFiComEnable = WIFI_ENABLE_ON;
    EEPROM_WriteByte(EEPROM_WIFI_COM_ENABLE, WiFiParam.WiFiComEnable);
  }
}

void SetNumberTxSwitchModem(uint8_t val)
{ 
  if((val >= WIFI_NUM_TX_SWITCH_MODEM_MIN) && (val <= WIFI_NUM_TX_SWITCH_MODEM_MAX))
  {
    WiFiParam.WiFiNumTxSwitchModem = val;
    EEPROM_WriteByte(EEPROM_WIFI_NUM_TX_SWITCH_MODEM, WiFiParam.WiFiNumTxSwitchModem);
    
    NumberTxWiFi = 0;
  }
  //else  ....lascio l'attuale
}

void SetRotazioniOrizzontali(uint8_t var_8)
{
  if( var_8 == 0x30 )		//se � un numero tra 0 e 1
  {
    Dati.SpinEvent.RotazioneOrizzontaleOn = ROTAZIONE_ORIZZ_OFF;
    EEPROM_WriteByte(EEPROM_ROTAZIONE_ORIZZ, Dati.SpinEvent.RotazioneOrizzontaleOn);
  }
  else if( var_8 == 0x31 )
  {
    Dati.SpinEvent.RotazioneOrizzontaleOn = ROTAZIONE_ORIZZ_ON;
    EEPROM_WriteByte(EEPROM_ROTAZIONE_ORIZZ, Dati.SpinEvent.RotazioneOrizzontaleOn);
  }
}

void SetGpsFreqForcing(uint8_t var_8)
{
  if( (var_8 >= GPS_FREQ_FORCING_MIN) && (var_8 <= GPS_FREQ_FORCING_MAX) )
  {
    Dati.GNSS_data.GpsFreqForcing = var_8;
    EEPROM_WriteByte(EEPROM_GPS_FREQ_FORCING, Dati.GNSS_data.GpsFreqForcing);
  }
}

void SetUpgradeFw(uint8_t version, uint8_t subversion, uint8_t type)
{
  uint16_t versione_idx=0, new_versione_idx=0;
  uint8_t buff_versione[2] = {
    SottoversioneFW,    //[0]
    VersioneFW,         //[1]
  };
  
  FlagUpgradeFw = 0;

  UpgradeDataRX.FIELD.version = version;
  UpgradeDataRX.FIELD.subversion = subversion;
  UpgradeDataRX.FIELD.type = type;
  memcpy((uint8_t*)&new_versione_idx, UpgradeDataRX.BUFFER, sizeof(new_versione_idx));          //indice versione FW a cui aggiornare = numero costruito in base 256
  
  switch(UpgradeDataRX.FIELD.type)
  {
    case UPG_USER:
      //Agg. FW USER
      memcpy((uint8_t*)&versione_idx, buff_versione, sizeof(versione_idx));     //indice versione FW (USER) corrente = numero costruito in base 256
      if(new_versione_idx > versione_idx)
      {
        UpgradeFwCount++;
        FlagUpgradeFw = 1;
      }
      break;
    default: break;
  }
}
 
void MonitorActivity(void)
{
  StatusModemMonitor = !HAL_GPIO_ReadPin(STATUS_MODEM_GPIO_Port, STATUS_MODEM_Pin);
  if(StatusModemMonitor)
  {
    Diagnostica.ModemActivityCounter++;
  }
  Diagnostica.MicroActivityCounter++;
}

void SetSchedulingInterval(uint32_t val)
{
  if(val <= LIMITE_SCHEDULING_INTERVAL)
  {
     Dati.ActivationTiming.SchedulingInterval = val;
  }
}

void SetInterval_ACTIVE(uint32_t val)
{
  if(val <= LIMITE_SCHEDULING_INTERVAL)	
  { 
    Dati.ActivationTiming.SchInt_ACTIVE = val;
    SalvaVariabileCritica(&SchIntActiveVc, BANCO_1_2);
    RefreshSchedulingInterval();
  }
}

void SetInterval_START_UP(uint32_t val)
{
  if(val <= LIMITE_SCHEDULING_INTERVAL)	
  {
    Dati.ActivationTiming.SchInt_START_UP = val;
    SalvaVariabileCritica(&SchIntStartUpVc, BANCO_1_2);
    RefreshSchedulingInterval();
  }
}

void SetInterval_WARM_UP(uint32_t val)
{
  if(val <= LIMITE_SCHEDULING_INTERVAL)	
  {
    Dati.ActivationTiming.SchInt_WARM_UP = val;
    SalvaVariabileCritica(&SchIntWarmUpVc, BANCO_1_2);
    RefreshSchedulingInterval();
  }
}

void SetMoveSchInterval(uint32_t val)
{
  if( (val >= MOVE_SCH_INTERVAL_MIN) && (val <= MOVE_SCH_INTERVAL_MAX) )
  {
    Dati.MovementEvent.MoveSchInterval = val;
    EEPROM_WriteWord(EEPROM_MOVE_SCH_INTERVAL, Dati.MovementEvent.MoveSchInterval);
  }
}

void SetSpinSchInterval(uint32_t val)
{
  if( (val >= SPIN_SCH_INTERVAL_MIN) && (val <= SPIN_SCH_INTERVAL_MAX) )
  {
    Dati.SpinEvent.SpinSchInterval = val;
    EEPROM_WriteWord(EEPROM_SPIN_SCH_INTERVAL, Dati.SpinEvent.SpinSchInterval);
  }
}

void SetShockSchInterval(uint32_t val)
{
  if( (val >= SHOCK_SCH_INTERVAL_MIN) && (val <= SHOCK_SCH_INTERVAL_MAX) )
  {
    Dati.ShockEvent.ShockSchInterval = val;
    EEPROM_WriteWord(EEPROM_SHOCK_SCH_INTERVAL, Dati.ShockEvent.ShockSchInterval);
  }
}

void SetNumberTx_START_UP(uint32_t val)
{
  if( (val >= NUMERO_TX_START_UP_MIN) && (val <= NUMERO_TX_START_UP_MAX) )
  {
    Dati.ActivationTiming.NumberTx_START_UP = val;
    SalvaVariabileCritica(&NumberTxStartUpVc, BANCO_1_2);
  }
}

void SetNumberTx_WARM_UP(uint32_t val)
{
  if( (val >= NUMERO_TX_WARM_UP_MIN) && (val <= NUMERO_TX_WARM_UP_MAX) )
  {
    Dati.ActivationTiming.NumberTx_WARM_UP = val;
    SalvaVariabileCritica(&NumberTxWarmUpVc, BANCO_1_2);
  } 
}

void SetSogliaMovement(uint8_t val)
{
  
  if( (val >= SOGLIA_MOV_MIN) && (val <= SOGLIA_MOV_MAX) )
  {
    Dati.MovementEvent.SogliaMovimento = val;
    EEPROM_WriteByte(EEPROM_SOGLIA_MOV, Dati.MovementEvent.SogliaMovimento);
  }    
}

void SetSogliaShock(uint8_t val)
{
  if( (val >= SOGLIA_SHOCK_MIN) && (val <= SOGLIA_SHOCK_MAX ) )
  {
    Dati.ShockEvent.SogliaShock = val;
    EEPROM_WriteByte(EEPROM_SOGLIA_SHOCK, Dati.ShockEvent.SogliaShock);
  } 
}

void SetGnssSatelliti(uint32_t val)
{
  Dati.GNSS_data.GnssSatelliti = val;
  FlagSetSetGnssSatelliti = 1;
  SalvaVariabileCritica(&GnssSatellitiVc, BANCO_1_2);
}

void SetTimeoutSpin(uint8_t val)
{
  
  //TO DO inserire vincoli
  if( (val >= FILTRO_QUADRANTE_FERMO_MIN) && (val <= FILTRO_QUADRANTE_FERMO_MAX) )
  {
    Dati.MovementEvent.FiltroMovimento = val;
    EEPROM_WriteByte(EEPROM_FILTRO_QUADRANTE_FERMO, Dati.MovementEvent.FiltroMovimento);
  }
}

void SetGpsMode(uint8_t val)
{
  if( (val == GPS_MODE_ON) || (val == GPS_MODE_OFF) || (val == GPS_MODE_OFF_TILL_MOVE) ) 
  {
    Dati.GNSS_data.GpsMode = val;
    EEPROM_WriteByte(EEPROM_GPS_MODE, Dati.GNSS_data.GpsMode);
  }

}

void SetNumberTxIot(uint8_t val)
{ 
  if((val >= NUMBER_TX_IOT_MIN) && (val <= NUMBER_TX_IOT_MAX))
  {
    NumberTxIot = val;
    EEPROM_WriteByte(EEPROM_NUMBER_TX_IOT, NumberTxIot);
    
    CounterTxIot = 0;
  }
  //else  ....lascio l'attuale
}


void SetVariabiliCriticheDefault(void)
{
  
  *StatoAleseaVc.var32 = (uint8_t)StatoAleseaVc.defaultVal;
  SalvaVariabileCritica(&StatoAleseaVc, BANCO_1_2);
  
  *SchIntStartUpVc.var32 = SchIntStartUpVc.defaultVal;
  SalvaVariabileCritica(&SchIntStartUpVc, BANCO_1_2);
  
  *SchIntWarmUpVc.var32 = SchIntWarmUpVc.defaultVal;
  SalvaVariabileCritica(&SchIntWarmUpVc, BANCO_1_2);
  
  *SchIntActiveVc.var32 = SchIntActiveVc.defaultVal;
  SalvaVariabileCritica(&SchIntActiveVc, BANCO_1_2);
 
  *NumberTxStartUpVc.var32 = NumberTxStartUpVc.defaultVal;
  SalvaVariabileCritica(&NumberTxStartUpVc, BANCO_1_2);
  
  *NumberTxWarmUpVc.var32 = NumberTxWarmUpVc.defaultVal;
  SalvaVariabileCritica(&NumberTxWarmUpVc, BANCO_1_2);
  
  *GpsTimeoutVc.var32 = GpsTimeoutVc.defaultVal;
  SalvaVariabileCritica(&GpsTimeoutVc, BANCO_1_2);

  *GnssSatellitiVc.var32 = GnssSatellitiVc.defaultVal;
  SalvaVariabileCritica(&GnssSatellitiVc, BANCO_1_2); 
  
  EEPROM_WriteByte(CHIAVE_VARIABILE_CRITICA_1, CHIAVE_VARIABILE_CRITICA);
  EEPROM_WriteByte(CHIAVE_VARIABILE_CRITICA_2, CHIAVE_VARIABILE_CRITICA);
  EEPROM_WriteByte(CHIAVE_VARIABILE_CRITICA_3, CHIAVE_VARIABILE_CRITICA);
}

void AggiornaModemType(void)
{
   modem_GET_Type(&Dati.TipoModem);
   EEPROM_WriteByte(EEPROM_TIPO_MODEM, Dati.TipoModem);
}

void SetStatoAlesea(/*StatoAlesea_t*/uint32_t stato)
{
  switch(stato)
  {
    case ALESEA_START_UP:
      Dati.StatoAlesea = stato;
      CounterTxStartUp = 0;
    break;
    
    case ALESEA_WARM_UP:
      Dati.StatoAlesea = stato;
      CounterTxWarmUp = 0;
    break;
  
    case ALESEA_ACTIVE:
      Dati.StatoAlesea = stato; 
    break;
      
    case ALESEA_READY:
      Dati.StatoAlesea = stato;
    break;
    
    case ALESEA_PRE_COLLAUDO:
      Dati.StatoAlesea = stato;
    break;
    
    case ALESEA_COLLAUDO:
      Dati.StatoAlesea = stato;
    break;
        
    default:
      Dati.StatoAlesea = ALESEA_ACTIVE;
      NdefCompile(Dati.StatoAlesea); 
    break;
  } 
  NdefCompile(Dati.StatoAlesea);
  SalvaVariabileCritica(&StatoAleseaVc, BANCO_1_2);
  RefreshSchedulingInterval();
  
}


void RefreshSchedulingInterval(void)
{
  switch(Dati.StatoAlesea)
  {
    case ALESEA_START_UP:
      SetSchedulingInterval(Dati.ActivationTiming.SchInt_START_UP);
    break;
    
    case ALESEA_WARM_UP:
      SetSchedulingInterval(Dati.ActivationTiming.SchInt_WARM_UP);
    break;
  
    case ALESEA_ACTIVE:
      SetSchedulingInterval(Dati.ActivationTiming.SchInt_ACTIVE);   
    break;
      
    case ALESEA_READY:
      SetSchedulingInterval(LIMITE_SCHEDULING_INTERVAL);
      ImpostaRisveglio(Dati.ActivationTiming.SchedulingInterval);
    break;
    
    case ALESEA_PRE_COLLAUDO:
      SetSchedulingInterval(LIMITE_SCHEDULING_INTERVAL);
    break;
    
    case ALESEA_COLLAUDO: 
      SetSchedulingInterval(PERIODO_COLLAUDO);
    break;
        
    default:
      
    break;
  }
}

void SetTxInterruptSource(void)
{
  Dati.NetworkPerformance.TxInterruptSource = FlagWakeupDaRTC*PESO_SCHEDULE + FlagCommOnInSpinSession*PESO_SPIN + FlagCommOnInShockSession*PESO_SHOCK + FlagCommOnInMovementSession*PESO_MOVE + FlagCommOnInSpinSessionError*PESO_SPIN_ERROR;
}


uint32_t EstraiNumeroDaStringa(uint8_t *buffer, uint32_t indice, uint8_t cifre)
{
  uint32_t result = 0;
  for(uint8_t i=0; i<cifre; i++)
  {
    indice++;
    if( (buffer[indice] >= 0x30) && (buffer[indice] <= 0x39) )		//se � un numero....
    {
      result = (result*10) + (buffer[indice] - 0x30);
    }
    else break;
  }
  return result;
}

void RefreshWD(void)
{
#ifndef DEBUG_ON
  WwdgCounter = (WWDG->CR & 0x7F);
  if((WwdgCounter < 127) && (WwdgCounter > 70))
  {
    HAL_WWDG_Refresh(&hwwdg);
  }
#endif
}

uint8_t StatoLed;
void GestioneLed()
{
  if(ContatoreAttesaCollaudo > 0)
  {
    if(!StatoLed)
    {
      HAL_GPIO_WritePin(LED1_HW_4_GPIO_Port, LED1_HW_4_Pin, GPIO_PIN_SET);  //led di segnalazione ON 
      StatoLed = 1;
    }
    else
    {
      HAL_GPIO_WritePin(LED1_HW_4_GPIO_Port, LED1_HW_4_Pin, GPIO_PIN_RESET);  //led di segnalazione OFF 
      StatoLed = 0;
    }
  }
  else
  {
    HAL_GPIO_WritePin(LED1_HW_4_GPIO_Port, LED1_HW_4_Pin, GPIO_PIN_SET);  //led di segnalazione ON
  }
}