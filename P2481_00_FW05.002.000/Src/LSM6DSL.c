#include "LSM6DSL.h"
#include "main.h"
#include "Gestione_dato.h"
#include "i2c.h"
#include "EEPROM.h"
#include "lis2mdl_SM.h"
#include "funzioni.h"

/* USER CODE BEGIN PM */
static axis3bit16_t data_raw_acceleration;
static axis3bit16_t data_raw_angular_rate;
static axis1bit16_t data_raw_temperature;
float acceleration_mg[3];
float angular_rate_mdps[3];
float acceleration_mg_old[3];
float angular_rate_mdps_old[3];
static uint8_t whoamI, rst;
float NumeroGiriGyr;
uint8_t count_init_LSM, count_res_LSM;

uint32_t LSM6DSL_interrupt_timer;
uint32_t lsm6dsl_angular_fail_count;
uint32_t lsm6dsl_acceleration_fail_count;
uint32_t lsm6dsl_angular_busy_count;
uint32_t lsm6dsl_acceleration_busy_count;
uint32_t lsm6dsl_orientation_fail_count;
uint32_t lsm6dsl_orientation_busy_count;
uint32_t lsm6dsl_temperature_fail_count;
uint16_t lsm6dsl_fail_count_dbg;
float soglia_ratio = 0.5;
float Derivata_cambio_segno;
uint16_t finestra_osservazione_cambio_segno;
uint16_t CounterAngularRateBlocked;
uint16_t CounterAccelerationBlocked;
uint8_t TimerAccelerationBlocked;
uint8_t TimerAngularRateBlocked;
uint8_t flag_lsm6dsl_ignore_int_1, flag_lsm6dsl_ignore_int_2;



int8_t angular_rate_segno, angular_rate_segno_old;
float timer_angular_rate_segno_pos, timer_angular_rate_segno_neg, ratio_timer_angular_rate_segno;
uint32_t Conta_inversione_segno;

uint16_t CountRegisterWRFailure;
uint16_t RegisterWRFailureMap;

uint8_t ff_duration;
lsm6dsl_ff_ths_t ff_soglia;

lsm6dsl_odr_xl_t x_data_rate;
lsm6dsl_odr_g_t y_data_rate;
lsm6dsl_fs_xl_t x_full_scale;
lsm6dsl_fs_g_t y_full_scale;
lsm6dsl_g_hm_mode_t y_power_mode;
lsm6dsl_xl_hm_mode_t x_power_mode;


uint8_t SogliaAccelerometro;

static int32_t platform_write(void *handle, uint8_t Reg, uint8_t *Bufp,
                              uint16_t len)
{
  HAL_StatusTypeDef result;
  if (handle == &hi2c1)
  {
    result = HAL_I2C_Mem_Write(handle, LSM6DSL_I2C_ADD_L, Reg,
                               I2C_MEMADD_SIZE_8BIT, Bufp, len, 100/*1000*/);
  }
  return result;
}

static int32_t platform_read(void *handle, uint8_t Reg, uint8_t *Bufp,
                             uint16_t len)
{
  HAL_StatusTypeDef result;
  if (handle == &hi2c1)
  {
    result = HAL_I2C_Mem_Read(handle, LSM6DSL_I2C_ADD_L, Reg,
                              I2C_MEMADD_SIZE_8BIT, Bufp, len, 100/*1000*/);
  } 
  return result;
}

/*
 *  Function to print messages
 */
void tx_com( uint8_t *tx_buffer, uint16_t len )
{
  #ifdef NUCLEO_STM32F411RE  
  HAL_UART_Transmit( &huart2, tx_buffer, len, 1000 );
  #endif
  #ifdef MKI109V2  
  CDC_Transmit_FS( tx_buffer, len );
  #endif
}

void LS6DSL_CaricaSetup(void)
{
  x_data_rate = LSM6DSL_XL_ODR_12Hz5;
  y_data_rate = LSM6DSL_GY_ODR_OFF;
  x_power_mode = LSM6DSL_XL_NORMAL;
  y_power_mode = LSM6DSL_GY_NORMAL;
  x_full_scale = LSM6DSL_4g;
  y_full_scale = LSM6DSL_2000dps;
  SogliaAccelerometro = SOGLIA_MOV_DEFAULT;
}

void InitLSM6DSL(void)
{
  /*
  *  Initialize mems driver interface
  */
  
  /*
  *  Check device ID
  */
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;

  do {
    lsm6dsl_device_id_get(&dev_ctx, &whoamI);
    count_init_LSM++;
    HAL_Delay(10);
  } while ( (whoamI != LSM6DSL_ID) && (count_init_LSM < 10) );
  count_init_LSM = 0;
  whoamI = 0;

  
  lsm6dsl_reset_set(&dev_ctx, PROPERTY_ENABLE);
  do {
    lsm6dsl_reset_get(&dev_ctx, &rst);
    count_res_LSM++;
    HAL_Delay(10);
  } while (rst && (count_res_LSM < 10) );
  count_res_LSM = 0;
  rst = 0;
  /*
  *  Enable Block Data Update
  */
  SetLSM6DSL(BDU);//lsm6dsl_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);
  
  //Abilitate pull-up interne su I2C MASTER
  SetLSM6DSL(PULL_UP);//lsm6dsl_sh_pin_mode_set(&dev_ctx,LSM6DSL_INTERNAL_PULL_UP); 
       
  //
  
  //enable low power settato con freq
  //lsm6dsl_xl_power_mode_set(&dev_ctx, x_power_mode);
  //lsm6dsl_gy_power_mode_set(&dev_ctx, y_power_mode);
  
  /*
  * Set Output Data Rate
  */
  
    
  SetLSM6DSL(ACC_LOW_POWER);//lsm6dsl_xl_data_rate_set(&dev_ctx, x_data_rate);
  SetLSM6DSL(GYR_OFF);//lsm6dsl_gy_data_rate_set(&dev_ctx, y_data_rate);
  /*
  * Set full scale
  */ 
  SetLSM6DSL(ACC_FS_4g);//lsm6dsl_xl_full_scale_set(&dev_ctx, x_full_scale);
  SetLSM6DSL(GYR_FS_2000dps);//lsm6dsl_gy_full_scale_set(&dev_ctx, y_full_scale);
  
  
  SetLSM6DSL(INT_1);//SetInterrupt1();
  SetLSM6DSL(INT_2);//SetInterrupt2();
  HAL_Delay(100);
  SetLSM6DSL(ACC_INTERRUPT_ANGLE);//SetInterruptAngolo();
  SetLSM6DSL(ACC_SOGLIA);//SogliaAccelerometroSet(SogliaAccelerometro);
  
  
  SetLSM6DSL(ACC_FILTER);
  /*
  * Configure filtering chain(No aux interface)
  */  
  /* Accelerometer - analog filter */
  //lsm6dsl_xl_filter_analog_set(&dev_ctx, LSM6DSL_XL_ANA_BW_1k5Hz);

  /* Gyroscope - filtering chain */
  //lsm6dsl_gy_band_pass_set(&dev_ctx, LSM6DSL_HP_DISABLE_LP1_LIGHT/*LSM6DSL_HP_260mHz_LP1_STRONG*/);
  
  /*
  * Read samples in polling mode (no int)
  */
}



uint8_t CheckI2CReinit(uint8_t tipo)
{
  uint8_t tipo_app;
  if(FlagReinit_I2C_Acc)
  {
    MX_I2C1_Init();
    CountI2C1Reinit++;
    if(tipo < 16)    tipo_app = tipo;
    else tipo_app = 15;
    RegisterWRFailureMap |= (1<<tipo_app);
    FlagReinit_I2C_Acc = 0;
    return 0;
  }
  else
    return 1;
  
}


void SetLSM6DSL(uint8_t tipo)
{
  uint8_t res = 0;
  for(uint8_t i = 0; i < 3; i++)
  {
    SetLSM6DSL_ciclo(tipo);
    if(CheckI2CReinit(tipo) == 1) 
    {
      res = 1;
      break;
    }
  }
  if(res == 0)  CountRegisterWRFailure++;
}

void SetLSM6DSL_ciclo(uint8_t tipo)
{
  RefreshWD();  
  switch(tipo)
  {
    
  case ACC_OFF:
    SetAccFreq(LSM6DSL_XL_ODR_OFF);
    break;
    
  case ACC_LOW_POWER:
    SetAccFreq(LSM6DSL_XL_ODR_12Hz5);
    break;
    
  case ACC_ACTIVE:
    SetAccFreq(LSM6DSL_XL_ODR_416Hz);
    break;
    
  case ACC_INTERRUPT_ANGLE:
    SetInterruptAngolo();
    break;
    
  case GYR_OFF:
    LSM6DSL_GYRO_OFF();
    break;
    
  case GYR_ON:
    LSM6DSL_GYRO_ON();  
    break;

  case ACC_FS_4g:
    SetAccFS(LSM6DSL_4g);
    break;
  case GYR_FS_2000dps:
    SetGyrFS(LSM6DSL_2000dps);
    break;
    
  case ACC_SOGLIA:
    SogliaAccelerometroSet(SogliaAccelerometro);
    break;
    
  case INT_1:
    SetInterrupt1();
    break;
    
  case ACC_FILTER:
    /* Accelerometer - analog filter */
    SetAccFilter(LSM6DSL_XL_ANA_BW_1k5Hz);//lsm6dsl_xl_filter_analog_set(&dev_ctx, LSM6DSL_XL_ANA_BW_1k5Hz);
    
    /* Gyroscope - filtering chain */
    SetGyrFilter(LSM6DSL_HP_DISABLE_LP1_LIGHT);//lsm6dsl_gy_band_pass_set(&dev_ctx, LSM6DSL_HP_DISABLE_LP1_LIGHT/*LSM6DSL_HP_260mHz_LP1_STRONG*/);
    break;
    
  case INT_2:
    SetInterrupt2();
    break;
    
  case BDU:
    SetAccBDU();
    break;
    
  case PULL_UP:
    SetAccPULL_UP();
    break;
  
  default:
    
    break;
  }
}

void SetAccPULL_UP(void)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_sh_pin_mode_set(&dev_ctx,LSM6DSL_INTERNAL_PULL_UP); 
}

void SetAccBDU(void)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);
}

void SetAccFilter(lsm6dsl_bw0_xl_t val)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_xl_filter_analog_set(&dev_ctx, val);
}

void SetGyrFilter(lsm6dsl_lpf1_sel_g_t val)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_gy_band_pass_set(&dev_ctx, val/*LSM6DSL_HP_260mHz_LP1_STRONG*/);
}

void SetAccFreq(lsm6dsl_odr_xl_t freq)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  x_data_rate = freq;
    
  lsm6dsl_xl_data_rate_set(&dev_ctx, x_data_rate);
  x_power_mode = LSM6DSL_XL_NORMAL;
  lsm6dsl_xl_power_mode_set(&dev_ctx, x_power_mode);
}

void SetAccFS(lsm6dsl_fs_xl_t fs)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  x_full_scale = fs;
    
  lsm6dsl_xl_full_scale_set(&dev_ctx, x_full_scale);
}

void SetGyrFS(lsm6dsl_fs_g_t fs)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  y_full_scale = fs;
    
  lsm6dsl_gy_full_scale_set(&dev_ctx, y_full_scale);
}

void SetInterruptAngolo(void)
{
  lsm6dsl_ctx_t dev_ctx;

  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  //angolo
  lsm6dsl_6d_threshold_set(&dev_ctx, LSM6DSL_DEG_50);
  lsm6dsl_6d_feed_data_set(&dev_ctx, LSM6DSL_LPF2_FEED);
}

void SetInterruptShock(void)
{
  lsm6dsl_ctx_t dev_ctx;
  lsm6dsl_int2_route_t int_2_reg;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_wkup_dur_set(&dev_ctx, 0);
  /*
  * Set Wake-Up threshold: 1 LSb corresponds to FS_XL/2^6
  */
  lsm6dsl_wkup_threshold_set(&dev_ctx, 64);		//0.15g //FS*N/64
  /*
  * Enable interrupt generation on Wake-Up INT1 pin
  */
  lsm6dsl_pin_int2_route_get(&dev_ctx, &int_2_reg);
  int_2_reg.int2_wu = PROPERTY_ENABLE;
  lsm6dsl_pin_int2_route_set(&dev_ctx, int_2_reg);
  
  
}


uint8_t TimerAngularRateBlockedMem;
float LSM6DSL_cycle(void)
{		
  int32_t res;
  float angolar_rate_app;
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	  
  
  lsm6dsl_reg_t reg;
  
  //lettura dato velocitÓ angolare
  reg.status_reg.gda = 0;
  
  
  if(lsm6dsl_status_reg_get(&dev_ctx, &reg.status_reg) == HAL_OK)
  {
    if (reg.status_reg.gda)
    {
      res = lsm6dsl_angular_rate_raw_get(&dev_ctx, data_raw_angular_rate.u8bit);
      if(res == HAL_OK)
      {
        angular_rate_mdps[2] = LSM6DSL_FROM_FS_2000dps_TO_mdps(data_raw_angular_rate.i16bit[2]);
        angolar_rate_app = angular_rate_mdps[2];
        if(angolar_rate_app > 1)  
        {
          angular_rate_segno = 1;
        }
        else angular_rate_segno = -1;
        
        if(angular_rate_segno != angular_rate_segno_old)
        {
          angular_rate_segno_old = angular_rate_segno;
          Conta_inversione_segno++;
        }    
        lsm6dsl_angular_fail_count = 0;
        
        if(angular_rate_mdps_old[2] == angular_rate_mdps[2])
        {
          TimerAngularRateBlocked++;
          if(TimerAngularRateBlocked > 100)
          {
            CounterAngularRateBlocked++;
            TimerAngularRateBlocked = 0;
          }
        }
        else    TimerAngularRateBlocked = 0;
        angular_rate_mdps_old[2] = angular_rate_mdps[2];
        
      }
      else/* if(res == HAL_ERROR)*/
      {
        lsm6dsl_angular_fail_count++;
        lsm6dsl_fail_count_dbg++;
      }
    }
    else
    {
      lsm6dsl_angular_fail_count++;
      lsm6dsl_fail_count_dbg++;
    }
  }
  else
  {
    lsm6dsl_angular_fail_count++;
    lsm6dsl_fail_count_dbg++;
  }
  //valuto necessitÓ di init I2C
  if(lsm6dsl_angular_fail_count > FILTRO_FAILURE_I2C)
  {
    MX_I2C1_Init();
    lsm6dsl_angular_fail_count = 0;   
  }
  
  //lettura dato accelerazione
  reg.status_reg.xlda = 0;
  if(lsm6dsl_status_reg_get(&dev_ctx, &reg.status_reg) == HAL_OK)
  {
    if (reg.status_reg.xlda)
    {
      res = lsm6dsl_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
      if(res == HAL_OK)
      {
        acceleration_mg[0] = My_LSM6DSL_FROM_FS_TO_mg( x_full_scale, data_raw_acceleration.i16bit[0]);
        acceleration_mg[1] = My_LSM6DSL_FROM_FS_TO_mg( x_full_scale, data_raw_acceleration.i16bit[1]);
 /*       
        acceleration_mg[0] = LSM6DSL_FROM_FS_4g_TO_mg( data_raw_acceleration.i16bit[0]);
        acceleration_mg[1] = LSM6DSL_FROM_FS_4g_TO_mg( data_raw_acceleration.i16bit[1]);
        */       
        lsm6dsl_acceleration_fail_count = 0;
        
        if( (acceleration_mg_old[0] == acceleration_mg[0]) || (acceleration_mg_old[1] == acceleration_mg[1]) )
        {
          TimerAccelerationBlocked++;
          if(TimerAccelerationBlocked > 100)
          {
            CounterAccelerationBlocked++;
            TimerAccelerationBlocked = 0;
          }
        }
        else    TimerAccelerationBlocked = 0;
        acceleration_mg_old[0] = acceleration_mg[0];
        acceleration_mg_old[1] = acceleration_mg[1];
      }
      else/* if(res == HAL_ERROR)*/
      {
        lsm6dsl_acceleration_fail_count++;
        lsm6dsl_fail_count_dbg++;
      }
    }
    else
    {
      lsm6dsl_acceleration_fail_count++;
      lsm6dsl_fail_count_dbg++;
    }
  }
  else
  {
    lsm6dsl_acceleration_fail_count++;
    lsm6dsl_fail_count_dbg++;
  }
  //valuto necessitÓ di init I2C
  if(lsm6dsl_acceleration_fail_count > FILTRO_FAILURE_I2C) //TODO: check errore I2C busy + reinit (3 condizioni in or con pulizia di tutte)
  {
    MX_I2C1_Init();
    lsm6dsl_acceleration_fail_count = 0;      
  }
   
  ///////conteggio numero giri basato su giroscopio///////
  //NumeroGiriGyr = (angular_rate_mdps[2]/3600);	//base 100ms
  NumeroGiriGyr = (angular_rate_mdps[2]);///36000);	//base 10ms
  ////////////////////////////////////////////////////////
  
  //tengo traccia di quanto tempo passo con segno di vel angolare 
  if(angular_rate_segno == 1)
  {
    timer_angular_rate_segno_pos++;
  }
  else
  {
    timer_angular_rate_segno_neg++;
  }
  
  //allo scadere della finestra di 5 secondi valuto sia la velocitÓ con cui go cambiato segno che il rapporto fra il tempo di permanenza nei due segni
  finestra_osservazione_cambio_segno++;
  if(finestra_osservazione_cambio_segno >= FINESTRA_OSSERVAZIONE_CAMBIO_SEGNO)
  {
    Derivata_cambio_segno = (float)Conta_inversione_segno/(float)finestra_osservazione_cambio_segno;

    if(Derivata_cambio_segno > DERIVATA_CAMBIO_SEGNO_SOGLIA)     
    {
      ContatoreVelAngolareStabile = 0;
      //FlagPulisciParzialeMagnetometro = 1;
    }
    
    Conta_inversione_segno = 0;
    
    //tempo di permanenza nei due segni
    ratio_timer_angular_rate_segno = timer_angular_rate_segno_pos/timer_angular_rate_segno_neg;
    /*
    if( (ratio_timer_angular_rate_segno > soglia_ratio)  &&  (ratio_timer_angular_rate_segno < (1/soglia_ratio)) )
    {
      ContatoreVelAngolareStabile_1 = 0;
      FlagPulisciParzialeMagnetometro_1 = 1;
    }
    */
    if(ratio_timer_angular_rate_segno > soglia_ratio)
    {
      Contatore_retio_pos++;
    }
    else if(ratio_timer_angular_rate_segno < (1/soglia_ratio))
    {
      Contatore_retio_neg++;
    }
    timer_angular_rate_segno_pos = 0;
    timer_angular_rate_segno_neg = 0;
    //
    finestra_osservazione_cambio_segno = 0;
  }
  
  
  
  return NumeroGiriGyr;	
}

float LSM6DSL_Orientamento(void)
{
  float result = ORIENTAMENTO_DUMMY;//il dato viene poi valorizzato solo se pronto e valido
  int32_t res;
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	  
  
  lsm6dsl_reg_t reg;
  reg.status_reg.xlda = 0;
  if(lsm6dsl_status_reg_get(&dev_ctx, &reg.status_reg) == HAL_OK)
  {
    if (reg.status_reg.xlda)
    {   
      res = lsm6dsl_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
      if(res == HAL_OK)
      {
        result = My_LSM6DSL_FROM_FS_TO_mg( x_full_scale, data_raw_acceleration.i16bit[2]);
        //result = LSM6DSL_FROM_FS_4g_TO_mg( data_raw_acceleration.i16bit[2]);
        lsm6dsl_orientation_fail_count = 0;
      }
      else
      {
        lsm6dsl_orientation_fail_count++;
        lsm6dsl_fail_count_dbg++;
      }
    }
    else
    {
      lsm6dsl_orientation_fail_count++;
      lsm6dsl_fail_count_dbg++;
    }
  }
  else
  {
    lsm6dsl_orientation_fail_count++;
    lsm6dsl_fail_count_dbg++;
  }
  //valuto necessitÓ di init I2C
  if(lsm6dsl_orientation_fail_count > FILTRO_FAILURE_I2C)
  {
    MX_I2C1_Init();
    lsm6dsl_orientation_fail_count = 0;
    
  }
  return result;
}

float LSM6DSL_Temperatura(void)
{
  float temperature_degC = 0xFF;        //il dato viene poi valorizzato solo se pronto e valido
  int32_t res;
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	  
  
  lsm6dsl_reg_t reg;
  reg.status_reg.tda = 0;
  if(lsm6dsl_status_reg_get(&dev_ctx, &reg.status_reg) == HAL_OK)
  {
    if (reg.status_reg.tda)
    {   
      res = lsm6dsl_temperature_raw_get(&dev_ctx, data_raw_temperature.u8bit);
      if(res == HAL_OK)
      {
        temperature_degC = LSM6DSL_FROM_LSB_TO_degC( data_raw_temperature.i16bit );
        lsm6dsl_temperature_fail_count = 0;
      }
      else/* if(res == HAL_ERROR)*/
      {
        lsm6dsl_temperature_fail_count++;
        lsm6dsl_fail_count_dbg++;
      }
    }
    else
    {
      lsm6dsl_temperature_fail_count++;
      lsm6dsl_fail_count_dbg++;
    }
  }
  else
  {
    lsm6dsl_temperature_fail_count++;
    lsm6dsl_fail_count_dbg++;
  } 
  //valuto necessitÓ di init I2C
  if(lsm6dsl_temperature_fail_count > FILTRO_FAILURE_I2C)
  {
    MX_I2C1_Init();
    lsm6dsl_temperature_fail_count = 0;
  }
  
  
  return temperature_degC;
}

void LSM6DSL_GYRO_OFF(void)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	
  
  y_data_rate = LSM6DSL_GY_ODR_OFF;
  y_power_mode = LSM6DSL_GY_NORMAL;
  
  lsm6dsl_gy_data_rate_set(&dev_ctx, y_data_rate);
  lsm6dsl_gy_power_mode_set(&dev_ctx, y_power_mode);	
  
  angular_rate_mdps[0] = 0;
  angular_rate_mdps[1] = 0;
  angular_rate_mdps[2] = 0;
}

void LSM6DSL_GYRO_ON(void)
{
  lsm6dsl_ctx_t dev_ctx;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;	
  
  y_data_rate = LSM6DSL_GY_ODR_1k66Hz;
  y_full_scale = LSM6DSL_2000dps;
  y_power_mode = LSM6DSL_GY_HIGH_PERFORMANCE;
    
  lsm6dsl_gy_data_rate_set(&dev_ctx, y_data_rate/*LSM6DSL_GY_ODR_12Hz5*/);
  lsm6dsl_gy_full_scale_set(&dev_ctx, y_full_scale);
  lsm6dsl_gy_power_mode_set(&dev_ctx, y_power_mode);	
}


void SogliaAccelerometroSet(uint8_t val)
{
  lsm6dsl_ctx_t dev_ctx;

  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;

  lsm6dsl_wkup_dur_set(&dev_ctx, 0);
  /*
  * Set Wake-Up threshold: 1 LSb corresponds to FS_XL/2^6
  */

  lsm6dsl_wkup_threshold_set(&dev_ctx, val);
  
  /*
  * Enable interrupt generation on Wake-Up INT1 pin
  */

}


void CleanFlagInterrupt(void)
{
  flag_lsm6dsl_ignore_int_1 = 0;
  flag_lsm6dsl_ignore_int_2 = 0;
      
}


void SetInterrupt1(void)
{
  lsm6dsl_ctx_t dev_ctx;
  lsm6dsl_int1_route_t int_1_reg;
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;

  lsm6dsl_pin_int1_route_get(&dev_ctx, &int_1_reg);
  int_1_reg.int1_6d = PROPERTY_ENABLE;
  lsm6dsl_pin_int1_route_set(&dev_ctx, int_1_reg);

  flag_lsm6dsl_ignore_int_1 = 1;
}

void SetInterrupt2(void)
{
  lsm6dsl_ctx_t dev_ctx;
  lsm6dsl_int2_route_t int_2_reg;
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;
  
  lsm6dsl_pin_int2_route_get(&dev_ctx, &int_2_reg);
  int_2_reg.int2_wu = PROPERTY_ENABLE;
  lsm6dsl_pin_int2_route_set(&dev_ctx, int_2_reg); 

  flag_lsm6dsl_ignore_int_2 = 1;
}

void SetInterruptFreeFall(uint8_t val)
{
  lsm6dsl_ctx_t dev_ctx;
  lsm6dsl_int2_route_t int_2_reg;
  
  dev_ctx.write_reg = platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c1;

  /*
  * Enable interrupt generation on Free fall INT1 pin
  */
  lsm6dsl_pin_int2_route_get(&dev_ctx, &int_2_reg);
  int_2_reg.int2_ff = PROPERTY_ENABLE;
  lsm6dsl_pin_int2_route_set(&dev_ctx, int_2_reg);
  
  ff_duration = val;
  ff_soglia = LSM6DSL_FF_TSH_156mg;
  lsm6dsl_ff_threshold_set(&dev_ctx, ff_soglia);
  lsm6dsl_ff_dur_set(&dev_ctx, ff_duration);
  
}



lsm6dsl_all_sources_t lsm6dsl_source;
uint8_t FreeFallSourceRead(void)
{
  lsm6dsl_ctx_t lsm6dsl_mov;
  //lsm6dsl_int2_route_t int_2_reg;
  
  lsm6dsl_mov.write_reg = platform_write;
  lsm6dsl_mov.read_reg = platform_read;
  lsm6dsl_mov.handle = &hi2c1;
  
  lsm6dsl_all_sources_get(&lsm6dsl_mov, &lsm6dsl_source);
  return lsm6dsl_source.reg.wake_up_src.ff_ia;
}

uint8_t WakeUpSourceRead(void)
{
  lsm6dsl_ctx_t lsm6dsl_mov;
  //lsm6dsl_int2_route_t int_2_reg;
  
  lsm6dsl_mov.write_reg = platform_write;
  lsm6dsl_mov.read_reg = platform_read;
  lsm6dsl_mov.handle = &hi2c1;
  
  lsm6dsl_all_sources_get(&lsm6dsl_mov, &lsm6dsl_source);
  return lsm6dsl_source.reg.wake_up_src.wu_ia;
}

float My_LSM6DSL_FROM_FS_TO_mg(lsm6dsl_fs_xl_t scala, short val)
{
  float res = 0;
  switch(scala)
  {
  case LSM6DSL_2g:
    res =  LSM6DSL_FROM_FS_2g_TO_mg(val);
    break;
    
  case LSM6DSL_4g:
    res =  LSM6DSL_FROM_FS_4g_TO_mg(val);
    break;
    
  case LSM6DSL_8g:
    res =  LSM6DSL_FROM_FS_8g_TO_mg(val);
    break;
    
  case LSM6DSL_16g:
    res =  LSM6DSL_FROM_FS_16g_TO_mg(val);
    break;
  }
  return res;

}


