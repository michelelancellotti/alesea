#include "Alesea_ciclo_vita.h"
#include "EEPROM.h"
#include "nfc_SM.h"
#include "communication_SM.h"
#include "modem_SM.h"
#include "modem_debug.h"
#include "stm32l0xx_hal.h"
#include "Gestione_dato.h"
#include "LSM6DSL_SM.h"
#include "lis2mdl_SM.h"
#include "Gestione_batteria.h"
#include "Alesea_memory.h"
#include "funzioni.h"
#include "wwdg.h"
#include "Globals.h"
#include "wifi.h"
#include "wifi_debug.h"
#include "lis2mdl_reg.h"
#include "LIS2MDL.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>




uint32_t Count1ms, Count1msOld, CountDiff;
uint8_t Count10ms, Count100ms, Count1000ms;
uint8_t flag1ms, flag10ms, flag100ms, flag1000ms, flag60000ms;

uint8_t FlagPresenzaNFC;
uint8_t InitShock;
uint8_t InitMovimento;

uint8_t FreeFallInterrupt;
uint8_t WakeUpInterrupt;

uint8_t TriggerContatoreRisvegli;




void HAL_SYSTICK_Callback(void)
{
  ml_SystemTick++;		//Usato per soft-timer libreria modem/WiFi
  
  Count1ms++;
  flag1ms = 1;
  if(Count1ms > 9)
  {
    Count10ms++;
    Count1ms = 0;
    flag10ms = 1;	
  }		
  if(Count10ms > 9)
  {
    Count100ms++;
    Count10ms = 0;
    flag100ms = 1;
  }	
  if(Count100ms > 9)
  {
    Count100ms = 0;
    flag1000ms = 1;
    Count1000ms++;
  }
  if(Count1000ms > 59)
  {
    flag60000ms = 1;
    Count1000ms = 0;
  }
}


///////////////////////////////////////////////////////////////////////////
//////////////////////////// BASE DEI TEMPI ///////////////////////////////
///////////////////////////////////////////////////////////////////////////
uint8_t CountErrorTemp;
void BaseDeiTempi(void)
{
  static uint8_t exec_modem = 0;
    
  HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_SET);	//Alimento TAG
  
  ///////1ms///////
  if(flag1ms)
  {
    flag1ms = 0;
    IncrementoTempiConsumi(Dati.ActivationTiming.SchedulingInterval);		//Funzione che incrementa il cronometro delle diverse fasi
  }
  ///////10ms///////
  if(flag10ms)
  {
    flag10ms = 0;
    //Automi eseguiti in maniera esclusiva
    if(exec_modem == 1)
    {
      modem_Automa(20);
      exec_modem = 0;
    }
    else
    {
      if(VersioneHW == VERSIONE_HW_4_X)
        wifi_Automa(20);
      exec_modem = 1;
    }
    
    if(TriggerContatoreRisvegli == 1)
    {
      TriggerContatoreRisvegli = 0;
      Diagnostica.ContatoreRisvegli++;
      EEPROM_WriteWord( EEPROM_CONTATORE_RISVEGLI, (uint32_t)Diagnostica.ContatoreRisvegli ); 
    }
    
    
    if(FlagWakeupDaMEMS && ! FlagNFCAttivo)		
    {
      MacchinaStatiMEMS();
      Supervisor = 0;	//tengo a zero il supervisore per non farlo intervenire durante lunghi srotolamenti
      
      if(ContatoreVelAngolareStabile_2 <= FILTRO_VEL_ANGOLARE_STABILE) 
      {    
        ContatoreVelAngolareStabile_2++;
        if(ContatoreVelAngolareStabile_2 == FILTRO_VEL_ANGOLARE_STABILE)
        {
          if( (Contatore_retio_pos == 0) || (Contatore_retio_neg == 0) )
          {
            IntegratoreMagnetometroOn_2 = 1;
          }
          FlagPulisciParzialeMagnetometro_2 = 1;
          Contatore_retio_pos = 0;
          Contatore_retio_neg = 0;
          //solo se sono tutti i parziali nella stessa direzione
          ContatoreVelAngolareStabile_2 = 0;
        }
      }
 
    }
    
    //Reset I2C accelerometro/tag in caso di errore periferica
    if(FlagReinit_I2C_Tag || FlagReinit_I2C_Acc)
    {
      FlagReinit_I2C_Tag = 0;
      FlagReinit_I2C_Acc = 0;
      MX_I2C1_Init();
      CountI2C1Reinit++;
    }
    //Reset I2C magnetometro in caso di errore periferica
    if(FlagReinit_I2C_Mag && VersioneHW==VERSIONE_HW_4_X)
    {
      FlagReinit_I2C_Mag = 0;
      MX_I2C3_Init();
      CountI2C3Reinit++;
    }

  }
  ///////100ms///////
  if(flag100ms)
  {    
    flag100ms = 0;
    if( TriggerOrientamento == 1)       
    {
      if(GetOrientamento(20) == 1)
        TriggerOrientamento = 0;
    }  
    
    if(Lis2mdl_powerOn)
      Magnetometro_dato_medio = LIS2MDL_get_mean_10();
    
    
    FlagPresenzaNFC = !HAL_GPIO_ReadPin(FD_GPIO_Port, FD_Pin);	//Presenza TAG
    if(FlagPresenzaNFC)
    {
      FlagWakeupDaNFC = 1;
    }
    
    
    //MOVIMENTAZIONE
    if(FlagWakeUpDaMovimento && InitMovimento && !FlagNFCAttivo)
    {
      MacchinaStatiMovimento();
    }
    else
    {
      FlagWakeUpDaMovimento = 0;
      InitMovimento = 1;
    }
    //FREEFALL
    if(FlagWakeUpDaFreeFall&& !FlagNFCAttivo)
    {
      MacchinaStatiFreeFall();
      FlagWakeUpDaFreeFall = 0;
    }
    
    //SHOCK
    if( FlagWakeUpDaShock && InitShock && !FlagNFCAttivo)		
    {
      MacchinaStatiShock();
    }
    else
    {
      InitShock = 1;
      FlagWakeUpDaShock = 0;
    }
    //
    
    //NFC
    if( FlagWakeupDaNFC == 1 )	
    {	
      if(Dati.StatoAlesea == ALESEA_PRE_COLLAUDO)
      {
        SetStatoAlesea(ALESEA_COLLAUDO);
        ContatoreAttesaCollaudo = ATTESA_COLLAUDO;
        FlagNoRxMQTT = 1;
        FlagWakeupDaRTC = 0; 
      }
      else
      {
        MacchinaStatiNFC();
      }
    }
    else
    {
      TimerPresenzaNFC = 0;
    } 
    //
    
    GestioneFlagComandi();
  }
  ///////1s///////
  if(flag1000ms)
  {
    flag1000ms = 0;
    /*
    if(ContatoreVelAngolareStabile <= FILTRO_VEL_ANGOLARE_STABILE) 
    {    
      ContatoreVelAngolareStabile++;
      if(ContatoreVelAngolareStabile == FILTRO_VEL_ANGOLARE_STABILE)
      {
        IntegratoreMagnetometroOn = 1;
        ContatoreVelAngolareStabile = 0;
      }
    }
    */
    
    /*
    if(ContatoreVelAngolareStabile_1 <= FILTRO_VEL_ANGOLARE_STABILE) 
    {    
      ContatoreVelAngolareStabile_1++;
      if(ContatoreVelAngolareStabile_1 == FILTRO_VEL_ANGOLARE_STABILE)
      {
        IntegratoreMagnetometroOn_1 = 1;
        ContatoreVelAngolareStabile_1 = 0;
      }
    }
    */
     
    
    
    
    if(FlagWakeupPerTemp)
    {
      if(GetTemperature())
      {
        FlagWakeupPerTemp = 0;
        CountErrorTemp = 0;
      }
      else
      {
        CountErrorTemp++;
        if(CountErrorTemp >= 5)
        {
          FlagWakeupPerTemp = 0;
          CountErrorTemp = 0;
        }
      }
      if( (Dati.MovementEvent.MovementSession == 0) && (Dati.ShockEvent.ShockSession == 0) && (Dati.SpinEvent.SpinSession == 0) )  //maschero il risveglio per temperatura se ho altri eventi in corso per non avere risvegli anticipati rispetto a quello impostato per evento
        ImpostaRisveglio((uint32_t)TEMPERATURE_SCH_INTERVAL);
    }
    
    
    if(Dati.StatoAlesea == ALESEA_COLLAUDO)		GestioneLed();	
    if(VersioneHW == VERSIONE_HW_4_X)   
    {
      if(Dati.StatoAlesea == ALESEA_COLLAUDO)   CollaudoMagnetometro(); 
      else      MacchinaStatiMagnetometro();
    }
    
    
    if(SpinTimeCounterEnable)   
    {
      SpinTimeCounter++;
      SubSessionDuration++;
    }
    MonitorActivity();
    GestioneSupervisore();

    //Gestione ritardo collaudo
    if(ContatoreAttesaCollaudo > 0)
    {
      ContatoreAttesaCollaudo--;
      if(ContatoreAttesaCollaudo == 1)
      {
        FlagWakeupDaRTC = 1;
      }
    }
    //
    
#ifdef ENB_COMMUNICATION_DEBUG 
    modem_debug_trigger();
    wifi_debug_trigger();
#else
    //Sveglia da RTC -> macchina a stati comunicazione: eseguo solo se il risveglio arriva da RTC e se non ha iniziato a contare giri prima di attivare comunicazione (solo in stato ACTIVE e COLLAUDO)
    if( 
         (( FlagWakeupDaRTC && (!FlagWakeupDaMEMS || ON_communication)) ||  FlagCommOnInSpinSession || FlagCommOnInShockSession || FlagCommOnInMovementSession) && ( (Dati.StatoAlesea == ALESEA_COLLAUDO) || (Dati.StatoAlesea == ALESEA_ACTIVE) || (Dati.StatoAlesea == ALESEA_START_UP)|| (Dati.StatoAlesea == ALESEA_WARM_UP) )        
      )	
    {
      Start_MacchinaStati_Comunicazione();      //start automa comunicazione (gestito ad evento)  
    }
    MacchinaStati_Comunicazione();
#endif   

    
    //Gestione dello spegnimento su condizione legata allo stato del dispositivo
    GestioneTimeoutAttivazione();
    
    //Se la comunicazione (WiFi/modem) � attiva incremento la variabile dedicata per il tempo di attivit�, che verr� poi stampata nel JSON
    if(ON_communication) Dati.NetworkPerformance.TempoModemOn++;
    else Dati.NetworkPerformance.TempoModemOn = 0;	
    
    //Gestione della condizione su cui entrare in stop mode
    if( (Dati.StatoAlesea != ALESEA_READY) && (Dati.StatoAlesea != ALESEA_PRE_COLLAUDO) )
    {
#ifdef DEBUG_ON
      Supervisor = 0;
#else
      GestioneStopMode();	//ultimo comando da eseguire dopo verifica flag relativi a comandi da remoto    
#endif
    }
  }	
  /////// 60 s ///////
  if(flag60000ms)
  {
    //Salvataggio periodico in EEPROM del numero giri
    //TODO:verificare se ha senso rimuoverlo
    if(FlagWakeupDaMEMS)	
    {
      EEPROM_WriteWord( EEPROM_NUMERO_GIRI_TOT_CW, (uint32_t)Dati.SpinEvent.NumeroGiriCw );
      EEPROM_WriteWord( EEPROM_NUMERO_GIRI_TOT_ACW, (uint32_t)Dati.SpinEvent.NumeroGiriACw );
    }
    
    //if( (Dati.AleseaMeasure.CaricaResidua > 0) && (Dati.AleseaMeasure.CaricaResidua < CARICA_RESIDUA_SWITCH_OFF) )      Spegnimento(); 
    flag60000ms = 0;
  }
}





////////////////////////////////////////////////////////////////////////////
////////////////GESTIONE DEI FLAG COMANDI DA APP E PORTALE//////////////////
////////////////////////////////////////////////////////////////////////////
void GestioneFlagComandi(void)
{
  if(FlagAttivazione)
  {
    if(Dati.StatoAlesea == ALESEA_READY)
    {
      AttivaAlesea();
      NdefCompile(Dati.StatoAlesea);
      FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
      FlagNoRxMQTT = 1; 	//bypass rx MQTT
    }
    FlagAttivazione = 0;
  }
  if(FlagBatteryReset)		
  {
    ResettaConsumo();
    //FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    //FlagNoRxMQTT = 1; 	//bypass rx MQTT
    FlagBatteryReset = 0;
  }
  if(FlagSpinReset)
  {
    ResettaContagiri();
    NdefCompile(Dati.StatoAlesea);
    FlagSpinReset = 0;
  }
  if(FlagDiagnosticaReset)
  {
    FlagDiagnosticaReset = 0;
    ResettaDiagnostica();
  }
  if(FlagShockReset)
  {
    ResettaContaShock();
    NdefCompile(Dati.StatoAlesea);
    FlagShockReset = 0;
  }
  if(FlagFreeFallReset)
  {
    ResettaContaFreeFall();
    //NdefCompile(Dati.StatoAlesea);
    FlagFreeFallReset = 0;
  }
  if(FlagFactoryReset == 1)
  {
    FactoryResetPendente = 1;   //sposto la chiamata alle funzioni dedicate nella macchina a stati della comunicazione prima che vada in stop mode
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagFactoryReset = 0;
  }	
  if(FlagDisattivazione == 1)
  {
    DisattivazionePendente = 1; //sposto la chiamata alle funzioni dedicate nella macchina a stati della comunicazione prima che vada in stop mode
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagDisattivazione = 0;
  }
  if(FlagSincronizzazione == 1)
  {
    NdefCompile(Dati.StatoAlesea);
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagSincronizzazione = 0;
  }
  if(FlagInstantMessage == 1)
  {
    NdefCompile(Dati.StatoAlesea);
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    //FlagNoRxMQTT = 1; 	//bypass rx MQTT
    FlagInstantMessage = 0;
  }
  if(FlagUpgradeFw == 1)        
  {
    NdefCompile(Dati.StatoAlesea);
    AggiornamentoFwPendente = 1;
    FlagWakeupDaRTC = 1;	//forzo il risveglio da schedulazione
    //FlagNoRxMQTT = 1; 		//bypass rx MQTT
    FlagUpgradeFw = 0;          
  }
}



////////////////////////////////////////////////////////////////
//                    Risveglio da RTC		              //																		
////////////////////////////////////////////////////////////////
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *hrtc)
{
 if(ContatoreDiRisvegliEnable == 1)
  {
    TriggerContatoreRisvegli = 1;
    ContatoreDiRisvegliEnable = 0;
  }
  //Alzo il flag solo se il dispositivo � attivo o in collaudo e se il flag non � gi� attivo
  if( !FlagWakeupDaRTC && ((Dati.StatoAlesea == ALESEA_ACTIVE) || (Dati.StatoAlesea == ALESEA_COLLAUDO) || (Dati.StatoAlesea == ALESEA_START_UP) || (Dati.StatoAlesea == ALESEA_WARM_UP)) )	
  {
    if(  Dati.SpinEvent.SpinSession == 1)   
    {      
      if( !FlagWakeupDaMEMS )     //se mi sono svegliato per schedulazione da rotazione e non sto pi� girando
      {
        FlagWakeupDaRTC = 1;          
        //if(Dati.AleseaMeasure.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON)        FlagWakeupPerTemp = 1;
        FlagCommOnInSpinSession = 0;       //forzato a zero (viene normalmente utilizzato per dare start a tx e schedulare nuova sveglia)
        //FlagSpinSessionStart = 0;
        Dati.SpinEvent.SpinSession = 2;
        SubSessionCounter = 0;
        
        //se � terminata la rotazione termino anche il movimento generato in concomitanza
        if(Dati.MovementEvent.MovementSession == 1)
          Dati.MovementEvent.MovementSession = 2;
        
        
        //nel caso di chiusura forzata della spin session per timeout sessione o numero sottosessioni elevato, il calcolo della freq di rotazione non � da considerare valido
        if( (Dati.TiltEvent.Orientamento == FLANGIA_A_TERRA) && (Dati.SpinEvent.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON) )  //rotazione orizzontale
          if(SpinTimeCounter > (/*FILTRO_QUADRANTE_FERMO*/ Dati.MovementEvent.FiltroMovimento*SpinStartStopCounterAppoggio) ) SpinTimeCounter = SpinTimeCounter - (/*FILTRO_QUADRANTE_FERMO*/Dati.MovementEvent.FiltroMovimento*SpinStartStopCounterAppoggio);  //passo da base 10ms a 1s
        else
          if(SpinTimeCounter > (/*FILTRO_QUADRANTE_FERMO*/ Dati.MovementEvent.FiltroMovimento*SpinStartStopCounterAppoggio) ) SpinTimeCounter = SpinTimeCounter - (FILTRO_ROTAZ_ORIZZ_FERMO_DEFAULT*SpinStartStopCounterAppoggio);  //passo da base 10ms a 1s
        
       
        Dati.SpinEvent.SpinFreq = 60*NumeroGiriFloatFase/SpinTimeCounter; 
        Dati.SpinEvent.SpinStartStopCounter = SpinStartStopCounterAppoggio; //numero sottosessioni di spin tra la prima (conclusa) e il risveglio da RTC schedulato a 3600 secondi dopo l'ultima rotazione
        SpinStartStopCounterAppoggio = 0;
        NumeroGiriFloatFase = 0;
        SpinTimeCounter = 0; 
      }
      else
      {
        ImpostaRisveglio(SPIN_SCH_INTERVAL_SECOND);     
      }
    }
    else if( (Dati.ShockEvent.ShockSession == 1) && (FlagWakeUpDaShock == 0) )   
    {
      FlagWakeupDaRTC = 1;             
      //FlagShockSessionStart = 0;
      Dati.ShockEvent.ShockSession = 2;
    }
    else if( (Dati.MovementEvent.MovementSession == 1) && (FlagWakeUpDaMovimento == 0) )   //aggiunta condizione su FlagWakeUpDaMovimento per evitare ingresso su scadere RTC durante comunicazione causata da movimento 
    {
      FlagWakeupDaRTC = 1;
      Dati.MovementEvent.MovementSession = 2;
      //FlagMovementSessionStart = 0;
      //SogliaAccelerometro = Dati.SogliaMovimento;//SOGLIA_MOV; //se trasmetto per RTC e non per movimentazioni riporto la soglia al default per rilevare movimento
      //SogliaAccelerometroSet(SogliaAccelerometro);
    }
    else    
    {
      if(Dati.AleseaMeasure.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON)     
      {
        if(SchIntervalCounter > 1)      //se il rapporto fra la schedulazione e 1 ora per lettura temp � > 1
        {
          SchIntervalCounter--;
        }
        else
        {
          FlagWakeupDaRTC = 1;  
        }
      }
      else
      {
        FlagWakeupDaRTC = 1;  
      }
      
      //SogliaAccelerometro = Dati.SogliaMovimento;//SOGLIA_MOV; //se trasmetto per RTC e non per movimentazioni riporto la soglia al default per rilevare movimento
      //SogliaAccelerometroSet(SogliaAccelerometro);
    }
  }
  FlagWakeupPerTemp = 1;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(GPIO_Pin);
  
  if( (Dati.StatoAlesea == ALESEA_COLLAUDO) || (Dati.StatoAlesea == ALESEA_PRE_COLLAUDO) || (Dati.StatoAlesea == ALESEA_UND) )       //bypass delgli interrupt da accelerometro se sono in collaudo in modo che non partano comunicazioni anticipate in collaudo
  {
    return;
  }
  
  
  if(flag_lsm6dsl_ignore_int_1 != 0)        
  {
    flag_lsm6dsl_ignore_int_1 = 0;
    return;
  }
  if(flag_lsm6dsl_ignore_int_2 != 0)        
  {
    flag_lsm6dsl_ignore_int_2 = 0;
    return;
  }
  if(ContatoreDiRisvegliEnable == 1)
  {
    TriggerContatoreRisvegli = 1;
    ContatoreDiRisvegliEnable = 0;
  }
  if(GPIO_Pin == GPIO_PIN_3)	//risveglio da MEMS
  {
#ifdef FREE_FALL_ON
    FreeFallInterrupt = FreeFallSourceRead();
#endif
    WakeUpInterrupt = WakeUpSourceRead();
    if(WakeUpInterrupt)
    {
      if(SogliaAccelerometro == Dati.ShockEvent.SogliaShock)    FlagWakeUpDaShock = 1;
      else if(!ON_communication)        //eseguo solo se la macchina a stati della comunicazione non � gi� attiva. Altrimenti rischio di avere un pacchetto con ms = 1 e senza GPS
      {
        if(Dati.MovementEvent.MovementSession == 0)
          FlagWakeUpDaMovimento = 1;
        if(Dati.SpinEvent.RotazioneOrizzontaleOn == ROTAZIONE_ORIZZ_ON) //solo se abilitata la rotaz orizz
          FlagWakeupDaMEMS = 1;
      }
      WakeUpInterrupt = 0;
    }
    if(FreeFallInterrupt)
    {
      FlagWakeUpDaFreeFall = 1;
      FreeFallInterrupt = 0;
    }
  }
  if( ((VersioneHW == VERSIONE_HW_3_X) && (GPIO_Pin == GPIO_PIN_4)) || ((VersioneHW == VERSIONE_HW_4_X) && (GPIO_Pin == GPIO_PIN_0)) ) //interrupt accelerometro su pin differente tra le 2 versioni HW
  {
    FlagWakeupDaMEMS = 1;
    FlagRefreshRotation = 1;
    ContaInterruptRotazione++;
  }
  if(GPIO_Pin == GPIO_PIN_5)	//risveglio da NFC
  {
    FlagWakeupDaNFC = 1;        
  }
  
  /* NOTE: This function Should not be modified, when the callback is needed,
  the HAL_GPIO_EXTI_Callback could be implemented in the user file
  */ 
}


