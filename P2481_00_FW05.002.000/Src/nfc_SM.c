#include "main.h"
#include "Gestione_dato.h"
#include "Gestione_batteria.h"
#include "nfc_SM.h"
#include "nfc.h"
#include "ndef.h"
#include "funzioni.h"
#include "ndef_lib.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>

AleseaMacchinaStatiNFC StatoNFC, StatoFuturoNFC;
uint8_t FlagWakeupDaNFC;
uint8_t TimerPresenzaNFC;
uint8_t FlagNFCAttivo;

uint32_t CountFail;
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////MACCHINA A STATI NFC////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
uint8_t TimerCambioStatoNFC;
uint8_t CampoPresente;
uint8_t NotificaEnd;
void MacchinaStatiNFC(void)
{
  //CampoPresente = !HAL_GPIO_ReadPin(FD_GPIO_Port, FD_Pin);	//Presenza TAG
  
 if( NotificaEnd )	
  {
    NotificaEnd = 0;  
    HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_RESET);        //Disalimento TAG
    StatoNFC = NFC_START;
    FlagWakeupDaNFC = 0;
    FlagNFCAttivo = 0;
    return;
  }
  
  
  switch(StatoNFC)
  {
  case NFC_WAIT:
    if(TimerCambioStatoNFC > 0)		TimerCambioStatoNFC--;
    else		StatoNFC = StatoFuturoNFC;
    break;
    
  case NFC_START:
    HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_SET);	//Alimento TAG
    Dati.AleseaMeasure.CaricaResidua = CalcolaCaricaResidua(Consumo_uA_h_tot);
    FlagNFCAttivo = 1;
    StatoNFC = NFC_WAIT;
    StatoFuturoNFC = NFC_READ;
    TimerCambioStatoNFC = 10;	//ritardo prima del cambio stato (base 100ms)
    break;
    
  case NFC_READ:
    //leggo il tag e compilo il buffer
    NfcRead();
    StatoNFC = NFC_DECODE;/*NFC_WAIT;*/
    StatoFuturoNFC = NFC_DECODE;
    TimerCambioStatoNFC = 0; //ritardo prima del cambio stato (base 100ms)
    break;
    
  case NFC_DECODE:
    //decodifico il buffer e compilo le strutture
    NdefDecode();
    StatoNFC = NFC_CHECK;/*NFC_WAIT;*/
    StatoFuturoNFC = NFC_CHECK;
    TimerCambioStatoNFC = 0; //ritardo prima del cambio stato (base 100ms)
    break;
    
  case NFC_CHECK:
    //verifico il codice inviato da app e gestisco il comando.
    if(VerificaCodiceNFC())
    {
      GestioneComandiNdef();			
      ClearL();
      //StatoNFC = NFC_WAIT;
      //StatoFuturoNFC = NFC_END;
      //TimerCambioStatoNFC = 50; //ritardo prima del cambio stato (base 100ms)
    }
    StatoNFC = NFC_WRITE;/*NFC_WAIT;*/
    StatoFuturoNFC = NFC_WRITE;
    TimerCambioStatoNFC = 0;//50; //ritardo prima del cambio stato (base 100ms)
    /*
    else	//se codice errato o non presente ritorno in stato READ
    {
    StatoNFC = NFC_WAIT;
    StatoFuturoNFC = NFC_READ;
    TimerCambioStatoNFC = 0;//20; //ritardo prima del cambio stato (base 100ms)
    CountFail++;
  }
    */
    break;
    
   case NFC_WRITE:
    NdefCompile(Dati.StatoAlesea);
    StatoNFC = NFC_END;/*NFC_CHECK_WRITE;*/
    break;
     
    
  case NFC_END:
    StatoNFC = NFC_START;/*NFC_WAIT;*/
    StatoFuturoNFC = NFC_START;
    NotificaEnd = 1;
    break;		
  }
}
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/*
void InitTag(void)
{    
  uint8_t buf2[16] = { 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE1, 0x10, 0x6D, 0x00 };
  HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_SET);	//accendo il tag 
  HAL_Delay(100);
  ScriviDatoNFC(&buf2[0], 0x00);
}

void DeInitTag(void)
{
  uint8_t buf2[16] = { 0xAA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  HAL_GPIO_WritePin(NFC_GPIO_Port, NFC_Pin, GPIO_PIN_SET);	//accendo il tag 
  HAL_Delay(100);
  ScriviDatoNFC(&buf2[0], 0x00);
}
*/

/*
void NFC_Read_var(uint16_t *array)
{  
  //130 ms esecuzione
  uint8_t counter_marker = 0;
  uint8_t pos_marker[6] = {0,0,0,0,0,0};
  uint16_t indice_pkt, giri_orari, giri_antiorari;
  uint8_t indice_pkt_lenght, giri_orari_lenght, giri_antiorari_lenght;
  
  RefreshWD();  
  HAL_Delay(10);
  NfcRead();
  NdefDecode();
  
  for(uint8_t i = 0; i < (uint8_t)L3.length; i++)
  {
    if(L3.buffer[i] == ';')
    {  
      pos_marker[counter_marker] = i; 
      counter_marker++;
      if(counter_marker == sizeof(pos_marker))
        break;
     
    }
  }
  indice_pkt_lenght = pos_marker[1] - pos_marker[0] - 1;
  giri_orari_lenght = pos_marker[4] - pos_marker[3] - 1;
  giri_antiorari_lenght = pos_marker[5] - pos_marker[4] - 1;
  
  indice_pkt = EstraiNumeroDaStringa(&L3.buffer[0], pos_marker[0], indice_pkt_lenght);
  giri_orari = EstraiNumeroDaStringa(&L3.buffer[0], pos_marker[3], giri_orari_lenght);
  giri_antiorari = EstraiNumeroDaStringa(&L3.buffer[0], pos_marker[4], giri_antiorari_lenght);
  
  RefreshWD();
  
  array[0] = indice_pkt;
  array[1] = giri_orari;
  array[2] = giri_antiorari;
}
*/

