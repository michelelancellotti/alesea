/**
******************************************************************************************
* File          	: communication_SM.c
* Descrizione        	: Gestione macchina a stati comunicazione (livello applicativo):
*                         - localizzazione + comunicazione WiFi
*                         - localizzazione GPS + comunicazione modem
******************************************************************************************
*
* COPYRIGHT(c) 2024 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************************
*/

#include "communication_SM.h"
#include "wifi_SM.h"
#include "modem_SM.h"
#include "LSM6DSL_SM.h"
#include "main.h"
#include "Gestione_dato.h"
#include "EEPROM.h"
#include "funzioni.h"
#include "ndef.h"
#include "Gestione_stato.h"
#include "Gestione_batteria.h"




Communication_State_t communication_state = COMM_IDLE_S;
Communication_State_t next_communication_state = COMM_IDLE_S;
uint8_t ON_communication = 0;

uint8_t FlagWakeupDaRTC;
uint8_t FactoryResetPendente;
uint8_t DisattivazionePendente;




/* Funzioni locali. */




void Start_MacchinaStati_Comunicazione(void)
{
  if(communication_state == COMM_IDLE_S)
    next_communication_state = COMM_START_S;
}


Communication_State_t Get_Stato_Comunicazione(void)
{
  return communication_state;
}


//TODO: fare test automa con HW3.x
/* Automa comunicazione (livello applicativo). */
void MacchinaStati_Comunicazione(void)
{
  uint8_t result;
  
  communication_state = next_communication_state;       //assegnazione prossimo stato automa
  switch(communication_state)
  {
    case COMM_START_S:
      //--- Inizializzazione comunicazione (comune a WiFi e Modem) ---      
      ON_communication = 1;     //comunicazione in corso
      
      if(VersioneHW == VERSIONE_HW_4_X)
        HAL_GPIO_WritePin(BOOST_EN_HW4_GPIO_Port, BOOST_EN_HW4_Pin, GPIO_PIN_SET);      //abilito switching
      
      Diagnostica.ContaStatoIn++;
      EEPROM_WriteByte(EEPROM_CONTA_STATO_IN, Diagnostica.ContaStatoIn);
      IncrementoIndiciTx();
      SetTxInterruptSource();
      Dati.AleseaMeasure.CaricaResidua = CalcolaCaricaResidua(Consumo_uA_h_tot);            
      GetStopModeDuration();
      
      TriggerOrientamento = 1;
      
      next_communication_state = COMM_GET_ORIENTAMENTO_S;    
      break;
      
      
    case COMM_GET_ORIENTAMENTO_S:
      //--- Attesa rilievo orientamento completato (comune a WiFi e Modem) ---
      if(TriggerOrientamento == 1)        
        next_communication_state = COMM_GET_ORIENTAMENTO_S;     //non avvio comunicazione fino a che non ho terminato il get orientamento 
      else
      {
        if((WiFiParam.WiFiScanEnable==WIFI_ENABLE_ON) && (VersioneHW==VERSIONE_HW_4_X))
        {
          Start_MacchinaStati_WiFi();
          next_communication_state = COMM_WIFI_S;
        }
        else
        {
          Start_MacchinaStati_Modem();
          next_communication_state = COMM_MODEM_S;
        }
      }
      break;

      
    case COMM_WIFI_S:
      //--- Localizzazione + comunicazione WiFi ---
      result = MacchinaStati_WiFi();
      if(result != WIFI_SM_IN_PROGRESS)      
      {
        //Automa WiFi concluso
        if((result==WIFI_SM_ENDED_ERROR) || (result==WIFI_SM_ENDED_NO_COMM))
        {
          Start_MacchinaStati_Modem();
          next_communication_state = COMM_MODEM_S;
        }
        else
          next_communication_state = COMM_END_S;
      }
      break;
      
      
    case COMM_MODEM_S:
      //--- Localizzazione + comunicazione modem ---
      result = MacchinaStati_Modem();
      if(result == MODEM_SM_ENDED)
        next_communication_state = COMM_END_S;
      break;

      
    case COMM_END_S:
      //--- Chiusura comunicazione (comune a WiFi e Modem) ---        
      if(VersioneHW == VERSIONE_HW_4_X) 
      {
        HAL_GPIO_WritePin(MDM_ON_OFF_HW4_GPIO_Port, MDM_ON_OFF_HW4_Pin, GPIO_PIN_RESET);        //disalimento modem
        HAL_GPIO_WritePin(WIFI_EN_HW4_GPIO_Port, WIFI_EN_HW4_Pin, GPIO_PIN_RESET);              //disabilito modulo WiFi
        HAL_GPIO_WritePin(WIFI_ON_OFF_HW4_GPIO_Port, WIFI_ON_OFF_HW4_Pin, GPIO_PIN_RESET);      //disalimento modulo WiFi  
        HAL_GPIO_WritePin(GPS_AMPLI_HW4_GPIO_Port, GPS_AMPLI_HW4_Pin, GPIO_PIN_RESET);	        //spengo ampli di sicurezza HW4
        HAL_GPIO_WritePin(BOOST_EN_HW4_GPIO_Port, BOOST_EN_HW4_Pin, GPIO_PIN_RESET);            //spengo switching        
      }
      else
        HAL_GPIO_WritePin(GPS_AMPLI_HW3_GPIO_Port, GPS_AMPLI_HW3_Pin, GPIO_PIN_RESET);	        //spengo ampli di sicurezza per HW3      
      
      Diagnostica.ContaStatoOut++;
      EEPROM_WriteByte(EEPROM_CONTA_STATO_OUT, Diagnostica.ContaStatoOut);
      //Scrittura chiave aggiornamento presente + reset uC
      if(FlagUpgradeSuccess)
      {
        FlagUpgradeSuccess = 0;        
        EEPROM_WriteByte(EEPROM_ADD_CHIAVE, UPD_KEY_PRESENTE);
        EEPROM_WriteWord(EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter);
        EEPROM_WriteWord(EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter);
        
        HAL_NVIC_SystemReset();        
      }
      //Se ricevuto comando di reset da remoto, eseguo reset uC
      if(FlagMicroReset)
      {
        FlagMicroReset = 0;
        EEPROM_WriteWord(EEPROM_MODEM_ACTIVITY, Diagnostica.ModemActivityCounter);
        EEPROM_WriteWord(EEPROM_MICRO_ACTIVITY, Diagnostica.MicroActivityCounter);  
        
        HAL_NVIC_SystemReset();
      }
      
      //Imposto futuro risveglio: se mi ero svegliato per rotazione allora imposto il risveglio a 2h o 1h a seconda del numero di eventi occorsi
      /////////////////////////////////// ALGORITMI SPIN-SHOCK-MOVIMENTO ////////////////////////////////////////////      
      if(FlagCommOnInSpinSession)
      {
        ImpostaRisveglio(Dati.SpinEvent.SpinSchInterval);
      }
      else if(FlagCommOnInShockSession)
      {
        ImpostaRisveglio(Dati.ShockEvent.ShockSchInterval);
      }
      else if(FlagCommOnInMovementSession)
      {
        ImpostaRisveglio(Dati.MovementEvent.MoveSchInterval);       //modificare impostazione schedulazione se si vuole un tx della chiusura movimento pi� ravvicinata es 2h)
      }
      else 
      {
        if(Dati.AleseaMeasure.ProfiloTemperaturaOn == PROFILO_TEMPERATURA_ON)
        {
          SchIntervalCounter = Dati.ActivationTiming.SchedulingInterval/TEMPERATURE_SCH_INTERVAL;
          if(SchIntervalCounter > 1)        ImpostaRisveglio((uint32_t)TEMPERATURE_SCH_INTERVAL); //se il rapporto fra la schedulazione e 1 ora per lettura temp � > 1 imposto la sveglia per temp
          else                              ImpostaRisveglio((uint32_t)Dati.ActivationTiming.SchedulingInterval);
          ArrayTemperaturaIndex = 0;        //ho comunicato quindi azzero indice in modo che la prima lettura dopo la comunicazione popoli il primo elemento dell'array
        }
        else
        {
          ImpostaRisveglio(Dati.ActivationTiming.SchedulingInterval);
        }
      }
      //Se sono in collaudo forzo in ogni caso il risveglio al tempo previsto per il collaudo     02/05/2022      MLN7
      if(Dati.StatoAlesea == ALESEA_COLLAUDO)     ImpostaRisveglio(Dati.ActivationTiming.SchedulingInterval);      
      /////////////////////////////////// end ALGORITMI SPIN-SHOCK-MOVIMENTO ////////////////////////////////////////////
      
      if(FactoryResetPendente)
      {	
        ResetParametriFabbrica();
        ResettaDiagnostica();
        DisattivaAlesea();
        NdefCompile(Dati.StatoAlesea);
        GestioneTimeoutAttivazione();
        FactoryResetPendente = 0;
      }
      if(DisattivazionePendente)
      {
        DisattivaAlesea();
        NdefCompile(Dati.StatoAlesea);
        GestioneTimeoutAttivazione();
        DisattivazionePendente = 0;
      }
      
      //Pulizia flag
      FlagCommOnInSpinSession = 0;
      FlagCommOnInShockSession = 0;
      FlagCommOnInMovementSession = 0;
      FlagCommOnInSpinSessionError = 0;
      FlagWakeupDaRTC = 0;
      ON_communication = 0;                             //comunicazione conclusa (ex FlagGetStatusOn)
      Dati.NetworkPerformance.InputVariation = '0';     //pulisco il campo in che conteneva il MsgType eventualmente ricevuto
      
      //Sessioni inerziali
      if(Dati.SpinEvent.SpinSession == 2)               //azzero solo se la sessione � conclusa
      {
        Dati.SpinEvent.SpinSession = 0;
        ContaInterruptRotazione = 0;
        TotGiriGyr = 0;
        TotGiriAcc = 0;
        Dati.SpinEvent.SpinMag = 0; 
        lsm6dsl_fail_count_dbg = 0;
      }
      if(Dati.MovementEvent.MovementSession == 2)       //azzero solo se la sessione � conclusa
      {
        SogliaAccelerometro = Dati.MovementEvent.SogliaMovimento;//SOGLIA_MOV; //se trasmetto per RTC e non per movimentazioni riporto la soglia al default per rilevare movimento
        SetLSM6DSL(ACC_SOGLIA);//SogliaAccelerometroSet(SogliaAccelerometro);
        Dati.MovementEvent.MovementSession = 0;
      }
      if(Dati.ShockEvent.ShockSession == 2)             //azzero solo se la sessione � conclusa
      {
        Dati.ShockEvent.ShockSession = 0;
      }
      Dati.SpinEvent.SpinFreq = 0;
      Dati.SpinEvent.SpinStartStopCounter = 0;
      
      RegisterWRFailureMap = 0;
      
      UpgradeDataRX.FIELD.type = UPG_NULL;
      CheckStateTransition();	      
      //Azzera calendar
      ResetCalendar();
      
      next_communication_state = COMM_IDLE_S;
      break;
      
    case COMM_IDLE_S:
      //--- Macchina a stati libera: consenso per ingresso in STOP mode (comune a WiFi e Modem) ---
      ON_communication = 0;                             //comunicazione conclusa (ex FlagGetStatusOn)
      next_communication_state = COMM_IDLE_S;
      break;
      
    default: next_communication_state=COMM_IDLE_S; break;
  }
}

