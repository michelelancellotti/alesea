/**
******************************************************************************************
* File          	: wifi_SM.c
* Descrizione        	: Gestione macchina a stati WiFi (livello applicativo):
*                         - localizzazione WiFi
*                         - comunicazione WiFi
******************************************************************************************
*
* COPYRIGHT(c) 2024 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************************
*/

#include "wifi_SM.h"
#include "wifi.h"
#include "main.h"
#include "usart.h"
#include "Gestione_dato.h"
#include "Gestione_batteria.h"




GPTIMER wifi_powerOn_timer;

WiFi_State_t wifi_state = WIFI_OFF_S;
WiFi_State_t next_wifi_state = WIFI_OFF_S;
uint8_t wifi_driver_active = 0;
WiFiTrigger_t operazione_wifi;
uint8_t wifi_error=0, wifi_bypass_com=0;

uint8_t NumberTxWiFi = 0;
AccessPointScan_t AccessPointScan;
MAC_Address_t MAC_Address;




/* Funzioni locali. */



WiFi_State_t Get_Stato_WiFi_Appli(void)
{
  return wifi_state;
}



void Start_MacchinaStati_WiFi(void)
{
  wifi_driver_active = 0;
  next_wifi_state = WIFI_ON_S;
}


/* Automa WiFi (livello applicativo). 
*
*@return:
*WIFI_SM_IN_PROGRESS (esecuzione automa in corso)
*WIFI_SM_ENDED (esecuzione automa conclusa)
*WIFI_SM_ENDED_ERROR (esecuzione automa conclusa: errore imprevisto o errore di rete)
*WIFI_SM_ENDED_NO_COMM (esecuzione automa conclusa: comunicazione WiFi non eseguita)
*/
uint8_t MacchinaStati_WiFi(void)
{
  static WiFiAppli_t wifi_driver_status = WA_OK;
  static uint8_t first_scan_after_reset = 1;
  
  if((wifi_driver_active==1) && (wifi_state!=WIFI_OFF_S) && (wifi_state<WIFI_STATE_NUM))
  {
    //Verifico lo stato del driver (low-layer) solo nel caso siano in corso dei trigger al driver stesso
    wifi_driver_status = wifi_GET_Status(operazione_wifi);
    if(wifi_driver_status == WA_BUSY)
      return WIFI_SM_IN_PROGRESS;
    
    if((wifi_driver_status==WA_UNP_ERROR) || (wifi_driver_status==WA_NETWORK_ERROR))
    {
      //WA_UNP_ERROR --> errore inatteso
      //WA_NETWORK_ERROR --> rete WiFi assente o errore connessione AP/comunicazione socket
      wifi_error = 1;
      next_wifi_state = WIFI_DISCONNETTI_RETE_S;
    }
  }
  
  wifi_state = next_wifi_state;         //assegnazione prossimo stato automa (e.g. dopo eventuale completamento di un trigger) 
  switch(wifi_state)
  {
    //< ----------------- ACCENSIONE WIFI ----------------- >
    case WIFI_ON_S:      
      HAL_GPIO_WritePin(WIFI_ON_OFF_HW4_GPIO_Port, WIFI_ON_OFF_HW4_Pin, GPIO_PIN_SET);  //alimento modulo WiFi
      HAL_GPIO_WritePin(WIFI_EN_HW4_GPIO_Port, WIFI_EN_HW4_Pin, GPIO_PIN_SET);          //abilito modulo WiFi: per consentire reset eventuali errori seriale con modulo attivo (stato WIFI_WAIT_ON_S)
      GPTIMER_init(&wifi_powerOn_timer, WIFI_POWER_ON_WAIT_TIME);
      GPTIMER_stop(&wifi_powerOn_timer);
      FlagConsumoWifiOn = 1;
      next_wifi_state = WIFI_WAIT_ON_S;
      break;

      
    case WIFI_WAIT_ON_S:
      MX_USART2_UART_Init();
      wifi_Init(WF_MOD_ESP_WROOM_02);   //init libreria WiFi
      Init_SCI(SERIALE_WIFI);
      
      //Attesa dopo alimentazione modulo
      if(!wifi_powerOn_timer.enable)
      {
        GPTIMER_stop(&wifi_powerOn_timer);
        GPTIMER_reTrigger(&wifi_powerOn_timer);         //start wait time
      }
      else if(GPTIMER_isTimedOut(&wifi_powerOn_timer))
      {
        GPTIMER_stop(&wifi_powerOn_timer);
        next_wifi_state = WIFI_RESET_S;
      }
      break;     
      
      
    case WIFI_RESET_S:
      wifi_driver_active = 1;
      operazione_wifi = WF_TRG_RESET;
      wifi_Trigger(operazione_wifi, NULL, NULL);      
      next_wifi_state = WIFI_SCAN_S; 
      break;
    //----> end ACCENSIONE WIFI

      
    //< ----------------- LOCALIZZAZIONE WIFI ----------------- >
    case WIFI_SCAN_S:
      operazione_wifi = WF_TRG_SCAN;
      wifi_Trigger(operazione_wifi, NULL, NULL);      
      next_wifi_state = WIFI_PARSE_S; 
      break;

      
    case WIFI_PARSE_S:
      //GET lista AP scansionati
      wifi_GET_NumAccessPoint(&AccessPointScan.NumAccessPoint);
      if(AccessPointScan.NumAccessPoint > 0) 
        AccessPointScan.Executed = 1;
      else 
        AccessPointScan.Executed = 0;
      wifi_GET_AccessPoint(AccessPointScan.String, &AccessPointScan.StringDim);
      //GET MAC address modulo
      if(Dati.StatoAlesea==ALESEA_COLLAUDO || first_scan_after_reset)
      {        
        wifi_GET_MAC_address(MAC_Address.String, &MAC_Address.StringDim);
        MAC_Address.String[sizeof(MAC_Address.String)-1] = 0x00;                //add terminatore
        MAC_Address.Available = 1;
        first_scan_after_reset = 0;
      }
      
      if((WiFiParam.WiFiComEnable==WIFI_ENABLE_ON) && (NumberTxWiFi<WiFiParam.WiFiNumTxSwitchModem))
      {
        NumberTxWiFi++;
        next_wifi_state = WIFI_CONNETTI_RETE_S;
      }
      else
      {
        wifi_bypass_com = 1;
        NumberTxWiFi = 0;        
        next_wifi_state = WIFI_OFF_S;
      }
      break;
    //----> end LOCALIZZAZIONE WIFI

      
    //< ----------------- COMUNICAZIONE WIFI ----------------- >
    case WIFI_CONNETTI_RETE_S:
      operazione_wifi = WF_TRG_CONNECT;
      wifi_Trigger(operazione_wifi, NULL, NULL);      
      next_wifi_state = WIFI_INVIA_TCP_S;
      break;

      
    case WIFI_INVIA_TCP_S:
      //Aggiornamento dati WiFi
      ServerType = SRV_TCP_SOCKET;
      wifi_GET_RSSI(&Dati.WiFiPerformance.RSSI);
      wifi_GET_Diagnostic(&Dati.WiFiPerformance.RegRete, &Dati.WiFiPerformance.LastError);      
      //Comunicazione
      GeneraStringa(&Dati, 0, PKT_SRC_WIFI);      
      if(wifi_Trigger(WF_TRG_TCP_SEND, DatoJson, DatoJsonDim) == WA_TRIG_OK)
        operazione_wifi = WF_TRG_TCP_SEND;
      else        
        wifi_error = 1; //segnalo errore perch� la comunicazione non � avvenuta (errore formale parametri)
      
      next_wifi_state = WIFI_DISCONNETTI_RETE_S;
      break;

      
    case WIFI_DISCONNETTI_RETE_S:
      operazione_wifi = WF_TRG_DISCONNECT;
      wifi_Trigger(operazione_wifi, NULL, NULL);
      next_wifi_state = WIFI_OFF_S;
      break;
    //----> end COMUNICAZIONE WIFI

      
    //< ----------------- SPEGNIMENTO WIFI ----------------- >
    case WIFI_OFF_S:
      HAL_GPIO_WritePin(WIFI_EN_HW4_GPIO_Port, WIFI_EN_HW4_Pin, GPIO_PIN_RESET);                //disabilito modulo WiFi
      HAL_GPIO_WritePin(WIFI_ON_OFF_HW4_GPIO_Port, WIFI_ON_OFF_HW4_Pin, GPIO_PIN_RESET);        //disalimento modulo WiFi  
      FlagConsumoWifiOn = 0;
      wifi_driver_active = 0;
      next_wifi_state = WIFI_OFF_S;
      break;
    //----> end SPEGNIMENTO WIFI

      
    default: next_wifi_state=WIFI_OFF_S; break;
  }
  
  if((wifi_driver_active==0) && (wifi_state==WIFI_OFF_S))
  {
    if(wifi_bypass_com)
    {
      wifi_bypass_com = 0;
      return WIFI_SM_ENDED_NO_COMM;     //esecuzione automa conclusa: comunicazione WiFi non eseguita --> prioritario
    }
    else if(wifi_error)
    {
      wifi_error = 0;
      return WIFI_SM_ENDED_ERROR;       //esecuzione automa conclusa: errore
    }
    else
      return WIFI_SM_ENDED;             //esecuzione automa conclusa
  }
  else
    return WIFI_SM_IN_PROGRESS;         //esecuzione automa in corso
}

