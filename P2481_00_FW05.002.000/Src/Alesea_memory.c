#include "Alesea_memory.h"
#include "Gestione_dato.h"
#include "funzioni.h"
#include "ndef.h"
#include "Gestione_batteria.h"
#include "LSM6DSL_SM.h"
#include "EEPROM.h"
#include "crc.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>
 



void ReadStruct(uint8_t *pStruct, uint32_t add, uint16_t size)
{
  uint8_t temp;
  for (uint8_t k=0; k<size; k++)
  {
    temp = EEPROM_ReadByte(add + k);
    ((uint8_t *)pStruct)[k] = temp;
  }    
  return;
}


void WriteStruct(uint8_t *pStruct, uint32_t add, uint16_t size)
{
  uint8_t temp;
  for (uint8_t k=0; k<size; k++)
  {
    temp = ((uint8_t *)pStruct)[k];
    EEPROM_WriteByte(add + k, temp);
  }    
  return;
}


uint8_t SalvaVariabileCritica(VariabileCritica_t* argStruttura, uint8_t argBanco)
{
  uint32_t crcRead1;
  uint32_t crcRead2;
  uint32_t crcTemp;
 
  //salvo in EEPROM la variabile verificandone prima la dimensione

  crcTemp = CalcolaCRC((*argStruttura->var32));
  if(argBanco == BANCO_1)    EEPROM_WriteWord(argStruttura->varEepromAddr1, *(argStruttura->var32));
  else if(argBanco == BANCO_2)    EEPROM_WriteWord(argStruttura->varEepromAddr2, *(argStruttura->var32));
  else {EEPROM_WriteWord(argStruttura->varEepromAddr1, *(argStruttura->var32));        EEPROM_WriteWord(argStruttura->varEepromAddr2, *(argStruttura->var32));}
  
  EEPROM_WriteWord(argStruttura->crcEepromAddr1, crcTemp);        //salvo CRC banco 1
  EEPROM_WriteWord(argStruttura->crcEepromAddr2, crcTemp);        //salvo CRC banco 2
  
  crcRead1 = EEPROM_ReadWord(argStruttura->crcEepromAddr1);       //verifico salvataggio
  crcRead2 = EEPROM_ReadWord(argStruttura->crcEepromAddr2);       //verifico salvataggio
  if( (crcRead1 == crcTemp) && (crcRead2 == crcTemp) )   return 1;
  else return 0;        //TODO: errore in scrittura: cosa fare??
}



uint32_t LeggiVariabileCritica(VariabileCritica_t* argStruttura)
{
  uint32_t crcRead;
  uint32_t valRead;
  uint32_t crcTemp;
  valRead = EEPROM_ReadWord(argStruttura->varEepromAddr1);
  
  crcRead = EEPROM_ReadWord(argStruttura->crcEepromAddr1);
  
  crcTemp = CalcolaCRC(valRead);
  
  if( crcRead == crcTemp ) //crc ok -> copio valore da eeprom
  {
    *argStruttura->var32 = valRead;

    return 1; 
  }
  else  //crc non ok
  {
    GestioneAllineamentoBancoEeprom(argStruttura); 
    return 0;
  }
}

void GestioneAllineamentoBancoEeprom(VariabileCritica_t* argStruttura)
{
  uint32_t crcRead;
  uint32_t valRead;
  uint32_t crcTemp;

  valRead = EEPROM_ReadWord(argStruttura->varEepromAddr2);                                  //leggo banco 2 
  crcRead = EEPROM_ReadWord(argStruttura->crcEepromAddr2);                                  //leggo crc
  crcTemp = CalcolaCRC(valRead);                                                            //calcolo crc
  if(crcRead == crcTemp)                                                                    //se crc banco2 ok
  {
    *argStruttura->var32 = valRead;                                                         //leggo il valore da eeprom banco 2
    SalvaVariabileCritica(argStruttura, BANCO_1);                                           //salvo in banco 1
  }
  else                                                                                      //crc banco2 no ok -> det default
  {
    *argStruttura->var32 = argStruttura->defaultVal;                                        //ricarico valore di default
    SalvaVariabileCritica(argStruttura, BANCO_1_2);                                         //ripristino banco 1 e 2                                       
  } 
  
}



uint32_t CalcolaCRC(uint32_t val)
{
  uint32_t crcTemp;
  uint32_t bufferTemp[1];
  bufferTemp[0] = val;
  crcTemp = HAL_CRC_Calculate(&hcrc, (uint32_t *)&bufferTemp[0], 1);
  bufferTemp[0] = 0;
  return crcTemp;
}



