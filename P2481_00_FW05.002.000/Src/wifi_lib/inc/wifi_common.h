/**
******************************************************************************
* File          	: wifi_common.h
* Versione libreria	: 1.01
* Descrizione        	: Invio comandi AT & parsing risposte.
******************************************************************************
*
* COPYRIGHT(c) 2021 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WIFI_COMMON_H
#define __WIFI_COMMON_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "at_common.h"
#include "softTimer.h"  
  
  
/* Defines */
#define WF_MOD_MS_PER_TICKS     ((uint16_t)100)         //[ms], moltiplicatore timeout risposte a comandi AT

#define WF_MOD_NO_TAILER        0xFF	//Invio comandi senza terminatore
  
#define MAX_AP                  4       //Numero MAX access-point scansionabili
#define MAX_AP_STRING           32      //Dimensione MAX stringa identificativa access-point
#define MAC_ADDRESS_SIZE        17      //Dimensione stringa MAC address [B]


#define TCP_SERVER_HOSTNAME     "\"wifi-tcp-iotc-devicemessage.centralus.cloudapp.azure.com\""          //indirizzo IP/DNS server TCP
                                //"\"52.173.75.244\""   
#define TCP_SERVER_PORT         "64000"                                                                 //porta server TCP
  
/* Types & global variables */

typedef enum {
  WF_MOD_ESP_WROOM_02 = 0x00,
  WF_MOD_NUM	
} Wf_Mod_t;


/* Global functions */  
extern void (*wf_mod_Write_seriale)(uint8_t id_seriale, uint8_t *src, uint16_t size);
extern void (*wf_mod_Drive_reset)(uint8_t ON);
extern void wf_mod_Init_AT(void);
extern ErrorCode_t wf_mod_Read_Response(const uint8_t timeout_ticks, const uint8_t maxRetries, uint8_t* resp, uint16_t* respOffset, uint8_t* error);
extern void wf_mod_Push_Cmd(uint8_t* cmd, uint8_t cmdCode, const uint16_t maxRetries, uint8_t modulo);


/* Global variables */
extern uint8_t wf_mod_id_Seriale;
extern Wf_Mod_t WiFiType;
#ifdef WIFI_UART_LOG
extern uint8_t dbg_wifi[100];
extern uint16_t dbg_wifi_idx;
#endif
extern uint8_t wf_mod_buff_Risposta[MOD_MAX_BUFFER_RX_UNBOUND];
extern uint16_t wf_mod_buff_Risposta_idx;
extern Mod_CmdAttesa_t wf_mod_cmdAttesa;
  
#ifdef __cplusplus
}
#endif
#endif /*__WIFI_COMMON_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
