/**
  ******************************************************************************
  * File          	: esp_wroom_02.h
  * Versione libreria	: 1.01
  * Descrizione        	: Supporto modulo EspressIF ESP-WROOM-02.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2021 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ESP_WROOM_02_H
#define __ESP_WROOM_02_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "esp_wroom_02_common.h"


/* Defines */  
#define ESP8226_RESET_WAIT_TIME         100     //[ms], tempo attesa dopo RESET
#define ESP2866_NOP_TIMEOUT		5000	//[ms], tempo attesa prima di dare comandi AT   

	 
/* Types & global variables */
extern ESP8266_State_t esp8266_status;
extern uint8_t esp8266_last_error;

/* Global functions */
extern void init_esp8266(void);
extern void esp8266_Automa(const uint32_t PERIODO);
extern void esp8266_ProcessaByte(uint8_t data);
extern ESP8266_State_t get_esp8266_status(void);

/* Global variables */


#ifdef __cplusplus
}
#endif
#endif /*__ESP_WROOM_02_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
