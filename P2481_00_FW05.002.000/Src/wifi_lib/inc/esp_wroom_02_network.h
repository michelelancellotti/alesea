/**
  ******************************************************************************
  * File          	: esp_wroom_02_network.h
  * Versione libreria	: 1.01
  * Descrizione        	: Gestione network.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2021 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
  
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ESP_WROOM_02_NETWORK_H
#define __ESP_WROOM_02_NETWORK_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "esp_wroom_02_common.h"   


/* Defines */
#define WIFI_MODE_STATION               "1"
#define WIFI_MODE_SOFT_AP               "2"
#define WIFI_MODE_SOFT_AP_AND_STATION   "3"

#define DHCP_OFF                        "0"
#define DHCP_ON                         "1"
   
#define TCP_MODE_NORMAL_MODE            "0"
#define TCP_MODE_TRANSPARENT_MODE       "1"
   
#define TCP_CONNECTION_SINGLE           "0"
#define TCP_CONNECTION_MULTIPLE         "1"
   
#define TCP_KEEP_ALIVE                  "120"           //tempo keep-alive connessione TCP [s] --> [0-7200], 0=disabilita keep-alive
#define TCP_OPEN_SOCKET_TIMEOUT         60              //timeout apertura socket TCP [s]
#define TCP_SEND_SOCKET_TIMEOUT         10              //timeout scrittura socket TCP [s]
   
   
/* Types & global variables */


/* Global functions */
extern void ESP8266_SCAN_func(const uint32_t PERIODO);
extern void ESP8266_CONNECT_func(const uint32_t PERIODO);
extern void ESP8266_TCP_SEND_func(const uint32_t PERIODO);
extern void ESP8266_DISCONNECT_func(const uint32_t PERIODO);
   

/* Global variables */
extern uint32_t esp8266_tempo_Scansione;
extern uint32_t esp8266_tempo_Connessione;


#ifdef __cplusplus
}
#endif
#endif /*__ESP_WROOM_02_NETWORK_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
