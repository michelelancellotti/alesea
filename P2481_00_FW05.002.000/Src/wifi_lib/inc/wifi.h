/**
******************************************************************************
* File          	: wifi.h
* Versione libreria	: 1.01
* Descrizione        	: Supporto modulo WiFi (livello applicativo).
******************************************************************************
*
* COPYRIGHT(c) 2021 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#ifndef _WIFI_H
#define _WIFI_H


/* Includes */
#include "mTypes.h"
#include "wifi_common.h"


/* Define & Typedef */

//########## Define & Typedef Applicazione ##########

#define WIFI_RST_PORT		        WIFI_EN_GPIO_Port
#define WIFI_RST_PIN		        WIFI_EN_Pin

#define WIFI_FULL_AP_STRING_SIZE        ((MAX_AP_STRING*MAX_AP)+6)      //+6 per includere caratteri separatori + terminatore stringa
#define WIFI_MAC_ADDRESS_SIZE           (MAC_ADDRESS_SIZE+1)            //+1 per includere terminatore di stringa


typedef enum {
  WF_TRG_RESET          = 0,
  WF_TRG_SCAN           = 1,
  WF_TRG_CONNECT        = 2,
  WF_TRG_TCP_SEND       = 3,
  WF_TRG_DISCONNECT     = 4,
} WiFiTrigger_t;

typedef enum {
  WA_BUSY 		= 0,	//Automa WiFi impegnato (operazione in corso) 
  WA_OK 		= 1,	//Operazione completata: nessun errore
  WA_ERROR 		= 2,	//Operazione completata: errore gestito
  WA_UNP_ERROR 		= 3,	//Errore imprevisto automa WiFi
  WA_INIT_ERROR		= 4,	//Errore inizializzazione libreria WiFi
  WA_TRIG_OK		= 5,	//Trigger operazione OK
  WA_NETWORK_ERROR      = 6,    //Errore connessione rete 
} WiFiAppli_t;

/* Diagnostica WiFi (estesa)
*  N.B.: attenzione a indici enum --> gestione posizionale
*/
typedef enum {
  W_DGN_STATO_AUTOMA            = 0,    //stato automa (livello driver)
  W_DNG_NUM                     //[1]
} WiFiDiagnostic_t;


/* Funzioni globali */
extern ErrorCode_t wifi_Init(Wf_Mod_t modello);
extern void (*wifi_Automa)(const uint32_t PERIODO);
extern void (*wifi_ProcessaByte)(uint8_t data);

extern WiFiAppli_t wifi_GET_NumAccessPoint(uint8_t* resp);
extern WiFiAppli_t wifi_GET_AccessPoint(uint8_t* resp, uint16_t* resp_size);
extern WiFiAppli_t wifi_GET_RSSI(uint8_t* resp);
extern WiFiAppli_t wifi_GET_Diagnostic(uint16_t* reg_status, uint8_t* last_error);
extern WiFiAppli_t wifi_GET_ExtDiagnostic(WiFiDiagnostic_t type, 
                                          uint8_t* resp);
extern WiFiAppli_t wifi_GET_MAC_address(uint8_t* resp, uint16_t* resp_size);

extern WiFiAppli_t wifi_GET_Status(WiFiTrigger_t operazione);
extern WiFiAppli_t wifi_Trigger(WiFiTrigger_t trigger, uint8_t* data, uint16_t data_size);


#endif	/* End _WIFI_H */
