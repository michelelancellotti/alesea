/**
******************************************************************************
* File          	: wifi_debug.c
* Versione libreria	: 1.01
* Descrizione        	: Debug libreria WiFi.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "wifi_debug.h"
#include "wifi.h"
#include "main.h"
#include "Globals.h"


#ifdef ENB_COMMUNICATION_DEBUG
extern uint8_t VersioneHW;
static char wf_debug_msg[32] = "AT+CIPMODE?";
static uint8_t wf_cmd_debug_trigger = 0;
static WiFiTrigger_t wf_dbg_trigger;
WiFiAppli_t wf_ec;


/* Funzioni locali. */


/* Trigger manuali: DEBUG modulo WiFi. */
void wifi_debug_trigger(void)
{
  static uint16_t wf_msg_num;
  int16_t cnt;

  
  wf_ec = wifi_GET_Status(wf_dbg_trigger);
  
  if(wf_cmd_debug_trigger == 1)
  {
    /* Reset buffer debug */
#ifdef WIFI_UART_LOG
    memset(dbg_wifi, 0x00, sizeof(dbg_wifi));
    dbg_wifi_idx = 0;
#endif
    wf_cmd_debug_trigger = 0;
  }
  else if(wf_cmd_debug_trigger == 2)
  {
    /* Test modulo WiFi ON */
    if(VersioneHW == VERSIONE_HW_4_X)
    {
      HAL_GPIO_WritePin(BOOST_EN_HW4_GPIO_Port, BOOST_EN_HW4_Pin, GPIO_PIN_SET); //alimento switching
      HAL_Delay(2000);
      HAL_GPIO_WritePin(WIFI_ON_OFF_HW4_GPIO_Port, WIFI_ON_OFF_HW4_Pin, GPIO_PIN_SET); //alimento il modulo
      HAL_Delay(2000);
    }
    wf_dbg_trigger = WF_TRG_RESET;
    wifi_Trigger(wf_dbg_trigger, NULL, NULL);
    wf_cmd_debug_trigger = 0;
  }
  else if(wf_cmd_debug_trigger == 3)
  {
    /* Test reset modulo WiFi */
    wf_dbg_trigger = WF_TRG_RESET;
    wifi_Trigger(wf_dbg_trigger, NULL, NULL);
    wf_cmd_debug_trigger = 0;
  }  
  else if(wf_cmd_debug_trigger == 4)
  {
    /* Invio comando generico */
    for(cnt=0; cnt<sizeof(wf_debug_msg) && wf_debug_msg[cnt]!=0x00; cnt++);      //Cerca fine stringa
    if(cnt >= sizeof(wf_debug_msg))
      cnt = sizeof(wf_debug_msg) - 3;      
    wf_debug_msg[cnt++] = CR_CHAR;
    wf_debug_msg[cnt++] = LF_CHAR;
    wf_mod_Write_seriale(wf_mod_id_Seriale, (uint8_t*)wf_debug_msg, cnt);
    wf_cmd_debug_trigger = 0;
  }
  else if(wf_cmd_debug_trigger == 5)
  {
    /* Test scansione reti WiFi */
    wf_dbg_trigger = WF_TRG_SCAN;
    wifi_Trigger(wf_dbg_trigger, NULL, NULL);
    wf_cmd_debug_trigger = 0;
  }
  else if(wf_cmd_debug_trigger == 6)
  {
    /* Test connessione a server TCP. */
    wf_dbg_trigger = WF_TRG_CONNECT;
    wifi_Trigger(wf_dbg_trigger, NULL, NULL);
    wf_cmd_debug_trigger = 0;
  }
  else if(wf_cmd_debug_trigger == 7)
  {
    /* Test scrittura dati su socket TCP. */
    memset(wf_debug_msg, 0x00, sizeof(wf_debug_msg));
    cnt = snprintf(wf_debug_msg, sizeof(wf_debug_msg), "{\"d0\":%d,\"d1\":%d}", ++wf_msg_num, 20);
    if(cnt > 0)
    {
      wf_dbg_trigger = WF_TRG_TCP_SEND;
      wifi_Trigger(wf_dbg_trigger, (uint8_t*)wf_debug_msg, cnt);
    }    
    wf_cmd_debug_trigger = 0;
  }
  else if(wf_cmd_debug_trigger == 8)
  {
    /* Test disconnessione da server TCP. */
    wf_dbg_trigger = WF_TRG_DISCONNECT;
    wifi_Trigger(wf_dbg_trigger, NULL, NULL);
    wf_cmd_debug_trigger = 0;
  }
}
#endif
