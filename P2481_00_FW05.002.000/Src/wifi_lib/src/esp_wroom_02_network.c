/**
******************************************************************************
* File          	: esp_wroom_02_network.c
* Versione libreria	: 1.01
* Descrizione        	: Gestione network:
*                         - Scansione reti WiFi disponibili
*                         - Configurazione + connessione a rete WiFi
*                         - Configurazione + apertura socket TCP (client)
*                         - Scrittura dati su socket TCP (client)
*                         - Disconnessione socket TCP / rete WiFi
******************************************************************************
*
* COPYRIGHT(c) 2021 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "esp_wroom_02_network.h"
#include "esp_wroom_02_command.h"




#define WF_FIFO_SCAN_SIZE               3               //N.B: ---- Attenzione a ordine comandi (utilizzo esp8266_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t wf_fifo_Scan[WF_FIFO_SCAN_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+CWMODE_CUR="WIFI_MODE_STATION,         .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_WIFI_MODE},//--> DEFAULT = SoftAP
  {.at_cmd=(uint8_t*)"AT+CWLAPOPT=1,12",                        .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP2866_CMD_SCAN_OPT},
  //SCAN_TIME_MAX = T_scan*n_canali = 360ms(default)*14 = 5040ms
  {.at_cmd=(uint8_t*)"AT+CWLAP",                                .at_resp=(uint8_t*)"+CWLAP:",           .AT_CMD_TYPE=ESP8266_CMD_SCAN,          .AT_RESP_TIMEOUT=ESP8266_6000MS_TO,     .AT_RESP_RETRY=2},  
};

#define WF_FIFO_CONNECT_SIZE            7               //N.B: ---- Attenzione a ordine comandi (utilizzo esp8266_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t wf_fifo_Connect[WF_FIFO_CONNECT_SIZE] = {
  //Il parametro <mode> del comando AT+CWDHCP_CUR deve essere coerente con la modalit� di funzionamento impostata (AT+CWMODE_CUR)
  {.at_cmd=(uint8_t*)"AT+CWDHCP_CUR=1,"DHCP_ON,                 .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_ENABLE_DHCP},//--> DEFAULT = DHCP ON
  {.at_cmd=(uint8_t*)"AT+CWCOUNTRY_CUR=0,\"AU\",1,13",          .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP2866_CMD_COUNTRY_CODE},//--> DEFAULT = 0,"CN",1,13
  {.at_cmd=(uint8_t*)"AT+CWJAP_CUR=",                           .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_CONNECT_AP,            .AT_RESP_TIMEOUT=ESP8266_5000MS_TO,     .AT_RESP_RETRY=6},
  {.at_cmd=(uint8_t*)"AT+CWJAP_CUR?",                           .at_resp=(uint8_t*)"+CWJAP_CUR:",       .AT_CMD_TYPE=ESP8266_CMD_QUERY_AP,              .AT_RESP_TIMEOUT=ESP8266_1000MS_TO},
  {.at_cmd=(uint8_t*)"AT+CIPMODE="TCP_MODE_NORMAL_MODE,         .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_TCP_MODE},//--> DEFAULT = normal mode
  {.at_cmd=(uint8_t*)"AT+CIPMUX="TCP_CONNECTION_SINGLE,         .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_TCP_SINGLE_CONN},//--> DEFAULT = single connection
  {.at_cmd=(uint8_t*)"AT+CIPSTART=\"TCP\","\
    TCP_SERVER_HOSTNAME","TCP_SERVER_PORT","\
    TCP_KEEP_ALIVE,                                             .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_TCP_OPEN_SOCKET,       .AT_RESP_TIMEOUT=ESP8266_1000MS_TO,     .AT_RESP_RETRY=TCP_OPEN_SOCKET_TIMEOUT},  
};

#define WF_FIFO_TCP_SEND_SIZE           2
static Mod_Fifo_Entry_t wf_fifo_TCP_Send[WF_FIFO_TCP_SEND_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+CIPSEND=",             .at_resp=(uint8_t*)">",                 .AT_CMD_TYPE=ESP8266_CMD_TCP_SOCKET_WRITE_LENGTH},
  {.at_cmd=(uint8_t*)"",                        .at_resp=(uint8_t*)"SEND OK",           .AT_CMD_TYPE=ESP8266_CMD_TCP_SOCKET_WRITE_PAYLOAD,              .AT_RESP_TIMEOUT=ESP8266_1000MS_TO,     .AT_RESP_RETRY=TCP_SEND_SOCKET_TIMEOUT}, 
};

#define WF_FIFO_DISCONNECT_SIZE         2               //N.B: ---- Attenzione a ordine comandi (utilizzo esp8266_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t wf_fifo_Disconnect[WF_FIFO_DISCONNECT_SIZE] = {
  {.at_cmd=(uint8_t*)"AT+CIPCLOSE",             .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_TCP_CLOSE_SOCKET,              .AT_RESP_TIMEOUT=ESP8266_1000MS_TO},
  {.at_cmd=(uint8_t*)"AT+CWQAP",                .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_DISCONNECT_AP,                 .AT_RESP_TIMEOUT=ESP8266_1000MS_TO}, 
};


uint32_t esp8266_tempo_Scansione;       //tempo scansione reti WiFi [ms]
uint32_t esp8266_tempo_Connessione;     //tempo connessione a server TCP [ms]
ESP8266_Scan_t esp8266_AP_list;         //lista access-point scansionati
int16_t esp8266_RSSI = 0x8000;          //RSSI access-point (connesso) [dBm]
uint16_t esp8266_size_data_TX;          //dimensione dati da trasmettere su socket TCP [B]

char esp8266_SSID[ESP8266_MAX_SSID+1] = "aleSea";               //SSID rete WiFi
char esp8266_password[ESP8266_MAX_PWD+1] = "Al353a!!";          //password rete WiFi


/* ############################################################
* Local functions
* ############################################################
*/
static uint8_t esp8266_parse_scanning(ESP8266_Scan_t* ap_list);




/* -------------------- Stato: ESP8266_SCAN -------------------- 
* Scansione reti WiFi disponibili.
*/
void ESP8266_SCAN_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
        
  esp8266_tempo_Scansione += PERIODO;
  
  err = esp8266_send_command(wf_fifo_Scan, esp8266_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {
    if(wf_fifo_Scan[esp8266_fifo_index].AT_CMD_TYPE == ESP8266_CMD_SCAN)
    {
      //Parsing risultato scansione
      memset(&esp8266_AP_list, 0x00, sizeof(esp8266_AP_list));
      esp8266_parse_scanning(&esp8266_AP_list);
    }
    
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {
    esp8266_critical_cmd_MASK = 0x0005;
    //--- Gestione eccezioni su errore ---
    //CMD[0]: impostazione modalit� funzionamento: CRITICO
    //CMD[2]: scansione reti: CRITICO
    if(esp8266_critical_cmd(esp8266_fifo_index, esp8266_critical_cmd_MASK))
    {    
      esp8266_reg.BIT.SCAN_ERROR = 1;
      set_esp8266_status(ESP8266_ERR_S);
      return;
    }
    else
      next_command = 1;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&esp8266_fifo_index, WF_FIFO_SCAN_SIZE))
      set_esp8266_status(ESP8266_IDLE_S);
  }
}


/* -------------------- Stato: ESP8266_CONNECT -------------------- 
* Configurazione connessione.
* Connessione a rete WiFi.
* Configurazione socket TCP.
* Apertura socket TCP (client).
*
* Nota: se il modulo fosse gi� connesso alla WLAN, questo si disconette 
* e si riconnette senza dover dare il comando esplicito di disconnessione.
*/
void ESP8266_CONNECT_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  esp8266_tempo_Connessione += PERIODO;
  
  err = esp8266_send_command(wf_fifo_Connect, esp8266_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {
    switch(wf_fifo_Connect[esp8266_fifo_index].AT_CMD_TYPE)
    {
      case ESP8266_CMD_CONNECT_AP:
        esp8266_reg.BIT.WLAN_CONNECTED = 1;
        break;
        
      case ESP8266_CMD_QUERY_AP:
        searchField(wf_mod_buff_Risposta, ',', offset, sizeof(wf_mod_buff_Risposta));
        esp8266_RSSI = (int16_t)(strtol((char const*)searchField_res[3].field, NULL, 10));
        break;
        
      default: break;
    }
      
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {
    esp8266_critical_cmd_MASK = 0x0044;
    //--- Gestione eccezioni su errore ---
    //CMD[2]: connessione a rete WiFi: CRITICO
    //CMD[6]: apertura socket TCP: CRITICO
    if(esp8266_critical_cmd(esp8266_fifo_index, esp8266_critical_cmd_MASK))
    {
      esp8266_reg.BIT.CONNECT_ERROR = 1;
      set_esp8266_status(ESP8266_ERR_S);
      return;
    }
    else
      next_command = 1;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&esp8266_fifo_index, WF_FIFO_CONNECT_SIZE))
      set_esp8266_status(ESP8266_IDLE_S);
  }
}


/* -------------------- Stato: ESP8266_TCP_SEND -------------------- 
* Scrittura dati su socket TCP.
*/
void ESP8266_TCP_SEND_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  esp8266_tempo_Connessione += PERIODO;
  
  err = esp8266_send_command(wf_fifo_TCP_Send, esp8266_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {
    esp8266_reg.BIT.TCP_SEND_ERROR = 1;
    set_esp8266_status(ESP8266_ERR_S);
    return;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&esp8266_fifo_index, WF_FIFO_TCP_SEND_SIZE))
      set_esp8266_status(ESP8266_IDLE_S);
  }
}


/* -------------------- Stato: ESP8266_DISCONNECT -------------------- 
* Chiusura socket TCP (client).
* Disconnessione da rete WiFi.
*/
void ESP8266_DISCONNECT_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;
  
  err = esp8266_send_command(wf_fifo_Disconnect, esp8266_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {
    esp8266_critical_cmd_MASK = 0x0002;
    //--- Gestione eccezioni su errore ---
    //CMD[1]: disconnessione da rete WiFi: CRITICO
    if(esp8266_critical_cmd(esp8266_fifo_index, esp8266_critical_cmd_MASK))
    {
      set_esp8266_status(ESP8266_ERR_S);
      return;
    }
    else
      next_command = 1;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&esp8266_fifo_index, WF_FIFO_DISCONNECT_SIZE))
      set_esp8266_status(ESP8266_IDLE_S);
  }
}


/* Parsing risultato scansione reti. */
static uint8_t esp8266_parse_scanning(ESP8266_Scan_t* ap_list)
{
  uint8_t stop_parse=0;
  uint16_t start=0, res_offset=0, newline_idx=0;
  
  ap_list->num = 0;
  for(start=0; start<sizeof(wf_mod_buff_Risposta) && !stop_parse; start++)
  {
    if(searchString(wf_mod_buff_Risposta, "+CWLAP:", start, sizeof(wf_mod_buff_Risposta), &res_offset, 1, 1))
    {
      //--- Match trovato ---
      for(newline_idx=0; newline_idx<MAX_AP_STRING; newline_idx++)
      {
        if(wf_mod_buff_Risposta[start+res_offset+newline_idx] == CR_CHAR) 
          break;        //passo al prossimo risultato
        else 
          ap_list->list[ap_list->num][newline_idx] = wf_mod_buff_Risposta[start+res_offset+newline_idx];    //copio risultato corrente
      }
      if(res_offset > 0)
        start += (res_offset-1);        //'searchString' ritorna posizione primo carattere dopo l'occorrenza: 
                                        //torno indietro di 1 per compensare l'incremento di 'start' eseguito dal ciclo for
      ap_list->num++;
      if(ap_list->num >= MAX_AP)
        stop_parse = 1;
    }
    else 
      stop_parse = 1;   //--- Match non trovato: interrompo parsing ---
  }
  
  return (ap_list->num);
}
