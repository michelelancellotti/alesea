/**
******************************************************************************
* File          	: esp_wroom_02_info.c
* Versione libreria	: 1.01
* Descrizione        	: Get info modulo WiFi:
*                         - check COM
*                         - versione FW
******************************************************************************
*
* COPYRIGHT(c) 2021 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "esp_wroom_02_info.h"
#include "esp_wroom_02_command.h"




#define WF_FIFO_INFO_SIZE       3               //N.B: ---- Attenzione a ordine comandi (utilizzo esp8266_critical_cmd_MASK) ----
static Mod_Fifo_Entry_t wf_fifo_GetInfo[WF_FIFO_INFO_SIZE] = {
  {.at_cmd=(uint8_t*)"AT",                      .at_resp=(uint8_t*)"OK",                .AT_CMD_TYPE=ESP8266_CMD_AT,                    .AT_CMD_RETRY=10,       .AT_RESP_TIMEOUT=ESP8266_1000MS_TO},
  {.at_cmd=(uint8_t*)"AT+GMR",                  .at_resp=(uint8_t*)"AT version:",       .AT_CMD_TYPE=ESP8266_CMD_FW_INFO},
  {.at_cmd=(uint8_t*)"AT+CIPAPMAC_CUR?",        .at_resp=(uint8_t*)"+CIPAPMAC_CUR:",    .AT_CMD_TYPE=ESP8266_CMD_GET_MAC_ADDRESS_AP,    .AT_CMD_RETRY=3},
};


static char esp8266_FW_info[ESP8266_FW_INFO_SIZE+1];		//Stringa versione FW AT
char esp8266_AP_MAC_address[MAC_ADDRESS_SIZE+1];                //MAC address Soft-AP
uint8_t esp8266_check_COM = 0;


/* ############################################################ 
* Local functions
* ############################################################
*/




/* -------------------- Stato: ESP8266_INFO -------------------- 
* Info modulo WiFi: versione FW.
*/
void ESP8266_INFO_func(const uint32_t PERIODO)
{
  ErrorCode_t err;
  uint8_t next_command = 0;
  uint16_t offset = 0;  
        
  if(!GPTIMER_isTimedOut(&esp8266_NopTimer) && esp8266_NopTimer.enable)
    return;
  
  GPTIMER_stop(&esp8266_NopTimer);					
  //Attesa invio primo comando AT conclusa
  err = esp8266_send_command(wf_fifo_GetInfo, esp8266_fifo_index, &offset);
  if(err == M_OK_AT_RESP)
  {
    switch(wf_fifo_GetInfo[esp8266_fifo_index].AT_CMD_TYPE)
    {
      case ESP8266_CMD_AT:
        esp8266_reg.BIT.READY = 1;
        esp8266_check_COM++;
        break;
        
      case ESP8266_CMD_FW_INFO:
        memset(esp8266_FW_info, 0x00, sizeof(esp8266_FW_info));
        memcpy(esp8266_FW_info, &wf_mod_buff_Risposta[offset], sizeof(esp8266_FW_info)-1);      //-1 per garantire l'aggiunta del terminatore di stringa
        break;
        
      case ESP8266_CMD_GET_MAC_ADDRESS_AP:
        offset++;       //scarto "
        memset(esp8266_AP_MAC_address, 0x00, sizeof(esp8266_AP_MAC_address));        
        memcpy(esp8266_AP_MAC_address, &wf_mod_buff_Risposta[offset], sizeof(esp8266_AP_MAC_address)-1);        //-1 per garantire l'aggiunta del terminatore di stringa
        break;
        
      default: break;
    }
    
    next_command = 1;
  }
  else if(err == M_NO_AT_RESP)
  {
    esp8266_critical_cmd_MASK = 0x0000;
    //--- Gestione eccezioni su errore ---
    if(esp8266_critical_cmd(esp8266_fifo_index, esp8266_critical_cmd_MASK))
    {
      set_esp8266_status(ESP8266_ERR_S);
      return;
    }
    else
      next_command = 1;
  }
  
  //Incremento coda comandi
  if(next_command)
  {
    if(mod_Inc_Fifo(&esp8266_fifo_index, WF_FIFO_INFO_SIZE))
      set_esp8266_status(ESP8266_IDLE_S);
  }
}

