/**
******************************************************************************
* File          	: wifi.c
* Versione libreria	: 1.01
* Descrizione        	: Supporto modulo WiFi (livello applicativo).
******************************************************************************
*
* COPYRIGHT(c) 2021 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

//### INCLUDE applicazione ###
#include "main.h"
#include "usart.h"
//############################

#include "wifi.h"
#include "esp_wroom_02.h"
#include "modem_utils.h"




//Handle modulo WiFi
Wf_Mod_t WiFiType = WF_MOD_ESP_WROOM_02;
void (*wifi_Automa)(const uint32_t PERIODO);
void (*wifi_ProcessaByte)(uint8_t data);




//### Risorse applicazione ###


/* Funzioni locali. */
static void Wifi_Reset_Pin(uint8_t ON);
static void wf_dummy_fun_arg0(const uint32_t src);
static void wf_dummy_fun_arg1(uint8_t src);
static uint16_t wifi_format_AccessPoint(uint8_t* src, uint8_t* dest);
static uint8_t wifi_convert_RSSI(int16_t RSSI_dBm);



/* Inizializza libreria WiFi: assegnazione driver seriale e reset GPIO + allocazione risorse. */
ErrorCode_t wifi_Init(Wf_Mod_t modello)
{
  ErrorCode_t err = M_NO_ERROR;  
  
  wifi_Automa = wf_dummy_fun_arg0;		
  wifi_ProcessaByte = wf_dummy_fun_arg1;
  
  if(modello >= WF_MOD_NUM) 
  {
    err = M_ERR_MODEL_INIT;
    WiFiType = WF_MOD_ESP_WROOM_02;     //default	
  }
  else 
    WiFiType = modello;
    
  //Assegnazione risorse HW 
  wf_mod_id_Seriale = SERIALE_WIFI;
  wf_mod_Write_seriale = Write_SCI;
  wf_mod_Drive_reset = Wifi_Reset_Pin;
  
  //Assegnazione macchina a stati	
  switch(WiFiType)
  {
    case WF_MOD_ESP_WROOM_02: 
      wifi_Automa = esp8266_Automa;
      wifi_ProcessaByte = esp8266_ProcessaByte;
      init_esp8266();
      break;
      
    default: err=M_ERR_MODEL_INIT; break;
  }
  
  return err; 
}

/* Pilotaggio pin reset modulo WiFi.
*
* Nota: si utilizza il pin EN del modulo (attivo alto). 
*/
static void Wifi_Reset_Pin(uint8_t ON)
{
  if(ON) HAL_GPIO_WritePin(WIFI_EN_HW4_GPIO_Port, WIFI_EN_HW4_Pin, GPIO_PIN_RESET);
  else HAL_GPIO_WritePin(WIFI_EN_HW4_GPIO_Port, WIFI_EN_HW4_Pin, GPIO_PIN_SET);
}

static void wf_dummy_fun_arg0(const uint32_t src){}
static void wf_dummy_fun_arg1(uint8_t src){}



/* GET numero access-point scansionati.
*	
*@param[OUT]: resp: numero access-point scansionati (1B)
*
*@return: 
*WA_OK
*WA_INIT_ERROR (modulo non gestito)
*/
WiFiAppli_t wifi_GET_NumAccessPoint(uint8_t* resp)
{
  WiFiAppli_t result = WA_INIT_ERROR;
  
  switch(WiFiType)
  {
    case WF_MOD_ESP_WROOM_02:
      *resp = esp8266_AP_list.num;
      result = WA_OK;
      break;
      
    default: break;
  }
  
  return result;
}

/* GET descrittivo complessivo access-point scansionati.
*	
*@param[OUT]: resp: descrittivo complessivo access-point
*@param[OUT]: resp_size: dimensione descrittivo (N bytes, MAX WIFI_FULL_AP_STRING_SIZE bytes, incluso terminatore di stringa)
*
*@return: 
*WA_OK
*WA_INIT_ERROR (modulo non gestito)
*/
WiFiAppli_t wifi_GET_AccessPoint(uint8_t* resp, uint16_t* resp_size)
{
  WiFiAppli_t result = WA_INIT_ERROR;
  uint16_t offset = 0;
  
  switch(WiFiType)
  {
    case WF_MOD_ESP_WROOM_02:
      //Compongo la stringa risultato come array JSON di elementi stringa
      resp[offset++] = '[';
      if(esp8266_AP_list.num > 0)
      {
        for(uint8_t id=0; id<esp8266_AP_list.num && id<MAX_AP; id++)
        {
          offset += wifi_format_AccessPoint((uint8_t*)esp8266_AP_list.list[id], &resp[offset]);
          if(offset < WIFI_FULL_AP_STRING_SIZE)
          {
            if(id < (esp8266_AP_list.num-1))
              resp[offset++] = ',';
            else
              resp[offset++] = ']';
          }
        }
      }
      else                      //nessun elemento: array vuoto
        resp[offset++] = ']';
      
      if(offset>=WIFI_FULL_AP_STRING_SIZE) offset=WIFI_FULL_AP_STRING_SIZE-1;
      resp[offset] = 0x00;      //append terminatore di stringa
      
      *resp_size = offset;
      result = WA_OK;
      break;
      
    default: break;
  }
  
  return result;
}


/* GET RSSI rete WiFi.
*	
*@param[OUT]: resp: RSSI rete WiFi (1B)
*
*@return: 
*WA_OK (connessione rete WiFi completata)
*WA_ERROR (connessione rete WiFi fallita)
*WA_INIT_ERROR (modulo non gestito)
*/
WiFiAppli_t wifi_GET_RSSI(uint8_t* resp)
{
  WiFiAppli_t result = WA_INIT_ERROR;
  
  switch(WiFiType)
  {
    case WF_MOD_ESP_WROOM_02:
      *resp = wifi_convert_RSSI(esp8266_RSSI);
      
      if(esp8266_reg.BIT.WLAN_CONNECTED)
        result = WA_OK;                         //connessione rete WiFi COMPLETATA
      else
        result = WA_ERROR;			//connessione rete WiFi FALLITA (dato non significativo)	      
      break;
      
    default: break;
  }
  
  return result;
}


/* GET diagnostica modulo WiFi.
*	
*@param[OUT]: reg_status, registro di stato modulo WiFi (2B)
*@param[OUT]: last_error, codice dell'ultimo errore rilevato (comandi AT) (1B)
*
*@return: 
*WA_OK
*WA_INIT_ERROR (modulo non gestito)
*/
WiFiAppli_t wifi_GET_Diagnostic(uint16_t* reg_status, uint8_t* last_error)
{
  WiFiAppli_t result = WA_INIT_ERROR;
  
  switch(WiFiType)
  {
    case WF_MOD_ESP_WROOM_02:
      *reg_status = esp8266_reg.WORD;
      *last_error = esp8266_last_error;
      result = WA_OK;
      break;
      
    default: break;
  }
  
  return result;
}

/* GET diagnostica WiFi estesa (single byte).
*
*@param[IN]: type: tipologia diagnostica
*@param[OUT]: resp: diagnostica richiesta (1B)
*
*@return: 
*WA_OK (tipologia diagnostica valida)
*WA_ERROR (tipologia diagnostica non valida)
*WA_INIT_ERROR (modulo non gestito)
*/	
WiFiAppli_t wifi_GET_ExtDiagnostic(WiFiDiagnostic_t type, 
                                   uint8_t* resp)
{
  WiFiAppli_t result = WA_INIT_ERROR;
  uint8_t diagnostic[W_DNG_NUM];
  
  diagnostic[W_DGN_STATO_AUTOMA] = esp8266_status;
    
  switch(WiFiType)
  {
    case WF_MOD_ESP_WROOM_02:
      if(type < sizeof(diagnostic))
      {
        *resp = diagnostic[type];
        result = WA_OK;
      }
      else
        result = WA_ERROR;
      break;
      
    default: break;
  }
  
  return result;
}


/* GET MAC address (usato in modalit� Soft-AP).
*	
*@param[OUT]: resp: MAC address (stringa HEX)
*@param[OUT]: resp_size: dimensione MAC address (17B)
*
*@return: 
*WA_OK
*WA_INIT_ERROR (modulo non gestito)
*/
WiFiAppli_t wifi_GET_MAC_address(uint8_t* resp, uint16_t* resp_size)
{
  WiFiAppli_t result = WA_INIT_ERROR;
  
  switch(WiFiType)
  {
  case WF_MOD_ESP_WROOM_02:
      *resp_size = sizeof(esp8266_AP_MAC_address) - 1;            //-1 per escludere terminatore
      memcpy(resp, esp8266_AP_MAC_address, sizeof(esp8266_AP_MAC_address)-1);
      result = WA_OK;
      break;
      
    default: break;
  }
  
  return result;
}


//#################### Applicazione ####################


/* Riformatta descrittivo access-point in ingresso, secondo il format:
 * "<MAC_ADDRESS_ACCESS_POINT>;<POTENZA_SEGNALE>"
 *
 *@param[IN]: src: buffer sorgente (size attesa = MAX_AP_STRING)
 *@param[OUT]: dest: buffer destinazione (size MIN = MAX_AP_STRING)
 *
 *@return: numero bytes scritti sul buffer destinazione (escluso terminatore di stringa)
*/
static uint16_t wifi_format_AccessPoint(uint8_t* src, 
                                        uint8_t* dest)
{
  char app_string[MAX_AP_STRING];
  uint16_t offset=0, free_space;
  int res;
    
  app_string[0] = 0x00;                         //pulizia buffer temporaneo
  free_space = sizeof(app_string);
  //Estrapolo MAC-address (stringa)
  searchField(src, '"', 0, MAX_AP_STRING);
  res = snprintf(&app_string[offset], free_space, "\"%s;", searchField_res[1].field);
  offset += check_snprintf(res, &free_space);
  //Estrapolo potenza segnale (stringa)
  searchField(src, ',', 1, MAX_AP_STRING);      //start=1 per bypass carattere '('
  res = snprintf(&app_string[offset], free_space, "%s\"", searchField_res[0].field);
  offset += check_snprintf(res, &free_space);
  //terminatore di stringa aggiunto da 'snprintf' ...
  
  if(offset>MAX_AP_STRING) offset=MAX_AP_STRING;
  memcpy(dest, app_string, offset);             //copio sul buffer di uscita  
  return offset;
}

/* Rimappa RSSI da dBm ad indice adimensionale.
 * Nota: range [0=MIN_RSSI;31=MAX_RSSI].
 *
 *@param[IN]: RSSI_dBm: valore RSSI in espresso in dBm
 *
 *@return: RSSI convertito
*/
static uint8_t wifi_convert_RSSI(int16_t RSSI_dBm)
{
  uint16_t result;
  
  if(RSSI_dBm < -113) 
    result = 0;
  else if(RSSI_dBm > -51) 
    result = 31;
  else
    result = (RSSI_dBm + 113)/2;
  
  return ((uint8_t)result);
}


/* GET stato operazione richiesta a modulo WiFi. 
*
*@param[IN]: operazione: tipologia operazione di cui conoscere lo stato
*
*@return:
*WA_OK (operazione conclusa: successo)
*WA_ERROR (operazione conclusa: errore)
*WA_BUSY (operazione in corso)
*WA_UNP_ERROR (operazione conclusa: errore imprevisto)
*WA_INIT_ERROR (tipologia modulo non gestita)
*/
WiFiAppli_t wifi_GET_Status(WiFiTrigger_t operazione)
{
  WiFiAppli_t result = WA_INIT_ERROR;
  uint8_t status;
  
  switch(WiFiType)
  {
    case WF_MOD_ESP_WROOM_02:
      status = get_esp8266_status();
      if(status != ESP8266_IDLE_S)
        result = WA_BUSY;               //--- Operazione in corso ---
      else
      {
        //Modulo in stato IDLE
        if(esp8266_reg.BIT.GENERIC_ERROR)
          result = WA_UNP_ERROR;        //--- Operazione completata: errore inatteso ---
        else
        {
          //--- Operazione completata: OK o errore gestito ---
          result = WA_OK;
          switch(operazione)
          {
            case WF_TRG_SCAN:
              if(esp8266_reg.BIT.SCAN_ERROR) result=WA_ERROR;           //Errore scansione reti
              break;
              
            case WF_TRG_CONNECT:
              if(esp8266_reg.BIT.CONNECT_ERROR) result=WA_NETWORK_ERROR;        //Errore connessione a server TCP
              break;
              
            case WF_TRG_TCP_SEND:
              if(esp8266_reg.BIT.TCP_SEND_ERROR) result=WA_NETWORK_ERROR;       //Errore scrittura socket TCP
              break;          
              
            default: break;
          }
        }
      }
      break;
      
    default: break;
  }
  
  return result;
}

/* Trigger azioni modulo WiFi.
*
*@param[IN]: trigger: tipologia operazione richiesta a modulo
*@param[IN]: data: buffer parametri
*@param[IN]: data_size: dimensione buffer parametri
*
*@return:
*WA_TRIG_OK (operazione richiesta gestita)
*WA_INIT_ERROR (tipologia modulo non gestita o errore parametri input)
*WA_BUSY (modulo impegnato in altre operazioni)
*/
WiFiAppli_t wifi_Trigger(WiFiTrigger_t trigger, uint8_t* data, uint16_t data_size)
{
  WiFiAppli_t result = WA_INIT_ERROR;
  uint16_t i;
  
  switch(WiFiType)
  {
    case WF_MOD_ESP_WROOM_02:
      if(trigger==WF_TRG_RESET) { set_esp8266_status(ESP8266_RESET_S); result=WA_TRIG_OK; }     //Trigger asincrono
      else if(get_esp8266_status() == ESP8266_IDLE_S)
      {
        //Trigger sincroni
        switch(trigger)
        {
          case WF_TRG_SCAN: 
            set_esp8266_status(ESP8266_SCAN_S); 
            result = WA_TRIG_OK;
            break;
            
          case WF_TRG_CONNECT:
            set_esp8266_status(ESP8266_CONNECT_S);
            result = WA_TRIG_OK;
            break;
            
          case WF_TRG_TCP_SEND:
            if(data && data_size>0)
            {
              //-- Imposta dati per scrittura su socket TCP --
              if(data != mod_buff_data_TX)
              {
                if(data_size >= sizeof(mod_buff_data_TX))
                  data_size = sizeof(mod_buff_data_TX) - 1;     //-1 per garantire l'aggiunta del terminatore di stringa
                memset(mod_buff_data_TX, 0x00, sizeof(mod_buff_data_TX));
                memcpy(mod_buff_data_TX, data, data_size);      //scrittura dati da inviare sul buffer di TX
                esp8266_size_data_TX = data_size;
              }
              else
              {
                //Dati da inviare gi� scritti sul buffer di TX (esternamente)
                esp8266_size_data_TX = 0;
                for(i=0; mod_buff_data_TX[i]!=0x00 && i<sizeof(mod_buff_data_TX)-1; i++)
                  esp8266_size_data_TX++;
                mod_buff_data_TX[i] = 0x00;     //aggiungo terminatore di stringa
              }
              set_esp8266_status(ESP8266_TCP_SEND_S);
              result = WA_TRIG_OK;
            }
            break;
            
          case WF_TRG_DISCONNECT:
            set_esp8266_status(ESP8266_DISCONNECT_S);
            result = WA_TRIG_OK;
            break;
            
          default: break;
        }
      }
      else
        result = WA_BUSY;       //BUSY (modulo impegnato)
      break;
      
    default: break;
  }
  
  return result;
}
