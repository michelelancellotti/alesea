/**
******************************************************************************
* File          	: esp_wroom_02.c
* Versione libreria	: 1.01
* Descrizione        	: Supporto modulo EspressIF ESP-WROOM-02.
******************************************************************************
*
* COPYRIGHT(c) 2021 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "esp_wroom_02.h"
#include "esp_wroom_02_info.h"
#include "esp_wroom_02_network.h"




ESP8266_State_t esp8266_status;

GPTIMER esp8266_NopTimer;
ESP8266_Reg_t esp8266_reg;
uint16_t esp8266_critical_cmd_MASK = 0;
uint8_t esp8266_fifo_index = 0;                                 //Indice coda comandi
uint8_t esp8266_last_error, esp8266_last_nc_error;

uint8_t WF_AT_ERROR_STR[] = "ERROR";

static GPTIMER esp8266_resetTimer, esp8266_NopTimer;		//Timer
static ESP8266_State_t esp8266_status_prev;
static uint32_t esp8266_err_cnt = 0;				//Contatore errori
static uint16_t esp8266_nc_err_cnt = 0;                         //Contatore errori comandi NON CRITICI e/o comandi che possono andare in errore (eccezioni)




/* ############################################################ 
* Local functions
* ############################################################
*/
static void init_ctx_esp8266(void);
static void ESP8266_RESET_func(const uint32_t PERIODO);
static void ESP8266_IDLE_func(const uint32_t PERIODO);
static void ESP8266_ERR_func(const uint32_t PERIODO);


static State_fptr esp8266_status_func[ESP8266_STATE_NUM] = {
  ESP8266_RESET_func,           //0x00
  ESP8266_IDLE_func,            //0x01
  ESP8266_INFO_func,            //0x02
  ESP8266_SCAN_func,            //0x04
  ESP8266_ERR_func,             //0x05
  ESP8266_CONNECT_func,         //0x06
  ESP8266_TCP_SEND_func,        //0x07
  ESP8266_DISCONNECT_func,      //0x08
};




void init_esp8266(void)
{	
  GPTIMER_init(&esp8266_resetTimer, ESP8226_RESET_WAIT_TIME);
  GPTIMER_init(&esp8266_NopTimer, ESP2866_NOP_TIMEOUT);
  
  init_ctx_esp8266();
  
  //Init macchina a stati
  set_esp8266_status(ESP8266_IDLE_S);
  esp8266_status_prev = ESP8266_IDLE_S;
}

/* Init contesto "sessione" corrente. */
static void init_ctx_esp8266(void)
{
  esp8266_reg.WORD = 0;
  
  esp8266_err_cnt = 0;
  esp8266_last_error = 0x00;
  esp8266_critical_cmd_MASK = 0;
  esp8266_check_COM = 0;
  
  wf_mod_Init_AT();             //Reset gestione RX/TX comandi AT (caso di reset asincroni del modulo)  
}


/* Determina se il comando indicizzato � CRITICO, in base alla maschera. 
*  Rilevare un errore su un comando CRITICO genera l'uscita immediata dallo stato dell'automa.
*/
uint8_t esp8266_critical_cmd(uint8_t cmd_index, uint16_t cmd_mask)
{
  if(((1<<(cmd_index)) & cmd_mask) == 0)
  {
    esp8266_nc_err_cnt++;
    esp8266_last_nc_error = wf_mod_cmdAttesa.lastCmd.cmdCode;
    return 0;		//comando NON CRITICO
  }
  else
    return 1;		//comando CRITICO
}


void set_esp8266_status(ESP8266_State_t new_status)
{
  switch(new_status)
  {
    case ESP8266_RESET_S: init_ctx_esp8266(); break;
    case ESP8266_INFO_S: esp8266_reg.BIT.READY=0; break;
    case ESP8266_SCAN_S: { 
      esp8266_reg.BIT.SCAN_START=1; esp8266_reg.BIT.SCAN_ERROR=0;
      esp8266_tempo_Scansione=0; esp8266_AP_list.num=0;      
    } break;
    case ESP8266_CONNECT_S: { 
      esp8266_reg.BIT.CONNECT_START=1; esp8266_reg.BIT.CONNECT_ERROR=0; 
      esp8266_reg.BIT.WLAN_CONNECTED=0; esp8266_tempo_Connessione=0; 
    } break;
    case ESP8266_TCP_SEND_S: esp8266_reg.BIT.TCP_SEND_ERROR=0; break;
    case ESP8266_ERR_S: esp8266_status_prev=esp8266_status; break;	//snapshot stato che ha generato errore
    default: break;
  }
  if(new_status!=ESP8266_IDLE_S) esp8266_reg.BIT.GENERIC_ERROR=0;
  esp8266_status = new_status;
  esp8266_fifo_index = 0;
}
ESP8266_State_t get_esp8266_status(void)
{
  return esp8266_status;	
}

/* Macchina a stati ESP-WROOM-02. */
void esp8266_Automa(const uint32_t PERIODO)
{
  /* Check limiti array punt. funz. stato */
  if(esp8266_status >= ESP8266_STATE_NUM)
  {
    set_esp8266_status(ESP8266_IDLE_S);
    return;
  }
  
  
  esp8266_status_func[esp8266_status](PERIODO);         //eseguo callback funzione di stato	
}


/* -------------------- Stato: ESP8266_RESET -------------------- 
* Reset modulo.
* Nota: al reset il modulo comunica delle informazioni di BOOT con un baudrate diverso da 115200bps 
* (per questo motivo ci sono errori, gestiti, sulla seriale).
*/
static void ESP8266_RESET_func(const uint32_t PERIODO)
{
  esp8266_fifo_index = 0;
  if(!esp8266_resetTimer.enable)
  {
    wf_mod_Drive_reset(1);                      //Reset attivo
    GPTIMER_stop(&esp8266_resetTimer);
    GPTIMER_setTics(&esp8266_resetTimer, ESP8226_RESET_WAIT_TIME);
    GPTIMER_reTrigger(&esp8266_resetTimer);     //Start wait time
  }
  else if(GPTIMER_isTimedOut(&esp8266_resetTimer))
  {
    GPTIMER_stop(&esp8266_resetTimer);              
    wf_mod_Drive_reset(0);			//Rilascio reset
    GPTIMER_stop(&esp8266_NopTimer);
    GPTIMER_setTics(&esp8266_NopTimer, ESP2866_NOP_TIMEOUT);
    GPTIMER_reTrigger(&esp8266_NopTimer);
    set_esp8266_status(ESP8266_INFO_S);
  }
}


/* -------------------- Stato: ESP8266_IDLE -------------------- */
static void ESP8266_IDLE_func(const uint32_t PERIODO)
{
  esp8266_fifo_index = 0;
  esp8266_critical_cmd_MASK = 0;
}


/* -------------------- Stato: ESP8266_ERR -------------------- 
* Stato di errore.
*/
static void ESP8266_ERR_func(const uint32_t PERIODO)
{
  esp8266_err_cnt++;
  esp8266_last_error = wf_mod_cmdAttesa.lastCmd.cmdCode;
  if(esp8266_status_prev==ESP8266_SCAN_S ||
      esp8266_status_prev==ESP8266_CONNECT_S ||
        esp8266_status_prev==ESP8266_TCP_SEND_S ||
          esp8266_status_prev==ESP8266_DISCONNECT_S)
  {
    //Errore GESTITO
    set_esp8266_status(ESP8266_IDLE_S);
  }
  else
  {
    //Errore IMPREVISTO
    esp8266_reg.BIT.GENERIC_ERROR = 1;
    set_esp8266_status(ESP8266_IDLE_S);
  }
}


/* Processa byte ricevuti su seriale (IRQ). */
void esp8266_ProcessaByte(uint8_t data)
{
  //### Modulo pronto a ricevere comandi ###
  //<<< Buffer ricerca risposta >>>
  if(wf_mod_cmdAttesa.attesaRisposta && (wf_mod_buff_Risposta_idx < sizeof(wf_mod_buff_Risposta)))
  {      
    wf_mod_buff_Risposta[wf_mod_buff_Risposta_idx++] = data;      
  }		  		
}

