/**
******************************************************************************
* File          	: esp_wroom_02_command.c
* Versione libreria	: 1.01
* Descrizione       	: Gestore invio comandi modulo EspressIF ESP-WROOM-02.
******************************************************************************
*
* COPYRIGHT(c) 2019 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "esp_wroom_02_command.h"




/* Buffer appoggio composizione comandi AT. */
static ESP8266_Buffer_t esp8266_buffer;

/* ############################################################ 
* Local functions
* ############################################################
*/




/* Invia comando da coda e attende ricezione risposta. */
ErrorCode_t esp8266_send_command(Mod_Fifo_Entry_t* fifo, uint8_t fifo_index, uint16_t* offset_resp)
{
  ErrorCode_t err;
  uint8_t AT_CMD_RETRY, AT_RESP_TIMEOUT, AT_RESP_RETRY, modulo;
  uint8_t* at_cmd;
  uint16_t offset=0, free_space=0;
  int res = 0;
  
  //Assegnazione comando (default, da coda comandi)
  at_cmd = fifo[fifo_index].at_cmd;
  
  //Assegnazione retry invio comando
  if(fifo[fifo_index].AT_CMD_RETRY == 0)
    AT_CMD_RETRY = 1;                   //default, se non assegnato
  else
    AT_CMD_RETRY = fifo[fifo_index].AT_CMD_RETRY;
  
  //Assegnazione modulo (default)
  modulo = WiFiType;
  
  //Assegnazione timeout risposta
  if(fifo[fifo_index].AT_RESP_TIMEOUT == 0) 
    AT_RESP_TIMEOUT = ESP8266_200MS_TO; //default, se non assegnato
  else
    AT_RESP_TIMEOUT = fifo[fifo_index].AT_RESP_TIMEOUT;
  
  //Assegnazione retry ricerca risposta
  if(fifo[fifo_index].AT_RESP_RETRY == 0)
    AT_RESP_RETRY = 1;                  //default, se non assegnato                  
  else
    AT_RESP_RETRY = fifo[fifo_index].AT_RESP_RETRY;
  
  //Add terminatore stringa all'inizio del buffer di appoggio:
  //--> pulizia veloce del buffer di appoggio e di TUTTI i suoi figli (union di buffer)
  esp8266_buffer.network_buff[0] = 0x00;
  
  switch(fifo[fifo_index].AT_CMD_TYPE)
  {
    case ESP8266_CMD_CONNECT_AP:
      offset += concatenaStringhe(esp8266_buffer.network_buff, offset, fifo[fifo_index].at_cmd, sizeof(esp8266_buffer.network_buff));   //add CMD header
      free_space = sizeof(esp8266_buffer.network_buff) - offset;
      res = snprintf((char*)&esp8266_buffer.network_buff[offset], free_space, "\"%s\",\"%s\"",
                     esp8266_SSID,                                                                                                      //add SSID rete WiFi
                     esp8266_password);                                                                                                 //add password rete WiFi
      offset += check_snprintf(res, &free_space);
      at_cmd = esp8266_buffer.network_buff;
      break;
      
    case ESP8266_CMD_TCP_SOCKET_WRITE_LENGTH:
      offset += concatenaStringhe(esp8266_buffer.network_buff, offset, fifo[fifo_index].at_cmd, sizeof(esp8266_buffer.network_buff));   //add CMD header
      free_space = sizeof(esp8266_buffer.network_buff) - offset;
      res = snprintf((char*)&esp8266_buffer.network_buff[offset], free_space, "%d", esp8266_size_data_TX);                              //add dimensione dati TX
      offset += check_snprintf(res, &free_space);
      at_cmd = esp8266_buffer.network_buff;
      break;
      
    case ESP8266_CMD_TCP_SOCKET_WRITE_PAYLOAD:
      modulo = WF_MOD_NO_TAILER;
      at_cmd = mod_buff_data_TX;
      break;
      
    default: break;
  }
  
  
  //Add terminatore di stringa sul buffer di appoggio
  if(offset < sizeof(esp8266_buffer))
    esp8266_buffer.network_buff[offset] = 0x00;
  else
    esp8266_buffer.network_buff[sizeof(esp8266_buffer)-1] = 0x00;
  
  
  //Invia comando  
  wf_mod_Push_Cmd(at_cmd, fifo[fifo_index].AT_CMD_TYPE, AT_CMD_RETRY, modulo);
  err = wf_mod_Read_Response(AT_RESP_TIMEOUT, AT_RESP_RETRY, fifo[fifo_index].at_resp, offset_resp, WF_AT_ERROR_STR);
  
  return err;
}
