/**
  ******************************************************************************
  * File          	: wifi_common.c
  * Versione libreria	: 1.01
  * Descrizione        	: Invio comandi AT & parsing risposte.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2021 "Computec srl"
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of "Computec srl" nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

#include "wifi_common.h"
#include "modem_utils.h"



Mod_CmdAttesa_t wf_mod_cmdAttesa;
uint8_t wf_mod_id_Seriale;

#ifdef WIFI_UART_LOG
uint8_t dbg_wifi[100];
uint16_t dbg_wifi_idx = 0;
#endif
uint8_t wf_mod_buff_Risposta[MOD_MAX_BUFFER_RX_UNBOUND];
uint16_t wf_mod_buff_Risposta_idx = 0;

void (*wf_mod_Write_seriale)(uint8_t id_seriale, uint8_t *src, uint16_t size);  //puntatore a driver per scrittura su seriale
void (*wf_mod_Drive_reset)(uint8_t ON);						//puntatore a driver pin RESET

static GPTIMER wf_mod_cmdTimer;		        //timer;
static uint8_t wf_tentativo = 0;
static uint8_t wStatus = 0;


/* ############################################################ 
 * Local functions
 * ############################################################
*/
static ErrorCode_t wf_mod_Wait_Response(const uint8_t timeout_ticks);
static void wf_mod_Purge_BufferResponse(void);




/* Init gestione comandi AT. */
void wf_mod_Init_AT(void)
{
  memset(&wf_mod_cmdAttesa, 0x00, sizeof(wf_mod_cmdAttesa));
  wf_tentativo = 0;
  wStatus = 0;
}


/* Invio comando AT al modulo. 
*
*@param[IN]: cmd, comando da inviare
*@param[IN]: maxRetries, numero tentativi di invio comando
*@param[IN]: modulo
*/
void wf_mod_Push_Cmd(uint8_t* cmd, uint8_t cmdCode, const uint16_t maxRetries, uint8_t modulo)
{
  uint16_t i, index_tailer, MAX_CMD_SIZE;
  
  if(!wf_mod_cmdAttesa.attesaRisposta)
  {
    wf_mod_Purge_BufferResponse();
    wf_mod_cmdAttesa.attesaRisposta = 1;		
    
    //Push comando
    MAX_CMD_SIZE = sizeof(wf_mod_cmdAttesa.lastCmd.cmd) - 2;    //-2 per considerare anche presenza di tailer comando
    memset(wf_mod_cmdAttesa.lastCmd.cmd, 0x00, sizeof(wf_mod_cmdAttesa.lastCmd.cmd));         
    for(i=0; cmd[i]!=0x00 && i<MAX_CMD_SIZE; i++)	        //Push fino a terminatore stringa o raggiunta MAX dimensione buffer comando
      wf_mod_cmdAttesa.lastCmd.cmd[i] = cmd[i];
    
    wf_mod_cmdAttesa.lastCmd.cmdSize = i;                       //push dimensione comando
    wf_mod_cmdAttesa.lastCmd.cmdCode = cmdCode;                 //push codice comando
    wf_mod_cmdAttesa.lastCmd.maxRetryCmd = maxRetries;          //push numero MAX retry invio comando
    wf_mod_cmdAttesa.retryCmd++;
    
    //Aggiungo tailer
    index_tailer = wf_mod_cmdAttesa.lastCmd.cmdSize;
    if(modulo != WF_MOD_NO_TAILER)
    {
      wf_mod_cmdAttesa.lastCmd.cmd[index_tailer++] = CR_CHAR;
      wf_mod_cmdAttesa.lastCmd.cmd[index_tailer++] = LF_CHAR;
    }
    //else ... invio comando senza tailer
    
    wf_mod_Write_seriale(wf_mod_id_Seriale, wf_mod_cmdAttesa.lastCmd.cmd, index_tailer);         //invio comando						
  }
}

/* Reset buffer risposta. */
void wf_mod_Purge_BufferResponse(void)
{
  memset(wf_mod_buff_Risposta, 0x00, sizeof(wf_mod_buff_Risposta));
  wf_mod_buff_Risposta_idx = 0;
}


/* Attendo timeout per lettura risposta a comando AT. */
ErrorCode_t wf_mod_Wait_Response(const uint8_t timeout_ticks)
{
  ErrorCode_t err = M_NO_ERROR;  
  
  if(wf_mod_cmdAttesa.attesaRisposta)
  { 
    switch(wStatus)
    {
      case 0:
        //Start timer timeout
        GPTIMER_stop(&wf_mod_cmdTimer);
        GPTIMER_setTics(&wf_mod_cmdTimer, WF_MOD_MS_PER_TICKS*((uint16_t)timeout_ticks));
        GPTIMER_reTrigger(&wf_mod_cmdTimer);        
        wStatus = 1;
        err = M_WAIT_AT_RESP;
        break;
        
      case 1: 
        //Wait
        if(GPTIMER_isTimedOut(&wf_mod_cmdTimer))
        {
          wStatus = 0;
          err = M_TO_AT_END;
        }
        else err=M_WAIT_AT_RESP;
        break;
        
      default: break;
    }
  }
  else
    wStatus = 0;
  
  return err;
}

/* Cerco risposta attesa a comando AT. 
*
*@param[IN]: timeout_ticks, tempo atteso prima di cercare la risposta (espresso come ticks da WF_MOD_MS_PER_TICKS ms)
*@param[IN]: maxRetries, numero tentativi ricerca risposta
*@param[IN]: resp, stringa risposta da cercare
*@param[OUT]: respOffset, indice offset dove termina la risposta
*@param[IN]: error, stringa errore da cercare in caso di mancata risposta
*
*@return: codice errore
*/
ErrorCode_t wf_mod_Read_Response(const uint8_t timeout_ticks, const uint8_t maxRetries, uint8_t* resp, uint16_t* respOffset, uint8_t* error)
{
  ErrorCode_t err = M_NO_ERROR;
  uint16_t errorOffset = 0;  
  
  if(wf_tentativo < maxRetries)
  {  
    err = wf_mod_Wait_Response(timeout_ticks);
    if(err == M_TO_AT_END)
    {
      //-- Timeout concluso --
      if(searchString(wf_mod_buff_Risposta, resp, 0, sizeof(wf_mod_buff_Risposta),/*wf_mod_buff_Risposta_idx,*/respOffset, 1, 1))
      {
        //-- Risposta trovata --
        wf_mod_cmdAttesa.attesaRisposta = 0;
        wf_mod_cmdAttesa.erroreRisposta = 0;
        wf_mod_cmdAttesa.codiceErrore = 0;
        wf_mod_cmdAttesa.retryCmd = 0;
        wf_tentativo = 0;				
        err = M_OK_AT_RESP;
      }
      else
      {
        //-- Risposta NON trovata --
        //Ricerca errore: limito la ricerca sull'ultima retry di invio comando
        if(searchString(wf_mod_buff_Risposta, error, 0, sizeof(wf_mod_buff_Risposta),/*wf_mod_buff_Risposta_idx,*/ &errorOffset, 1, 1) &&
           wf_mod_cmdAttesa.retryCmd>=wf_mod_cmdAttesa.lastCmd.maxRetryCmd)
        {
          wf_mod_cmdAttesa.attesaRisposta = 0;
          wf_mod_cmdAttesa.erroreRisposta = 1;
          wf_mod_cmdAttesa.codiceErrore = convertStrToLong(&wf_mod_buff_Risposta[errorOffset], errorOffset, sizeof(wf_mod_buff_Risposta));
          wf_mod_cmdAttesa.retryCmd = 0;
          wf_tentativo = 0;
          err = M_NO_AT_RESP;   //errore trovato
        }
        else
          wf_tentativo++;       //errore NON trovato
      }
    }
  }
  
  if(wf_tentativo >= maxRetries)
  {
    //-- Risposta NON trovata @ fine ricerca risposta --
    wf_mod_cmdAttesa.attesaRisposta = 0;
    wf_mod_cmdAttesa.erroreRisposta = 1;
    wf_mod_cmdAttesa.codiceErrore = MOD_DFLT_ERROR;    
    wf_tentativo = 0;
    
    if(wf_mod_cmdAttesa.retryCmd < wf_mod_cmdAttesa.lastCmd.maxRetryCmd)
      err = M_NO_AT_RESP_RETRY;         //risposta NON trovata, retry invio comando
    else
    {
      wf_mod_cmdAttesa.retryCmd = 0;
      err = M_NO_AT_RESP;		//risposta NON trovata
    }
  }
  
  return err;
}
