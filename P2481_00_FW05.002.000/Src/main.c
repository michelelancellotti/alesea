/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "crc.h"
#include "i2c.h"
#include "lptim.h"
#include "rtc.h"
#include "usart.h"
#include "wwdg.h"
#include "gpio.h"
#include "lis2mdl_reg.h"
#include "lis2mdl_SM.h"
#include "nfc_SM.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "LSM6DSL.h"
#include "LSM6DSL_driver.h"
#include "Gestione_dato.h"
#include "communication_SM.h"    
#include "modem_SM.h"   
#include "EEPROM.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nfc.h"
#include "crc.h"
#include "Gestione_batteria.h"
#include "Alesea_ciclo_vita.h"
#include "Alesea_init.h"
#include "Globals.h"
#include "funzioni.h"
#include "wifi_SM.h"
#include "LSM6DSL_SM.h"
#include "LIS2MDL.h"
#include "wifi.h"
    
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */


/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void clean_context(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/******************************************************************************
		V A R I A B L E S
******************************************************************************/

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  
  //Stabilizzazione clock (1s)
  for(uint8_t i = 0; i < 10; i++) 
    HAL_Delay(100);
  
  clean_context();
  
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  #ifndef DEBUG_ON   
  MX_WWDG_Init();           
  #endif
  MX_I2C1_Init();       
  MX_RTC_Init();
  MX_USART1_UART_Init();  
  MX_CRC_Init();
  
  InitVersioni();  
  
  #ifdef DEBUG_ON             
  VersioneHW = VERSIONE_HW_4_X;//VERSIONE_HW_3_X;       //TODO: rimuovere, DEBUG ONLY
  SottoversioneHW = SOTTO_VERSIONE_HW_4_X_COBIRA;//SOTTO_VERSIONE_HW_4_X_BASE;         //TODO: rimuovere, DEBUG ONLY
  #endif
  /* USER CODE BEGIN 2 */
  if(VersioneHW == VERSIONE_HW_4_X)
  {
    MX_GPIO_HW4_Init();
    MX_USART2_UART_Init(); 
    MX_I2C3_Init();
  }
  else /*if(VersioneHW == VERSIONE_HW_DEFAULT_BG96)*/
    MX_GPIO_HW3_Init();

  if(VersioneHW == VERSIONE_HW_4_X)
    HAL_GPIO_WritePin(LED1_HW_4_GPIO_Port, LED1_HW_4_Pin, GPIO_PIN_SET);    //led di segnalazione ON
  else /*if(VersioneHW == VERSIONE_HW_DEFAULT_BG96)*/
  {
    HAL_GPIO_WritePin(LED1_HW3_GPIO_Port, LED1_HW3_Pin, GPIO_PIN_SET);    //led di segnalazione ON
    HAL_GPIO_WritePin(LED2_HW3_GPIO_Port, LED2_HW3_Pin, GPIO_PIN_SET);    //led di segnalazione ON
    HAL_GPIO_WritePin(LED3_HW3_GPIO_Port, LED3_HW3_Pin, GPIO_PIN_SET);    //led di segnalazione ON
  }

  LS6DSL_CaricaSetup();
  InitLSM6DSL();         
  
#ifdef FREE_FALL_ON
  SetInterruptFreeFall(FALL_DUR_STOP_MODE); 
#endif
  
  /* USER CODE END 2 */
  
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  //////////////////////////////////////////////
  //gestione allineamento EEPROM tra 4.x e 5.x//
  //////////////////////////////////////////////
  
  if(VersioneHW == VERSIONE_HW_3_X)
  {
    if( (EEPROM_ReadByte(EEPROM_CHIAVE_FW_1) != CHIAVE_FW)  &&  (EEPROM_ReadByte(EEPROM_CHIAVE_FW_2) != CHIAVE_FW)  &&  (EEPROM_ReadByte(EEPROM_CHIAVE_FW_3) != CHIAVE_FW)  )     //se almeno una dell chiavi � coerente non allineo
    {
      AllineamentoEEPROM();
    }
  }
  //////////////////////////////////////////////
  //fine allineamento                         //
  //////////////////////////////////////////////
  
  
  InitCollaudo();
  InitDati();
  
  
  ImpostaRisveglio(Dati.ActivationTiming.SchedulingInterval);
  StatoMAG = MAG_LOW_POWER;
  modem_Init((Mod_t)Dati.TipoModem, VersioneHW, SottoversioneHW);       //init libreria modem
  Init_SCI(SERIALE_MODEM);
  
  
  if(VersioneHW == VERSIONE_HW_4_X)
  {
    wifi_Init(WF_MOD_ESP_WROOM_02);       //Init libreria WiFi
    Init_SCI(SERIALE_WIFI);
    HAL_GPIO_WritePin(MDM_ON_OFF_HW4_GPIO_Port, MDM_ON_OFF_HW4_Pin, GPIO_PIN_RESET);    //tengo spento il modem
    HAL_GPIO_WritePin(WIFI_ON_OFF_HW4_GPIO_Port, WIFI_ON_OFF_HW4_Pin, GPIO_PIN_RESET);  //tengo spento il modulo WiFi
    HAL_GPIO_WritePin(WIFI_EN_HW4_GPIO_Port, WIFI_EN_HW4_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LED1_HW_4_GPIO_Port, LED1_HW_4_Pin, GPIO_PIN_RESET);  //led di segnalazione OFF
  }
  else /*if(VersioneHW == VERSIONE_HW_DEFAULT_BG96)*/
  {
    HAL_GPIO_WritePin(LED1_HW3_GPIO_Port, LED1_HW3_Pin, GPIO_PIN_RESET);    //led di segnalazione OFF
    HAL_GPIO_WritePin(LED2_HW3_GPIO_Port, LED2_HW3_Pin, GPIO_PIN_RESET);    //led di segnalazione OFF
    HAL_GPIO_WritePin(LED3_HW3_GPIO_Port, LED3_HW3_Pin, GPIO_PIN_RESET);    //led di segnalazione OFF
  }
  
  FlagForceGpsOn = 1;  //forzo attivazione GPS
  if( (Dati.StatoAlesea != ALESEA_READY) && (Dati.StatoAlesea != ALESEA_PRE_COLLAUDO) )
  {
    FlagWakeupDaRTC = 1; //forzo una trasmissione dopo un reset   	
  }
  //Imposto all'avvio un risveglio pari a 1 anno di default per non avere l'intervento continuo dell'interrupt del RTC
  if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, LIMITE_SCHEDULING_INTERVAL/HZ_RTC, RTC_WAKEUPCLOCK_CK_SPRE_16BITS) != HAL_OK)
  {
    Error_Handler();
  }
  InitConsumiFasi();
  
  CleanFlagInterrupt();

  
  while (1)
  {		
    /* USER CODE END WHILE */
    
    /* USER CODE BEGIN 3 */  
    RefreshWD();  
    BaseDeiTempi();
  }
  /* USER CODE END 3 */
}

/**
* @brief System Clock Configuration
* @retval None
*/
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
  
  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV16;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_I2C3
                              |RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_LPTIM1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_HSI;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.LptimClockSelection = RCC_LPTIM1CLKSOURCE_PCLK;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void clean_context(void)
{
  /* Disable DMA IT */
  DMA1_Channel2->CCR &= ~(DMA_IT_TC | DMA_IT_HT | DMA_IT_TE);
  DMA1_Channel3->CCR &= ~(DMA_IT_TC | DMA_IT_HT | DMA_IT_TE);
  /* Disable the channel */
  DMA1_Channel2->CCR &=  ~DMA_CCR_EN;
  DMA1_Channel3->CCR &=  ~DMA_CCR_EN;
  
  if (HAL_IS_BIT_SET(USART1->CR3, USART_CR3_DMAR))
  {
    CLEAR_BIT(USART1->CR3, USART_CR3_DMAR);
  }
}



/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
