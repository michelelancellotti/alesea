/**
******************************************************************************
* File          	: az_hmac_sha256.h
* Versione libreria	: 0.01
* Descrizione        	: Calcolo firma messaggio HMAC (keyed-Hash Message Authentication Code).
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_HMAC_SHA256_H
#define __AZ_HMAC_SHA256_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
#include "az_sha256.h"
  
/* Defines */

  
/* Types & global variables */
  
/* This structure will hold context information for the HMAC keyed hashing operation. */
typedef struct HMACContext {
  int hashSize;                                         //hash size of SHA being used
  int blockSize;                                        //block size of SHA being used
  SHA256Context shaContext;                             //SHA context
  unsigned char k_opad[SHA256_Message_Block_Size];      //outer padding - key XORd with opad
} HMACContext;  
  
/* Global functions */
extern int hmac_sha256(const Az_Fixed_Buffer_t* message, const Az_Fixed_Buffer_t* key, uint8_t digest[SHA256HashSize]);
  
/* Global variables */

  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_HMAC_SHA256_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
