/**
******************************************************************************
* File          	: az_sas_token.h
* Versione libreria	: 0.01
* Descrizione        	: Composizione SAS token.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_SAS_TOKEN_H
#define __AZ_SAS_TOKEN_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
  
/* Defines */
#define UT_EMERGENCY            2840140800      //Unix Time @ 01/01/2060 00:00:00

extern const Az_Fixed_Buffer_t SCOPE_ID;
extern const Az_Fixed_Buffer_t REGISTRATIONS;
  
/* Types & global variables */
typedef enum {
  STANDARD_TOKEN        = 0,    //Token per connessione a servizi IoT Central
  DPS_TOKEN,            //1     //Token per connessione a servizi DPS
} Az_SAS_Token_t;

extern uint8_t az_sas_token_buff[AZ_MAX_SAS_TOKEN];
extern Az_Fixed_Buffer_t signature;
  
/* Global functions */
extern void az_calculate_sas_token(Az_Client_t* client, uint32_t unix_time, uint16_t token_duration_hh, Az_SAS_Token_t token_type, 
                                   Az_Buffer_t* sas_token);
  
/* Global variables */

  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_SAS_TOKEN_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
