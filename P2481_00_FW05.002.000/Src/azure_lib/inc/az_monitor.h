/**
*****************************************************************************************
* File          	: az_monitor.h
* Versione libreria	: 0.01
* Descrizione        	: Gestione controllo connessione verso IoT Central.
*****************************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_MONITOR_H
#define __AZ_MONITOR_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
  
/* Defines */
#define AZ_CHECK_CONN_MAX_FAIL          5       //Numero MAX tentativi connessione verso IoT Central prima di rifare DPS
  
/* Types & global variables */
typedef enum {
  AZ_CHECK_CONN_IDLE            = 0x00,
  AZ_CHECK_CONN_RUN,            //0x01
} Az_CheckConn_t;
  
/* Global functions */
extern void az_process_check_conn(void);
extern void az_set_check_conn_status(Az_CheckConn_t new_status);
extern Az_CheckConn_t az_get_check_conn_status(void);
  
/* Global variables */
extern uint8_t az_conn_fail_count;
  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_MONITOR_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
