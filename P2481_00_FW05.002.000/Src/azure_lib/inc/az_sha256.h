/**
******************************************************************************
* File          	: az_sha256.h
* Versione libreria	: 0.01
* Descrizione        	: Algoritmo di hashing SHA-256.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_SHA256_H
#define __AZ_SHA256_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
  
/* Defines */
#define SHA256HashSize                  32      //[B], dimensione risultato hashing SHA-256
#define SHA256_Message_Block_Size       64      //[B], dimensione blocco per SHA-256
  
/* Types & global variables */

/* All SHA functions return one of these values. */
enum {
  shaSuccess            = 0x00,
  shaNull               = 0x01, //null pointer parameter
  shaInputTooLong       = 0x02, //input data too long
  shaStateError         = 0x04, //called 'SHA_256_Input' after 'SHA_256_Result'
  shaBadParam           = 0x08, //passed a bad parameter
}; 


/* This structure will hold context information for the SHA-256 hashing operation. */
typedef struct SHA256Context {
  uint32_t Intermediate_Hash[SHA256HashSize/4];         //Message Digest  
  uint32_t Length_Low;                                  //Message length in bits
  uint32_t Length_High;                                 //Message length in bits  
  int_least16_t Message_Block_Index;                    //Message_Block array index  
  uint8_t Message_Block[SHA256_Message_Block_Size];     //512-bit message blocks  
  int Computed;                                         //Is the digest computed ?
  int Corrupted;                                        //Is the digest corrupted ?
} SHA256Context;
  
/* Global functions */
extern int SHA_256_Reset(SHA256Context* context);
extern int SHA_256_Input(SHA256Context* context, const uint8_t* message_array, unsigned int length);
extern int SHA_256_Result(SHA256Context* context, uint8_t Message_Digest[]);
  
/* Global variables */

  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_SHA256_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
