/**
******************************************************************************
* File          	: az_utils.h
* Versione libreria	: 0.01
* Descrizione        	: Funzioni di appoggio libreria Azure.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_UTILS_H
#define __AZ_UTILS_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
  
/* Defines */
/* --------------------- Definizione topic MQTT --------------------- */ 
#define AZ_TOPIC_PUB_TELEMETRY                  "devices/%s/messages/events/"                                                   //Publish telemetry (segnaposto %s � per il <device-id>)
#define AZ_TOPIC_PUB_GET_DESIRED_PROPS          "$iothub/twin/GET/?$rid=%d"                                                     //Publish per ottenere lista propriet� desired (segnaposto %d � per <request-id>)
#define AZ_TOPIC_PUB_UPDATE_REPORTED_PROPS      "$iothub/twin/PATCH/properties/reported/?$rid=%d"                               //Publish per aggiornare lista propriet� reported (segnaposto %d � per <request-id>)
#define AZ_TOPIC_PUB_DPS_REGISTRATION           "$dps/registrations/PUT/iotdps-register/?$rid=%d"                               //Publish per registrazione a DPS (segnaposto %d � per <request-id>)
#define AZ_TOPIC_PUB_DPS_GET_STATUS             "$dps/registrations/GET/iotdps-get-operationstatus/?$rid=%d&operationId=%s"     //Publish per controllo stato registrazione a DPS (segnaposto %d � per <request-id>, %s � per <operation-id>)
#define AZ_TOPIC_SUB_DEVTWINS                   "$iothub/twin/res/#"                                                            //Subscribe ricezione eventi per aggiornamento device-twins
#define AZ_TOPIC_SUB_DPS                        "$dps/registrations/res/#"                                                      //Subscribe ricezione eventi DPS
/* ------------------------------------------------------------------ */
  
/* Types & global variables */
  
  
/* Inline functions */
inline uint8_t _az_number_to_upper_hex(uint8_t number)           /* Converte un numero in input [0..15] nella sua rappresentazione esadecimale (come carattere). */
{
  return (uint8_t)(number + (number < 10 ? '0' : ('A' - 10)));
}

inline uint8_t _az_is_byte_letter(uint8_t c)                    /* Verifica se il carattere in input � una lettera [A-Za-z]. */
{ 
  return (('A'<=c && c<='Z') || ('a'<=c && c<='z')); 
}

inline uint8_t _az_is_byte_digit(uint8_t c)                     /* Verifica se il carattere in input � un numero [0-9]. */
{ 
  return ('0'<=c && c<='9'); 
}
  
/* Global functions */
extern uint16_t az_get_free_space(const Az_Buffer_t* src);
extern Az_Err_t az_append_string(const Az_Fixed_Buffer_t* src, 
                                 Az_Buffer_t* dest);
extern Az_Err_t az_append_number(uint32_t number, 
                                 Az_Buffer_t* dest);
extern Az_Err_t az_append_buffer(const Az_Buffer_t* src, 
                                 Az_Buffer_t* dest);
extern Az_Err_t az_append_terminator(Az_Buffer_t* dest);
extern Az_Fixed_Buffer_t az_convert_buffer_2_fixed_buffer(Az_Buffer_t* src);
extern void az_clear_buffer_fast(Az_Buffer_t* src);
extern uint8_t az_find_key(const uint8_t* key, Az_Buffer_t* src, 
                           uint16_t* offset);
extern Az_Err_t az_find_value_string(const uint8_t* key, Az_Buffer_t* src,
                                     Az_Buffer_t* dest);
extern Az_Err_t az_url_encode(const Az_Fixed_Buffer_t* src, 
                              Az_Buffer_t* dest);
extern Az_Err_t az_set_topic(Az_PubSubEvnt_t event, 
                             Az_Buffer_t* topic);
  
/* Global variables */

  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_UTILS_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
