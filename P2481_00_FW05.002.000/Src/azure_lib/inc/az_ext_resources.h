/**
******************************************************************************
* File          	: az_ext_resources.h
* Versione libreria	: 0.01
* Descrizione        	: Risorse esterne alla libreria Azure.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_EXT_RESOURCES_H
#define __AZ_EXT_RESOURCES_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
#include "modem_utils.h"
#include "softTimer.h"
  
/* Defines */

  
/* Types & global variables */

  
/* Global functions */
extern Az_Op_Callback_t az_op_callback;
extern Az_DevTwins_Callback_t az_dev_twins_callback;
  
/* Global variables */
extern Az_Fixed_Buffer_t az_device_id;

extern Az_Buffer_t az_publish_topic;
extern Az_Buffer_t az_subscribe_topic;
extern Az_Buffer_t az_publish_data;
extern Az_Buffer_t az_subscribe_data;
extern Az_Buffer_t az_received_topic;
extern Az_Buffer_t az_devkey_data;
extern Az_Buffer_t az_calculated_devkey_data;

extern Az_Property_List_t az_properties;
  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_EXT_RESOURCES_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
