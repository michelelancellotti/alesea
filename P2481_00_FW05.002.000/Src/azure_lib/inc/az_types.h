/**
******************************************************************************
* File          	: az_types.h
* Versione libreria	: 0.01
* Descrizione        	: Data-types libreria Azure.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_TYPES_H
#define __AZ_TYPES_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include <string.h>
  
/* Defines */
/* #########################################################################################################################################
 * Tipologia ambiente (backend):
*/
#define AZ_DEV                  0       //Ambiente SVILUPPO
#define AZ_PROD                 1       //Ambiente PRODUZIONE

#define AZURE_ENVIRONMENT       AZ_PROD

#if ((AZURE_ENVIRONMENT!=AZ_DEV) && (AZURE_ENVIRONMENT!=AZ_PROD))
#error "Configurazione non valida."
#endif
/* ######################################################################################################################################### */

  
  
#define AZ_OPT_IS_TRIGGER               0x01    //Az_Property_Option_t @ [b0]
  
  
/* ---------------- Parametri buffer libreria Azure ---------------- */
/* Dimensione MAX buffer appoggio per calcolo SAS token.
*  Usato per:
*  - salvataggio temporaneo resource URI
*  - salvataggio temporaneo codifica base 64 della signature (HMAC SHA-256)
*/
#define AZ_MAX_SAS_SUPPORT_BUFFER       128     //[B]

/* Dimensione MAX SAS token. */  
#define AZ_MAX_SAS_TOKEN                256     //[B]
  
/* Dimensione MAX host-name IoT Hub. */  
#define AZ_MAX_IOT_HUB                  80      //[B]

/* Dimensione MAX chiave univoca dispositivo. */   
#define AZ_MAX_DEVICE_KEY               64      //[B]
  
/* Dimensione MAX user-name MQTT. */  
#define AZ_MAX_USER_NAME                (AZ_MAX_IOT_HUB+48)      //[B]  

#if (AZ_MAX_SAS_TOKEN<AZ_MAX_SAS_SUPPORT_BUFFER || AZ_MAX_SAS_TOKEN<AZ_MAX_IOT_HUB)  
#error "Azure parameter error ..."
#endif
/* ----------------------------------------------------------------- */
  
/* Types & global variables */
typedef enum {
  AZ_NO_ERROR                   = 0x00,
  AZ_ERR_APPEND_STRING          = 0xFF,         /* Errore append stringa: dimensione buffer insufficiente. */
  AZ_ERR_APPEND_NUMBER          = 0xFE,         /* Errore append numero: dimensione buffer insufficiente. */
  AZ_ERR_APPEND_BUFFER          = 0xFD,         /* Errore copy buffer: dimensione buffer insufficiente. */
  AZ_ERR_APPEND_TERMINATOR      = 0xFC,         /* Errore append terminatore stringa: dimensione buffer insufficiente. */
  AZ_ERR_ENCODE_BASE_64         = 0xFB,         /* Errore encoding base 64: dimensione buffer insufficiente. */
  AZ_ERR_DECODE_BASE_64         = 0xFA,         /* Errore decoding base 64: dimensione buffer insufficiente. */
  AZ_ERR_DECODE_BASE_64_PAR     = 0xF9,         /* Errore decoding base 64: errore parametro input. */
  AZ_ERR_HMAC_SHA_256           = 0xF8,         /* Errore HMAC SHA-256: errore hashing. */  
  AZ_ERR_URL_ENCODING           = 0xF7,         /* Errore URL-encoding: dimensione buffer insufficiente. */
  AZ_ERR_UNXPECTED              = 0xF6,         /* Errore imprevisto: tipo di dato non gestito. */
  AZ_ERR_BUILD_TOPIC            = 0xF5,         /* Errore costruzione topic MQTT: dimensione buffer insufficiente. */
  AZ_ERR_MQTT_CALLBACK          = 0xF4,         /* Errore esecuzione callback MQTT. */
  AZ_ERR_LIB_INIT               = 0xF3,         /* Errore inizializzazione libreria Azure. */
  AZ_ERR_FIND_VALUE_STRING      = 0xF2,         /* Errore ricerca valore campo JSON (stringa): chiave non trovata. */
  AZ_ERR_PROCESS_BUSY           = 0xF1,         /* Errore processo background: processo giÓ avviato. */
  AZ_ERR_MQTT_OPERATION         = 0xF0,         /* Errore operazione MQTT precedentemente eseguita. */
  AZ_ERR_PROPERTIES_INIT        = 0xEF,         /* Errore inizializzazione lista proprietÓ (applicazione). */
  AZ_ERR_DEVTWINS_PARSING       = 0xEE,         /* Errore parsing messaggio MQTT device-twins. */
  AZ_ERR_DPS_BUILD_MESSAGE      = 0xED,         /* Errore costruzione payload per DPS. */
  AZ_ERR_DPS_PARSING            = 0xEC,         /* Errore parsing messaggio MQTT DPS. */
  AZ_CONTINUE_DPS_POLLING       = 0xEB,         /* Registrazione DPS in corso: continuare il polling. */
  AZ_ERR_DPS_POLLING            = 0xEA,         /* Errore polling stato registrazione DPS: raggiunto numero MAX retry. */
  AZ_ERR_READ_DEVKEY            = 0xE9,         /* Errore lettura chiave individuale dispositivo da memoria modem. */
  AZ_ERR_HTTP_DEVKEY            = 0xE8,         /* Errore API HTTP per richiesta chiave individuale dispositivo. */
  AZ_ERR_NULL_STRING            = 0xE7,         /* Errore ricerca valore campo JSON (stringa): stringa nulla non ammessa. */
  AZ_ERR_BUILD_DEVKEY_MESSAGE   = 0xE6,         /* Errore costruzione messaggio per scrittura su file di chiave individuale dispositivo (calcolata). */
  AZ_ERR_WRITE_DEVKEY           = 0xE5,         /* Errore scrittura chiave individuale dispositivo (calcolata) su memoria modem. */
} Az_Err_t;

typedef enum {
  AZ_OP_MQTT_CONNECT            = 0x00,
  AZ_OP_PUBLISH,                //0x01
  AZ_OP_SUBSCRIBE,              //0x02
  AZ_OP_DATA_RECEIVED,          //0x03  
  AZ_OP_MQTT_DISCONNECT,        //0x04
  AZ_OP_READ_DEVKEY,            //0x05
  AZ_OP_HTTP_DEVKEY,            //0x06
  AZ_OP_WRITE_DEVKEY,           //0x07
} Az_Operation_t;

typedef enum {
  //------------------------ PUBLISH ------------------------
  AZ_EV_PUB_TELEMETRY                   = 0x00,         //Publish servizi IoT Central: telemetria
  AZ_EV_PUB_GET_DESIRED_PROPS,          //0x01          //Publish servizi IoT Central: ricezione lista proprietÓ desired
  AZ_EV_PUB_UPDATE_REPORTED_PROPS,      //0x02          //Publish servizi IoT Central: aggiornamento lista proprietÓ reported
  AZ_EV_PUB_DPS_REGISTRATION,           //0x03          //Publish servizi DPS: registrazione device
  AZ_EV_PUB_DPS_GET_STATUS,             //0x04          //Publish servizi DPS: controllo stato registrazione
  //------------------------ SUBSCRIBE ----------------------
  AZ_EV_SUB_DEVTWINS_RESPONSE,          //0x05          //Subscribe servizi IoT Central: aggiornamento device-twins
  AZ_EV_SUB_DPS_RESPONSE,               //0x06          //Subscribe servizi DPS
} Az_PubSubEvnt_t;


typedef union {
  uint16_t WORD;
  struct {
    uint8_t LIB_INIT:1;                 //[b0] 0=libreria Azure inizializzata, 1=libreria Azure NON inizializzata
    uint8_t USE_BKP_UNIX_TIME:1;        //[b1] 0=SAS token calcolato con unix-time corrente, 1=SAS token calcolato con unix-time di backup
    uint8_t USE_EMERGENCY_UNIX_TIME:1;  //[b2] 0=SAS token calcolato con unix-time corrente, 1=SAS token calcolato con unix-time di emergenza (nel futuro)
    uint8_t ERR_APPEND_STRING:1;        //[b3] 0=nessun errore append stringa, 1=almeno un errore append stringa
    uint8_t ERR_APPEND_NUMBER:1;        //[b4] 0=nessun errore append numero, 1=almeno un errore append numero
    uint8_t ERR_APPEND_BUFFER:1;        //[b5] 0=nessun errore copy buffer, 1=almeno un errore copy buffer
    uint8_t ERR_APPEND_TERMINATOR:1;    //[b6] 0=nessun errore append terminatore stringa, 1=almeno un errore append terminatore stringa
    uint8_t ERR_ENCODE_BASE_64:1;       //[b7] 0=nessun errore encoding base 64, 1=almeno un errore encoding base 64
    uint8_t ERR_DECODE_BASE_64:1;       //[b8] 0=nessun errore decoding base 64 (dimensione buffer), 1=almeno un errore decoding base 64 (dimensione buffer)
    uint8_t ERR_DECODE_BASE_64_PAR:1;   //[b9] 0=nessun errore decoding base 64 (parametro input), 1=almeno un errore decoding base 64 (parametro input)
    uint8_t ERR_HMAC_SHA_256:1;         //[b10] 0=nessun errore hashing HMAC SHA-256, 1=almeno un errore hashing HMAC SHA-256
    uint8_t ERR_URL_ENCODING:1;         //[b11] 0=nessun errore URL-encoding, 1=almeno un errore URL-encoding
    uint8_t ERR_UNEXPECTED:1;           //[b12] 0=nessun errore imprevisto (tipo di dato non gestito), 1=almeno un errore imprevisto (tipo di dato non gestito)
    uint8_t ERR_BUILD_TOPIC:1;          //[b13] 0=nessun errore costruzione topic MQTT, 1=almeno un errore imprevisto (tipo di dato non gestito)
    uint8_t ERR_MQTT_CALLBACK:1;        //[b14] 0=nessun errore esecuzione callback MQTT, 1=almeno un errore esecuzione callback MQTT
    uint8_t ERR_PROCESS_BUSY:1;         //[b15] 0=nessun errore di avvio processi in background, 1=almeno un errore di avvio processi in background
  } BIT;
} Azure_Reg_t;

typedef union {
  uint16_t WORD;
  struct {
    uint8_t DEVTWINS_START:1;                   //[b0] 0=processo aggiornamento device-twins non iniziato, 1=processo aggiornamento device-twins iniziato
    uint8_t DEVTWINS_GET_DESIRED_PROPS:1;       //[b1] 1=passaggio da stato di GET lista proprietÓ 'desired'
    uint8_t DEVTWINS_UPDATE_REPORTED_PROPS:1;   //[b2] 1=passaggio da stato di aggiornamento lista proprietÓ 'reported'
    uint8_t DEVTWINS_ERR_PARSING:1;             //[b3] 0=nessun errore parsing messaggi device-twins, 1=errore parsing messaggi device-twins 
    uint8_t ERR_OPCODE:1;                       //[b4] 0=nessun errore codice operazione, 1=errore codice operazione
    uint8_t DEVTWINS_ONLY_ONE:1;                //[b5] 0=presenza di 'desired' e 'reported' per ogni proprietÓ, 1=assenza di 'desired' o 'reported' per almeno una proprietÓ
    uint8_t DPS_DEV_REGISTERED:1;               //[b6] 0=registrazione DPS non completata, 1=registrazione DPS completata
    uint8_t DPS_START:1;                        //[b7] 0=processo DPS non iniziato, 1=processo DPS iniziato
    uint8_t DPS_READ_DEVKEY:1;                  //[b8] 1=passaggio da stato di lettura chiave dispositivo (da file modem)
    uint8_t DPS_ERR_DEVKEY_CHECKSUM:1;          //[b9] 0=nessun errore verifica checksum chiave dispositivo, 1=errore verifica checksum chiave dispositivo
    uint8_t DPS_CONNECT:1;                      //[b10] 1=passaggio da stato di connessione a servizio DPS
    uint8_t DPS_HTTP_DEVKEY:1;                  //[b11] 1=passaggio da stato di HTTP (API download chiave individuale)
    uint8_t DPS_REGISTER:1;                     //[b12] 1=passaggio da stato di invio messaggio registrazione a DPS
    uint8_t DPS_CHECK_REGISTRATION:1;           //[b13] 1=passaggio da stato di controllo registrazione a DPS     
    uint8_t DPS_ERR_TIMEOUT:1;                  //[b14] 0=richiesta registrazione DPS entro il timeout, 1=raggiunto timeout richiesta registrazione DPS
    uint8_t DPS_ERR_PARSING:1;                  //[b15] 0=nessun errore parsing messaggi DPS, 1=errore parsing messaggi DPS    
  } BIT;
} Azure_Reg_Ext_t;
  
  
typedef struct {
  uint16_t size;        //[B] Dimensione buffer (per le stringhe utilizzare la dimensione netta, ovvero escluso il terminatore)
  uint8_t* buffer;      //Riferimento buffer
} Az_Fixed_Buffer_t;


typedef struct {
  uint16_t size;        //[B] Dimensione totale (MAX) buffer
  uint16_t offset;      //Offset scrittura buffer [B]
  uint8_t* buffer;      //Riferimento buffer
} Az_Buffer_t;


typedef struct {
  Az_Fixed_Buffer_t* hostname;          //Hostname IoT Hub
  Az_Fixed_Buffer_t* device_id;         //ID dispositivo
  Az_Fixed_Buffer_t* device_key;        //Chiave univoca dispositivo
} Az_Client_t;


typedef struct {
/* Callback esecuzione operazione. 
*
*@param[IN] op: tipo operazione da eseguire
*
*return: codice errore: 
*AZ_NO_ERROR, callback eseguita
*ALTRI ERRORI, callback non eseguita 
*/
  Az_Err_t (*exec_operation)(Az_Operation_t op);
/* Callback lettura stato operazione. 
*
*@param[IN] op: tipo operazione di cui richiedere lo stato
*
*return: codice errore: 
*AZ_NO_ERROR, stato operazione = OK
*ALTRI ERRORI, stato operazione = ERRORE
*/  
  Az_Err_t (*operation_status)(Az_Operation_t op);          //Callback lettura stato operazione
} Az_Op_Callback_t;


//################################ DEVICE-TWINS ################################
typedef enum {
  AZ_JS_MAIN_KEY        = 0,            //Chiave oggetto JSON
  AZ_JS_F1_KEY,         //1             //Chiave campo 1
  AZ_JS_F2_KEY,         //2             //Chiave campo 2
  AZ_JS_KEYS_NUM        //3             //Numero MAX chiavi oggetto JSON: chiave oggetto + chiavi campi (eventuali)
} AZ_JSON_Key_t;  

typedef enum {
  AZ_U32                = 0,            //Intero a 4B (senza segno)
  AZ_OBJ_BOOL_U32,      //1             //Oggetto: 1 campo boolean + 1 campo intero 4B (senza segno)
  AZ_OBJ_U32_U32,       //2             //Oggetto: 1 campo intero 4B + 1 campo intero 4B (senza segno)
  AZ_BOOL,              //3
} Az_Schema_t;


typedef struct {
  uint8_t BOOL;
  uint32_t U32;
} Az_Object_BOOL_U32;

typedef struct {
  uint32_t U32_0;
  uint32_t U32_1;
} Az_Object_U32_U32;

typedef union {
  uint8_t               BOOL;
  uint32_t              U32;
  Az_Object_BOOL_U32    OBJ_BOOL_U32;
  Az_Object_U32_U32     OBJ_U32_U32;
} Az_Property_Value_t;


typedef union {
  uint8_t BYTE;
  struct {
    uint8_t OPT_IS_TRIGGER:1;           //[b0]: 0=disabilita la gestione a trigger della proprietÓ, 1=abilita la gestione a trigger della proprietÓ
  } BIT;
} Az_Property_Option_t;

typedef union {
  uint8_t BYTE;
  struct {
    uint8_t DESIRED_RX:1;               //[b0]: 0=valore 'desired' NON ricevuto, 1=valore 'desired' ricevuto
    uint8_t REPORTED_RX:1;              //[b1]: 0=valore 'reported' NON ricevuto, 1=valore 'reported' ricevuto
    uint8_t MISMATCH:1;                 //[b2]: 0=valore 'desired' uguale a valore 'reported', 1=valore 'desired' diverso da valore 'reported'              
  } BIT;
} Az_Property_Reg_t;


typedef struct {
  const uint8_t* keys[AZ_JS_KEYS_NUM];  //Lista chiavi oggetto JSON (chiave oggetto + chiavi campi [eventuali])
  const Az_Schema_t schema;             //Tipologia oggetto JSON
  const Az_Property_Option_t option;    //Opzioni gestione proprietÓ
  Az_Property_Reg_t reg;                //Registro flag gestione proprietÓ
  Az_Property_Value_t desired;          //Valore proprietÓ 'desired'
  Az_Property_Value_t reported;         //Valore proprietÓ 'reported'
} Az_Property_t;


typedef struct {
  Az_Property_t* list;                  //Handle lista proprietÓ
  uint8_t num;                          //Numero elementi lista proprietÓ
} Az_Property_List_t;


typedef struct {
/* Callback sincronizzazione tra dati applicazione e valori proprietÓ 'desired' (e.g. esecuzione comandi). */
  void (*sync_app_data)(void);
/* Callback sincronizzazione tra valori proprietÓ 'reported' e dati applicazione. */
  void (*sync_reported_props)(void);
} Az_DevTwins_Callback_t;
  
/* Global functions */
  
  
/* Global variables */
extern Azure_Reg_t azure_reg;
extern Azure_Reg_Ext_t azure_reg_ext;
  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_TYPES_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
