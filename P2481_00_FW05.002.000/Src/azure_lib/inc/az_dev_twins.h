/**
******************************************************************************
* File          	: az_dev_twins.h
* Versione libreria	: 0.01
* Descrizione        	: Gestione aggiornamento device twins Azure.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_DEV_TWINS_H
#define __AZ_DEV_TWINS_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
  
/* Defines */
#define AZ_DEVTWINS_OP_RESP_OK                  200     //operation response: success
#define AZ_DEVTWINS_OP_RESP_TOO_REQUESTS        429     //operation response: too many requests (throttled)
#define AZ_DEVTWINS_OP_RESP_SERVER_ERR          500     //operation response: server errors [5**]

#define AZ_REPORTED                             0  
#define AZ_DESIRED                              1

#define AZ_MAX_PROP_LENGTH                      8       //profonditÓ ricerca valore proprietÓ
    
/* Types & global variables */
typedef enum {
  AZ_DEVTWINS_IDLE                      = 0x00,
  AZ_DEVTWINS_SUBSCRIBE,                //0x01
  AZ_DEVTWINS_GET_DESIRED_PROPS,        //0x02
  AZ_DEVTWINS_UPDATE_REPORTED_PROPS,    //0x03
  AZ_DEVTWINS_OPERATION_STATUS,         //0x04  
} Az_DevTwins_t;
  
/* Global functions */
extern void az_init_dev_twins(void);
extern void az_process_dev_twins(void);
extern void az_set_dev_twins_status(Az_DevTwins_t new_status);
extern Az_DevTwins_t az_get_dev_twins_status(void);
  
/* Global variables */
extern uint16_t az_dev_twins_ack_count, az_dev_twins_nack_count;
  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_DEV_TWINS_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
