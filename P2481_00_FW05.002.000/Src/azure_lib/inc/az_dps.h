/**
*****************************************************************************************
* File          	: az_dps.h
* Versione libreria	: 0.01
* Descrizione        	: Gestione connessione a Device Provisioning System (DPS) Azure.
*****************************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_DPS_H
#define __AZ_DPS_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
  
/* Defines */
#define AZ_MODEL_ID                             "dtmi:iotcAlesea:ALESEA418_5t9;2"                                       //Identifico modello dispositivo  
#define AZ_PAYLOAD_DPS_REGISTRATION             "{\"registrationId\":\"%s\",\"payload\":{\"modelId\":\"%s\"}}"          //Payload publish registrazione DPS (segnaposto %s sono per <device-id> e <model-id>)
    

#define AZ_DPS_OP_RESP_OK                       200     //Operation response: success
#define AZ_DPS_OP_RESP_ACCEPTED                 202     //Operation response: accepted

#define AZ_DPS_OP_ID_SIZE                       80      //[B], dimensione MAX DPS operation-id   
#define AZ_DPS_RETRY_TIMEOUT_DFLT               3       //[s], timeout retry polling stato registrazione device
#define AZ_DPS_MAX_RETRY_TIMEOUT                10      //[s], MAX timeout retry polling stato registrazione device
#define AZ_DPS_MAX_RETRY                        3       //Numero max retry polling stato registrazione device
#define AZ_DPS_MAX_CHECKSUM_ERROR               3       //Numero MAX verifiche checksum chiave prima di rifare richiesta tramite API HTTP
    
/* Types & global variables */
typedef enum {
  AZ_DPS_IDLE                           = 0x00,
  AZ_DPS_CONNECT,                       //0x01
  AZ_DPS_SUBSCRIBE,                     //0x02
  AZ_DPS_REGISTER,                      //0x03
  AZ_DPS_CHECK_REGISTRATION,            //0x04
  AZ_DPS_OPERATION_STATUS,              //0x05
  AZ_DPS_WAIT_RETRY_TIMEOUT,            //0x06
  AZ_DPS_DISCONNECT,                    //0x07
  AZ_DPS_READ_DEVKEY,                   //0x08
  AZ_DPS_HTTP_DEVKEY,                   //0x09
  AZ_DPS_WRITE_DEVKEY,                  //0x0A
} Az_DPS_t;

typedef enum {
  AZ_DEV_UNREGISTERED_KEY_MISSING       = 0x00,         //Dispositivo NON registrato: chiave individuale mancante
  AZ_DEV_UNREGISTERED,                  //0x01          //Dispositivo NON registrato: chiave individuale presente (DPS da eseguire)
  AZ_DEV_REGISTERED,                    //0x02          //Dispositivo registrato
} Az_Provisioning_t;
  
/* Global functions */
extern void az_init_dps(void);
extern void az_process_dps(void);
extern void az_set_dps_status(Az_DPS_t new_status);
extern Az_DPS_t az_get_dps_status(void);
extern void az_set_provisioning(Az_Provisioning_t new_status);
  
/* Global variables */
extern uint8_t az_dps_operation_id[AZ_DPS_OP_ID_SIZE];
extern Az_Provisioning_t az_provisioning;

  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_DPS_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
