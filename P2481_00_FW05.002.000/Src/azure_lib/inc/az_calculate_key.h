/**
*****************************************************************************************
* File          	: az_calculate_key.h
* Versione libreria	: 0.01
* Descrizione        	: Derivazione chiave individuale dispositivo 
*                         da chiave di gruppo (emergenza).
*****************************************************************************************
*
* COPYRIGHT(c) 2023 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_CALCULATE_KEY_H
#define __AZ_CALCULATE_KEY_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
  
/* Defines */
  
/* Messaggio per scrittura su file di chiave individuale dispositivo calcolata:
 * --> segnaposto %s � per <device_key>
 * --> segnaposto %02X � per checksum
*/
#define AZ_CALCULATED_DEVKEY_MSG                \
"{\
\"device_key\":\"%s\",\
\"device_key_checksum\":\"%02X\",\
\"success\":true\
}"
    
/* Types & global variables */
typedef struct {
  uint8_t buffer[AZ_MAX_DEVICE_KEY];
  uint8_t checksum;
} Az_Calc_Key_t;
  
/* Global functions */
extern void az_calculate_device_key(Az_Fixed_Buffer_t* device_id);
extern Az_Err_t az_create_devkey_message(void);
  
/* Global variables */

  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_CALCULATE_KEY_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
