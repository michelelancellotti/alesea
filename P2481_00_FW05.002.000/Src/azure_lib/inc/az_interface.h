/**
******************************************************************************
* File          	: az_interface.h
* Versione libreria	: 0.01
* Descrizione        	: Interfaccia libreria Azure.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AZ_INTERFACE_H
#define __AZ_INTERFACE_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "az_types.h"
  
/* Defines */

  
/* Types & global variables */
typedef enum {
  AZ_SERVICE_GP_BROKER          = 0x00,         //Connessione ai servizi legacy (broker general-purpose)
  AZ_SERVICE_IOT_CENTRAL,       //0x01          //Connessione ai servizi IoT Central
  AZ_SERVICE_DPS,               //0x02          //Connessione ai servizi DPS
} Az_Service_t;

typedef enum {
  AZ_PROCESS_IDLE               = 0x00,         //Nessun processo in background attivo
  AZ_PROCESS_DEVTWINS,          //0x01          //Processo background Azure: gestione device-twins
  AZ_PROCESS_DPS,               //0x02          //Processo background Azure: gestione DPS
  AZ_PROCESS_CHECK_CONN,        //0x03          //Processo background Azure: monitor connessione IoT Central
} Az_Process_t;
  
/* Global functions */
extern Az_Err_t az_init(Az_Buffer_t publish_topic_buffer, Az_Buffer_t subscribe_topic_buffer, Az_Buffer_t received_topic_buffer, 
                        Az_Buffer_t publish_buffer, Az_Buffer_t subscribe_buffer, Az_Buffer_t devkey_read_buffer, Az_Buffer_t devkey_write_buffer,
                        Az_Op_Callback_t callback);
extern Az_Err_t az_init_properties(Az_Property_t* list, uint8_t num, 
                                   Az_DevTwins_Callback_t callback);  
extern void az_process_background(void);
extern uint8_t az_background_in_progress(void);
extern Az_Err_t az_start_background(Az_Process_t process);
extern uint8_t az_process_error(Az_Process_t process);
  
/* Global variables */

  
#ifdef __cplusplus
}
#endif
#endif /* __AZ_INTERFACE_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT "Computec srl" *****END OF FILE****/
