/**
******************************************************************************
* File          	: az_base_64.c
* Versione libreria	: 0.01
* Descrizione       	: Gestione codifica/decodifica base 64.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "az_base_64.h"
#include "az_utils.h"


#define splitInt(intVal, bytePos)       (char)((intVal >> (bytePos << 3)) & 0xFF)
#define joinChars(a, b, c, d)           (uint32_t)((uint32_t)a + ((uint32_t)b << 8) + ((uint32_t)c << 16) + ((uint32_t)d << 24))

/* ############################################################ 
* Local functions
* ############################################################
*/
static int base64toValue(char base64character, unsigned char* value);
static uint16_t numberOfBase64Characters(const Az_Fixed_Buffer_t* encodedString);
static char base64char(unsigned char val);
static char base64b16(unsigned char val);
static char base64b8(unsigned char val);




/* Esegue codifica base 64 del buffer in ingresso. 
*
*@param[IN] src, buffer sorgente
*@param[IN] enb_string_terminator:
*0 = NON appende '\0' al termine della stringa
*!0 = appende '\0' al termine della stringa
*@param[OUT] dest, buffer destinazione (risultato encoding in append)
*
*return: codice errore: 
*AZ_ERR_ENCODE_BASE_64 (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_encode_to_base64(const Az_Fixed_Buffer_t* src, uint8_t enb_string_terminator, 
                             Az_Buffer_t* dest)
{
  /*the following will happen*/
  /*1. the "data" of the binary shall be "eaten" 3 characters at a time and produce 4 base64 encoded characters for as long as there are more than 3 characters still to process*/
  /*2. the remaining characters (1 or 2) shall be encoded.*/
  /*there's a level of assumption that 'a' corresponds to 0b000000 and that '_' corresponds to 0b111111*/
  /*the encoding will use the optional [=] or [==] at the end of the encoded string, so that other less standard aware libraries can do their work*/
  
  uint16_t neededSize=0, free_space=0, currentPosition=0;    
    
  neededSize += (src->size == 0) ? (0) : ((((src->size - 1) / 3) + 1) * 4);
  neededSize += 1; //+1 perch� c'� \0 alla fine della stringa (se abilitato)
  
  free_space = az_get_free_space(dest);         //calcolo spazio libero nel buffer di destinazione
  if(free_space==0 || neededSize>free_space)
  {
    azure_reg.BIT.ERR_ENCODE_BASE_64 = 1;
    return AZ_ERR_ENCODE_BASE_64;
  }
  
  /*b0            b1(+1)          b2(+2)
  7 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0
  |----c1---| |----c2---| |----c3---| |----c4---|
  */
  
  uint8_t* dest_begin = dest->buffer + dest->offset;
  uint8_t* dest_ptr = dest_begin;
  uint8_t* dest_end = dest_begin + free_space;
  
  while(src->size >= (currentPosition + 3))
  {
    char c1 = base64char(src->buffer[currentPosition] >> 2);
    char c2 = base64char(
                         ((src->buffer[currentPosition] & 0x03) << 4) |
                           (src->buffer[currentPosition + 1] >> 4)
                             );
    char c3 = base64char(
                         ((src->buffer[currentPosition + 1] & 0x0F) << 2) |
                           ((src->buffer[currentPosition + 2] >> 6) & 0x03)
                             );
    char c4 = base64char(
                         src->buffer[currentPosition + 2] & 0x3F
                           );
    currentPosition += 3;
    
    if((dest_ptr + 3) >= dest_end)      //safety check (teoricamente sono gi� protetto dal controllo su 'neededSize')
    {
      azure_reg.BIT.ERR_ENCODE_BASE_64 = 1;
      return AZ_ERR_ENCODE_BASE_64;
    }
    
    dest_ptr[0] = c1;
    dest_ptr[1] = c2;
    dest_ptr[2] = c3;
    dest_ptr[3] = c4;
    dest_ptr += 4;
  }
    
  if((dest_ptr + 4) >= dest_end)        //safety check (teoricamente sono gi� protetto dal controllo su 'neededSize')
  {
    azure_reg.BIT.ERR_ENCODE_BASE_64 = 1;
    return AZ_ERR_ENCODE_BASE_64;
  }
  
  //Byte padding (la stringa codificata deve essere multipla di 4B)
  if(src->size == (currentPosition + 2))
  {
    char c1 = base64char(src->buffer[currentPosition] >> 2);
    char c2 = base64char(
                         ((src->buffer[currentPosition] & 0x03) << 4) |
                           (src->buffer[currentPosition + 1] >> 4)
                             );
    char c3 = base64b16(src->buffer[currentPosition + 1] & 0x0F);
    dest_ptr[0] = c1;
    dest_ptr[1] = c2;
    dest_ptr[2] = c3;
    dest_ptr[3] = '=';
    dest_ptr += 4;
  }
  else if(src->size == (currentPosition + 1))
  {
    char c1 = base64char(src->buffer[currentPosition] >> 2);
    char c2 = base64b8(src->buffer[currentPosition] & 0x03);
    dest_ptr[0] = c1;
    dest_ptr[1] = c2;
    dest_ptr[2] = '=';
    dest_ptr[3] = '=';
    dest_ptr += 4;
  }
  
  //Append terminatore di stringa
  if(enb_string_terminator)
  {
    *dest_ptr = '\0';
    dest_ptr++;
  }
  
  dest->offset += ((int32_t)(dest_ptr - dest_begin)); //sposto offset buffer destinazione
  
  return AZ_NO_ERROR;
}


/* Esegue decodifica da base 64 del buffer in ingresso. 
*
*@param[IN] src, buffer sorgente: stringa in base 64 da decodificare
*@param[OUT] dest, buffer destinazione (risultato decoding in append)
*
*return: codice errore: 
*AZ_ERR_DECODE_BASE_64_PAR (dimensione stringa sorgente non valida)
*AZ_ERR_DECODE_BASE_64 (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_decode_from_base64(const Az_Fixed_Buffer_t* src, 
                               Az_Buffer_t* dest)
{  
  uint16_t free_space=0, numberOfEncodedChars;
  uint16_t indexOfFirstEncodedChar;
  
  if((((src->size)%4) != 0) || (src->size < 4))
  {
    azure_reg.BIT.ERR_DECODE_BASE_64_PAR = 1;   //la stringa da decodificare non � multipla di 4
    return AZ_ERR_DECODE_BASE_64_PAR;
  }
    
  free_space = az_get_free_space(dest);         //calcolo spazio libero nel buffer di destinazione
  if(free_space == 0)
  {
    azure_reg.BIT.ERR_DECODE_BASE_64 = 1;
    return AZ_ERR_DECODE_BASE_64;
  }
  
  uint8_t* dest_begin = dest->buffer + dest->offset;
  uint8_t* dest_ptr = dest_begin;
  uint8_t* dest_end = dest_begin + free_space;
  
  numberOfEncodedChars = numberOfBase64Characters(src);
  indexOfFirstEncodedChar = 0;
  while(numberOfEncodedChars >= 4)
  {
    unsigned char c1;
    unsigned char c2;
    unsigned char c3;
    unsigned char c4;
    base64toValue(src->buffer[indexOfFirstEncodedChar], &c1);
    base64toValue(src->buffer[indexOfFirstEncodedChar + 1], &c2);
    base64toValue(src->buffer[indexOfFirstEncodedChar + 2], &c3);
    base64toValue(src->buffer[indexOfFirstEncodedChar + 3], &c4);
    
    if((dest_ptr + 2) >= dest_end)
    {
      azure_reg.BIT.ERR_DECODE_BASE_64 = 1;
      return AZ_ERR_DECODE_BASE_64;
    }
    
    dest_ptr[0] = (c1 << 2) | (c2 >> 4);
    dest_ptr[1] = ((c2 & 0x0f) << 4) | (c3 >> 2);
    dest_ptr[2] = ((c3 & 0x03) << 6) | c4;
    dest_ptr += 3;
    
    numberOfEncodedChars -= 4;
    indexOfFirstEncodedChar += 4;   
  }
  
  if((dest_ptr + 1) >= dest_end)
  {
    azure_reg.BIT.ERR_DECODE_BASE_64 = 1;
    return AZ_ERR_DECODE_BASE_64;
  }
  
  if(numberOfEncodedChars == 2)
  {
    unsigned char c1;
    unsigned char c2;
    base64toValue(src->buffer[indexOfFirstEncodedChar], &c1);
    base64toValue(src->buffer[indexOfFirstEncodedChar + 1], &c2);
    *dest_ptr = (c1 << 2) | (c2 >> 4);
    dest_ptr++;
  }
  else if(numberOfEncodedChars == 3)
  {
    unsigned char c1;
    unsigned char c2;
    unsigned char c3;
    base64toValue(src->buffer[indexOfFirstEncodedChar], &c1);
    base64toValue(src->buffer[indexOfFirstEncodedChar + 1], &c2);
    base64toValue(src->buffer[indexOfFirstEncodedChar + 2], &c3);
    dest_ptr[0] = (c1 << 2) | (c2 >> 4);
    dest_ptr[1] = ((c2 & 0x0f) << 4) | (c3 >> 2);
    dest_ptr += 2;
  }
  
  dest->offset += ((int32_t)(dest_ptr - dest_begin)); //sposto offset buffer destinazione
  
  return AZ_NO_ERROR;
}


/* Analizza la stringa in ingresso e ritorna il numero di caratteri con codifica base 64 valida (esclusi i caratteri di padding '='). */
static uint16_t numberOfBase64Characters(const Az_Fixed_Buffer_t* encodedString)
{
  uint16_t length = 0;
  unsigned char junkChar;
  //Interrompo la decodifica al primo carattere non valido o al termine della stringa
  while((base64toValue(encodedString->buffer[length], &junkChar) != -1) && (length < encodedString->size))
    length++;
  return length;
}


/* Decodifica di un singolo carattere da base 64. */
static int base64toValue(char base64character, unsigned char* value)
{
  int result = 0;
  if (('A' <= base64character) && (base64character <= 'Z'))
  {
    *value = base64character - 'A';
  }
  else if (('a' <= base64character) && (base64character <= 'z'))
  {
    *value = ('Z' - 'A') + 1 + (base64character - 'a');
  }
  else if (('0' <= base64character) && (base64character <= '9'))
  {
    *value = ('Z' - 'A') + 1 + ('z' - 'a') + 1 + (base64character - '0');
  }
  else if ('+' == base64character)
  {
    *value = 62;
  }
  else if ('/' == base64character)
  {
    *value = 63;
  }
  else
  {
    *value = 0;
    result = -1;
  }
  return result;
}


/* Codifica di un singolo carattere in base 64. */
static char base64char(unsigned char val)
{
  char result;
  
  if (val < 26)
  {
    result = 'A' + (char)val;
  }
  else if (val < 52)
  {
    result = 'a' + ((char)val - 26);
  }
  else if (val < 62)
  {
    result = '0' + ((char)val - 52);
  }
  else if (val == 62)
  {
    result = '+';
  }
  else
  {
    result = '/';
  }
  
  return result;
}


static char base64b16(unsigned char val)
{
  const uint32_t base64b16values[4] = {
    joinChars('A', 'E', 'I', 'M'),
    joinChars('Q', 'U', 'Y', 'c'),
    joinChars('g', 'k', 'o', 's'),
    joinChars('w', '0', '4', '8')
  };
  return splitInt(base64b16values[val >> 2], (val & 0x03));
}

static char base64b8(unsigned char val)
{
  const uint32_t base64b8values = joinChars('A', 'Q', 'g', 'w');
  return splitInt(base64b8values, val);
}
