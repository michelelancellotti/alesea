/**
******************************************************************************
* File          	: az_sas_token.c
* Versione libreria	: 0.01
* Descrizione       	: Composizione SAS token.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "az_sas_token.h"
#include "az_base_64.h"
#include "az_hmac_sha256.h"
#include "az_utils.h"


#if (AZURE_ENVIRONMENT == AZ_DEV)
#define SCOPE_ID_STR            "0ne004F5BCB"
#else
#define SCOPE_ID_STR            "0ne008BEBDD"
#endif
const Az_Fixed_Buffer_t SCOPE_ID = {.buffer=(uint8_t*)SCOPE_ID_STR, .size=sizeof(SCOPE_ID_STR)-1};

#define SR_HEADER_STR   "SharedAccessSignature sr="
const Az_Fixed_Buffer_t SR_HEADER = {.buffer=(uint8_t*)SR_HEADER_STR, .size=sizeof(SR_HEADER_STR)-1};
#define REGISTRATIONS_STR        "/registrations/"
const Az_Fixed_Buffer_t REGISTRATIONS = {.buffer=(uint8_t*)REGISTRATIONS_STR, .size=sizeof(REGISTRATIONS_STR)-1};
#define DEVICES_STR     "/devices/"
const Az_Fixed_Buffer_t DEVICES = {.buffer=(uint8_t*)DEVICES_STR, .size=sizeof(DEVICES_STR)-1};
#define LF_STR          "\x0A"
const Az_Fixed_Buffer_t LF_CHAR = {.buffer=(uint8_t*)LF_STR, .size=sizeof(LF_STR)-1};
#define SIG_HEADER_STR  "&sig="
const Az_Fixed_Buffer_t SIG_HEADER = {.buffer=(uint8_t*)SIG_HEADER_STR, .size=sizeof(SIG_HEADER_STR)-1};
#define SE_HEADER_STR   "&se="
const Az_Fixed_Buffer_t SE_HEADER = {.buffer=(uint8_t*)SE_HEADER_STR, .size=sizeof(SE_HEADER_STR)-1};
#define SKN_HEADER_STR  "&skn="
const Az_Fixed_Buffer_t SKN_HEADER = {.buffer=(uint8_t*)SKN_HEADER_STR, .size=sizeof(SKN_HEADER_STR)-1};
#define REG_POLICY_STR  "registration"
const Az_Fixed_Buffer_t REG_POLICY = {.buffer=(uint8_t*)REG_POLICY_STR, .size=sizeof(REG_POLICY_STR)-1};


uint8_t az_sas_token_buff[AZ_MAX_SAS_TOKEN];                    //Buffer SAS token

static uint8_t sas_support_buff[AZ_MAX_SAS_SUPPORT_BUFFER];     //Buffer appoggio per calcolo SAS token
static uint8_t signature_buff[SHA256HashSize];                  //Buffer messaggio firmato --> qualunque messaggio firmato con HMAC SHA-256 � di dimensione fissa, pari a 32B
Az_Fixed_Buffer_t signature = {
  .buffer=signature_buff, .size=sizeof(signature_buff)
};

static uint32_t az_backup_unix_time;


/* ############################################################ 
* Local functions
* ############################################################
*/
static void az_calculate_signature(Az_Client_t* client, Az_Buffer_t* resource_uri, uint32_t token_expiry, 
                                   Az_Buffer_t* dest);
static uint32_t az_calculate_token_expiry(uint32_t unix_time, uint16_t token_duration_hh);





/* Calcola SAS token per la connessione ad Azure.
* N.B. Il calcolo richiede 7ms c.a. @ 16MHz --> 70% del tempo per hashing HMAC SHA-256 
*
*@param[IN] client, handle client 
*@param[IN] unix_time, unix-time corrente (oppure 0 per significare dato N/D)
*#param[IN] token_duration_hh, durata token, espressa in ore
*#param[IN] token_type, tipologia token
*@param[OUT] sas_token, buffer destinazione (risultato calcolo token in append)
*/
void az_calculate_sas_token(Az_Client_t* client, uint32_t unix_time, uint16_t token_duration_hh, Az_SAS_Token_t token_type, 
                            Az_Buffer_t* sas_token)
{
  Az_Buffer_t resource_uri = {
    .buffer=sas_support_buff, .size=sizeof(sas_support_buff), .offset=0
  };
  uint32_t token_expiry;
    
  sas_token->offset = 0;
  az_append_string(&SR_HEADER, sas_token);              //Append "SharedAccessSignature sr="
  //---------- Resource URI -------------------------------------------------------------------------------- 
  //Creo resource URI (su buffer temporaneo)
  if(token_type == DPS_TOKEN)
  {
    az_url_encode(&SCOPE_ID, &resource_uri);            //Append URL-encoding di scope-id
    az_url_encode(&REGISTRATIONS, &resource_uri);       //Append URL-encoding di "/registrations/"
  }
  else
  {
    az_url_encode(client->hostname, &resource_uri);     //Append URL-encoding di iot-hub hostname
    az_url_encode(&DEVICES, &resource_uri);             //Append URL-encoding di "/devices/"
  }
  az_url_encode(client->device_id, &resource_uri);      //Append URL-encoding di device-id
  //---------- end Resource URI ----------------------------------------------------------------------------
  az_append_buffer(&resource_uri, sas_token);           //Append resource URI
  az_append_string(&SIG_HEADER, sas_token);             //Append "&sig="
  
  //##### Calcolo scadenza token #####
  if(unix_time > az_backup_unix_time)
  {
    //Sincronizzazione data/ora valida
    token_expiry = az_calculate_token_expiry(unix_time, token_duration_hh);
    az_backup_unix_time = unix_time;
    azure_reg.BIT.USE_BKP_UNIX_TIME = 0;
    azure_reg.BIT.USE_EMERGENCY_UNIX_TIME = 0;
  }
  else
  {
    //Sincronizzazione data/ora N/D
    if(az_backup_unix_time == 0)
      azure_reg.BIT.USE_EMERGENCY_UNIX_TIME = 1;        //POR uC: forzo uso unix-time di emergenza
    
    if(azure_reg.BIT.USE_EMERGENCY_UNIX_TIME)
    {
      token_expiry = az_calculate_token_expiry(UT_EMERGENCY, token_duration_hh);        //Uso unix-time di emergenza (i.e. nel futuro)
      azure_reg.BIT.USE_BKP_UNIX_TIME = 0;
    }
    else
    {
      token_expiry = az_calculate_token_expiry(az_backup_unix_time, token_duration_hh); //Uso unix-time da ultima sincronizzazione valida (backup)
      azure_reg.BIT.USE_BKP_UNIX_TIME = 1;
    }
  }
  
  az_calculate_signature(client, &resource_uri, token_expiry, sas_token);  //Append signature HMAC SHA-256
  az_append_string(&SE_HEADER, sas_token);              //Append "&se="
  az_append_number(token_expiry, sas_token);            //Append unix-time scadenza token
  if(token_type == DPS_TOKEN)
  {
    az_append_string(&SKN_HEADER, sas_token);           //Append "&skn="
    az_append_string(&REG_POLICY, sas_token);           //Append "registration" policy
  }
  
  az_append_terminator(sas_token);                      //Append terminatore di stringa (SAS token pu� essere di dimensione variabile)
}


/* Calcola signature HMAC SHA-256. 
*
*@param[IN] client, handle client 
*@param[IN] resource_uri, resource URI su cui calcolare la signature
*#param[IN] token_expiry, unix-time scadenza token
*@param[OUT] dest, buffer destinazione (risultato calcolo signature in append)
*/
static void az_calculate_signature(Az_Client_t* client, Az_Buffer_t* resource_uri, uint32_t token_expiry, 
                                   Az_Buffer_t* dest)
{  
  //Decodifico la chiave dispositivo da base 64
  uint8_t dec_dkey_buff[48];
  Az_Buffer_t decoded_device_key = {
    .buffer=dec_dkey_buff, .size=sizeof(dec_dkey_buff), .offset=0
  };  
  az_decode_from_base64(client->device_key, &decoded_device_key);
  //Append scadenza token a resource URI
  az_append_string(&LF_CHAR, resource_uri);             //Append '\n'
  az_append_number(token_expiry, resource_uri);         //Append unix-time scadenza token
  //Calcolo firma HMAC SHA-256 su resource URI
  Az_Fixed_Buffer_t message = az_convert_buffer_2_fixed_buffer(resource_uri);
  Az_Fixed_Buffer_t key = az_convert_buffer_2_fixed_buffer(&decoded_device_key);
  hmac_sha256(&message, &key, signature.buffer);
  //Codifica la firma ottenuta in base 64
  Az_Buffer_t encoded_signature = {
    .buffer=sas_support_buff, .size=sizeof(sas_support_buff), .offset=0
  };   
  az_encode_to_base64(&signature, 0, &encoded_signature);

  //URL-encoding della firma convertita in base 64
  Az_Fixed_Buffer_t enc_sig = az_convert_buffer_2_fixed_buffer(&encoded_signature);
  az_url_encode(&enc_sig, dest);
}


/* Calcola unix-time scadenza token.
*
*@param[IN] token_duration_hh, durata token (espressa in ore)
*@param[IN] unix_time, unix-time corrente
*
*return: unix-time scadenza token 
*/
static uint32_t az_calculate_token_expiry(uint32_t unix_time, uint16_t token_duration_hh)
{
  uint32_t expiry_unix_time = unix_time + (uint32_t)(token_duration_hh*3600);
  return expiry_unix_time;
}
