/**
*****************************************************************************************
* File          	: az_dps.c
* Versione libreria	: 0.01
* Descrizione       	: Gestione connessione a Device Provisioning System (DPS) Azure:
*                         - registrazione automatica dispositivo
*                         - ricezione host-name IoT hub assegnato al dispositivo
*                         TODO[az]: test processo DPS con nome modello sbagliato
*****************************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************************
*/

#include "az_dps.h"
#include "az_utils.h"
#include "az_client.h"
#include "az_calculate_key.h"
#include "az_ext_resources.h"




static Az_DPS_t az_dps_status = AZ_DPS_IDLE;                            //Stato processo DPS
static Az_DPS_t az_prev_dps_status;
static uint16_t az_dps_err_code;                                        //Codice errore risposta operazione
static GPTIMER az_dps_timer;                                            //Timer retry polling stato registrazione
static uint8_t az_dps_num_retry = AZ_DPS_MAX_RETRY;                     //Contatore retry polling stato registrazione
static uint8_t az_dps_timeout_retry = AZ_DPS_RETRY_TIMEOUT_DFLT;        //Tempo attesa tra retry polling stato registrazione
static uint8_t az_device_key_checksum_buff[10];                         //Checksum su chiave individuale dispositivo
static uint8_t az_dps_devkey_requested, az_dps_devkey_invalid;
static uint8_t az_dps_checksum_err_count;

Az_Provisioning_t az_provisioning = AZ_DEV_UNREGISTERED_KEY_MISSING;    //Stato provisioning
uint8_t az_dps_ack_count, az_dps_nack_count;
uint8_t az_dps_operation_id[AZ_DPS_OP_ID_SIZE];                         //Operation-id


/* ############################################################ 
* Local functions
* ############################################################
*/
static Az_Err_t az_parse_registration_message(Az_DPS_t* next_dps_status);
static Az_Err_t az_parse_registration_status_message(Az_DPS_t* next_dps_status);
static Az_Err_t az_parse_devkey_message(Az_DPS_t* next_dps_status);
static Az_Err_t az_build_registration_message(void);
static void az_dps_fail(Az_DPS_t* next_dps_status);
static uint8_t az_get_dps_timeout(Az_Buffer_t* src);




/* Init processo DPS. */
void az_init_dps(void)
{
  az_dps_timeout_retry = AZ_DPS_RETRY_TIMEOUT_DFLT;
  GPTIMER_init(&az_dps_timer, 1000*az_dps_timeout_retry);  
  az_dps_num_retry = AZ_DPS_MAX_RETRY;
  az_dps_ack_count = 0;
  az_dps_nack_count = 0;
  az_dps_devkey_requested = 0;
}


/* Gestione processo DPS. */
void az_process_dps(void)
{
  Az_DPS_t next_dps_status = AZ_DPS_IDLE;
  
  if(!azure_reg.BIT.LIB_INIT)                   //safety check: libreria inizializzata
    az_set_dps_status(AZ_DPS_IDLE);
  
  switch(az_dps_status)
  {
    case AZ_DPS_READ_DEVKEY:
      //#### Lettura chiave individuale dispositivo da memoria modem ####
      az_op_callback.exec_operation(AZ_OP_READ_DEVKEY);
      az_set_dps_status(AZ_DPS_OPERATION_STATUS);
      break;
      
      
    case AZ_DPS_WRITE_DEVKEY:
      //#### Scrittura chiave individuale dispositivo (calcolata) su memoria modem ####
      //-- Compone messaggio --
      az_create_devkey_message();
      //-- Esegue scrittura su file (memoria modem) --
      az_op_callback.exec_operation(AZ_OP_WRITE_DEVKEY);
      az_set_dps_status(AZ_DPS_OPERATION_STATUS);
      break;
      
      
    case AZ_DPS_HTTP_DEVKEY:
      //#### Richiesta chiave individuale dispositivo tramite API HTTP ####
      az_op_callback.exec_operation(AZ_OP_HTTP_DEVKEY);
      az_set_dps_status(AZ_DPS_OPERATION_STATUS);
      break;
      
      
    case AZ_DPS_CONNECT:
      //#### Connessione al servizio DPS ####
      az_op_callback.exec_operation(AZ_OP_MQTT_CONNECT);
      az_set_dps_status(AZ_DPS_OPERATION_STATUS);
      break;
    
      
    case AZ_DPS_SUBSCRIBE:
      //#### Subscribe per ricezione stato operazioni ####
      //-- Imposta topic --
      if(az_set_topic(AZ_EV_SUB_DPS_RESPONSE, &az_subscribe_topic) == AZ_NO_ERROR)
      {
        az_op_callback.exec_operation(AZ_OP_SUBSCRIBE);
        next_dps_status = AZ_DPS_OPERATION_STATUS;
      }
      else
        next_dps_status = AZ_DPS_DISCONNECT;
      
      az_set_dps_status(next_dps_status);
      break;
      
      
    case AZ_DPS_REGISTER:
      //#### Invia messaggio registrazione device ####
      //-- Imposta topic --
      if(az_set_topic(AZ_EV_PUB_DPS_REGISTRATION, &az_publish_topic) == AZ_NO_ERROR)
      {
        //-- Imposta dati publish --
        az_build_registration_message();
        //-- Esegue publish --
        az_op_callback.exec_operation(AZ_OP_PUBLISH);
        next_dps_status = AZ_DPS_OPERATION_STATUS;
      }
      else
        next_dps_status = AZ_DPS_DISCONNECT;
      
      az_set_dps_status(next_dps_status);
      break;
      
      
    case AZ_DPS_CHECK_REGISTRATION:
      //#### Controlla stato registrazione device ####
      //-- Imposta topic --
      if(az_set_topic(AZ_EV_PUB_DPS_GET_STATUS, &az_publish_topic) == AZ_NO_ERROR)
      {
        //-- Imposta dati publish --
        az_clear_buffer_fast(&az_publish_data); //payload vuoto
        //-- Esegue publish --
        az_op_callback.exec_operation(AZ_OP_PUBLISH);
        next_dps_status = AZ_DPS_OPERATION_STATUS;
      }
      else
        next_dps_status = AZ_DPS_DISCONNECT;
      
      az_set_dps_status(next_dps_status);
      break;
      
      
    case AZ_DPS_WAIT_RETRY_TIMEOUT:
      //#### Attesa timeout retry polling stato registrazione device ####
      if(!az_dps_timer.enable)
      {
        GPTIMER_stop(&az_dps_timer);
        GPTIMER_setTics(&az_dps_timer, 1000*az_dps_timeout_retry);   
        GPTIMER_reTrigger(&az_dps_timer);                       //start tempo attesa
      }
      else if(GPTIMER_isTimedOut(&az_dps_timer))
      {
        GPTIMER_stop(&az_dps_timer);
        az_set_dps_status(AZ_DPS_CHECK_REGISTRATION);
      }
      break;
      
      
    case AZ_DPS_DISCONNECT:
      //#### Disconnessione dal servizio DPS ####
      az_op_callback.exec_operation(AZ_OP_MQTT_DISCONNECT);
      az_set_dps_status(AZ_DPS_OPERATION_STATUS);
      break;
      
      
    case AZ_DPS_OPERATION_STATUS:
      //#### Verifica stato operazione ####
      switch(az_prev_dps_status)
      {
        case AZ_DPS_READ_DEVKEY:
          if(az_op_callback.operation_status(AZ_OP_READ_DEVKEY)==AZ_NO_ERROR && 
             az_dps_devkey_invalid==0)
          {
            //Chiave presente e non invalidata
            //-- Parsing payload API --
            az_parse_devkey_message(&next_dps_status);
          }
          else
          {
            az_dps_devkey_invalid = 0;                  //reset flag
            if(!az_dps_devkey_requested)
              next_dps_status = AZ_DPS_HTTP_DEVKEY;     //chiave assente ma non ancora richiesta --> eseguo call API HTTP
            else
              next_dps_status = AZ_DPS_IDLE;            //chiave assente dopo averla richiesta --> errore
          }
            
          az_set_dps_status(next_dps_status);
          break;
          
        case AZ_DPS_WRITE_DEVKEY:
          //Ignoro eventuali errori, gestiti negli stati successivi
          az_set_dps_status(AZ_DPS_READ_DEVKEY);    
          break;
      
        case AZ_DPS_HTTP_DEVKEY:
          if(az_op_callback.operation_status(AZ_OP_HTTP_DEVKEY) == AZ_NO_ERROR)
            next_dps_status = AZ_DPS_READ_DEVKEY;       //HTTP completato --> eseguo lettura chiave
          else
          {
            az_calculate_device_key(&az_device_id);     //errore HTTP --> calcolo chiave localmente            
            next_dps_status = AZ_DPS_WRITE_DEVKEY;
          }
        
          az_set_dps_status(next_dps_status);
          break;
        
        case AZ_DPS_CONNECT:
          if(az_op_callback.operation_status(AZ_OP_MQTT_CONNECT) == AZ_NO_ERROR)
            next_dps_status = AZ_DPS_SUBSCRIBE;
          else
            az_dps_fail(&next_dps_status);
          
          az_set_dps_status(next_dps_status);
          break;
          
        case AZ_DPS_SUBSCRIBE:
          //Ignoro eventuali errori, gestiti negli stati successivi
          az_set_dps_status(AZ_DPS_REGISTER);        
          break;
          
        case AZ_DPS_REGISTER:
        case AZ_DPS_CHECK_REGISTRATION:
          if(az_op_callback.operation_status(AZ_OP_DATA_RECEIVED) == AZ_NO_ERROR)
          {
            //Messaggio ricevuto (publish + parsing OK)
            //-- Parsing payload messaggio --
            if(az_prev_dps_status == AZ_DPS_REGISTER)
              az_parse_registration_message(&next_dps_status);
            else
              az_parse_registration_status_message(&next_dps_status);
          }
          else
            az_dps_fail(&next_dps_status);
          
          az_set_dps_status(next_dps_status);
          break;
          
        default: az_set_dps_status(AZ_DPS_IDLE); break;
      }
      break;
      
      
    default: az_set_dps_status(AZ_DPS_IDLE); break;
  }
}


/* Imposta stato processo DPS. */
void az_set_dps_status(Az_DPS_t new_status)
{
  switch(new_status)
  {
    case AZ_DPS_READ_DEVKEY: azure_reg_ext.BIT.DPS_READ_DEVKEY=1; break;
    case AZ_DPS_CONNECT: azure_reg_ext.BIT.DPS_CONNECT=1; break;
    case AZ_DPS_HTTP_DEVKEY: { azure_reg_ext.BIT.DPS_HTTP_DEVKEY=1; az_dps_devkey_requested=1; } break;
    case AZ_DPS_REGISTER: azure_reg_ext.BIT.DPS_REGISTER=1; break;
    case AZ_DPS_CHECK_REGISTRATION: azure_reg_ext.BIT.DPS_CHECK_REGISTRATION=1; break;
    case AZ_DPS_IDLE: az_dps_devkey_requested=0; break;
    default: break;
  }
  az_prev_dps_status = az_dps_status;       //snapshot stato corrente
  az_dps_status = new_status;
}

/* Get stato processo DPS. */
Az_DPS_t az_get_dps_status(void)
{
  return az_dps_status;
}


/* Parsing messaggio registrazione device (DPS): 
*  - get codice operazione
*  - get timeout retry polling stato registrazione
*/
static Az_Err_t az_parse_registration_message(Az_DPS_t* next_dps_status)
{
  Az_Err_t err = AZ_NO_ERROR;
  Az_Buffer_t operation_id_buff = {
    .buffer = az_dps_operation_id,
    .offset = 0,
    .size = sizeof(az_dps_operation_id)
  };

  az_received_topic.offset = 0;         //accesso in lettura sempre dall'inizio del buffer
  az_subscribe_data.offset = 0;         //accesso in lettura sempre dall'inizio del buffer
  
  //Parse codice errore risposta
  searchField(az_received_topic.buffer, '/', az_received_topic.offset, az_received_topic.size);
  az_dps_err_code = convertStrToLong(searchField_res[3].field, 0, MAX_FIELD_SIZE);
  //Parse timeout retry da utilizzare durante il polling
  az_dps_timeout_retry = az_get_dps_timeout(&az_received_topic);
  
  if(az_dps_err_code == AZ_DPS_OP_RESP_ACCEPTED)
  {
    az_clear_buffer_fast(&operation_id_buff);
    //Get codice operazione da utilizzare durante il polling
    err = az_find_value_string("operationId", &az_subscribe_data, &operation_id_buff);  //parsing valore campo "operationId"
    az_append_terminator(&operation_id_buff);    
    if(err == AZ_NO_ERROR)
    {
      az_dps_ack_count++;
      *next_dps_status = AZ_DPS_CHECK_REGISTRATION;
    }
    else
    {
      azure_reg_ext.BIT.DPS_ERR_PARSING = 1;
      az_dps_fail(next_dps_status);
    }
  }
  else
  {
    azure_reg_ext.BIT.ERR_OPCODE = 1;
    az_dps_fail(next_dps_status);
    err = AZ_ERR_DPS_PARSING;
  }
  
  return err;
}

/* Parsing messaggio stato registrazione device (DPS):
*  - get timeout retry polling stato registrazione
*  - get hostname IoT Hub
*/
static Az_Err_t az_parse_registration_status_message(Az_DPS_t* next_dps_status)
{
  Az_Err_t err = AZ_NO_ERROR;
  Az_Buffer_t assigned_hub_buff = {
    .buffer = az_iot_hub_hostname_buff,
    .offset = 0,
    .size = sizeof(az_iot_hub_hostname_buff)
  };
  
  az_received_topic.offset = 0;         //accesso in lettura sempre dall'inizio del buffer
  az_subscribe_data.offset = 0;         //accesso in lettura sempre dall'inizio del buffer
  
  searchField(az_received_topic.buffer, '/', az_received_topic.offset, az_received_topic.size);
  az_dps_err_code = convertStrToLong(searchField_res[3].field, 0, MAX_FIELD_SIZE);
  switch(az_dps_err_code)
  {
    case AZ_DPS_OP_RESP_OK:             //registrazione DPS conclusa
      az_clear_buffer_fast(&assigned_hub_buff);
      //Get hostname IoT Hub da utilizzare nella fase di connessione ai servizi IoT Central
      err = az_find_value_string("assignedHub", &az_subscribe_data, &assigned_hub_buff);        //parsing valore campo "assignedHub"
      az_append_terminator(&assigned_hub_buff);      
      if(err == AZ_NO_ERROR)
      {
        az_dps_ack_count++;
        az_set_provisioning(AZ_DEV_REGISTERED);
        *next_dps_status = AZ_DPS_DISCONNECT;
      }
      else
      {
        azure_reg_ext.BIT.DPS_ERR_PARSING = 1;
        az_dps_fail(next_dps_status);
      }
      break;
      
    case AZ_DPS_OP_RESP_ACCEPTED:       //registrazione DPS in corso: continuare il polling      
      az_dps_timeout_retry = az_get_dps_timeout(&az_received_topic);            //parse timeout retry da utilizzare durante il polling
      if(az_dps_num_retry > 0)
      {
        az_dps_num_retry--;
        *next_dps_status = AZ_DPS_WAIT_RETRY_TIMEOUT;
        err = AZ_CONTINUE_DPS_POLLING;
      }
      else
      {
        azure_reg_ext.BIT.DPS_ERR_TIMEOUT = 1;
        az_dps_fail(next_dps_status);   //numero max retry polling raggiunto        
        err = AZ_ERR_DPS_POLLING;
      }
      break;
      
    default:                            //errore registrazione DPS
      azure_reg_ext.BIT.ERR_OPCODE = 1;
      az_dps_fail(next_dps_status);
      err = AZ_ERR_DPS_PARSING;
      break;
  }
  
  return err;
}

/* Parsing payload API richiesta chiave individuale dispositivo:
*  - chiave individuale dispositivo
*  - verifica checksum su chiave individuale dispositivo
*/
static Az_Err_t az_parse_devkey_message(Az_DPS_t* next_dps_status)
{
  Az_Err_t err = AZ_NO_ERROR;
  uint8_t i, checksum=0;
  uint8_t checksum_hex_string[4];
  int res;
  Az_Buffer_t devkey_buff = {
    .buffer = az_device_key_buff,
    .offset = 0,
    .size = sizeof(az_device_key_buff)
  };
  Az_Buffer_t checksum_buff = {
    .buffer = az_device_key_checksum_buff,
    .offset = 0,
    .size = sizeof(az_device_key_checksum_buff)
  };
  
  az_devkey_data.offset = 0;            //accesso in lettura sempre dall'inizio del buffer  
  az_clear_buffer_fast(&devkey_buff);
  az_clear_buffer_fast(&checksum_buff);
  
  //Get chiave individuale dispositivo necessario per generare SAS token  
  err = az_find_value_string("device_key", &az_devkey_data, &devkey_buff);              //parsing valore campo "device_key"
  az_append_terminator(&devkey_buff);
  if(err == AZ_NO_ERROR)
  {
    //Get checksum su chiave individuale dispositivo    
    err = az_find_value_string("device_key_checksum", &az_devkey_data, &checksum_buff); //parsing valore campo "device_key_checksum"
    az_append_terminator(&checksum_buff);
    //Calcolo checksum
    checksum_hex_string[0] = 0x00;
    for(i=0; devkey_buff.buffer[i]!=0x00 && i<devkey_buff.size; i++)
      checksum ^= devkey_buff.buffer[i];
    res = snprintf((char*)checksum_hex_string, sizeof(checksum_hex_string), "%02X", checksum);  //converto checksum in stringa HEX (terminatore di stringa aggiunto da 'snprintf' )
    //Verifica checksum
    if(err==AZ_NO_ERROR && res>0 && 
       (memcmp(checksum_hex_string, checksum_buff.buffer, 2)==0))                       //la checksum � rappresentata su 1B --> 2 cifre HEX
    {
      //Chiave valida
      az_set_provisioning(AZ_DEV_UNREGISTERED);
      az_dps_checksum_err_count = 0;
      az_dps_devkey_invalid = 0;
      *next_dps_status = AZ_DPS_CONNECT;
    }
    else
    {
      //Chiave NON valida
      azure_reg_ext.BIT.DPS_ERR_DEVKEY_CHECKSUM = 1;
      az_dps_checksum_err_count++;
      if(az_dps_checksum_err_count >= AZ_DPS_MAX_CHECKSUM_ERROR)
      {
        az_dps_checksum_err_count = 0;
        az_dps_devkey_invalid = 1;      //invalido definitivamente la chiave e triggero una nuova richiesta tramite API HTTP
      }
      *next_dps_status = AZ_DPS_IDLE;   //errore checksum
    }
  }
  else
  {
    azure_reg_ext.BIT.DPS_ERR_PARSING = 1;
    *next_dps_status = AZ_DPS_IDLE;     //errore parsing
  }
  
  return err;
}


/* Composizione messaggio registrazione DPS. 
*
*return: codice errore:
*AZ_ERR_DPS_BUILD_MESSAGE (dimensione buffer insufficiente)
*AZ_NO_ERROR
*/
static Az_Err_t az_build_registration_message(void)
{
  Az_Err_t err;
  char* dest_ptr;
  int res;
  
  az_clear_buffer_fast(&az_publish_data);
  dest_ptr = (char*)&az_publish_data.buffer[az_publish_data.offset];
  res = snprintf(dest_ptr, az_publish_data.size, AZ_PAYLOAD_DPS_REGISTRATION, 
                 az_device_id.buffer,
                 AZ_MODEL_ID);
  //terminatore di stringa aggiunto da 'snprintf' ...
  if(res<0 || res>=az_publish_data.size)
    err = AZ_ERR_DPS_BUILD_MESSAGE;
  else
  {
    az_publish_data.offset += res;
    err = AZ_NO_ERROR;
  }
  
  return err;
}


/* Parse timeout retry da utilizzare durante il polling. */
static uint8_t az_get_dps_timeout(Az_Buffer_t* src)
{
  uint8_t timeout;
  uint16_t retry_timeout_start = 0;
  
  if(searchString(src->buffer, "retry-after=", src->offset, src->size, &retry_timeout_start, 1, 1))
  {
    timeout = convertStrToByte(&src->buffer[retry_timeout_start], retry_timeout_start, src->size);
    if(timeout>AZ_DPS_MAX_RETRY_TIMEOUT) timeout=AZ_DPS_MAX_RETRY_TIMEOUT;    //controllo valore massimo --> saturazione
    if(timeout==0) timeout=AZ_DPS_RETRY_TIMEOUT_DFLT;                         //controllo valore minimo --> porto a default
  }
  else
    timeout = AZ_DPS_RETRY_TIMEOUT_DFLT;   //errore parsing retry timeout: porto a default
  
  return timeout;
}


/* Segnala errore processo DPS. */
static void az_dps_fail(Az_DPS_t* next_dps_status)
{
  az_dps_nack_count++;
  *next_dps_status = AZ_DPS_DISCONNECT;
}


/* Imposta stato provisionig DPS. */
void az_set_provisioning(Az_Provisioning_t new_status)
{
  if(new_status==AZ_DEV_REGISTERED) azure_reg_ext.BIT.DPS_DEV_REGISTERED=1;
  else azure_reg_ext.BIT.DPS_DEV_REGISTERED=0;
  az_provisioning = new_status;
}
