/**
******************************************************************************
* File          	: az_dev_twins.c
* Versione libreria	: 0.01
* Descrizione       	: Gestione aggiornamento device twins Azure:
*                         - lettura lista 'desired' properties 
*                           (i.e. set-points impostati da remoto)
*                         - aggiornamento lista 'reported' properties 
*                           (i.e. parametri dipendenti dai set-points)
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "az_dev_twins.h"
#include "az_utils.h"
#include "az_ext_resources.h"




static Az_DevTwins_t az_dev_twins_status = AZ_DEVTWINS_IDLE;            //Stato processo aggiornamento device-twins
static Az_DevTwins_t az_prev_dev_twins_status;
static uint16_t az_dev_twins_err_code;                                  //Codice errore risposta operazione

uint16_t az_dev_twins_ack_count, az_dev_twins_nack_count;               //Contatore errori


/* ############################################################ 
* Local functions
* ############################################################
*/
static Az_Err_t az_reset_dev_twins_ctx(void);
static Az_Err_t az_parse_dev_twins_message(void);
static Az_Err_t az_parse_property_value(Az_Buffer_t* src, uint8_t select, Az_Property_t* dest);
static Az_Err_t az_build_dev_twins_message(void);
static Az_Err_t az_build_property_value(Az_Property_t* src, uint8_t is_first_prop, uint8_t is_last_prop, 
                                        Az_Buffer_t* dest);
static void az_dev_twins_fail(Az_DevTwins_t* next_dev_twins_status);




/* Init processo aggiornamento device-twins. */
void az_init_dev_twins(void)
{
  az_dev_twins_ack_count = 0;
  az_dev_twins_nack_count = 0;
}


/* Gestione processo aggiornamento device-twins. */
void az_process_dev_twins(void)
{
  Az_DevTwins_t next_dev_twins_status = AZ_DEVTWINS_IDLE;
  
  if(!azure_reg.BIT.LIB_INIT)                   //safety check: libreria inizializzata
    az_set_dev_twins_status(AZ_DEVTWINS_IDLE);
  
  switch(az_dev_twins_status)
  {
    case AZ_DEVTWINS_SUBSCRIBE:
      //#### Subscribe per ricezione stato operazioni ####
      //-- Imposta topic --
      if(az_set_topic(AZ_EV_SUB_DEVTWINS_RESPONSE, &az_subscribe_topic) == AZ_NO_ERROR)
      {
        az_op_callback.exec_operation(AZ_OP_SUBSCRIBE);
        next_dev_twins_status = AZ_DEVTWINS_OPERATION_STATUS;
      }
      else
        next_dev_twins_status = AZ_DEVTWINS_IDLE;
      
      az_set_dev_twins_status(next_dev_twins_status);
      break;
    
      
    case AZ_DEVTWINS_GET_DESIRED_PROPS:
      //#### Get lista 'desired' properties ####
      //-- Imposta topic --
      if(az_set_topic(AZ_EV_PUB_GET_DESIRED_PROPS, &az_publish_topic) == AZ_NO_ERROR)
      {
        //-- Imposta dati publish --
        az_clear_buffer_fast(&az_publish_data);         //payload vuoto
        //-- Esegue publish --
        az_op_callback.exec_operation(AZ_OP_PUBLISH);
        next_dev_twins_status = AZ_DEVTWINS_OPERATION_STATUS;
      }
      else
        next_dev_twins_status = AZ_DEVTWINS_IDLE;
      
      az_set_dev_twins_status(next_dev_twins_status);
      break;
      
      
    case AZ_DEVTWINS_UPDATE_REPORTED_PROPS:
      //#### Aggiorna lista 'reported' properties ####
      //-- Imposta topic --
      if(az_set_topic(AZ_EV_PUB_UPDATE_REPORTED_PROPS, &az_publish_topic) == AZ_NO_ERROR)
      {
        //-- Sincronizzazione tra propriet� 'reported' e dati applicazione --
        if(az_dev_twins_callback.sync_reported_props)
          az_dev_twins_callback.sync_reported_props();
        //-- Imposta dati publish --
        az_build_dev_twins_message();
        //-- Esegue publish --
        az_op_callback.exec_operation(AZ_OP_PUBLISH);
        next_dev_twins_status = AZ_DEVTWINS_OPERATION_STATUS;
      }
      else
        next_dev_twins_status = AZ_DEVTWINS_IDLE;
      
      az_set_dev_twins_status(next_dev_twins_status);
      break;
      
      
    case AZ_DEVTWINS_OPERATION_STATUS:
      //#### Verifica stato operazione ####
      switch(az_prev_dev_twins_status)
      {
        case AZ_DEVTWINS_SUBSCRIBE:
          //Ignoro eventuali errori, gestiti negli stati successivi
          az_set_dev_twins_status(AZ_DEVTWINS_GET_DESIRED_PROPS);        
          break;
          
        case AZ_DEVTWINS_GET_DESIRED_PROPS:
          if(az_op_callback.operation_status(AZ_OP_DATA_RECEIVED) == AZ_NO_ERROR)
          {
            //Messaggio ricevuto (publish + parsing OK)
            //-- Parsing payload messaggio --
            if(az_parse_dev_twins_message() == AZ_NO_ERROR)
            {              
              //-- Sincronizzazione tra dati applicazione e propriet� 'desired' --
              if(az_dev_twins_callback.sync_app_data)
                az_dev_twins_callback.sync_app_data();
              
              az_dev_twins_ack_count++;
              next_dev_twins_status = AZ_DEVTWINS_UPDATE_REPORTED_PROPS;
            }
            else
              az_dev_twins_fail(&next_dev_twins_status);
          }
          else
            az_dev_twins_fail(&next_dev_twins_status);
            
          az_set_dev_twins_status(next_dev_twins_status);
          break;
          
        default: az_set_dev_twins_status(AZ_DEVTWINS_IDLE); break;
      }
      break;
      
      
    default: az_set_dev_twins_status(AZ_DEVTWINS_IDLE); break;
  }
}


/* Imposta stato processo aggiornamento device-twins. */
void az_set_dev_twins_status(Az_DevTwins_t new_status)
{
  switch(new_status)
  {
    case AZ_DEVTWINS_GET_DESIRED_PROPS: azure_reg_ext.BIT.DEVTWINS_GET_DESIRED_PROPS=1; break;
    case AZ_DEVTWINS_UPDATE_REPORTED_PROPS: azure_reg_ext.BIT.DEVTWINS_UPDATE_REPORTED_PROPS=1; break;
    default: break;
  }
  az_prev_dev_twins_status = az_dev_twins_status;       //snapshot stato corrente
  az_dev_twins_status = new_status;
}

/* Get stato processo aggiornamento device-twins. */
Az_DevTwins_t az_get_dev_twins_status(void)
{
  return az_dev_twins_status;
}


/* Reset flags gestione device-twins. */
Az_Err_t az_reset_dev_twins_ctx(void)
{
  Az_Err_t err = AZ_NO_ERROR;
  uint8_t p;
  
  if(az_properties.list && az_properties.num>0)
  {
    for(p=0; p<az_properties.num; p++)
      az_properties.list[p].reg.BYTE = 0x00;
  }
  else
    err = AZ_ERR_PROPERTIES_INIT;
  
  return err;
}


/* Parsing messaggio stato operazione (device-twins). */
static Az_Err_t az_parse_dev_twins_message(void)
{
  Az_Err_t err = AZ_NO_ERROR;
  uint16_t desired_start=0, reported_start=0, offset=0;
  uint8_t p, desired_found, reported_found;
  
  az_reset_dev_twins_ctx();     //Reset flags gestione device-twins
  
  if(az_properties.list && az_properties.num>0)
  {
    az_received_topic.offset = 0;         //accesso in lettura sempre dall'inizio del buffer
    az_subscribe_data.offset = 0;         //accesso in lettura sempre dall'inizio del buffer
    
    //Parse codice errore risposta
    searchField(az_received_topic.buffer, '/', az_received_topic.offset, az_received_topic.size);
    az_dev_twins_err_code = convertStrToLong(searchField_res[3].field, 0, MAX_FIELD_SIZE);
    
    if(az_dev_twins_err_code == AZ_DEVTWINS_OP_RESP_OK)
    {
      desired_found = searchString(az_subscribe_data.buffer, "\"desired\"", az_subscribe_data.offset, az_subscribe_data.size, &desired_start, 1, 1);
      reported_found = searchString(az_subscribe_data.buffer, "\"reported\"", az_subscribe_data.offset, az_subscribe_data.size, &reported_start, 1, 1);
      if(desired_found && reported_found && desired_start!=reported_start)
      {
        //--- Sincronizzazione propriet� 'desired' ---
        for(p=0; p<az_properties.num; p++)
        {
          az_subscribe_data.offset = desired_start;         //comincio la ricerca dall'inizio dell'oggetto JSON 'desired' (ricerca campo NON posizionale)
          az_properties.list[p].reg.BIT.DESIRED_RX = az_find_key(az_properties.list[p].keys[AZ_JS_MAIN_KEY], &az_subscribe_data, &offset);
          if((az_properties.list[p].reg.BIT.DESIRED_RX && desired_start<reported_start) && offset>=reported_start)
          {
            //1 - Oggetto 'desired' posizionato PRIMA di oggetto 'reported' (normalmente si verifica)
            //2 - Chiave JSON trovata in posizione non valida (e.g. DOPO l'oggetto 'reported') 
            az_properties.list[p].reg.BIT.DESIRED_RX = 0;
          }
          if(az_properties.list[p].reg.BIT.DESIRED_RX)
          {
            az_subscribe_data.offset = offset;      //sposto puntatore su valore campo JSON (i.e. dopo chiave campo JSON)
            az_parse_property_value(&az_subscribe_data, AZ_DESIRED, &az_properties.list[p]);        
          }
        }
        //--- Sincronizzazione propriet� 'reported' ---
        for(p=0; p<az_properties.num; p++)
        {
          az_subscribe_data.offset = reported_start;        //comincio la ricerca sempre dall'inizio dell'oggetto JSON 'reported' (ricerca campo NON posizionale)            
          az_properties.list[p].reg.BIT.REPORTED_RX = az_find_key(az_properties.list[p].keys[AZ_JS_MAIN_KEY], &az_subscribe_data, &offset);
          if((az_properties.list[p].reg.BIT.REPORTED_RX && reported_start<desired_start) && offset>=desired_start)
          {
            //1 - Oggetto 'reported' posizionato PRIMA di oggetto 'desired' (normalmente NON si verifica)
            //2 - Chiave JSON trovata in posizione non valida (e.g. DOPO l'oggetto 'desired') 
            az_properties.list[p].reg.BIT.REPORTED_RX = 0;
          }
          if(az_properties.list[p].reg.BIT.REPORTED_RX)
          {
            az_subscribe_data.offset = offset;    //sposto puntatore su valore campo JSON (i.e. dopo chiave campo JSON)
            az_parse_property_value(&az_subscribe_data, AZ_REPORTED, &az_properties.list[p]);
          }
        }
        
        //Diagnostica casi dove viene rilevata la presenza della sola propriet� 'desired' o della sola propriet� 'reported'
        for(p=0; p<az_properties.num; p++)
        {
          if(az_properties.list[p].reg.BYTE==1 || az_properties.list[p].reg.BYTE==2)
            azure_reg_ext.BIT.DEVTWINS_ONLY_ONE = 1;
        }
      }
      else
      {
        azure_reg_ext.BIT.DEVTWINS_ERR_PARSING = 1;
        err = AZ_ERR_DEVTWINS_PARSING;
      }
    }
    else
    {
      azure_reg_ext.BIT.ERR_OPCODE = 1;
      err = AZ_ERR_DEVTWINS_PARSING;
    }
  }
  else
    err = AZ_ERR_PROPERTIES_INIT;
  
  return err;
}

/* Decodifica il valore dell'oggetto JSON in base alla tipologia di propriet�.
*
*@param[IN] src, buffer sorgente
*@param[IN] select, seleziona se assegnare il risultato del parsing al valore 'desired' o al valore 'reported' 
*@param[IN/OUT] dest, propriet� destinazione
*
*
*return: codice errore:
*AZ_ERR_UNXPECTED (schema non gestito)
*AZ_NO_ERROR
*/
static Az_Err_t az_parse_property_value(Az_Buffer_t* src, uint8_t select, Az_Property_t* dest)
{
  Az_Err_t err = AZ_NO_ERROR;
  Az_Property_Value_t* prop_value;
  uint8_t res_f1, res_f2, app_bool;
  uint16_t offset_f1, offset_f2;
  uint16_t search_limit_1, search_limit_2;  
  
  if(select == AZ_DESIRED)
    prop_value = &(dest->desired);
  else
    prop_value = &(dest->reported);
  
  //Definisco la profondit� di ricerca del valore 
  search_limit_1 = sommaSaturata(src->offset, AZ_MAX_PROP_LENGTH, src->size);
      
  switch(dest->schema)
  {
    case AZ_U32:
      prop_value->U32 = convertStrToLong(&src->buffer[src->offset], src->offset, search_limit_1);
      break;
      
    case AZ_OBJ_BOOL_U32:
    case AZ_OBJ_U32_U32:
      //Ricerca campi all'interno dell'oggetto
      res_f1 = az_find_key(dest->keys[AZ_JS_F1_KEY], src, &offset_f1);
      res_f2 = az_find_key(dest->keys[AZ_JS_F2_KEY], src, &offset_f2);
      if(res_f1 && res_f2)
      {
        //Campi presenti
        search_limit_1 = sommaSaturata(offset_f1, AZ_MAX_PROP_LENGTH, src->size);
        search_limit_2 = sommaSaturata(offset_f2, AZ_MAX_PROP_LENGTH, src->size);
        if(dest->schema == AZ_OBJ_BOOL_U32)
        {
          app_bool = convertStrToBoolean(src->buffer, offset_f1, search_limit_1);
          if(app_bool != BOOL_UNDEF_VALUE)
            prop_value->OBJ_BOOL_U32.BOOL = app_bool;    //conversione valida
          prop_value->OBJ_BOOL_U32.U32 = convertStrToLong(&src->buffer[offset_f2], offset_f2, search_limit_2);
        }
        else
        {
          prop_value->OBJ_U32_U32.U32_0 = convertStrToLong(&src->buffer[offset_f1], offset_f1, search_limit_1);
          prop_value->OBJ_U32_U32.U32_1 = convertStrToLong(&src->buffer[offset_f2], offset_f2, search_limit_2);
        }
      }
      break;
      
    case AZ_BOOL:
      app_bool = convertStrToBoolean(src->buffer, src->offset, search_limit_1);
      if(app_bool != BOOL_UNDEF_VALUE)
        prop_value->BOOL = app_bool;    //conversione valida
      break;
      
    default:
      azure_reg.BIT.ERR_UNEXPECTED = 1;
      err = AZ_ERR_UNXPECTED;
      break;
  }
  
  return err;
}


/* Composizione messaggio aggiornamento propriet�. */
static Az_Err_t az_build_dev_twins_message(void)
{
  Az_Err_t err = AZ_NO_ERROR;
  uint8_t p, is_first_prop, is_last_prop; 
  
  az_clear_buffer_fast(&az_publish_data);       //accesso in scrittura sempre dall'inizio del buffer  
  
  if(az_properties.list && az_properties.num>0)
  {  
    for(p=0; p<az_properties.num; p++)
    {
      //Rileva inizio JSON
      if(p == 0)
        is_first_prop = 1;
      else
        is_first_prop = 0;
      //Rileva fine JSON
      if(p == (az_properties.num-1))
        is_last_prop = 1;
      else
        is_last_prop = 0;
      //Compone JSON
      az_build_property_value(&az_properties.list[p], is_first_prop, is_last_prop, &az_publish_data);
    }
    az_append_terminator(&az_publish_data);
  }
  else
    err = AZ_ERR_PROPERTIES_INIT;
  
  return err;
}

/* Codifica in formato JSON il valore 'reported' della propriet�.
*
*@param[IN] src, propriet� sorgente
*@param[IN] is_first_prop, indica se la propriet� � la prima della lista
*@param[IN] is_last_prop, indica se la propriet� � l'ultima della lista
*@param[OUT] dest, buffer destinazione
*
*
*return: codice errore:
*AZ_ERR_UNXPECTED (schema non gestito)
*AZ_NO_ERROR
*/
static Az_Err_t az_build_property_value(Az_Property_t* src, uint8_t is_first_prop, uint8_t is_last_prop, 
                                        Az_Buffer_t* dest)
{
  Az_Err_t err = AZ_NO_ERROR;
  uint8_t formatted_data_str[36];
  uint8_t formatted_data_offset, formatted_data_size;
  char json_end;
  Az_Fixed_Buffer_t formatted_data = {
    .buffer=formatted_data_str,
    .size=0  
  };
  
  //Pulizia buffer appoggio
  memset(formatted_data_str, 0x00, sizeof(formatted_data_str));
  //Selezione header JSON
  if(is_first_prop)
  {
    formatted_data_str[0] = '{';
    formatted_data_offset = 1;
  }
  else 
    formatted_data_offset = 0;
  //Selezione tailer JSON
  if(is_last_prop) 
    json_end = '}';
  else 
    json_end = ',';
  
  formatted_data_size = sizeof(formatted_data_str) - formatted_data_offset;
  //Composizione JSON
  switch(src->schema)
  {
    case AZ_U32:
      snprintf((char*)&formatted_data_str[formatted_data_offset], formatted_data_size, "\"%s\":%u%c", 
               src->keys[AZ_JS_MAIN_KEY], src->reported.U32, 
               json_end);
      formatted_data.size = strnlen((char const*)formatted_data_str, sizeof(formatted_data_str));      
      break;
      
    case AZ_OBJ_BOOL_U32:
      if(src->reported.OBJ_BOOL_U32.BOOL)
      {
        snprintf((char*)&formatted_data_str[formatted_data_offset], formatted_data_size, "\"%s\":{\"%s\":%s,\"%s\":%u}%c", 
                 src->keys[AZ_JS_MAIN_KEY], 
                 src->keys[AZ_JS_F1_KEY], BOOL_TRUE_VALUE, 
                 src->keys[AZ_JS_F2_KEY], src->reported.OBJ_BOOL_U32.U32, 
                 json_end);
      }
      else
      {
        snprintf((char*)&formatted_data_str[formatted_data_offset], formatted_data_size, "\"%s\":{\"%s\":%s,\"%s\":%u}%c", 
                 src->keys[AZ_JS_MAIN_KEY], 
                 src->keys[AZ_JS_F1_KEY], BOOL_FALSE_VALUE, 
                 src->keys[AZ_JS_F2_KEY], src->reported.OBJ_BOOL_U32.U32, 
                 json_end);
      }
      formatted_data.size = strnlen((char const*)formatted_data_str, sizeof(formatted_data_str));
      break;
      
    case AZ_OBJ_U32_U32:
      snprintf((char*)&formatted_data_str[formatted_data_offset], formatted_data_size, "\"%s\":{\"%s\":%u,\"%s\":%u}%c", 
               src->keys[AZ_JS_MAIN_KEY], 
               src->keys[AZ_JS_F1_KEY], src->reported.OBJ_U32_U32.U32_0, 
               src->keys[AZ_JS_F2_KEY], src->reported.OBJ_U32_U32.U32_1, 
               json_end);
      formatted_data.size = strnlen((char const*)formatted_data_str, sizeof(formatted_data_str));
      break;
      
    case AZ_BOOL:
      if(src->reported.BOOL)
        snprintf((char*)&formatted_data_str[formatted_data_offset], formatted_data_size, "\"%s\":%s%c", 
                src->keys[AZ_JS_MAIN_KEY], BOOL_TRUE_VALUE,
                json_end);
      else
        snprintf((char*)&formatted_data_str[formatted_data_offset], formatted_data_size, "\"%s\":%s%c", 
                src->keys[AZ_JS_MAIN_KEY], BOOL_FALSE_VALUE,
                json_end);
      formatted_data.size = strnlen((char const*)formatted_data_str, sizeof(formatted_data_str));
      break;
      
    default:
      azure_reg.BIT.ERR_UNEXPECTED = 1;
      err = AZ_ERR_UNXPECTED;
      break;
  }
  //Append stringa formattata sul buffer destinazione
  az_append_string(&formatted_data, dest);
  
  return err;
}


/* Segnala errore processo aggiornamento device-twins. */
static void az_dev_twins_fail(Az_DevTwins_t* next_dev_twins_status)
{
  az_dev_twins_nack_count++;
  *next_dev_twins_status = AZ_DEVTWINS_IDLE;
}

