/**
*****************************************************************************************
* File          	: az_calculate_key.h
* Versione libreria	: 0.01
* Descrizione        	: Derivazione chiave individuale dispositivo 
*                         da chiave di gruppo (emergenza).
*****************************************************************************************
*
* COPYRIGHT(c) 2023 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************************
*/

#include "az_calculate_key.h"
#include "az_hmac_sha256.h"
#include "az_base_64.h"
#include "az_client.h"
#include "az_ext_resources.h"




/* Chiave di gruppo backup (GIA' decodificata da base 64) */
static const uint8_t az_backup_group_key_buff[AZ_MAX_DEVICE_KEY] = {
  0xF8, 0x65, 0xD1, 0x71, 0xC1, 0x17, 0x43, 0x27, 0x65, 0xD9, 0xA0, 0x3C, 0x6C, 0x9A, 0xA0, 0x84,
  0x21, 0xC0, 0x44, 0xDF, 0x18, 0xD6, 0x20, 0x5F, 0x5D, 0xC3, 0x56, 0x94, 0xAE, 0x1D, 0x65, 0x66,
  0x6D, 0x7D, 0x74, 0xA0, 0x2E, 0xBB, 0xDE, 0xE7, 0xAF, 0xB4, 0xC1, 0x54, 0x53, 0x58, 0x40, 0x69,
  0xEB, 0x7F, 0x6C, 0xB8, 0xDD, 0x88, 0x6C, 0x08, 0x67, 0xBF, 0x6C, 0xD7, 0x99, 0xFC, 0xF0, 0x6B

};             
/* Chiave univoca dispositivo (calcolata da chiave di gruppo) */
Az_Calc_Key_t az_calculated_key;


/* ############################################################ 
* Local functions
* ############################################################
*/




/* Calcola chiave individuale dispositivo da chiave di gruppo (emergenza). */
void az_calculate_device_key(Az_Fixed_Buffer_t* device_id)
{
  uint8_t i;
  Az_Fixed_Buffer_t key = {
    .buffer=(uint8_t*)az_backup_group_key_buff, .size=sizeof(az_backup_group_key_buff)
  };
  Az_Buffer_t devkey_buff = {
    .buffer = az_calculated_key.buffer,
    .offset = 0,
    .size = sizeof(az_calculated_key.buffer)
  };
  
  //Calcolo firma HMAC SHA-256 su device-id (con chiave di gruppo)
  hmac_sha256(device_id, &key, signature.buffer);
  //Converto la firma ottenuta in base 64 (con aggiunta terminatore stringa)
  az_encode_to_base64(&signature, 1, &devkey_buff);
  //Calcolo checksum integritÓ
  az_calculated_key.checksum = 0;
  for(i=0; az_calculated_key.buffer[i]!=0x00 && i<sizeof(az_calculated_key.buffer); i++)
    az_calculated_key.checksum ^= az_calculated_key.buffer[i]; 
}


/* Compone messaggio per scrittura su file di chiave individuale dispositivo calcolata. 
*
*return: codice errore: 
*AZ_ERR_BUILD_DEVKEY_MESSAGE (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_create_devkey_message(void)
{
  int res;
  uint16_t free_space = 0;
  
  az_calculated_devkey_data.offset = 0;
  free_space = az_calculated_devkey_data.size;
  res = snprintf((char*)az_calculated_devkey_data.buffer, free_space, AZ_CALCULATED_DEVKEY_MSG,
                 az_calculated_key.buffer,      //add device-key
                 az_calculated_key.checksum);   //add checksum
  //terminatore di stringa aggiunto da 'snprintf' ...  
  az_calculated_devkey_data.offset += check_snprintf(res, &free_space);
  
  if(az_calculated_devkey_data.offset > 0)
    return AZ_NO_ERROR;
  else
    return AZ_ERR_BUILD_DEVKEY_MESSAGE;
}
