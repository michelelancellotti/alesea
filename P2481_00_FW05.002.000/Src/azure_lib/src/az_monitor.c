/**
*****************************************************************************************
* File          	: az_monitor.c
* Versione libreria	: 0.01
* Descrizione       	: Gestione controllo connessione verso IoT Central.
*****************************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************************
*/

#include "az_monitor.h"
#include "az_utils.h"
#include "az_ext_resources.h"
#include "az_client.h"




static Az_CheckConn_t az_check_conn_status = AZ_CHECK_CONN_IDLE;        //Stato processo controllo connessione IoT Central
Az_CheckConn_t az_prev_check_conn_status;

uint8_t az_conn_fail_count = 0;                                         //Contatore fallimenti (consecutivi) connessione verso IoT Central


/* ############################################################ 
* Local functions
* ############################################################
*/




/* Gestione processo controllo connessione verso IoT Central. */
void az_process_check_conn(void)
{ 
  if(!azure_reg.BIT.LIB_INIT)                   //safety check: libreria inizializzata
    az_set_check_conn_status(AZ_CHECK_CONN_IDLE);
  
  switch(az_check_conn_status)
  {
    case AZ_CHECK_CONN_RUN:
      //#### Verifica stato connessione verso IoT Central ####
      if(az_op_callback.operation_status(AZ_OP_MQTT_CONNECT) == AZ_NO_ERROR)
      {
        az_conn_fail_count = 0;
        az_iot_hub_failover = 0;
      }
      else
      {
        if(az_iot_hub_failover)
        {
          //Rilevato failover IoT hub
          az_conn_fail_count = AZ_CHECK_CONN_MAX_FAIL;  //forzo saturazione contatore fallimenti: restart DPS immediato
          az_iot_hub_failover = 0;
        }
        else
        {
          //Rilevato errore di connessione
          if(az_conn_fail_count < AZ_CHECK_CONN_MAX_FAIL)
            az_conn_fail_count++;
        }
      }
      az_set_check_conn_status(AZ_CHECK_CONN_IDLE);
      break;
      
    default: az_set_check_conn_status(AZ_CHECK_CONN_IDLE); break;
  }
}


/* Imposta stato processo controllo connessione. */
void az_set_check_conn_status(Az_CheckConn_t new_status)
{
  az_prev_check_conn_status = az_check_conn_status;       //snapshot stato corrente
  az_check_conn_status = new_status;
}

/* Get stato processo controllo connessione. */
Az_CheckConn_t az_get_check_conn_status(void)
{
  return az_check_conn_status;
}

