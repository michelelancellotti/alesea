/**
******************************************************************************
* File          	: az_interface.c
* Versione libreria	: 0.01
* Descrizione       	: Interfaccia libreria Azure.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "az_interface.h"
#include "az_dev_twins.h"
#include "az_dps.h"
#include "az_monitor.h"




Azure_Reg_t azure_reg;                                  //Registro diagnostica errori libreria Azure
Azure_Reg_Ext_t azure_reg_ext;                          //Registro diagnostica errori libreria Azure (esteso)
Az_Process_t azure_background = AZ_PROCESS_IDLE;        //Processo in background corrente

/* Risorse MQTT: allocazione esterna alla libreria. */
Az_Buffer_t az_publish_topic;                           //Handle buffer per topic MQTT (publish): il buffer � allocato esternamente alla libreria
Az_Buffer_t az_subscribe_topic;                         //Handle buffer per topic MQTT (subscribe): il buffer � allocato esternamente alla libreria
Az_Buffer_t az_publish_data;                            //Handle buffer per dati MQTT (publish)
Az_Buffer_t az_subscribe_data;                          //Handle buffer per dati MQTT (subscribe)
Az_Buffer_t az_received_topic;                          //Handle buffer per topic MQTT su cui viene ricevuto un messaggio (subscribe): il buffer � allocato esternamente alla libreria
Az_Buffer_t az_devkey_data;                             //Handle buffer per lettura chiave individuale dispositivo
Az_Buffer_t az_calculated_devkey_data;                  //Handle buffer per scrittura chiave individuale dispositivo calcolata
Az_Op_Callback_t az_op_callback;                        //Callback per esecuzione operazioni

/* Risorse device-twins: allocazione esterna alla libreria. */
Az_Property_List_t az_properties;                       //Handle lista propriet� (applicazione)
Az_DevTwins_Callback_t az_dev_twins_callback;           //Callback per sincronizzazione tra dati applicazione e device-twins


/* ############################################################ 
* Local functions
* ############################################################
*/





/* Inizializzazione libreria Azure ed assegnazione risorse esterne. 
*
*@param[IN] publish_topic_buffer: buffer scrittura topic MQTT PUBLISH
*@param[IN] subscribe_topic_buffer: buffer scrittura topic MQTT SUBSCRIBE
*@param[IN] received_topic_buffer: buffer topic MQTT su cui viene ricevuto un messaggio (SUBSCRIBE)
*@param[IN] publish_buffer: buffer trasmissione dati (MQTT PUBLISH)
*@param[IN] subscribe_buffer: buffer ricezione dati (MQTT SUBSCRIBE)
*@param[IN] devkey_read_buffer: buffer ricezione dati (CHIAVE INDIVIDUALE DA FILE)
*@param[IN] devkey_write_buffer: buffer trasmissione dati (CHIAVE INDIVIDUALE CALCOLATA SU FILE)
*@param[IN] callback: funzioni di callback per esecuzione operazione
*
*return: codice errore: 
*AZ_ERR_LIB_INIT (parametri assenti)
*AZ_NO_ERROR
*/
Az_Err_t az_init(Az_Buffer_t publish_topic_buffer, Az_Buffer_t subscribe_topic_buffer, Az_Buffer_t received_topic_buffer,
             Az_Buffer_t publish_buffer, Az_Buffer_t subscribe_buffer, Az_Buffer_t devkey_read_buffer, Az_Buffer_t devkey_write_buffer,
             Az_Op_Callback_t callback)
{
  Az_Err_t err = AZ_NO_ERROR;
  
  if(publish_topic_buffer.buffer && subscribe_topic_buffer.buffer && received_topic_buffer.buffer &&
      publish_buffer.buffer && subscribe_buffer.buffer && devkey_read_buffer.buffer && devkey_write_buffer.buffer &&
        callback.exec_operation && callback.operation_status)
  {
    memcpy(&az_publish_topic, &publish_topic_buffer, sizeof(az_publish_topic));
    az_publish_topic.offset = 0;
    memcpy(&az_subscribe_topic, &subscribe_topic_buffer, sizeof(az_subscribe_topic));
    az_subscribe_topic.offset = 0;
    memcpy(&az_received_topic, &received_topic_buffer, sizeof(az_received_topic));
    az_received_topic.offset = 0;
    memcpy(&az_publish_data, &publish_buffer, sizeof(az_publish_data));
    az_publish_data.offset = 0;
    memcpy(&az_subscribe_data, &subscribe_buffer, sizeof(az_subscribe_data));
    az_subscribe_data.offset = 0;
    memcpy(&az_devkey_data, &devkey_read_buffer, sizeof(az_devkey_data));
    az_devkey_data.offset = 0;
    memcpy(&az_calculated_devkey_data, &devkey_write_buffer, sizeof(az_calculated_devkey_data));
    az_calculated_devkey_data.offset = 0;
    
    memcpy(&az_op_callback, &callback, sizeof(az_op_callback));
    
    azure_reg.BIT.LIB_INIT = 1;
  }
  else
  {
    azure_reg.BIT.LIB_INIT = 0;
    err = AZ_ERR_LIB_INIT;
  }
  
  return err;
}


/* Inizializzazione lista propriet� (applicazione) per libreria Azure. 
*
*@param[IN] list: lista propriet�
*@param[IN] num: numero elementi lista propriet�
*@param[IN] callback: funzioni di callback per sincronizzazione tra dati applicazione e device-twins
*
*return: codice errore: 
*AZ_ERR_PROPERTIES_INIT (parametri assenti)
*AZ_NO_ERROR
*/
Az_Err_t az_init_properties(Az_Property_t* list, uint8_t num, Az_DevTwins_Callback_t callback)
{
  Az_Err_t err = AZ_NO_ERROR;
  
  if(list && num>0 && callback.sync_app_data && callback.sync_reported_props)
  {
    az_properties.list = list;
    az_properties.num = num;
    
    memcpy(&az_dev_twins_callback, &callback, sizeof(az_dev_twins_callback));    
  }
  else
    err = AZ_ERR_PROPERTIES_INIT;
    
  return err;
}


/* Gestore processi in background libreria Azure. 
* Nota: la funzione deve essere eseguita periodicamente per la corretta gestione dei processi sottostanti.
*/
void az_process_background(void)
{  
  if(!azure_reg.BIT.LIB_INIT)                   //safety check: libreria inizializzata
    azure_background = AZ_PROCESS_IDLE;
  
  switch(azure_background)
  {
    case AZ_PROCESS_DEVTWINS:
      az_process_dev_twins();
      //Check fine processo
      if(az_get_dev_twins_status() == AZ_DEVTWINS_IDLE)
        azure_background = AZ_PROCESS_IDLE;
      break;
      
    case AZ_PROCESS_DPS:
      az_process_dps();
      //Check fine processo
      if(az_get_dps_status() == AZ_DPS_IDLE)
        azure_background = AZ_PROCESS_IDLE;
      break;
      
    case AZ_PROCESS_CHECK_CONN:
      az_process_check_conn();
      //Check fine processo
      if(az_get_check_conn_status() == AZ_CHECK_CONN_IDLE)
      {
        if(az_process_error(azure_background))
          az_set_provisioning(AZ_DEV_UNREGISTERED);        //raggiunto limite errori connessioni verso IoT Central: restart DPS
        azure_background = AZ_PROCESS_IDLE;
      }
      break;
      
    default: azure_background=AZ_PROCESS_IDLE; break;
  }
}


/* Verifica se � in esecuzione un processo in background. */
uint8_t az_background_in_progress(void)
{
  if(azure_background==AZ_PROCESS_DEVTWINS || azure_background==AZ_PROCESS_DPS || azure_background==AZ_PROCESS_CHECK_CONN)
    return 1;
  else
    return 0;
}


/* Avvia in background il processo selezionato. */
Az_Err_t az_start_background(Az_Process_t process)
{
  Az_Err_t err = AZ_NO_ERROR;
  
  if(azure_background == AZ_PROCESS_IDLE)
  {
    //Nessun processo in esecuzione
    switch(process)
    {
      case AZ_PROCESS_DEVTWINS:
        azure_reg_ext.BIT.DEVTWINS_START = 1;
        az_init_dev_twins();
        az_set_dev_twins_status(AZ_DEVTWINS_SUBSCRIBE);         //entry-point loop aggiornamento device-twins
        azure_background = process;
        break;
        
      case AZ_PROCESS_DPS:
        //Avvio processo DPS solo se:
        //- non ho ancora la chiave individuale dispositivo, oppure
        //- ho la chiave individuale dispositivo ma non sono ancora registrato (non ho mai eseguito DPS)
        //- ho la chiave individuale dispositivo ma devo verificare se sono gi� registrato, per ottenere nome IoT hub (e.g. POR dopo aver gi� completato DPS)
        switch(az_provisioning)
        {
          case AZ_DEV_UNREGISTERED_KEY_MISSING:
          case AZ_DEV_UNREGISTERED:
            azure_reg_ext.BIT.DPS_START = 1;
            az_init_dps();
            //Entry-point loop DPS
            if(az_provisioning == AZ_DEV_UNREGISTERED)
              az_set_dps_status(AZ_DPS_CONNECT);        //ho la chiave individuale dispositivo, eseguo solo DPS
            else
              az_set_dps_status(AZ_DPS_READ_DEVKEY);    //non ho ancora la chiave individuale dispositivo
            azure_background = process;
            break;
          default: 
            azure_reg_ext.BIT.DPS_DEV_REGISTERED = 1;   //resto in stato AZ_PROCESS_IDLE 
            break;
        }
        break;
        
      case AZ_PROCESS_CHECK_CONN:
        az_set_check_conn_status(AZ_CHECK_CONN_RUN);    //entry-point loop controllo connessione IoT Central
        azure_background = process;
        break;
        
      default:
        azure_background = AZ_PROCESS_IDLE;
        azure_reg.BIT.ERR_UNEXPECTED = 1;
        err = AZ_ERR_UNXPECTED; 
        break;
    }
  }
  else
  {
    //Tentativo di avvio processo con processi gi� in esecuzione 
    azure_reg.BIT.ERR_PROCESS_BUSY = 1;
    err = AZ_ERR_PROCESS_BUSY;
  }

  return err;
}


/* Ritorna 1 se il processo selezionato � andato in errore, 0 altrimenti. */
uint8_t az_process_error(Az_Process_t process)
{
  uint8_t res = 0;
  
  switch(process)
  {
    case AZ_PROCESS_DPS:      
      if(az_provisioning != AZ_DEV_REGISTERED)
        res = 1;
      break;
      
    case AZ_PROCESS_DEVTWINS:
      if(az_dev_twins_nack_count>0 || az_dev_twins_ack_count<1)
        res = 1;
      break;
      
    case AZ_PROCESS_CHECK_CONN:
      if(az_conn_fail_count >= AZ_CHECK_CONN_MAX_FAIL)
      {
        az_conn_fail_count = 0;
        res = 1;
      }
      break;
      
    default: break;
  }
  
  return res;
}
