/**
******************************************************************************
* File          	: az_utils.c
* Versione libreria	: 0.01
* Descrizione       	: Funzioni di appoggio libreria Azure.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "az_utils.h"
#include "az_ext_resources.h"
#include "az_dps.h"


extern inline uint8_t _az_number_to_upper_hex(uint8_t number);
extern inline uint8_t _az_is_byte_letter(uint8_t c);
extern inline uint8_t _az_is_byte_digit(uint8_t c);
extern inline uint8_t _az_url_should_encode(uint8_t c);


static uint8_t az_request_id;           //Id publish MQTT (loop richieste aggiornamento device-twins)
static uint8_t az_dps_request_id;       //Id publish MQTT (loop DPS)


/* ############################################################ 
* Local functions
* ############################################################
*/




/* Calcola lo spazio libero nel buffer di ingresso.
*
*@param[IN] src: buffer ingresso
*
*return: numero byte liberi
*/
uint16_t az_get_free_space(const Az_Buffer_t* src)
{
  if(src->offset < src->size)    
    return (src->size - src->offset);    //spazio libero
  else
    return 0;
}


/* Concatena stringa al buffer di ingresso. 
*
*@param[IN] src: stringa da appendere
*@param[OUT] dest: buffer destinazione
*
*return: codice errore: 
*AZ_ERR_APPEND_STRING (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_append_string(const Az_Fixed_Buffer_t* src, 
                          Az_Buffer_t* dest)
{
  uint16_t free_space = 0;  
  
  free_space = az_get_free_space(dest);         //calcolo spazio libero nel buffer di destinazione
  if(free_space==0 || (src->size)>free_space)
  {
    azure_reg.BIT.ERR_APPEND_STRING = 1;
    return AZ_ERR_APPEND_STRING;
  }
  
  uint8_t* dest_begin = dest->buffer + dest->offset;
  memcpy(dest_begin, src->buffer, src->size);
  dest->offset += src->size;
  
  return AZ_NO_ERROR;
}


/* Concatena numero (convertito in stringa) al buffer di ingresso. 
*
*@param[IN] src: numero da appendere
*@param[OUT] dest: buffer destinazione
*
*return: codice errore: 
*AZ_ERR_APPEND_NUMBER (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_append_number(uint32_t number, 
                          Az_Buffer_t* dest)
{
  uint16_t free_space = 0;
  int res;
  
  free_space = az_get_free_space(dest);         //calcolo spazio libero nel buffer di destinazione
  if(free_space == 0)
  {
    azure_reg.BIT.ERR_APPEND_NUMBER = 1;
    return AZ_ERR_APPEND_NUMBER;
  }
  
  uint8_t* dest_begin = dest->buffer + dest->offset;
  res = snprintf((char*)dest_begin, free_space, "%u", number);
  if(res<0 || res>=free_space)
  {
    azure_reg.BIT.ERR_APPEND_NUMBER = 1;
    return AZ_ERR_APPEND_NUMBER;
  }
  else
  {
    dest->offset += res;
    return AZ_NO_ERROR;
  }
}


/* Concatena buffer al buffer di ingresso. 
*
*@param[IN] src: buffer da appendere
*@param[OUT] dest: buffer destinazione
*
*return: codice errore: 
*AZ_ERR_APPEND_BUFFER (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_append_buffer(const Az_Buffer_t* src, 
                          Az_Buffer_t* dest)
{
  uint16_t free_space = 0;
  uint16_t to_copy = src->offset;
  
  free_space = az_get_free_space(dest);         //calcolo spazio libero nel buffer di destinazione
  if(free_space==0 || to_copy>free_space)
  {
    azure_reg.BIT.ERR_APPEND_BUFFER = 1;
    return AZ_ERR_APPEND_BUFFER;
  }
  
  uint8_t* dest_begin = dest->buffer + dest->offset;
  memcpy(dest_begin, src->buffer, to_copy);
  dest->offset += to_copy;
  
  return AZ_NO_ERROR;
}


/* Concatena terminatore di stringa in coda al buffer d'ingresso.
* Nota: se il buffer in ingresso � pieno il terminatore sovrascrive l'ultimo carattere del buffer. 
*
*@param[OUT] dest: buffer destinazione
*
*return: codice errore: 
*AZ_ERR_APPEND_TERMINATOR (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_append_terminator(Az_Buffer_t* dest)
{
  if(dest->offset < dest->size)
  {
    dest->buffer[dest->offset] = 0x00;          //aggiungo terminatore in coda
    return AZ_NO_ERROR;
  }
  else
  {
    dest->buffer[dest->size-1] = 0x00;          //buffer pieno: sovrascrivo ultimo carattere con terminatore
    azure_reg.BIT.ERR_APPEND_TERMINATOR = 1;
    return AZ_ERR_APPEND_TERMINATOR;
  }
}


/* Converte di tipo, da 'Az_Buffer_t' a 'Az_Fixed_Buffer_t'.
*  N.B. La conversione di tipo � sicura SOLO SE il buffer in ingresso � stato manipolato tramite le funzioni di libreria.
*       Le funzioni di libreria garantiscono che lo spazio che si dichiara occupato (campo .offset) sia minore/uguale
*       della dimensione MAX (campo .size) dell'array di riferimento (campo .buffer). 
*
*@param[IN] src: buffer da convertire
*
*return: buffer convertito
*/
Az_Fixed_Buffer_t az_convert_buffer_2_fixed_buffer(Az_Buffer_t* src)
{
  Az_Fixed_Buffer_t res;
  
  res.buffer = src->buffer;
  res.size = src->offset;
  return res;
}


/* Pulisce il buffer d'ingresso, riportando l'offset all'inizio del buffer.
*  N.B. La pulizia consiste solamente nell'inserire il terminatore di stringa all'inizio del buffer.
*       Quindi i caratteri successivi al primo restano invariati.
*
*@param[IN] src: buffer da pulire
*/
void az_clear_buffer_fast(Az_Buffer_t* src)
{
  src->buffer[0] = 0x00;
  src->offset = 0;
}


/* Ricerca la chiave di un oggetto JSON (prima occorrenza) all'interno del buffer in ingresso.
*
*@param[IN] key, chiave oggetto JSON da cercare
*@param[IN] src, buffer sorgente
*@param[OUT] offset, posizione (assoluta) primo carattere dopo l'occorrenza
*
*return: 0=chiave NON trovata, 1=chiave trovata
*/
uint8_t az_find_key(const uint8_t* key, Az_Buffer_t* src, 
                    uint16_t* offset)
{
  uint8_t formatted_key[24];
  uint16_t relative_offset = 0;
  
  memset(formatted_key, 0x00, sizeof(formatted_key));
  snprintf((char*)formatted_key, sizeof(formatted_key), "\"%s\"", key);
  if(searchString(src->buffer, formatted_key, src->offset, src->size, &relative_offset, 1, 1))
  {
    *offset = src->offset + relative_offset;    //calcolo offset assoluto
    return 1;
  }
  else
  {
    *offset = 0;
    return 0;
  }
}


/* Ricerca il valore di un oggetto JSON di tipo stringa all'interno del buffer in ingresso. 
*
*@param[IN] key: chiave oggetto JSON
*@param[IN] src: buffer sorgente
*@param[OUT] dest: buffer destinazione
*
*return: codice errore: 
*AZ_ERR_FIND_VALUE_STRING (chiave oggetto JSON non trovata)
*AZ_ERR_NULL_STRING (valore stringa nullo)
*AZ_ERR_APPEND_STRING (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_find_value_string(const uint8_t* key, Az_Buffer_t* src,
                              Az_Buffer_t* dest)
{
  Az_Err_t err = AZ_NO_ERROR;
  uint16_t value_start=0, size=0;
  Az_Fixed_Buffer_t value;
  
  if(az_find_key(key, src, &value_start))       //cerco la chiave
  {
    //Chiave trovata
    searchField(src->buffer, '"', value_start, src->size);
    //Calcolo dimensione valore del campo JSON (stringa)
    value_start = searchField_res[0].idx + 1;           //il valore del campo JSON (stringa) inizia dopo la prima occorrenza di '"'
    if(searchField_res[1].idx > value_start)            //il valore del campo JSON (stringa) finisce dopo la seconda occorrenza di '"'
      size = searchField_res[1].idx - value_start;
    if(size > 0)
    {
      //Imposto puntatore a dati sorgente
      value.buffer = &src->buffer[value_start];
      value.size = size;
      err = az_append_string(&value, dest);
    }
    else
      err = AZ_ERR_NULL_STRING;                 //valore stringa nullo
  }
  else
    err = AZ_ERR_FIND_VALUE_STRING;             //chiave non trovata
  
  return err;
}


/* Esegue URL-encoding del buffer in ingresso. 
*
*@param[IN] src, stringa sorgente
*@param[OUT] dest, buffer destinazione (risultato encoding in append)
*
*return: codice errore: 
*AZ_ERR_URL_ENCODING (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_url_encode(const Az_Fixed_Buffer_t* src, 
                       Az_Buffer_t* dest)
{
  uint16_t i, free_space=0;
  uint8_t ch;
  
  free_space = az_get_free_space(dest);         //calcolo spazio libero nel buffer di destinazione
  if(free_space == 0)
  {
    azure_reg.BIT.ERR_URL_ENCODING = 1;
    return AZ_ERR_URL_ENCODING;
  }
  
  uint8_t* dest_begin = dest->buffer + dest->offset;
  uint8_t* dest_ptr = dest_begin;
  uint8_t* dest_end = dest_begin + free_space;    
  
  for(i=0; i<src->size; i++)
  {
    ch = src->buffer[i];
    if(_az_url_should_encode(ch))
    {
      //Eseguo URL-encoding
      if((dest_ptr + 2) >= dest_end)
      {
        azure_reg.BIT.ERR_URL_ENCODING = 1;
        return AZ_ERR_URL_ENCODING;
      }
      
      dest_ptr[0] = '%';
      dest_ptr[1] = _az_number_to_upper_hex((ch>>4));
      dest_ptr[2] = _az_number_to_upper_hex((ch&0x0F));
      dest_ptr += 3;
    }
    else
    {
      //URL-encoding non necessaria: copio il carattere sorgente
      if(dest_ptr >= dest_end)
      {
        azure_reg.BIT.ERR_URL_ENCODING = 1;
        return AZ_ERR_URL_ENCODING;
      }
      
      *dest_ptr = ch;
      dest_ptr++;
    }
  }
  
  dest->offset += ((int32_t)(dest_ptr - dest_begin)); //sposto offset buffer destinazione
  return AZ_NO_ERROR;
}


/* Imposta topic PUBLISH/SUBSCRIBE MQTT, in base alla tipologia di evento. 
*
*@param[IN] event, tipologia evento
*@param[OUT] topic, buffer destinazione (accesso in scrittura sempre dall'inizio del buffer)
*
*return: codice errore:
*AZ_ERR_UNXPECTED (tipo evento non gestito)
*AZ_ERR_BUILD_TOPIC (dimensione buffer destinazione insufficiente)
*AZ_NO_ERROR
*/
Az_Err_t az_set_topic(Az_PubSubEvnt_t event, 
                      Az_Buffer_t* topic)
{
  Az_Err_t err;
  char* dest_ptr;
  int res = -1;
  
  az_clear_buffer_fast(topic);
  dest_ptr = (char*)&topic->buffer[topic->offset];
  
  switch(event)
  {
    //------------------------ PUBLISH ------------------------
    case AZ_EV_PUB_TELEMETRY:      
      res = snprintf(dest_ptr, topic->size, AZ_TOPIC_PUB_TELEMETRY, az_device_id.buffer);      
      break;
    case AZ_EV_PUB_GET_DESIRED_PROPS:      
      res = snprintf(dest_ptr, topic->size, AZ_TOPIC_PUB_GET_DESIRED_PROPS, az_request_id);
      az_request_id++;
      break;
    case AZ_EV_PUB_UPDATE_REPORTED_PROPS:
      res = snprintf(dest_ptr, topic->size, AZ_TOPIC_PUB_UPDATE_REPORTED_PROPS, az_request_id);
      az_request_id++;
      break;
    case AZ_EV_PUB_DPS_REGISTRATION:
      res = snprintf(dest_ptr, topic->size, AZ_TOPIC_PUB_DPS_REGISTRATION, az_dps_request_id);
      az_dps_request_id++;
      break;
    case AZ_EV_PUB_DPS_GET_STATUS:
      res = snprintf(dest_ptr, topic->size, AZ_TOPIC_PUB_DPS_GET_STATUS, az_dps_request_id, az_dps_operation_id);
      az_dps_request_id++;
      break;
    //------------------------ SUBSCRIBE ----------------------
    case AZ_EV_SUB_DEVTWINS_RESPONSE:
      res = snprintf(dest_ptr, topic->size, AZ_TOPIC_SUB_DEVTWINS); 
      break;      
    case AZ_EV_SUB_DPS_RESPONSE:
      res = snprintf(dest_ptr, topic->size, AZ_TOPIC_SUB_DPS);
      break;
      
    default:
      azure_reg.BIT.ERR_UNEXPECTED = 1;
      return AZ_ERR_UNXPECTED;
      break;
  }
  //terminatore di stringa aggiunto da 'snprintf' ...
  
  if(res<0 || res>=topic->size)
  {
    err = AZ_ERR_BUILD_TOPIC;
    azure_reg.BIT.ERR_BUILD_TOPIC = 1;
  }
  else
  {
    topic->offset += res;
    err = AZ_NO_ERROR;
  }
  
  return err;
}


/* Verifica se � necessario eseguire URL-encoding sul carattere in input. */
inline uint8_t _az_url_should_encode(uint8_t c)
{
  switch (c)
  {
    case '-':
    case '_':
    case '.':
    case '~':
      return 0;
    default:
      return !(_az_is_byte_digit(c) || _az_is_byte_letter(c));
  }
}
