/**
******************************************************************************
* File          	: az_hmac_sha256.c
* Versione libreria	: 0.01
* Descrizione       	: Calcolo firma messaggio HMAC (keyed-Hash Message Authentication Code).
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "az_hmac_sha256.h"
#include "az_sha256.h"


static HMACContext hmac_ctx;                                   //Contesto calcolo firma HMAC (SHA-256)
static unsigned char k_ipad[SHA256_Message_Block_Size];        //Inner padding - key XORd with ipad

/* ############################################################ 
* Local functions
* ############################################################
*/
static int hmac_sha256_reset(HMACContext* ctx, const unsigned char* key, int key_len);
static int hmac_sha256_input(HMACContext* ctx, const unsigned char* text, int text_len);
static int hmac_sha256_result(HMACContext* ctx, uint8_t digest[SHA256HashSize]);




/*
*  hmac_sha256
*
*  Description:
*      This function will compute an HMAC message digest.
*
*  Parameters:
*      message: [in]
*          An array of characters representing the message.
*      key: [in]
*          The secret shared key.
*      digest: [out]
*          Where the digest is returned.
*
*  Returns:
*      sha Error Code.
*
*/
int hmac_sha256(const Az_Fixed_Buffer_t* message, const Az_Fixed_Buffer_t* key, uint8_t digest[SHA256HashSize])
{
  memset(&hmac_ctx, 0x00, sizeof(hmac_ctx));
  
  int res = hmac_sha256_reset(&hmac_ctx, key->buffer, key->size) ||
              hmac_sha256_input(&hmac_ctx, message->buffer, message->size) ||
                hmac_sha256_result(&hmac_ctx, digest);
  
  if(res != shaSuccess) 
    azure_reg.BIT.ERR_HMAC_SHA_256 = 1;
  return res;
}


/*
*  hmac_sha256_reset
*
*  Description:
*      This function will initialize the hmacContext in preparation for computing a new HMAC message digest.
*
*  Parameters:
*      context: [in/out]
*          The context to reset.
*      key: [in]
*          The secret shared key.
*      key_len: [in]
*          The length of the secret shared key.
*
*  Returns:
*      sha Error Code.
*
*/
static int hmac_sha256_reset(HMACContext* ctx, const unsigned char* key, int key_len)
{
  int i, blocksize, hashsize;    
  unsigned char tempkey[SHA256HashSize];        //temporary buffer when keylen > blocksize
  
  /* MLV7: array 'k_ipad' diventato globale per limitare l'utilizzo di stack */
  
  if(!ctx) return shaNull;
  
  blocksize = ctx->blockSize = SHA256_Message_Block_Size;
  hashsize = ctx->hashSize = SHA256HashSize;

  //If key is longer than the hash blocksize, reset it to key = HASH(key)
  if(key_len > blocksize) 
  {
    /* 
    * MLV7: se necessario ricalcolare la chiave,
    * sfrutto lo stesso contesto utilizzato in seguito per l'hashing del messaggio 
    * --> limito l'utilizzo di stack 
    */
    int err = SHA_256_Reset(&ctx->shaContext) ||
      SHA_256_Input(&ctx->shaContext, key, key_len) ||
        SHA_256_Result(&ctx->shaContext, tempkey);
    if(err != shaSuccess) return err;
    
    key = tempkey;
    key_len = hashsize;
  }
  
  /*
  * The HMAC transform looks like:
  *
  * SHA(K XOR opad, SHA(K XOR ipad, text))
  *
  * where K is an n byte key.
  * ipad is the byte 0x36 repeated blocksize times
  * opad is the byte 0x5c repeated blocksize times
  * and text is the data being protected.
  */  
  //Store key into the pads, XOR'd with ipad and opad values
  for(i=0; i<key_len; i++) {
    k_ipad[i] = key[i] ^ 0x36;
    ctx->k_opad[i] = key[i] ^ 0x5c;
  }
  //Remaining pad bytes are '\0' XOR'd with ipad and opad values
  for (; i<blocksize; i++) {
    k_ipad[i] = 0x36;
    ctx->k_opad[i] = 0x5c;
  }
  
  //Perform inner hash
  //Init context for 1st pass
  return SHA_256_Reset(&ctx->shaContext) ||
    //and start with inner pad
    SHA_256_Input(&ctx->shaContext, k_ipad, blocksize);
}


/*
*  hmac_sha256_input
*
*  Description:
*      This function accepts an array of octets as the next portion of the message.
*
*  Parameters:
*      context: [in/out]
*          The HMAC context to update
*      message_array: [in]
*          An array of characters representing the next portion of the message.
*      length: [in]
*          The length of the message in message_array
*
*  Returns:
*      sha Error Code.
*
*/
static int hmac_sha256_input(HMACContext* ctx, const unsigned char* text, int text_len)
{
  if (!ctx) return shaNull;
  return SHA_256_Input(&ctx->shaContext, text, text_len);
}


/*
* hmac_sha256_result
*
* Description:
*   This function will return the N-byte message digest into the Message_Digest array provided by the caller.
*   NOTE: The first octet of hash is stored in the 0th element, the last octet of hash in the 32th element.
*
* Parameters:
*   context: [in/out]
*     The context to use to calculate the HMAC hash.
*   digest: [out]
*     Where the digest is returned.
*
* Returns:
*   sha Error Code.
*
*/
static int hmac_sha256_result(HMACContext* ctx, uint8_t digest[SHA256HashSize])
{
  if(!ctx) return shaNull;
  
  //Finish up 1st pass
  //(use digest here as a temporary buffer)
  return SHA_256_Result(&ctx->shaContext, digest) ||    
    //Perform outer SHA
    //Init context for 2nd pass
    SHA_256_Reset(&ctx->shaContext) ||      
      //Start with outer pad
      SHA_256_Input(&ctx->shaContext, ctx->k_opad, ctx->blockSize) ||        
        //Then results of 1st hash
        SHA_256_Input(&ctx->shaContext, digest, ctx->hashSize) ||          
          //Finish up 2nd pass
          SHA_256_Result(&ctx->shaContext, digest);
}
