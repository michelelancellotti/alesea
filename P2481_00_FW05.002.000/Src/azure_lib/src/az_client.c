/**
******************************************************************************
* File          	: az_client.c
* Versione libreria	: 0.01
* Descrizione       	: Gestione client per connessione verso Azure.
******************************************************************************
*
* COPYRIGHT(c) 2022 "Computec srl"
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of "Computec srl" nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/

#include "az_client.h"
#include "az_utils.h"
#include "az_interface.h"


#define SLASH_STR               "\x2F"
const Az_Fixed_Buffer_t SLASH_CHAR = {.buffer=(uint8_t*)SLASH_STR, .size=sizeof(SLASH_STR)-1};
#define API_VERSION_STR         "/?api-version=2021-04-12"
const Az_Fixed_Buffer_t API_VERSION = {.buffer=(uint8_t*)API_VERSION_STR, .size=sizeof(API_VERSION_STR)-1};
#define API_VERSION_DPS_STR     "/api-version=2019-03-31"
const Az_Fixed_Buffer_t API_VERSION_DPS = {.buffer=(uint8_t*)API_VERSION_DPS_STR, .size=sizeof(API_VERSION_DPS_STR)-1};
#define DEMO_DEV_ID             "dev_id_00"


/* ID univoco dispositivo */
Az_Fixed_Buffer_t az_device_id = {.buffer=DEMO_DEV_ID, .size=sizeof(DEMO_DEV_ID)-1};
/* Host-name IoT Hub corrente */
uint8_t az_iot_hub_hostname_buff[AZ_MAX_IOT_HUB];
/* Chiave univoca dispositivo */
uint8_t az_device_key_buff[AZ_MAX_DEVICE_KEY];


uint8_t az_username_buff[AZ_MAX_USER_NAME];     //User-name MQTT (calcolato dinamicamente)
uint8_t az_iot_hub_failover;                    //Flag segnalazione failover IoT hub


/* ############################################################ 
* Local functions
* ############################################################
*/
static Az_Err_t az_create_username(Az_Client_t* client, Az_Service_t service,
                                   Az_Buffer_t* user_name);




/* Calcola credenziali di accesso per connessione MQTT verso Azure:
*  - user-name MQTT
*  - password MQTT (SAS token)
*
*@param[IN] unix_time, unix-time corrente (oppure 0 per significare dato N/D)
*#param[IN] token_type, tipologia token
*/
void az_calculate_credentials(uint32_t unix_time, Az_SAS_Token_t token_type)
{
  uint16_t str_size;
  //Init struttura host-name IoT Hub hostname
  str_size = strnlen((char const*)az_iot_hub_hostname_buff, sizeof(az_iot_hub_hostname_buff));
  Az_Fixed_Buffer_t az_iot_hub_hostname = {
    .buffer=az_iot_hub_hostname_buff, .size=str_size
  };
  //Init struttura chiave dispositivo
  str_size = strnlen((char const*)az_device_key_buff, sizeof(az_device_key_buff));
  Az_Fixed_Buffer_t az_device_key = {
    .buffer=az_device_key_buff, .size=str_size
  };
  //Init struttura Azure client
  Az_Client_t client = {
    .hostname = &az_iot_hub_hostname,
    .device_id = &az_device_id,
    .device_key = &az_device_key
  };
  
  //Compone user-name MQTT per connessione verso Azure 
  //--> output in 'az_username_buff'
  Az_Buffer_t az_user_name = {
    .buffer=az_username_buff, .offset=0, .size=sizeof(az_username_buff)
  };
  if(token_type == STANDARD_TOKEN)
    az_create_username(&client, AZ_SERVICE_IOT_CENTRAL, &az_user_name);
  else
    az_create_username(&client, AZ_SERVICE_DPS, &az_user_name);
  //Calcola password MQTT per connessione verso Azure (SAS token)
  //--> output in 'az_sas_token_buff'
  Az_Buffer_t az_password = {
    .buffer=az_sas_token_buff, .offset=0, .size=sizeof(az_sas_token_buff)
  };
  az_calculate_sas_token(&client, unix_time, AZ_TOKEN_DFLT_DURATION, token_type, &az_password); 
}


/* Imposta device-id utilizzato dalla libreria Azure.
*
*@param[IN] device_id, device-id
*@param[IN] device_id_size, dimensione device-id
*/
void az_assign_device_id(uint8_t* device_id, uint16_t device_id_size)
{
  if(device_id && device_id_size>0)
  {
    az_device_id.buffer = device_id;
    az_device_id.size = device_id_size;
  }
}


/* Calcola user-name per la connessione MQTT ad Azure.
*
*@param[IN] client, handle client
*@param[IN] service, tipologia servizio remoto
*@param[OUT] dest, buffer destinazione (risultato calcolo user-name in append)
*
*return: codice errore: 
*AZ_ERR_UNXPECTED (tipo servizio non gestito)
*AZ_NO_ERROR
*/
static Az_Err_t az_create_username(Az_Client_t* client, Az_Service_t service,
                                   Az_Buffer_t* user_name)
{
  Az_Err_t res = AZ_NO_ERROR;
  
  user_name->offset = 0;
  switch(service)
  {
    case AZ_SERVICE_IOT_CENTRAL:
      az_append_string(client->hostname, user_name);    //Append host-name IoT Hub
      az_append_string(&SLASH_CHAR, user_name);         //Append "/"
      az_append_string(client->device_id, user_name);   //Append device-id
      az_append_string(&API_VERSION, user_name);        //Append "/?api-version=YYYY-MM-DD"
      break;
      
    case AZ_SERVICE_DPS:
      az_append_string(&SCOPE_ID, user_name);           //Append scope-id (DPS)
      az_append_string(&REGISTRATIONS, user_name);      //Append "/registrations/"
      az_append_string(client->device_id, user_name);   //Append device-id
      az_append_string(&API_VERSION_DPS, user_name);    //Append "/api-version=YYYY-MM-DD"
      break;
      
    default:
      azure_reg.BIT.ERR_UNEXPECTED = 1;
      res = AZ_ERR_UNXPECTED;
      break;
  }

  az_append_terminator(user_name);      //Append terminatore di stringa (user-name pu� essere di dimensione variabile)
  return res;
}
