#include "FuelGauge.h"
#include "i2c.h"

uint8_t FuelGauge;

int32_t FG_CorrenteMedia, FG_CorrenteTot, FG_CorrenteSample;

uint16_t FGSizeTX,FGSizeRX;
uint8_t FGBlock[3];
uint8_t FGRead[40];
uint16_t FGAddressWD = 0xAA;
uint16_t FGAddressRD = 0xAB;


void GestioneFuelGauge(void)    //chiamata ogni secondo
{
  FG_CorrenteTot += FG_getStandardCommands(GET_MEAN_CURRENT_LSB);
  FG_CorrenteSample++;
  FG_CorrenteMedia = -FG_CorrenteTot/FG_CorrenteSample;
}

void InitFuelGaugeInstant(void)
{
  FG_CorrenteTot = 0;
  FG_CorrenteSample = 0;
}

int32_t FG_getStandardCommands(uint16_t add)
{
  int32_t data;
  int16_t app;
  FGBlock[0] = add;
  FGSizeTX = 1;
  FGSizeRX = 2;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3, FGAddressWD, (uint8_t*)&FGBlock[0], FGSizeTX, 1000);
  HAL_Delay(1);
  HAL_I2C_Master_Receive( (I2C_HandleTypeDef *)&hi2c3,FGAddressRD, (uint8_t*)&FGRead[0], FGSizeRX, 1000);
  data = FGRead[0];
  data |= (FGRead[1] << 8) & 0xFF00;
  app = (int16_t)data;
  return app;
}



int32_t FG_SetManufactoringParam(uint16_t add)
{ 
  FGBlock[0] = MANUFACTURER_ACCESS;
  FGBlock[1] = (uint8_t)(0x00FF & add);
  FGBlock[2] = (uint8_t)(0x00FF & (add>>8));
  FGSizeTX = 3;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3, FGAddressWD, (uint8_t*)&FGBlock[0], FGSizeTX, 1000);
}
/*
int32_t FG_GetManufacturingStatus(void)
{ 
  FGBlock[0] = MANUFACTURER_ACCESS;
  FGBlock[1] = (uint8_t)(0x00FF & MANUFACTURING_STATUS);
  FGBlock[2] = (uint8_t)(0x00FF & (MANUFACTURING_STATUS>>8));
  FGSizeTX = 3;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3, FGAddressWD, (uint8_t*)&FGBlock[0], FGSizeTX, 1000);
  HAL_Delay(1);
  FGSizeTX = 1;
  FGSizeRX = 2;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3,FGAddressWD, (uint8_t*)&FGBlock[0], FGSizeTX, 1000);
  HAL_Delay(1);
  HAL_I2C_Master_Receive( (I2C_HandleTypeDef *)&hi2c3,FGAddressRD, (uint8_t*)&FGRead[0], FGSizeRX, 1000);
}
*/

/*
int32_t FG_GetManufactoringParam(uint16_t add)
{ 
  FGBlock[0] = MANUFACTURER_ACCESS;
  FGBlock[1] = (uint8_t)(0x00FF & add);
  FGBlock[2] = (uint8_t)(0x00FF & (add>>8));
  FGSizeTX = 3;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3, FGAddressWD, (uint8_t*)&FGBlock[0], FGSizeTX, 1000);
  HAL_Delay(1);
  //FGSizeTX = 1;
  //FGSizeRX = 4;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3,FGAddressWD, (uint8_t*)&FGBlock[0], 1, 1000);
  HAL_Delay(1);
  HAL_I2C_Master_Receive( (I2C_HandleTypeDef *)&hi2c3,FGAddressRD, (uint8_t*)&FGRead[0], 2, 1000);
}
*/
/*
uint32_t FG_getMacData(void)
{
  FGBlock[0] = MAC_DATA;
  FGSizeTX = 1;
  FGSizeRX = 32;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3, FGAddressWD, (uint8_t*)&FGBlock[0], FGSizeTX, 1000);
  HAL_Delay(1);
  HAL_I2C_Master_Receive( (I2C_HandleTypeDef *)&hi2c3,FGAddressRD, (uint8_t*)&FGRead[0], FGSizeRX, 1000);
}

uint32_t FG_getMacDataLenght(void)
{
  FGBlock[0] = MAC_DATA_LENGHT;
  FGSizeTX = 1;
  FGSizeRX = 2;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3, FGAddressWD, (uint8_t*)&FGBlock[0], FGSizeTX, 1000);
  HAL_Delay(1);
  HAL_I2C_Master_Receive( (I2C_HandleTypeDef *)&hi2c3,FGAddressRD, (uint8_t*)&FGRead[0], FGSizeRX, 1000);
}


int32_t FG_CalibrationEnable(void)
{ 
  FGBlock[0] = MANUFACTURER_ACCESS;
  FGBlock[1] = (uint8_t)(0x00FF & CALIBRATION_MODE);
  FGBlock[2] = (uint8_t)(0x00FF & (CALIBRATION_MODE>>8));
  FGSizeTX = 3;
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c3, FGAddressWD, (uint8_t*)&FGBlock[0], FGSizeTX, 1000);
}
*/


