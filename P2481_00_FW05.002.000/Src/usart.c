/**
  ******************************************************************************
  * File Name          : USART.c
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */

#include "modem.h"
#include "wifi.h"



/* Seriale verso modem */
#define handle_UART_Modem                       huart1
#define UART_MODEM                              USART1

UART_Mng_t Seriale_Modem;

/* Seriale verso modulo WiFi */
#define handle_UART_WiFi                        huart2
#define UART_WIFI                               USART2

UART_Mng_t Seriale_WiFi;

/* USER CODE END 0 */

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USART1 init function */

void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }

}
/* USART2 init function */

void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspInit 0 */

  /* USER CODE END USART1_MspInit 0 */
    /* USART1 clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    GPIO_InitStruct.Pin = CPU_TX_GSM_Pin|CPU_RX_GSM_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    HAL_NVIC_SetPriority(USART1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspInit 1 */

  /* USER CODE END USART1_MspInit 1 */
  }
  else if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspInit 0 */

  /* USER CODE END USART2_MspInit 0 */
    /* USART2 clock enable */
    __HAL_RCC_USART2_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    GPIO_InitStruct.Pin = WIFI_TX_HW4_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_USART2;
    HAL_GPIO_Init(WIFI_TX_HW4_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = WIFI_RX_HW4_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_USART2;
    HAL_GPIO_Init(WIFI_RX_HW4_GPIO_Port, &GPIO_InitStruct);

    /* USART2 interrupt Init */
    HAL_NVIC_SetPriority(USART2_IRQn, 2, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspInit 1 */

  /* USER CODE END USART2_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspDeInit 0 */

  /* USER CODE END USART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();

    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    HAL_GPIO_DeInit(GPIOA, CPU_TX_GSM_Pin|CPU_RX_GSM_Pin);

    /* USART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspDeInit 1 */

  /* USER CODE END USART1_MspDeInit 1 */
  }
  else if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspDeInit 0 */

  /* USER CODE END USART2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART2_CLK_DISABLE();

    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    HAL_GPIO_DeInit(GPIOA, WIFI_TX_HW4_Pin|WIFI_RX_HW4_Pin);

    /* USART2 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspDeInit 1 */

  /* USER CODE END USART2_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */

//-----------------------------------------------------------------------------
// Function Name: Init_SCI
// Description  : funzione di inizializzazione variabili SCIx
//-----------------------------------------------------------------------------
void Init_SCI(uint8_t id_seriale)
{
  UART_HandleTypeDef* uart_handle = NULL;
  UART_Mng_t* seriale_handle = NULL;
  uint8_t uart_gestita;
  
  uart_gestita = 1;
  switch(id_seriale)
  {
    case SERIALE_MODEM: 
      uart_handle = &handle_UART_Modem;
      seriale_handle = &Seriale_Modem;
      break;
      
    case SERIALE_WIFI: 
      uart_handle = &handle_UART_WiFi;
      seriale_handle = &Seriale_WiFi; 
      break;
      
    default: uart_gestita=0; break;
  }
  
  if(uart_gestita)
  {
    seriale_handle->buffer.tx_cnt = seriale_handle->buffer.tx_end = seriale_handle->buffer.tx_st = 0;
    seriale_handle->buffer.rx_cnt = seriale_handle->buffer.rx_end = seriale_handle->buffer.rx_st = 0;
    seriale_handle->buffer.tx_avail = 1;
    seriale_handle->buffer.rx_avail = 0;
    
    //### IRQ ###
    //Armo interrupt RX
    if(HAL_UART_Receive_IT(uart_handle, seriale_handle->buffer.rx_buffer, 1) != HAL_OK)
      seriale_handle->debug.hal_err_count_RX++;
  }
}


//-----------------------------------------------------------------------------
// Function Name: Write_SCI
// Description  : funzione di scrittura su SCIx
//-----------------------------------------------------------------------------
void Write_SCI(uint8_t id_seriale, uint8_t *str, uint16_t cnt)//funzione di scrittura su buffer circolare per periferica DEVICE e inizio tx se libero
{
  UART_HandleTypeDef* uart_handle = NULL;
  UART_Mng_t* seriale_handle = NULL;
  uint8_t uart_gestita;
  
  uart_gestita = 1;  
  switch(id_seriale)
  {
    case SERIALE_MODEM: 
      uart_handle = &handle_UART_Modem;
      seriale_handle = &Seriale_Modem;
      break;
      
    case SERIALE_WIFI: 
      uart_handle = &handle_UART_WiFi;
      seriale_handle = &Seriale_WiFi; 
      break;
      
    default: uart_gestita=0; break;
  }
  
  if(!uart_gestita)
    return;
  if((seriale_handle->buffer.tx_avail==0) || (cnt>sizeof(seriale_handle->buffer.tx_buffer)))
    return;
  
  memcpy(seriale_handle->buffer.tx_buffer, str, cnt);
  seriale_handle->buffer.tx_cnt = cnt;
  
  seriale_handle->buffer.tx_avail = 0;
  if(HAL_UART_Transmit_IT(uart_handle, seriale_handle->buffer.tx_buffer, seriale_handle->buffer.tx_cnt) != HAL_OK)
    seriale_handle->debug.hal_err_count_TX++;
}


//-----------------------------------------------------------------------------
// Function Name: HAL_UART_TxCpltCallback
// Description  : funzione di gestione interrupt SCIx in trasmissione
//-----------------------------------------------------------------------------
void HAL_UART_TxCpltCallback(UART_HandleTypeDef* huart)
{
  if(huart->Instance == UART_MODEM)
  {
    /* Seriale modem */
    Seriale_Modem.buffer.tx_avail = 1;
  }
  else if(huart->Instance == UART_WIFI)
  {
    /* Seriale WiFi */
    Seriale_WiFi.buffer.tx_avail = 1;
  }
}

//-----------------------------------------------------------------------------
// Function Name: HAL_UART_RxCpltCallback
// Description  : funzione di gestione interrupt SCIx in ricezione
//-----------------------------------------------------------------------------
void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
  uint8_t temp_RX_Modem, temp_RX_WiFi;

  if(huart->Instance == UART_MODEM)
  {
    /* Seriale modem */
    temp_RX_Modem = Seriale_Modem.buffer.rx_buffer[0];
    
    modem_ProcessaByte(temp_RX_Modem);
    
    Seriale_Modem.buffer.rx_cnt++;
#ifdef MODEM_UART_LOG
    dbg_modem[dbg_modem_idx++] = temp_RX_Modem;
    if(dbg_modem_idx >= sizeof(dbg_modem))
      dbg_modem_idx = 0;
#endif
    
    if(HAL_UART_Receive_IT(&handle_UART_Modem, Seriale_Modem.buffer.rx_buffer, 1) != HAL_OK)
      Seriale_Modem.debug.hal_err_count_RX++;
  }
  else if(huart->Instance == UART_WIFI)
  {
    /* Seriale WiFi */
    temp_RX_WiFi = Seriale_WiFi.buffer.rx_buffer[0];
    
    wifi_ProcessaByte(temp_RX_WiFi);
    
    Seriale_WiFi.buffer.rx_cnt++;
#ifdef WIFI_UART_LOG
    dbg_wifi[dbg_wifi_idx++] = temp_RX_WiFi;
    if(dbg_wifi_idx >= sizeof(dbg_wifi))
      dbg_wifi_idx = 0;
#endif
    
    if(HAL_UART_Receive_IT(&handle_UART_WiFi, Seriale_WiFi.buffer.rx_buffer, 1) != HAL_OK)
      Seriale_WiFi.debug.hal_err_count_RX++;
  }
}

//-----------------------------------------------------------------------------
// Function Name: HAL_UART_ErrorCallback
// Description  : funzione generica di gestione errori UART
//-----------------------------------------------------------------------------
void HAL_UART_ErrorCallback(UART_HandleTypeDef* huart)
{
  UART_HandleTypeDef* uart_handle = NULL;
  UART_Mng_t* seriale_handle = NULL;
  uint8_t uart_gestita;
  
  uart_gestita = 1;
  if(huart->Instance == UART_MODEM)
  {
    uart_handle = &handle_UART_Modem;
    seriale_handle = &Seriale_Modem;
  }
  else if(huart->Instance == UART_WIFI)
  {
    uart_handle = &handle_UART_WiFi;
    seriale_handle = &Seriale_WiFi;
  }
  else    
    uart_gestita = 0;
  
  if(uart_gestita)
  {
    seriale_handle->debug.err_code = huart->ErrorCode;
    seriale_handle->debug.err_count++;		
    
    if ((HAL_IS_BIT_SET(huart->Instance->CR3, USART_CR3_DMAR)) ||
        ((seriale_handle->debug.err_code & HAL_UART_ERROR_ORE) != 0U))
    {
      seriale_handle->debug.ORE_error++;            
      if(HAL_UART_Receive_IT(uart_handle, seriale_handle->buffer.rx_buffer, 1) != HAL_OK)
        seriale_handle->debug.hal_err_count_ERR++;
    }
  }
}

/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
