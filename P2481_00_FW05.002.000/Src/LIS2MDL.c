#include "main.h"
#include "Gestione_dato.h"
#include "i2c.h"
#include "EEPROM.h"
#include "LIS2MDL.h"
#include "lis2mdl_reg.h"
#include "math.h"
/* USER CODE BEGIN PM */
/*
static axis3bit16_t data_raw_acceleration;
static axis3bit16_t data_raw_angular_rate;
static axis1bit16_t data_raw_temperature;
*/



//float NumeroGiriGyr;
uint8_t count_init_LIM;

//uint8_t lsm6dsl_angular_fail_count_dbg, lsm6dsl_acceleration_fail_count_dbg, FlagDebugTemp;

//uint8_t flag_lsm6dsl_ignore_int;
//uint8_t timer_flag_lsm6dsl_ignore_int;

//uint8_t ff_duration;
//lsm6dsl_ff_ths_t ff_soglia;

static int32_t lis_platform_write(void *handle, uint8_t Reg, uint8_t *Bufp,
                              uint16_t len)
{
  HAL_StatusTypeDef result;
  if (handle == &hi2c3)
  {
    result = HAL_I2C_Mem_Write(handle, LIS2MDL_I2C_ADD_L, Reg,
                               I2C_MEMADD_SIZE_8BIT, Bufp, len, 100/*1000*/);
  }
  return result;
}

static int32_t platform_read(void *handle, uint8_t Reg, uint8_t *Bufp,
                             uint16_t len)
{
  HAL_StatusTypeDef result;
  if (handle == &hi2c3)
  {
    result = HAL_I2C_Mem_Read(handle, LIS2MDL_I2C_ADD_L, Reg,
                              I2C_MEMADD_SIZE_8BIT, Bufp, len, 100/*1000*/);
  } 
  return result;
}


//lis2mdl_status_reg_t reg;
void InitLIS2MDL(void)
{
  /*
  *  Initialize mems driver interface
  */
  
  /*
  *  Check device ID
  */
  stmdev_ctx_t dev_ctx;

  dev_ctx.write_reg = lis_platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c3;

  lis2mdl_data_rate_set(&dev_ctx, LIS2MDL_ODR_100Hz);
  lis2mdl_power_mode_set(&dev_ctx, LIS2MDL_LOW_POWER);
  lis2mdl_operating_mode_set(&dev_ctx, LIS2MDL_CONTINUOUS_MODE);
  lis2mdl_low_pass_bandwidth_set(&dev_ctx, LIS2MDL_ODR_DIV_4);
  lis2mdl_offset_temp_comp_set(&dev_ctx, 1);
  lis2mdl_block_data_update_set(&dev_ctx, 1);
}


void LIS2MDL_low_power(void)
{
  /*
  *  Initialize mems driver interface
  */
  
  /*
  *  Check device ID
  */
  stmdev_ctx_t dev_ctx;

  dev_ctx.write_reg = lis_platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c3;

  lis2mdl_operating_mode_set(&dev_ctx, LIS2MDL_POWER_DOWN);
  //lis2mdl_set_rst_sensor_single_set(&dev_ctx, 1);
    
  //lis2mdl_mag_user_offset_get(&dev_ctx, &qwerty[0]);
  //
}




uint8_t count_mg;
int32_t tot_mg, mean_mg, mean_mg_old;
int16_t mag_raw[10];
int16_t raw_mg;
int32_t LIS2MDL_get_mean_10(void)
{
  stmdev_ctx_t dev_ctx;
  
  
  
  dev_ctx.write_reg = lis_platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c3;
  
  lis2mdl_magnetic_raw_get(&dev_ctx, &mag_raw[0]);
  
  
  /*if(mag_raw[0] < 0) raw_mg = -mag_raw[0];
  else raw_mg = mag_raw[0];*/
  raw_mg = mag_raw[0];
  
  
  if(count_mg < 10)
  {
    count_mg++;
    tot_mg += (int16_t)raw_mg;
    if(count_mg == 10)
    {
      mean_mg = tot_mg/10;
      tot_mg = 0;
      count_mg = 0;
    }
  }
  return mean_mg;  
}


uint8_t LIS2MDL_get_ID(void)
{
  
  stmdev_ctx_t dev_ctx;
  dev_ctx.write_reg = lis_platform_write;
  dev_ctx.read_reg = platform_read;
  dev_ctx.handle = &hi2c3;
  uint8_t temp;
  
  lis2mdl_device_id_get(&dev_ctx, &temp);
  
  return temp;  
}





