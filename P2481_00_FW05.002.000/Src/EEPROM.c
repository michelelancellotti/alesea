#include "EEPROM.h"
#include "stm32l0xx_hal.h"
#include "Gestione_dato.h"
#include "funzioni.h"
                                                            
                                                                	
uint16_t EEPROM_IndiceArray;                                                               
uint16_t CounterErroreEEPROM;
                              
                              
/* Scrive una word in EEPROM */
void EEPROM_WriteWord(uint32_t Indirizzo, int32_t Dato)
{
  uint32_t add = Indirizzo;
  int32_t dato_letto = 0;
  
  RefreshWD();
  My_DATAEEPROM_Unlock();
  My_DATAEEPROM_Program(add++, (uint8_t)(Dato >> 0));
  My_DATAEEPROM_Program(add++, (uint8_t)(Dato >> 8));
  My_DATAEEPROM_Program(add++, (uint8_t)(Dato >> 16));
  My_DATAEEPROM_Program(add, (uint8_t)(Dato >> 24));
  My_DATAEEPROM_Lock();
  
  dato_letto = EEPROM_ReadWord(Indirizzo);
  if(dato_letto != Dato) 
  {
    CounterErroreEEPROM++;
  }
}


/* Scrive un byte in EEPROM */
void EEPROM_WriteByte(uint32_t Indirizzo, uint8_t Dato)
{
  uint8_t dato_letto = 0;

  RefreshWD();
  My_DATAEEPROM_Unlock();
  My_DATAEEPROM_Program(Indirizzo, Dato);
  My_DATAEEPROM_Lock();
  
  dato_letto = EEPROM_ReadByte(Indirizzo);
  if(dato_letto != Dato)        
  {
    CounterErroreEEPROM++;
  }
}

void My_DATAEEPROM_Program(uint32_t Indirizzo, uint8_t Dato)
{
  uint8_t status = 0;
  for(uint8_t i = 0; i < 3; i++)
  {
    if( HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, Indirizzo, Dato) == HAL_OK  )
    {
      break;
    }
    else
    {
      status++;
    }
  }
}

void My_DATAEEPROM_Unlock(void)
{
  uint8_t status = 0;
  for(uint8_t i = 0; i < 3; i++)
  {
    if( HAL_FLASHEx_DATAEEPROM_Unlock() == HAL_OK)
    {
      break;
    }
    else
    {
      status++;
    }
  }
}

void My_DATAEEPROM_Lock(void)
{
  uint8_t status = 0;
  for(uint8_t i = 0; i < 3; i++)
  {
    if( HAL_FLASHEx_DATAEEPROM_Lock() == HAL_OK)
    {
      break;
    }
    else
    {
      status++;
    }
  }
}


/* Legge una word in EEPROM */
int32_t EEPROM_ReadWord(uint32_t Posizione)
{
  uint8_t appoggio;
  int32_t DatoLettoEEPROM;
  
  appoggio = *(int8_t*)Posizione++;
  DatoLettoEEPROM = ((int32_t)appoggio) & 0x000000FF;
  
  appoggio = *(int8_t*)Posizione++;
  DatoLettoEEPROM |= ( ((int32_t)appoggio)<<8) & 0x0000FF00;
  
  appoggio = *(int8_t*)Posizione++;
  DatoLettoEEPROM |= ( ((int32_t)appoggio)<<16) & 0x00FF0000;
  
  appoggio = *(int8_t*)Posizione;
  DatoLettoEEPROM |= ( ((int32_t)appoggio)<<24) & 0xFF000000;
  
  return DatoLettoEEPROM;
}

/* Legge un byte in EEPROM */
uint8_t EEPROM_ReadByte(uint32_t Posizione)
{
  uint32_t align_offset = (Posizione%4);
  uint32_t align_posizione = Posizione - align_offset;
  uint32_t WordLettaEEPROM = *(uint32_t*)align_posizione;
  uint8_t DatoLettoEEPROM = (WordLettaEEPROM >> (8*align_offset));      //Workaround necessario perch� se l'indirizzo non � allineato a 4B non si riesce a leggere (HARD FAULT) 
  return DatoLettoEEPROM;
}





