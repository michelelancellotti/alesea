#include "FuelGauge.h"
#include "i2c.h"

void TEstFuelGauge(void)
{
  HAL_I2C_Master_Transmit( (I2C_HandleTypeDef *)&hi2c1, NFCAddressRR, (uint8_t*)&NFCBlock[0], NFCSizeTX, NFCTimeout);
}
